<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_active_to_online_slide_image extends CI_Migration
{

   public function up()
   {
       $this->dbforge->add_column('online_slide_image', [
           'active' => [
               'type' => 'TINYINT',
               'constraint' => '1',
               'null' => false,
           ]
       ]);

   }

   public function down()
   {
       $this->dbforge->drop_column('online_slide_image', 'bodyimage');
   }

}