<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_restaurant_name_to_online_settings extends CI_Migration
{

   public function up()
   {
       $this->dbforge->add_column('online_settings', [
           'restaurant_name' => [
               'type' => 'VARCHAR',
               'constraint' => '250',
           ]
       ]);

   }

   public function down()
   {
       $this->dbforge->drop_column('online_settings', 'restaurant_name');
   }

}