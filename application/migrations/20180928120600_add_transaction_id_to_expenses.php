
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_transaction_id_to_expenses extends CI_Migration
{

   public function up()
   {
       $this->dbforge->add_column('expenses', [
           'transaction_id' => [
               'type' => 'INT',
               'constraint' => '11',
               'null' => true,
           ]
       ]);

       $this->dbforge->add_key('transaction_id', false);

       $this->db->query(add_foreign_key('expenses', 'transaction_id', 'transactions(id)', 'NO ACTION', 'NO ACTION'));
   }

   public function down()
   {
       $this->db->query(drop_foreign_key('expenses', 'transaction_id'));

       $this->dbforge->drop_column('expenses', 'transaction_id');
   }

}