
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_modify_stocks_id_to_ys_products extends CI_Migration
{
	public function up()
    {
       $this->dbforge->modify_column('ys_products', [
           'stocks_id' => [
               'name' => 'stock_id',
               'type' => 'INT',
               'null' => true,
           ]
       ]);

    }

    public function down()
    {
       $this->dbforge->modify_column('ys_products', [
           'stock_id' => [
               'name' => 'stocks_id',
               'type' => 'INT',
               'null' => true,
           ]
       ]);
    }

}