<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_bodyimage_to_online_settings extends CI_Migration
{

   public function up()
   {
       $this->dbforge->add_column('online_settings', [
           'bodyimage' => [
               'type' => 'VARCHAR',
               'constraint' => '255',
           ]
       ]);

   }

   public function down()
   {
       $this->dbforge->drop_column('online_settings', 'bodyimage');
   }

}