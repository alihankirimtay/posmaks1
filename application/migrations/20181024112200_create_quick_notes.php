
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_quick_notes extends CI_Migration
{

    public function up()
    {
      // quick_notes
      $this->dbforge->add_field([
        'id' => [
          'type' => 'INT',
          'constraint' => 11,
          'auto_increment' => TRUE
        ],
        'title' => [
          'type' => 'VARCHAR',
          'constraint' => 255,
        ],
        'active' => [
           'type' => 'TINYINT',
           'constraint' => '1',
           'null' => false,
         ],
         'created' => [
           'type' => 'DATETIME',
           'null' => true,
         ],
         'modified' => [
           'type' => 'DATETIME',
           'null' => true,
         ],
         'deleted' => [
           'type' => 'DATETIME',
           'null' => true,
         ],
      ]);
      $this->dbforge->add_key('id', TRUE);
      $this->dbforge->create_table('quick_notes');



      // products_quick_notes
      $this->dbforge->add_field([
        'product_id' => [
          'type' => 'INT',
          'constraint' => 11,
        ],
        'quick_note_id' => [
          'type' => 'INT',
          'constraint' => 11,
        ],
      ]);
      $this->dbforge->add_key('product_id');
      $this->dbforge->add_key('quick_note_id');
      $this->dbforge->create_table('products_quick_notes');

      $this->db->query(add_foreign_key('products_quick_notes', 'product_id', 'products(id)', 'NO ACTION', 'NO ACTION'));
      $this->db->query(add_foreign_key('products_quick_notes', 'quick_note_id', 'quick_notes(id)', 'NO ACTION', 'NO ACTION'));

    }

    public function down()
    {
      $this->db->query(drop_foreign_key('products_quick_notes', 'product_id'));
      $this->db->query(drop_foreign_key('products_quick_notes', 'quick_note_id'));
      
      $this->dbforge->drop_table('quick_notes', TRUE);

      $this->dbforge->drop_table('products_quick_notes', TRUE);
    }

}