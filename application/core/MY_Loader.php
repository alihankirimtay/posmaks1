<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MY_Loader extends CI_Loader {

    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $this->view('header', $vars, $return);
        $this->view($template_name, $vars, $return);
        $this->view('footer', $vars, $return);
    }
}