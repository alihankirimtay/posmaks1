<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->db->query("SET time_zone='+0:00'");

        $this->website();

        $this->page_title = NULL;

        $this->_assets = (object) [
            'css' => [],
            'js' => [],
            'script' => '',
        ];

        $this->number_format = (object) [
            'decimal' => ',',
            'thousands' => '.'
        ];

        $this->days = [
                        ['title' =>'Bugün',         'id'=>'today'],
                        ['title' =>'Son 365 gün',   'id'=>'last365days'],
                        ['title' =>'Özel',          'id'=>'custome'],
                        ['title' =>'Dün',           'id'=>'yesterday'],
                        ['title' =>'Bu Hafta',      'id'=>'thisweek'],
                        ['title' =>'Bu Ay',         'id'=>'thismonth'],
                        ['title' =>'Bu Yıl',        'id'=>'thisyear'],
                        ['title' =>'Geçen Hafta',   'id'=>'lastweek'],
                        ['title' =>'Geçen Ay',      'id'=>'lastmonth'],
                        ['title' =>'Geçen Yıl',     'id'=>'lastyear'],
        ];

        $this->log_types = [
            1 => 'USB Backup Request',
            2 => 'Application Manage Request',
            3 => 'POS Discount Request',
            4 => 'POS Retrun Request',
            5 => 'POS and Location select',
            6 => 'POS Remove Tax Request',
        ];

        $this->units = [
            'number' => __('Adet'),
            'l'      => __('Litre'),
            'kg'     => __('Kilogram'),
            'gr'     => __('Gram'),
        ];

        $this->taxes = [
            18 => '%18',
            8  => '%8',
            1  => '%1',
            0  => '%0',
        ];
    }

    function exists($from, $where, $what = 'id, title, desc, active')
    {
        return $this->db    ->select($what)     ->where($where)     ->get($from, 1)->row();
    }

    function website()
    {
        $this->website = $this->db->get('settings', 1)->row();

        if(!$this->website->active) show_error( $this->website->name . ' is inactive at the moment. Please contact to: <a href="mailto:'.$this->website->email.'">Send mail</a>');

        $this->website->screen_saver = ($this->website->screen_saver && file_exists(FCPATH . $this->website->screen_saver)) ? $this->website->screen_saver : 'assets/ScreenSaver.jpg';

        $this->website->dtz = 'UTC';
        date_default_timezone_set( ($this->website->dtz ? $this->website->dtz : 'UTC') );

        $CI =& get_instance();
        $CI->config->website = $this->website;
        $CI->config->dtz['server'] = $this->website->dtz;//['server' => $this->website->dtz];
    }

    function sendMail($from, $to, $title, $body)
    {
        $config['protocol']  = 'smtp';
        $config['smtp_host'] = $this->website->smtp_host;
        $config['smtp_port'] = $this->website->smtp_port;
        $config['smtp_user'] = $this->website->smtp_user;
        $config['smtp_pass'] = $this->website->smtp_pass;
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($title);
        $this->email->message($body);

        if($this->email->send())
            return true;
        return false;
    }

    function DDosAttack()
    {
        // $uri = md5($_SERVER[ 'REQUEST_URI' ]);
        // $exp = 2; // seconds
        // $hash = $uri .'|'. time();
        // if (!isset($_SESSION[ 'ddos' ])) {
        //     $_SESSION[ 'ddos' ] = $hash;
        // }

        // list($_uri, $_exp) = explode('|', $_SESSION[ 'ddos' ]);
        // if ($_uri == $uri && time() - $_exp < $exp) {
        //     header('HTTP/1.1 503 Service Unavailable');
        //     exit;
        // }

        // // Save last request
        // $_SESSION[ 'ddos' ] = $hash;
    }
}

class Public_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }
}

class Auth_Controller extends MY_Controller {
    
    public $Globals;

    function __construct()
    {
        parent::__construct();
        $this->is_logged_in();
        $this->get_user();
        $this->_userPerms();
        // $this->check_auth();
        $this->Globals();
        $this->checkExpireDate();
        $this->triggerStockController();
        $this->handleTutorialScript();
    }

    protected function Globals()
    {
        $this->Globals = (object) [
            //'payment_types' => $this->_paymentTypes(),
            'max_execution_time' => ini_get('max_execution_time'),
            'request_time' => $this->input->server('REQUEST_TIME'),
        ];
    }

    protected function _paymentTypes()
    {
        $payment_types = $this->db
        ->where('active', 1)
        ->get('payment_types')
        ->result();

        if (!$payment_types) {
            exit('No payment type found.');
        }

        $tmp = null;
        foreach ($payment_types as $payment) {

            switch ($payment->id) {
                case 1: $alias = 'cash'; break;
                case 2: $alias = 'credit'; break;
                case 5: $alias = 'voucher'; break;
                case 6: $alias = 'ticket'; break;
                case 7: $alias = 'sodexo'; break;
            }

            $tmp[$alias] = $payment;
        }
        return (object) $tmp;
    }

    protected function is_logged_in()
    {
        if(!$this->session->userdata('loggedin')) {

            if ($this->input->is_ajax_request()) {
                messageAJAX('error', 'Sisteme giriş yapın.', [
                    'event' => 'login'
                ]);
            }
            redirect(base_url('login'));
        }
    }

    protected function get_user()
    {
        $userid = (int) $this->session->userdata('loggedin');

        if (!$this->user = $this->exists('users', ['id' => $userid], 'users.*')) {
            
            logs(null, $userid . ': User does not exist.');
            redirect(base_url('login/logout'));
        }
        
        if(!file_exists(FCPATH.'assets/uploads/profile/'.$this->user->image)){
            $this->user->image = 'no_image.png';
        }

        if(!$this->user->active) {

            $this->session->set_flashdata('_error', logs($userid, 'Sorry, your account has been disabled.'));
            redirect(base_url('login/logout'));
        }

        $this->db->where('active', 1);
        if (!$role = $this->exists('roles', ['id' => $this->user->role_id], '*')) {
            
            $this->session->set_flashdata('_error', logs($userid, 'Sorry, your account role has been disabled.'));
            redirect(base_url('login/logout'));
        }

        $this->user->role = $role;


        // Kullanıcının Lokasyonları çekiliyor.
        $this->db->select('L.id, L.title, L.currency, L.yemeksepeti, L.dtz, L.active');
        if((int)$this->user->role_id !== 1) {

            $this->db->where('UHL.user_id' , $this->user->id);
            $this->db->join('user_has_locations UHL', 'UHL.location_id = L.id');
        }
        $this->db->where('active', 1);
        $this->user->locations_array = $this->db->get('locations L')->result_array();
        $this->user->locations       = ($this->user->locations_array) ? array_column( $this->user->locations_array , 'id' ) : [0];
		
		$tmp_locations = [];
		if($this->user->locations_array) {
			foreach($this->user->locations_array as $value) {
				$tmp_locations[ $value['id'] ] = $value;
			}
		}
		$this->user->locations_array = $tmp_locations;

        // $this->config->item('dtz') = ['client' => $this->user->dtz];

        $CI =& get_instance();
        $CI->config->website = $this->website;
        $CI->config->dtz['client'] = ($this->user->dtz ? $this->user->dtz : $CI->config->dtz['server']);

    }

    protected function _userPerms()
    {
        $this->user->permissions = [];

        if ($this->user->role->alias != 'admin') {      
            $roles_permissions = $this->db
            ->select('permissions.*')
            ->where([
                'permissions.active' => 1,
                'role_has_permissions.role_id' => $this->user->role_id,
            ])
            ->join('role_has_permissions', 'role_has_permissions.permission_id = permissions.id')
            ->get('permissions')->result();

            foreach ($roles_permissions as $role_permisson) {
                $this->user->permissions[$role_permisson->action] = $role_permisson;
            }
        }
    }

    protected function check_auth($data = array())
    {
        if( intval($this->user->role_id) === 1 ) {
            return true;
        }

        $controller = $this->router->fetch_class();
        $model      = $this->router->fetch_method();

        // Özel izin verilen fonksiyonlar.
        if (isset($data['allowed'])) {
            if (is_array($data['allowed'])) {
                foreach ($data['allowed'] as $allowed) {
                    if ($allowed == $model) {
                        return true;
                    }
                }
            }
        }

        if (!hasPermisson("{$controller}.{$model}")) {
            if ($this->input->is_ajax_request()) {
                messageAJAX('error', 'Bu işlemi yapma yetkiniz yok.');
            }
            error_('access');
        }
    }

    public function checkExpireDate()
    {
        if ($this->router->fetch_class() == 'memberships') {
            return;
        }

        if(!isValidDate($this->website->date_expire, 'Y-m-d H:i:s'))
            redirect(base_url('memberships'));

        $dateCurrent = new DateTime();
        $dateExpire = new DateTime($this->website->date_expire);

        $interval = $dateCurrent->diff($dateExpire);
        
        if ($dateCurrent > $dateExpire) {
            redirect(base_url('memberships'));
        } elseif ($interval->days <= 7) {

            $notify_time = $this->input->cookie('membershipNotifyTime');
            $notify_time = isValidDate($notify_time, 'Y-m-d H:i:s') ? date('H', strtotime($notify_time)) : null;

            if ($notify_time === null) {
                setcookie('membershipNotifyTime', date('Y-m-d H:i:s'), time()+60*60*24*365, '/');
            }

            if (between((int) $dateCurrent->format('i'), 54, 59)) {
                if ($notify_time != $dateCurrent->format('H')) {

                    $notifiedAt = $dateCurrent->format('Y-m-d H:i:s');

                    $this->_assets->js[] = 'panel/js/view/memberships.js';
                    $this->_assets->script.= "Memberships.notify({
                        days: {$interval->d},
                        notifiedAt: '{$notifiedAt}',
                    });";
                }
            }          
        }
    }

    public function triggerStockController()
    {
        $this->_assets->js[] = 'panel/js/view/stocks.js';
        $this->_assets->script.= "Stocks.countCriticalStocks();";
    }

    public function handleTutorialScript() {

        $mobile=$this->agent->is_mobile();

        if (!$mobile) {
            $this->_assets->css[] = 'globals/css/tutorialize.css';
            $this->_assets->js[] = 'panel/js/tutorial.js';
            $this->_assets->js[] = 'globals/js/jquery.tutorialize.js';
        }

    }
}


/**
        API Controller
**/
class API_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function getPOST($field, $rules = 'required|trim|min_length[5]|max_length[12]', $xss_clean = TRUE)
    {
        if(!isset($_POST[$field])) api_messageError('The '.$field.' field is required.');

        $this->load->library('form_validation');
        $this->form_validation->set_rules($field, $field, $rules);
        if(! $this->form_validation->run()) api_messageError(validation_errors(NULL, NULL));

        return $this->input->post($field, ($xss_clean ? TRUE : NULL));
    }
}
