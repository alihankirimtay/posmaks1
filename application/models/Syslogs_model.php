<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Syslogs_Model extends CI_Model {
	
	    const logs = 'logs';
	    const users = 'users';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($page = 1)
	    {
        	$page = (int)$page ? $page : 1;

	    	$page = ($page * 20) - 20;

	    	$this->db->select('id, SUBSTRING(message,1, 50) as message, ip, created, (SELECT username FROM '.self::users.' WHERE id = '.self::logs.'.user_id LIMIT 1) as user');
	    	$this->db->order_by('id', 'desc');
	    	return $this->db->get(self::logs, 30, $page)->result();
	    }
	}    
?>
