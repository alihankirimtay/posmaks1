<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Products_Model extends CI_Model {
	
        const products = 'products';
        const locationhasproducts = 'location_has_products';
	    const productshasproducts = 'products_has_products';
        const locations           = 'locations';
        const stocks              = 'stocks';
        const producthasstocks = 'product_has_stocks';
		const produccategories    = 'product_types';

        const upload_path         = 'assets/uploads/products';

        private $producttypesarray = ['package'];

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index($location)
        {
            $location = (int) $location;

            if($location && !in_array($location, $this->user->locations)) error_('access');

            $this->db->select('P.*, L.currency, L.title as location');
			$this->db->join(self::locations.' L' , 'L.id = P.location_id');

            ($location)
            ? $this->db->where('P.location_id', $location)
            : $this->db->where_in('P.location_id', $this->user->locations);

            $this->db->group_by('P.id');
            $this->db->where('P.active !=', 3);
            return $this->db->get(self::products.' P')->result();  
        }

	    function add()
	    {
            $data[ 'product_categories' ] = $this->db
                ->where('active !=', '3')
            ->get(self::produccategories)->result_array();



            if ($this->input->is_ajax_request()) {

                $values[ 'location_id' ]     = $this->input->post('location');
                $values[ 'title' ]           = $this->input->post('title', TRUE);
                $values[ 'desc' ]            = $this->input->post('desc', TRUE);
                $values[ 'bar_code' ]        = $this->input->post('bar_code', TRUE);
                $values[ 'type' ]            = $this->input->post('type');
                $values[ 'color' ]           = $this->input->post('color');
                $values[ 'active']           = $this->input->post('active');
                $values[ 'price' ]           = $this->input->post('price');
                $values[ 'product_type_id' ] = $this->input->post('category') ? $this->input->post('category') : NULL;
                $values[ 'created' ]         = _date();
                $values[ 'production_id' ]   = $this->input->post('production_id');
                $values[ 'tax' ]             = $this->input->post('tax');
                $package_has_products        = NULL;
                $product_has_stock           = NULL;


                // IF CREATING NEW PACKAGE
                if (in_array($values[ 'type' ], $this->producttypesarray)) {

                    foreach ($this->input->post('products[]') as $key => $product) {

                        // Location has product?
                        $location_has_product = $this->db
                            ->where('id', (int) $product)
                            ->where('location_id', $values[ 'location_id' ])
                            ->get(self::products, 1)->row();

                        if (! $location_has_product) {

                            messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', $values + ['products_in_package' => $this->input->post('products[]')]));
                        }

                        $package_has_products[ ] = [
                            'package_id' => 0,
                            'product_id' => $product
                        ];
                    }
                } else {

                    foreach ($this->input->post('stocks[]') as $stock_id => $value) {

                         // check stock
                        $location_has_stock = $this->db
                            ->where('id', (int) $stock_id)
                            ->where('location_id', $values[ 'location_id' ])
                            ->get(self::stocks, 1)->row();

                        if (!$location_has_stock) {
                            messageAJAX('error', logs_($this->user->id, 'Geçersiz stok ürünü. ', $values + ['location_has_stock' => $this->input->post('stocks[]')]));
                        }

                        $product_has_stock[] = [
                            'stock_id'   => $stock_id,
                            'product_id' => 0,
                            'quantity'   => doubleval(str_replace(',', '', $value))
                        ];
                    }
                }


                if($values['product_type_id'] && !in_array($values['product_type_id'], array_column($data['product_categories'], 'id'))) {
                    messageAJAX( 'error', logs_($this->user->id, 'Geçersiz Kategori. ', $values) );
                }

                $this->load->helper('string');
                $config['upload_path']   = self::upload_path;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['file_name']     = sprintf('%d-%s-%s-%s-%s-%s',
                                                $this->user->id,
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                str_replace('.', '_', microtime(1)));

                @mkdir(FCPATH . self::upload_path);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('image')) {
                    $values['image'] = $config['upload_path'] . '/' . $this->upload->data()['file_name'];
                } else {
                    messageAJAX('error', $this->upload->display_errors());
                }


                if ($this->db->insert(self::products, $values)) {

                    $insert_id = $this->db->insert_id();

                    // If inserting a new package then  insert products in package.
                    if (in_array($values[ 'type' ], $this->producttypesarray)) {
                        foreach ($package_has_products as $key => $package) {
                            $package_has_products[ $key ][ 'package_id' ] = $insert_id;

                            $this->db->insert(self::productshasproducts, $package_has_products[ $key ]);
                        }

                    } else {
                        foreach ($product_has_stock as $key => $stock) {
                            $product_has_stock[ $key ][ 'product_id' ] = $insert_id;

                            $this->db->insert(self::producthasstocks, $product_has_stock[ $key ]);
                        }
                    }

                    messageAJAX('success', logs_($this->user->id, 'Ürün oluşturuldu. ', [ 'id' => $insert_id ] + $values + [ 'products_in_package' => $this->input->post('products[]') ]));
                }

                messageAJAX('error', logs_($this->user->id, 'An error occurred. See logs.', $values + [ 'products_in_package' => $this->input->post('products[]') ]));

            } else {

                return $data;

            }
	    }

	    function edit($id, $_product)
	    {
            $id = (int) $id;

            $data[ 'product_categories' ] = $this->db
                ->where('active !=', '3')
            ->get(self::produccategories)->result_array();

            if ($this->input->is_ajax_request()) {

                $values[ 'location_id' ]     = $this->input->post('location');
                $values[ 'title' ]           = $this->input->post('title', TRUE);
                $values[ 'desc' ]            = $this->input->post('desc', TRUE);
                $values[ 'bar_code' ]        = $this->input->post('bar_code', TRUE);
                $values[ 'type' ]            = $this->input->post('type');
                $values[ 'color' ]           = $this->input->post('color');
                $values[ 'active']           = $this->input->post('active');
                $values[ 'price' ]           = $this->input->post('price');
                $values[ 'product_type_id' ] = $this->input->post('category') ? $this->input->post('category') : NULL;
                $values[ 'production_id' ]   = $this->input->post('production_id', TRUE);
                $values[ 'tax' ]             = $this->input->post('tax');
                $values[ 'modified' ]        = _date();
                $package_has_products        = NULL;
                $product_has_stock           = NULL;

                // IF EDITING A PACKAGE
                if (in_array($values[ 'type' ], $this->producttypesarray)) {

                    foreach ($this->input->post('products[]') as $key => $product) {

                        // Location has product?
                        $location_has_product = $this->db
                            ->where('id', (int) $product)
                            ->where('location_id', $values[ 'location_id' ])
                            ->get(self::products, 1)->row();

                        if (! $location_has_product) {

                            messageAJAX('error', logs_($this->user->id, 'Geçersiz Restoran. ', $values + ['products_in_package' => $this->input->post('products[]')]));
                        }

                        $package_has_products[ ] = [
                            'package_id' => $id,
                            'product_id' => $product
                        ];
                    }
                } else {

                    foreach ($this->input->post('stocks[]') as $stock_id => $value) {

                         // check stock
                        $location_has_stock = $this->db
                            ->where('id', (int) $stock_id)
                            ->where('location_id', $values[ 'location_id' ])
                            ->get(self::stocks, 1)->row();

                        if (!$location_has_stock) {
                            messageAJAX('error', logs_($this->user->id, 'Geçersiz stok ürünü. ', $values + ['location_has_stock' => $this->input->post('stocks[]')]));
                        }

                        $product_has_stock[] = [
                            'stock_id'   => $stock_id,
                            'product_id' => $id,
                            'quantity'   => doubleval(str_replace(',', '', $value))
                        ];
                    }
                }

                if($values['product_type_id'] && !in_array($values['product_type_id'], array_column($data['product_categories'], 'id'))) {
                    messageAJAX( 'error', logs_($this->user->id, 'Geçersiz Kategori. ', $values) );
                }

                if (isset($_FILES['image'])) {
                    

                    $this->load->helper('string');
                    $config['upload_path']   = self::upload_path;
                    $config['allowed_types'] = 'gif|jpg|jpeg|png';
                    $config['file_name']     = sprintf('%d-%s-%s-%s-%s-%s',
                                                    $this->user->id,
                                                    strtoupper(random_string('alnum', 4)),
                                                    strtoupper(random_string('alnum', 4)),
                                                    strtoupper(random_string('alnum', 4)),
                                                    strtoupper(random_string('alnum', 4)),
                                                    str_replace('.', '_', microtime(1)));

                    @mkdir(FCPATH . self::upload_path);
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('image')) {
                        $values['image'] = $config['upload_path'] . '/' . $this->upload->data()['file_name'];
                        if ($_product->image && file_exists(FCPATH . $_product->image)) {
                            @unlink(FCPATH . $_product->image);
                        }
                        
                    } else {
                        messageAJAX('error', $this->upload->display_errors());
                    }
                }


                $this->db->where('id', $id);
                if ($this->db->update(self::products, $values)) {

                    // Delete products in package
                    $this->db->where('package_id', $id);
                    $this->db->delete(self::productshasproducts);

                    $this->db->where('product_id', $id);
                    $this->db->delete(self::producthasstocks);

                    // If this is a package then  insert products in package.
                    if (in_array($values[ 'type' ], $this->producttypesarray)) {

                        // Daha önceden üründü ve artık paket oluyorsa, daha önce ait olduğu paketlerin içerisinden siliniyor.
                        $this->db->where('product_id', $id);
                        $this->db->delete(self::productshasproducts);

                        $this->db->insert_batch(self::productshasproducts, $package_has_products);
                    } else {

                        // Daha önceden paketti ve artık ürün oluyorsa, daha önce ait olduğu ürünlerin içerisinden siliniyor.
                        $this->db->where('product_id', $id);
                        $this->db->delete(self::productshasproducts);

                        $this->db->insert_batch(self::producthasstocks, $product_has_stock);

                    }

                    messageAJAX('success', logs_($this->user->id, 'Ürün güncellendi. ', [ 'id' => $id ] + $values + [ 'products_in_package' => $this->input->post('products[]') ]));
                }

                messageAJAX('error', logs_($this->user->id, 'An error occurred. See logs.', $values + [ 'products_in_package' => $this->input->post('products[]') ]));

            } else {

                // Get product list in this location
                $data[ 'products' ] = $this->db
                    ->where('location_id', $_product->location_id)
                    ->where('type', 'product')
                ->get(self::products)->result();

                $data[ 'stocks' ] = $this->db
                    ->where('location_id', $_product->location_id)
                    ->where('active !=', 3)
                ->get(self::stocks)->result();

                // if this is a package then get products in this package.
                $data[ 'package_has_products' ] = [];
                $data[ 'package_has_stocks' ] = [];
                if (in_array($_product->type, $this->producttypesarray)) {
                    $data[ 'package_has_products' ] = $this->db
                        ->select('P.id, P.title')
                        ->where('PHP.package_id', $_product->id)
                        ->join(self::products . ' P', 'P.id = PHP.product_id')
                    ->get(self::productshasproducts . ' PHP')->result();
                } else {
                    $data[ 'package_has_stocks' ] = $this->db
                        ->select('PHS.stock_id, PHS.quantity, (SELECT title FROM stocks WHERE id = PHS.stock_id LIMIT 1) as stock_title, (SELECT unit FROM stocks WHERE id = PHS.stock_id LIMIT 1) as stock_unit')
                        ->where('PHS.product_id', $_product->id)
                    ->get(self::producthasstocks . ' PHS')->result();
                }

                return $data;
            } 
	    }


        function delete($id, $product)
        {
            $id   = (int) $id;
            $data = [ 'active' => 3, 'modified' => _date() ];
            
            $this->db->where('id', $id);
            if( $this->db->update(self::products, $data) ) {

                $this->db->where('product_id', $id);
                $this->db->update(self::locationhasproducts, $data);
                
                messageAJAX('success', logs_($this->user->id, 'Ürün silindi.', $product));
            }
        }
	}    
?>
