<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Login_Model extends CI_Model {
	
	    const users = 'users';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index()
	    {
	    	$name     = $this->input->post('name', TRUE);
            $password = sha1( $this->input->post('password', TRUE) . $this->config->config['salt'] );

			$this->db->where('email', $name);
			$this->db->or_where('username', $name);


			if( $user = $this->db->get(self::users)->row() ) {

				if( $user->password == $password ) {


					if(! $user->active) {

						return ['error' => logs_($user->id, 'Üzgünüz, hesabınız devre dışı bırakılmış.')];
					}

					if (!$role = $this->db->where('active=1 AND id='.$user->role_id)->get('roles',1)->row()) {
						
						return ['error' => logs_($user->id, 'Üzgünüz, heabınızın yetki rolü devre dışı bırakılmış.')];
					}

					


					$user->role_alias = $role->alias;

					$this->db->update( self::users, ['lastip' => $this->input->ip_address(), 'lastlogin' => _date()], "id = $user->id" );

					logs_($user->id, 'Giriş başarılı.');

					$this->session->set_userdata('loggedin', $user->id);

					$this->load->model('Sessions_M');
					$this->Sessions_M->createOnAvailableLocationsForUser($user);

       				$device = $this->input->get('device');

       				if ($device == "garson") {
       					redirect('mobilpos?device=garson', 'refresh');
       				} else if ($device == "tablet") {
       					redirect('mobilpos/tablet?device=tablet', 'refresh');
       				} else {
       					if($role->alias == "admin")
						{
							$user = $this->db->get_where(self::users,["id"=>$user->id],1)->row();
							$token = sha1(json_encode($user));
							$url=base_url("Online/Auth/quickLoginAdmin/{$user->id}/{$token}");
							redirect(base_url("home/index?online={$url}"));

						
						}
						redirect(base_url());
       				}
				}
			}
			return ['error' => logs_(NULL, 'Hatalı kullanıcı veya şifre.', ['name'=>$name])];
		}

		public function activationControl($code)
		{

			
			$this->db->where('activation_code' , $code);
			$control = $this->db->get('user_has_activation');

			if($control->num_rows() > 0):
				
				$this->db->where('id' , $control->row()->user_id)->update('users' , array( 'password' => $control->row()->password));
			
			if($this->db->affected_rows() > 0):

				$this->db->where('activation_code' , $code)->delete('user_has_activation');

				logs_($control->row()->user_id , 'Şifre değiştirildi', $control->row());

				return TRUE;

			else:
				return FALSE;
			endif;

			else:

				return FALSE;

			endif;      
		}

	    
	}    


?>
