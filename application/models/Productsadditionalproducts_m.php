<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductsAdditionalProducts_M extends M_Model {
	
	    public $table = 'products_additional_products';
        public $soft_deletes = false;
        public $timestamps = false;

	    public function __construct()
	    {
	    	parent::__construct();
	    }

        public function findByProductIds($id)
        {
            return $this->where_in('product_id', $id)->getAll();
        }
	}    
?>
