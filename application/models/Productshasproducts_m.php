<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductsHasProducts_M extends M_Model {
	
	    public $table = 'products_has_products';
	    public $soft_deletes = false;
        public $timestamps = false;

	    public function __construct()
	    {
	    	parent::__construct();
	    }
	}    
?>
