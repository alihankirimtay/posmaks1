<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PointOfSales_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    // public function index($location)
    // {   
    //     $this->db->select('
    //         sales.id,
    //         sales.ticket,
    //         sales.created ,
    //         sales.amount ,
    //         sales.discount_amount ,
    //         sales.discount_type ,
    //         l.title as location,
    //         u.username as operator
    //     ');
    //     $this->db->join('locations as l' , 'l.id = sales.location_id' , 'left');
    //     $this->db->join('users as u' , 'u.id = sales.user_id' , 'left');
    //     return $this->db->get('sales')->result();
    // }

    // public function add()
    // {
    //     /**
    //         Step 1 Kontrollü;
            
    //         Ticket
    //         Product ID
    //         Location ID
    //     */
        
    //     $step1 = $this->SaleControl(1 , TRUE);
        

    //     // Step 1 Hata messajı 

    //     if($step1['result'] == 0):
    //         messageAJAX('error' , $step1['message']);
    //     endif;

    //     /**
    //         Step 2 Kontrolü;

    //         Manager Session
    //         İndirim Tipi
    //         İndirim Miktarı
    //         Ödeme Tipi
    //     */

    //     $step2 = $this->SaleControl(2 , TRUE);

    //     // Step 2 Hata Mesajı
    //     if($step2['result'] == 0):
    //         messageAJAX('error' , $step2['message']);
    //     endif;


    //     /* Sales tablo verileri */

    //     $sales = array(
    //         'location_id'       => $this->input->post('location_id'),
    //         'user_id'           => $this->user->id,
    //         'payment_type_id'   => $this->input->post('payment'),
    //         'ticket'            => $this->input->post('ticket'),
    //         'amount'            => $this->input->post('amounth'),
    //         'discount_amount'   => $this->input->post('disciont_amounth'),
    //         'discount_type'     => ($this->input->post('discount_type')== '$' ? 'amount' : 'percent'),
    //         'created'           => _date(),
    //         'active'            => 1,
    //         'sale_type_id'      => 1, // Satış
    //         'payment_date'      => date('Y-m-d'), // Satış tarihi
    //     );
        



        
    //     $this->db->insert('sales' , $sales);

    //     $insert_id = $this->db->insert_id();


    //     /* Sales Has Product Tablo Verileri */
    //     foreach($this->input->post('product') as $product):
    //         $data = array(
    //             'sale_id' => $insert_id,
    //             'product_id' => $product['product_id'],
    //             'quantity' => $product['each'],
    //             'created'  => _date(),
    //             'active'   => 1
    //         );
            
    //         $sales['products'][] = $data;
    
    //         $this->db->insert('sale_has_products' , $data);
        
    //     endforeach;


    //     /* Eğer ürün tipi paket ise içinde dijital ürün varmı kontrol ediyor */

    //     if($step1['product_type'] == 'package'):

    //         $this->db->select('p.id , p.digital');
    //         $this->db->where('pp.package_id' , $this->input->post('product_id'));
    //         $this->db->join('products p' , 'p.id = pp.product_id');

    //         $pack = $this->db->get('products_has_products as pp')->result();

    //         foreach($pack as $value):

    //             /* Eğere paket içerisindeki ürün dijital ürün ise şifre oluşturuyor */
                
    //             if($value->digital == 1):

    //                 $values = array(
    //                     'sale_id' => $insert_id,
    //                     'user_id' => $this->user->id,
    //                     'key'     => rand(1000,9999),
    //                     'active'  => 1
    //                     );

    //             $this->db->insert('digital_keys' , $values);

    //             endif;

    //         endforeach;

    //     /**
    //      Ürün paket değil ise ürünün dijital olup olmadığını kontrol ediyor
    //      Ürün dijital ürün ise 4 haneli şifre oluşturup kaydediyor 

    //     */

    //     elseif($step1['digital'] == 1):

    //         $values = array(
    //             'sale_id' => $insert_id,
    //             'user_id' => $this->user->id,
    //             'key'     => rand(1000,9999),
    //             'active'  => 1
    //         );

    //         $this->db->insert('digital_keys' , $values);

    //     endif;

    //     /**
    //         Eğer satışa indirim uygulanmış ise oluşturulan 
    //         veriyi veritabanından oluşturulan session temizleniyor
    //     */

    //     if(isset($step2['session'])):

    //     $this->db->where('session' , $step2['session']);
    //     $this->db->delete('discount_keys');

    //     endif;

    //     /**
    //         Herangibi bir hata yok ise fatura sayfasına yönlendiriliyor
    //     */
    //         messageAJAX('success' , logs_($this->user->id , 'New Sales Created' , $sales) , $insert_id);
        
    // }


    // public function SaleControl($step , $control = FALSE)
    // {
    //     if($step == 1):

    //         $ticket = $this->input->post('ticket');
    //         $location = $this->input->post('location_id');
    //         $products = $this->input->post('product');



    //         if($ticket):

    //             $TicketControl = $this->db
    //                                   ->like('title' , $ticket , 'after')
    //                                   ->get('photos')->num_rows();
    //         endif;

    //         if($TicketControl < 1):

    //             messageAJAX('error' , 'Ticket Not Found');
    //         endif;


    //         if($location):

    //             $this->db->where('id' , $location);
    //             $this->db->join('user_has_locations as ul', 'ul.location_id IN('.implode(',' , $this->user->locations).')');
    //             $LocationControl = $this->db->get('locations');

    //             if($LocationControl->num_rows() < 1):
    //                 messageAJAX('error' , 'Location Not Found');
    //             endif;

    //         endif;



            
    //         foreach($products as $product):
    //             if($product['product_id']):
            
    //                 $this->db->select('p.id , p.digital , p.type , lp.price');
    //                 $this->db->where('p.id' , $product['product_id']);
    //                 $this->db->join('location_has_products as lp' , 'lp.location_id = '.$location.' AND product_id = '.$product['product_id']);
    
    //                 if($this->user->role_id != 1):
    //                     $this->db->where('p.id' , $product['product_id']);
    //                 endif;
                       
    //                 $ProductControl =  $this->db->get('products as p');
            
    //             endif;
            
            

    //             if($ProductControl->num_rows() < 1):
    
    //                 messageAJAX('error' , 'Product Not Found');
    //             endif;
    //         endforeach;

    //         $data = array(
    //             'result'        => 1,
    //             'product_id'    => $ProductControl->row()->id,
    //             'prodcut_price' => $ProductControl->row()->price,
    //             'product_type'  => $ProductControl->row()->type,
    //             'digital'       => $ProductControl->row()->digital,
    //             'location_id'   => $LocationControl->row()->id
    //         );
            
    //         if($control === FALSE):
    //             echo json_encode($data);
    //         else:
    //             return $data;
    //         endif;


               


    //     elseif($step == 2):

    //         $payment = $this->input->post('payment');
    //         $session = $this->input->post('session');

    //         if(isset($session)):

    //             $sessionControl = $this->db->where('session' , $session)->get('discount_keys')->num_rows();

    //             if($sessionControl < 1):
    //                  messageAJAX('error' , 'Discount key not found');
    //             endif;

    //             $data['session'] = $session;

    //         endif;


    //         $paymentControl = $this->db->where('id' , $payment)->get('payment_types')->num_rows();

    //         $data['result']  = 1;
    //         $data['payment'] = $payment;
            

    //         if($paymentControl < 1):
    //             messageAJAX('error' , 'Payment type not found');
    //         endif;

    //         if($control === FALSE):
    //             echo json_encode($data);
    //         else:
    //             return $data;
    //         endif;

    //     endif;


        
    // }

    // public function managerLogin()
    // {
    //     $location_id = $this->input->post('location_id');
    //     $pass  = sha1( $this->input->post('password', TRUE) . $this->config->config['salt'] );
    //     $this->db->select('users.id');
    //     $this->db->where('password' , $pass);
    //     $this->db->where('role_id' , 2);
    //     $this->db->where('users.active', 1);
    //     $this->db->join('user_has_locations as uhl' , 'uhl.user_id = users.id AND uhl.location_id ='.$location_id);
    //     $this->db->join('roles' , 'roles.id = 2 AND roles.active = 1');
    //     $control = $this->db->get('users' , 1);
        
        
    //     if($control->num_rows() > 0):

    //        $values = array(
    //         'user_id'   => $this->input->post('email'),
    //         'session'  => sha1(uniqid())
    //         );

    //         $this->db->insert('discount_keys' , $values);

    //         messageAJAX('success' , $values['session']);
    //     else:
    //          messageAJAX('error' , 'Password Wrong');
            
    //     endif;
        
    // }


    // public function payment_types()
    // {
    //     return $this->db->where('active' , 1)->get('payment_types')->result();
        
    // }

    // public function invoice_data($id)
    // {   
    //     $data = [];
    //     $this->db->select('
    //         sales.ticket,
    //         sales.payment_date,
    //         sales.amount,
    //         sales.discount_type,
    //         sales.discount_amount,
    //         shp.quantity,
    //         dk.key,
    //         dk.active as key_status,
    //         p.title,
    //         p.desc,
    //         p.digital,
    //         lhp.price,
    //         it.header,
    //         it.footer,
    //         it.logo,
    //         l.currency');
    //     $this->db->where('sales.id' , $id);
    //     $this->db->join('sale_has_products as shp' , 'shp.sale_id = '.$id , 'left');
    //     $this->db->join('products as p' ,'p.id = shp.product_id' , 'left');
    //     $this->db->join('invoice_template as it' , 'it.location_id = sales.location_id' , 'left');
    //     $this->db->join('digital_keys as dk' , 'dk.sale_id = '.$id , 'left');
    //     $this->db->join('location_has_products lhp' , 'lhp.location_id = sales.location_id AND lhp.product_id = shp.product_id' , 'left');
    //     $this->db->join('locations as l' , 'l.id = lhp.location_id');
    //     $data = $this->db->get('sales')->result();

    //     foreach($data as $key => $val) {

    //         $products[] = array(
    //             'title' => $val->title,
    //             'desc'  => $val->desc,
    //             'each'  => $val->quantity,
    //             'digital' => $val->digital,
    //             'price'   => $val->price
    //         );

    //         $data = array(
    //             'ticket' => $val->ticket,
    //             'date'   => date( 'd.M.Y' , strtotime($val->payment_date)),
    //             'sub_total' => $val->amount,
    //             'discount_type' => $val->discount_type,
    //             'key'           => $val->key,
    //             'key_status'    => $val->key_status,
    //             'discount_amount' => $val->discount_amount,
				// 'currency'		  => $val->currency,
    //             'products'  => $products,
    //             'logo'      => $val->logo,
    //             'header'    => $val->header,
    //             'footer'    => $val->footer,

    //         );

        
    //     }

    //     return $data;

    // }

    

}

/* End of file Sales_model.php */
/* Location: ./application/models/Sales_model.php */
