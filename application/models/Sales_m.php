<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Sales_M extends M_Model {
	
	    public $table = 'sales';

	    public function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['products'] = [
                'foreign_model' => 'Saleproducts_M',
                'foreign_table' => 'sale_has_products',
                'foreign_key'   => 'sale_id',
                'local_key'     => 'id'
            ];

            $this->has_many['related_sales'] = [
                'foreign_model' => 'Salescari_M',
                'foreign_table' => 'sales_cari',
                'foreign_key'   => 'sale_id',
                'local_key'     => 'id'
            ];

            $required_if_status_completed = ($this->input->post('status') == 'completed') ? 'required|' : null;

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'pos_id' => [
                        'field' => 'pos_id',
                        'label' => 'Kasa',
                        'rules' => 'required|trim|search_table[pos.id]',
                    ],
                    'point_id' => [
                        'field' => 'point_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[points.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'zone_id' => [
                        'field' => 'zone_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[zones.id]',
                    ],
                    'sale_type_id' => [
                        'field' => 'sale_type_id',
                        'label' => 'sale_type_id',
                        'rules' => 'required|trim',
                    ],
                    'payment_type_id' => [
                        'field' => 'payment_type_id',
                        'label' => 'payment_type_id',
                        'rules' => $required_if_status_completed . 'trim|search_table[payment_types.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'Personel',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'completer_user_id' => [
                        'field' => 'completer_user_id',
                        'label' => 'Personel (Hesap)',
                        'rules' => $required_if_status_completed . 'trim|search_table[users.id]',
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Müşteri',
                        'rules' => 'trim|search_table[customers.id]',
                    ],
                    'courier_id' => [
                        'field' => 'courier_id',
                        'label' => 'Kurye',
                        'rules' => 'trim|search_table[users.id]',
                    ],
                    'return_id' => [
                        'field' => 'return_id',
                        'label' => 'İade',
                        'rules' => 'trim|search_table[sales.id]',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'payment_date',
                        'rules' => $required_if_status_completed . 'trim|is_date[Y-m-d H:i:s]',
                    ],
                    'service_type' => [
                        'field' => 'service_type',
                        'label' => 'service_type',
                        'rules' => 'required|trim',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Toplam',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'subtotal' => [
                        'field' => 'subtotal',
                        'label' => 'Ara Toplam',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'taxtotal' => [
                        'field' => 'taxtotal',
                        'label' => 'Toplam Vergi',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'gift_amount' => [
                        'field' => 'gift_amount',
                        'label' => 'Toplam İkram',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'interim_payment_amount' => [
                        'field' => 'interim_payment_amount',
                        'label' => 'Toplam Ara Ödeme',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'change' => [
                        'field' => 'change',
                        'label' => 'Para Üstü',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'discount_amount' => [
                        'field' => 'discount_amount',
                        'label' => 'discount_amount',
                        'rules' => 'trim',
                    ],
                    'discount_type' => [
                        'field' => 'discount_type',
                        'label' => 'discount_type',
                        'rules' => 'trim|in_list[amount,percent]',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Not',
                        'rules' => 'trim|xss_clean',
                    ],
                    'status' => [
                        'field' => 'status',
                        'label' => 'Sipariş Durumu',
                        'rules' => 'required|trim|in_list[pending,completed,cancelled]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'pos_id' => [
                        'field' => 'pos_id',
                        'label' => 'Kasa',
                        'rules' => 'required|trim|search_table[pos.id]',
                    ],
                    'point_id' => [
                        'field' => 'point_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[points.id]',
                    ],
                    'floor_id' => [
                        'field' => 'floor_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[floors.id]',
                    ],
                    'zone_id' => [
                        'field' => 'zone_id',
                        'label' => 'Masa',
                        'rules' => 'required|trim|search_table[zones.id]',
                    ],
                    'sale_type_id' => [
                        'field' => 'sale_type_id',
                        'label' => 'sale_type_id',
                        'rules' => 'required|trim',
                    ],
                    'payment_type_id' => [
                        'field' => 'payment_type_id',
                        'label' => 'payment_type_id',
                        'rules' => $required_if_status_completed . 'trim|search_table[payment_types.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'Personel',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'completer_user_id' => [
                        'field' => 'completer_user_id',
                        'label' => 'Personel (Hesap)',
                        'rules' => $required_if_status_completed . 'trim|search_table[users.id]',
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Müşteri',
                        'rules' => 'trim|search_table[customers.id]',
                    ],
                    'courier_id' => [
                        'field' => 'courier_id',
                        'label' => 'Kurye',
                        'rules' => 'trim|search_table[users.id]',
                    ],
                    'return_id' => [
                        'field' => 'return_id',
                        'label' => 'İade',
                        'rules' => 'trim|search_table[sales.id]',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'payment_date',
                        'rules' => $required_if_status_completed . 'trim|is_date[Y-m-d H:i:s]',
                    ],
                    'service_type' => [
                        'field' => 'service_type',
                        'label' => 'service_type',
                        'rules' => 'required|trim',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Toplam',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'subtotal' => [
                        'field' => 'subtotal',
                        'label' => 'Ara Toplam',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'taxtotal' => [
                        'field' => 'taxtotal',
                        'label' => 'Toplam Vergi',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'gift_amount' => [
                        'field' => 'gift_amount',
                        'label' => 'Toplam İkram',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'interim_payment_amount' => [
                        'field' => 'interim_payment_amount',
                        'label' => 'Toplam Ara Ödeme',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'change' => [
                        'field' => 'change',
                        'label' => 'Para Üstü',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'discount_amount' => [
                        'field' => 'discount_amount',
                        'label' => 'discount_amount',
                        'rules' => 'trim',
                    ],
                    'discount_type' => [
                        'field' => 'discount_type',
                        'label' => 'discount_type',
                        'rules' => 'trim|in_list[amount,percent]',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Not',
                        'rules' => 'trim|xss_clean',
                    ],
                    'status' => [
                        'field' => 'status',
                        'label' => 'Sipariş Durumu',
                        'rules' => 'required|trim|in_list[pending,completed,cancelled]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function rulesPaymetTypes()
        {
            $payment_types = $this->db->where(['active' => 1])->get('payment_types')->result();

            foreach ($payment_types as $payment_type) {

                $this->rules['insert'][$payment_type->alias] = 
                $this->rules['update'][$payment_type->alias] = [
                    'field' => $payment_type->alias,
                    'label' => $payment_type->title,
                    'rules' => 'required|trim|is_decimal',
                ];
            }

            return $this;
        }

        public function findSaleWithProducts($where = [], $configs = [])
        {
            $this->db->limit(1);
            $sale = $this->_handleSalesWithProducts($where, $configs);
            if (!$sale)
                return false;

            return $sale[0];
        }

        public function findSalesWithProducts($where = [], $configs = [])
        {
            $sales = $this->_handleSalesWithProducts($where, $configs);
            if (!$sales)
                return [];

            return $sales;
        }
        
        private function _handleSalesWithProducts($where, $configs)
        {
            $sale_products =
            $sale_package_products =
            $sale_products_stocks =
            $sale_packages_stocks =
            $sale_products_notes = [];

            $sales = $this->where($where)->get_all();
            if (!$sales)
                return false;

            if (isset($configs['sale_has_products'])) {
                if (isset($configs['sale_has_products']['fields'])) {
                    $this->db->select($configs['sale_has_products']['fields']);
                }
                if (isset($configs['sale_has_products']['where'])) {
                    $this->db->where($configs['sale_has_products']['where']);
                }
                if (isset($configs['sale_has_products']['raw_where'])) {
                    $this->db->where($configs['sale_has_products']['raw_where'], null, false);
                }
            }

            $this->groupWhereIn($sales, 'sale_id', 'id');
            $sale_products = $this->db
            ->where('active !=', 3)
            ->get('sale_has_products')
            ->result();

            // Handle Products
            if ($sale_products) {

                $product_ids = [];
                $package_ids = [];
                foreach ($sale_products as $row) {
                    if ($row->type == 'package') {
                        $package_ids[] = $row->id;
                    } else if ($row->type == 'product') {
                        $product_ids[] = $row->id;
                    }
                }
                
                // Handle Packages
                if ($package_ids) {

                    if (isset($configs['package_has_products'])) {
                        if (isset($configs['package_has_products']['fields'])) {
                            $this->db->select($configs['package_has_products']['fields']);
                        }
                        if (isset($configs['package_has_products']['where'])) {
                            $this->db->where($configs['package_has_products']['where']);
                        }
                    }

                    $this->groupWhereIn($package_ids, 'package_id');
                    $sale_package_products = $this->db
                    ->where('active !=', 3)
                    ->get('package_has_products')
                    ->result();

                    // Handle Stocks in sale packages
                    if ($sale_package_products) {

                        $this->groupWhereIn($sale_package_products, 'sale_package_id', 'id');
                        $sale_packages_stocks = $this->db
                        ->select('*, (SELECT title FROM stocks WHERE id = stock_id LIMIT 1) As stock_name')
                        ->get('sale_packages_stocks')
                        ->result();

                        foreach ($sale_package_products as &$sale_package_product) {
                            
                            $sale_package_product->stocks = [];

                            // Stocks in package
                            foreach ($sale_packages_stocks as $key => $sale_package_stock) {
                                if ($sale_package_stock->sale_package_id != $sale_package_product->id){
                                    continue;
                                }

                                $sale_package_product->stocks[$sale_package_stock->stock_id] = $sale_package_stock;
                                unset($sale_packages_stocks[$key]);
                            }
                        }
                        unset($sale_package_product);

                    }
                }

                // Handle Stocks in sale products
                if ($product_ids) {

                    $this->groupWhereIn($product_ids, 'sale_product_id');
                    $sale_products_stocks = $this->db
                    ->select('*, (SELECT title FROM stocks WHERE id = stock_id LIMIT 1) As stock_name')
                    ->get('sale_products_stocks')
                    ->result();
                }

                // Handle Prodcut and Package
                if ($package_ids || $product_ids) {
                    
                    // Product notes
                    if (isset($configs['sale_product_notes'])) {

                        if (isset($configs['sale_product_notes']['fields'])) {
                            $this->db->select($configs['sale_product_notes']['fields']);
                        }
                        if (isset($configs['sale_product_notes']['where'])) {
                            $this->db->where($configs['sale_product_notes']['where']);
                        }

                        $this->groupWhereIn(array_merge($package_ids, $product_ids), 'sale_product_id');
                        $sale_products_notes = $this->db
                        ->select('id,sale_product_id')
                        ->get('sale_product_notes')
                        ->result();
                    }
                }
            }
        

            // Hadnle stocks in sale products
            foreach ($sale_products as &$sale_product) {
                
                $sale_product->additional_products = [];

                if ($sale_product->type == 'product') {
                    
                    $sale_product->stocks = [];

                    // Stocks in product
                    foreach ($sale_products_stocks as $key => $sale_product_stock) {
                        if ($sale_product_stock->sale_product_id != $sale_product->id){
                            continue;
                        }

                        $sale_product->stocks[$sale_product_stock->stock_id] = $sale_product_stock;
                        unset($sale_products_stocks[$key]);
                    }

                    // Product notes
                    if (isset($configs['sale_product_notes'])) {

                        $sale_product->notes = [];

                        // Notes in product
                        foreach ($sale_products_notes as $sale_product_note) {
                            if ($sale_product_note->sale_product_id != $sale_product->id){
                                continue;
                            }
                            $sale_product->notes[$sale_product_note->id] = $sale_product_note;
                        }
                    }
                }

                
                else if ($sale_product->type == 'package') {

                    $sale_product->products = [];

                    // Products in package
                    foreach ($sale_package_products as $key2 => $sale_package_product) {
                        if ($sale_package_product->package_id != $sale_product->id){
                            continue;
                        }

                        $sale_product->products[] = $sale_package_product;
                        unset($sale_package_products[$key2]);
                    }

                    // Product notes
                    if (isset($configs['sale_product_notes'])) {

                        $sale_product->notes = [];
                        
                        // Notes in product
                        foreach ($sale_products_notes as $sale_product_note) {
                            if ($sale_product_note->sale_product_id != $sale_product->id){
                                continue;
                            }
                            $sale_product->notes[$sale_product_note->id] = $sale_product_note;
                        }
                    }
                }
            }
            unset($sale_product);

            // Move the additional products into the parent product.
            $_sale_products = $sale_products;
            foreach ($sale_products as &$sale_product) {
                if (!$sale_product->additional_sale_product_id) {
                    foreach ($_sale_products as $key => $_sale_product) {
                        if ($sale_product->id == $_sale_product->additional_sale_product_id) {
                            $sale_product->additional_products[$_sale_product->product_id] = $_sale_product;
                            unset($_sale_products[$key]);
                        }
                    }
                }
            }
            unset($sale_product, $_sale_products);


            foreach ($sales as &$sale) {

                $sale->products = [];

                foreach ($sale_products as $key => $sale_product) {

                    if ($sale_product->additional_sale_product_id) { // Unset additional products from main porducts list
                        unset($sale_products[$key]);
                        continue;
                    }
                    if ($sale_product->sale_id != $sale->id) {
                        continue;
                    }

                    $sale->products[] = $sale_product;
                    unset($sale_products[$key]);
                }
            }
            unset($sale);

            return $sales;
        }

        public function getTransactionsWithSales($customer_id, $date)
        {
            $sales_cari = $this->db
            ->select('S.payment_date, SC.amount, 4 as type, "" as desc, SC.cancelled')
            ->where([
                'SC.customer_id' => $customer_id,
                'S.status' => "completed",
                'S.payment_date >=' => $date[ 'date_from' ],
                'S.payment_date <=' => $date[ 'date_to' ],
            ])
            ->join('sales_cari as SC', 'S.id = SC.sale_id')
            ->get('sales as S')->result();

            return $sales_cari;
        }
	}    
?>
