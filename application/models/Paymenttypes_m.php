<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class PaymentTypes_M extends M_Model {
	
	    public $table = 'payment_types';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->rules = [
                'insert' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'alias' => [
                        'field' => 'alias',
                        'label' => 'Takma isim',
                        'rules' => 'required|trim',
                    ],
                    'icon' => [
                        'field' => 'icon',
                        'label' => 'Simge',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'alias' => [
                        'field' => 'alias',
                        'label' => 'Takma isim',
                        'rules' => 'required|trim',
                    ],
                    'icon' => [
                        'field' => 'icon',
                        'label' => 'Simge',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }
	}    
?>
