<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_Model extends CI_Model {

        const sales = 'sales';
        const salehasproducts = 'sale_has_products';
        const expenses = 'expenses';
        const photos = 'photos';
        const products = 'products';
        const locationhasproducts = 'location_has_products';
        const printers = 'printers';
        const logs = 'logs';
        const paxnegs = 'paxnegs';
        const locationlosts = 'location_losts';
        const users = 'users';


        function __construct()
        {
            parent::__construct();
        }

        function sales($select = 'S.id, S.return_id')
        {
            $this->db->select($select);
            $this->db->where('S.active !=', 3);
            return $this->db->get(self::sales.' S')->result_array();
        }



        function expenses($select = 'E.id')
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::expenses.' E')->result_array();
        }

        function paxnegs($select = "SUM(pax) as pax, SUM(negs) as negs")
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::paxnegs.' P')->row();
        }

        function paxnegs_all($select = "SUM(pax) as pax, SUM(negs) as negs")
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::paxnegs.' P')->result_array();
        }

        function total_ticket_count($select = 'P.id')
        {
            $this->db->select($select);
            $this->db->group_by('P.title');
            return $this->db->get(self::photos.' P')->result_array();
        }

        function total_sale_count($select = 'S.id')
        {       
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::sales.' S')->result_array();
        }

        // function products($select = 'P.id, LHP.price')
        // {       
        //     $this->db->join(self::locationhasproducts.' LHP', 'LHP.product_id = P.id');
        //     $this->db->where('LHP.active !=', 3);

        //     $this->db->group_by('P.id');
        //     $this->db->where('P.active !=', 3);
        //     return $this->db->get(self::products.' P')->result_array();
        // }

        function products($select = 'P.id, P.price')
        {       
            $this->db->where('P.active !=', 3);
            return $this->db->get(self::products.' P')->result_array();
        }

        // function users($select = 'U.id')
        // {
        //     $this->db->where('U.active !=', 3);
        //     return $this->db->get(self::users.' U')->result_array();

        // }

        function usersandlocations($select = 'U.id, U.username') {
           $users = $this->db->select($select)
            ->from('users U')
            ->join('user_has_locations as UHL', 'U.id = UHL.user_id', 'inner')
            ->get()->result_array();
            return $users;

        }

        // function salesandproducts() {
        //     $sales = $this->db->select()
        //             ->from(self::sales.' S')
        //             ->join(self::salehasproducts.' SHP', 's.id=SHP.sale_id', 'inner')
        //             ->get()->result_array();
        //     return $sales;

        // }

        function sold_products($select = 'SHP.id, SHP.title, SHP.price, SHP.tax, SHP.quantity, SHP.bonus, SHP.bonus_type')
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::salehasproducts.' SHP')->result_array();

        }


        function printers($select = 'P.id')
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::printers.' P')->result_array();
        }

        function printer_waste($select = 'SUM(L.unsold) as unsold, SUM(L.waste) as waste, SUM(L.comps) as comps')
        {
            $this->db->select($select);
            $this->db->where('active !=', 3);
            return $this->db->get(self::locationlosts.' L')->row();
        }

        function usb_requests($select = 'L.id')
        {
            $this->db->select($select);
            $this->db->where('L.type', 1);
            return $this->db->get(self::logs.' L')->result_array();
        }

        function FLTR_locaiton($db)
        {
            if($location = (int) isGet('l')) {
                $this->db->where($db.'.location_id', $location);
            } else{
                $this->db->where_in($db.'.location_id', $this->user->locations);
            } 
        }

        function FLTR_date($db, $field)
        {
            if($date = sanitize(isGet('d'))) {
                if($date == 'today'){
                    $this->db->where('DATE('.$db.'.'.$field.') = CURDATE()');
                } else if($date == 'yesterday'){
                    $this->db->where('DATE('.$db.'.'.$field.') = DATE(CURDATE() - INTERVAL 1 DAY)');
                } else if($date == 'thisweek'){
                    $this->db->where('YEARWEEK('.$db.'.'.$field.') = YEARWEEK(CURDATE())');
                } else if($date == 'thismonth'){
                    $this->db->where('YEAR('.$db.'.'.$field.') = YEAR(CURDATE()) AND MONTH('.$db.'.'.$field.') = MONTH(CURDATE())');
                } else if($date == 'thisyear'){
                    $this->db->where('YEAR('.$db.'.'.$field.') = YEAR(CURDATE())');
                } else if($date == 'lastweek'){
                    $this->db->where('YEARWEEK('.$db.'.'.$field.') = YEARWEEK(CURDATE() - INTERVAL 7 DAY)');
                } else if($date == 'lastmonth'){
                    $this->db->where('YEAR('.$db.'.'.$field.') = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH('.$db.'.'.$field.') = MONTH(CURDATE() - INTERVAL 1 MONTH)');
                } else if($date == 'lastyear'){
                    $this->db->where('YEAR('.$db.'.'.$field.') = YEAR(CURDATE() - INTERVAL 1 YEAR)');
                } else if($date == 'custome'){
                    if(isValidDate(isGet('dFrom'))) $this->db->where('DATE('.$db.'.'.$field.') >=', $_GET['dFrom']);
                    if(isValidDate(isGet('dTo')))   $this->db->where('DATE('.$db.'.'.$field.') <=', $_GET['dTo']);
                } else {
                    $this->db->where('DATE('.$db.'.'.$field.') = CURDATE()');
                }
            } else {
                $this->db->where('DATE('.$db.'.'.$field.') = CURDATE()');
            } 
        }
}
?>