<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    public function get_ticket($id)
    {
        $tmp = $this->db->where('location_id' , $id)->get('ticket_template');

        if($tmp->num_rows() > 0):
            return $tmp->row();
        else:
            return NULL;
        endif;
        
    }

    public function save_ticket($id)
    {
        if($this->db->where('location_id' , $id)->get('ticket_template')->num_rows() > 0):

            $values = array(
                'header' => $this->input->post('header'),
                'footer' => $this->input->post('footer')
            );

            $this->db->where('location_id' , $id);
            $this->db->update('ticket_template' , $values);

                logs($this->user->id , 'Bilet güncellendi'.json_encode(array('location_id' => $id)));
                messageAJAX('success' , 'Başarılı');

        else:

            $values = array(
                'location_id' => $id,
                'header'      => $this->input->post('header'),
                'footer'      => $this->input->post('footer')
            );

            if(!$this->db->insert('ticket_template' , $values)):
                messageAJAX('error' , 'Database Error');
            else:
                logs($this->user->id , 'Bilet oluşturldu'.json_encode(array('location_id' => $id)));
                messageAJAX('success' , 'Başarılı');
            endif;

        endif;
    }


    public function get_invoice($id)
    {
        $tmp = $this->db->where('location_id' , $id)->get('invoice_template');

        if($tmp->num_rows() > 0):
            return $tmp->row();
        else:
            return NULL;
        endif;
        
    }

    public function save_invoice($id)
    {
        if($this->db->where('location_id' , $id)->get('invoice_template')->num_rows() > 0):


            $path = 'assets/uploads/invoice_logo/';
            @mkdir(FCPATH . $path);

            $config = [
                'encrypt_name'  => true,
                'upload_path'   => FCPATH . $path,
                'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
            ];

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('image')){
                messageAJAX('error', $this->upload->display_errors());
            }


            $data['image'] = $path . $this->upload->data('file_name');

            $values = array(
                'header' => $this->input->post('header'),
                'footer' => $this->input->post('footer'),
                'logo' => $data['image']
            );

            $this->db->where('location_id' , $id);
            $this->db->update('invoice_template' , $values);

                logs($this->user->id , 'Bilet güncellendi'.json_encode(array('location_id' => $id)));
                messageAJAX('success' , 'Başarılı');

        else:

            $values = array(
                'location_id' => $id,
                'header'      => $this->input->post('header'),
                'footer'      => $this->input->post('footer')
            );

            if(!$this->db->insert('invoice_template' , $values)):
                messageAJAX('error' , 'Database Error');
            else:
                logs($this->user->id , 'Added a new ticket design'.json_encode(array('location_id' => $id)));
                messageAJAX('success' , 'Başarılı');
            endif;

        endif;
    }


    

}

/* End of file Ticket_model.php */
/* Location: ./application/models/Ticket_model.php */
