<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'roles';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('title !=' , 'Admin');
        if ($where !== NULL) {
            if (is_array($where)) {
                foreach ($where as $field=>$value) {
                    $this->db->where($field, $value);
                }
            } else {
                $this->db->where(self::PRI_INDEX, $where);
            }
        }
        $result = $this->db->get()->result();
        if ($result) {
            if ($where !== NULL) {
                return array_shift($result);
            } else {
                return $result;
            }
        } else {
            return [];
        }
    }

    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
        if ($this->db->insert(self::TABLE_NAME, $data)) {
            $insert_id = $this->db->insert_id();

            $data['id'] = $insert_id;

            logs($this->user->id , 'Rol oluşturuldu'.json_encode($data));

            return $insert_id;
        } else {
            return false;
        }
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
            if (!is_array($where)) {
                $where = array(self::PRI_INDEX => $where);
            }
        $this->db->update(self::TABLE_NAME, $data, $where);

        $data['id'] = $where[self::PRI_INDEX];

        logs($this->user->id , 'Rol güncellendi'.json_encode($data));
        return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
        if (!is_array($where)) {
            $where = array(self::PRI_INDEX => $where);
        }

        logs($this->user->id , 'Rol silindi '.json_encode($this->get($where[self::PRI_INDEX])));

        $this->db->delete(self::TABLE_NAME, $where);
        return $this->db->affected_rows();
    }


    public function permissions($id = NULL)
    {   
        if($id == NULL):
            return $this->db ->where('active',1) ->order_by('page' , 'DESC')->get('permissions')->result();
        else:
            return $this->db->where('role_id' , $id)->get('role_has_permissions')->result();

        endif;
        
    }

    public function save_permissions($values , $id)
    {
        $this->db
             ->where('role_id' , $id)
             ->delete('role_has_permissions');
        if($values != NULL):
            foreach($values as $value):

                $this->db->insert('role_has_permissions' , $value);
            endforeach;
        endif;

        return TRUE;
    }
}
        
