<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Floors_Model extends CI_Model {
	
	    const floors = 'floors';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	if($location && !in_array($location, $this->user->locations)) error_('access');

	    	$this->db->select('P.*, (SELECT title FROM locations WHERE id = P.location_id LIMIT 1) AS location');
	    	if($location) {

	    		$this->db->where('P.location_id', $location);
	    	}
    		else {

    			$this->db->where_in('P.location_id', $this->user->locations);
    		}

    		$this->db->where('P.active !=', 3);
	    	return $this->db->get(self::floors.' P')->result();	    	
	    }

	    function add()
	    {
	    	$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['created']  	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', $values));
            }

            if( $this->db->insert(self::floors, $values) ) {

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Kat oluşturuldu. ', $values));
            }
	    }

	    function edit($id)
	    {
    		$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']  = (int) $this->input->post('location');
            $values['modified'] = _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', ['id'=>$id]+$values));
            }

       		$this->db->where('id', $id);
            if( $this->db->update(self::floors, $values) ) {

            	messageAJAX('success', logs_($this->user->id, 'Kat güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete($id, $data)
	    {
	    	$this->db->where('id', $id);
            if( $this->db->update(self::floors, ['modified' => _date(), 'active' => 3]) ) {

            	messageAJAX('success', logs_($this->user->id, 'Kat silindi. ', $data));
            }
	    }
	}    
?>
