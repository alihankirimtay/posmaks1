<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Transactions_M extends M_Model {
	
        public $table = 'transactions';
        private $type = '';

	    function __construct()
	    {
            $this->before_create = ['_setPaymentDate'];
            $this->before_update = ['_setPaymentDate'];

            $this->has_one['related_transaction'] = [
                'foreign_model' => 'Transactions_M',
                'foreign_table' => 'transactions',
                'foreign_key'   => 'id',
                'local_key'     => 'related_transaction_id'
            ];


            $this->rules = [
                'insert' => [
                    'bank_id' => [
                        'field' => 'bank_id',
                        'label' => 'Banka',
                        'rules' => 'trim|search_table[banks.id]' .$this->typeIs('bank'),
                    ],
                    'pos_id' => [
                        'field' => 'pos_id',
                        'label' => 'Kasa',
                        'rules' => 'trim|search_table[pos.id]' .$this->typeIs('pos'),
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Ünvan',
                        'rules' => 'trim|search_table[customers.id]' .$this->typeIs('customer'),
                    ],
                    'related_transaction_id' => [
                        'field' => 'related_transaction_id',
                        'label' => 'Transaction',
                        'rules' => 'trim',
                    ],
                    'type_id' => [
                        'field' => 'type_id',
                        'label' => 'Type Id',
                        'rules' => 'trim',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Tutar',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'increment' => [
                        'field' => 'increment',
                        'label' => 'Artış',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'date' => [
                        'field' => 'date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                ],
                'update' => [
                    'bank_id' => [
                        'field' => 'bank_id',
                        'label' => 'Banka',
                        'rules' => 'trim|search_table[banks.id]' .$this->typeIs('bank'),
                    ],
                    'pos_id' => [
                        'field' => 'pos_id',
                        'label' => 'Kasa',
                        'rules' => 'trim|search_table[pos.id]' .$this->typeIs('pos'),
                    ],
                    'customer_id' => [
                        'field' => 'customer_id',
                        'label' => 'Ünvan',
                        'rules' => 'trim|search_table[customers.id]' .$this->typeIs('customer'),
                    ],
                    'related_transaction_id' => [
                        'field' => 'related_transaction_id',
                        'label' => 'Transaction',
                        'rules' => 'trim',
                    ],
                    'type_id' => [
                        'field' => 'type_id',
                        'label' => 'Type Id',
                        'rules' => 'trim',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Tutar',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'increment' => [
                        'field' => 'increment',
                        'label' => 'Artış',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'date' => [
                        'field' => 'date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    
                ]
            ];

	    	parent::__construct();
        }
        

        public function setType($type)
        {
            if (!in_array((string) $type, ['bank', 'pos', 'customer'])) {
                messageAjax('error', 'invalid type');
            }
            $this->type = $type;

            return $this;
        }

        public function typeIs($type)
        {
            return $this->type == $type ? "|required" : "";
        }

        public function _setPaymentDate($data)
        {
            if ($this->input->post('date')) {
                $data['date'] = null;
                $data['date'] = dateConvert2($this->input->post('date'), 'server', dateFormat());
                return $data;
            }
        }

        public function getTypes() {

            $types = [
                [
                    'id' => 1,
                    'title' => 'Ödeme',
                    'label' => 'label-success' 
                ],
                [
                    'id' => 2,
                    'title' => 'Tahsilat',
                    'label' => 'label-warning'
                ],
                [
                    'id' => 3,
                    'title' => 'Gider Ödemesi',
                    'label' => 'label-info' 
                ],
                [
                    'id' => 4,
                    'title' => 'Satış',
                    'label' => 'label-danger'
                ],
                [
                    'id' => 5,
                    'title' => 'Transfer',
                    'label' => 'label-danger'
                ],
                [
                    'id' => 6,
                    'title' => 'Açılış Miktarı',
                    'label' => 'label-danger'
                ]
            ];

            return $types;

        }

        public function getAllTransactionsResource($customer_id) {

           

        }


	}    
?>
