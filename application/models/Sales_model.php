<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Sales_Model extends CI_Model {
	
        const sales = 'sales';
        const saletypes = 'sale_types';
        const salehasproducts = 'sale_has_products';
        const saleattachments = 'sale_attachments';
        const users = 'users';
        const paymenttypes = 'payment_types';
        const locationhasproducts = 'location_has_products';
        const photos = 'photos';


	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index($location)
        {
            $location = (int) $location;

            if($location && !in_array($location, $this->user->locations)) error_('access');

            $this->db->select('S.*');
            $this->db->select('(SELECT title FROM locations WHERE id = S.location_id LIMIT 1) AS location');
            // $this->db->select('(SELECT title FROM '.self::saletypes.' WHERE id = S.sale_type_id LIMIT 1) AS type');
            $this->db->select('(SELECT username FROM '.self::users.' WHERE id = S.user_id LIMIT 1) AS user');
            $this->db->select('(SELECT title FROM '.self::paymenttypes.' WHERE id = S.payment_type_id LIMIT 1) AS payment_method');

            if($location) $this->db->where('S.location_id', $location);
            else          $this->db->where_in('S.location_id', $this->user->locations);

            return $this->db->get(self::sales.' S')->result();
        }

        public function invoice_data($id = FALSE)
        {   
            if (!$id = (int) $id) {
                return FALSE;
            }

            $data['sale'] = $this->db
            ->where('id', $id)
            ->get('sales', 1)->row();

            if (! $data['sale']) {
                return false;
            }

            // $data['location']['tax']      = $tax      = $this->user->locations_array[ $data['sale']->location_id ]['tax'];
            $data['location']['currency'] = $currency = $this->user->locations_array[ $data['sale']->location_id ]['currency'];
            $data['sale']->sub_total      = $data['sale']->subtotal; //round($data['sale']->amount / ($tax / 100 + 1), 2);
            $data['sale']->tax            = $data['sale']->amount - $data['sale']->sub_total;


            $data['products'] = $this->db
            ->select('SHP.*, (SELECT title FROM products WHERE id = SHP.product_id LIMIT 1) as title')
            ->select('(SELECT tax_inclusive FROM location_has_products WHERE product_id = SHP.product_id 
                AND location_id = (SELECT location_id FROM sales WHERE id = SHP.sale_id LIMIT 1)
                LIMIT 1) as tax_inclusive')
            ->where('SHP.sale_id', $id)
            ->get('sale_has_products SHP')->result();

            $tmp = NULL;
            if ($data['products']) {

                foreach ($data['products'] as $key => $product) {

                    $price = ($product->tax_inclusive)
                    ? $product->price - $product->price * $product->tax / 100 
                    : $product->price;
                    $price = number_format(round($price, 2), 2);

                    $tmp[] = [
                        'id'         => $product->id,
                        'product_id' => $product->product_id,
                        'no'         => $key + 1,
                        'title'      => $product->title,
                        'quantity'   => $product->quantity,
                        'price'      => $price,
                        'total'      => number_format(round($price * $product->quantity, 2), 2)
                    ];
                }

                $data['products'] = $tmp;
            }

            $data['usbs'] = $this->db
            ->where('sale_id', $id)
            ->get('digital_keys')->result();

            $data['template'] = $this->db
            ->where('location_id', $data['sale']->location_id)
            ->get('invoice_template', 1)->row();

            return $data;
        }

	    function add()
	    {
            if ($this->input->is_ajax_request()) {


                $products             = $this->input->post('products[]');
                $payments             = $this->input->post('payments[]');
                $location             = $this->input->post('location');
                $pos                  = $this->input->post('pos');
                $point                = $this->input->post('point');
                $desc                 = $this->input->post('desc', TRUE);
                $payment_date         = dateConvert($this->input->post('payment_date'), 'server');
                $created              = _date();
                $digital_keys         = [];
                $payment_type_id      = 0;


                if (! in_array($location, $this->user->locations)) {
                    messageAJAX('error', 'Geçersiz restoran.');
                }

                $this->db->where('active', 1);
                $this->db->where('id', $pos);
                $this->db->where('location_id', $location);
                if (!$this->db->get('pos', 1)->row()) {
                    messageAJAX('error', 'Geçersiz kasa.');
                }

                $this->db->where('active', 1);
                $this->db->where('id', $point);
                $this->db->where('location_id', $location);
                if (!$this->db->get('points', 1)->row()) {
                    messageAJAX('error', 'Geçersiz masa.');
                }


                $data = $this->_calculate($location, (array) $products, (array) $payments);


                // PRODUCTS
                foreach ($products as $product => $quantity) {

                    $this->db->where_in('P.id', (int)$product);
                    $this->db->limit(1);
                    $_products = $this->Products_model->index( $location )
                    OR messageAJAX('error', 'Geçersiz ürün.');

                    $_product = $_products[ 0 ];
                    $quantity = is_numeric($quantity) ? $quantity : 1;

                    // COUNT USB PRODUCTS
                    if (in_array($_product->type, ['package', 'ves'])) {

                        $package_has_products = $this->db
                            ->select('P.*')
                            ->where('PHP.package_id', $_product->id)
                            ->where('P.type', 'product')
                            ->join('products P', 'P.id = PHP.product_id')
                        ->get('products_has_products PHP')->result();

                        $data[ 'package_has_products' ][ $product ] = NULL;
                        foreach ($package_has_products as $key => $has_product) {
                            

                            // PACKAGE CONTENT PREPARING
                            $data[ 'package_has_products' ][ $product ][] = [
                                'package_id' => NULL,
                                'product_id' => $has_product->id,
                                'quantity'   => $quantity * 1,
                                'price'      => $has_product->price,
                                'content'    => $has_product->content,
                                'digital'    => $has_product->digital,
                                'is_video'   => $has_product->is_video,
                                'title'      => $has_product->title,
                                'created'    => $created,
                                'active'     => 1,
                                'tax'        => $has_product->tax,
                            ];  
                        }

                    }

                    // PREPARE SALE_HAS_PRODUCTS ARRAY
                    $data[ 'sale_has_products' ][ $product ] = [
                        'sale_id'    => 0,
                        'product_id' => $_product->id,
                        'quantity'   => $quantity,
                        'price'      => $_product->price,
                        'content'    => $_product->content,
                        'digital'    => $_product->digital,
                        'is_video'   => $_product->is_video,
                        'title'      => $_product->title,
                        'type'       => $_product->type,
                        'created'    => $created,
                        'active'     => 1,
                        'tax'        => $_product->tax,
                    ];
                }
                // END - PRODUCTS

                if ($data[ 'paid_cash' ]) {
                    $payment_type_id = 1;
                }
                if ($data[ 'paid_credit' ]) {
                    $payment_type_id = 2;
                }
                if ($data[ 'paid_voucher' ]) {
                    $payment_type_id = 5;
                }
                if ( ($data[ 'paid_cash' ] && $data[ 'paid_credit' ]) || ($data[ 'paid_cash' ] && $data[ 'paid_voucher' ]) || ($data[ 'paid_credit' ] && $data[ 'paid_voucher' ])) {
                    $payment_type_id = 4;
                } 


                if ($data[ 'total' ] != $data[ 'paid' ]) {
                    messageAJAX('error', 'Yetersiz Ödeme.');
                }


                // PREPARE SALES
                $data['sales'] = [
                    'location_id'     => $location,
                    'pos_id'          => $pos,
                    'point_id'        => $point,
                    'sale_type_id'    => 1,
                    'payment_type_id' => $payment_type_id,
                    'payment_date'    => $payment_date,
                    'user_id'         => $this->user->id,
                    'amount'          => $data[ 'total' ],
                    'subtotal'        => $data[ 'subtotal' ],
                    'change'          => $data[ 'paid' ] - $data[ 'total' ],
                    // 'tax'             => $data[ 'location_tax' ],
                    'cash'            => $data[ 'paid_cash' ],
                    'credit'          => $data[ 'paid_credit' ],
                    'voucher'         => $data[ 'paid_voucher' ],
                    'discount_amount' => NULL,
                    'discount_type'   => NULL,
                    'desc'            => $desc,
                    'created'         => $created,
                    'status'          => 'completed',
                    'active'          => 1,
                ];

                if (! $this->db->insert('sales', $data[ 'sales' ])) {
                    messageAJAX('error', 'Bir hata oluştu.');
                }

                $data['sales'] = [ 'id' => $this->db->insert_id() ] + $data[ 'sales' ];
                // END - PREPARE SALES
                

                // PREPARE PRODUCT IN SALE $product => $quantity
                foreach($products as $product => $quantity) {

                    $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
            
                    $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);

                    $package_id = $this->db->insert_id();

                    // Products in package
                    if (in_array($data[ 'sale_has_products' ][ $product ][ 'type' ], ['package', 'ves'])) {
                        foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                            $has_product[ 'package_id' ] = $package_id;

                            $this->db->insert('package_has_products', $has_product);

                            // re-calculate stocks
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $has_product[ 'product_id' ])
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($quantity * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                        }
                    } elseif ($data['sale_has_products'][ $product ][ 'type' ] == 'product') {
                        
                        $product_has_stocks = $this->db
                        ->select('PHS.*')
                        ->where('PHS.product_id', $product)
                        ->join('stocks S', 'S.id = PHS.stock_id')
                        ->where('S.active', 1)
                        ->get('product_has_stocks PHS')->result();

                        if ($product_has_stocks) {
                            foreach ($product_has_stocks as $stock) {
                                
                                $this->db->where('id', $stock->stock_id);
                                $this->db->set('quantity', '(quantity - '.($quantity * $stock->quantity).')', false);
                                $this->db->update('stocks', ['modified' => _date()]);
                            }
                        }
                    }
                }
                // END PREPARE PRODUCT IN SALE

                // PREPARE DIGITAL_KEYS
                // for ($i = 0; $i < $usb_products; $i++) {

                //     $digital_keys[ $i ] = [
                //         'sale_id' => $data[ 'sales' ][ 'id' ],
                //         'user_id' => $this->user->id,
                //         'key'     => rand(1001,9999),
                //         'active'  => 1,
                //     ];

                //     $this->db->insert('digital_keys', $digital_keys[ $i ]);
                // }
                // END - PREPARE SALES

                messageAJAX( 'success', logs_($this->user->id, 'Satış oluşturuldu.', $data) );
            }
	    }
		
		function edit($id = NULL)
	    {
			$id = (int) $id;

            $data[ 'sale' ] = $this->db
                ->where('id', $id)
                ->where('active != 3')
                ->get(self::sales, 1)
                ->row()
            OR show_404();


            if ($this->input->is_ajax_request()) {
                
                $products             = $this->input->post('products[]');
                $payments             = $this->input->post('payments[]');
                $location             = $this->input->post('location');
                $pos                  = $this->input->post('pos');
                $point                = $this->input->post('point');
                $desc                 = $this->input->post('desc', TRUE);
                $payment_date         = dateConvert($this->input->post('payment_date'), 'server');
                $modified              = _date();
                $digital_keys         = [];
                $payment_type_id      = 0;


                if (! in_array($location, $this->user->locations)) {
                    messageAJAX('error', 'Geçersiz restoran.');
                }

                $this->db->where('active', 1);
                $this->db->where('id', $pos);
                $this->db->where('location_id', $location);
                if (!$this->db->get('pos', 1)->row()) {
                    messageAJAX('error', 'Geçersiz kasa.');
                }

                $this->db->where('active', 1);
                $this->db->where('id', $point);
                $this->db->where('location_id', $location);
                if (!$this->db->get('points', 1)->row()) {
                    messageAJAX('error', 'Geçersiz masa.');
                }



                $order = $this->db->where([
                    'id'     => $id,
                    'active' => 1,
                ])->order_by('id', 'DESC')->get('sales', 1)->row() OR messageAJAX('error', 'Sipariş geçersiz veya iptal edilmiş olabilir.');

                $order_has_products = $this->db->where([
                    'sale_id' => $id,
                ])->get('sale_has_products')->result_array();

                $tmp = [];
                $order_has_products_prdct_array = [];
                foreach ($order_has_products as $value) {
                    $tmp[$value['id']] = $value;
                    $order_has_products_prdct_array[$value['product_id']] = $value['id'];
                }
                $order_has_products = $tmp;




                $data+= $this->_calculate($location, (array) $products, (array) $payments);


                // PRODUCTS
                $ignore_products_on_delete = null;
                foreach ($products as $product => $quantity) {

                    $this->db->where_in('P.id', (int)$product);
                    $this->db->limit(1);
                    $_products = $this->Products_model->index( $location )
                    OR messageAJAX('error', 'Geçersiz ürün.');

                    $_product = $_products[ 0 ];
                    $quantity = is_numeric($quantity) ? $quantity : 1;



                    // COUNT USB PRODUCTS
                    if (in_array($_product->type, ['package', 'ves'])) {

                        $package_has_products = $this->db
                            ->select('P.*')
                            ->where('PHP.package_id', $_product->id)
                            ->where('P.type', 'product')
                            ->join('products P', 'P.id = PHP.product_id')
                        ->get('products_has_products PHP')->result();

                        $data[ 'package_has_products' ][ $product ] = NULL;
                        foreach ($package_has_products as $key => $has_product) {
                            

                            // PACKAGE CONTENT PREPARING
                            $data[ 'package_has_products' ][ $product ][] = [
                                'package_id' => NULL,
                                'product_id' => $has_product->id,
                                'quantity'   => $quantity * 1,
                                'price'      => $has_product->price,
                                'content'    => $has_product->content,
                                'digital'    => $has_product->digital,
                                'is_video'   => $has_product->is_video,
                                'title'      => $has_product->title,
                                'created'    => $modified,
                                'modified'   => $modified,
                                'active'     => 0,
                                'tax'        => $has_product->tax,
                            ];  
                        }
                    }



                    // PREPARE SALE_HAS_PRODUCTS ARRAY
                    if (isset($order_has_products_prdct_array[$_product->id])) {

                        $ignore_products_on_delete[] = $order_has_products_prdct_array[$_product->id];

                        // PREPARE SALE_HAS_PRODUCTS UPDATE ARRAY
                        $data[ 'sale_has_products' ]['update'][ $product ] = [
                            'id'         => $order_has_products_prdct_array[$_product->id],
                            // 'sale_id'    => $order_id,
                            // 'product_id' => $_product->id,
                            'quantity'   => $quantity,
                            'price'      => $_product->price,
                            'content'    => $_product->content,
                            'digital'    => $_product->digital,
                            'is_video'   => $_product->is_video,
                            'title'      => $_product->title,
                            'type'       => $_product->type,
                            'modified'   => $modified,
                            // 'active'     => 0,
                            'tax'        => $_product->tax,
                        ];

                    } else {

                        // PREPARE SALE_HAS_PRODUCTS INSERT ARRAY
                        $data[ 'sale_has_products' ]['insert'][ $product ] = [
                            'sale_id'    => NULL,
                            'product_id' => $_product->id,
                            'quantity'   => $quantity,
                            'price'      => $_product->price,
                            'content'    => $_product->content,
                            'digital'    => $_product->digital,
                            'is_video'   => $_product->is_video,
                            'title'      => $_product->title,
                            'type'       => $_product->type,
                            'created'    => $modified,
                            'active'     => 1,
                            'tax'        => $_product->tax,
                        ];
                    }
                        
                    // END - PRODUCTS
                    // $data[ 'sale_has_products' ][ $product ] = [
                    //     'sale_id'    => 0,
                    //     'product_id' => $_product->id,
                    //     'quantity'   => $quantity,
                    //     'price'      => $_product->price,
                    //     'content'    => $_product->content,
                    //     'digital'    => $_product->digital,
                    //     'is_video'   => $_product->is_video,
                    //     'title'      => $_product->title,
                    //     'type'       => $_product->type,
                    //     'created'    => $modified,
                    //     'modified'   => $modified,
                    //     'active'     => 1
                    // ];
                }
                // END - PRODUCTS

                if ($data[ 'paid_cash' ]) {
                    $payment_type_id = 1;
                }
                if ($data[ 'paid_credit' ]) {
                    $payment_type_id = 2;
                }
                if ($data[ 'paid_voucher' ]) {
                    $payment_type_id = 5;
                }
                if ( ($data[ 'paid_cash' ] && $data[ 'paid_credit' ]) || ($data[ 'paid_cash' ] && $data[ 'paid_voucher' ]) || ($data[ 'paid_credit' ] && $data[ 'paid_voucher' ])) {
                    $payment_type_id = 4;
                } 


                if ($data[ 'total' ] > $data[ 'paid' ] || $data[ 'total' ] < $data[ 'paid' ]) {
                    messageAJAX('error', 'Yetersiz Ödeme.');
                }


                // PREPARE SALES
                $data['sales'] = [
                    'location_id'     => $location,
                    'pos_id'          => $pos,
                    'point_id'        => $point,
                    'payment_type_id' => $payment_type_id,
                    'payment_date'    => $payment_date,
                    'amount'          => $data[ 'total' ],
                    'subtotal'        => $data[ 'subtotal' ],
                    'change'          => $data[ 'paid' ] - $data[ 'total' ],
                    // 'tax'             => $data[ 'location_tax' ],
                    'cash'            => $data[ 'paid_cash' ],
                    'credit'          => $data[ 'paid_credit' ],
                    'voucher'         => $data[ 'paid_voucher' ],
                    'desc'            => $desc,
                    'modified'        => $modified,
                ];

                $this->db->where('id', $data[ 'sale' ]->id);
                if (! $this->db->update('sales', $data[ 'sales' ])) {
                    messageAJAX('error', 'Bir hata oluştu.');
                }

                $data['sales'] = [ 'id' => $data[ 'sale' ]->id ] + $data[ 'sales' ];
                // END - PREPARE SALES
                
                


                // PREPARE PRODUCT IN SALE
                if ($ignore_products_on_delete) {
                    $this->db->where_not_in('id', $ignore_products_on_delete);
                    $this->db->where('sale_id', $id);
                    $this->db->delete('sale_has_products');

                    $this->db->where_in('package_id', $ignore_products_on_delete);
                    $this->db->delete('package_has_products');
                }

                if (@$data[ 'sale_has_products' ][ 'update' ]) {
                    foreach ($data[ 'sale_has_products' ][ 'update' ] as $product => $update_product) {
                    
                        $_id = $update_product['id'];
                        unset($update_product['id']);

                        $this->db->where('id', $_id);
                        $this->db->update('sale_has_products', $update_product);

                        $package_id = $_id;

                        // Products in package
                        if ($data[ 'sale_has_products' ][ 'update' ][ $product ][ 'type' ] == 'package') {
                            foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                                $has_product[ 'package_id' ] = $package_id;

                                $this->db->insert('package_has_products', $has_product);

                                // If it is a sale, re-calculate stocks
                                $product_has_stocks = $this->db
                                ->select('PHS.*')
                                ->where('PHS.product_id', $has_product[ 'product_id' ])
                                ->join('stocks S', 'S.id = PHS.stock_id')
                                ->where('S.active', 1)
                                ->get('product_has_stocks PHS')->result();

                                if ($product_has_stocks) {
                                    foreach ($product_has_stocks as $stock) {
                                        
                                        $this->db->where('id', $stock->stock_id);
                                        $this->db->set('quantity', '(quantity - '.($update_product['quantity'] * $stock->quantity).')', false);
                                        $this->db->update('stocks', ['modified' => _date()]);
                                    }
                                }
                                
                            }

                        } elseif ($update_product[ 'type' ] == 'product') {
                            
                            // If it is a sale, re-calculate stocks
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $product)
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($update_product['quantity'] * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                            
                        }

                    }
                }

                
                if (@$data[ 'sale_has_products' ][ 'insert' ]) {
                    
                    foreach ($data[ 'sale_has_products' ][ 'insert' ] as $product => $insert_product) {
                        
                        $insert_product[ 'sale_id' ] = $sale_id;

                        $this->db->insert('sale_has_products', $insert_product);

                        $package_id = $this->db->insert_id();

                        // Products in package
                        if ($insert_product[ 'type' ] == 'package') {
                            foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                                $has_product[ 'package_id' ] = $package_id;

                                $this->db->insert('package_has_products', $has_product);


                                // If it is a sale, re-calculate stocks
                                $product_has_stocks = $this->db
                                ->select('PHS.*')
                                ->where('PHS.product_id', $has_product[ 'product_id' ])
                                ->join('stocks S', 'S.id = PHS.stock_id')
                                ->where('S.active', 1)
                                ->get('product_has_stocks PHS')->result();

                                if ($product_has_stocks) {
                                    foreach ($product_has_stocks as $stock) {
                                        
                                        $this->db->where('id', $stock->stock_id);
                                        $this->db->set('quantity', '(quantity - '.($insert_product['quantity'] * $stock->quantity).')', false);
                                        $this->db->update('stocks', ['modified' => _date()]);
                                    }
                                }

                            }
                        } elseif ($insert_product[ 'type' ] == 'product') {
                                
                            // If it is a sale, re-calculate stocks
                            $product_has_stocks = $this->db
                            ->select('PHS.*')
                            ->where('PHS.product_id', $product)
                            ->join('stocks S', 'S.id = PHS.stock_id')
                            ->where('S.active', 1)
                            ->get('product_has_stocks PHS')->result();

                            if ($product_has_stocks) {
                                foreach ($product_has_stocks as $stock) {
                                    
                                    $this->db->where('id', $stock->stock_id);
                                    $this->db->set('quantity', '(quantity - '.($insert_product['quantity'] * $stock->quantity).')', false);
                                    $this->db->update('stocks', ['modified' => _date()]);
                                }
                            }
                                
                        }
                    }
                }
                // END PREPARE PRODUCT IN SALE




                // // BEFORE PREPARE PRODUCT IN SALE DELETE ALL
                // $this->db->select('id');
                // $this->db->where('sale_id', $data[ 'sale' ]->id);
                // $this->db->where_in('type', ['package', 'ves']);
                // $deleted_products = $this->db->get('sale_has_products')->result_array();

                // if ($deleted_products) {
                //     $this->db->where_in('package_id', array_column($deleted_products, 'id'));
                //     $this->db->delete('package_has_products');
                // }

                // $this->db->where('sale_id', $data[ 'sale' ]->id);
                // $this->db->delete('sale_has_products');
                // // END - BEFORE PREPARE PRODUCT IN SALE DELETE ALL
                

                // // PREPARE PRODUCT IN SALE $product => $quantity
                // foreach($products as $product => $quantity) {

                //     $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
            
                //     $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);

                //     $package_id = $this->db->insert_id();

                //     // Products in package
                //     if (in_array($data[ 'sale_has_products' ][ $product ][ 'type' ], ['package', 'ves'])) {
                //         foreach ($data[ 'package_has_products' ][ $product ] as $has_product) {

                //             $has_product[ 'package_id' ] = $package_id;

                //             $this->db->insert('package_has_products', $has_product);
                //         }
                //     }
                // }
                // // END PREPARE PRODUCT IN SALE


                messageAJAX( 'success', logs_($this->user->id, 'Sale has been edited.', $data) );


            } else {


                $data[ 'products' ] = $this->Products_model->index($data[ 'sale' ]->location_id);

                $data[ 'sale_has_products' ] = $this->db
                    ->where('sale_id', $id)
                    ->get(self::salehasproducts)
                    ->result_array();

                $data[ 'sale_has_products' ] = array_combine( array_column($data[ 'sale_has_products' ], 'product_id'), $data[ 'sale_has_products' ] );
                
                $data[ 'pos' ] = $this->db
                    ->where('location_id', $data[ 'sale' ]->location_id)
                    ->where('active', 1)
                    ->get('pos')
                    ->result_array();

                $data[ 'points' ] = $this->db
                    ->where('location_id', $data[ 'sale' ]->location_id)
                    ->where('active', 1)
                    ->get('points')
                    ->result_array();

                return $data;
            }





			
     //        $data['sale_types'] = $this->db ->order_by('title') ->get(self::saletypes)->result_array();
     //        $data['sale_has_products'] = $this->db
     //            // ->select('SHP.*, SHP.tax_inclusive')
     //            ->where('SHP.sale_id', $id)
     //            // ->join('products P', 'P.id = SHP.product_id')
     //            ->get(self::salehasproducts.' SHP')->result_array();
     //        $data['payment_methods'] = $this->db->get(self::paymenttypes)->result_array();

     //        if ($this->input->is_ajax_request()) {
     //            exit('Processing...');
     //            $values['ticket']           = $this->input->post('ticket', TRUE);
     //            $values['location_id']      = $this->input->post('location');
     //            $values['discount_amount']  = $this->input->post('discount_amount');
     //            $values['discount_type']    = $this->input->post('discount_type');
     //            $values['payment_date']     = date('Y-m-d HH:ii:ss', strtotime($this->input->post('payment_date')));
     //            $values['payment_type_id']  = $this->input->post('payment_method');
     //            $values['desc']             = $this->input->post('desc', TRUE);
     //            // $values['sale_type_id']     = 1;
     //            // $values['active']           = 1;
     //            $values['modified']         = _date();

     //            $values['amount'] = 0.00;
     //            $values_product_quantity = [];
     //            foreach ($this->input->post('quantity') as $product_id => $quantity) {
                    
     //                $this->db->where('location_id', $values['location_id']) ->where('product_id', $product_id);
     //                if(! $product = $this->db->get(self::locationhasproducts, 1)->row()) messageAJAX( 'error', logs_($this->user->id, 'Geçersiz ürün.  for location. ', $values) );
                    
     //                $values['amount'] += $product->price * $quantity;

     //                $values_product_quantity[] = [  'sale_id'    => 0,
     //                                                'product_id' => $product_id,
     //                                                'quantity'   => $quantity,
     //                                                'created'    => $values['modified'],
     //                                                'modified'   => $values['modified'],
     //                ];
     //            }

     //            if(!in_array($values['payment_type_id'], array_column($data['payment_methods'], 'id'))) messageAJAX( 'error', logs_($this->user->id, 'Invalid Payment Method. ', $values) );

     //            if(!in_array($values['location_id'], $this->user->locations)) messageAJAX( 'error', logs_($this->user->id, 'Geçersiz restoran. ', $values) );

     //            $this->db->like('title', $values['ticket'], 'AFTER');
     //            if(! $this->db->get(self::photos, 1)->row()) messageAJAX( 'error', logs_($this->user->id, 'Invalid Ticket Number. ', $values) );

     //            if( $this->db->update(self::sales, $values) ){
					
					// $this->db->where('sale_id', $id);
					// $this->db->delete(self::salehasproducts);
					
     //                foreach ($values_product_quantity as $key => $value) {
                        
     //                    $values_product_quantity[$key]['sale_id'] = $id;

     //                    $this->db->insert(self::salehasproducts, $values_product_quantity[$key]); 
     //                }
                    

     //                messageAJAX( 'success', logs_($this->user->id, 'Sale has been edited.', ['id'=>$id]+$values) );
     //            }

     //        } else {

     //            return $data;
     //        }
	    }

        // function view($id)
        // {
            // pr($id,1);
        // }
		
		function delete($id, $values)
	    {
			$id = (int) $id;
			
			$values->sale_has_products = $this->db ->where('sale_id', $id) ->get(self::salehasproducts)->result_array();
			
	    	$this->db->where('id', $id);
            if( $this->db->delete(self::sales) ) messageAJAX('success', logs_($this->user->id, 'Sale has been deleted.', $values));
	    }

        function _calculate($location = FALSE, $products = [], $payments = [])
        {
            $data[ 'total' ]                 = 0.00;
            $data[ 'total_formated' ]        = 0.00;
            $data[ 'subtotal' ]              = 0.00;
            $data[ 'subtotal_formated' ]     = 0.00;
            $data[ 'tax' ]                   = 0.00;
            $data[ 'tax_formated' ]          = 0.00;
            $data[ 'paid' ]                  = 0.00;
            $data[ 'paid_formated' ]         = 0.00;
            $data[ 'paid_cash' ]             = 0.00;
            $data[ 'paid_cash_formated' ]    = 0.00;
            $data[ 'paid_credit' ]           = 0.00;
            $data[ 'paid_credit_formated' ]  = 0.00;
            $data[ 'paid_voucher' ]          = 0.00;
            $data[ 'paid_voucher_formated' ] = 0.00;
            $data[ 'rest' ]                  = 0.00;
            $data[ 'rest_formated' ]         = 0.00;
            $data[ 'location_currency' ]     = $this->user->locations_array[ $location ][ 'currency' ];
            // $data[ 'location_tax' ]          = $this->user->locations_array[ $location ][ 'tax' ];


            // CALCULATE PRODUCTS
            foreach ($products as $product => $quantity) {

                $this->load->model('Products_model');
                $this->db
                    ->where_in('P.id', (int) $product)
                    ->limit(1);
                $_products = $this->Products_model->index( $location ) OR messageAJAX('error', 'Geçersiz ürün.');

                $_product = $_products[ 0 ];

                $calc_tax = $_product->price * $_product->tax / 100;

                $quantity = is_numeric($quantity) ? $quantity : 1;

                $data[ 'subtotal' ] += $tmp_subtotal = round($_product->price * $quantity, 2);

                $data[ 'tax' ] += $tmp_subtotal * $_product->tax / 100;
            }

            $data[ 'subtotal_formated' ] += number_format($data[ 'subtotal' ], 2);

            // TOTAL TAX
            $data[ 'tax_formated' ] = number_format($data[ 'tax' ], 2);


            // TOTAL AMOUNT
            $data[ 'total' ]          = round($data[ 'subtotal' ] + $data[ 'tax' ], 2);
            $data[ 'total_formated' ] = number_format($data[ 'total' ], 2 );



            // CALCULATE PAYMENTS
            if (isset($payments[ 'cash' ])) {

                foreach ($payments[ 'cash' ] as $payment) {

                    $data[ 'paid_cash' ] += doubleval( str_replace( ',', '', $payment ) );

                }
            }
            if (isset($payments[ 'credit' ])) {

                foreach ($payments[ 'credit' ] as $payment) {

                    $data[ 'paid_credit' ] += doubleval( str_replace( ',', '', $payment ) );

                }
            }
            if (isset($payments[ 'voucher' ])) {

                foreach ($payments[ 'voucher' ] as $payment) {

                    $data[ 'paid_voucher' ] += doubleval( str_replace( ',', '', $payment ) );

                }
            }

            $data[ 'paid_cash' ]             = round($data[ 'paid_cash' ], 2);
            $data[ 'paid_credit' ]           = round($data[ 'paid_credit' ], 2);
            $data[ 'paid_voucher' ]          = round($data[ 'paid_voucher' ], 2);
            
            $data[ 'paid_cash_formated' ]    = number_format($data[ 'paid_cash' ], 2);
            $data[ 'paid_credit_formated' ]  = number_format($data[ 'paid_credit' ], 2);
            $data[ 'paid_voucher_formated' ] = number_format($data[ 'paid_voucher' ], 2);

            $data[ 'paid' ]                  = $data[ 'paid_cash' ] + $data[ 'paid_credit' ] + $data[ 'paid_voucher' ];
            $data[ 'paid_formated' ]         = number_format($data[ 'paid' ], 2);
            
            $data[ 'rest' ]                  = $data[ 'total' ] - $data[ 'paid' ];
            $data[ 'rest_formated' ]         = number_format($data[ 'rest' ], 2);
            
            $data[ 'status' ]                = ($data[ 'paid' ] >= $data[ 'total' ]) ? false : true;

            return $data;
        }

        function getSaleData($sale_id = FALSE)
        {
            $data[ 'sale' ] = $this->db
                ->where('id', $sale_id)
                ->where('active !=', 3)
            ->get(self::sales, 1)->row();

            if ($data[ 'sale' ]) {
                
                $data[ 'products' ] = $this->db
                   ->where('sale_id', $sale_id)
                   ->where('active !=', 3)
                ->get(self::salehasproducts, 1)->result();

                
            }
        }
	}    
?>
