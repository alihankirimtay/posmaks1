<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ExpenseTypes_Model extends CI_Model {
	
        const expenses = 'expenses';
        const expensetypes = 'expense_types';
        const users = 'users';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index()
        {
            return $this->db->get(self::expensetypes)->result();
        }

	    function add()
	    {
            if ($this->input->is_ajax_request()) {
    
                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['active']   = $this->input->post('active');
                $values['created']  = _date();

                if( $this->db->insert(self::expensetypes, $values) ){

                    $id = $this->db->insert_id();

                    messageAJAX( 'success', logs_($this->user->id, 'Gider türü oluşturludu.', ['id'=>$id]+$values) );
                }

            }
	    }

        function view($id)
        {
            pr($id,1);
        }

        function edit($id)
        {
            $id = (int) $id;

            if ($this->input->is_ajax_request()) {

                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['active']   = $this->input->post('active');
                $values['modified'] = _date();

                $this->db->where('id', $id);
                if( $this->db->update(self::expensetypes, $values) ){

                    messageAJAX( 'success', logs_($this->user->id, 'Gider türü güncellendi.', $values) );
                }
                
            } else {
                
                
            }
            
        }

        function delete($id, $data)
        {
            $id = (int) $id;

            $this->db->where('id', $id);
            if( $this->db->delete(self::expensetypes) ) {

                messageAJAX( 'success', logs_($this->user->id, 'Gider türü silindi. ', $data) );
            }
        }
	}    
?>
