<?php 

	class Mobilpos_model extends CI_Model {


		public function get() {

			return $this->db->get("points")->result();

		}

		public function location() {

			return $this->db->get("locations")->result();

		}

		public function floors() {

			return $this->db->get("floors")->result();

		}

		public function zones() {

			return $this->db->get("zones")->result();

		}

		public function category() {

			$this->db->select('pt.*');
        	$this->db->where('pt.active', 1);
			$category = $this->db->get('product_types as pt')->result_array();

			return $category;

		}

		public function products($location_id) {

			$this->db->select('p.*');
        	$this->db->where('p.active', 1);
        	$this->db->where('p.location_id', $location_id);
        	$this->db->where('p.portion_id', NULL);
			$products = $this->db->get('products as p')->result_array();

			return $products;

		}

		public function orderProducts($productId){

			$this->db->select('p.title,p.type,p.price,p.tax,p.id');
	       	$this->db->where_in('p.id', $productId);
	       	$result = $this->db->get('products as p')->result_array();

       		return $result;
		}

		// public function complete($sales_id) {
		// 	$this->db->select('s.*');
  //       	$this->db->where('s.active', 1);
  //       	$this->db->where('s.id',$sales_id);
  //       	$sales = $this->db->get('sales as s')->result_array();

  //       	return $sales;


		// }


	}




?>