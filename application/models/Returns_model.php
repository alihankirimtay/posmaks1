<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Returns_Model extends CI_Model {
	
        const sales = 'sales';
        const salehasproducts = 'sale_has_products';


	    function __construct()
	    {
	    	parent::__construct();
	    }

        function invoice($id = FALSE)
        {
            $id = (int) $id;

            if (!$id) {
                return false;
            }

             $data[ 'sale' ] = $this->db
                ->where('id', $id)
                ->where('active !=', 3)
            ->get(self::sales, 1)->row();

            if (!$data[ 'sale' ]) {
                return false;
            }
                
            $data[ 'products' ] = $this->db
               ->select('*, (SELECT title FROM products WHERE id = SHP.product_id LIMIT 1) as title')
               ->where('sale_id', $id)
               ->where('active !=', 3)
            ->get(self::salehasproducts.' SHP', 1)->result();

             
             return $data; 
            
        }


  //       function index($location)
  //       {
  //           $location = (int) $location;

  //           if($location && !in_array($location, $this->user->locations)) error_('access');

  //           $this->db->select('S.*');
  //           $this->db->select('(SELECT title FROM locations WHERE id = S.location_id LIMIT 1) AS location');
  //           // $this->db->select('(SELECT title FROM '.self::saletypes.' WHERE id = S.sale_type_id LIMIT 1) AS type');
  //           $this->db->select('(SELECT username FROM '.self::users.' WHERE id = S.user_id LIMIT 1) AS user');
  //           $this->db->select('(SELECT title FROM '.self::paymenttypes.' WHERE id = S.payment_type_id LIMIT 1) AS payment_method');

  //           if($location) $this->db->where('S.location_id', $location);
  //           else          $this->db->where_in('S.location_id', $this->user->locations);

  //           return $this->db->get(self::sales.' S')->result();
  //       }

  //       public function invoice_data($id = FALSE)
  //       {   
  //           if (!$id = (int) $id) {
  //               return FALSE;
  //           }

  //           $data['sale'] = $this->db
  //           ->where('id', $id)
  //           ->get('sales', 1)->row();

  //           if (! $data['sale']) {
  //               return false;
  //           }

  //           $data['location']['tax']      = $tax      = $this->user->locations_array[ $data['sale']->location_id ]['tax'];
  //           $data['location']['currency'] = $currency = $this->user->locations_array[ $data['sale']->location_id ]['currency'];
  //           $data['sale']->sub_total      = round($data['sale']->amount / ($tax / 100 + 1), 2);
  //           $data['sale']->tax            = $data['sale']->amount - $data['sale']->sub_total;


  //           $data['products'] = $this->db
  //           ->select('SHP.*, (SELECT title FROM products WHERE id = SHP.product_id LIMIT 1) as title')
  //           ->select('(SELECT tax_inclusive FROM location_has_products WHERE product_id = SHP.product_id 
  //               AND location_id = (SELECT location_id FROM sales WHERE id = SHP.sale_id LIMIT 1)
  //               LIMIT 1) as tax_inclusive')
  //           ->where('SHP.sale_id', $id)
  //           ->get('sale_has_products SHP')->result();

  //           $tmp = NULL;
  //           if ($data['products']) {

  //               foreach ($data['products'] as $key => $product) {

  //                   $price = ($product->tax_inclusive)
  //                   ? $product->price - $product->price * $tax / 100 
  //                   : $product->price;
  //                   $price = number_format(round($price, 2), 2);

  //                   $tmp[] = [
  //                       'id'         => $product->id,
  //                       'product_id' => $product->product_id,
  //                       'no'         => $key + 1,
  //                       'title'      => $product->title,
  //                       'quantity'   => $product->quantity,
  //                       'price'      => $price,
  //                       'total'      => number_format(round($price * $product->quantity, 2), 2)
  //                   ];
  //               }

  //               $data['products'] = $tmp;
  //           }

  //           $data['usbs'] = $this->db
  //           ->where('sale_id', $id)
  //           ->get('digital_keys')->result();

  //           $data['template'] = $this->db
  //           ->where('location_id', $data['sale']->location_id)
  //           ->get('invoice_template', 1)->row();

  //           return $data;
  //       }

	 //    function add()
	 //    {
  //           if ($this->input->is_ajax_request()) {


  //               $products             = $this->input->post('products[]');
  //               $payments             = $this->input->post('payments[]');
  //               $location             = $this->input->post('location');
  //               $desc                 = $this->input->post('desc', TRUE);
  //               $payment_date         = dateConvert($this->input->post('payment_date'), 'server');
  //               $created              = _date();
  //               $digital_keys         = [];
  //               $payment_type_id      = 0;
  //               $usb_products         = 0;


  //               if (! in_array($location, $this->user->locations)) {
  //                   messageAJAX('error', 'Invalid Location.');
  //               }


  //               $data = $this->_calculate($location, (array) $products, (array) $payments);


  //               // PRODUCTS
  //               foreach ($products as $product => $quantity) {

  //                   $this->db->where_in('P.id', (int)$product);
  //                   $this->db->limit(1);
  //                   $_products = $this->Products_model->index( $location )
  //                   OR messageAJAX('error', 'Invalid product');

  //                   $_product = $_products[ 0 ];
  //                   $quantity = is_numeric($quantity) ? $quantity : 1;

  //                   // COUNT USB PRODUCTS
  //                   if ($_product->type == 'package') {

  //                       $pack_has_usb = $this->db
  //                           ->select('PHP.product_id')
  //                           ->where('PHP.package_id', $_product->id)
  //                           ->where('P.type', 'product')
  //                           ->where('P.digital', 1)
  //                           ->join('products P', 'P.id = PHP.product_id')
  //                       ->get('products_has_products PHP');

  //                       $usb_products += $pack_has_usb->num_rows() * $quantity;

  //                   } elseif ($_product->digital) {

  //                       $usb_products += $quantity;
  //                   }

  //                   // PREPARE SALE_HAS_PRODUCTS ARRAY
  //                   $data[ 'sale_has_products' ][ $product ] = [
  //                       'sale_id'    => 0,
  //                       'product_id' => $_product->id,
  //                       'quantity'   => $quantity,
  //                       'price'      => $_product->price,
  //                       'content'    => $_product->content,
  //                       'digital'    => $_product->digital,
  //                       'created'    => $created,
  //                       'active'     => 1
  //                   ];
  //               }
  //               // END - PRODUCTS

  //               if ($data[ 'paid_cash' ]) {
  //                   $payment_type_id = 1;
  //               }
  //               if ($data[ 'paid_credit' ]) {
  //                   $payment_type_id = 2;
  //               }
  //               if ($data[ 'paid_voucher' ]) {
  //                   $payment_type_id = 5;
  //               }
  //               if ( ($data[ 'paid_cash' ] && $data[ 'paid_credit' ]) || ($data[ 'paid_cash' ] && $data[ 'paid_voucher' ]) || ($data[ 'paid_credit' ] && $data[ 'paid_voucher' ])) {
  //                   $payment_type_id = 4;
  //               } 


  //               if ($data[ 'total' ] > $data[ 'paid' ] || $data[ 'total' ] < $data[ 'paid' ]) {
  //                   messageAJAX('error', 'Wrong payment amount.');
  //               }


  //               // PREPARE SALES
  //               $data['sales'] = [
  //                   'location_id'     => $location,
  //                   'sale_type_id'    => 1,
  //                   'payment_type_id' => $payment_type_id,
  //                   'payment_date'    => $payment_date,
  //                   'user_id'         => $this->user->id,
  //                   'amount'          => $data[ 'total' ],
  //                   'subtotal'        => $data[ 'subtotal' ],
  //                   'change'          => $data[ 'paid' ] - $data[ 'total' ],
  //                   'tax'             => $data[ 'location_tax' ],
  //                   'cash'            => $data[ 'paid_cash' ],
  //                   'credit'          => $data[ 'paid_credit' ],
  //                   'voucher'         => $data[ 'paid_voucher' ],
  //                   'discount_amount' => NULL,
  //                   'discount_type'   => NULL,
  //                   'desc'            => $desc,
  //                   'created'         => $created,
  //                   'active'          => 1,
  //               ];

  //               if (! $this->db->insert('sales', $data[ 'sales' ])) {
  //                   messageAJAX('error', 'An error occurred.');
  //               }

  //               $data['sales'] = [ 'id' => $this->db->insert_id() ] + $data[ 'sales' ];
  //               // END - PREPARE SALES
                

  //               // PREPARE PRODUCT IN SALE $product => $quantity
  //               foreach($products as $product => $quantity) {

  //                   $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
            
  //                   $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);
  //               }
  //               // END PREPARE PRODUCT IN SALE

  //               // PREPARE DIGITAL_KEYS
  //               for ($i = 0; $i < $usb_products; $i++) {

  //                   $digital_keys[ $i ] = [
  //                       'sale_id' => $data[ 'sales' ][ 'id' ],
  //                       'user_id' => $this->user->id,
  //                       'key'     => rand(1001,9999),
  //                       'active'  => 1,
  //                   ];

  //                   $this->db->insert('digital_keys', $digital_keys[ $i ]);
  //               }
  //               // END - PREPARE SALES

  //               messageAJAX( 'success', logs_($this->user->id, 'Sale has been added.', $data) );
  //           }
	 //    }
		
		// function edit($id = NULL)
	 //    {
		// 	$id = (int) $id;

  //           $data[ 'sale' ] = $this->db
  //               ->where('id', $id)
  //               ->where('active != 3')
  //               ->get(self::sales, 1)
  //               ->row()
  //           OR show_404();


  //           if ($this->input->is_ajax_request()) {
                
  //               $products             = $this->input->post('products[]');
  //               $payments             = $this->input->post('payments[]');
  //               $location             = $this->input->post('location');
  //               $desc                 = $this->input->post('desc', TRUE);
  //               $payment_date         = dateConvert($this->input->post('payment_date'), 'server');
  //               $modified              = _date();
  //               $digital_keys         = [];
  //               $payment_type_id      = 0;


  //               if (! in_array($location, $this->user->locations)) {
  //                   messageAJAX('error', 'Invalid Location.');
  //               }


  //               $data+= $this->_calculate($location, (array) $products, (array) $payments);


  //               // PRODUCTS
  //               foreach ($products as $product => $quantity) {

  //                   $this->db->where_in('P.id', (int)$product);
  //                   $this->db->limit(1);
  //                   $_products = $this->Products_model->index( $location )
  //                   OR messageAJAX('error', 'Invalid product');

  //                   $_product = $_products[ 0 ];
  //                   $quantity = is_numeric($quantity) ? $quantity : 1;

  //                   // PREPARE SALE_HAS_PRODUCTS ARRAY
  //                   $data[ 'sale_has_products' ][ $product ] = [
  //                       'sale_id'    => 0,
  //                       'product_id' => $_product->id,
  //                       'quantity'   => $quantity,
  //                       'price'      => $_product->price,
  //                       'content'    => $_product->content,
  //                       'digital'    => $_product->digital,
  //                       'created'    => $modified,
  //                       'modified'   => $modified,
  //                       'active'     => 1
  //                   ];
  //               }
  //               // END - PRODUCTS

  //               if ($data[ 'paid_cash' ]) {
  //                   $payment_type_id = 1;
  //               }
  //               if ($data[ 'paid_credit' ]) {
  //                   $payment_type_id = 2;
  //               }
  //               if ($data[ 'paid_voucher' ]) {
  //                   $payment_type_id = 5;
  //               }
  //               if ( ($data[ 'paid_cash' ] && $data[ 'paid_credit' ]) || ($data[ 'paid_cash' ] && $data[ 'paid_voucher' ]) || ($data[ 'paid_credit' ] && $data[ 'paid_voucher' ])) {
  //                   $payment_type_id = 4;
  //               } 


  //               if ($data[ 'total' ] > $data[ 'paid' ] || $data[ 'total' ] < $data[ 'paid' ]) {
  //                   messageAJAX('error', 'Wrong payment amount.');
  //               }


  //               // PREPARE SALES
  //               $data['sales'] = [
  //                   'location_id'     => $location,
  //                   'payment_type_id' => $payment_type_id,
  //                   'payment_date'    => $payment_date,
  //                   'amount'          => $data[ 'total' ],
  //                   'subtotal'        => $data[ 'subtotal' ],
  //                   'change'          => $data[ 'paid' ] - $data[ 'total' ],
  //                   'tax'             => $data[ 'location_tax' ],
  //                   'cash'            => $data[ 'paid_cash' ],
  //                   'credit'          => $data[ 'paid_credit' ],
  //                   'voucher'         => $data[ 'paid_voucher' ],
  //                   'desc'            => $desc,
  //                   'modified'         => $modified,
  //               ];

  //               $this->db->where('id', $data[ 'sale' ]->id);
  //               if (! $this->db->update('sales', $data[ 'sales' ])) {
  //                   messageAJAX('error', 'An error occurred.');
  //               }

  //               $data['sales'] = [ 'id' => $data[ 'sale' ]->id ] + $data[ 'sales' ];
  //               // END - PREPARE SALES
                
  //               // BEFORE PREPARE PRODUCT IN SALE DELETE ALL
  //               $this->db->where('sale_id', $data[ 'sale' ]->id);
  //               $this->db->delete('sale_has_products');
  //               // END - BEFORE PREPARE PRODUCT IN SALE DELETE ALL
                

  //               // PREPARE PRODUCT IN SALE $product => $quantity
  //               foreach($products as $product => $quantity) {

  //                   $data[ 'sale_has_products' ][ $product ][ 'sale_id' ] = $data[ 'sales' ][ 'id' ];
            
  //                   $this->db->insert('sale_has_products', $data[ 'sale_has_products' ][ $product ]);
  //               }
  //               // END PREPARE PRODUCT IN SALE


  //               messageAJAX( 'success', logs_($this->user->id, 'Sale has been edited.', $data) );


  //           } else {


  //               $data[ 'products' ] = $this->Products_model->index($data[ 'sale' ]->location_id);

  //               $data[ 'sale_has_products' ] = $this->db
  //                   ->where('sale_id', $id)
  //                   ->get(self::salehasproducts)
  //                   ->result_array();

  //               $data[ 'sale_has_products' ] = array_combine( array_column($data[ 'sale_has_products' ], 'product_id'), $data[ 'sale_has_products' ] );
                    
  //               return $data;
  //           }





			
  //    //        $data['sale_types'] = $this->db ->order_by('title') ->get(self::saletypes)->result_array();
  //    //        $data['sale_has_products'] = $this->db
  //    //            // ->select('SHP.*, SHP.tax_inclusive')
  //    //            ->where('SHP.sale_id', $id)
  //    //            // ->join('products P', 'P.id = SHP.product_id')
  //    //            ->get(self::salehasproducts.' SHP')->result_array();
  //    //        $data['payment_methods'] = $this->db->get(self::paymenttypes)->result_array();

  //    //        if ($this->input->is_ajax_request()) {
  //    //            exit('Processing...');
  //    //            $values['ticket']           = $this->input->post('ticket', TRUE);
  //    //            $values['location_id']      = $this->input->post('location');
  //    //            $values['discount_amount']  = $this->input->post('discount_amount');
  //    //            $values['discount_type']    = $this->input->post('discount_type');
  //    //            $values['payment_date']     = date('Y-m-d HH:ii:ss', strtotime($this->input->post('payment_date')));
  //    //            $values['payment_type_id']  = $this->input->post('payment_method');
  //    //            $values['desc']             = $this->input->post('desc', TRUE);
  //    //            // $values['sale_type_id']     = 1;
  //    //            // $values['active']           = 1;
  //    //            $values['modified']         = _date();

  //    //            $values['amount'] = 0.00;
  //    //            $values_product_quantity = [];
  //    //            foreach ($this->input->post('quantity') as $product_id => $quantity) {
                    
  //    //                $this->db->where('location_id', $values['location_id']) ->where('product_id', $product_id);
  //    //                if(! $product = $this->db->get(self::locationhasproducts, 1)->row()) messageAJAX( 'error', logs_($this->user->id, 'Invalid Product  for location. ', $values) );
                    
  //    //                $values['amount'] += $product->price * $quantity;

  //    //                $values_product_quantity[] = [  'sale_id'    => 0,
  //    //                                                'product_id' => $product_id,
  //    //                                                'quantity'   => $quantity,
  //    //                                                'created'    => $values['modified'],
  //    //                                                'modified'   => $values['modified'],
  //    //                ];
  //    //            }

  //    //            if(!in_array($values['payment_type_id'], array_column($data['payment_methods'], 'id'))) messageAJAX( 'error', logs_($this->user->id, 'Invalid Payment Method. ', $values) );

  //    //            if(!in_array($values['location_id'], $this->user->locations)) messageAJAX( 'error', logs_($this->user->id, 'Invalid Location. ', $values) );

  //    //            $this->db->like('title', $values['ticket'], 'AFTER');
  //    //            if(! $this->db->get(self::photos, 1)->row()) messageAJAX( 'error', logs_($this->user->id, 'Invalid Ticket Number. ', $values) );

  //    //            if( $this->db->update(self::sales, $values) ){
					
		// 			// $this->db->where('sale_id', $id);
		// 			// $this->db->delete(self::salehasproducts);
					
  //    //                foreach ($values_product_quantity as $key => $value) {
                        
  //    //                    $values_product_quantity[$key]['sale_id'] = $id;

  //    //                    $this->db->insert(self::salehasproducts, $values_product_quantity[$key]); 
  //    //                }
                    

  //    //                messageAJAX( 'success', logs_($this->user->id, 'Sale has been edited.', ['id'=>$id]+$values) );
  //    //            }

  //    //        } else {

  //    //            return $data;
  //    //        }
	 //    }

  //       // function view($id)
  //       // {
  //           // pr($id,1);
  //       // }
		
		// function delete($id, $values)
	 //    {
		// 	$id = (int) $id;
			
		// 	$values->sale_has_products = $this->db ->where('sale_id', $id) ->get(self::salehasproducts)->result_array();
			
	 //    	$this->db->where('id', $id);
  //           if( $this->db->delete(self::sales) ) messageAJAX('success', logs_($this->user->id, 'Sale has been deleted.', $values));
	 //    }

  //       function _calculate($location = FALSE, $products = [], $payments = [])
  //       {
  //           $data[ 'total' ]                 = 0.00;
  //           $data[ 'total_formated' ]        = 0.00;
  //           $data[ 'subtotal' ]              = 0.00;
  //           $data[ 'subtotal_formated' ]     = 0.00;
  //           $data[ 'tax' ]                   = 0.00;
  //           $data[ 'tax_formated' ]          = 0.00;
  //           $data[ 'paid' ]                  = 0.00;
  //           $data[ 'paid_formated' ]         = 0.00;
  //           $data[ 'paid_cash' ]             = 0.00;
  //           $data[ 'paid_cash_formated' ]    = 0.00;
  //           $data[ 'paid_credit' ]           = 0.00;
  //           $data[ 'paid_credit_formated' ]  = 0.00;
  //           $data[ 'paid_voucher' ]          = 0.00;
  //           $data[ 'paid_voucher_formated' ] = 0.00;
  //           $data[ 'rest' ]                  = 0.00;
  //           $data[ 'rest_formated' ]         = 0.00;
  //           $data[ 'location_currency' ]     = $this->user->locations_array[ $location ][ 'currency' ];
  //           $data[ 'location_tax' ]          = $this->user->locations_array[ $location ][ 'tax' ];


  //           // CALCULATE PRODUCTS
  //           foreach ($products as $product => $quantity) {

  //               $this->load->model('Products_model');
  //               $this->db
  //                   ->where_in('P.id', (int) $product)
  //                   ->limit(1);
  //               $_products = $this->Products_model->index( $location ) OR messageAJAX('error', 'Invalid product');

  //               $_product = $_products[ 0 ];

  //               $calc_tax = $_product->price * $data[ 'location_tax' ] / 100;

  //               $quantity = is_numeric($quantity) ? $quantity : 1;

  //               $data[ 'subtotal' ] += round($_product->price * $quantity, 2);
  //           }

  //           $data[ 'subtotal_formated' ] += number_format($data[ 'subtotal' ], 2);

  //           // TOTAL TAX
  //           $data[ 'tax' ]          = $data[ 'subtotal' ] * $data[ 'location_tax' ] / 100;
  //           $data[ 'tax_formated' ] = number_format($data[ 'tax' ], 2);


  //           // TOTAL AMOUNT
  //           $data[ 'total' ]          = round($data[ 'subtotal' ] + $data[ 'tax' ], 2);
  //           $data[ 'total_formated' ] = number_format($data[ 'total' ], 2 );



  //           // CALCULATE PAYMENTS
  //           if (isset($payments[ 'cash' ])) {

  //               foreach ($payments[ 'cash' ] as $payment) {

  //                   $data[ 'paid_cash' ] += doubleval( str_replace( ',', '', $payment ) );

  //               }
  //           }
  //           if (isset($payments[ 'credit' ])) {

  //               foreach ($payments[ 'credit' ] as $payment) {

  //                   $data[ 'paid_credit' ] += doubleval( str_replace( ',', '', $payment ) );

  //               }
  //           }
  //           if (isset($payments[ 'voucher' ])) {

  //               foreach ($payments[ 'voucher' ] as $payment) {

  //                   $data[ 'paid_voucher' ] += doubleval( str_replace( ',', '', $payment ) );

  //               }
  //           }

  //           $data[ 'paid_cash' ]             = round($data[ 'paid_cash' ], 2);
  //           $data[ 'paid_credit' ]           = round($data[ 'paid_credit' ], 2);
  //           $data[ 'paid_voucher' ]          = round($data[ 'paid_voucher' ], 2);
            
  //           $data[ 'paid_cash_formated' ]    = number_format($data[ 'paid_cash' ], 2);
  //           $data[ 'paid_credit_formated' ]  = number_format($data[ 'paid_credit' ], 2);
  //           $data[ 'paid_voucher_formated' ] = number_format($data[ 'paid_voucher' ], 2);

  //           $data[ 'paid' ]                  = $data[ 'paid_cash' ] + $data[ 'paid_credit' ] + $data[ 'paid_voucher' ];
  //           $data[ 'paid_formated' ]         = number_format($data[ 'paid' ], 2);
            
  //           $data[ 'rest' ]                  = $data[ 'total' ] - $data[ 'paid' ];
  //           $data[ 'rest_formated' ]         = number_format($data[ 'rest' ], 2);
            
  //           $data[ 'status' ]                = ($data[ 'paid' ] >= $data[ 'total' ]) ? false : true;

  //           return $data;
  //       }

  //       function getSaleData($sale_id = FALSE)
  //       {
  //           $data[ 'sale' ] = $this->db
  //               ->where('id', $sale_id)
  //               ->where('active !=', 3)
  //           ->get(self::sales, 1)->row();

  //           if ($data[ 'sale' ]) {
                
  //               $data[ 'products' ] = $this->db
  //                  ->where('sale_id', $sale_id)
  //                  ->where('active !=', 3)
  //               ->get(self::salehasproducts, 1)->result();

                
  //           }
  //       }
	}    
?>
