<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Stocks_M extends M_Model {
	
	    public $table = 'stocks';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_one['product'] = [
                'foreign_model' => 'Products_M',
                'foreign_table' => 'products',
                'foreign_key'   => 'id',
                'local_key'     => 'product_id'
            ];

            $this->has_many_pivot['products'] = [
                'foreign_model'     =>'Products_M',
                'pivot_table'       =>'product_has_stocks',
                'local_key'         =>'id',
                'pivot_local_key'   =>'stock_id',
                'pivot_foreign_key' =>'prodcut_id',
                'foreign_key'       =>'id',
            ];  

            $this->rules = [
                'insert' => [
                   
                ],
                'update' => [
                   
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }
	}    
?>
