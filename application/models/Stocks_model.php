<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Stocks_Model extends CI_Model {
	
	    public $table = 'stocks';

	    function __construct()
	    {
            $unit_keys = implode(',', array_keys($this->units));

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'quantity' => [
                        'field' => 'quantity',
                        'label' => 'Adet',
                        'rules' => 'trim|is_decimal',
                    ],
                    'sub_limit' => [
                        'field' => 'sub_limit',
                        'label' => 'Alt limit',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'unit' => [
                        'field' => 'unit',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$unit_keys.']',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Locaiton',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                     'quantity' => [
                        'field' => 'quantity',
                        'label' => 'Adet',
                        'rules' => 'trim|is_decimal',
                    ],
                    'sub_limit' => [
                        'field' => 'sub_limit',
                        'label' => 'Alt limit',
                        'rules' => 'required|trim|is_decimal',
                    ],
                    'unit' => [
                        'field' => 'unit',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$unit_keys.']',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function select($select)
        {
            $this->db->select($select);
            return $this;
        }

        public function getAll($conditions = [])
        {
            return $this->_get($conditions)->result();
        }

        public function get($conditions = [])
        {
            return $this->_get($conditions)->row();
        }

        private function _get($conditions = [])
        {
            if ($conditions) {
                $this->db->where($conditions);
            }
            return $this->db->where('active !=', 3)->get($this->table);
        }

        public function findOrFail($conditions = [])
        {
            if (!is_array($conditions)) {
                $conditions = [
                    'id' => (int) $conditions
                ];
            }

            if (!$data = $this->get($conditions)) {
                if ($this->input->is_ajax_request()) {
                    messageAJAX('error', 'İçerik bulunamadı.');
                }

                show_404();
            }

            return $data;
        }

        public function insert($aditional_data = [])
        {
            if ($aditional_data) {
                $aditional_data['created'] = date('Y-m-d H:i:s');
                $this->db->insert($this->table, $aditional_data);
                return $this->db->insert_id();
            }

            $this->form_validation->set_rules($this->rules['insert']);
            if($this->form_validation->run()) {
                
                $data['created'] = date('Y-m-d H:i:s');
                foreach ($this->rules['insert'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

                $this->db->insert($this->table, $data);

                return $this->db->insert_id();
            }
        }

        public function update($conditions = [], $aditional_data = [])
        {
            if ($aditional_data) {
                $aditional_data['modified'] = date('Y-m-d H:i:s');
                $this->db->where($conditions);
                $this->db->update($this->table, $aditional_data);
                return true;
            }

            $this->form_validation->set_rules($this->rules['update']);
            
            if($this->form_validation->run()) {
                
                $data['modified'] = date('Y-m-d H:i:s');
                foreach ($this->rules['update'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

               $this->db->where($conditions);
               $this->db->update($this->table, $data);

                return true;
            }
        
        }    

        public function delete($conditions = [])
        {
            $this->db->where($conditions);
            $this->db->update($this->table,[
                'active' => 3
            ]);
        }    

	}    
?>
