<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductTypes_M extends M_Model {
	
	    public $table = 'product_types';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['subCategories'] = [
                'foreign_model' => 'ProductTypes_M',
                'foreign_table' => 'product_types',
                'foreign_key'   => 'parent_id',
                'local_key'     => 'id'
            ];

            $this->has_many['products'] = [
                'foreign_model' => 'Products_M',
                'foreign_table' => 'products',
                'foreign_key'   => 'product_type_id',
                'local_key'     => 'id'
            ];

            $this->rules = [
                // 'insert' => [
                //     'title' => [
                //         'field' => 'title',
                //         'label' => 'Başlık',
                //         'rules' => 'required|trim|xss_clean',
                //     ],
                //     'description' => [
                //         'field' => 'description',
                //         'label' => 'Açıklama',
                //         'rules' => 'trim|xss_clean',
                //     ],
                //     'active' => [
                //         'field' => 'active',
                //         'label' => 'Durum',
                //         'rules' => 'required|trim|in_list[1,0]',
                //     ],
                // ],
                // 'update' => [
                //     'title' => [
                //         'field' => 'title',
                //         'label' => 'Başlık',
                //         'rules' => 'required|trim|xss_clean',
                //     ],
                //     'description' => [
                //         'field' => 'description',
                //         'label' => 'Açıklama',
                //         'rules' => 'trim|xss_clean',
                //     ],
                //     'active' => [
                //         'field' => 'active',
                //         'label' => 'Durum',
                //         'rules' => 'required|trim|in_list[1,0]',
                //     ],
                // ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function findCategoriesWithProducts($where_products = [])
        {
            $categories = $this->ProductTypes_M->get_all([
                'parent_id !=' => null,
            ]);

            if (!$categories) {
                return [];
            }

            $products = $this->db
            ->where($where_products)
            ->where('active', 1)
            ->where('portion_id', null)
            ->get('products')
            ->result();

            $data = [];
            foreach ($categories as $category) {

                $category->products = [];

                // Products
                foreach ($products as $product) {
                    if ($category->id == $product->product_type_id) {
                        $category->products[$product->id] = $product;
                    }
                }

                $data[$category->id] = $category;
            }

            return $data;
        }

        public function findCategoriesWithProductsAndRelatedItems($where_products = [])
        {
            $categories = $this->ProductTypes_M->get_all([
                'parent_id !=' => null,
            ]);

            if (!$categories) {
                return [];
            }

            $category_ids = array_map(function ($row) {
                return $row->id;
            }, $categories);

            $products = $this->db
            ->where($where_products)
            ->where('active', 1)
            ->where('portion_id', null)
            ->where_in('product_type_id', $category_ids)
            ->get('products')
            ->result();

            if ($products) {

                $product_ids = array_map(function ($row) {
                    return $row->id;
                }, $products);
                

                $additional_products = $this->db
                ->where('products.active', 1)
                ->where_in('products_additional_products.product_id', $product_ids)
                ->join('products_additional_products', 'products_additional_products.additional_product_id = products.id')
                ->get('products')
                ->result();

                $stocks = $this->db
                ->select('stocks.*, product_has_stocks.product_id, product_has_stocks.quantity product_stock_quantity, product_has_stocks.optional')
                ->where('stocks.active', 1)
                ->where_in('product_has_stocks.product_id', $product_ids)
                ->join('product_has_stocks', 'product_has_stocks.stock_id = stocks.id')
                ->get('stocks')
                ->result();

                $portion_products = $this->db
                ->where('active', 1)
                ->where_in('portion_id', $product_ids)
                ->get('products')
                ->result();


                foreach ($products as &$product) {

                    $product->additional_products = [];
                    $product->stocks = [];
                    $product->portions = [];
                    
                    // Additional prodcuts
                    foreach ($additional_products as $additional_product) {
                        if ($product->id == $additional_product->product_id) {
                            $product->additional_products[$additional_product->id] = $additional_product;
                        }
                    }

                    // Stocks
                    foreach ($stocks as $stock) {
                        if ($product->id == $stock->product_id) {
                            $product->stocks[$stock->id] = $stock;
                        }
                    }

                    // Portions
                    foreach ($portion_products as $portion_product) {
                        if ($product->id == $portion_product->portion_id) {
                            $product->portions[$portion_product->id] = $portion_product;
                        }
                    }
                }
                unset($product);
            }

            $data = [];
            foreach ($categories as $category) {

                $category->products = [];

                // Products
                foreach ($products as $product) {
                    if ($category->id == $product->product_type_id) {
                        $category->products[$product->id] = $product;
                    }
                }

                $data[$category->id] = $category;
            }

            return $data;
        }
	}    
?>
