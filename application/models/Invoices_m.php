<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Invoices_M extends M_Model {
	
	    public $table = 'invoices';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->soft_deletes = true;

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'description' => [
                        'field' => 'description',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'json' => [
                        'field' => 'json',
                        'label' => 'json',
                        'rules' => 'required|trim',
                    ],
                    'width' => [
                        'field' => 'width',
                        'label' => 'Kağıt Genişliği',
                        'rules' => 'required|trim|is_money'
                    ],

                    'height' => [
                        'field' => 'height',
                        'label' => 'Kağıt Yüksekliği',
                        'rules' => 'required|trim|is_money'
                    ],

                    'default' => [
                        'field' => 'default',
                        'label' => 'Ön tanımlı',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'description' => [
                        'field' => 'description',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],

                    'width' => [
                        'field' => 'width',
                        'label' => 'Kağıt Genişliği',
                        'rules' => 'required|trim|is_money'
                    ],

                    'height' => [
                        'field' => 'height',
                        'label' => 'Kağıt Yüksekliği',
                        'rules' => 'required|trim|is_money'
                    ],
                    'json' => [
                        'field' => 'json',
                        'label' => 'json',
                        'rules' => 'required|trim',
                    ],
                    'default' => [
                        'field' => 'default',
                        'label' => 'Ön tanımlı',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }
	}    
?>
