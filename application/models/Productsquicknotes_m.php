<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class ProductsQuickNotes_M extends M_Model {
	
	    public $table = 'products_quick_notes';
        public $soft_deletes = false;
        public $timestamps = false;

	    public function __construct()
	    {
	    	parent::__construct();
	    }
	    
	}    
