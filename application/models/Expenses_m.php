<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Expenses_M extends M_Model {
	
	    public $table = 'expenses';

	    public function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $tax_keys = implode(',', array_keys($this->taxes));

            $this->rules = [
                'insert' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'expense_type_id' => [
                        'field' => 'expense_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[expense_types.id]',
                    ],
                    'payment_type_id' => [
                        'field' => 'payment_type_id',
                        'label' => 'Ödeme şekli',
                        'rules' => 'required|trim|in_list[1,2]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'İsim',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$tax_keys.']',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'has_stock' => [
                        'field' => 'has_stock',
                        'label' => 'has_stock',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'location_id' => [
                        'field' => 'location_id',
                        'label' => 'Restoran',
                        'rules' => 'required|trim|search_table[locations.id]',
                    ],
                    'user_id' => [
                        'field' => 'user_id',
                        'label' => 'user_id',
                        'rules' => 'required|trim|search_table[users.id]',
                    ],
                    'expense_type_id' => [
                        'field' => 'expense_type_id',
                        'label' => 'Kategori',
                        'rules' => 'required|trim|search_table[expense_types.id]',
                    ],
                    'title' => [
                        'field' => 'title',
                        'label' => 'İsim',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'desc' => [
                        'field' => 'desc',
                        'label' => 'Açıklama',
                        'rules' => 'trim|xss_clean',
                    ],
                    'amount' => [
                        'field' => 'amount',
                        'label' => 'Fiyat',
                        'rules' => 'required|trim|is_money',
                    ],
                    'tax' => [
                        'field' => 'tax',
                        'label' => 'Ölçü birimi',
                        'rules' => 'required|trim|in_list['.$tax_keys.']',
                    ],
                    'payment_date' => [
                        'field' => 'payment_date',
                        'label' => 'Ödeme tarihi',
                        'rules' => 'required|trim|is_date['.dateFormat().']',
                    ],
                    'payment_type_id' => [
                        'field' => 'payment_type_id',
                        'label' => 'Ödeme şekli',
                        'rules' => 'required|trim|in_list[1,2]',
                    ],
                    'has_stock' => [
                        'field' => 'has_stock',
                        'label' => 'has_stock',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }
	}    
?>
