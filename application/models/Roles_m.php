<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Roles_M extends M_Model {
	
	    public $table = 'roles';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->before_get = ['_whereNonDeleted'];

            $this->has_many['users'] = [
                'foreign_model' => 'Users_M',
                'foreign_table' => 'users',
                'foreign_key'   => 'role_id',
                'local_key'     => 'id'
            ];

            $this->has_many_pivot['permissions'] = [
                'foreign_model'     =>'Permissions_M',
                'pivot_table'       =>'role_has_permissions',
                'local_key'         =>'id',
                'pivot_local_key'   =>'role_id',
                'pivot_foreign_key' =>'permission_id',
                'foreign_key'       =>'id',
            ];

            $this->rules = [
                'insert' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'alias' => [
                        'field' => 'alias',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|is_unique[roles.alias]',
                    ],
                    'editable' => [
                        'field' => 'editable',
                        'label' => 'editable',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'title' => [
                        'field' => 'title',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'alias' => [
                        'field' => 'alias',
                        'label' => 'Başlık',
                        'rules' => 'required|trim|is_unique_update[roles.alias.'.$this->uri->segment(3).']',
                    ],
                    'editable' => [
                        'field' => 'editable',
                        'label' => 'editable',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _whereNonDeleted()
        {
            $this->where('active !=', 3);
        }

        public function hasPermission($role_id, $permission_action)
        {
            $role_permission = $this->with_permissions([
                'where' => [
                    'permissions.action' => $permission_action,
                    'permissions.active' => 1,
                ],
                'limt' => 1
            ])->get([
                'id' => $role_id,
                'active' => 1,
            ]);

            return (bool) $role_permission->permissions;
        }

        public function findPermissionsList($role_id)
        {
            $role_permissions = $this->with_permissions([
                'fields' => ['(SELECT action FROM permissions where id = permission_id) as action'],
                'where' => [
                    'permissions.active' => 1,
                ],
                'limt' => 1
            ])->get([
                'id' => $role_id,
                'active' => 1,
            ]);


            $temp = [];
            foreach ($role_permissions->permissions as $key => $value) {
                $temp[] = $value->action;
            }

           return $temp;
        }
	}    
?>
