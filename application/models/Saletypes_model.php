<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class SaleTypes_Model extends CI_Model {

        const saletypes = 'sale_types';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index()
        {
            return $this->db->get(self::saletypes)->result();
        }

	    function add()
	    {
            if ($this->input->is_ajax_request()) {
    
                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['active']   = $this->input->post('active');
                $values['created']  = _date();

                if( $this->db->insert(self::saletypes, $values) ){

                    $id = $this->db->insert_id();

                    messageAJAX( 'success', logs_($this->user->id, 'Satış türü oluşturuldu.', ['id'=>$id]+$values) );
                }

            }
	    }

        function edit($id)
        {
            $id = (int) $id;

            if ($this->input->is_ajax_request()) {

                $values['title']    = $this->input->post('title', TRUE);
                $values['desc']     = $this->input->post('desc', TRUE);
                $values['active']   = $this->input->post('active');
                $values['modified'] = _date();

                $this->db->where('id', $id);
                if( $this->db->update(self::saletypes, $values) ){

                    messageAJAX( 'success', logs_($this->user->id, 'Satış türü güncellendi.', $values) );
                }
                
            } else {
                
                
            }
            
        }

        function delete($id, $data)
        {
            $id = (int) $id;

            $this->db->where('id', $id);
            if( $this->db->delete(self::saletypes) ) {

                messageAJAX( 'success', logs_($this->user->id, 'Satış türü silindi. ', $data) );
            }
        }
	}    
?>
