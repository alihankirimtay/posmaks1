<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Locations_Model extends CI_Model {
	
	    const locations = 'locations';
	    const userhaslocations = 'user_has_locations';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index()
	    {
	    	$this->db->where('active !=', 3);
	    	return $this->db->get(self::locations)->result();
	    }

	    function add()
	    {
			$values['title']                 = $this->input->post('title', TRUE);
			$values['desc']                  = $this->input->post('desc', TRUE);
			$values['currency']              = $this->input->post('currency', TRUE);
			$values['prepaid_limit']         = $this->input->post('prepaid_limit');
			$values['force_sale_via_ticket'] = $this->input->post('force_sale_via_ticket');
			$values['autoalbum_active']      = $this->input->post('autoalbum_active');
			$values['autoalbum_time']        = $this->input->post('autoalbum_time');
			$values['dtz']                   = $this->input->post('dtz');
			$values['active']                = $this->input->post('active');
			$values['created']               = _date();

            if( $this->db->insert(self::locations, $values) ) {
				
				$id = $this->db->insert_id();

				$this->db->insert('points', [
					'location_id' => $id,
					'title' => 'SERVICE DESK FOR ' . $values['title'],
					'created' => $values['created'],
					'is_service' => 1,
					'active' => 1,
				]);

				messageAJAX('success', logs_($this->user->id, 'Restoran oluşturuldu.', ['id'=>$id]+$values));
				
			}
	    }

	    function edit($id)
	    {
			$values['title']                 = $this->input->post('title', TRUE);
			$values['desc']                  = $this->input->post('desc', TRUE);
			$values['currency']              = $this->input->post('currency', TRUE);
			$values['prepaid_limit']         = $this->input->post('prepaid_limit');
			$values['force_sale_via_ticket'] = $this->input->post('force_sale_via_ticket');
			$values['autoalbum_active']      = $this->input->post('autoalbum_active');
			$values['autoalbum_time']        = $this->input->post('autoalbum_time');
			$values['dtz']                   = $this->input->post('dtz');
			$values['active']                = $this->input->post('active');
			$values['modified']              = _date();

       		$this->db->where('id', $id);
            if( $this->db->update(self::locations, $values) ) messageAJAX('success', logs_($this->user->id, 'Restoran güncellendi.', ['id'=>$id]+$values));
	    }

	    function delete($id, $values)
	    {
	    	$this->db->where('id', $id);
            if( $this->db->update(self::locations, [ 'active' => 3, 'modified' => _date() ]) ) {
            	messageAJAX('success', logs_($this->user->id, 'Restoran silindi.', $values));
            }
	    }

	    // FUNCTIONS
	    function locationsByUser($location = NULL)
	    {
	    	$location = (int) $location;
	    	
	    	$this->db->select('L.*');
	    	if((int)$this->user->role_id !== 1) {

	    		$this->db->where('UHL.user_id', $this->user->id);
	    		$this->db->join(self::userhaslocations.' UHL', 'UHL.location_id = L.id');

	    		if($location) {

	    			$this->db->where('UHL.location_id', $location);
	    		}
	    	}
	    	return $this->db->get(self::locations.' L');
	    }
	}    
?>
