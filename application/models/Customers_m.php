<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Customers_M extends M_Model {
	
	    public $table = 'customers';

	    function __construct()
	    {
            $this->before_create = [];
            $this->before_update = [];
            $this->soft_deletes = true;
            
            $this->rules = [
                'insert' => [
                    'name' => [
                        'field' => 'name',
                        'label' => 'Ünvan / Ad Soyad',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'phone' => [
                        'field' => 'phone',
                        'label' => 'Telefon',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'address' => [
                        'field' => 'address',
                        'label' => 'Adres',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'tax_office' => [
                        'field' => 'tax_office',
                        'label' => 'Vergi Dairesi',
                        'rules' => 'trim|xss_clean',
                    ],
                    'tax_no' => [
                        'field' => 'tax_no',
                        'label' => 'Vergi Numarası',
                        'rules' => 'trim|xss_clean',
                    ],
                    'isSupplier' => [
                        'field' => 'isSupplier',
                        'label' => '',
                        'rules' => 'trim|xss_clean',
                    ],
                    'ys_id' => [
                        'field' => 'ys_id',
                        'label' => 'Customer Id',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'name' => [
                        'field' => 'name',
                        'label' => 'Ünvan / Ad Soyad',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'phone' => [
                        'field' => 'phone',
                        'label' => 'Telefon',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'tax_office' => [
                        'field' => 'tax_office',
                        'label' => 'Vergi Dairesi',
                        'rules' => 'trim|xss_clean',
                    ],
                    'tax_no' => [
                        'field' => 'tax_no',
                        'label' => 'Vergi Numarası',
                        'rules' => 'trim|xss_clean',
                    ],
                    'isSupplier' => [
                        'field' => 'isSupplier',
                        'label' => '',
                        'rules' => 'trim|xss_clean',
                    ],
                    'address' => [
                        'field' => 'address',
                        'label' => 'Adres',
                        'rules' => 'required|trim|xss_clean',
                    ],
                    'ys_id' => [
                        'field' => 'ys_id',
                        'label' => 'Customer Id',
                        'rules' => 'trim|xss_clean',
                    ],
                    'active' => [
                        'field' => 'active',
                        'label' => 'Durum',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }
	}    
?>
