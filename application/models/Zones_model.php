<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Zones_Model extends CI_Model {
	
	    const zones = 'zones';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	if($location && !in_array($location, $this->user->locations)) error_('access');

	    	$this->db->select('Z.*, (SELECT title FROM floors WHERE id = Z.floor_id LIMIT 1) AS location');
	    	if($location) {

	    		$this->db->where('F.location_id', $location);
	    	}
    		else {

    			$this->db->where_in('F.location_id', $this->user->locations);
    		}

    		$this->db->join('floors F', 'F.id = Z.floor_id');
    		$this->db->join('locations L', 'L.id = F.location_id');
            $this->db->where('Z.active !=', 3);
            $this->db->where('F.active !=', 3);
	    	return $this->db->get(self::zones.' Z')->result();
	    }

	    function add()
	    {
	    	$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['floor_id']		= (int) $this->input->post('floor');
            $values['created']  	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', $values));
            }


            if( $this->db->insert(self::zones, $values) ) {

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Bölge oluşturuldu. ', $values));
            }
	    }

	    function edit($id)
	    {
    		$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']  = (int) $this->input->post('location');
            $values['floor_id']		= (int) $this->input->post('floor');
            $values['modified'] 	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', ['id'=>$id]+$values));
            }

            
       		$this->db->where('id', $id);
            if( $this->db->update(self::zones, $values) ) {

            	messageAJAX('success', logs_($this->user->id, 'Bölge güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete($id, $data)
	    {
	    	$this->db->where('id', $id);
            if( $this->db->update(self::zones, ['modified' => _date(), 'active' => 3]) ) {

            	messageAJAX('success', logs_($this->user->id, 'Bölge silindi. ', $data));
            }
	    }
	}    
?>
