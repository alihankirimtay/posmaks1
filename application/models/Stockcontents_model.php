<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Stockcontents_Model extends CI_Model {
	
	    const stockcontents = 'stock_contents';
	    const stocks = 'stocks';

	    public $table = 'stock_contents';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($stock)
	    {
	    	$this->db->select('P.*, (SELECT username FROM users WHERE id = P.user_id LIMIT 1) AS user');
	    	return $this->db->where(['active !=' => 3, 'stock_id' => $stock])->get(self::stockcontents.' P')->result();	    	
	    }

	    function add($stock)
	    {
			$values['stock_id'] = (int) $stock->id;
			$values['user_id']  = (int) $this->user->id;
			$values['price']    = doubleval(str_replace(',', '', $this->input->post('price')));
			$values['quantity'] = doubleval(str_replace(',', '', $this->input->post('quantity')));
			$values['desc']     = $this->input->post('desc', TRUE);
			$values['created']  = _date();
			$values['active']   = 1;

            if( $this->db->insert(self::stockcontents, $values) ) {

            	$total = $stock->quantity + $values['quantity'];

            	$this->db->where('id', $stock->id);
            	$this->db->set('quantity', $total);
            	$this->db->update(self::stocks, ['modified' => _date()]);

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Stok işlemi oluşturuldu. ', $values));
            }
	    }

	    function edit($id, $stock, $content)
	    {
			$values['user_id']  = (int) $this->user->id;
			$values['price']    = doubleval(str_replace(',', '', $this->input->post('price')));
			$values['quantity'] = doubleval(str_replace(',', '', $this->input->post('quantity')));
			$values['desc']     = $this->input->post('desc', TRUE);
			$values['modified'] = _date();

       		$this->db->where('id', $id);
            if( $this->db->update(self::stockcontents, $values) ) {

            	$total = ($stock->quantity - $content->quantity) + $values['quantity'];

            	$this->db->where('id', $stock->id);
            	$this->db->set('quantity', $total);
            	$this->db->update(self::stocks, ['modified' => _date()]);

            	messageAJAX('success', logs_($this->user->id, 'Stok işlemi güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete1($stock, $id, $data)
	    {
	    	$this->db->where('id', $id);
	    	$this->db->where('stock_id', $stock);
            if ($this->db->update(self::stockcontents, ['active' => 3, 'modified' => _date()])) {

            	$total = $data['current_stock']->quantity - $data['stock_content']->quantity;

            	$this->db->where('id', $stock);
            	$this->db->set('quantity', $total);
            	$this->db->update(self::stocks, ['modified' => _date()]);

            	messageAJAX('success', logs_($this->user->id, 'Stok işlemi silindi. ', $data));
            }
	    }












	    public function select($select)
        {
            $this->db->select($select);
            return $this;
        }

        public function getAll($conditions = [])
        {
            return $this->_get($conditions)->result();
        }

        public function get($conditions = [])
        {
            return $this->_get($conditions)->row();
        }

        private function _get($conditions = [])
        {
            if ($conditions) {
                $this->db->where($conditions);
            }
            return $this->db->where('active !=', 3)->get($this->table);
        }

        public function findOrFail($conditions = [])
        {
            if (!is_array($conditions)) {
                $conditions = [
                    'id' => (int) $conditions
                ];
            }

            if (!$data = $this->get($conditions)) {
                if ($this->input->is_ajax_request()) {
                    messageAJAX('error', 'İçerik bulunamadı.');
                }

                show_404();
            }

            return $data;
        }

        public function insert($aditional_data = [])
        {
            if ($aditional_data) {
                $aditional_data['created'] = date('Y-m-d H:i:s');
                $this->db->insert($this->table, $aditional_data);
                return $this->db->insert_id();
            }

            $this->form_validation->set_rules($this->rules['insert']);
            if($this->form_validation->run()) {
                
                $data['created'] = date('Y-m-d H:i:s');
                foreach ($this->rules['insert'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

                $this->db->insert($this->table, $data);

                return $this->db->insert_id();
            }
        }

        public function update($conditions = [], $aditional_data = [])
        {
            if ($aditional_data) {
                $aditional_data['modified'] = date('Y-m-d H:i:s');
                $this->db->where($conditions);
                $this->db->update($this->table, $aditional_data);
                return true;
            }

            $this->form_validation->set_rules($this->rules['update']);
            if($this->form_validation->run()) {
                
                $data['modified'] = date('Y-m-d H:i:s');
                foreach ($this->rules['update'] as $input) {
                    $data[$input['field']] = $this->input->post($input['field']);
                }

               $this->db->where($conditions);
               $this->db->update($this->table, $data);

                return true;
            }
        
        }    

        public function delete($conditions = [])
        {
            $this->db->where($conditions);
            $this->db->update($this->table,[
                'active' => 3
            ]);
        }  
	}    
?>
