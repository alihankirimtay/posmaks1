<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Printercounts_Model extends CI_Model {
	
        const printers         = 'printers';
        const printerhascounts = 'printer_has_counts';
        const locationlosts    = 'location_losts';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index($printer = NULL)
        {
            $printer = (int) $printer;

            $this->db->select('PC.*');
            $this->db->where('PC.printer_id', $printer);
            return $this->db->get(self::printerhascounts.' PC')->result();  
        }

	    function add($printer = NULL)
	    {
            $printer = (int) $printer;

            if ($this->input->is_ajax_request()) {

                $values['printer_id'] = $printer;
                $values['count']      = $this->input->post('counter');
                $values['date']       = dateConvert($this->input->post('date'), 'server');
                $values['created']    = _date();

                if( $this->db->insert(self::printerhascounts, $values) ) {

                    $id = $this->db->insert_id();

                    messageAJAX('success', logs_($this->user->id, 'Printer count has been added.', ['id'=>$id]+$values));
                }

            }               
	    }

	    function edit($printer = NULL, $id = NULL)
	    {
            $printer = (int) $printer;
            $id      = (int) $id;

            if ($this->input->is_ajax_request()) {

                $values[ 'count' ]      = $this->input->post('counter');
                $values[ 'date' ]       = dateConvert($this->input->post('date'), 'server');
                $values[ 'modified' ]   = _date();

                $this->db->where('id', $id);
                $this->db->where('printer_id', $printer);
                if( $this->db->update(self::printerhascounts, $values) ) {

                    messageAJAX('success', logs_($this->user->id, 'Printer count has been edited.', [ 'id' => $id ]+$values));
                }
            }
	    }

	    // function delete($id, $data)
	    // {
     //        $id = (int) $id;

	    // 	$this->db->where('id', $id);
     //        if( $this->db->delete(self::printerhascounts) ) {

     //            $data['printer']->printer_has_counts = $data['printer_count'];

     //            messageAJAX('success', logs_($this->user->id, 'Printer count has been deleted. ', $data['printer']));
     //        }
	    // }

        function multiple($location = NULL)
        {
            $location    = (int) $location;
            $date_client = $this->input->post('date');
            $date        = dateConvert($date_client, 'server');
            $data        = [];
            
            $client_day_number = dateReFormat($date_client, dateFormat(), 'Y-m-d');
            $start_of_day      = dateConvert2($client_day_number . ' 00:00:00', 'server');
            $end_of_day        = dateConvert2($client_day_number . ' 23:59:59', 'server');


            $this->db->select('*, (SELECT title FROM '.self::printers.' WHERE id = '.self::printerhascounts.'.printer_id LIMIT 1) as title');
            foreach ($this->input->post('printers[]') as $key => $printer) {
                
                $data['printer_values'][] = [
                    'printer_id' => $printer,
                    'count'      => $this->input->post('values['.$printer.']'),
                    'date'       => $date,
                    'created'    => _date(),
                    'active'     => 1,
                ];

                $this->db
                ->or_group_start()
                    ->where('date >=', $start_of_day)
                    ->where('date <=', $end_of_day)
                    ->where('printer_id', $printer)
                ->group_end();
            }

            if ($alreadyHave = $this->db->get(self::printerhascounts, 1)->row()) {
                messageAJAX('error', logs_($this->user->id, 'The printer "'.$alreadyHave->title.'" already has data for this date.', $alreadyHave));
            }

            $this->db
                ->where('date >=', $start_of_day)
                ->where('date <=', $end_of_day);
            $this->db->where('location_id', $location);
            if ($lost = $this->db->get(self::locationlosts, 1)->row()) {
                messageAJAX('error', logs_($this->user->id, 'The Location already has waste data for this date.', $alreadyHave));
            }


            $this->db->insert_batch(self::printerhascounts, $data['printer_values']);
            $this->db->insert(self::locationlosts, [
                'location_id' => $location,
                'unsold'      => $this->input->post('unsold'),
                'waste'       => $this->input->post('waste'),
                'comps'       => $this->input->post('comps'),
                'unseen'      => $this->input->post('unseen'),
                'date'        => $date,
                'created'     => _date(),
                'active'      => 1,
            ]);

            messageAJAX('success', 'Success');

        }
	}    
?>
