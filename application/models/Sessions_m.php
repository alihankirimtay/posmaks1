<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Sessions_M extends M_Model {
		
		public $table = 'sessions';
		public $soft_deletes = true;

	    public function __construct()
	    {
	    	$this->before_create = [];
            $this->before_update = [];

            $this->rules = [
                'insert' => [
                   
                ],
                'update' => [
                   
                ]
            ];

	    	parent::__construct();
	    }

	    public function createOnAvailableLocationsForUser($user, $location_id = null)
	    {
	    	$datetime = date('Y-m-d H:i:s');

	    	if ($location_id) {
	    		$this->db->where('locations.id', $location_id);
	    	}

	    	if ($user->role_alias == 'admin') {

		    	$locations = $this->db->
		    	where('active', 1)
		    	->get('locations')
		    	->result();
	    	} else {

	    		$locations = $this->db
		    	->select('locations.id')
		    	->where([
		    		'user_has_locations.user_id' => $user->id,
		    		'locations.active' => 1,
		    	])
		    	->join('locations', 'locations.id = user_has_locations.location_id')
		    	->get('user_has_locations')
		    	->result();
	    	}

	    	if (!$locations) {
	    		return false;
	    	}

	    	$location_ids = array_map(function ($row) {
	    		return $row->id;
	    	}, $locations);

	    	// Find Open sessions
	    	$sessions = $this->db
	    	->where([
	    		'user_id' => $user->id,
	    		'deleted' => null,
	    	])
	    	->group_start()
	    		->where('status', null)
	    		->or_where('status', 0)
	    	->group_end()
	    	->where_in('location_id', $location_ids)
	    	->get($this->table)
	    	->result();

	    	$insert_sessions = [];
	    	foreach ($locations as $location) {

	    		$sessionExists = false;
	    		foreach ($sessions as $session) {
	    			if ($location->id == $session->location_id) {
	    				$sessionExists = true;
	    				break;
	    			}
	    		}

	    		if (!$sessionExists) {
	    			$insert_sessions[$location->id] = [
		    			'location_id' => $location->id,
		    			'user_id' => $user->id,
		    			'created' => $datetime,
		    			'closed' => $datetime,
		    		];
	    		}
	    	}

	    	if ($insert_sessions) {
	    		$this->db->insert_batch($this->table, $insert_sessions);
	    	}

	    	return true;
		}
		
		public function findOpenSession($user_id)
	    {
	    	return $this->db
	    	->where([
	    		'user_id' => $user_id,
	    		'deleted' => null,
	    	])
	    	->group_start()
	    		->where('status', null)
	    		->or_where('status', 0)
	    	->group_end()
	    	->order_by('id', 'desc')
	    	->get($this->table, 1)->row();
	    }

	    public function findCashTypes()
	    {
	    	$cash_types = [
	            '200' => 200,
	            '100' => 100,
	            '50' => 50,
	            '20' => 20,
	            '10' => 10,
	            '5' => 5,
	            '2' => 2,
	            '1' => 1,
	            '0_50' => 0.50,
	            '0_25' => 0.25,
	            '0_10' => 0.10,
	            '0_05' => 0.05,
	            '0_01' => 0.01,
	        ];

	        return $cash_types;
	    }
	}