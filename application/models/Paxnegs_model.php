<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Paxnegs_Model extends CI_Model {
	
        const paxnegs = 'paxnegs';

	    function __construct()
	    {
	    	parent::__construct();
	    }


        function index($location = NULL)
        {
            $location = (int) $location;

            if($location && !in_array($location, $this->user->locations)) error_('access');

            $this->db->select('P.*, (SELECT title FROM locations WHERE id = P.location_id LIMIT 1) AS location');

            ($location)
            ? $this->db->where('P.location_id', $location)
            : $this->db->where_in('P.location_id', $this->user->locations);
            return $this->db->get(self::paxnegs.' P')->result();  
        }

	    function add($location = NULL)
	    {
            if ($this->input->is_ajax_request()) {

                $values['location_id'] = $location;
                $values['pax']         = $this->input->post('pax');
                $values['negs']        = $this->input->post('negs');
                $values['date']        = dateConvert($this->input->post('date'), 'server');
                $values['created']     = _date();
                $values['active']      = 1;

                if($location && !in_array($location, $this->user->locations)) {
                    messageAJAX('error', logs_($this->user->id, 'Permission denied.', $values));
                }

                if( $this->db->insert(self::paxnegs, $values) ) {

                    $id = $this->db->insert_id();

                    messageAJAX('success', logs_($this->user->id, 'Pax & Negs has been added. ', ['id'=>$id]+$values));
                }

            }               
	    }

	    function edit($location = NULL, $id = NULL)
	    {
            $location = (int) $location;
            $id = (int) $id;

            if ($this->input->is_ajax_request()) {
                
                $values['pax']      = $this->input->post('pax');
                $values['negs']     = $this->input->post('negs');
                $values['date']     = dateConvert($this->input->post('date'), 'server');
                $values['modified'] = _date();

                $this->db->where('id', $id);
                if( $this->db->update(self::paxnegs, $values) ) {

                    messageAJAX('success', logs_($this->user->id, 'Pax & Negs has been edited.', ['id'=>$id]+$values));
                }

            }
	    }

	    // function delete($id = NULL, $paxnegs)
	    // {
     //        $id = (int) $id;
            
	    // 	$this->db->where('id', $id);
     //        if( $this->db->delete(self::paxnegs) ) {

     //            messageAJAX('success', logs_($this->user->id, 'Pax & Negs has been deleted. ', $paxnegs));
     //        }
	    // }
	}    
?>
