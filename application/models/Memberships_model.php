<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Memberships_Model extends CI_Model {

    public $errorMessage = null;

    function __construct()
    {
        parent::__construct();
    }

    public function findAll()
    {
        return $this->apiRequest('memberships');
    }

    public function findPayments()
    {
        return $this->apiRequest('membershipsPayments');
    }

    public function getCheckOutForm($inputs)
    {
        return $this->apiRequest('membershipsCheckOut', $inputs);
    }

    public function findSettings()
    {
        return $this->apiRequest('membershipsGetSettings');
    }

    public function updateSettings($inputs)
    {
        return $this->apiRequest('membershipsUpdateSettings', $inputs);
    }

    public function subscription($data)
    {
        return $this->apiRequest('membershipsSubscription', $data);
    }

    public function Discount($data)
    {
        return $this->apiRequest('membershipsDiscount', $data);
    }

    public function mailSettings()
    {
        return $this->apiRequest('mailSettings');
    }

    private function apiRequest($url, $inputs = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, posmaksCmsUrl("api/?{$url}&api_token={$this->website->api_token}"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($inputs));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close($ch);



        $data = (Array) json_decode($server_output, true);


        $data = (Object) array_merge([
            'status' => 'ERROR',
            'message' => '500 Internal Server Error!',
            'items' => []
        ], $data);


        if ($data->status != 'SUCCESS') {
            $this->errorMessage = $data->message;
            return [];
        }

        return $data->items;
    }

}    
?>
