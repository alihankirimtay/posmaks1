<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class SaleProductNotes_M extends M_Model {
	
	    public $table = 'sale_product_notes';

        private $sound_note_path = 'assets/uploads/sound_notes/';

	    function __construct()
	    {
            $this->before_create = ['_setSound'];
            $this->before_update = ['_setSound'];
            $this->before_get = [];

            $this->rules = [
                'insert' => [
                    'sale_product_id' => [
                        'field' => 'sale_product_id',
                        'label' => 'Ürün',
                        'rules' => 'trim|search_table[sale_has_products.id]',
                    ],
                    'note' => [
                        'field' => 'note',
                        'label' => 'Not',
                        'rules' => 'trim|xss_clean',
                    ],
                    'sound' => [
                        'field' => 'sound',
                        'label' => 'Ses',
                        'rules' => 'trim',
                    ],
                    'type' => [
                        'field' => 'type',
                        'label' => 'Type',
                        'rules' => 'required|trim|in_list[1,2]',
                    ],
                    'viewed' => [
                        'field' => 'viewed',
                        'label' => 'Görüldü',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ],
                'update' => [
                    'sale_product_id' => [
                        'field' => 'sale_product_id',
                        'label' => 'Ürün',
                        'rules' => 'trim|search_table[sale_has_products.id]',
                    ],
                    'note' => [
                        'field' => 'note',
                        'label' => 'Not',
                        'rules' => 'trim|xss_clean',
                    ],
                    'sound' => [
                        'field' => 'sound',
                        'label' => 'Ses',
                        'rules' => 'trim',
                    ],
                    'type' => [
                        'field' => 'type',
                        'label' => 'Type',
                        'rules' => 'required|trim|in_list[1,2]',
                    ],
                    'viewed' => [
                        'field' => 'viewed',
                        'label' => 'Görüldü',
                        'rules' => 'required|trim|in_list[1,0]',
                    ],
                ]
            ];

	    	parent::__construct();
	    }

        public function _setSound($data)
        {
            if (!isset($_FILES['sound'])) {
                return $data;
            }
            
            @mkdir(FCPATH . $this->sound_note_path);

            $this->load->library('upload', [
                'encrypt_name' => true,
                'upload_path' => FCPATH . $this->sound_note_path,
                'allowed_types' => 'mp3|wav'
            ]);

            if (!$this->upload->do_upload('sound')) {
                $this->form_validation->set_error($this->upload->display_errors());
                return false;
            }
            $data['sound'] = $this->sound_note_path . $this->upload->data()['file_name'];

            return $data;
        }

        public function requiredNote()
        {
            $this->rules['insert']['note']['rules'] .= '|required';
            $this->rules['update']['note']['rules'] .= '|required';
            return $this;
        }
	}    
?>
