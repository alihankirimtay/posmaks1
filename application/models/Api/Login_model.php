<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Login_Model extends CI_Model {

		const users = 'users';
		const userhaslocations = 'user_has_locations';

		function __construct()
		{
			parent::__construct();
		}

		function index($data)
		{
            $data['password'] = sha1( $data['password'] . $this->config->config['salt'] );

            $this->db->select('id, username, password');
			$this->db->where('active', 1);
			$this->db->where('email', $data['name']);
			$this->db->or_where('username', $data['name']);
			if( $user = $this->db->get(self::users)->row() ) {

				if( $user->password == $data['password'] ) {

					$this->db->where('user_id', $user->id);
					$this->db->where('location_id', $data['location']);
					if(! $this->db->get(self::userhaslocations, 1)->row()) {

						api_messageError( logs($user->id, 'You do not have permission to access this location.') );
					}

					$json['count'] = 1;
					$json['message'] = logs($user->id, 'You have been successfully logged in.');
					$json['items'][] = array(
						'id' 	   => $user->id,
						'username' => $user->username
					);

					api_messageOk($json);
				}
			}

			api_messageError( logs(NULL, 'Wrong Username or Password.', $data['name']) );
		}


	}    
	?>