<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Sales_Model extends CI_Model {

		const sales = 'sales';
		const users = 'users';
		const userhaslocations = 'user_has_locations';
		const digitalkeys = 'digital_keys';

		function __construct()
		{
			parent::__construct();
		}

		function digital($values)
		{
			$this->db->select('DKEYS.*, SLS.location_id');
			$this->db->where('DKEYS.key', $values['password']);
			$this->db->join(self::sales.' AS SLS', 'SLS.id = DKEYS.sale_id AND SLS.location_id = '.$values['location'], 'left');
			$key = $this->db->get(self::digitalkeys.' AS DKEYS', 1)->row();

			if(!$key) api_messageError( logs_(NULL, 'Wrong digital sale password.', $values) );

			if(!$key->active) api_messageError( logs_(NULL, 'Digital sale password already has been used.', $values+['digital'=>$key]) );

			if(!$key->location_id) api_messageError( logs_(NULL, 'Digital sale password is not defined for this location.', $values+['digital'=>$key]) );

			$this->db->where('id', $key->id);
			if($this->db->update(self::digitalkeys, ['active'=>0])) api_messageOk( logs_(NULL, 'Digital sale password successfully validated.', $values+['digital'=>$key]));
		}

		function login($data)
		{
            $data['password'] = sha1( $data['password'] . $this->config->config['salt'] );

            $this->db->select('id, username, password, role_id');
			$this->db->where('active', 1);
			$this->db->where('email', $data['name']);
			$this->db->or_where('username', $data['name']);
			if( $user = $this->db->get(self::users)->row() ) {

				if( $user->password == $data['password'] ) {

					if(! (int) $user->role_id === 2) api_messageError( logs_($user->id, 'Your account is not a manager.', $user) );

					$this->db->where('user_id', $user->id);
					$this->db->where('location_id', $data['location']);
					if(! $this->db->get(self::userhaslocations, 1)->row()) {

						api_messageError( logs_($user->id, 'You do not have permission to access this location for manage USB.', $user) );
					}

					$data['password'] = '{REMOVED}';

					$json['count'] = 1;
					$json['message'] = logs_($user->id, 'You can manage an USB.', $data, 1, $data['location']);
					$json['items'][] = array(
						'id' 	   => $user->id,
						'username' => $user->username
					);

					api_messageOk($json);
				}
			}

			$data['password'] = '{REMOVED}';
			
			api_messageError( logs_(NULL, 'Wrong Username or Password for manage USB.', $data) );
		}

		function client($ticket , $location_id){

			return  $this->db->query("
				SELECT 
					location_id , folder , title
				FROM
					photos
				WHERE
					title REGEXP '^$ticket(\.|\_)'
				AND
					location_id = $location_id
				ORDER BY
					title ASC
				")->result();
			
		
			
		}

		function operator($data , $sales = FALSE){

			if($sales != FALSE):
				$sales_data = array(
					'location_id' 		=> $sales['location_id'],
					'sale_type_id' 		=> $sales['sale_type_id'],
					'payment_type_id' 	=> $sales['payment_type_id'],
					'payment_date'		=> $sales['payment_date'],
					'user_id'			=> $sales['user_id'],
					'ticket'			=> $sales['ticket'],
					'amount'			=> $sales['amount'],
					'discount_amount' 	=> $sales['discount_amount'],
					'discount_type'	  	=> $sales['discount_type'],
					'created'			=> $sales['created'],
					'active'			=> 1
				);

				$this->db->insert('sales' , $sales_data);

				$product_data = array(
					'sale_id' 		=> $this->db->insert_id(),
					'product_id'	=> $sales['product_id'],
					'quantity'		=> 1,
					'created'		=> $sales['created'],
					'active'		=> 1
				);
				
				$this->db->insert('sale_has_products' , $product_data);

				logs_(1 , 'New Sales' , $sales);
			endif;

			$this->db->insert('donations' , $data);

			logs_(1 , 'New Donation' , $data);
			messageAJAX('success' , 'Success');
		}

		
		public function payment_types()
		{
			return $this->db->where('active' , 1)->get('payment_types')->result();
			
		}
	}    
	?>
