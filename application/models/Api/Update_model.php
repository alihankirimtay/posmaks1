<?php <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_model extends CI_Model {

	public function getVersion(){
		$data = $this->db->fields('version,branch')
		->get('settings');

		return [
			'version' => $data['version'],
			'branch' => $data['branch'],
			'path' =>$this->apiRequest('update',$data)
		];
	}

	 private function apiRequest($url, $inputs = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, posmaksCmsUrl("api/?{$url}&api_token={$this->website->api_token}"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($inputs));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close($ch);


        $data = (Array) json_decode($server_output, true);


        $data = (Object) array_merge([
            'status' => 'ERROR',
            'message' => '500 Internal Server Error!',
            'items' => []
        ], $data);


        if ($data->status != 'SUCCESS') {
            $this->errorMessage = $data->message;
            return [];
        }

        return $data->items;
    }
	

}

/* End of file Update_model.php */
/* Location: ./application/models/Api/Update_model.php */