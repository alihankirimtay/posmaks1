<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Points_Model extends CI_Model {
	
	    const points = 'points';
	    const userhaslocations = 'user_has_locations';

	    function __construct()
	    {
	    	parent::__construct();
	    }

	    function index($location)
	    {
	    	if($location && !in_array($location, $this->user->locations)) error_('access');

	    	$this->db->select('P.*, (SELECT title FROM locations WHERE id = P.location_id LIMIT 1) AS location');
	    	if($location) {

	    		$this->db->where('P.location_id', $location);
	    	}
    		else {

    			$this->db->where_in('P.location_id', $this->user->locations);
    		}

            $this->db->where('P.active !=', 3);
            $this->db->where('P.is_service', 0);
	    	return $this->db->get(self::points.' P')->result();	    	
	    }

	    function add()
	    {
	    	$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']	= (int) $this->input->post('location');
            $values['floor_id']		= (int) $this->input->post('floor');
            $values['zone_id']		= (int) $this->input->post('zone');
            $values['created']  	= _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', $values));
            }

            if( $this->db->insert(self::points, $values) ) {

                $values['id'] = $this->db->insert_id();
            	messageAJAX('success', logs_($this->user->id, 'Masa oluşturuldu. ', $values));
            }
	    }

	    function edit($id)
	    {
    		$values['title']    	= $this->input->post('title', TRUE);
            $values['desc']     	= $this->input->post('desc', TRUE);
            $values['active']   	= $this->input->post('active');
            $values['location_id']  = (int) $this->input->post('location');
            $values['floor_id']		= (int) $this->input->post('floor');
            $values['zone_id']		= (int) $this->input->post('zone');
            $values['modified'] = _date();

            if($this->user->locations && !in_array($values['location_id'], $this->user->locations)) {

            	messageAJAX('error', logs_($this->user->id, 'Geçersiz restoran. ', ['id'=>$id]+$values));
            }

       		$this->db->where('id', $id);
            if( $this->db->update(self::points, $values) ) {

            	messageAJAX('success', logs_($this->user->id, 'Masa güncellendi. ', ['id'=>$id]+$values));
            }
	    }

	    function delete($id, $data)
	    {
	    	$this->db->where('id', $id);
            if( $this->db->update(self::points, ['modified' => _date(), 'active' => 3]) ) {

            	messageAJAX('success', logs_($this->user->id, 'Masa silindi. ', $data));
            }
	    }
	}    
?>
