<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Online extends API_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Locations_M');
        $this->load->model('Products_M');
        $this->load->model('ProductTypes_M');
        $this->load->model('ProductsAdditionalProducts_M');
        $this->load->model('PaymentTypes_M');
        $this->load->model('Points_M');
        $this->load->model('Floors_M');
        $this->load->model('Zones_M');
        $this->load->model('Sales_M');
    }

    public function getRestaurants()
    {
        $restaurants = $this->Locations_M->fields('id,title,desc,dtz,active')->get_all();

        api_messageOk([
        	'count' => count($restaurants),
        	'items' => $restaurants
        ]);
    }

    public function getProducts()
    {
    	$location_id = $this->getPOST('location_id', 'required|trim|is_natural_no_zero|search_table[locations.id]');

        $products = $this->Products_M->get_all([
        	'location_id' => $location_id
        ]);

        api_messageOk([
        	'count' => count($products),
        	'items' => $products
        ]);
    }
     public function getProductsGroup()
    {
        $products_group = $this->ProductTypes_M->get_all([
            'parent_id !=' => null
        ]);
         api_messageOk([
            'count' => count($products_group),
            'items' => $products_group
        ]);
    }

    public function getFindProductsWithRelatedItems()
    {

        $location_id = $this->getPOST('location_id','required');

        $system_products = $this->Products_M->findProductsWithRelatedItems([
            'products' => [
                'location_id' => $location_id,
                'active !=' => 3,
            ],
            'stocks' => [
                'stocks.location_id' => $location_id,
                'stocks.active' => 1,
            ]
        ]);

         api_messageOk([
            'count' => count($system_products),
            'items' => $system_products
        ]); 
    }

    public function getProducts_Additional()
    {
        $product_id = $this->getPOST('product_id', 'required');

        $products = $this->ProductsAdditionalProducts_M->get_all([
            'product_id' => $product_id
        ]);

        api_messageOk([
            'count' => count($products),
            'items' => $products
        ]);
    }

    public function getPayment_Types()
    {
        $payment = $this->PaymentTypes_M->get_all();

        api_messageOk([
            'count' => count($payment),
            'items' => $payment
        ]);
    }

    public function create_Customer()
    {
        $first_name = $this->input->post('first_name','required');
        $phone = $this->input->post('phone','required');

        $data = array(
            "name" => $first_name,
            "phone" => $phone,
            "created" => date('Y-m-d H:i:s'),
            "active" => 1
        );
        $this->db->insert("customers",$data);
        $customer_id = $this->db->insert_id();

        api_messageOk([
            'count' => count($customer_id),
            'items' => $customer_id
        ]);

    }

    public function customer_edit()
    {
        $customer_id = $this->input->post('customer_id','required');
        $first_name = $this->input->post('first_name','required');
        $phone = $this->input->post('phone','required');
        $modified = $this->input->post('modified','required');

        $this->db->set('name', $first_name);
        $this->db->set('phone', $phone);
        $this->db->set('modified', $modified);
        $this->db->where('id', $customer_id);
        $this->db->update("customers");

        $data = array(
            "customer_id" => $customer_id,
            "first_name" => $first_name,
            "phone" => $phone
        );

        api_messageOk([
            'count' => count($data),
            'items' => $data
        ]);
        
    }

    public function customer_Order()
    {
        $customer_id = $this->input->post('customer_id','required');
        $customer_orders = $this->db->get_where('sales',array('customer_id' => $customer_id));
        $customer_orders->result();

        api_messageOk([
            'count' => count($customer_orders),
            'items' => $customer_orders
        ]);
		
    }

    public function getAll_Order()
    {
        $all_orders = $this->Sales_M->get_all();

        api_messageOk([
            'count' => count($all_orders),
            'items' => $all_orders
        ]);
		
    }

    public function getOrder_Detail()
    {
        $sale_id = $this->input->post('sale_id','required');
        $detail = $this->Sales_M->findSaleWithProducts(["id" => $sale_id]);

        api_messageOk([
            'count' => count($detail),
            'items' => $detail
        ]);
    }

   


    public function create_Sale()
    {

        $location_id_ = $this->input->post('location_id','required');
        $customer_id = $this->input->post('customer_id','required');
        $address     = $this->input->post("address");
        $products = $this->input->post('products','required');
        $payments = $this->input->post('payments','required');
        $order_date = $this->input->post('order_date','required');
        $order_note = $this->input->post('order_note');
        $discount_amount = $this->input->post('discount_amount','required');


        $this->db->set('address', $address);
        $this->db->where('id', $customer_id);
        $this->db->update("customers");
        
    
    //Boş Masa Bulma 
        $point_id = null;
        $point = null;
        if($point == null){
            $location_id = $location_id_;
            $service_type = "package";

            if (!in_array($service_type, ['self', 'package'])) {
                messageAJAX('error', 'Hatalı servis isteği.');
            }

            $service_type = $this->Points_M->findServiceTypeByType($service_type);

            $point = $this->Points_M->where('location_id', $location_id_)->get([
                'location_id' => $location_id_,
                'service_type' => $service_type->id,
                'sale_id' => null,
                'active' => 1,
            ]);

            if (!$point) {
                $point = $this->createServicePoint($service_type,$location_id_);
            }
            $point_id = $point->id;
        }
    //

        $this->load->library('Possale', null, 'PosSale');
       
        $sale = $this->PosSale
        ->setAction('order')
        ->setPaymentDate($order_date)
        ->setProducts($products)
        ->setPayments($payments)
        ->setLocationId($location_id_)
        ->setPosId("1")
        ->setPointId($point_id)
        ->setCustomerId($customer_id)
        ->setUserId("1")
        ->setDiscountAmount($discount_amount)
        ->setDiscountType("percent")
        ->processSale();

        if (!$sale) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        //messageAJAX('success', 'Satış başarıyla oluşturuldu.', compact('sale'), 'json');

        $sale_id = $sale->sale_id;
       
        api_messageOk([
            'count' => count($sale_id),
            'items' => $sale_id
        ]);


    }

//Boş Masa Yoksa Yeni Masa Oluşturma
    private function createServicePoint($service_type,$location_id_)
    {
        $floor = $this->Floors_M->fields('id')->findOrFail([
            'location_id' => $location_id_
        ], 'Kat oluşturun.');

        $zone = $this->Zones_M->fields('id')->findOrFail([
            'location_id' => $location_id_
        ], 'Bölge oluşturun.');

        $id = $this->Points_M->insert([
            'title' => $service_type->title,
            'location_id' => $location_id_,
            'floor_id' => $floor->id,
            'zone_id' => $zone->id,
            'service_type' => $service_type->id,
            'active' => 1,
        ]);

        return $this->Points_M->get($id);
    }


}
?>