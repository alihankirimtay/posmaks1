<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class App extends API_Controller {

    function __construct()
    {
        parent::__construct();

        //$this->load->model('Api/App_model');
        $this->load->model('Update_model');
    }

    function index()
    {
        
    }
	
	function version()
    {
		$json['count'] = 1;
		$json['items'][] = array(
						'version' => 1,
						'title'   => base_url('assets/uploads/versions/APP_v_1.exe')
					);

		api_messageOk($json);
    }

    function trigger()
    {
        $version = $this->Update->getVersion();
        $paths = $this->Update->apiRequest('update',$version);
        if ($paths) {
            foreach ($paths as $path) {

                $archive_name = basename($path);
                $target = base_url('updates/').$archive_name;

                if (file_put_contents($target,fopen($path, 'r')) != FALSE) {
                    $zip = new ZipArchive();

                    if ($zip->open($target) == TRUE) {
                      $processes = json_decode($zip->getFromName('config'));

                      if ($processes->deleted !== NULL) {
                        foreach ($processes->deleted as $deleted) {
                          unlink(base_url($deleted));
                        }
                      }

                      if ($processes->added !== NULL) {
                        foreach ($processes->added as $added) {
                       //  print_r($zip->extractTo(BASEPATH.$added,'x+'));
                       }
                     }  
                   }
                }
            }  
        }       
    }
}
?>