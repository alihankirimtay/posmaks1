<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends API_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Customers_M');
    }

    function FindByPhoneNumber()
    {
        $phone = (String) $this->getPOST('phone', 'trim', TRUE);        

        $customer = $this->Customers_M
        ->order_by('id', 'desc')
        ->get([
            'phone' => $phone
        ]);

        api_messageOk([
            'items' => $customer
        ]);
    }
}
?>