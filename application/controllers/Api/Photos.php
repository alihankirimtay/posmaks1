	<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Photos extends API_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Api/Photos_model');
		}

		function index()
		{

		}

		function add()
		{
			$values['location'] = (int) $this->getPOST('location', 'trim|integer');
			$values['base64']   = $this->getPOST('base64', 'trim', FALSE);
			$values['name'] 	= $this->getPOST('name', 'trim');
			$values['path'] 	= $this->getPOST('path', 'trim');

			if(!$values[ 'location_data' ] = $this->exists('locations', "id = ${values['location']} AND active = 1", 'id, prepaid_limit')) {
				api_messageError( logs_(NULL, 'Location does not exist.', $values) );
			}

			$this->Photos_model->add($values);
		}

		function logs()
		{
			// pr($_POST,1);
			$values['location'] = (int) $this->getPOST('location', 'trim|integer');
			$values['path'] 	= $this->getPOST('path', 'trim');
			$values['date'] 	= $this->getPOST('date', 'trim|callback__regexDate');

			if(!$this->exists('locations', "id = ${values['location']} AND active = 1", 'id')) api_messageError( logs_(NULL, 'Location does not exist.', $values) );

			$this->Photos_model->logs($values);
		}

		function _regexDate($date)
		{
			return (bool) (preg_match('/^20[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3(0|1))$/', $date));
		}

		public function ticket()
		{
			if(!$_POST) exit(messageAJAX('error' , 'Ticket Empty'));

			$this->load->library('form_validation');
			$this->load->helper('security');

			$this->form_validation->set_rules('ticket', 'Ticket', 'trim|required|min_length[4]|xss_clean');
			$this->form_validation->set_rules('location_id' , 'Location' , 'trim|required|integer|xss_clean');

			if($this->form_validation->run() === FALSE):

				messageAJAX('error' , validation_errors());

			else:

				$ticket 	 = $this->input->post('ticket');
				$location_id = $this->input->post('location_id');

				$file = $this->Photos_model->ticket($ticket , $location_id);
				
				
				if(!count($file)):
					messageAJAX('error' , 'Ticket Not Found');
				else:
					messageAJAX('success' , 'Ticket Found' , $file);
				endif;

			endif;
		}
	}
	?>
