	<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Videos extends API_Controller {

		function __construct()
		{
			parent::__construct();

			$this->load->model('Api/Videos_model');
		}

		function index()
		{
			api_messageError('invalid request.');
		}

		function add()
		{
			$values['location'] = (int) $this->getPOST('location', 'trim|integer');
			$values['ticket'] 	= $this->getPOST('ticket', 'trim');
			$values['url'] 	    = $this->getPOST('url', 'trim');

			if(!$values[ 'location_data' ] = $this->exists('locations', "id = ${values['location']} AND active = 1", 'id')) {
				api_messageError( logs_(NULL, 'Location does not exist.', $values) );
			}

			$this->Videos_model->add($values);
		}

		function uploaded_videos()
		{
			$values['location'] = (int) $this->getPOST('location', 'trim|integer');

			if(!$this->exists('locations', "id = ${values['location']} AND active = 1", 'id')) api_messageError( logs_(NULL, 'Location does not exist.', $values) );

			$this->Videos_model->uploaded_videos($values);
		}
	}
	?>
