<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales extends API_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Api/Sales_model');
    }

    function index()
    {

    }

    function digital()
    {
        $values['location'] = (int) $this->getPOST('location', 'trim|integer');
        $values['password'] = $this->getPOST('password', 'trim|exact_length[4]');

        if(!$this->exists('locations', "id = ${values['location']} AND active = 1", 'id')) {
			$values['password'] = '{REMOVED}';
			api_messageError( logs_(NULL, 'Location does not exist.', $values) );
		}

        $this->Sales_model->digital($values);
    }

    function login()
    {
        $data['name']     = $this->getPOST('name', 'trim|required|min_length[4]|max_length[32]', TRUE);
        $data['password'] = $this->getPOST('password', 'trim|required|min_length[6]|max_length[32]', TRUE);
        $data['location'] = (int) $this->getPOST('location', 'trim|integer');

        if(!$this->exists('locations', "id = ${data['location']} AND active = 1", 'id'))  {
			$values['password'] = '{REMOVED}';
			api_messageError( logs_(NULL, 'Location does not exist.', $values) );
		}

        $this->Sales_model->login($data);
    }

    function client(){

        if(!$_POST) exit(messageAJAX('error' , 'Ticket Not Found'));

        $this->load->library('form_validation');
        $this->load->helper('security');

        $this->form_validation->set_rules('ticket', 'Ticket', 'trim|required|min_length[4]|xss_clean');

        if($this->form_validation->run() === FALSE):
            messageAjax('error' , validation_errors());
        else:

            $ticket = $this->input->post('ticket');
            $location_id = $this->input->post('location_id');

            $getImages = $this->Sales_model->client($ticket , $location_id);
            

            foreach ($getImages as $images):
                $data[] =  base_url('assets/uploads/photos/'.$images->location_id.'/'.$images->folder.'/'.$images->title);
            endforeach;

            echo json_encode($data);

        endif;
    }


    function operator(){
        
        if(!$_POST) exit('Access Denied');

        $this->load->library('form_validation');
        $this->load->helper('security');

        $this->form_validation->set_rules('location_id', 'Location', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('sale_type_id', 'fieldlabel', 'trim|required|max_length[1]|integer|xss_clean');
        $this->form_validation->set_rules('product_id', 'Product', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('payment_date', 'Payment Date', 'trim|required|xss_clean');
        $this->form_validation->set_rules('payment_type_id', 'Payment Type', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required|integer|xss_clean');
        $this->form_validation->set_rules('ticket', 'Ticket', 'trim|required|xss_clean');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|xss_clean');
        $this->form_validation->set_rules('created', 'Created', 'trim|required|xss_clean');
        $this->form_validation->set_rules('donation_amount', 'Donation Amount', 'trim|required|xss_clean');
        $this->form_validation->set_rules('active', 'Status', 'trim|required|min_length[1]|max_length[1]|xss_clean');

        if($this->form_validation->run() === FALSE):
            messageAJAX('error' , validation_errors());
        else:

            $donation = array(
                'location_id' => $this->input->post('location_id'),
                'amount'      => $this->input->post('donation_amount'),
                'ticket'   => $this->input->post('ticket'),
                'created'   => $this->input->post('created'),
                'active'    => $this->input->post('active')
            );


            if($this->input->post('product_id') > 0):

                $this->Sales_model->operator($donation , $this->input->post());
            else:
                $this->Sales_model->operator($donation , FALSE);
            endif;

        endif;

    }

    public function payment_types()
    {
       $data = $this->Sales_model->payment_types();

       echo json_encode($data);
    }
}
?>
