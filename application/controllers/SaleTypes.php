<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class SaleTypes extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('SaleTypes_model');
    }

    function index()
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $data['saletypes'] = $this->SaleTypes_model->index();

    	$this->load->template('SaleTypes/index', $data);
    }

    function add()
    {
        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->SaleTypes_model->add();
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('SaleTypes/add');
        }       
    }

    function edit($id = NULL)
    {
        if(!$data['saletype'] = $this->exists('sale_types', "id = $id", '*')) {

            show_404();
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->SaleTypes_model->edit($id);
            }
            
            messageAJAX('error', validation_errors());

        } else {
            
            $this->SaleTypes_model->edit($id);

            $this->load->template('SaleTypes/edit', $data);
        }
    }

    function delete($id = NULL)
    {
        $id = (int) $id;

        if(!$product = $this->exists('sale_types', "id = $id")) {

            show_404();
        }

        $this->SaleTypes_model->delete($id, $product);
    }
}
?>
