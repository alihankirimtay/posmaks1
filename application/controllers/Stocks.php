<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stocks extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);
        $this->load->model('Stocks_model', 'Stocks');
        $this->load->model('Stockcontents_model', 'Stocks_content');
    }

    function index($location = NULL)
    {
        $current_location = (int) $location;

        if (!$current_location) {
            $current_location = $this->user->locations[0];
        }

        $this->isAccessibleLocation($current_location);

        $stocks = $this->Stocks->select('*, (SELECT title FROM locations WHERE id = location_id LIMIT 1) AS location')->getAll([
            'location_id' => $current_location
        ]);

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];
        $this->load->template('Stocks/index', compact('stocks', 'current_location'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            $_POST['quantity'] = 0;
            if ($id = $this->Stocks->insert()) {
                messageAJAX('success', __('Stok başarıyla oluşturuldu.'), compact('id'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Stocks/add');
    }

    public function edit($id = null)
    {
        $stock = $this->Stocks->findOrFail($id);
        
        if ($this->input->is_ajax_request()) {
            $_POST['quantity'] = $stock->quantity;
            if ($this->Stocks->update(['id' => $id])) {
                messageAJAX('success', __('Stok başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Stocks/edit', compact('stock'));
    }

    public function delete($id = null)
    {
        $this->Stocks->findOrFail($id);

        $this->Stocks->delete(['id' => $id]);

        messageAJAX('success', __('Stok başarıyla silindi.'));
    }

    public function alarm($location = null)
    {
        $current_location = (int) $location;

        if (!$current_location) {
            $current_location = $this->user->locations[0];
        }

        $this->isAccessibleLocation($current_location);

        $this->db->where('quantity - sub_limit <= 0', null,false);
        $stocks = $this->Stocks->select('*, (SELECT title FROM locations WHERE id = location_id LIMIT 1) AS location')->getAll([
            'location_id' => $current_location
        ]);
        
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];
        $this->load->template('Stocks/alarm', compact('stocks', 'current_location'));
    }

    function ajax()
    {
        $target = isPost('target');

        switch ($target) {
            case 'stocks':
                $this->getStocks();
                break;
            case 'getStockById':
                $this->getStockById();
                break;

            case 'getStocksByTitle':
                $this->getStocksByTitle();
                break;

            case 'countCriticalStocks':
                $this->countCriticalStocks();
                break;  

            case 'addStockWithQuantity':
                $this->addStockWithQuantity();
                break; 
            
            case 'updateStockWithQuantity':
                $this->updateStockWithQuantity();
                break; 

            case 'deleteStockWithQuantity':
                $this->deleteStockWithQuantity();
                break; 
                
            default:
                messageAJAX('error', __('Geçersiz Hedef!'));
                break;
        }
    }


    private function getStocks()
    {
        $location_id = $this->input->post('location');

        if (!in_array($location_id, $this->user->locations)) {
            messageAJAX('error', __('Geçersiz Lokasyon'));
        }

        $stocks = $this->Stocks
        ->select('*')
        ->select('(SELECT COUNT(id) FROM stock_contents WHERE stock_contents.stock_id = stocks.id LIMIT 1) AS stock_count')        
        ->select('(SELECT stock_contents.expense_id FROM stock_contents WHERE stock_contents.stock_id = stocks.id LIMIT 1) AS expense_id')
        ->getAll([
            'location_id' => $location_id
        ]);

       foreach ($stocks as &$stock) {
            $stock->unit = @$this->units[$stock->unit];
            $stock->edit = ($stock->stock_count == 1 && !$stock->expense_id) ? true : false ;
       }

       messageAJAX('success', '', $stocks);
    }

    private function getStockById() {

        $id = $this->input->post('id');

        $stock = $this->Stocks
        ->get([
            'id' => $id
        ]);

       messageAJAX('success', '', $stock);
            
    }

    private function getStocksByTitle()
    {
        $customers = [];
        $q = trim($this->input->post('q', TRUE));
        $location_id = (int) $this->input->post('location_id');

        if (strlen($q)) {
        
            $this->db
            ->select('id, title, unit')
            ->like('title', $q);
            $customers = $this->Stocks->getAll([
                'location_id' => $location_id,
                'active' => 1,
            ]);
        }

        api_messageOk([
            'items' => $customers
        ]);
    }

    private function countCriticalStocks()
    {   
        $this->db->where('sub_limit >= quantity', null, false);
        $this->db->where_in('location_id', $this->user->locations);
        $stocks = $this->Stocks->select('id')->getAll([
            'active' => 1,
        ]);

        if ($stocks) {
            messageAJAX('success', null, [
                'count' => count($stocks)
            ]);
        }
    }

    private function isAccessibleLocation($location_id)
    {
        if($location_id && !in_array($location_id, $this->user->locations))
            error_('access');
    }

    private function addStockWithQuantity() {

        if ($stock_id = $this->Stocks->insert()) {
            $stock = (object) [
                'id' => $stock_id,
                'quantity' => 0
            ];

            if ($this->Stocks_content->add($stock)) {
                messageAJAX('success', __('Stok başarıyla oluşturuldu.'));  
            }

        } else {
            messageAJAX('error', validation_errors());
        }

    }

    private function updateStockWithQuantity()
    {
        $id = $this->input->post('stock_id');
        // $stock = $this->Stocks->findOrFail($id);

        if($this->Stocks->update(['id' => $id])){

            messageAJAX('success', __('Stok başarıyla güncellendi.'));  
            
        } else {

            messageAJAX('error', validation_errors());
            

        }

        $stock_content = $this->Stocks_content->get([
            'stock_id' => $id
        ]);

        $stock = (object) [
            'id' => $id,
            'quantity' => 0
        ];

        $content = (object) [
            'quantity' => 0
        ];

        $this->Stocks_content->edit($stock_content->id, $stock, $content);

        messageAJAX('success', __('Stok başarıyla güncellendi.'));  
        
        
    }

    private function deleteStockWithQuantity() 
    {
        $id = $this->input->post('id');
        
        $stock_content = $this->Stocks_content->get([
            'stock_id' => $id
        ]);

        $this->Stocks->delete(['id' => $id]);

        $this->Stocks_content->delete([
            'id' => $stock_content->id
        ]);

        messageAJAX('success', __('Stok başarıyla silindi.'));  
              

    }
}
?>