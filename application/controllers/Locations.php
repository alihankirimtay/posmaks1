<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Locations_M');
        $this->load->model('Pos_M');
        $this->user_locations = $this->Locations_M->findByUser($this->user->id);
    }

    public function ajax($event= null)
    {
        if(method_exists(__CLASS__, (String) $event)) {
            $this->{$event}();
        } else {
            messageAJAX('error', 'Invalid request!');
        }
    }

    public function index()
    {

        if ($this->user->role->alias == 'admin') {
            $locations = $this->Locations_M->get_all();
        } else {
             $locations = $this->user_locations;
        }


        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $this->load->template('Locations/index', compact('locations'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($id = $this->Locations_M->from_form()->insert()) {

                $this->Pos_M->insert([
                    'location_id' => $id,
                    'title' => __('Kasa').' 1',
                    'amount' => '0',
                    'active' => '1'
                ]);

                messageAJAX('success', __('Restoran başarıyla oluşturuldu.'),[
                    'id' => $id
                ]);
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $date_time_zones = $this->Locations_M->findDateTimeZones();

        $this->load->template('Locations/add', compact('date_time_zones'));       
    }

    public function edit($id = NULL)
    {
        if ($this->user->role->alias != 'admin') {
            $this->db->where_in('id', array_map(function($row){
                return $row->id;
            },$this->user_locations));   
        }

        $location = $this->Locations_M->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Locations_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Restoran başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $date_time_zones = $this->Locations_M->findDateTimeZones();

        $this->load->template('Locations/edit', compact('location', 'date_time_zones'));    
    }

    public function delete($id = NULL)
    {

        if ($this->user->role->alias != 'admin') {
            $this->db->where_in('id', array_map(function($row){
                return $row->id;
            },$this->user_locations));   
        }

        $this->Locations_M->findOrFail($id);

        $this->Locations_M->update([
            'active' => 3
        ], ['id' => $id]);

        messageAJAX('success', __('Restoran başarıyla silindi.'));
    }

    public function getLocations()
    {
        $locations = [];
        $locations = $this->user->locations_array;

        messageAjax('success','',$locations);
    }

    public function getlocation()
    {
        $id = $this->input->get('id');

        if (isset($this->user->locations_array[$id])) {
            messageAJAX('success', '', $this->user->locations_array[$id]);
        }
    }

}
?>
