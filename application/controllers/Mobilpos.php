<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'controllers/Component/Sales_Component.php';

class Mobilpos extends Sales_Component {

    
    const cookie = 'stp_1114TeUmRrKaAhN02T0L6a';
    const sound_path = 'assets/uploads/sound_notes';

    //private $pos_settings;
    private $pos_setup;
    private $pos_session;

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->library('Possession', null, 'PosSession');
        $this->load->library('Possale', null, 'PosSale');

        // $this->pos_settings = $this->getPosSettings();
        // $this->operator_session = $this->getOperatorSession();

        $this->load->model("mobilpos_model");
        $this->load->model('Sales_m', 'Sales_M');
        $this->load->model('Points_M');
        $this->load->model('Pos_M');
        $this->load->model('Products_M');
        $this->load->model('Saleproducts_M', 'SaleProducts_M');
        $this->load->model('Producttypes_M', 'ProductTypes_M');
        $this->load->model('Paymenttypes_M', 'Paymenttypes_M');
        $this->load->model('Saleproductnotes_M', 'SaleProductNotes_M');
        $this->load->model('Customers_M');
        $this->load->model('Sessions_M');
        $this->load->model('Roles_M');
        $this->load->model('Floors_M');
        $this->load->model('Zones_M');
        $this->load->model('Productions_M');

        $this->pos_setup = $this->PosSession->getSetup();
        $this->pos_session = $this->PosSession->getSession();

        
    } 
        
    public function index()
    {

      if ($this->pos_setup) {

        if (!isset($this->user->locations_array[$this->pos_setup->location_id])) {
            $this->PosSession->removeSetup();
            $this->PosSession->removeSession();
            redirect('pointofsales');
        }


        setcookie(self::cookie, json_encode([ 'l' => 1, 'p' => 1 ]), time()+60*60*24*365, '/');

        $mobile=$this->agent->is_mobile();

        //$this->check_auth();
        $location = $this->user->locations_array[$this->pos_setup->location_id];
        $location_currency = $location['currency'];
        $payment_types = $this->PosSale->getPaymentTypes();
        //$payment_types = $this->getPaymentTypes();
        $location_id = $location['id'];
        $location_invoice_template = $this->_invoiceTemplate($location_id);
        $operator_session_json = json_encode($this->operator_session);
        $pos_session_json = json_encode($this->pos_session);
        $pos_settings = $this->pos_setup;

        $this->theme_plugin = array(
            'css'   => ['globals/plugins/select2/css/select2.min.css', 'panel/css/pos.css'],
            'js'    => array( 'panel/js/view/posmobil.js',
                              'globals/plugins/jquery.print/print.js',
                              'globals/plugins/ionsound/js/ion.sound.min.js',
                              'globals/scripts/soundplayer.js',
                              'globals/plugins/recorderjs/recorder.js',
                              'globals/plugins/touchSwipe/jquery.touchSwipe.js',
                              'globals/plugins/touchSwipe/jquery.touchSwipe.min.js'
                            ),
            'start' => '
             posmobil.payment_types = '.json_encode($payment_types).';
             posmobil.operator_session = '.$pos_session_json.';
             posmobil.init();'
        );

        $this->load->template('pointofsales/mobilindex', compact('pos_settings', 'location_currency', 'location_invoice_template', 'payment_types'));

      } else {

          $pos_points = $this->Pos_M->fields('id,title,location_id')->where('location_id', $this->user->locations)->get_all([
              'active' => 1
          ]);
          
          $pos_points_json = json_encode($pos_points);

          $this->theme_plugin = [
                'css' => ['panel/css/view/pointofsales.css'],
                'js'    => ['panel/js/view/pointofsales.js'],
                'start' => "
                    Pointofsales.Setup.init({
                        pos_points: ${pos_points_json},
                        return_url:'mobilpos'
                    });"
            ];

            $this->load->template('pointofsales/setup');
      }
      
    }

    public function tablet()
    {

      if ($this->pos_setup) {

         if (!isset($this->user->locations_array[$this->pos_setup->location_id])) {
            $this->PosSession->removeSetup();
            $this->PosSession->removeSession();
            redirect('pointofsales');
        }


        setcookie(self::cookie, json_encode([ 'l' => 1, 'p' => 1 ]), time()+60*60*24*365, '/');

        $mobile=$this->agent->is_mobile();

        //$this->check_auth();
        $location = $this->user->locations_array[$this->pos_setup->location_id];
        $location_currency = $location['currency'];
        $payment_types = $this->PosSale->getPaymentTypes();
        //$payment_types = $this->getPaymentTypes();
        $location_id = $location['id'];
        $location_invoice_template = $this->_invoiceTemplate($location_id);
        $operator_session_json = json_encode($this->operator_session);
        $pos_session_json = json_encode($this->pos_session);

        $pos_settings = $this->pos_setup;

        $this->theme_plugin = array(
            'css'   => ['globals/plugins/select2/css/select2.min.css', 'panel/css/pos.css'],
            'js'    => array( 'panel/js/view/posmobil.js',
                              'globals/plugins/jquery.print/print.js',
                              'globals/plugins/ionsound/js/ion.sound.min.js',
                              'globals/scripts/soundplayer.js',
                              'globals/plugins/recorderjs/recorder.js',
                              'globals/plugins/touchSwipe/jquery.touchSwipe.js',
                              'globals/plugins/touchSwipe/jquery.touchSwipe.min.js'
                            ),
            'start' => '
             posmobil.payment_types = '.json_encode($payment_types).';
             posmobil.operator_session = '.$pos_session_json.';
             posmobil.isTablet = true;
             posmobil.init();'
        );

        $this->load->template('pointofsales/tabletindex', compact('pos_settings', 'location_currency', 'location_invoice_template', 'payment_types'));

      } else {

          $pos_points = $this->Pos_M->fields('id,title,location_id')->where('location_id', $this->user->locations)->get_all([
              'active' => 1
          ]);
          
          $pos_points_json = json_encode($pos_points);

          $this->theme_plugin = [
                'css' => ['panel/css/view/pointofsales.css'],
                'js'    => ['panel/js/view/pointofsales.js'],
                'start' => "
                    Pointofsales.Setup.init({
                        pos_points: ${pos_points_json},
                        return_url:'tablet'
                    });"
            ];

            $this->load->template('pointofsales/setup');
      }
      
    }

    public function ajax($event= null)
    {
        if(method_exists(__CLASS__, (String) $event)) {
            $this->{$event}();
        } else {
            messageAJAX('error', 'Invalid request!');
        }
    }

    private function required(Array $requireds)
    {
        if (in_array('setup', $requireds) && !$this->pos_setup) {
            messageAJAX('error', __('Kasa kurulumu yapınız.'), ['event' => 'requiredSetup']);
        }

        if (in_array('session', $requireds) && !$this->pos_session) {
            messageAJAX('error', __('Oturum açın.'), ['event' => 'requiredSession']);
        }
    }

    private function createCallAccount()
    {
        $location_id = $this->pos_setup->location_id;
        $sale_id = $this->input->post('sale_id');

        $sale = $this->Sales_M->findOrFail([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'pending',
            'active' => 1,
        ], 'Geçersiz Satış.');

        $this->Sales_M->where('id', $sale_id)->update(['call_account' => 1]);

        messageAJAX('success', __('Hesap isteği gönderildi'));
    }

    private function loginForDiscountKey()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $discount_amount = $this->input->post('discount_amount');
        $discount_type = $this->input->post('discount_type');
        $login_required = false;

        if (!$this->PosSession->checkPermissionByUserSession($this->pos_session, 'pointofsales.adddiscount')) { 
            $login_required = true;
        }

        $discount_key = $this->PosSession->createDiscount([
            'location_id' => $location_id,
            'username' => $username,
            'password' => $password,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
        ], $login_required);

        

        if (!$discount_key) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        messageAJAX('success', __('İndirim oluşturuldu.'), compact('discount_key', 'discount_amount', 'discount_type'));
    }

    private function loginForSetup()
    {
        $location_id = $this->pos_setup->location_id;
        $pos_id = $this->input->post('pos_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $setup = $this->PosSession->createSetup([
            'location_id' => $location_id,
            'pos_id' => $pos_id,
            'username' => $username,
            'password' => $password,
        ]);

        if (!$setup) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        messageAJAX('success', __('Kasa kurulumu yapıldı.'));
    }

    // New
    private function loginForOperatorSession()
    {
        $this->required(['setup']);

        $location_id = $this->pos_setup->location_id;
        $password = $this->input->post('password-login');

        $user = $this->PosSession->createSession([
            'location_id' => $location_id,
            'password' => $password,
        ]);

        if (!$user) {
            messageAJAX('error', $this->PosSession->errorMessage());
        }

        $this->Sessions_M->createOnAvailableLocationsForUser($user, $location_id);

        messageAJAX('success', __('Oturum açıldı.'), compact('user'));
    }

    // New
    private function loginForMobilSettings()
    {

       $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $password = $this->input->post('password-lock-login');

        $this->form_validation
        ->set_rules('password-lock-login', 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {

             messageAJAX('error', validation_errors());
        }

        $user = $this->db
        ->select('id')
        ->where([
            'active' => 1,
            'pin' => $password,
        ])->get('users', 1)->row();
        if (!$user) {
             messageAJAX('error', __('Hatalı giriş'));
        }

        messageAJAX('success', 'Success');
        
    }

    // New
    private function logoutForOperatorSession()
    {
        $this->required(['setup']);

        $this->PosSession->removeSession();

        messageAJAX('success', __('Oturum kapatıldı.'));
    }

    // New
    private function checkOperatorSession()
    {
         $this->required(['setup']);

        if ($this->pos_session) {
            messageAJAX('success', 'Success');
        }

        messageAJAX('error');
    }

    // New
    private function removePosSetup()
    {
        $this->PosSession->removeSetup();
        $this->PosSession->removeSession();

        messageAJAX('success', __('Kasa değiştirliyor.'));
    }

    //New
    private function getCompletedUnDeliveredProducts()
    {

        $products = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setUserId($this->pos_session->user_id)
        ->getCompletedUnDeliveredProducts();

        $call_accounts = $this->PosSale->getActiveCallAccounts();


        messageAJAX('success', 'Success', compact('products', 'call_accounts'), 'json');
    }

    // New
    private function productDelivered()
    {
        $result = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setSaleId($this->input->get('sale_id'))
        ->setProductId($this->input->get('product_id'))
        ->productDelivered();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', 'Success');
    }

    private function setViewedCallAccount()
    {

        $result = $this->PosSale
        ->setLocationId($this->pos_setup->location_id)
        ->setSaleId($this->input->get('sale_id'))
        ->setViewedCallAccount();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', 'Success');

    }

    // New
    private function getFloorsWithZones()
    {
        $this->required(['setup', 'session']);

        $location_id = $this->pos_setup->location_id;
        $user_id = $this->pos_session->user_id;
        $user_role = $this->pos_session->user_role;

        $where_zones = [];
        $zone_ids = [0];
        if ($user_role != 'admin') {
            if ($user_zones = $this->Zones_M->findZonesByUser($user_id)) {
                $zone_ids = array_map(function ($row) { return $row->id; }, $user_zones);
            }

            $where_zones = [
                'where_in' => [
                    'id' => $zone_ids
                ]
            ];
        }

        $floors = $this->Floors_M->fields('id, title')
        ->with_zones([
            'where' => [
                'location_id' => $location_id,
                'active' => 1,
            ],
            'order_inside' => 'title asc'
        ] + $where_zones)
        ->where('location_id', $this->user->locations)
        ->order_by('title', 'asc')
        ->get_all([
            'location_id' => $location_id,
            'active' => 1
        ]);

        messageAJAX('success', 'Success', compact('floors'));
    }

    private function getTable()
    {

        $location_id = $this->pos_setup->location_id;
        $floors = $this->input->post('floor_id');
        $zones = $this->input->post('zone_id');

        if (!in_array($location_id, $this->user->locations)) {
            messageAJAX('error', __('Yetki hatası'));
            
        }

        $tables = $this->db
        ->select('p.title,p.id,p.status,p.sale_id')
        ->select('(SELECT amount FROM sales WHERE id = p.sale_id) as "amount"')
        ->select('(SELECT interim_payment_amount FROM sales WHERE id = p.sale_id) as "interim_payment_amount"')
        ->where([
            'p.location_id' => $location_id,
            'p.active' => 1,
            'p.floor_id' => $floors,
            'p.zone_id' => $zones,
            'service_type' => 1
        ])
        ->group_by('p.id')
        ->get('points p')
        ->result();


        messageAJAX('success',NULL,$tables);

    }

    public function getCategoriedProducts()
    {

      $location_id = $this->pos_setup->location_id;
      
      $products = $this->mobilpos_model->products($location_id);
      $categories = $this->mobilpos_model->category();

      foreach ($categories as $key => &$category) {
          $category['products'] = [];

          foreach ($products as $key => $product) {

              if ($product['product_type_id'] == $category['id']) {  
                $category['products'][$product['id']] = $product;
              }
          }
      }  
      messageAJAX('success',NULL,$categories);
    }

    private function getProductsOptions() 
    {

      $product_id = (int) $this->input->post('product_id');
      $saleProductId = (int) $this->input->post('saleProductId');

      $products = $this->Products_M
      ->group_start()
      ->where('id', $product_id)
      ->or_where('portion_id', $product_id)
      ->group_end()
      ->order_by('portion_id', 'asc')
      ->get_all([
        'active' => 1
      ]);

      if ($products) {

        $product_ids = array_map(function ($row) {
          return $row->id;
        }, $products);

        $additional_products = $this->db
        ->where('products.active', 1)
        ->where_in('products_additional_products.product_id', $product_ids)
        ->join('products_additional_products', 'products_additional_products.additional_product_id = products.id')
        ->get('products')
        ->result();

        $stocks = $this->db
        ->select('stocks.*, product_has_stocks.product_id, product_has_stocks.quantity product_stock_quantity, product_has_stocks.optional')
        ->where('stocks.active', 1)
        ->where('product_has_stocks.optional', 1)
        ->where_in('product_has_stocks.product_id', $product_ids)
        ->join('product_has_stocks', 'product_has_stocks.stock_id = stocks.id')
        ->get('stocks')
        ->result();

        $package_products = $this->db
            ->where_in('products_has_products.package_id', $product_ids)
            ->where('active', 1)
            ->join('products_has_products', 'products_has_products.product_id = products.id')
            ->get('products')
            ->result();

        $quick_notes = $this->db
        ->select('quick_notes.*, products_quick_notes.product_id')
        ->where('quick_notes.active', 1)
        ->where('quick_notes.deleted', null)
        ->where_in('products_quick_notes.product_id', $product_ids)
        ->join('products_quick_notes', 'products_quick_notes.quick_note_id = quick_notes.id')
        ->get('quick_notes')
        ->result();

        $data = [];
        foreach ($products as $product) {

          $product->additional_products = [];
          $product->stocks = [];
          $product->quick_notes = [];

        // Additional prodcuts
            foreach ($additional_products as $additional_product) {
                if ($product->id == $additional_product->product_id) {
                    $product->additional_products[$additional_product->id] = $additional_product;
                }
            }

        // Stocks
            foreach ($stocks as $stock) {
                if ($product->id == $stock->product_id) {
                    $product->stocks[$stock->id] = $stock;
                }
            }

            // Quick Notes
            foreach ($quick_notes as $quick_note) {
                if ($product->id == $quick_note->product_id) {
                    $product->quick_notes[$quick_note->id] = $quick_note;
                }
            }

            if ($product->type == "package") {
                foreach ($package_products as $package_product) {
                    if ($product->id == $package_product->package_id) {
                        $product->products[$package_product->product_id] = $package_product;
                    }   
                }
            }

          $data[$product->id] = $product;
        }

        $products = $data;
      }

      // ara ödemesi yapılmış ürünlerin disabled edilmesi
      if ($saleProductId) {
        $sale_has_products = $this->db
        ->where('id', $saleProductId)
        ->get('sale_has_products')
        ->result();
      }

      $payment_types = $this->getPaymentTypes();

      messageAJAX('success', 'Success', compact('products', 'sale_has_products', 'payment_types'));
    }

    public function complete() 
    {

      $this->required(['setup', 'session']);

      $products = (array) $this->input->post('products[]');
      $payments = (array) $this->input->post('payments[]');
      $action = (string) $this->input->get('action');
      $sale_id = (int) $this->input->post('sale_id');
      $location_id = $this->pos_setup->location_id;
      $pos_id = $this->pos_setup->pos_id;
      $point_id = (int) $this->input->post('table_id');
      $discount_key = (string) $this->input->post('discount_key');
      $customer_id = (int) $this->input->post('customer_id');
      $user_id = $this->pos_session->user_id;
      $completer_user_id = $this->pos_session->user_id;
      $discount_amount = 0;
      $discount_type = null;
      $discount_remove = false;
      $datetime = date('Y-m-d H:i:s');
      $discount_key = (string) $this->input->post('discount_key');

      if ($discount_key) {
            $discount = $this->PosSession->checkDiscount($discount_key);
            if (!$discount) {
                messageAJAX('error', $this->PosSession->errorMessage());
            }

            $discount_amount = $discount->discount_amount;
            $discount_type = $discount->discount_type;
            $discount_remove = ((int) $discount_amount === 0);
        }

        $sale = $this->PosSale
        ->setProducts($products)
        ->setPayments($payments)
        ->setAction($action)
        ->setSaleId($sale_id)
        ->setLocationId($location_id)
        ->setPosId($pos_id)
        ->setPointId($point_id)
        ->setCustomerId($customer_id)
        ->setUserId($user_id)
        ->setCompleterUserId($completer_user_id)
        ->setDiscountAmount($discount_amount)
        ->setDiscountType($discount_type)
        ->setDiscountRemove($discount_remove)
        ->setPaymentDate($datetime)
        ->processSale();

        if (!$sale) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        if ($discount_key) {
            $this->PosSession->removeDiscount();
        }

        $message = $sale->message;
        $sale_id = $sale->sale_id;
        $inserted_product_ids = $sale->inserted_product_ids;
        $product_calculations = $sale->product_calculations;
        if (isset($sale->new_sale_id))
            $new_sale_id = $sale->new_sale_id;


        $sale = $this->Sales_M->findSaleWithProducts(['id' => $sale_id]);
        $sale->payment_date_client = dateConvert($datetime, 'client', dateFormat());

        $sale->customer = [];
        if ($sale->customer_id) {
            $sale->customer = $this->Customers_M->get(['id' => $sale->customer_id]);
        }

        messageAJAX('success', $message, compact('status', 'new_sale_id', 'sale_id', 'inserted_product_ids', 'product_calculations', 'sale'), 'json');

    }

    // New
    private function cancelSale() 
    {
        $this->required(['setup', 'session']);

        $sale_id = $this->input->post('sale_id');
        $point_id = $this->input->post('point_id');
        $location_id = $this->pos_setup->location_id;

       

        $result = $this->PosSale
        ->setLocationId($location_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->cancelSale();

         if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Masa iptal edildi.'));

    }

    private function completeInterimPayment()
    {
        $this->required(['setup', 'session']);

        $sale_id = $this->input->post('sale_id');
        $location_id = $this->pos_setup->location_id;
        $point_id = $this->input->post('table_id');
        $payment_type = $this->input->post('payment_type');

        $interim_products = (Array) $this->input->post('interim_payment_products');

        $products = [];

        foreach ($interim_products as $key => $interim_product) {

          $products[$interim_product['sale_product_id']] = $interim_product['quantity'];
          
        }

        $interim_payment_amount = $this->PosSale
        ->setLocationId($location_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->setPaymentType($payment_type)
        ->setProducts($products)
        ->makeInterimPayment();

        if (!$interim_payment_amount) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Ara ödeme alındı.'), compact('interim_payment_amount'));
    }

    private function createGift()
    {
        $this->required(['setup', 'session']);

        $interim_products = (Array) $this->input->post('interim_payment_products');
        $location_id = $this->pos_setup->location_id;
        $user_id = $this->pos_session->user_id;
        $sale_id = $this->input->post('sale_id');
        $point_id = $this->input->post('table_id');

        $products = [];

        foreach ($interim_products as $key => $interim_product) {

          $products[$interim_product['sale_product_id']] = $interim_product['quantity'];
          
        }

        $result = $this->PosSale
        ->setLocationId($location_id)
        ->setUserId($user_id)
        ->setSaleId($sale_id)
        ->setPointId($point_id)
        ->setProducts($products)
        ->makeProductsAsGift();

        if (!$result) {
            messageAJAX('error', $this->PosSale->errorMessage());
        }

        messageAJAX('success', __('Ürünler ikram edildi.'));
    }

    private function orderCheck() 
    {
      $location_id = (int) $this->pos_setup->location_id;
      $sale_id = (int) $this->input->post('sale_id');
      $tableId = (int) $this->input->post('table_id');

      if ($sale_id) {
        $sale = $this->Sales_M
        ->fields(['*', '(SELECT title FROM points where id = point_id) as point_title', '(SELECT title FROM zones where id = zone_id) as zone_title'])
        ->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'pending',
            'point_id' =>$tableId,
            'active' => 1,
        ]);

        if (!$sale) {
          messageAJAX('error' , __('Geçersiz sipariş.'));
        }


        $interim_payment_amount = $sale->interim_payment_amount;
        $sale_products = $sale->products;
        $payment_types = $this->getPaymentTypes();
        $amount = $sale->amount;

      }
      messageAJAX('success', '', compact('sale_products', 'payment_types', 'interim_payment_amount', 'amount', 'sale'));
    }

    private function getTableStatus()
    {
      $tableId = $this->input->post('table_id');
      $location_id = (int) $this->pos_setup->location_id;

      $point = $this->db->select('sale_id')->where([
        'location_id' => $location_id,
        'id' =>$tableId,
        'active' => 1
      ])
      ->get('points')->row();

      if (isset($point->sale_id)) {

        $sales_id = $point->sale_id;

        messageAJAX('success','', compact('sales_id'));

      }
     
    }

    public function getComplete() {

        $tableId = $this->input->post('tableId');
        $saleDetails = $this->db->query('SELECT s.amount, s.subtotal, s.id as "sale_id" FROM sales as s WHERE point_id = ? AND status = "pending"', array($tableId))->result_array();
        $tax = $saleDetails[0]['amount'] - $saleDetails[0]['subtotal']; 
        
        messageAJAX('success',NULL,[
            'amount' => $saleDetails[0]['amount'],
            'subtotal' => $saleDetails[0]['subtotal'],
            'tax' => $tax
            ]);
    }


    private function checkPosSettings()
    {
        if (!isset($_COOKIE[ self::cookie])) {

            if ($this->input->is_ajax_request()) {
                
                $this->load->library('form_validation');
                $this->form_validation->set_rules('location', 'Restoran', 'trim|required|is_natural_no_zero');
                $this->form_validation->set_rules('pos', 'Kasa', 'trim|required|is_natural_no_zero');
                $this->form_validation->set_rules('username', 'Kullanıcı adı/Eposta', 'trim|required');
                $this->form_validation->set_rules('password', 'Şifre', 'trim|required');

                if (!$this->form_validation->run()) {
                    messageAJAX('error' , validation_errors());
                }

                $data['location'] = $this->pos_setup->location_id;
                $data['pos']      = $this->input->post('pos_id');
                $data['username'] = $this->input->post('username', TRUE);
                $data['password'] = $this->input->post('password', TRUE);

                $this->db->select('password, role_id')
                    ->where('active', 1)
                    ->group_start()
                        ->where('username', $data['username'])
                        ->or_where('email', $data['username'])
                    ->group_end();
                $user = $this->db->get('users', 1)->row();

                if (! $user) {
                    $data['password'] = '{REMOVED}';
                    messageAJAX('error', logs_($this->user->id, __('Hatalı kullanıcı adı veya şifre.'), $data, 5, $data['location']));
                }
                if ($user->password != sha1($data['password'] . $this->config->config['salt'])) {
                    $data['password'] = '{REMOVED}';
                    messageAJAX('error', logs_($this->user->id, __('Hatalı şifre.'), $data, 5, $data['location']));
                }
                if (intval($user->role_id) !== 1) {

                    $this->db->where('location_id', $data['location']);
                    $this->db->limit(1);
                    if(!$this->db->get('user_has_locations', 1)->row()) {
                        $data['password'] = '{REMOVED}';
                        messageAJAX('error',
                            logs_($this->user->id, __('Bu restorana erişim izniniz yok.'), $data, 5, $data['location']));
                    }


                    $this->db->select('METHOD.id');
                    $this->db->where('CONTROLLER.page', 'mobilpos');
                        $this->db->join('permissions METHOD', 'METHOD.parent_id = CONTROLLER.id')
                        ->where('METHOD.page', 'settingsforpos');
                    $page = $this->db->get('permissions CONTROLLER', 1)->row();

                    if(! $page) {
                        messageAJAX('error', __('Erişim yetkiniz yok. x001'));
                    }

                    // Kullanıcı Rolüne atanmış mı?
                    $this->db->where('permission_id', $page->id);
                    $this->db->where('role_id', $user->role_id);
                    if(! $this->db->get('role_has_permissions', 1)->row() ) {
                        messageAJAX('error', __('Erişim yetkiniz yok. x002'));
                    }
                }

                $data['password'] = '{REMOVED}';
                logs_($this->user->id, __('Kasa ayarları bu bilgisayara kaydedildi.'), $data, 5, $data['location']);

                setcookie(self::cookie, json_encode([ 'l' => $data[ 'location' ], 'p' => $data[ 'pos' ] ]), time()+60*60*24*365, '/');

                messageAJAX('success', logs_($this->user->id, __('Kasa ayarları kaydedildi.'), $data, 5, $data['location']));
                // return true;
            }

            return false;

        } else {

            $json = json_decode(@$_COOKIE[ self::cookie ]);
            if (!isset($json->l, $json->p)) {
                return false;
            }

            $location = (int) $json->l;
            $pos      = (int) $json->p;


            if (in_array($location, $this->user->locations)) {

                $this->db
                    ->where('id', $pos)
                    ->where('location_id', $location)
                    ->where('active', 1);
                if ($this->db->get('pos', 1)->row()) {

                    return [ 'location' => $location, 'pos' => $pos ];
                }
            }

            unset($_COOKIE[ self::cookie ]);
            setcookie(self::cookie, "", time()-60*60*24*365, '/');

            return false;
        }

        
    }


    function changePos()
    {
        unset($_COOKIE[ self::cookie ]);
        setcookie(self::cookie, "", time()-60*60*24*365, '/');

        redirect('mobilpos','refresh');
    }

    protected function _invoiceTemplate($location_id)
    {
        if ($invoice_template = $this->db->where('location_id', $location_id)->get('invoice_template', 1)->row()) {
            return $invoice_template;
        }

        return (Object) [
            'logo' => null,
            'header' => null,
            'footer' => null,
        ];
    }

    public function saveSoundNote()
    {
      $this->required(['setup', 'session']);

      $_POST['type'] = 2;
      $_POST['viewed'] = 0;

      if (isset($_FILES['sound']))
          if ($_FILES['sound']['name'] == 'blob')
              $_FILES['sound']['name'] = 'blob.wav';
      
      if (!$id = $this->SaleProductNotes_M->from_form()->insert()) {
          messageAJAX('error', validation_errors());
      }

      $sound_note = $this->SaleProductNotes_M->get($id);

      messageAJAX('success', 'Success', compact('sound_note'));
    }

    public function generateInvoice($sale_id , $tableId)
    {
        $location_id = (int) $this->pos_setup->location_id;

        if ($sale_id) {
            $sale = $this->Sales_M
            ->fields(['*', '(SELECT title FROM points where id = point_id) as point_title', '(SELECT title FROM zones where id = zone_id) as zone_title'])
            ->findSaleWithProducts([
                'id' => $sale_id,
                'location_id' => $location_id,
                'status' => 'pending',
                'point_id' =>$tableId,
                'active' => 1,
            ]);

            if (!$sale) {
            messageAJAX('error' , __('Geçersiz sipariş.'));
            }


            $interim_payment_amount = $sale->interim_payment_amount;
            $sale_products = $sale->products;
            $payment_types = $this->getPaymentTypes();
            $amount = $sale->amount;
            $remaining_amount = $amount - $interim_payment_amount;

            header("Content-Type: application/xml");

            $this->load->library('Xmlgenerate');

            $xml = $this->xmlgenerate;

            $logo = base_url('assets/logo1.png');

            $xml->addLogo($logo);
            $xml->addText('left',$sale->point_title);
            $xml->addText('right',$sale->id);
            $xml->addText('left',$sale->zone_title);
            $xml->addText('right',date("Y-m-d H:i:s"));
            
            

            $head = ['Urun' , 'Adet' , 'Fiyat'];

            $items = [];
            $i = 0;
            foreach($sale_products as $key => $item){
                $add_products = $item->additional_products;
                $price = $item->price + ($item->price * $item->tax)/100;
                $items[$i] = [
                    $item->title,
                    $item->quantity,
                    $price
                ];
                $i++;

                if($add_products) {
                    foreach ($add_products as $key => $add_product) {
                        $add_price = $add_product->price + ($add_product->price * $add_product->tax)/100;   
                        $items[$i++] = [
                            $add_product->title,
                            $add_product->quantity,
                            $add_price
                        ];
                    }
                }
            }

            $xml->seperator();
            $xml->createList($head , $items , 15 , 14);
            $xml->seperator();
            
            $xml->addText('right','Toplam :'. $amount);
            $xml->addText('right','Ara Ödeme :'. $interim_payment_amount);
            $xml->addText('right','Kalan :'. $remaining_amount);
            
            $xml->Create();

        }
        
    }


}



 ?>