<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Productions extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);
        $this->load->model('Productions_model', 'Productions');
        $this->load->model('Productions_m', 'Productions_M');
    }

    public function ajax($event= null)
    {
        if(method_exists(__CLASS__, (String) $event)) {
            $this->{$event}();
        } else {
            messageAJAX('error', 'Invalid request!');
        }
    }

    function index()
    {
        $productions = $this->Productions->getAll();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

    	$this->load->template('Productions/index', compact('productions'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Productions->insert()) {
                messageAJAX('success', __('Üretim Yeri başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Productions/add');
    }

    public function edit($id = null)
    {
        $production = $this->Productions->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Productions->update(['id' => $id])) {
                messageAJAX('success', __('Üretim Yeri başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->load->template('Productions/edit', compact('production'));
    }

    public function delete($id = null)
    {
        $this->Productions->findOrFail($id);

        $this->Productions->delete(['id' => $id]);

        messageAJAX('success', __('Üretim Yeri başarıyla silindi.'));
    }

    public function getProductions()
    {
        $productions = $this->Productions->getAll();
        
        messageAJAX('success', 'Success', compact('productions'));
    }

    public function getProductionById()
    {
        $id = $this->input->get('id');
        $production = $this->Productions_M->where('id', $id)->get();
        
        messageAJAX('success', 'Success', compact('production'));
    }
}
?>