<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Points extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Points_M');
        $this->load->model('Floors_M');
        $this->load->model('Zones_M');
        $this->load->model('Locations_M');

    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index($location_id = NULL)
    {
        if ($location_id) {
            $this->db->where('location_id', $location_id);
        }

        $points = $this->Points_M
        ->fields('*')
        ->select('(SELECT title FROM locations where locations.id = points.location_id) as location_title')
        ->select('(SELECT title FROM floors where floors.id = points.floor_id) as floor_title')
        ->select('(SELECT title FROM zones where zones.id = points.zone_id) as zone_title')
        ->where('location_id', $this->user->locations)->get_all([
            'service_type' => 1
        ]);
        
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

    	$this->load->template('Points/index', compact('points', 'location_id'));
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {
            if ($this->Points_M->from_form()->insert()) {
                messageAJAX('success', __('Masa başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $service_types = $this->Points_M->findServiceTypes();

        $this->theme_plugin = [
            'js'    => ['panel/js/view/points.js'],
            'start' => 'Points.init({ planner: false });'
        ];

        $this->load->template('Points/add', compact('service_types'));      
    }

    public function addMultiple()
    {
        $count = (int) $this->input->post('table_count');

        if ($count <= 0) {
            messageAJAX('error', __('Masa Adeti Giriniz'));
        }

        for ($i=0; $i < $count; $i++) {
            
            $planner_settings = pointSizeCalc($i);

            $_POST['title'] = 'Masa '.($i+1);
            $_POST['planner_settings'] = $planner_settings;
            $_POST['service_type'] = 1;
            $_POST['zone_id'] = $this->input->get('zone_id');

            if (!$this->Points_M->from_form()->insert()) {
                messageAJAX('error', validation_errors());
                
            }
        }

        messageAJAX('success', __('Masa başarıyla oluşturuldu.'));
        

    }

    public function edit($id = NULL)
    {
        $point = $this->Points_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            if ($this->Points_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Masa başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $floors = $this->Floors_M->get_all([
            'location_id' => $point->location_id
        ]);
        
        $zones = [];
        if ($floors) {
            $floor_ids = array_map(function ($row) { return $row->id; }, $floors);
            $zones = $this->Zones_M->where('floor_id', $floor_ids)->get_all([
                'location_id' => $point->location_id
            ]);
        }

        $service_types = $this->Points_M->findServiceTypes();

        $this->theme_plugin = [
            'js'    => ['panel/js/view/points.js'],
            'start' => 'Points.init({ planner: false });'
        ];

        $this->load->template('Points/edit', compact('point', 'floors', 'zones', 'service_types'));      
    }

    public function delete($id = NULL)
    {
        $this->Points_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Points_M->update(['active' => 3], $id);

        messageAJAX('success', 'Success');
    }

    public function planner()
    {
        $this->theme_plugin = [
            'css'   => ['globals/plugins/jquery-ui/jquery-ui.min.css'],
            'js'    => ['panel/js/view/points.js'],
            'start' => 'Points.init({
                planner: true
            });'
        ];

        $this->load->template('Points/planner');
    }

    private function getPointsByZoneId()
    {
        $zone_id = $this->input->post('zone_id');

        $points = $this->Points_M->get_all([
            'zone_id' => $zone_id
        ]);

        messageAJAX('success', null, compact('points'));
    }

    private function findPointsByData()
    {
        $location_id = (int) $this->input->post('location');
        $floor_id = (int) $this->input->post('floor');
        $zone_id = (int) $this->input->post('zone');

        $points = $this->db->select('id, title, planner_settings')->where([
            'location_id' => $location_id,
            'floor_id' => $floor_id,
            'zone_id' => $zone_id,
            'service_type' => 1,
            'active !=' => 3,
        ])->get('points')->result();

        messageAJAX('success', null, compact('points'));
    }

    private function savePlan()
    {
        $location_id = (int) $this->input->post('location');
        $floor_id = (int) $this->input->post('floor');
        $zone_id = (int) $this->input->post('zone');
        $points = (Array) $this->input->post('points[]');
        $point_ids = [];

        if (isset($_FILES['background']['name'])) {
            $path = 'assets/uploads/zones/';
            @mkdir(FCPATH . $path);

            $config = [
                'encrypt_name'  => true,
                'upload_path'   => FCPATH . $path,
                'allowed_types' => 'gif|jpg|jpeg|jpe|png|svg',
            ];

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('background')){
                messageAJAX('error', $this->upload->display_errors());
            }

            $background = $path . $this->upload->data('file_name');
        }

        foreach ($points as $point_id => $settings) {
            
            $point_ids[] = $point_id = (int) $point_id;

            $this->db->where([
                'id' => $point_id,
                'location_id' => $location_id,
                'floor_id' => $floor_id,
                'zone_id' => $zone_id,
                'active !=' => 3,
            ])
            ->update('points', [
                'planner_settings' => $settings
            ]);
        }


        if ($point_ids)
            $this->db->where_not_in('id', $point_ids);
        $this->db
        ->where([
            'location_id' => $location_id,
            'floor_id' => $floor_id,
            'zone_id' => $zone_id,
            'active !=' => 3,
        ])
        ->update('points', [
            'planner_settings' => null
        ]);

        if (isset($background)) {
            $this->Zones_M->update([
                'background' => $background
            ], ['id' => $zone_id]);
        }

        messageAJAX('success', __('Masa düzeni başarıyla güncellendi.'), compact('background'));
    }
}
?>