<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sales_Component extends Auth_Controller {

    private $inserted_product_ids = [];
    private $interim_payments = [
        'total' => 0,
        // ... other payment types.
    ];
    private $payment_types;
    private $sound_note_path = 'assets/uploads/sound_notes/';
    protected $operator_session;

    function __construct()
    {
        parent::__construct();

        $this->load->helper('file');

        $this->setPaymentTypes();
    }

    protected function getPaymentTypes()
    {
        return $this->payment_types;
    }

    protected function setPaymentTypes()
    {
        $this->payment_types = $this->db
        ->where(['active' => 1])
        ->get('payment_types')
        ->result();
    }

    protected function getPosSettings()
    {
        $pos_settings = $this->session->userdata('pos_settings');

        if (!isset($pos_settings->location_id) || !isset($pos_settings->pos_id)) {
            return false;
        }

        return $pos_settings;
    }

    protected function getOperatorSession()
    {
        $operator_session = $this->session->userdata('operator_session');

        if (!isset($operator_session->user_id)) {
            return false;
        }

        return $operator_session;
    }

    protected function setOperatorSession($user)
    {
        $this->session->set_userdata('operator_session', (Object) [
            'user_id' => (int) $user->id,
            'user_name' => $user->username,
        ]);
    }

    protected function filterRequestedProducts()
    {
        $products = [];
        $input['products'] = (Array) $this->input->post('products');
        
        $i = 0;
        foreach ($input['products'] as $index => $product) {

            if (!isset($product['product_id'], $product['quantity']))
                continue;

            $product_id = (int) $product['product_id'];
            $quantity = (int) $product['quantity'];

            if ($product_id && $quantity) {

                $sale_product_id = isset($product['sale_product_id']) ? $product['sale_product_id'] : null;

                // Products
                $products[$i] = (Object) [
                    'index' => $index,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'sale_product_id' => $sale_product_id,
                    'stocks' => [],
                    'additional_products' => [],
                    'product' => [], // System product
                    'sale_product' => [], // Product in sale_has_products table
                    'text_notes' => [],
                    'sound_notes' => [],
                ];

                // Stoks
                if (isset($product['stocks'])) {
                    $stocks = (Array) $product['stocks'];

                    foreach ($stocks as $stock_id) {
                        $stock_id = (int) $stock_id;

                        if ($stock_id) {
                            $products[$i]->stocks[$stock_id] = $stock_id;
                        }
                    }
                }

                // Additional Prorducts
                if (isset($product['additional_products'])) {
                    $additional_products = (Array) $product['additional_products'];

                    foreach ($additional_products as $additional_product_id) {
                        $additional_product_id = (int) $additional_product_id;

                        if ($additional_product_id) {
                            $products[$i]->additional_products[$additional_product_id] = $additional_product_id;
                        }
                    }
                }

                // Text Notes
                if (isset($product['text_notes'])) {
                    $text_notes = (Array) $product['text_notes'];

                    foreach ($text_notes as $text_note) {
                        $text_note = trim($text_note);

                        if ($text_note) {
                            $products[$i]->text_notes[] = $text_note;
                        }
                    }
                }

                // Sound Notes
                if (isset($product['sound_notes'])) {
                    $sound_notes = (Array) $product['sound_notes'];

                    foreach ($sound_notes as $sound_note) {
                        $sound_note = trim($sound_note);

                        if ($sound_note) {
                            $products[$i]->sound_notes[] = $sound_note;
                        }
                    }
                }

                $i++;
            }
        }

        return $products;
    }

    protected function filterRequestedReturnProducts()
    {
        $products = [];
        $input['products'] = (Array) $this->input->post('products[]');

        foreach ($input['products'] as $sale_product_id => $quantity) {

            $sale_product_id = (int) $sale_product_id;
            $quantity = (int) $quantity;

            if (!$sale_product_id || !$quantity)
                continue;

            $products[$sale_product_id] = (Object) [
                'index' => $sale_product_id,
                'quantity' => $quantity,
                'sale_product_id' => $sale_product_id,
                'additional_products' => [],
                'product' => [], 
                'text_notes' => [],
                'sound_notes' => [],
            ];
        }

        return $products;
    }

    /**
     * [handleRequestedProducts Handle prodcuts requested and exists in sale.]
     * @param  Array       $products      [Requested products]
     * @param  Array|array $sale_products [Products in sale]
     * @return Array                      [The Products will be inserting, updating and deleting.]
     */
    protected function handleRequestedProducts(Array $requested_products, Array $sale_products, Array $system_products)
    {
        $inserts = 
        $updates = 
        $deletes = [];

        foreach ($requested_products as $key => $requested_product) {

            if (!isset($system_products[$requested_product->product_id])) {
                messageAJAX('error', __('Ürün geçersiz.'));
            }

            $requested_product->product = $system_products[$requested_product->product_id];
            
            // Update
            if ($requested_product->sale_product_id) {

                // if the product has at least one interim payment then stock and additional products wont change.
                $interim_payment_counts = 0;

                $requested_product->insert_additional_products = [];
                $requested_product->update_additional_products = [];
                $requested_product->delete_additional_products = [];
                
                $sale_product = null;
                foreach ($sale_products as $_sale_product) {
                    if ($_sale_product->id == $requested_product->sale_product_id) {
                        $sale_product = $_sale_product;
                        break;
                    }
                }

                if (!$sale_product) {
                    messageAJAX('error', __('Sipariş edilen ürün geçersiz.'));
                }

                $interim_payment_counts = $this->countInterimPayments($sale_product);
                if ($interim_payment_counts > $requested_product->quantity) {
                    messageAJAX('error', __('Sipariş edilen ürün adedi ara ödemesi yapılmış adetten az olamaz.'));
                }
                $requested_product->sale_product = $sale_product;

                if ($sale_product->type == 'product') {

                    // Check the stock selections in the product stocks
                    foreach ($requested_product->stocks as $stock_id) {
                        if (!isset($sale_product->stocks[$stock_id])) {
                            messageAJAX('error', __('Malzeme geçersiz.'));
                        }
                        if (!$sale_product->stocks[$stock_id]->optional) {
                            messageAJAX('error', __('Malzeme seçimi geçersiz.'));
                        }

                        $requested_product->sale_product->stocks[$stock_id]->unwanted = true;
                    }
                    if ($interim_payment_counts) {
                        foreach ($sale_product->stocks as $stock) {
                            if ($stock->in_use && isset($requested_product->stocks[$stock->stock_id])) {
                                messageAJAX('error', __('Ara ödemesi alınmış ürünün malzemesi silinemez.'));
                            } else if (!$stock->in_use && !isset($requested_product->stocks[$stock->stock_id])) {
                                messageAJAX('error', __('Ara ödemesi alınmış ürüne malzeme eklenemez.'));
                            }
                        }
                    }
                }

                
                // Loop the new selected additional products
                foreach ($requested_product->additional_products as $additional_product_id) {
                    if (isset($sale_product->additional_products[$additional_product_id])) {
                        $requested_product->update_additional_products[$additional_product_id] = $sale_product->additional_products[$additional_product_id];
                    } else {
                        $requested_product->insert_additional_products[] = $system_products[$additional_product_id];
                        if ($interim_payment_counts) {
                            messageAJAX('error', __('Ara ödemesi alınmış ürüne ilave ürün eklenemez.'));
                        }
                    }
                }
                // Loop the old selected additional products
                foreach ($sale_product->additional_products as $sale_product_additional_product) {
                    // if the old additional product doesn't exist in the new additional product selections, then delete it.
                    if (!isset($requested_product->additional_products[$sale_product_additional_product->product_id])) {
                        $requested_product->delete_additional_products[$sale_product_additional_product->id] = $sale_product_additional_product;
                        if ($interim_payment_counts) {
                            messageAJAX('error', __('Ara ödemesi alınmış ürünün ilave ürünü silinemez.'));
                        }
                    }
                }

                // Loop the new sound notes
                foreach ($requested_product->sound_notes as $key_sound_note => $sound_note) {
                    $sound_note_data = $this->handleSoundNote($sound_note);
                    if ($sound_note_data) {
                        $requested_product->sound_notes[$key_sound_note] = $sound_note_data;
                    } else {
                        messageAJAX('error', __('Sesli not geçersiz.'));
                    }
                }


                $updates[] = $requested_product;
            }

            // Insert
            else {

                if ($requested_product->product->type == 'product') {

                    // Check the stock selections in the product stocks
                    foreach ($requested_product->stocks as $stock_id) {
                        if (!isset($requested_product->product->stocks[$stock_id])) {
                            messageAJAX('error', __('Malzeme geçersiz.'));
                        }
                        if (!$requested_product->product->stocks[$stock_id]->product_stock_optional) {
                            messageAJAX('error', __('Malzeme seçimi geçersiz.'));
                        }

                        $requested_product->product->stocks[$stock_id]->unwanted = true;
                    }

                }
                
                // Check the Additional products in product and pacakge
                foreach ($requested_product->additional_products as $additional_product_id) {
                    if (!isset($requested_product->product->additional_products[$additional_product_id])) {
                        messageAJAX('error', __('İlave Ürün geçersiz.'));
                    }
                }

                // Loop the new sound notes
                foreach ($requested_product->sound_notes as $key_sound_note => $sound_note) {
                    $sound_note_data = $this->handleSoundNote($sound_note);
                    if ($sound_note_data) {
                        $requested_product->sound_notes[$key_sound_note] = $sound_note_data;
                    } else {
                        messageAJAX('error', __('Sesli not geçersiz.'));
                    }
                }

                $inserts[] = $requested_product;
            }
        }

        // Delete
        foreach ($sale_products as $sale_product) {

            // if the product has at least one interim payment then the product wont delete.
            $interim_payment_counts = 0;

            $delete_sale_product = null;
            foreach ($requested_products as $requested_product) {
                if ($requested_product->sale_product_id == $sale_product->id) {
                    $delete_sale_product = 1;
                    break;
                }
            }

            if (!$delete_sale_product) {

                $interim_payment_counts = $this->countInterimPayments($sale_product);
                if ($interim_payment_counts) {
                    messageAJAX('error', __('Ara ödemesi yapılmış ürün iptal edilemez.'));
                }

                $deletes[] = $sale_product;
            }
        }

        $results = compact('inserts', 'updates', 'deletes');

        return $results;
    }

    // New
    protected function handleRequestedReturnProducts(Array $requested_products, Array $sale_products)
    {
        $inserts = 
        $updates = 
        $deletes = [];

        // Liken to sale
        foreach ($requested_products as $key => $requested_product) {

            $sale_product = null;
            foreach ($sale_products as $_sale_product) {
                if ($_sale_product->id == $requested_product->sale_product_id) {

                    // Liken to sale
                    $_sale_product->sale_product_id = $_sale_product->id;
                    $_sale_product->id = $_sale_product->product_id;

                    $sale_product = $_sale_product;
                    break;
                }
            }

            if (!$sale_product) {
                messageAJAX('error', __('Ürün geçersiz.'));
            }
            if (($sale_product->quantity - $sale_product->return_quantity) < $requested_product->quantity) {
               messageAJAX('error', __('İade edilen adet satın alınan adetten fazla olamaz.'));
            }

            if ($sale_product->type == 'product') {
                foreach ($sale_product->stocks as $key_stock => $stock) {
                    // Liken to sale
                    $stock->id = $stock->stock_id;
                    $stock->product_stock_quantity = $stock->quantity;
                    $stock->product_stock_optional = $stock->optional;
                    if (!$stock->in_use) {
                        $stock->unwanted = true;
                    }

                    $sale_product->stocks[$key_stock] = $stock;
                }
            } elseif ($sale_product->type == 'package') {
                
                foreach ($sale_product->products as $key_package_product => $package_product) {
                    // Liken to sale
                    $package_product->sale_product_id = $package_product->id;
                    $package_product->id = $package_product->product_id;

                    foreach ($package_product->stocks as $key_stock => $stock) {
                        // Liken to sale
                        $stock->id = $stock->stock_id;
                        $stock->product_stock_quantity = $stock->quantity;
                        $stock->product_stock_optional = $stock->optional;
                        if (!$stock->in_use) {
                            $stock->unwanted = true;
                        }

                        $package_product->stocks[$key_stock] = $stock;
                    }

                    $sale_product->products[$key_package_product] = $package_product;
                }
            }

            // Liken to sale
            $tmp_additional_products = [];
            foreach ($sale_product->additional_products as $additional_product) {
                // Liken to sale
                $additional_product->sale_product_id = $additional_product->id;
                $additional_product->id = $additional_product->product_id;

                $requested_product->additional_products[] = $additional_product->product_id;

                foreach ($additional_product->stocks as $key_additional_product_stock => $additional_product_stock) {
                    // Liken to sale
                    $additional_product_stock->id = $additional_product_stock->stock_id;
                    $additional_product_stock->product_stock_quantity = $additional_product_stock->quantity;
                    $additional_product_stock->product_stock_optional = $additional_product_stock->optional;

                    $additional_product->stocks[$key_additional_product_stock] = $additional_product_stock;
                }

                $tmp_additional_products[$additional_product->product_id] = $additional_product;
            }
            $sale_product->additional_products = $tmp_additional_products;

            $requested_product->product = $sale_product;
            $inserts[] = $requested_product;
        }

        $results = compact('inserts', 'updates', 'deletes');

        return $results;
    }

    protected function calculateRequestedProducts($products)
    {
        $total =
        $subtotal =
        $taxtotal =
        $gifttotal =
        $giftsubtotal = 0;

        // Prepare interim payment array for $this->calculateInterimPayments
        $this->fillInterimPaymentArray();

        foreach ($products['inserts'] as $product) {
            $tax = $product->product->price * $product->product->tax / 100;
            $tax = round($tax, 2);
            $tax *= $product->quantity;
            $temp_price = round($product->product->price,2);
            $price = $temp_price * $product->quantity;


            $subtotal += $price;
            $taxtotal += $tax;
            $total += $price + $tax;

            foreach ($product->additional_products as $additional_product_id) {
                $additional_product = $product->product->additional_products[$additional_product_id];

                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 2);
                $tax *= $product->quantity;
                $temp_add_price = round($additional_product->price,2);
                $price = $temp_add_price * $product->quantity;

                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
        }

        foreach ($products['updates'] as $product) {
            $tax = $product->sale_product->price * $product->sale_product->tax / 100;
            $tax = round($tax, 2);
            $tax *= $product->quantity;
            $temp_price = round($product->sale_product->price,2);
            $price = $temp_price * $product->quantity;

            if ($product->sale_product->is_gift) {
                $giftsubtotal += $price;
                $gifttotal += $price + $tax;
            } else {
                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
            

            // Interim Payments
            $this->calculateInterimPayments($product->sale_product);

            foreach ($product->update_additional_products as $additional_product) {
                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 2);
                $tax *= $product->quantity;
                $temp_add_price = round($additional_product->price,2);
                $price = $temp_add_price * $product->quantity;

                if ($product->sale_product->is_gift) {
                    $giftsubtotal += $price;
                    $gifttotal += $price + $tax;
                } else {
                    $subtotal += $price;
                    $taxtotal += $tax;
                    $total += $price + $tax;
                }

                // Interim Payments
                $this->calculateInterimPayments($additional_product);
            }

            foreach ($product->insert_additional_products as $additional_product) {
                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 2);
                $tax *= $product->quantity;
                $temp_price = round($additional_product->price,2);
                $price = $temp_price * $product->quantity;

                $subtotal += $price;
                $taxtotal += $tax;
                $total += $price + $tax;
            }
        }

        $result = (Object) [
            'total' => round($total, 3),
            'subtotal' => round($subtotal, 3),
            'taxtotal' => round($taxtotal, 3),
            'gifttotal' => round($gifttotal, 3),
            'giftsubtotal' => round($giftsubtotal, 3),
            'interim_payments' => (Object) $this->interim_payments
        ];

        return $result;
    }

    protected function checkDiscountKey($discount_key)
    {
        if (strlen($discount_key) == 40) {
            $discount = $this->session->userdata('discount');

            if (!$discount) {
                messageAJAX('error', __('İndirim bulunamadı.'));
            } else if ($discount->discount_key != $discount_key) {
                messageAJAX('error', __('Geçersiz indirim.'));
            }

            return $discount;
        }
    }

    protected function calculateSaleDiscount($discount_session_or_sale, $products_total)
    {
        $discount_session_or_sale->discount_total = 0;

        if ($discount_session_or_sale->discount_type == 'amount') {
            $discount_session_or_sale->discount_total = $discount_session_or_sale->discount_amount;
        } else {
            $discount_session_or_sale->discount_total = round($products_total * $discount_session_or_sale->discount_amount / 100, 2);
        }

        return $discount_session_or_sale;
    }

    protected function calculatePayments()
    {
        $inputs = [];
        $payments = [
            'total' => 0
            // ... other payment types
        ];

        foreach ($this->getPaymentTypes() as $payment_type) {

            $inputs[$payment_type->alias] = (Array) $this->input->post("payments[{$payment_type->alias}]");
            $payments[$payment_type->alias] = 0;

            foreach ($inputs[$payment_type->alias] as $value) {
                $payments[$payment_type->alias] += doubleval(str_replace(',', '', $value));
            }

            $payments['total'] += $payments[$payment_type->alias];
        }

        return (Object) $payments;
    }

    protected function processProducts($products, $sale_id, $datetime)
    {
        $this->_insertProducts($products, $sale_id, $datetime);
        $this->_updateProducts($products, $sale_id, $datetime);
        $this->_deleteProducts($products, $datetime);

        return $this->inserted_product_ids;
    }

    protected function processReturnedProducts($products)
    {
        foreach ($products['inserts'] as $product) {

            $this->SaleProducts_M->update([
                'return_quantity' => $product->product->return_quantity + $product->quantity
            ], ['id' => $product->product->sale_product_id]);

            foreach ($product->product->additional_products as $additional_product) {

                $this->SaleProducts_M->update([
                    'return_quantity' => $additional_product->return_quantity + $product->quantity
                ], ['id' => $additional_product->sale_product_id]);
            }
        }
    }

    protected function moveAndMergeTables($location_id, $source_point_id, $target_point_id)
    {
        $target_sale_id = null;
        $datetime = date('Y-m-d H:i:s');

        $source_point = $this->Points_M->where('location_id', $this->user->locations)->findOrFail([
            'id' => $source_point_id,
            'location_id' => $location_id,
            'active' => 1,
        ], 'Geçersiz kaynak masa.');

        $target_point = $this->Points_M->where('location_id', $this->user->locations)->findOrFail([
            'id' => $target_point_id,
            'location_id' => $location_id,
            'active' => 1,
        ], 'Geçersiz hedef masa.');

        if (!$source_point->sale_id) {
            messageAJAX('error', __('Kaynak masa boş.'));
        } elseif ($source_point->id == $target_point->id) {
            messageAJAX('error', __('Taşınacak masalar farklı olmalı.'));
        }

        $sale = $this->Sales_M
        ->findSaleWithProducts([
            'id' => $source_point->sale_id,
            'location_id' => $location_id,
            'point_id' => $source_point_id,
            'status' => 'pending',
            'active' => 1,
        ])
        OR messageAJAX('error', __('Kaynak masada sipariş bulunamadı.'));

        if ($target_point->sale_id) {
            $target_sale = $this->Sales_M
            ->findSaleWithProducts([
                'id' => $target_point->sale_id,
                'location_id' => $location_id,
                'point_id' => $target_point_id,
                'status' => 'pending',
                'active' => 1,
            ]);

            if ($target_sale) {
                $target_sale_id = $target_sale->id;
            }
        }

        // Cancelling the source sale
        $this->Sales_M->update([
            'status' => 'cancelled'
        ], ['id' =>  $sale->id]);

        $this->Points_M->update([
            'sale_id' => null,
            'status' => 0,
            'opened_at' => null,
        ], ['id' => $source_point->id]);

        $products = $sale->products;
        unset($sale->products);

        if ($target_sale_id) {
            unset($target_sale->products);
            $target_sale->amount += $sale->amount;
            $target_sale->subtotal += $sale->subtotal;
            $target_sale->taxtotal += $sale->taxtotal;
            $target_sale->gift_amount += $sale->gift_amount;
            $target_sale->interim_payment_amount += $sale->interim_payment_amount;
            $this->Sales_M->update($target_sale, ['id' => $target_sale->id]);
        } else {
            $sale->id = null;
            $sale->point_id = $target_point->id;
            $sale->floor_id = $target_point->floor_id;
            $sale->zone_id = $target_point->zone_id;
            $sale->location_id = $target_point->location_id;
            $target_sale_id = $this->Sales_M->insert($sale);

            $this->Points_M->update([
                'sale_id' => $target_sale_id,
                'status' => 1,
                'opened_at' => $datetime,
            ], ['id' => $target_point->id]);
        }

        foreach ($products as $product) {

            $additional_products = $product->additional_products;
            unset($product->additional_products);

            if ($product->type == 'package') {
                $package_products = $product->products;
                unset($product->products);
            } elseif ($product->type == 'product') {
                $stocks = $product->stocks;
                unset($product->stocks);
            }

            $product->id = null;
            $product->sale_id = $target_sale_id;
            $product->created = $datetime;
            $this->db->insert('sale_has_products', $product);
            $new_sale_product_id = $this->db->insert_id();

            if ($product->type == 'package') {
                foreach ($package_products as $package_product) {
                    $package_product_stocks = $package_product->stocks;
                    unset($package_product->stocks, $package_product->additional_products);

                    $package_product->id = null;
                    $package_product->package_id = $new_sale_product_id;
                    $this->db->insert('package_has_products', $package_product);
                    $new_package_product_id = $this->db->insert_id();

                    foreach ($package_product_stocks as $package_product_stock) {
                        unset($package_product_stock->stock_name);
                        $package_product_stock->sale_package_id = $new_package_product_id;
                        $this->db->insert('sale_packages_stocks', $package_product_stock);
                    }
                }
            } elseif ($product->type == 'product') {
                foreach ($stocks as $stock) {
                    unset($stock->stock_name);
                    $stock->sale_product_id = $new_sale_product_id;
                    $this->db->insert('sale_products_stocks', $stock);
                }
            }

            foreach ($additional_products as $additional_product) {

                $additional_product_stocks = $additional_product->stocks;
                unset($additional_product->stocks, $additional_product->additional_products);
                
                $additional_product->id = null;
                $additional_product->additional_sale_product_id = $new_sale_product_id;
                $additional_product->sale_id = $target_sale_id;
                $additional_product->created = $datetime;
                $this->db->insert('sale_has_products', $additional_product);
                $new_sale_additional_product_id = $this->db->insert_id();

                foreach ($additional_product_stocks as $additional_product_stock) {
                    unset($additional_product_stock->stock_name);
                    $additional_product_stock->sale_product_id = $new_sale_additional_product_id;
                    $this->db->insert('sale_products_stocks', $additional_product_stock);
                }
            }
        }

        return $target_sale_id;
    }

    protected function makeInterimPayment($products, $sale_id, $location_id, $point_id, $payment_type)
    {
        $interim_payment_amount = 0;
        $datetime = date('Y-m-d H:i:s');

        $sale_product_updates = [];
        $payment_types = $this->getPaymentTypes();
        $payment_type_aliases = array_map(function ($row) { return $row->alias; }, $payment_types);

        if (!$products) {
            messageAJAX('error', __('En az bir ürün seçmelisiniz.'));
        } elseif (!in_array($payment_type, $payment_type_aliases)) {
            messageAJAX('error', __('Seçilen ödeme geçersiz.'));
        }

        $sale = $this->Sales_M->where('location_id', $this->user->locations)->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'point_id' => $point_id,
            'status' => 'pending',
            'active' => 1,
        ])
        OR messageAJAX('error', __('Satış bulunamadı.'));

        foreach ($products as $sale_product_id => $quantity) {
            
            $paid = 0;
            $unpaid = 0;
            $quantity = abs($quantity);
            $sale_product_id = (int) $sale_product_id;

            if (!$quantity)
                continue;

            $sale_product = null;
            foreach ($sale->products as $product) {
                if ($product->id == $sale_product_id) {
                    $sale_product = $product;

                    $price = $product->price + $product->price * $product->tax / 100;
                    $price = round($price, 2);
                    $interim_payment_amount += $price * $quantity;

                    foreach ($product->additional_products as $additional_product) {
                        $price = $additional_product->price + $additional_product->price * $additional_product->tax / 100;
                        $price = round($price, 2);
                        $interim_payment_amount += $price * $quantity;
                    }
                    break;
                }
            }
            if (!$sale_product) {
                messageAJAX('error', __('Hatalı ürün.'));
            }

            foreach ($payment_types as $type) {
                $paid += $sale_product->{"paid_{$type->alias}"};
            }

            $unpaid = $sale_product->quantity - $paid;
            if ($unpaid < $quantity) {
                messageAJAX('error', __('{product} için en fazla {quantity} adet ödeme yapabilirsiniz.', [
                    '{product}' => $sale_product->title,
                    '{quantity}' => $unpaid,
                ]));
            }

            $sale_product_updates[$sale_product->id] = [
                "paid_{$payment_type}" => $sale_product->{"paid_{$payment_type}"} + $quantity,
                'modified' => $datetime,
            ];
        }

        foreach ($sale_product_updates as $id => $update) {
            $this->SaleProducts_M
                ->group_start()
                    ->where('id', $id)
                    ->or_where('additional_sale_product_id', $id)
                ->group_end()
            ->update($update);
        }


        $this->Sales_M->update([
            'interim_payment_amount' => $sale->interim_payment_amount + $interim_payment_amount
        ], [
            'id' => $sale->id
        ]);

        return $interim_payment_amount;
    }

    protected function makeProductsAsGift($products, $sale_id, $location_id, $point_id)
    {
        $gift_amount =
        $gift_subtotal = 0;
        $datetime = date('Y-m-d H:i:s');

        $sale_product_inserts = [];
        $sale_product_updates = [];
        $new_sale_product_ids = [];
        $payment_types = $this->getPaymentTypes();

        if (!$products) {
            messageAJAX('error', __('En az bir ürün seçmelisiniz.'));
        }

        $sale = $this->Sales_M->findSaleWithProducts([
            'id' => $sale_id,
            'location_id' => $location_id,
            'point_id' => $point_id,
            'status' => 'pending',
            'active' => 1,
        ]);

        if (!$sale) {
            messageAJAX('error', __('Satış bulunamadı.'));
        }

        foreach ($products as $sale_product_id => $quantity) {
            
            $paid = 0;
            $unpaid = 0;
            $quantity = abs($quantity);
            $sale_product_id = (int) $sale_product_id;

            if (!$quantity)
                continue;

            $sale_product = null;
            foreach ($sale->products as $product) {
                if ($sale_product_id == $product->id) {
                    $sale_product = $product;
                }
            }

            if (!$sale_product) {
                messageAJAX('error', __('Hatalı ürün.'));
            }

            foreach ($payment_types as $type) {
                $paid += $sale_product->{"paid_{$type->alias}"};
            }

            $unpaid = $sale_product->quantity - $paid;
            if ($unpaid < $quantity) {
                messageAJAX('error', __('{product} için en fazla {quantity} adet ikram yapabilirsiniz.', [
                    '{product}' => $sale_product->title,
                    '{quantity}' => $unpaid,
                ]));
            }

            $sale_product_updates[$sale_product->id] = [
                'quantity' => $sale_product->quantity - $quantity,
                'modified' => $datetime,
            ];

            $sale_product->quantity = $quantity;
            foreach ($this->getPaymentTypes() as $payment_type)
                $sale_product->{"paid_{$payment_type->alias}"} = 0;
            $sale_product_inserts[$sale_product->id] = $sale_product;
        }

        // Update main product
        foreach ($sale_product_updates as $id => $update) {
            $this->SaleProducts_M
                ->group_start()
                    ->where('id', $id)
                    ->or_where('additional_sale_product_id', $id)
                ->group_end()
            ->update($update);
        }

        // Insert product as gift
        foreach ($sale_product_inserts as $product) {

            $additional_products = $product->additional_products;
            unset($product->additional_products);

            if ($product->type == 'package') {
                $package_products = $product->products;
                unset($product->products);
            } elseif ($product->type == 'product') {
                $stocks = $product->stocks;
                unset($product->stocks);
            }

            $tax = $product->price * $product->tax / 100;
            $tax = round($tax, 2);
            $tax *= $product->quantity;
            $price = $product->price * $product->quantity;
            $gift_subtotal += $price;
            $gift_amount += $price + $tax;

            $product->id = null;
            $product->user_id = $this->operator_session->user_id;
            $product->is_gift = 1;
            $new_sale_product_id = $this->SaleProducts_M->insert($product);
            $new_sale_product_ids[] = $new_sale_product_id;


            if ($product->type == 'package') {
                foreach ($package_products as $package_product) {
                    $package_product_stocks = $package_product->stocks;
                    unset($package_product->stocks, $package_product->additional_products);

                    $package_product->id = null;
                    $package_product->package_id = $new_sale_product_id;
                    $this->db->insert('package_has_products', $package_product);
                    $new_package_product_id = $this->db->insert_id();

                    foreach ($package_product_stocks as $package_product_stock) {
                        unset($package_product_stock->stock_name);
                        $package_product_stock->sale_package_id = $new_package_product_id;
                        $this->db->insert('sale_packages_stocks', $package_product_stock);
                    }
                }
            } elseif ($product->type == 'product') {
                foreach ($stocks as $stock) {
                    unset($stock->stock_name);
                    $stock->sale_product_id = $new_sale_product_id;
                    $this->db->insert('sale_products_stocks', $stock);
                }
            }

            foreach ($additional_products as $additional_product) {

                $additional_product_stocks = $additional_product->stocks;
                unset($additional_product->stocks, $additional_product->additional_products);

                $tax = $additional_product->price * $additional_product->tax / 100;
                $tax = round($tax, 2);
                $tax *= $product->quantity;
                $price = $additional_product->price * $product->quantity;
                $gift_subtotal += $price;
                $gift_amount += $price + $tax;
                
                $additional_product->id = null;
                $additional_product->quantity = $product->quantity;
                $additional_product->is_gift = 1;
                $additional_product->additional_sale_product_id = $new_sale_product_id;
                $additional_product->created = $datetime;
                foreach ($this->getPaymentTypes() as $payment_type)
                    $additional_product->{"paid_{$payment_type->alias}"} = 0;
                $this->db->insert('sale_has_products', $additional_product);
                $new_sale_additional_product_id = $this->db->insert_id();

                foreach ($additional_product_stocks as $additional_product_stock) {
                    unset($additional_product_stock->stock_name);
                    $additional_product_stock->sale_product_id = $new_sale_additional_product_id;
                    $this->db->insert('sale_products_stocks', $additional_product_stock);
                }
            }
        }

        $this->Sales_M->update([
            'amount' => $sale->amount - $gift_amount,
            'subtotal' => $sale->subtotal - $gift_subtotal,
            'gift_amount' => $sale->gift_amount + $gift_amount,
        ], [
            'id' => $sale->id
        ]);


        return $new_sale_product_ids;
    }

    protected function createDiscountKey($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->form_validation
        ->set_rules('location_id' , 'Restoran', 'trim|required')
        ->set_rules('username' , 'Kullanıcı Adı/Eposta', 'trim|required')
        ->set_rules('password' , 'Şifre', 'trim|required')
        ->set_rules('discount_amount' , 'İndirim miktarı', 'trim|required|is_money')
        ->set_rules('discount_type' , 'İndirim tipi', 'trim|required|in_list[amount,percent]');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $location_id = $this->input->post('location_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $discount_amount = $this->input->post('discount_amount');
        $discount_type = $this->input->post('discount_type');
        $discount_key = sha1(uniqid());

        $user = $this->db
        ->select('users.id, users.password, users.role_id')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $username)
                ->or_where('users.email', $username)
            ->group_end()
        ->join('roles', 'roles.id = users.role_id')
        ->where('roles.active', 1)
        ->get('users', 1)->row() 
        OR 
        messageAJAX('error' , __('Hatalı giriş.'));

        if ($user->password != sha1($password . $this->config->config['salt'])) {
            messageAJAX('error' , __('Hatalı giriş.'));
        }
        if ($user->role_id != 1) {
            if (!isset($this->user->permissions['pointofsales.adddiscount'])) {
                messageAJAX('error', __('Yetkilendirme hatası.'));
            }
        }

        $this->session->set_userdata('discount', (Object) [
            'discount_key' => $discount_key,
            'discount_amount' => $discount_amount,
            'discount_type' => $discount_type,
        ]);

        return $discount_key;
    }

    protected function createProductReturnSession($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->form_validation
        ->set_rules('location_id' , 'Restoran', 'trim|required')
        ->set_rules('sale_id' , 'Satış No', 'trim|required')
        ->set_rules('username' , 'Kullanıcı Adı/Eposta', 'trim|required')
        ->set_rules('password' , 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $location_id = $this->input->post('location_id');
        $sale_id = $this->input->post('sale_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $session = sha1(uniqid());

        $user = $this->db
        ->select('users.password, users.role_id')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $username)
                ->or_where('users.email', $username)
            ->group_end()
        ->join('roles', 'roles.id = users.role_id')
        ->where('roles.active', 1)
        ->get('users', 1)->row() 
        OR 
        messageAJAX('error' , __('Hatalı giriş.'));

        if ($user->password != sha1($password . $this->config->config['salt'])) {
            messageAJAX('error' , __('Hatalı giriş.'));
        }

        if ($user->role_id != 1) {
            if (!isset($this->user->permissions['pointofsales.return'])) {
                messageAJAX('error', __('Yetkilendirme hatası.'));
            }
        }


        $this->Sales_M->where('location_id', $this->user->locations)->findOrFail([
            'id' => $sale_id,
            'location_id' => $location_id,
            'status' => 'completed',
            'return_id' => null,
            'active' => 1,
        ])
        OR messageAJAX('error', __('Geçersiz satış.'));


        $this->session->set_userdata('return_session', $session);

        return $session;
    }

    protected function createPosSettingsSession($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->form_validation
        ->set_rules('location_id', 'Restoran', 'trim|required')
        ->set_rules('pos_id', 'Kasa', 'trim|required')
        ->set_rules('username', 'Kullanıcı adı/Eposta', 'trim|required')
        ->set_rules('password', 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $location_id = $this->input->post('location_id');
        $pos_id = $this->input->post('pos_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->db
        ->select('users.password, users.role_id')
            ->where('users.active', 1)
            ->group_start()
                ->where('users.username', $username)
                ->or_where('users.email', $username)
            ->group_end()
        ->join('roles', 'roles.id = users.role_id')
        ->where('roles.active', 1)
        ->get('users', 1)->row() 
        OR 
        messageAJAX('error' , __('Hatalı giriş.'));

        if ($user->password != sha1($password . $this->config->config['salt'])) {
            messageAJAX('error' , __('Hatalı giriş.'));
        }

        if ($user->role_id != 1) {
            if (!isset($this->user->permissions['pointofsales.setup'])) {
                messageAJAX('error', __('Yetkilendirme hatası.'));
            }
        }


        $this->Locations_M->where('id', $this->user->locations)->findOrFail([
            'id' => $location_id,
            'active' => 1,
        ])
        OR messageAJAX('error', __('Geçersiz Restoran.'));

        $this->Pos_M->where('location_id', $this->user->locations)->findOrFail([
            'id' => $pos_id,
            'location_id' => $location_id,
            'active' => 1,
        ])
        OR messageAJAX('error', __('Geçersiz Kasa.'));

        $this->session->set_userdata('pos_settings', (Object) [
            'location_id' => (int) $location_id,
            'pos_id' => (int) $pos_id,
        ]);
    }

    protected function createOperatorSession($values)
    {
        $_POST = array_merge($_POST, $values);

        $this->form_validation->set_rules('password', 'Şifre', 'trim|required');

        if (!$this->form_validation->run()) {
            messageAJAX('error' , validation_errors());
        }

        $password = $this->input->post('password');

        $user = $this->db->select('id,username,email')->where([
            'active' => 1,
            'pin' => $password,
        ])->get('users', 1)->row()
        OR 
        messageAJAX('error' , __('Hatalı giriş.'));

        $this->setOperatorSession($user);

        return $user;
    }

    private function _insertProducts($products, $sale_id, $datetime)
    {
        // Insert Products
        foreach ($products['inserts'] as $key => $product) {
            $this->db->insert('sale_has_products', [
                'user_id' => $this->operator_session->user_id,
                'sale_id' => $sale_id,
                'product_id' => $product->product->id,
                'title' => $product->product->title,
                'price' => $product->product->price,
                'tax' => $product->product->tax,
                'type' => $product->product->type,
                'quantity' => $product->quantity,
                'bonus' => $product->product->bonus,
                'bonus_type' => $product->product->bonus_type,
               /*'completed' => 0,
                'seen' => 0,
                'paid_cash' => 0,
                'paid_credit' => 0,
                'paid_vaucher' => 0,
                'paid_ticket' => 0,
                'paid_sodexo' => 0,*/
                'created' => $datetime,
                'active' => 1,
            ]);
            $new_sale_product_id = $this->db->insert_id();

            $this->inserted_product_ids[] = [
                'index' => $product->index,
                'sale_product_id' => $new_sale_product_id,
            ];

            if ($product->product->type == 'product') {

                foreach ($product->product->stocks as $stock) {
                    $this->db->insert('sale_products_stocks', [
                        'sale_product_id' => $new_sale_product_id,
                        'stock_id' => $stock->id,
                        'quantity' => $stock->product_stock_quantity,
                        'optional' => $stock->product_stock_optional,
                        'in_use' => isset($stock->unwanted) ? 0 : 1,
                    ]);
                }

            } elseif ($product->product->type == 'package') {

                foreach ($product->product->products as $package_product) {
                    
                    $this->db->insert('package_has_products', [
                        'package_id' => $new_sale_product_id,
                        'product_id' => $package_product->id,
                        'title' => $package_product->title,
                        'price' => $package_product->price,
                        'tax' => $package_product->tax,
                        'content' => $package_product->content,
                        'quantity' => $product->quantity,
                        'created' => $datetime,
                        'active' => 1,
                    ]);
                    $new_package_product_id = $this->db->insert_id();

                    foreach ($package_product->stocks as $stock) {
                        $this->db->insert('sale_packages_stocks', [
                            'sale_package_id' => $new_package_product_id,
                            'stock_id' => $stock->id,
                            'quantity' => $stock->product_stock_quantity,
                            'optional' => $stock->product_stock_optional,
                            'in_use' => 1,
                        ]);
                    }
                }
            }

            foreach ($product->additional_products as $additional_product_id) {
                foreach ($product->product->additional_products as $additional_product) {
                    if ($additional_product->id == $additional_product_id) {
                        $this->db->insert('sale_has_products', [
                            'user_id' => $this->operator_session->user_id,
                            'sale_id' => $sale_id,
                            'product_id' => $additional_product->id,
                            'title' => $additional_product->title,
                            'additional_sale_product_id' => $new_sale_product_id,
                            'price' => $additional_product->price,
                            'tax' => $additional_product->tax,
                            'type' => $additional_product->type,
                            'quantity' => $product->quantity,
                            'bonus' => $additional_product->bonus,
                            'bonus_type' => $additional_product->bonus_type,
                           /*'completed' => 0,
                            'seen' => 0,
                            'paid_cash' => 0,
                            'paid_credit' => 0,
                            'paid_vaucher' => 0,
                            'paid_ticket' => 0,
                            'paid_sodexo' => 0,*/
                            'created' => $datetime,
                            'active' => 1,
                        ]);
                        $new_sale_additional_product_id = $this->db->insert_id();

                        foreach ($additional_product->stocks as $additional_product_stock) {
                            $this->db->insert('sale_products_stocks', [
                                'sale_product_id' => $new_sale_additional_product_id,
                                'stock_id' => $additional_product_stock->id,
                                'quantity' => $additional_product_stock->product_stock_quantity,
                                'optional' => $additional_product_stock->product_stock_optional,
                                'in_use' => 1,
                            ]);
                        }
                    }
                }
            }

            // Text Notes
            foreach ($product->text_notes as $text_note) {
                $this->SaleProductNotes_M->insert([
                    'sale_product_id' => $new_sale_product_id,
                    'note' => $text_note,
                    'viewed' => 0,
                    'type' => 1,
                ]);
            }
            // Sound Notes
            foreach ($product->sound_notes as $sound_note) {
                if (write_file(FCPATH . $sound_note->fullname, base64_decode($sound_note->base64))) {
                    $this->SaleProductNotes_M->insert([
                        'sale_product_id' => $new_sale_product_id,
                        'sound' => $sound_note->fullname,
                        'viewed' => 0,
                        'type' => 2,
                    ]);
                }
            }
        }
        // END - Insert Products
    }

    private function _updateProducts($products, $sale_id, $datetime)
    {
        // Update Products
        foreach ($products['updates'] as $key => $product) {
            $this->db
            ->where('id', $product->sale_product->id)
            ->update('sale_has_products', [
                'quantity' => $product->quantity,
                /*'seen' => 0,
                'paid_cash' => 0,
                'paid_credit' => 0,
                'paid_vaucher' => 0,
                'paid_ticket' => 0,
                'paid_sodexo' => 0,*/
                'modified' => $datetime,
            ]);

            if ($product->sale_product->type == 'product') {

                $this->db->where('sale_product_id', $product->sale_product->id)->delete('sale_products_stocks');

                foreach ($product->sale_product->stocks as $stock) {
                    $this->db
                    ->insert('sale_products_stocks', [
                        'sale_product_id' => $stock->sale_product_id,
                        'stock_id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'optional' => $stock->optional,
                        'in_use' => isset($stock->unwanted) ? 0 : 1,
                    ]);
                }

            } elseif ($product->sale_product->type == 'package') {
                foreach ($product->sale_product->products as $package_product) {
                    
                    $this->db
                    ->where('id', $package_product->id)
                    ->update('package_has_products', [
                        'quantity' => $product->quantity,
                        'modified' => $datetime,
                    ]);

                    $this->db->where('sale_package_id', $package_product->id)->delete('sale_packages_stocks');

                    foreach ($package_product->stocks as $stock) {
                        $this->db->insert('sale_packages_stocks', [
                            'sale_package_id' => $package_product->id,
                            'stock_id' => $stock->stock_id,
                            'quantity' => $stock->quantity,
                            'optional' => $stock->optional,
                            'in_use' => 1,
                        ]);
                    }
                }
            }

            // Insert the updated prodcut's additional products and stocks
            foreach ($product->insert_additional_products as $additional_product) {
                $this->db->insert('sale_has_products', [
                    'user_id' => $this->operator_session->user_id,
                    'sale_id' => $sale_id,
                    'product_id' => $additional_product->id,
                    'additional_sale_product_id' => $product->sale_product->id,
                    'price' => $additional_product->price,
                    'tax' => $additional_product->tax,
                    'type' => $additional_product->type,
                    'quantity' => $product->quantity,
                    'bonus' => $additional_product->bonus,
                    'bonus_type' => $additional_product->bonus_type,
                   /*'completed' => 0,
                    'seen' => 0,
                    'paid_cash' => 0,
                    'paid_credit' => 0,
                    'paid_vaucher' => 0,
                    'paid_ticket' => 0,
                    'paid_sodexo' => 0,*/
                    'created' => $datetime,
                    'active' => 1,
                ]);
                $new_sale_additional_product_id = $this->db->insert_id();

                foreach ($additional_product->stocks as $additional_product_stock) {
                    $this->db->insert('sale_products_stocks', [
                        'sale_product_id' => $new_sale_additional_product_id,
                        'stock_id' => $additional_product_stock->id,
                        'quantity' => $additional_product_stock->product_stock_quantity,
                        'optional' => $additional_product_stock->product_stock_optional,
                        'in_use' => 1,
                    ]);
                }
            }
            // Update the updated product's addtional products and stocks
            foreach ($product->update_additional_products as $additional_product) {
                $this->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'quantity' => $product->quantity,
                    'modified' => $datetime,
                ]);

                $this->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');

                foreach ($additional_product->stocks as $stock) {
                    $this->db
                    ->insert('sale_products_stocks', [
                        'sale_product_id' => $stock->sale_product_id,
                        'stock_id' => $stock->stock_id,
                        'quantity' => $stock->quantity,
                        'optional' => $stock->optional,
                        'in_use' => 1,
                    ]);
                }
            }
            // Delete the updated product's addtional products and stocks
            foreach ($product->delete_additional_products as $additional_product) {
                $this->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'active' => 3,
                    'modified' => $datetime,
                ]);

                $this->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');
            }

            // Text Notes
            foreach ($product->text_notes as $text_note) {
                $this->SaleProductNotes_M->insert([
                    'sale_product_id' => $product->sale_product->id,
                    'note' => $text_note,
                    'viewed' => 0,
                    'type' => 1,
                ]);
            }
            // Sound Notes
            foreach ($product->sound_notes as $sound_note) {
                if (write_file(FCPATH . $sound_note->fullname, base64_decode($sound_note->base64))) {
                    $this->SaleProductNotes_M->insert([
                        'sale_product_id' => $product->sale_product->id,
                        'sound' => $sound_note->fullname,
                        'viewed' => 0,
                        'type' => 2,
                    ]);
                }
            }
        }
        // END - Update Products
    }

    private function _deleteProducts($products, $datetime)
    {
        // Delete Products
        foreach ($products['deletes'] as $key => $product) {
            $this->db
            ->where('id', $product->id)
            ->update('sale_has_products', [
                'active' => 3,
                'modified' => $datetime,
            ]);

            if ($product->type == 'product') {

                $this->db->where('sale_product_id', $product->id)->delete('sale_products_stocks');

            } elseif ($product->type == 'package') {
                foreach ($product->products as $package_product) {
                    
                    $this->db
                    ->where('id', $package_product->id)
                    ->update('package_has_products', [
                        'active' => 3,
                        'modified' => $datetime,
                    ]);

                    $this->db->where('sale_package_id', $package_product->id)->delete('sale_packages_stocks');
                }

            }

            // Delete Additional products
            foreach ($product->additional_products as $additional_product) {
                $this->db
                ->where('id', $additional_product->id)
                ->update('sale_has_products', [
                    'active' => 3,
                    'modified' => $datetime,
                ]);

                $this->db->where('sale_product_id', $additional_product->id)->delete('sale_products_stocks');
            }
        }
        // END - Delete Products
    }

    private function calculateInterimPayments($product)
    {
        foreach ($this->getPaymentTypes() as $payment_type) {

            if (empty($this->interim_payments[$payment_type->alias])) {
                $this->interim_payments[$payment_type->alias] = 0;
            }

            $price = $product->price + $product->price * $product->tax / 100;
            $price = round($price, 2);
            $total = $price * $product->{"paid_{$payment_type->alias}"};

            $this->interim_payments[$payment_type->alias] += $total;
            $this->interim_payments['total'] += $total;
        }
    }

    private function fillInterimPaymentArray()
    {
        foreach ($this->getPaymentTypes() as $payment_type) {
            $this->interim_payments[$payment_type->alias] = 0;
        }
    }

    private function countInterimPayments($product)
    {
        $count = 0;
        foreach ($this->getPaymentTypes() as $payment_type) {
            $count += $product->{"paid_{$payment_type->alias}"};
        }
        return $count;
    }

    protected function getPaymentTypeId($payments, $interim_payments)
    {
        $payment_type_id = null;

        foreach ($this->getPaymentTypes() as $payment_type) {
            if ($payments->{$payment_type->alias} || $interim_payments->{$payment_type->alias}) {

                // if second payment type exists then this is multiple payment and return null.
                if ($payment_type_id) {
                    return null;
                }

                $payment_type_id = $payment_type->id;
            }
        }

        return $payment_type_id;
    }

    protected function getPaymentTypeIdByAlias($payment_type)
    {
        $payment_type_id = null;

        foreach ($this->getPaymentTypes() as $_payment_type) {
            if ($_payment_type->alias == $payment_type) {
                $payment_type_id = $_payment_type->id;
                break;
            }
        }

        return $payment_type_id;
    }

    private function handleSoundNote($sound_note)
    {
        preg_match('/^data:audio\/mp3;base64,([a-zA-Z0-9+\/]+={0,3})$/', $sound_note, $matches);
        if ($matches) {
            @mkdir($this->sound_note_path);
            $name =  md5(uniqid()) . '.mp3';
            return (Object) [
                'name' => $name,
                'base64' => $matches[1],
                'fullname' => $this->sound_note_path . $name,
            ];
        }
    }

}
?>
