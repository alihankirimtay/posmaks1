    <?php
    if (!defined('BASEPATH')) exit('No direct script access allowed');

    class Ajax extends Auth_Controller {

        function __construct()
        {
            parent::__construct();

        // $this->load->model('Ajax_model');
        }

        function index()
        {
        }

        function uploadAttachment()
        {
            if(! $this->input->is_ajax_request()) return false;

            $mime = array(
                'image' => 'gif|jpg|jpeg|png',
                'video' => 'avi|mp4|mpeg|mpg|rv|mpe|qt|mov|movie|3g2|3gp|f4v|webm|wmv|wma',
                'audio' => 'mp3|mp4|wav|wma|mid|midi|mpga|mp2|aif|aiff|aifc|ram|rm|rpm|ra|m4a|aac|au|ac3|flac|ogg',
                'file'  => 'exe|psd|pdf|xls|ppt|pptx|wbxml|gz|gzip|swf|tar|tgz|z|zip|rar|html|htm|shtml|txt|text|doc|docx|dot|dotx|xlsx|word|xl|json|7zip|jar|svg|vcf',
            );

            $this->load->helper('string');

            $config['upload_path']      = 'assets/uploads/TMP/';
            $config['allowed_types']    = $mime['image'].'|'.$mime['video'].'|'.$mime['audio'].'|'.$mime['file'];
            $config['max_size']         = '25000000';
            $config['file_name']        = sprintf('(TMP)%d-%s-%s-%s-%s-%s',
                                                $this->user->id,
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                strtoupper(random_string('alnum', 4)),
                                                str_replace('.', '_', microtime(1)));
            
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('_file')) messageAJAX('error', $this->upload->display_errors(NULL, NULL));
            
            $data = $this->upload->data();
            $data['upload_path'] = $config['upload_path'];

            messageAJAX('success', 'Success', $data);
        }
    }