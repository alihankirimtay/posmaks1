<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shiftschedule extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['report']
        ]);
    }

    function index($location = NULL)
    {
        $data[ 'location' ] = (int) $location;

        if (!$data[ 'location' ]) {
            $data[ 'location' ] = $this->user->locations[0];
        }

        $date = $this->input->get('date');
        if (!isValidDate($date, dateFormat1())) {
            $start = dateConvert2(date('Y-m-d H:i:s'), 'client', 'Y-m-d H:i:s', 'Y-m-d 00:00:00');
            $start = dateConvert2($start, 'server', 'Y-m-d H:i:s', 'Y-m-d H:i:s');
        } else {
            $date = dateReFormat($date, dateFormat1(), 'Y-m-d 00:00:00');
            $start = dateConvert2($date, 'server', 'Y-m-d H:i:s', 'Y-m-d H:i:s');
        }

        $end = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($start)));

        $data['dates'][1] = [
            'start' => $start,
            'end' => $end
        ];
        for ($i=2; $i < 8; $i++) { 
            $data['dates'][$i] = [
                'start' => date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($data['dates'][$i - 1]['start']))),
                'end' => date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($data['dates'][$i - 1]['end']))),
            ];
        }

        $data['users'] = $this->db
            ->select('Users.*')
            ->where([
                'Users.active !=' => 3,
                'Users.role_id !=' => 1
            ])
            ->join('user_has_locations AS UserHasLocations', 'UserHasLocations.user_id = Users.id')
            ->where('UserHasLocations.location_id', $data[ 'location' ])
            ->get('users AS Users')->result();
        $data['users_count'] = count($data['users']) + 1;

        $data['schedules'] = $this->db
        ->where([
            'active' => 1,
            'location_id' => $data[ 'location' ]
        ])
        ->group_start()
            ->group_start() // filtrelenen tarihin kayıtlı vardiyaların tarihleri aralığına denk gelenleri getir.
                ->where("'$start' BETWEEN `start_date` AND  `end_date`", null, FALSE)
                ->or_where("'{$data['dates'][$i-1]['end']}' BETWEEN `start_date` AND  `end_date`", null, FALSE)
            ->group_end()

            ->or_group_start() // filtrelenen tarih aralığındaki tüm vardiyaları getir.
                ->where('start_date >=', $start)
                ->where('end_date <=', $data['dates'][$i-1]['end'])
            ->group_end()
        ->group_end()

        ->group_by('id')
        ->get('shift_schedule')->result();


        $this->theme_plugin = array(
            'js'    => [ 'panel/js/view/shiftschedule.js', 'globals/plugins/printjs/print.min.js' ],
            'start' => 'Shiftschedule.init(); MyFunction.datetimepicker("'.dateFormat1(TRUE).'");'
        );

        $this->load->template('Shiftschedule/index', $data);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }


        if ($this->input->post()) {
            
            $client_date_fromat = dateFormat();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('date_start', 'Başlangıç Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
            $this->form_validation->set_rules('date_end', 'Bitiş Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
            $this->form_validation->set_rules('user', 'Personel', 'required|trim|is_natural_no_zero');
            $this->form_validation->set_rules('location', 'Restoran', 'required|trim|is_natural_no_zero', ['is_natural_no_zero' => 'Restoran seçin']);

            if (!$this->form_validation->run()) {
                messageAJAX('error', validation_errors());
            }
            
            $date_start = $this->input->post('date_start');
            $date_end = $this->input->post('date_end');
            $user_id = $this->input->post('user');
            $location_id = $this->input->post('location');

            if (!preg_match('/:(00|30)$/', $date_start) || !preg_match('/:(00|30)$/', $date_end)) {
                messageAJAX('error', __('00 veya 30 Dakika olmalıdır.'));
            }

            $date_start = dateConvert2($date_start, 'server', $client_date_fromat);
            $date_end = dateConvert2($date_end, 'server', $client_date_fromat);

            if (strtotime($date_start) >= strtotime($date_end)) {
                messageAJAX('error', __('Bitiş zamanı başlangıçtan büyük veya eşit olamaz.'));
            }

            $data = [
                'user_id' => $user_id,
                'location_id' => $location_id,
                'start_date' => $date_start,
                'end_date' => $date_end,
                'created' => _date(),
                'active' => 1,

            ];

            $this->db->insert('shift_schedule', $data);

            messageAJAX('success', __('Vardiya oluşturuldu'));
        }

        // Handle form
        $user = $this->input->get('user');
        $location = $this->input->get('location');

        $user = $this->db->where('id', $user)->get('users', 1)->row();

        $date_start = $this->input->get('date');
        if (!isValidDate($date_start, 'Y-m-d H:i:s')) {
            $date_start = date('Y-m-d H:i');
        } else {
            $date_start = dateReFormat($date_start, 'Y-m-d H:i:s', 'Y-m-d H:i');
        }
        $date_end = date('Y-m-d H:i', strtotime('+1 hours', strtotime($date_start)));

        exit($this->load->view('Shiftschedule/add', compact('date_start', 'date_end', 'user', 'location'), true));
    }

    public function edit($id = null)
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        if(!$schedule = $this->exists('shift_schedule', "id = $id", '*')) {
            show_404();
        }


        if ($this->input->post()) {
            
            $client_date_fromat = dateFormat();

            $this->load->library('form_validation');
            $this->form_validation->set_rules('date_start', 'Başlangıç Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
            $this->form_validation->set_rules('date_end', 'Bitiş Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
            $this->form_validation->set_rules('user', 'Personel', 'required|trim|is_natural_no_zero');
            $this->form_validation->set_rules('location', 'Restoran', 'required|trim|is_natural_no_zero', ['is_natural_no_zero' => 'Restoran seçin']);

            if (!$this->form_validation->run()) {
                messageAJAX('error', validation_errors());
            }
            
            $date_start = $this->input->post('date_start');
            $date_end = $this->input->post('date_end');
            $user_id = $this->input->post('user');
            $location_id = $this->input->post('location');

            if (!preg_match('/:(00|30)$/', $date_start) || !preg_match('/:(00|30)$/', $date_end)) {
                messageAJAX('error', __('Dakikalar 30 veya 00 olmalıdır.'));
            }

            $date_start = dateConvert2($date_start, 'server', $client_date_fromat);
            $date_end = dateConvert2($date_end, 'server', $client_date_fromat);

            $data = [
                'user_id' => $user_id,
                'location_id' => $location_id,
                'start_date' => $date_start,
                'end_date' => $date_end,
                'active' => 1,

            ];

            $this->db->where('id', $id);
            $this->db->update('shift_schedule', $data);

            messageAJAX('success', __('Vardiya güncellendi'));
        }


        // // Handle form
        $user = $this->input->get('user');
        $location = $this->input->get('location');

        $user = $this->db->where('id', $user)->get('users', 1)->row();

        exit($this->load->view('Shiftschedule/edit', compact('schedule', 'user', 'location'), true));
    }

    function delete($id = NULL)
    {
        if(!$this->exists('shift_schedule', "id = $id", '*')) {
            show_404();
        }

        $this->db->where('id', $id)->update('shift_schedule', ['active' => 3]);

        messageAJAX('success', __('Vardiya silindi'));
    }

    public function report()
    {
        $table = '
        <tr>
            <td colspan="2" class="text-center">
                Sonuç Bulunamadı
            </td>
        </tr>';

        $client_date_fromat = dateFormat1();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('date_start', 'Başlangıç Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
        $this->form_validation->set_rules('date_end', 'Bitiş Tarihi', 'required|trim|is_date['.$client_date_fromat.']');
        $this->form_validation->set_rules('user[]', 'Personel', 'required|trim|is_natural_no_zero');
        $this->form_validation->set_rules('location', 'Restoran', 'required|trim|is_natural_no_zero', ['is_natural_no_zero' => 'Restoran seçin']);

        if (!$this->form_validation->run()) {
            messageAJAX('error', validation_errors());
        }

        $location = $this->input->post('location');

        $date = dateReFormat($this->input->post('date_start'), $client_date_fromat, 'Y-m-d 00:00:00');
        $start = dateConvert2($date, 'server', 'Y-m-d H:i:s', 'Y-m-d H:i:s');

        $date = dateReFormat($this->input->post('date_end'), $client_date_fromat, 'Y-m-d 00:00:00');
        $date = date('Y-m-d H:i:s', strtotime('+24 hours', strtotime($date)));
        $end = dateConvert2($date, 'server', 'Y-m-d H:i:s', 'Y-m-d H:i:s');

        $users = $this->db
            ->where_in('id', $this->input->post('user[]'))
            ->get('users')->result();


        if ($users) {
            
            $schedules = $this->db
                ->select('SUM(UNIX_TIMESTAMP(end_date) - UNIX_TIMESTAMP(start_date)) as work_time, user_id')
                ->where([
                    'active' => 1,
                    'start_date >=' => $start,
                    'end_date <=' => $end,
                    'location_id' => $location
                ])
                ->where_in('user_id', $this->input->post('user[]'))
                ->group_by('user_id')
                ->get('shift_schedule')->result();


            $table = null;
            foreach ($users as $user) {
                
                $table.= '<tr>';

                if (!$schedules) {
                    $table.= '<td>';
                    $table.= $user->name;
                    $table.= '</td>';
                    $table.= '<td>';
                    $table.= '-';
                    $table.= '</td>';
                } else {

                    $has_user_name = false;
                    foreach ($schedules as $schedule) {
                        if ($user->id == $schedule->user_id) {
                            $has_user_name = true;
                            $table.= '<td>';
                            $table.= $user->name;
                            $table.= '</td>';
                            $table.= '<td>';
                            $table.= second2String($schedule->work_time);
                            $table.= '</td>';
                        }
                    }
                    if (!$has_user_name) {
                        $table.= '<td>';
                        $table.= $user->name;
                        $table.= '</td>';
                        $table.= '<td>';
                        $table.= '-';
                        $table.= '</td>';
                    }
                }

                $table.= '</tr>';
            }
        }




        messageAJAX('success', null, $table);
    }
}
?>