<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Zones extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Zones_M');
        $this->load->model('Locations_m');
        $this->load->model('Floors_M');
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index($location_id = null) 
    {

        if ($location_id) {
            $this->db->where('location_id', $location_id);
        }

        $zones = $this->Zones_M
        ->fields(['*','(SELECT title FROM locations WHERE id = location_id) as location_title','(SELECT title FROM floors WHERE floors.id = zones.floor_id) as floor_title'])
        ->where('location_id', $this->user->locations)->get_all();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $this->load->template('Zones/index', compact('zones', 'location_id'));
    }

    public function add() 
    {
        if ($this->input->is_ajax_request()) {
            $users_zones = $this->handleZonesUser();

            if ($id = $this->Zones_M->from_form()->insert()) {
                foreach ($users_zones as $user_zone) {
                    $user_zone->zone_id = $id;
                    $this->db->insert('users_zones', $user_zone);
                }
                messageAJAX('success', __('Bölge başarıyla oluşturuldu.'),[
                    'id' => $id
                ]);
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $this->theme_plugin = [
            'js'    => ['panel/js/view/zones.js'],
            'start' => 'Zones.init();'
        ];

        $this->load->template('Zones/add');      

    }

    public function edit($id = null)
    {

        $zone = $this->Zones_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            $users_zones = $this->handleZonesUser();

            if ($this->Zones_M->from_form(null, null, ['id' => $id])->update()) {


                $this->db->delete('users_zones', ['zone_id' => $id]);

                foreach ($users_zones as $user_zone) {
                    $user_zone->zone_id = $id;
                    $this->db->insert('users_zones', $user_zone);
                }

                messageAJAX('success', __('Bölge başarıyla güncellendi.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $floors = $this->Floors_M->get_all([
            'location_id' => $zone->location_id
        ]);

        $users_zone = $this->Zones_M->findUsersByZone($zone->id);
        $users = $this->Locations_m->findLocationUsers($zone->location_id);

        $this->theme_plugin = [
            'js'    => ['panel/js/view/zones.js'],
            'start' => 'Zones.init();'
        ];

        $this->load->template('Zones/edit', compact('zone', 'floors', 'users', 'users_zone'));      
    }

    public function delete($id = NULL)
    {

        $this->Zones_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Zones_M->update(['active' => 3], ['id' => $id]);

        messageAJAX('success', __('Bölge başarıyla silindi.'));

    }

    private function getZonesByFloorId()
    {
        $floor_id = $this->input->post('floor_id');

        $zones = $this->Zones_M->order_by('title')->get_all([
            'floor_id' => $floor_id
        ]);

        messageAJAX('success', 'Success', compact('zones'), 'json');
    }

    private function getUsersByLocation()
    {
        $location_id = $this->input->post('location_id');
        $users = $this->Locations_m->findLocationUsers($location_id);
        messageAJAX('success', null, compact('users'));
    }

    private function handleZonesUser() 
    {
        $users = (array) $this->input->post('users[id]');
        $location = $this->input->post('location_id');

        $system_users = $this->Locations_m->findLocationUsers($location);

        $user_data = [];

        foreach ($users as $user_id) {

            foreach ($system_users as $system_user) {

                if($system_user->id == $user_id) {

                    $user_data[$user_id] = (object)[
                        'user_id' => $user_id,
                        'zone_id' => null 
                    ];
                    unset($system_user);
                    break;
                }
            }
        }
        return $user_data;

    }

    public function getZonesByLocationId()
    {
        $location_id = $this->input->get('location_id');

        $zones = $this->Zones_M
        ->fields('*')
        ->select('(SELECT COUNT(id) FROM points WHERE points.zone_id = zones.id) AS zone_count')
        ->select('(SELECT title FROM floors WHERE floors.id = zones.floor_id) AS floor_title')
        ->where('location_id',$location_id)
        ->get_all();

        messageAJAX('success', null, compact('zones'));
    }

    public function getZoneById()
    {
        $id = $this->input->get('id');
        
        $zone = $this->Zones_M
        ->fields('*')
        ->select('(SELECT COUNT(id) FROM points WHERE points.zone_id = zones.id) AS zone_count')
        ->select('(SELECT title FROM floors WHERE floors.id = zones.floor_id) AS floor_title')
        ->where('id',$id)->get();
        
        messageAJAX('success', 'Success', compact('zone'));
    }
}
?>