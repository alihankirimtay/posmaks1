<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Expenses extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);
        $this->load->model('Expenses_model', 'Expenses');
        $this->load->model('Stocks_model', 'Stocks');
        $this->load->model('Stockcontents_model', 'StockContents');
        $this->load->model('ExpenseTypes_model');
        $this->load->model('Employes_model');
        $this->load->model('Transactions_M');

        $this->load->library('Transaction_helper', null, 'TransactionHelper');

    }

    public function ajax($event= null)
    {
        if(method_exists(__CLASS__, (String) $event)) {
            $this->{$event}();
        } else {
            messageAJAX('error', 'Invalid request!');
        }
    }

    function index($location = NULL)
    {
       

        $this->db->select('*, (SELECT title FROM locations WHERE id = location_id LIMIT 1) AS location');
        $this->db->select('(SELECT title FROM expense_types WHERE id = expense_type_id LIMIT 1) AS type');
        $this->db->select('(SELECT username FROM users WHERE id = user_id LIMIT 1) AS user');
        $this->db->select('(SELECT title FROM payment_types WHERE id = payment_type_id LIMIT 1) AS payment_method');

        // TYPE
        if($t = (int) isGet('t')) {
            $this->db->where('expense_type_id', $t);
        }
        // LOCATION
        if($l = (int) isGet('l')) {
            $this->db->where('location_id', $l);
        }
        // OPERATOR
        if($o = (int) isGet('o')) {
            $this->db->where('user_id', $o);
        }

        $client_day_number = dateConvert2(date('Y-m-d H:i:s'), 'client', 'Y-m-d H:i:s', 'd');
        $start_of_day      = dateConvert2(date('Y-m-'.$client_day_number.' 00:00:00'), 'server', 'Y-m-d H:i:s');
        $end_of_day        = dateConvert2(date('Y-m-'.$client_day_number.' 23:59:00'), 'server', 'Y-m-d H:i:s');

        // DATE
        if($d = sanitize(isGet('d'))) {
            if($d == 'today'){
                $this->db->where('payment_date >=', $start_of_day);
            } else if($d == 'last365days'){
                $this->db->where('payment_date >= DATE_SUB("'.$start_of_day.'", INTERVAL 365 DAY)');
            } else if($d == 'yesterday'){
                $this->db->where('payment_date >= DATE_SUB("'.$start_of_day.'", INTERVAL 1 DAY)');
                $this->db->where('payment_date <= DATE_SUB("'.$end_of_day.'", INTERVAL 1 DAY)');
            } else if($d == 'thisweek'){
                $this->db->where('YEARWEEK(payment_date)', 'YEARWEEK("'.$start_of_day.'")');
            } else if($d == 'thismonth'){
                $this->db->where('YEAR(payment_date) = YEAR(CURDATE()) AND MONTH(payment_date) = MONTH("'.$start_of_day.'")');
            } else if($d == 'thisyear'){
                $this->db->where('YEAR(payment_date) = YEAR("'.$start_of_day.'")');
            } else if($d == 'lastweek'){
                $this->db->where('YEARWEEK(payment_date) = YEARWEEK("'.$start_of_day.'" - INTERVAL 7 DAY)');
            } else if($d == 'lastmonth'){
                $this->db->where('YEAR(payment_date) = YEAR("'.$start_of_day.'" - INTERVAL 1 MONTH) AND MONTH(payment_date) = MONTH("'.$start_of_day.'" - INTERVAL 1 MONTH)');
            } else if($d == 'lastyear'){
                $this->db->where('YEAR(payment_date) = YEAR("'.$start_of_day.'" - INTERVAL 1 YEAR)');
            } else if($d == 'custome'){
                if ( isValidDate(@$_GET['dFrom'], dateFormat()) ) {
                    $this->db->where('payment_date >=', dateConvert($this->input->get('dFrom', TRUE), 'server'));
                }
                if ( isValidDate(@$_GET['dTo'], dateFormat()) ) {
                    $this->db->where('payment_date <=', dateConvert($this->input->get('dTo', TRUE), 'server'));
                }
            } else {
                $this->db->where('payment_date >=', $start_of_day);
            }
        } else {
            $this->db->where('payment_date >=', $start_of_day);
        }
        // PAYMENT METHOD
        // if($t = (int) isGet('pm')) {
        //     $this->db->where('payment_type_id', $t);
        // }

        $data['current_location'] = (int) $location;
        $data['expenses'] = $this->Expenses->getAll();


        $this->db->where('active',1)->order_by('title', 'ASC');
        $data['expense_types'] = $this->ExpenseTypes_model->index();

        
        $data['employes'] = $this->Employes_model->get();

        $this->db->where_in('id', [1,2]); // CASH and CREDIT CARD
        $data['payment_methods'] = $this->db->get('payment_types')->result_array();

        foreach ($data['expenses'] as $expenses) {
            $expenses->amount = $expenses->amount + $expenses->amount * $expenses->tax / 100;
        }

        $selected_user = (int) isGet('o');

        $this->theme_plugin['js']    = array('panel/js/view/expenses.js',
                                            'globals/plugins/jquery.print/print.js');
        $this->theme_plugin['start'] = "TablesDataTables.init();
                                        MyFunction.datetimepicker('".dateFormat(TRUE)."');
                                        Expenses.init({
                                            selected_user: {$selected_user}
                                        });";


    	$this->load->template('Expenses/index', $data);
    }

    public function add()
    {
        if ($this->input->is_ajax_request()) {

            $_POST['user_id'] = $this->user->id;
            $_POST['has_stock'] = 0;
            $_POST['active'] = 1;

            $paidType = $this->input->post('paid-type');

            if ($paidType == "paid") {

                $_POST['increment'] = 0;
                $_POST['is_paid'] = 1;
                $_POST['payment_date'] = $this->input->post('paid_date');
                $_POST['date'] = $this->input->post('paid_date');

                $this->form_validation->set_rules($this->Transactions_M->setType($this->input->post('account-type'))->rules['insert']);
                if(!$this->form_validation->run()) {
                    messageAJAX('error', validation_errors());                
                }

            } else if($paidType == "payable") {
                $_POST['is_paid'] = 0;
                $_POST['payment_date'] = $this->input->post('payable_date');
            }

            if ($expense_id = $this->Expenses->insert()) {

                if ($paidType == "paid") {

                    $this->insertTransaction($expense_id);
                    
                }

                messageAJAX('success', __('Gider başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $expense_types = $this->db->where('active',1)->order_by('title')->get('expense_types')->result_array();
        $payment_methods = $this->db->where_in('id', [1,2])->where('id != 3')->get('payment_types')->result_array();
        $customers = $this->db
        ->where([
            'isSupplier' => 1,
            'active' => 1
         ])->get('customers')->result_array();
        $banks = $this->db->where('active', 1)->order_by('title', 'ASC')->get('banks')->result_array();
        $pos = $this->db->where('active', 1)->order_by('title', 'ASC')->get('pos')->result_array();

        $this->theme_plugin = [
            'js'    => ['panel/js/view/expenses.js'],
            'start' => 'eUploadAttachments.init(); MyFunction.datetimepicker("'.dateFormat(TRUE).'"); ExpenseWithTransaction.init();'
        ];
        $this->load->template('Expenses/add', compact('expense_types', 'payment_methods', 'customers', 'banks', 'pos'));
    }


    public function edit($id = null)
    {
        $expense = $this->Expenses->findOrFail($id);

        if ($this->input->is_ajax_request()) {

            $paidType = $this->input->post('paid-type');

            if ($expense->transaction_id) {
                $_POST['transaction_id'] = $expense->transaction_id;
                $_POST['is_paid'] = 1;
                $_POST['payment_date'] = dateReFormat($expense->payment_date, 'Y-m-d H:i:s', 'd-m-Y H:i');

            } else {

                if ($paidType == "paid") {

                    $_POST['increment'] = 0;
                    $_POST['is_paid'] = 1;
                    $_POST['payment_date'] = $this->input->post('paid_date');
                    $_POST['date'] = $this->input->post('paid_date');

                    $this->form_validation->set_rules($this->Transactions_M->setType($this->input->post('account-type'))->rules['insert']);
                    if(!$this->form_validation->run()) {
                        messageAJAX('error', validation_errors());                
                    }

                } else if($paidType == "payable") {
                    $_POST['is_paid'] = 0;

                    $_POST['payment_date'] = $this->input->post('payable_date');
                    
                }

            }

            $_POST['user_id'] = $this->user->id;
            $_POST['has_stock'] = 0;
            $_POST['active'] = 1;

            if ($this->Expenses->update(['id' => $id])) {

                $transaction_js = [];

                if ($paidType == "paid" && !$expense->transaction_id) {

                    $type= $this->input->post('account-type');
                    $bank_id= $this->input->post('bank[id]');
                    $pos_id = $this->input->post('pos[id]');
                    $amount = $this->input->post('amount');
                    $increment = 0;
                    $customer_id = $this->input->post('customer_id');
                    $payment_date = $_POST['payment_date'];
                    $desc = $this->input->post('desc');
                    $type_id = $this->input->post('type_id');

                    try {
                        $transaction_id = $this->TransactionHelper
                        ->setType($type)
                        ->setBankId($bank_id)
                        ->setPosId($pos_id)
                        ->setAmount($amount)
                        ->setIncrement($increment)
                        ->setCustomerId($customer_id)
                        ->setPaymentDate($payment_date)
                        ->setDesc($desc)
                        ->setTypeId($type_id)
                        ->processTransactionCustomer();

                        $this->db->set('transaction_id', $transaction_id)->where(['id' => $id])->update('expenses');

                        $transaction = $this->Transactions_M
                        ->fields('*')
                        ->select('(select title from banks where id = bank_id) As bank_title')
                        ->select('(select title from pos where id = pos_id) As pos_title')
                        ->where(['id' => $transaction_id])->get();
                        
                        $transaction_js = [
                            'onSuccess' => 'ExpenseWithTransaction.createTransactionDiv('.json_encode($transaction).')'
                        ];

                    } catch (Exception $error) {
                        messageAJAX('error', $error->getMessage());
                    }
                    
                }

                messageAJAX('success', __('Gider başarıyla güncellendi.'), $transaction_js);
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $expense_types = $this->db->where('active',1)->order_by('title')->get('expense_types')->result_array();
        $payment_methods = $this->db->where_in('id', [1,2])->where('id != 3')->get('payment_types')->result_array();
        $customers = $this->db
        ->where([
            'isSupplier' => 1,
            'active' => 1
         ])->get('customers')->result_array();
        $banks = $this->db->where('active', 1)->order_by('title', 'ASC')->get('banks')->result_array();
        $pos = $this->db->where('active', 1)->order_by('title', 'ASC')->get('pos')->result_array();
        $transaction = null;
        if ($expense->transaction_id) {
            $transaction = $this->Transactions_M
            ->fields('*')
            ->select('(select title from banks where id = bank_id) As bank_title')
            ->select('(select title from pos where id = pos_id) As pos_title')
            ->where(['id' => $expense->transaction_id])->get();
        }

        $this->theme_plugin = [
            'js'    => ['panel/js/view/expenses.js'],
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                        Expenses.init();
                        ExpenseWithTransaction.init();'
        ];

        $this->load->template('Expenses/edit', compact('expense', 'expense_types', 'payment_methods','customers', 'banks', 'pos', 'transaction'));
    }


    function view($id = NULL)
    {
        $id = (int) $id;

        $data = $this->Expenses_model->view($id);
    }

    function delete ($id = NULL )
    {
        $id = (int) $id;

        $expense = $this->Expenses->findOrFail([
            'id' => $id,   
            'has_stock' => 0
        ]);

        $this->Expenses->delete([
            'id' => $expense->id
        ]);

        messageAJAX('success', __('Gider başarıyla silindi.'));

    }


    public function addWithStock($supplier_id = null)
    {
        if ($this->input->is_ajax_request()) {

            $paidType = $this->input->post('paid-type');

            // if ($paidType == "paid") {

                $_POST['amount'] = 0.00;
                $_POST['increment'] = 0;
                $_POST['is_paid'] = 1;
                $_POST['payment_date'] = $this->input->post('paid_date');
                $_POST['date'] = $this->input->post('paid_date');

                $this->form_validation->set_rules($this->Transactions_M->setType($this->input->post('account-type'))->rules['insert']);
                if(!$this->form_validation->run()) {
                    messageAJAX('error', validation_errors());                
                }

            // } else if($paidType == "payable") {
            //     $_POST['is_paid'] = 0;
            //     $_POST['payment_date'] = $this->input->post('payable_date');
            // }


            $stocks = $this->Expenses->handleStocks();

            $_POST['user_id'] = $this->user->id;
            $_POST['active'] = 1;
            $_POST['has_stock'] = 1;
            $_POST['tax'] = 0;
            $_POST['amount'] = $stocks['expense']['amount'];

            
            if ($id = $this->Expenses->insert()) {

                // if ($paidType == "paid") {

                    $type= $this->input->post('account-type');
                    $bank_id= $this->input->post('bank[id]');
                    $pos_id = $this->input->post('pos[id]');
                    $amount = $stocks['expense']['amount'];
                    $increment = 0;
                    $customer_id = $this->input->post('customer_id');
                    $payment_date = $_POST['payment_date'];
                    $desc = $this->input->post('desc');
                    $type_id = $this->input->post('type_id');
                    $isExpense = true;

                    try {
                        $transaction_id = $this->TransactionHelper
                        ->setType($type)
                        ->setBankId($bank_id)
                        ->setPosId($pos_id)
                        ->setAmount($amount)
                        ->setIncrement($increment)
                        ->setCustomerId($customer_id)
                        ->setPaymentDate($payment_date)
                        ->setDesc($desc)
                        ->setTypeId($type_id)
                        ->processTransactionCustomer($isExpense);

                        $this->db->set('transaction_id', $transaction_id)->where(['id' => $id])->update('expenses');

                    } catch (Exception $error) {
                        messageAJAX('error', $error->getMessage());
                    }
                    
                // }

                foreach ($stocks['stocks']['insert'] as $key => $stock) {
                    $this->form_validation->reset_validation();
                    $stock_id = $this->Stocks->insert($stock);

                    $stock_content = $stocks['stock_contents']['insert'][$key];
                    $stock_content['stock_id'] = $stock_id;
                    $stock_content['expense_id'] = $id;

                    $this->form_validation->reset_validation();
                    $this->StockContents->insert($stock_content);
                }
                $this->db->reset_query();

                foreach ($stocks['stocks']['update'] as $key => $stock) {
                    $this->form_validation->reset_validation();
                    $this->db->set('quantity', "quantity + {$stock['quantity']}", false)->where([
                        'id' => $stock['id']
                    ])->update('stocks');

                    $stock_content = $stocks['stock_contents']['insert'][$key];
                    $stock_content['expense_id'] = $id;

                    $this->form_validation->reset_validation();
                    $this->StockContents->insert($stock_content);
                }

                messageAJAX('success', __('Gider başarıyla oluşturuldu.'));
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $expense_types = $this->db->where('active',1)->order_by('title')->get('expense_types')->result_array();
        $payment_methods = $this->db->where_in('id', [1,2])->where('id != 3')->get('payment_types')->result_array();
        $template = $this->load->view('Expenses/elements/stock_row', ['stock_number' => '{stock_number}'], true);
        $customers = $this->db
        ->where([
            'isSupplier' => 1,
            'active !=' => 3,
            'deleted' => null
         ])->get('customers')->result_array();
        $banks = $this->db->where('active', 1)->order_by('title', 'ASC')->get('banks')->result_array();
        $pos = $this->db->where('active', 1)->order_by('title', 'ASC')->get('pos')->result_array();

        $this->theme_plugin = [
            'css' => ['globals/plugins/select2/css/select2.min.css'],
            'js' => [
                'globals/plugins/select2/js/select2.full.min.js',
                'globals/plugins/select2/js/i18n/tr.js',
                'panel/js/view/expenses.js',
            ],
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                        ExpenseWithStock.init({
                            rowTemplate: "'.escapeJavaScriptText($template).'",
                            addFirstRow: true,
                            method: "add",
                        });
                        ExpenseWithTransaction.init();'
        ];
        $this->load->template('Expenses/add_with_stock', compact('expense_types', 'payment_methods', 'customers', 'banks', 'pos', 'supplier_id'));
    }

    public function editWithStock($id = null)
    {
        $expense = $this->Expenses->findOrFail([
            'has_stock' => 1,
            'id' => $id,
        ]);

        if ($this->input->is_ajax_request()) {

            if ($expense->transaction_id) {
                $_POST['transaction_id'] = $expense->transaction_id;
                $_POST['is_paid'] = 1;
                $_POST['payment_date'] = dateReFormat($expense->payment_date, 'Y-m-d H:i:s', 'd-m-Y H:i');

            } else {

                $paidType = $this->input->post('paid-type');

                if ($paidType == "paid") {

                    $_POST['amount'] = $expense->amount;
                    $_POST['increment'] = 0;
                    $_POST['is_paid'] = 1;
                    $_POST['payment_date'] = $this->input->post('paid_date');
                    $_POST['date'] = $this->input->post('paid_date');

                    $this->form_validation->set_rules($this->Transactions_M->setType($this->input->post('account-type'))->rules['insert']);
                    if(!$this->form_validation->run()) {
                        messageAJAX('error', validation_errors());                
                    }

                } else if($paidType == "payable") {
                    $_POST['is_paid'] = 0;

                    $_POST['payment_date'] = $this->input->post('payable_date');
                    
                }

            }

            $stocks = $this->Expenses->handleStocks();

            $_POST['user_id'] = $this->user->id;
            $_POST['active'] = $expense->active;
            $_POST['has_stock'] = 1;
            $_POST['tax'] = 0;
            $_POST['amount'] = $stocks['expense']['amount'];

            if ($this->Expenses->update(['id' => $id])) {

                $transaction_js = [];
                
                if (isset($paidType) && $paidType == "paid" && !$expense->transaction_id) {

                    $type= $this->input->post('account-type');
                    $bank_id= $this->input->post('bank[id]');
                    $pos_id = $this->input->post('pos[id]');
                    $amount = $stocks['expense']['amount'];
                    $increment = 0;
                    $customer_id = $this->input->post('customer_id');
                    $payment_date = $_POST['payment_date'];
                    $desc = $this->input->post('desc');
                    $type_id = $this->input->post('type_id');

                    try {
                        $transaction_id = $this->TransactionHelper
                        ->setType($type)
                        ->setBankId($bank_id)
                        ->setPosId($pos_id)
                        ->setAmount($amount)
                        ->setIncrement($increment)
                        ->setCustomerId($customer_id)
                        ->setPaymentDate($payment_date)
                        ->setDesc($desc)
                        ->setTypeId($type_id)
                        ->processTransactionCustomer();

                        $this->db->set('transaction_id', $transaction_id)->where(['id' => $id])->update('expenses');

                        $transaction = $this->Transactions_M
                        ->fields('*')
                        ->select('(select title from banks where id = bank_id) As bank_title')
                        ->select('(select title from pos where id = pos_id) As pos_title')
                        ->where(['id' => $transaction_id])->get();
                        
                        $transaction_js = [
                            'onSuccess' => 'ExpenseWithTransaction.createTransactionDiv('.json_encode($transaction).')'
                        ];

                    } catch (Exception $error) {
                        messageAJAX('error', $error->getMessage());
                    }
                    
                }

                foreach ($stocks['stocks']['insert'] as $key => $stock) {
                    $stock_id = $this->Stocks->insert($stock);

                    $stock_content = $stocks['stock_contents']['insert'][$key];
                    $stock_content['stock_id'] = $stock_id;
                    $stock_content['expense_id'] = $id;

                    $this->StockContents->insert($stock_content);
                }

                foreach ($stocks['stocks']['update'] as $key => $stock) {
                    $this->db->set('quantity', "quantity + {$stock['quantity']}", false)->where([
                        'id' => $stock['id']
                    ])->update('stocks');

                    $stock_content = $stocks['stock_contents']['insert'][$key];
                    $stock_content['expense_id'] = $id;

                    $this->StockContents->insert($stock_content);
                }

                foreach ($stocks['stocks']['delete'] as $key => $stock) {
                    $this->db->set('quantity', "quantity - {$stock['quantity']}", false)->where([
                        'id' => $stock['id']
                    ])->update('stocks');

                    $stock_content = $stocks['stock_contents']['delete'][$key];
                    $stock_content['expense_id'] = $id;

                    $this->StockContents->delete($stock_content);
                }

                messageAJAX('success', __('Gider başarıyla güncellendi.'), $transaction_js);
            } else {
                messageAJAX('error', validation_errors());
            }
        }

        $stocks = $this->Expenses->findStocksWithContents(['expense_id' => $expense->id]);
        $expense_types = $this->db->where('active',1)->order_by('title')->get('expense_types')->result_array();
        $payment_methods = $this->db->where_in('id', [1,2])->where('id != 3')->get('payment_types')->result_array();
        $customers = $this->db
        ->where([
            'isSupplier' => 1,
            'active !=' => 3,
            'deleted' => null
         ])->get('customers')->result_array();
        $template = $this->load->view('Expenses/elements/stock_row', ['stock_number' => '{stock_number}'], true);
        $banks = $this->db->where('active', 1)->order_by('title', 'ASC')->get('banks')->result_array();
        $pos = $this->db->where('active', 1)->order_by('title', 'ASC')->get('pos')->result_array();
        $transaction = null;
        if ($expense->transaction_id) {
            $transaction = $this->Transactions_M
            ->fields('*')
            ->select('(select title from banks where id = bank_id) As bank_title')
            ->select('(select title from pos where id = pos_id) As pos_title')
            ->where(['id' => $expense->transaction_id])->get();
        }

        $this->theme_plugin = [
            'css' => ['globals/plugins/select2/css/select2.min.css'],
            'js' => [
                'globals/plugins/select2/js/select2.full.min.js',
                'globals/plugins/select2/js/i18n/tr.js',
                'panel/js/view/expenses.js',
            ],
            'start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");
                        ExpenseWithStock.init({
                            rowTemplate: "'.escapeJavaScriptText($template).'",
                            addFirstRow: false,
                            number: '.count($stocks).',
                            triggerTheCalculating: true,
                            method: "edit",
                        });
                        ExpenseWithTransaction.init();'
                    
        ];

        $this->load->template('Expenses/edit_with_stock', compact('stocks', 'expense', 'expense_types', 'payment_methods', 'customers', 'transaction','banks','pos'));
    }

    public function deleteTransaction()
    {
        $id = $this->input->post('transaction_id');
        $customer_id = $this->input->post('customer_id');
        $expense_id = $this->input->post('expense_id');

        $transaction = $this->Transactions_M->findOrFail($id);

        if ($transaction->pos_id) {
            $type = "pos";
        } else if ($transaction->bank_id) {
            $type = "bank";
        }

        $datetime = date("Y-m-d H:i:s");
        $todatetime = dateReFormat($datetime, 'Y-m-d H:i:s', 'd-m-Y H:i');

        $_POST['date'] = $todatetime;

        $bank_id= $transaction->bank_id;
        $pos_id = $transaction->pos_id;
        $amount = $transaction->amount;
        $increment = 1;
        $customer_id = $customer_id;
        $payment_date = $_POST['date'];
        $desc = "İptal edildi";
        $type_id = $transaction->type_id;

        try {
            $this->TransactionHelper
            ->setType($type)
            ->setBankId($bank_id)
            ->setPosId($pos_id)
            ->setAmount($amount)
            ->setIncrement($increment)
            ->setCustomerId($customer_id)
            ->setPaymentDate($payment_date)
            ->setDesc($desc)
            ->setTypeId($type_id)
            ->processTransactionCustomer();

        } catch (Exception $error) {
            messageAJAX('error', $error->getMessage());
        }

        $this->db->set([
            'transaction_id'  => null,
            'payment_date'    => null
        ])->where(['id' => $expense_id])->update('expenses');

        messageAJAX('success', __('Fiş/Fatura ödemesi silindi.'));


    }

    public function insertTransaction ($expense_id) {
        $type= $this->input->post('account-type');
        $bank_id= $this->input->post('bank[id]');
        $pos_id = $this->input->post('pos[id]');
        $amount = $this->input->post('amount');
        $increment = 0;
        $customer_id = $this->input->post('customer_id');
        $payment_date = $_POST['payment_date'];
        $desc = $this->input->post('desc');
        $type_id = $this->input->post('type_id');

        try {
            $transaction_id = $this->TransactionHelper
            ->setType($type)
            ->setBankId($bank_id)
            ->setPosId($pos_id)
            ->setAmount($amount)
            ->setIncrement($increment)
            ->setCustomerId($customer_id)
            ->setPaymentDate($payment_date)
            ->setDesc($desc)
            ->setTypeId($type_id)
            ->processTransactionCustomer();

            $this->db->set('transaction_id', $transaction_id)->where(['id' => $expense_id])->update('expenses');

        } catch (Exception $error) {
            messageAJAX('error', $error->getMessage());
        }

    }
}
?>
