<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Yemeksepetisync extends Auth_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Products_M');
        $this->load->model('Ysproducts_M');
        $this->load->model('Locations_m');
        $this->check_auth([
            'allowed' => ['ajax']
        ]);  		

        $this->load->library('Yemeksepeti', null, 'YemekSepeti');


    }

    public function ajax($event = null)
	{
		if(method_exists(__CLASS__, (String) $event)) {
			$this->{$event}();
		} else {
			messageAJAX('error', 'Invalid request!');
		}
	}


	public function index($location_id = NULL)
	{	
		if (!$location_id) {
            $location_id = $this->user->locations[0];
        }

		$this->theme_plugin = array(
            'js'    => [ 'panel/js/view/ysproducts.js' ],
            'start' => "Ysproducts.init({$location_id});"
        );
	
    	$this->load->template('Yemeksepetisync/index', compact('location_id'));

	}

	public function getYsMenu()
	{

		$location_id = $this->input->post('location_id');

        if ($this->YemekSepeti->initByLocationId($location_id)) {

			$menus = $this->YemekSepeti->GetMenu();

			$products = $menus['items']->Products;
			$options = $menus['items']->Options;


			$system_products = $this->Products_M->fields(['*','(SELECT id FROM ys_products WHERE product_id = products.id LIMIT 1 ) AS ys_products_id' ])->get_all([
	            'location_id' => $location_id
	        ]);

			messageAJAX('success','', compact('products', 'system_products', 'options'));

		}

	}

	public function getProductWithExtras()
	{

		$location_id = $this->input->post('location_id');
        $product_id = $this->input->post('product_id');
        $includeYemekSepeti = true;

        $this->db->where_in($product_id, ['id','portion_id'], false);
        $system_products = $this->Products_M->findProductsWithRelatedItems([
            'products' => [
                'location_id' => $location_id,
                'active !=' => 3,
            ],
            'stocks' => [
                'stocks.location_id' => $location_id,
                'stocks.active' => 1,
            ]
        ], $includeYemekSepeti);

		messageAJAX('success','', compact('system_products'));


	}

	public function update($location_id = null)
	{

		if ($this->input->is_ajax_request()) {

			$ys_products = $this->input->post('products');

        	$products = $this->handleYsProducts($ys_products, $location_id);


        	if (!$products) {
        		messageAJAX('error', __('Ürün bulunamadı.'));
        	}

        	$this->db->where_in('product_id', array_column($products, 'product_id'));
            $this->db->delete('ys_products');

            $this->Ysproducts_M->insert($products);

            messageAJAX('success', __('Yemek sepeti ürün eşleştirme tamamlandı'));


		}

	}

	private function handleYsProducts($ys_products, $location_id)
	{
		$data = [];
		$includeYemekSepeti = true;

		$ys_products = $this->filterProductPost($ys_products);

		if (!$ys_products) {
			return $data;
		}

		$product_ids = array_map(function($row){
        	return $row['product'];
        }, $ys_products);

		$this->db->where_in('id', $product_ids);
		$system_products = $this->Products_M->findProductsWithRelatedItems([
            'products' => [
                'location_id' => $location_id,
                'active !=' => 3
            ],
            'stocks' => [
                'stocks.location_id' => $location_id,
                'active !=' => 3
            ]
	    ], $includeYemekSepeti);

	    if (!$system_products) {
        	return $data;
        }

		foreach ($ys_products as $ys_product_id => $item) {

			if (!in_array($item['product'], $product_ids)) {
				continue;
			}

			$product_id = $item['product'];

			$data[] = [
				'id' => $ys_product_id, 
				'product_id' => $product_id,
				'portion_id' => null,
				'add_product_id' => null,
				'stock_id' => null,
				'location_id' => $location_id
			];

			if (!empty($item['stocks'])) {

				foreach ($item['stocks'] as $ys_stock_id => $stock_id) {

					$stocks = $system_products[$product_id]->stocks;

					$stock_exist = array_filter($stocks, function($row) use($stock_id) {
						return $row->id == $stock_id;
					});

					if ($stock_exist) {

						$data[] = [
							'id' => $ys_stock_id, 
							'product_id' => $product_id,
							'portion_id' => null,
							'add_product_id' => null,
							'stock_id' => $stock_id,
							'location_id' => $location_id
						];

					}
				} 

			}

			if (!empty($item['add_products'])) {

				foreach ($item['add_products'] as $ys_add_id => $add_id) {

					$additional_products = $system_products[$product_id]->additional_products;

					$additional_products_exist = array_filter($additional_products, function($row) use($add_id) {
						return $row->id == $add_id;
					});

					if ($additional_products_exist) {

						$data[] = [
							'id' => $ys_add_id, 
							'product_id' => $product_id,
							'portion_id' => null,
							'add_product_id' => $add_id,
							'stock_id' => null,
							'location_id' => $location_id
						];

					}
				} 
			}

			if (!empty($item['portions'])) {

				foreach ($item['portions'] as $ys_portion_id => $portion_id) {

					$data[] = [
						'id' => $ys_portion_id, 
						'product_id' => $system_products[$product_id]->id,
						'portion_id' => $portion_id,
						'add_product_id' => null,
						'stock_id' => null,
						'location_id' => $location_id
					];

				}
			}


		}

		return $data;

	}

	private function filterProductPost($ys_products)
	{
		$tmp_ys_products = [];

		foreach ($ys_products as $ys_product_id => $item) {

			if (strlen($ys_product_id) !== 32 || empty($item['product'])) {
				continue;
			}

			$product_id = (int) $item['product'];

			if (!$product_id) {
				continue;
			}

			$tmp_ys_products[$ys_product_id]['product'] = $product_id; 

			if (isset($item['stocks']) && is_array($item['stocks'])) {

				foreach ($item['stocks'] as $ys_stock_id => $stock_id) {

					$stock_id = (int) $stock_id;

					if (strlen($ys_stock_id) !== 32 || !$stock_id) {
						continue;
					}

					$tmp_ys_products[$ys_product_id]['stocks'][$ys_stock_id] = $stock_id; 
					
				}
			}

			if (isset($item['add_products']) && is_array($item['add_products'])) {

				foreach ($item['add_products'] as $ys_add_id => $add_product_id) {

					$add_product_id = (int) $add_product_id;

					if (strlen($ys_add_id) !== 32 || !$add_product_id) {
						continue;
					}

					$tmp_ys_products[$ys_product_id]['add_products'][$ys_add_id] = $add_product_id; 
					
				}
			}

			if (isset($item['portions']) && is_array($item['portions'])) {

				foreach ($item['portions'] as $ys_portion_id => $portion_id) {

					$portion_id = (int) $portion_id;

					if (strlen($ys_portion_id) !== 32 || !$portion_id) {
						continue;
					}

					$tmp_ys_products[$ys_product_id]['portions'][$ys_portion_id] = $portion_id; 
					
				}
			}

		}
		
		return $tmp_ys_products;

	}

}

/* End of file Yemeksepeti.php */
/* Location: ./application/controllers/Yemeksepeti.php */