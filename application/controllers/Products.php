<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Products_M');
        $this->load->model('Stocks_M');
        $this->load->model('Productions_M');
        $this->load->model('QuickNotes_M');
        $this->load->model('Producttypes_M', 'ProductTypes_M');
        $this->load->model('ProductsAdditionalProducts_M');
        $this->load->model('ProductsQuickNotes_M');
        $this->load->model('ProductsHasProducts_M');
        $this->load->model('ProductHasStocks_M');
    }

    public function ajax($event = null)
    {
        if(method_exists(__CLASS__, (String) $event) && $this->input->is_ajax_request()) {
            $this->{$event}();
        }

        messageAJAX('error', __('Hatalı istek [404]'));
    }

    public function index()
    {
        $location_id = $this->input->get('location_id');
        $category_id = (int) $this->input->get('category_id');

        if (!$location_id) {
            $location_id = $this->user->locations[0];
        }

        if ($category_id) {
            $this->db->where('product_type_id', $category_id);  
        }
        $products = $this->Products_M->get_all([
            'location_id' => $location_id,
        ]);

        $categories = $this->ProductTypes_M->get_all();

        $this->theme_plugin = [
            'js' => ['panel/js/view/products.js'],
            'start' => 'TablesDataTables.init(); Products.init();'];

    	$this->load->template('Products/index', compact('products', 'location_id', 'categories'));
    }

    public function add()
    {
        $products = $this->Products_M->where('location_id', $this->user->locations)->get_all(['active' => 1]);
        $quickNotes = $this->QuickNotes_M->where('active', 1)->get_all();

        if ($this->input->is_ajax_request()) {

            $product_stocks = $this->handleProductStocks();
            $package_products = $this->handlePackageProducts($products);
            $additional_products = $this->handleAdditionalProducts($products);
            $portion_products = $this->handlePortionProducts($products);
            $quick_notes = $this->handleQuickNotes($quickNotes);


            if ($id = $this->Products_M->from_form()->insert()) {

                foreach ($product_stocks as $product_stock) {
                    $product_stock['product_id'] = $id;
                    $this->ProductHasStocks_M->insert($product_stock);
                }

                foreach ($package_products as $package_product) {
                    $package_product['package_id'] = $id;
                    $this->ProductsHasProducts_M->insert($package_product);
                }

                foreach ($additional_products as $additional_produc) {
                    $additional_produc['product_id'] = $id;
                    $this->ProductsAdditionalProducts_M->insert($additional_produc);
                }

                foreach ($portion_products as $portion_product_id) {
                    $this->Products_M->where('id', $portion_product_id)->update([
                        'portion_id' => $id
                    ]);
                }

                foreach ($quick_notes as $quick_note) {
                    $quick_note['product_id'] = $id;
                    $this->ProductsQuickNotes_M->insert($quick_note);
                }

                messageAJAX('success', logs_($this->user->id, __('Ürün oluşturuldu.'), ['id' => $id]));
            }

            messageAJAX('error', validation_errors());
        }


        $stocks = $this->Stocks_M->where('location_id', $this->user->locations)->get_all();
        $product_types = $this->ProductTypes_M->where('active',1)->get_all(['parent_id !=' => null]);
        $productions = $this->Productions_M->get_all();


        $this->theme_plugin = [
            'css' => ['panel/css/wizard.css'],
            'js' => ['panel/js/view/products.js'],
            'start' => 'Products.init();',
        ];

        $this->load->template('Products/add', compact('products', 'stocks', 'productions', 'product_types', 'quickNotes'));
    }

    public function edit($id = null)
    {
        $product = $this->Products_M->findOrFail($id);
        $products = $this->Products_M->where('location_id', $this->user->locations)->get_all(['active' => 1]);
        $quickNotes = $this->QuickNotes_M->where('active', 1)->get_all();

        if ($this->input->is_ajax_request()) {

            $product_stocks = $this->handleProductStocks();
            $package_products = $this->handlePackageProducts($products);
            $additional_products = $this->handleAdditionalProducts($products);
            $portion_products = $this->handlePortionProducts($products);
            $quick_notes = $this->handleQuickNotes($quickNotes);

            if ($this->Products_M->from_form(null, null, ['id' => $id])->update()) {

                $this->ProductHasStocks_M->delete(['product_id' => $id]);
                $this->ProductsHasProducts_M->delete(['package_id' => $id]);
                $this->ProductsAdditionalProducts_M->delete(['product_id' => $id]);
                $this->Products_M->where('portion_id', $id)->update(['portion_id' => null]);
                $this->ProductsQuickNotes_M->delete(['product_id' => $id]);
                

                foreach ($product_stocks as $product_stock) {
                    $product_stock['product_id'] = $id;
                    $this->ProductHasStocks_M->insert($product_stock);
                }

                foreach ($package_products as $package_product) {
                    $package_product['package_id'] = $id;
                    $this->ProductsHasProducts_M->insert($package_product);
                }

                foreach ($additional_products as $additional_produc) {
                    $additional_produc['product_id'] = $id;
                    $this->ProductsAdditionalProducts_M->insert($additional_produc);
                }

                if ($portion_products) {
                    $this->Products_M->where('id', $portion_products)->update([
                        'portion_id' => $id
                    ]);
                }

                foreach ($quick_notes as $quick_note) {
                    $quick_note['product_id'] = $id;
                    $this->ProductsQuickNotes_M->insert($quick_note);
                }
                
                messageAJAX('success', logs_($this->user->id, __('Ürün güncellendi.'), ['id' => $id]));
            }

            messageAJAX('error', validation_errors());
        }

        $product = $this->Products_M->handleProductRelatedItems($product);
        $stocks = $this->Stocks_M->where('location_id', $this->user->locations)->get_all();
        $product_types = $this->ProductTypes_M->get_all(['parent_id !=' => null]);
        $productions = $this->Productions_M->get_all();


        $this->theme_plugin = [
            'css' => ['panel/css/wizard.css'],
            'js' => ['panel/js/view/products.js'],
            'start' => 'Products.init({
                editMode: true
            });',
        ];

        $this->load->template('Products/edit', compact('product', 'products', 'stocks', 'productions', 'product_types', 'quickNotes'));
    }

    public function delete($id = NULL)
    {
        $this->Products_M->findOrFail((int) $id);

        $this->Products_M->update(['active' => 3], $id);

        messageAJAX('success', 'Success');
    }

    public function productActive()
    {

        $id = $this->input->post('product_id');
        $active = $this->input->post('value');

        $this->Products_M->findOrFail((int) $id);

        $this->Products_M->update(['active' => $active], $id);

        messageAJAX('success', 'Success',null, 'json');

    }

    public function AllProductActiveOrPassive()
    {
        $ids = (array) $this->input->post('id[]');
        $status = $this->input->get('status');

        if ($ids) {

            $status = ($status == 'active') ? 1 : 0;

            $this->db->where_in('id', $ids)->update('products', ['active' => $status]);
        }

        messageAJAX('success', 'Success', null, 'json');
    }

    private function handleProductStocks()
    {
        $stocks = (Array) $this->input->post('stocks');
        $location_id = (int) $this->input->post('location_id');
        $type = $this->input->post('type');

        if ($type == 'package') {
            return [];
        } elseif ($type == 'product' && !$stocks) {
            messageAJAX('error', __('Stok içeriği seçmelisiniz.'));
        }

        $data = [];
        foreach ($stocks as $stock_id => $stock) {

            if (!isset($stock['quantity'])) {
                messageAJAX('error', __('Stok miktarı girmelisiniz.'));
            }
            
            $this->Stocks_M->findOrFail([
                'id' => $stock_id,
                'location_id' => $location_id,
            ], 'Geçersiz stok.');

            $quantity = doubleval(str_replace(',', '', $stock['quantity']));
            $optional = (int) isset($stock['optional']);

            $data[$stock_id] = [
                'stock_id' => $stock_id,
                'product_id' => null,
                'quantity' => $quantity,
                'optional' => $optional
            ];
        }

        return $data;
    }

    private function handlePackageProducts($location_products)
    {
        $products = (Array) $this->input->post('products');
        $type = $this->input->post('type');

        if ($type == 'product') {
            return [];
        } elseif ($type == 'package' && !$products) {
            messageAJAX('error', __('Menü içeriği seçmelisiniz.'));
        }

        $data = [];
        foreach ($products as $product_id) {
            
            foreach ($location_products as $location_product) {
                if ($location_product->id == $product_id) {
                    $data[$product_id] = [
                        'package_id' => null,
                        'product_id' => $product_id,
                    ];
                }
            }
        }

        return $data;
    }

    private function handleAdditionalProducts($location_products)
    {
        $additional_products = (Array) $this->input->post('additional_products');
        $location_id = (int) $this->input->post('location_id');

        $data = [];
        foreach ($additional_products as $additional_product_id) {

            foreach ($location_products as $location_product) {
                if ($location_product->id == $additional_product_id) {
                    $data[$additional_product_id] = [
                        'product_id' => null,
                        'additional_product_id' => $additional_product_id,
                    ];
                }
            }
        }

        return $data;
    }

    private function handlePortionProducts($location_products)
    {
        $portion_products = (Array) $this->input->post('portion_products');
        $location_id = (int) $this->input->post('location_id');

        $data = [];
        foreach ($portion_products as $portion_product_id) {

            foreach ($location_products as $location_product) {
                if ($location_product->id == $portion_product_id) {
                    $data[$portion_product_id] = $portion_product_id;
                }
            }
        }

        return $data;
    }

    private function getProductsByLocationId()
    {
        $location_id = $this->input->post('location_id');

        $products = $this->Products_M->order_by('title')->get_all([
            'location_id' => $location_id,
            'portion_id' => null,
        ]);

        messageAJAX('success', 'Success', compact('products'));
    }

    private function getProductWithRelatedItems()
    {
        $product_id = $this->input->post('product_id');

        $product = $this->Products_M
        ->with_portions([
            'fields' => 'id, title',
            'where' => [
                'products.active !=' => 3 
            ]
        ])
        ->with_stocks([
            'fields' => ['stocks.id', 'stocks.title', 'product_has_stocks.optional', 'product_has_stocks.quantity'],
            'where' => [
                'stocks.active !=' => 3 
            ]
        ])
        ->with_additional_products([
            'alias' => 'AdditionalProducts',
            'fields' => 'AdditionalProducts.*',
            'where' => [
                'AdditionalProducts.active !=' => 3 
            ]
        ])
        ->findOrFail([
            'id' => $product_id,
            'portion_id' => null,
        ]);

        messageAJAX('success', 'Success', compact('product'), 'json');
    }
    
    private function getProductsWithCategoryAndProduction(Type $var = null)
    {
        $location_id = $this->input->post('location_id');

        $products = $this->Products_M
        ->fields('*')
        ->select('(SELECT title FROM productions WHERE productions.id = products.production_id LIMIT 1) AS production_title')        
        ->select('(SELECT title FROM product_types WHERE product_types.id = products.product_type_id LIMIT 1) AS category')        
        ->order_by('id','DESC')->get_all([
            'location_id' => $location_id,
            'portion_id' => null,
        ]);
        
        messageAJAX('success', 'Success', compact('products'));
        
    }

    private function getCategories()
    {
        $categories = $this->ProductTypes_M->get_all(['parent_id !=' => null]);

        messageAJAX('success', 'Success', compact('categories'));
        
    }

    private function getCategoryTypes () {

        $product_types = $this->ProductTypes_M
        ->fields('*')        
        ->select('(SELECT title FROM product_types p WHERE p.id = product_types.parent_id LIMIT 1) as parent')
        ->get_all([
            'parent_id' => null
        ]);

        messageAJAX('success', 'Success', compact('product_types'));
        
    }

    private function handleQuickNotes($sys_quick_notes)
    {
        $quick_notes = (Array) $this->input->post('quick_notes');

        $data = [];
        foreach ($quick_notes as $quick_note_id) {

            foreach ($sys_quick_notes as $quick_note) {
                if ($quick_note->id == $quick_note_id) {
                    $data[$quick_note_id] = [
                        'product_id' => null,
                        'quick_note_id' => $quick_note_id,
                    ];
                }
            }
        }

        return $data;
    }

    // public function getProductsWithStocks()
    // {   
    //     $id = $this->input->post('product_id');

    //     $product = $this->Products_M->findOrFail($id);

    //     pr($product);
    // }

    function ajax1()
    {
        $target = isPost('target');

        if($target == 'products') {
            
            $location = @$_POST['location'];

            if (! in_array($location, $this->user->locations)) {
                messageAJAX('error', __('Geçersiz Restoran'));
            }

            $this->db->where('type', 'product');
            $products = $this->Products_model->index($location);

            messageAJAX('success', '', $products);
        }

        messageAJAX('error', __('Geçersiz işlem.'));
    }
}
?>
