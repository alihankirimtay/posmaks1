<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies

        $this->check_auth();
        
        $this->load->model('Configuration_model');

    }

    // List all your items
    public function index( $offset = 0 )
    {
        if(!$_POST):

            $this->load->template('Configuration/index');

        else:
            $this->load->library('form_validation');
            $this->load->helper('security');

            $this->form_validation->set_rules('name', 'Site Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('company', 'Company Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'E-Mail', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('url', 'URL', 'trim|required|prep_url|xss_clean');
            $this->form_validation->set_rules('date_short', 'Short Date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('date_long', 'Long Date', 'trim|required|xss_clean');
            // $this->form_validation->set_rules('dtz', 'Timezone', 'trim|required|xss_clean');
            $this->form_validation->set_rules('smtp_host', 'SMTP Host', 'trim|prep_url|required|xss_clean');
            $this->form_validation->set_rules('smtp_user', 'SMTP User', 'trim|required|xss_clean');
            $this->form_validation->set_rules('smtp_pass', 'SMTP Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('smtp_port', 'SMTP Port', 'trim|required|integer|max_length[5]|xss_clean');
            $this->form_validation->set_rules('is_ssl', 'SSL', 'trim|required|integer|max_length[1]|xss_clean');
            $this->form_validation->set_rules('virtual_keyboard', 'Sanal Klavye', 'trim|required|in_list[1,0]');
            
            if($this->form_validation->run() === FALSE):

                messageAJAX('error' , validation_errors());

            else:

                $save = $this->Configuration_model->save($this->input->post());

                if($save == TRUE):
                    messageAJAX('success' ,  __('Başarılı'));
                else:
                    messageAJAX('error' ,  __('Veritabanı hatası'));
                endif;

            endif;


        endif;

    }
}

/* End of file Configuration.php */
/* Location: ./application/controllers/Configuration.php */
