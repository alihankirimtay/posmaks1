<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paxnegs extends Auth_Controller {

    function __construct()
    {
        parent::__construct();
        $this->check_auth();
        $this->load->model('Paxnegs_model');
    }

    function index($location = NULL)
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $location = (int) $location;
        $data['current_location'] = $location ? $location : $this->user->locations[0];

        $data['paxnegs'] = $this->Paxnegs_model->index($data['current_location']);

    	$this->load->template('Paxnegs/index', $data);
    }

    function add($location = NULL)
    {
        $data['current_location'] = (int) $location;

        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('pax', 'Pax', 'trim|required|is_natural');
            $this->form_validation->set_rules('negs', 'Negs', 'trim|required|is_natural');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']|callback__unique_PaxnegsDate[ADD,'.$data['current_location'].']');

            if($this->form_validation->run()) {

                $this->Paxnegs_model->add( $data['current_location'] );
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin = ['start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");'];

            $this->load->template('Paxnegs/add', $data);
        }       
    }

    function edit($location = NULL, $id = NULL)
    {
        $data['current_location'] = (int) $location;
        $id = (int) $id;
        
        if(!$data['paxnegs'] = $this->exists('paxnegs', "id = $id AND location_id = ${data['current_location']}", '*')) {

            show_404();
        }

        if($data['current_location'] && !in_array($data['current_location'], $this->user->locations)) {
            messageAJAX('error', logs_($this->user->id, 'Permission denied.', $values));
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('pax', 'Pax', 'trim|required|is_natural');
            $this->form_validation->set_rules('negs', 'Negs', 'trim|required|is_natural');
            $this->form_validation->set_rules('date', 'Date', 'trim|required|is_date['.dateFormat().']|callback__unique_PaxnegsDate[EDIT,'.$data['current_location'].','.$id.']');

            if($this->form_validation->run()) {

                $this->Paxnegs_model->edit($data['current_location'], $id);
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->theme_plugin = ['start' => 'MyFunction.datetimepicker("'.dateFormat(TRUE).'");'];

            $this->load->template('Paxnegs/edit', $data);
        }      
    }

    // function delete($location = NULL, $id = NULL)
    // {
    //     $location = (int) $location;
    //     $id = (int) $id;

    //     if(!$paxnegs = $this->exists('paxnegs', "id = $id AND location_id = $location", '*')) {

    //         show_404();
    //     }

    //     $this->Paxnegs_model->delete($id, $paxnegs);
    // }

    function _unique_PaxnegsDate($date, $str)
    {
        $params   = @explode(',', $str);
        $target   = @$params[0];
        $location = (int) @$params[1];
        $id       = (int) @$params[2];

        $client_day_number = dateReFormat($date, dateFormat(), 'Y-m-d');
        $start_of_day      = dateConvert2($client_day_number . ' 00:00:00', 'server');
        $end_of_day        = dateConvert2($client_day_number . ' 23:59:59', 'server');

        $this->form_validation->set_message('_unique_PaxnegsDate', 'This %s already has been added.');

        $this->db->where([
            'date >='     => $start_of_day,
            'date <='     => $end_of_day,
            'location_id' => $location
        ]);

        if($target == 'EDIT') {

            $this->db->where('id !=', $id);

        }elseif($target != 'ADD') {

            return FALSE;
        }

        return (bool) (!$this->db->get('paxnegs', 1)->row());
    }
}
?>
