<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Memberships extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Memberships_model', 'Memberships');
    }

    public function index()
    {
        $this->theme_plugin = [
            'js' => ['panel/js/view/memberships.js'],
            'start' => "Memberships.index();"
        ];

        $this->load->template('Memberships/index');
    }

    public function checkOut()
    {
        $membership_id = (int) $this->input->get('membership_id');

        $form = $this->Memberships->getCheckOutForm([
            'membership_id' => $membership_id,
            'redirect' => base_url('memberships'),
        ]);

        if (!$form) {
            $error_message = $this->Memberships->errorMessage;
        }

        $this->load->template('Memberships/checkout', compact('form', 'error_message'));
    }

    public function settings()
    {
        if ($this->input->is_ajax_request()) {

            if ($this->Memberships->updateSettings($_POST)) {
                messageAJAX('success', __('Abonelik ayarlarınız başarıyla güncellendi!'));
            } else {
                messageAJAX('error', $this->Memberships->errorMessage);
            }

        } else {

            $this->theme_plugin = [
                'js' => ['panel/js/view/memberships.js'],
                'start' => "Memberships.settings();"
            ];

            $this->load->template('Memberships/settings');
        }
    }

    public function ajax($event = null)
    {
        switch ($event) {
            case 'getMemeberships':
                $this->getMemeberships();
                break;

            case 'getPayments':
                $this->getPayments();
                break;

            case 'getSettings':
                $this->getSettings();
                break;

            case 'subscription':
                $this->subscription();
                break;
            case 'DiscountCoupon':
                $this->Discount();
                break;
        }
    }

    private function getMemeberships()
    {
        if (!$memberships = $this->Memberships->findAll()) {
            messageAJAX('error', $this->Memberships->errorMessage);
        }

        messageAJAX('success', null, [
            'memberships' => $memberships
        ]);
    }

    private function getPayments()
    {
        if (!$payments = $this->Memberships->findPayments()) {
            messageAJAX('error', $this->Memberships->errorMessage);
        }

        messageAJAX('success', null, [
            'payments' => $payments
        ]);
    }

    private function getSettings()
    {
        if (!$payments = $this->Memberships->findSettings()) {
            messageAJAX('error', $this->Memberships->errorMessage);
        }

        messageAJAX('success', null, [
            'settings' => $payments
        ]);
    }

    private function subscription()
    {
        $data = [
            'membership_id' => $this->input->get('membership_id'),
            'discount_code' => $this->input->get('discountCode'),
            'redirect_url'  => base_url('memberships')
        ];

        if (!$subscription = $this->Memberships->subscription($data)) {
            messageAJAX('error', $this->Memberships->errorMessage);
        }

        messageAJAX('success', null, [
            'subscription' => $subscription
        ]);
    }

    private function Discount()
    {
        $data = [
            'code' => $this->input->get('code')
        ];

        if(!$discount = $this->Memberships->Discount($data)){
            messageAJAX('error', $this->Memberships->errorMessage);
        }

        messageAJAX('success', null, [
            'discount' => $discount
        ]);
    }
}
?>