<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Invoices extends Auth_Controller {

    private $locaiton_id;

    public function __construct()
    {
        parent::__construct();
        $this->check_auth([
            'allowed' => ['ajax']
        ]);

        $this->load->model('Invoices_M');
        $this->load->model('Locations_M');
    }

    public function index($location_id = null)
    {
        if ($location_id) {
            $this->db->where('location_id', $location_id);
        }

        $invoices = $this->Invoices_M
        ->fields(['*','(SELECT title FROM locations WHERE id = location_id) as location_title'])
        ->where('location_id', $this->user->locations)->get_all();

        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

    	$this->load->template('Invoices/index', compact('invoices', 'location_id'));
    }

    public function add()
    {

        if ($this->input->is_ajax_request()) {
            
            if ($this->Invoices_M->from_form()->insert()) {
                messageAJAX('success', __('Şablon oluşturuldu.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->theme_plugin = [
            'css' => ['panel/css/view/invoices.css', 'globals/plugins/jquery-ui/jquery-ui.min.css'],
            'js' => ['panel/js/view/invoices.js'],
            'start' => 'Invoices.initEditor();',
        ];

        $this->load->template('Invoices/add');
    }

    public function edit($id = null)
    {
        $invoice = $this->Invoices_M->where('location_id', $this->user->locations)->findOrFail($id);

        if ($this->input->is_ajax_request()) {
            
            if ($this->Invoices_M->from_form(null, null, ['id' => $id])->update()) {
                messageAJAX('success', __('Şablon güncellendi.'));
            }

            messageAJAX('error', validation_errors());
        }

        $this->theme_plugin = [
            'css' => ['panel/css/view/invoices.css', 'globals/plugins/jquery-ui/jquery-ui.min.css'],
            'js' => ['panel/js/view/invoices.js'],
            'start' => "Invoices.initEditor('{$invoice->json}' , $invoice->width , $invoice->height);",
        ];

        $this->load->template('Invoices/edit', compact('invoice'));
    }

    public function delete($id = null)
    {
        $this->Invoices_M->where('location_id', $this->user->locations)->findOrFail($id);

        $this->Invoices_M->delete(['id' => $id]);

        messageAJAX('success', __('Şablon başarıyla silindi.'));
    }


    public function setDefault($id = null){

        $location_id = $this->Invoices_M->get($id)->location_id;


        $this->Invoices_M->where('location_id' , $location_id);
        $this->Invoices_M->where('id !=' , $id);
        $this->Invoices_M->update(['default' => 0]);

        $this->Invoices_M->update(['default' => 1] , $id);

        redirect(base_url('invoices'),'refresh');
    }
}
?>
