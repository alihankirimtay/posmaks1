<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Expensetypes extends Auth_Controller {

    function __construct()
    {
        parent::__construct();

        $this->check_auth();
        
        $this->load->model('ExpenseTypes_model');
    }

    function index()
    {
        $this->theme_plugin = ['start' => 'TablesDataTables.init();'];

        $data['expensetypes'] = $this->ExpenseTypes_model->index();

    	$this->load->template('ExpenseTypes/index', $data);
    }

    function add()
    {
        if ($this->input->is_ajax_request()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->ExpenseTypes_model->add();
            }
            
            messageAJAX('error', validation_errors());

        } else {

            $this->load->template('ExpenseTypes/add');
        }       
    }

    function view($id = NULL)
    {
        $id = (int) $id;

        $data = $this->ExpenseTypes_model->view($id);
    }

    function edit($id = NULL)
    {
        if(!$data['expensetype'] = $this->exists('expense_types', "id = $id", '*')) {

            show_404();
        }

        if ($this->input->is_ajax_request()) {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'İsim', 'trim|required|min_length[2]|max_length[45]');
            $this->form_validation->set_rules('desc', 'Açıklama', 'trim');
            $this->form_validation->set_rules('active', 'Durum', 'trim|required|integer|in_list[0,1]');

            if($this->form_validation->run()) {

                $this->ExpenseTypes_model->edit($id);
            }
            
            messageAJAX('error', validation_errors());

        } else {
            
            $this->ExpenseTypes_model->edit($id);

            $this->load->template('ExpenseTypes/edit', $data);
        }
    }

    // function delete($id = NULL)
    // {
    //     $id = (int) $id;

    //     if(!$product = $this->exists('expense_types', "id = $id")) {

    //         show_404();
    //     }

    //     $this->ExpenseTypes_model->delete($id, $product);
    // }
}
?>
