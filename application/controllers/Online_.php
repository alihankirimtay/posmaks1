<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Online_ extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->check_auth();


    }
    
    public function index()
    {
        $this->load->template('Online/Online');
    }
}