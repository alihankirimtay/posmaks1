<div id="wizardModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style="display:flex; flex-flow: column nowrap;">
        <div class="wizard-header">
            <div class="col-lg-11 col-md-11 header-title">
                <span>HIZLI KURULUM SİHİRBAZI</span>
            </div>
            <div class="col-lg-1 col-md-1 close-button p-a-0">
                <button class="btn btn-danger col-md-12 br-0" style="padding:0 !important; height:64px !important"><i class="glyphicon glyphicon-remove br-0 fa-2x"></i></button>
            </div>
        </div>
        <div class="modal-body">
            <ul class="nav nav-pills nav-stacked nav-wizard">
                <li class="active"><a href="#stepLocation" data-toggle="pill">1.</a></li>
                <li><a href="#stepFloorsAndZones" data-toggle="pill">2.</a></li>
                <li><a href="#stepProduction" data-toggle="pill">3.</a></li>
                <li><a href="#stepStock" data-toggle="pill">4.</a></li>
                <li><a href="#stepProduct" data-toggle="pill">5.</a></li>
                <li><a href="#stepInfo" data-toggle="pill">6.</a></li>
            </ul>
            <div class="tab-content w100 h500" >
                    <div class="tab-pane active" id="stepLocation">
                        <?php $this->load->view('home/elements/tabs/location'); ?>     
                    </div>
                    <div class="tab-pane" id="stepFloorsAndZones">
                        <?php $this->load->view('home/elements/tabs/floorandzone'); ?>                             
                    </div>
                    <div class="tab-pane" id="stepProduction">
                        <?php $this->load->view('home/elements/tabs/production'); ?>                             
                    </div>
                    <div class="tab-pane" id="stepStock">
                        <?php $this->load->view('home/elements/tabs/stock'); ?>                                                     
                    </div>
                    <div class="tab-pane" id="stepProduct">
                        <?php $this->load->view('home/elements/tabs/product'); ?>                                                     
                    </div>
                    <div class="tab-pane" id="stepInfo" style="overflow-y: auto;height: 450px;">
                        <?php $this->load->view('home/elements/tabs/wizard_information'); ?>                                                     
                    </div>
            </div>
        </div>
        
    </div>

  </div>
</div>
