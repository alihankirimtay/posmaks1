<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <h4 class="content-header"><?= __('HIZLI ÜRÜN EKLEME') ?></h4>
            <a id="createProduct" href="#" class="btn btn-success btn-xs btn-ripple pull-right text-color-white"><i class="fa fa-plus"></i><?= __('Ürün Oluştur') ?></a>
        </div>
    </div>

    <div class="p-l-0 p-r-0 panel-body p-t-0">
        <div class="hidden">
            <a href="#indexProduct" data-toggle="tab" >1.</a>
            <a href="#addProduct" data-toggle="tab">2.</a>
            <a href="#editProduct" data-toggle="tab">3.</a>
        </div>
        <div class="tab-content p-a-0 b-0">

            <div class="tab-pane active" id="indexProduct">

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><?= __('Ürün Adı') ?></th>
                                <th><?= __('Üretim Yeri') ?></th>
                                <th><?= __('Ürün Kategorisi') ?></th>
                                <th><?= __('Fiyat') ?></th>
                                <th><?= __('İşlem') ?></th>
                            </tr>
                        </thead>
                    </table>
                    <div class="product-table-scroll">
                        <table id="fillWizardProductTable" class="table table-striped">
                            <tbody>
                            </tbody>
                        </table>
                    </div>     
                </div> 

            </div>

            <div class="tab-pane" id="addProduct">
            
            </div>
            <div class="tab-pane" id="editProduct">
            </div>
        </div>







        
    </div>

</div>
