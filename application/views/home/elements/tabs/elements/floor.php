<div class="well p-a-2">
    <div class="hidden">
        <a href="#floorAdd" data-toggle="tab" >1.</a>
        <a href="#floorEdit" data-toggle="tab">2.</a>
    </div>
    <div class="tab-content p-a-0 b-0">
        <div class="tab-pane active" id="floorAdd">
            <label><?= __('Kat Adı') ?></label>
            <form action="#" class="form-horizontal">
                <div class="form-content">
                    <div class="form-inline" style="width: calc(100% - 200px);">
                        <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title" placeholder="<?= __('Kat Adı Giriniz') ?>">
                        <input type="hidden" name="location_id" value="">
                        <input type="hidden" name="active" value="1">
                    </div>
                    <div class="form-inline pull-right">   
                        <button id="saveFloor" type="button" class="btn btn-success input-lg br-0"><?= __('KAYDET') ?></button>   
                    </div>
                </div>
            </form>
            <label class="label-ex-floor"><?= __('Örnek : Kat 1, Kat 2') ?></label>            
        </div>

        <div class="tab-pane b-0 p-a-0" id="floorEdit">
            <label><?= __('Kat Adını Düzenleyiniz') ?></label>
            <form action="#" class="form-horizontal">
                <div class="form-content">
                    <div class="form-inline" style="width: calc(100% - 200px);">
                        <input type="name" class="form-control input-lg bg-special-white w100 bb-0" name="title">
                        <input type="hidden" name="active" value="">
                        <input type="hidden" name="location_id" value="">
                        <input type="hidden" name="floor_id" value="">
                    </div>
                    <div class="form-inline pull-right">   
                        <button id="btnFloorEdit" type="button" class="btn btn-success input-lg br-0"><?= __('KAYDET') ?></button>   
                        <button id="btnFloorCancel" type="button" class="btn btn-danger input-lg br-0"><?= __('İPTAL') ?></button>   
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<div class="floor-list">
    
</div>

<button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl" data-type="floor">İLERİ</button>
