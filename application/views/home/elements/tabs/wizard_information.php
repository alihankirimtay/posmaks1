<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <div class="card card-green p-a-2"><?= __("Tebrikler, Posmaks kurulumunuz tamamlanmıştır. Posmaks'ı daha verimli kullanmak için aşağıdaki adımları takip edebilirsiniz."); ?> </div>
            <h4 class="content-header"><?= __('BUNDAN SONRA NELER YAPABİLİRSİNİZ ?') ?></h4>
        </div>
    </div>

    <div class="p-l-0 p-r-0 p-t-0 panel-body">
        <div class="tab-content p-a-0 b-0">
            <ul class="list-group">

                <li class="list-group-item m-t-2">
                    <?= __('Personellerinizi Ekleyebilirsiniz.'); ?>(<a href="<?= base_url('employes/add'); ?>" target="_blank"><?= __('Personel eklemek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __('Ürünlerinize ait fotoğraf ve detay bilgilerini girebilirsiniz.'); ?>(<a href="<?= base_url('products/index'); ?>" target="_blank"><?= __('Eklemiş olduğunuz ürünleri görüntülemek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __("Google Play Store'dan Posmaks Papyon Uygulamasını indirebilirsiniz."); ?>(<a href="https://play.google.com/store/apps/details?id=com.posmakspapyon" target="_blank"><?= __('Papyon uygulumasını görüntülemek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __("Google Play Store'dan Posmaks Tablet Menü uygulamasını indirebilirsiniz."); ?>(<a href="https://play.google.com/store/apps/details?id=com.posmakscustomer" target="_blank"><?= __('Tablet menü uygulumasını görüntülemek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __('Online Sipariş Modülü ile kendi web sitenizi kurarak internet üzerinden komisyonsuz sipariş almaya başlayabilirsiniz.'); ?>(<a href="<?= base_url('Online_'); ?>" target="_blank"><?= __('Online sipariş modülüne gitmek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __('Yemek Sepeti üyesi iseniz Posmaks ile entegre edebilirsiniz.'); ?>(<a href="<?= base_url('locations/edit/' . $this->user->locations[0]); ?>" target="_blank"><?= __('İşletmenize yemek sepeti entegrasyonu yapmak için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __('Cari Hesap çalıştığınız "müşteri" ve "tedarikçilerinizi" sisteme ekleyebilirsiniz.'); ?>(<a href="<?= base_url('cari'); ?>" target="_blank"><?= __('Cari hesap modülüne gitmek için tıklayınız'); ?></a>)
                </li>

                <li class="list-group-item m-t-2">
                    <?= __('Posmaks Masaüstü uygulamasını bilgisayarınıza yükleyerek üretim yerlerinizde caller id sistemini ve pos printer kullanmaya başlayabilirsiniz.'); ?>(<a href="https://posmaks.com/posmaks-app/posmaks.zip" target="_blank"><?= __('Posmaks masaüstü uygulamasını indirmek için tıklayınız'); ?></a>)
                </li>
                
                <li class="list-group-item m-t-2">
                    <?= __('Daha fazla bilgi için '); ?><a href="http://destek.posmaks.com" target="_blank">destek.posmaks.com</a> <?= __('adresini ziyaret edebilirsiniz.'); ?>
                </li>
            </ul>
        </div>
    </div>

</div>