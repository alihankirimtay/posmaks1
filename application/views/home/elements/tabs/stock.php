<div class="panel">
    <div class="panel-heading bb-0">
        <div class="panel-title p-l-0">
            <h4 class="content-header"><?= __('STOKLAR') ?></h4>
            <h6 style="color: black"><?= __('Ürün reçetelerinizin içerdiği malzemeleri elinizdeki güncel miktarları ile giriniz.'); ?></h6>
        </div>
    </div>

    <div class="p-l-0 p-r-0 p-t-0 panel-body" style="overflow-y:auto;height: 300px;overflow-x: hidden;">
        <div class="well p-a-2 col-md-12" >
            <div class="hidden">
                <a href="#stockAdd" data-toggle="tab" >1.</a>
                <a href="#stockEdit" data-toggle="tab">2.</a>
            </div>
            <div class="tab-content p-a-0 b-0">
                <div class="tab-pane active" id="stockAdd">
                    <form class="form-inline col-md-12 p-l-0" role="form">
                        <div class="col-md-12 p-l-0">
                        
                            <div class="form-group col-md-6 p-l-0">
                                <label for="stockTitleInput d-block">Stok Adı</label>
                                <input id="stockTitleInput" type="text" name="title" class="form-control bg-special-white bb-0 w-full"/>
                                <input type="hidden" name="active" value="1">
                                <input type="hidden" name="location_id" value="">
                                <input type="hidden" name="sub_limit" value="0">
                                <input type="hidden" name="target" value="0">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="stockUnitInput">Birim</label>                             
                                <select id="stockUnitInput" data-placeholder="Ölçü Birimi bulunamadı" name="unit" class="chosen-select">
                                    <?php foreach ($this->units as $key => $unit): ?>
                                        <option value="<?= $key ?>"><?= $unit ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="stockQuantityInput">Miktar</label>                                                         
                                <input id="stockQuantityInput" type="number" name="quantity" min="0" class="form-control bg-special-white w100 bb-0" />
                            </div>     
                        </div>

                        <label class="label-ex-floor col-md-12 p-l-0"><?= __('Örnek : Kola 330ml, Su 1lt, Domates, Portakal, Zeytinyağı, Tuz, Mayonez, Ketçap vs.'); ?></label>                        
                        
                        <button id="saveStock" type="button" class="btn btn-success input-lg br-0">YENİ STOK EKLE</button>
                    
                    </form>
                </div>

                <div class="tab-pane b-0 p-a-0" id="stockEdit">
                    <form class="form-inline col-md-12 p-l-0" role="form">
                        <div class="col-md-12 p-l-0 button-flexible">

                            <div class="form-group col-md-4 p-l-0">
                                <label for="stockEditTitleInput d-block">Stok Adı</label>                                
                                <input type="text" name="title" id="stockEditTitleInput" class="form-control bg-special-white bb-0 w-full" />
                                <input type="hidden" name="active" value="">
                                <input type="hidden" name="location_id" value="">
                                <input type="hidden" name="sub_limit" value="">
                                <input type="hidden" name="stock_id" value="">
                                <input type="hidden" name="target" value="">
                            </div>
                                <div class="form-group col-md-2">
                                <select name="unit" class="chosen-select">
                                    <?php foreach ($this->units as $key => $unit): ?>
                                        <option value="<?= $key ?>"><?= $unit ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <input type="number" name="quantity" min="0" class="form-control bg-special-white w100 bb-0" />
                            </div>
                            <button id="btnEditStock" type="button" class="btn btn-success col-md-2 br-0">KAYDET</button>
                            <button id="btnCancelStock" type="button" class="btn btn-danger col-md-2 br-0">İPTAL</button>
                        </div>
                    </form>
                    <label class="label-ex-floor">Örnek : Kola 330ml, Su 1lt, Domates, Portakal vs</label>
                    
                </div>
            </div>

        </div>
        <div class="stock-list">
            
        </div> 
        
                <button type="button" class="btn btn-success footer-button-fixed br-0 btn-xxl">İLERİ</button>

    </div>


</div>
