
<div class="content">

    <div class="row p-t-3">

        <?php if (hasPermisson('pointofsales.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('pointofsales') ?>">
                <div class="card card-iconic card-green">
                    <div class="card-full-icon fa fa-shopping-basket"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __('YENİ SİPARİŞ'); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif; ?>
        
        <?php if (hasPermisson('pointofsales.tables')): ?>
       <div class="col-md-3">
            <a href="<?= base_url('pointofsales/tables') ?>">
                <div class="card card-iconic card-red">
                    <div class="card-full-icon fa fa-check"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __('HESAP AL'); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div> 
        <?php endif; ?>
        
        <?php if (hasPermisson('pointofsales.orders')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('pointofsales/orders') ?>">
                <div class="card card-iconic card-amber">
                    <div class="card-full-icon fa fa-birthday-cake"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("MUTFAK"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('sales.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('sales') ?>">
                <div class="card card-iconic card-pink">
                    <div class="card-full-icon fa fa-line-chart"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("SATIŞLAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('products.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('products') ?>">
                <div class="card card-iconic card-brown">
                    <div class="card-full-icon fa fa-shopping-bag"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("ÜRÜNLER"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('producttypes.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('producttypes') ?>">
                <div class="card card-iconic card-brown dark">
                    <div class="card-full-icon fa fa-list"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("ÜRÜN KATEGORİLERİ"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('stocks.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('stocks') ?>">
                <div class="card card-iconic card-blue">
                    <div class="card-full-icon fa fa-archive"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("STOKLAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('pos.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('pos') ?>">
                <div class="card card-iconic card-orange">
                    <div class="card-full-icon fa fa-money"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("KASALAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('expenses.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('expenses') ?>">
                <div class="card card-iconic card-pink">
                    <div class="card-full-icon fa fa-area-chart"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("GİDERLER"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('expensetypes.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('expensetypes') ?>">
                <div class="card card-iconic card-red">
                    <div class="card-full-icon fa fa-flask"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("GİDER TÜRLERİ"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('inventory.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('inventory') ?>">
                <div class="card card-iconic card-blue-grey">
                    <div class="card-full-icon fa fa-star-half-o"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("DEMİRBAŞLAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('reports.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('reports') ?>">
                <div class="card card-iconic card-purple">
                    <div class="card-full-icon fa fa-pie-chart"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("RAPORLAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('floors.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('floors') ?>">
                <div class="card card-iconic card-light-green">
                    <div class="card-full-icon fa fa-building-o"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("KATLAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('zones.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('zones') ?>">
                <div class="card card-iconic card-deep-purple">
                    <div class="card-full-icon fa fa-building"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("BÖLGELER"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('points.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('points') ?>">
                <div class="card card-iconic card-brown">
                    <div class="card-full-icon fa fa-map-marker"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("MASALAR"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('employes.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('employes') ?>">
                <div class="card card-iconic card-indigo">
                    <div class="card-full-icon fa fa-users"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("PERSONELLER"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('sales.scoreboard')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('sales/scoreboard') ?>">
                <div class="card card-iconic card-grey">
                    <div class="card-full-icon fa fa-bar-chart "></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("GÜNLÜK PERFORMANS"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <?php if (hasPermisson('shiftschedule.index')): ?>
        <div class="col-md-3">
            <a href="<?= base_url('shiftschedule') ?>">
                <div class="card card-iconic card-indigo">
                    <div class="card-full-icon fa fa-clock-o"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("VARDİYA ÇİZELGESİ"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

        <div class="col-md-3">
            <a href="<?= base_url('Online_') ?>">
                <div class="card card-iconic card-blue">
                    <div class="card-full-icon fa fa-laptop"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("ONLİNE SİPARİŞ MODÜLÜ"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>

        <?php if (hasPermisson('wizard')): ?>
        <div class="col-md-3">
            <a href="#" id="homeWizard">
                <div class="card card-iconic card-orange">
                    <div class="card-full-icon fa fa-magic"></div>
                    <div class="card-body text-left">
                        <h4><b><?= __("HIZLI KURULUM SİHİRBAZI"); ?></b></h4>
                    </div> 
                </div>
            </a>
        </div>
        <?php endif;?>

    </div>

    <?php $this->load->view('home/elements/wizard'); ?>
    
    
    
</div>

<style type="text/css">
    .card.card-iconic {
        max-height: 121px;
    }
    .card.card-iconic .card-full-icon {
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left:  30%;
        margin-top: -40px;
        font-size: 80px;
        opacity: .3;
        color: #fff;
    }
    .card .card-body h4 {
        margin-top: 14.5px !important;
    }
</style>