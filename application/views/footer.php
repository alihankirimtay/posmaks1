<div class="layer-container">
        <!-- BEGIN MENU LAYER -->
        <div class="menu-layer">
            <ul>
                <li>
                    <a href="<?= base_url('') ?>"><?= __('ANA EKRAN') ?></a>
                </li>

                <li id="pos">
                    <a href="<?= base_url('pointofsales') ?>"><?= __('POS') ?></a>
                </li>

                <li data-tutorialize="orders">
                    <a href="<?= base_url('pointofsales/orders') ?>"><?= __('MUTFAK') ?></a>
                </li>

                <li id="islemler">
                    <a href="javascript:;"><?= __('İŞLEMLER') ?></a>
                    <ul class="child-menu">
                        <li><a href="<?= base_url('sales') ?>"><?= __('Satışlar') ?></a></li>
                        <li><a href="<?= base_url('expenses') ?>"><?= __('Giderler') ?></a></li>
                        <li><a href="<?= base_url('expensetypes') ?>"><?= __('Gider Kategorileri') ?></a></li>
                        <li><a href="<?= base_url('customers') ?>"><?= __('Müşteriler') ?></a></li>
                        <li><a href="<?= base_url('suppliers') ?>"><?= __('Tedarikçiler') ?></a></li>
                    </ul>
                </li>

                <li id="pos">
                    <a href="<?= base_url('cari') ?>"><?= __('CARİ HESAP') ?></a>
                </li>

                <li id="isletmeYonetimi">
                    <a href="javascript:;"><?= __('İŞLETME YÖNETİMİ') ?></a>
                    <ul class="child-menu">
                        <li><a href="<?= base_url('locations') ?>"><?= __('İşletmeler') ?></a></li>
                        <li><a href="<?= base_url('inventory') ?>"><?= __('Demirbaşlar') ?></a></li>
                        <li><a href="<?= base_url('pos') ?>"><?= __('Kasalar') ?></a></li>
                        <li><a href="<?= base_url('floors') ?>"><?= __('Katlar') ?></a></li>
                        <li><a href="<?= base_url('zones') ?>"><?= __('Bölgeler') ?></a></li>
                        <li><a href="<?= base_url('points') ?>"><?= __('Masalar') ?></a></li>
                        <li><a href="<?= base_url('invoices') ?>"><?= __('Fatura Şablonları') ?></a></li>
                    </ul>
                </li>

                <li id="urunler">
                    <a href="javascript:;"><?= __('ÜRÜNLER') ?></a>
                    <ul class="child-menu">
                        <li><a href="<?= base_url('products') ?>"><?= __('Tüm Ürünler') ?></a></li>
                        <li><a href="<?= base_url('producttypes') ?>"><?= __('Ürün Kategorileri') ?></a></li>
                        <li><a href="<?= base_url('productions') ?>"><?= __('Üretim Yerleri') ?></a></li>
                        <li><a href="<?= base_url('stocks') ?>"><?= __('Stoklar') ?></a></li>
                        <li><a id="stockAlarms" href="<?= base_url('stocks/alarm') ?>"><?= __('Stok Alarmları') ?></a></li>
                        <li><a href="<?= base_url('stockcounts') ?>"><?= __('Stok Sayımı') ?></a></li>
                        <li><a href="<?= base_url('yemeksepetisync') ?>"><?= __('Yemek Sepeti') ?></a></li>
                    </ul>
                </li>

                <li id= "personel">
                    <a href="javascript:;"><?= __('PERSONEL YÖNETİMİ') ?></a>
                    <ul class="child-menu">
                        <li><a href="<?= base_url('employes') ?>"><?= __('Personeller') ?></a></li>
                        <li><a href="<?= base_url('shiftschedule') ?>"><?= __('Vardiya Çizelgesi') ?></a></li>
                    </ul>
                </li>

                <li data-tutorialize="reports">
                    <a href="<?= base_url('reports?tab=index') ?>"><?= __('RAPORLAR') ?></a>
                </li>

                <li>
                    <a href="<?= base_url('sales/scoreboard') ?>"><?= __('GÜNLÜK PERFORMANS') ?></a>
                </li>

                <li><a href="<?= base_url('Online_') ?>"><?= __('ONLINE SİPARİŞ MODÜLÜ') ?></a></li>

                <li id="ayarlar">
                    <a href="javascript:;"><?= __('AYARLAR') ?></a>
                    <ul class="child-menu">
                        <li><a href="<?= base_url('configuration'); ?>"><?= __('Uygulama Ayarları') ?></a></li>
                        <li><a href="<?= base_url('roles'); ?>"><?= __('Yetkiler') ?></a></li>
                        <li><a href="<?= base_url('syslogs'); ?>"><?= __('Loglar') ?></a></li>
                        <li><a href="<?= base_url('memberships'); ?>"><?= __('Abonelik Bilgileri') ?></a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?= base_url('sss') ?>"><?= __('YARDIM') ?></a>
                </li>
                
                <li>
                    <a href="<?= base_url('login/logout') ?>"><?= __('ÇIKIŞ') ?></a>
                </li>
            </ul>
        </div><!--.menu-layer-->
        <!-- END OF MENU LAYER -->


        <!-- BEGIN USER LAYER -->
        <div class="user-layer">
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="active"><a href="#messages" data-toggle="tab"><?= __('Mesajlar') ?> <span class="badge">0</span></a></li>
                <li><a href="#settings" data-toggle="tab"><?= __('Ayarlar') ?></a></li>
            </ul>

            <div class="row no-gutters tab-content">

                <div class="tab-pane  fade in active" id="messages">
                    <div class="col-md-4">
                        <div class="message-list-overlay"></div>
                        <div class="form-group">
                            <input type="text" id="user-search" class="form-control text-center" placeholder="<?= __('Kullanıcı Ara') ?>">
                        </div>
                        <ul class="list-material message-list">
                        
                         </ul>
                    </div><!--.col-->
                    <div class="col-md-8">
                        <div class="message-send-container" style="display:none;">

                            <div class="messages">
                                
                            </div><!--.messages-->

                            <div class="send-message">
                                <div class="input-group">
                                    <div class="inputer inputer-blue">
                                        <div class="input-wrapper">
                                            <textarea rows="1" id="send-message-input" class="form-control js-auto-size" placeholder="<?= __('Mesaj') ?>"></textarea>
                                        </div>
                                    </div><!--.inputer-->
                                    <span class="input-group-btn">
                                        <button id="send-message-button" class="btn btn-blue" type="button"><?= __('Gönder') ?></button>
                                    </span>
                                </div>
                            </div><!--.send-message-->

                        </div><!--.message-send-container-->
                    </div><!--.col-->

                    <div class="mobile-back">
                        <div class="mobile-back-button"><i class="ion-android-arrow-back"></i></div>
                    </div><!--.mobile-back-->
                </div><!--.tab-pane #messages-->

                
                <div class="tab-pane" id="settings">
                    <div class="col-md-6 col-md-offset-3">
                    <form>
                        <div class="settings-panel">

                            <div class="legend"><?= __('Profil Ayarları') ?></div>
                            <ul>
                                <li>
                                <?= __('Profil Resmi :') ?>
                                
                                <div class="pull-right">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                            <img src="<?= ($this->user->image ? base_url('assets/uploads/profile/'.$this->user->image) : base_url('assets/uploads/profile/no_image.png')) ?>" width="100%">
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new"><?= __('Resim seç') ?></span>
                                                <span class="fileinput-exists"><?= __('Değiştir') ?></span>
                                                <input type="file" name="pimage"></span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= __('Kaldır') ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Zaman Dilimi:') ?>
                                    <div class="pull-right" style="min-width: 300px;">
                                        <select name="dtz" data-live-search="true" class="chosen-select">
                                            <option value=""><?= __('Seç') ?></option>
                                            <?php foreach(DateTimeZone::listIdentifiers() as $key => $timezone): ?>
                                                <option <?= selected($this->user->dtz, $timezone) ?> value="<?= $timezone; ?>"><?= $timezone; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Dil') ?>
                                    <div class="pull-right" style="min-width: 300px;">
                                        <select name="language" class="chosen-select">
                                            <?php foreach($this->lang->getList() as $lang): ?>
                                                <option <?= selected($this->lang->get(), $lang) ?> value="<?= $lang; ?>"><?= __(ucfirst($lang)) ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Kullanıcı Adı:') ?>
                                    <div class=" pull-right">
                                       <span><?= $this->user->username; ?></span>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Yetki:') ?>
                                    <div class=" pull-right">
                                       <span><?= $this->user->role->title ?></span>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Eposta:') ?>
                                    <div class="pull-right">
                                        <span><?= $this->user->email;?></span>
                                    </div>
                                </li>
                                <li>
                                    <?= __('Şifre:') ?>
                                    <div class="pull-right">
                                        <input type="password" name="password" placeholder="<?= __('Şifre') ?>" class="form-control">
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-right small">
                                        <i><?= dateConvert2(date('Y-m-d H:i:s'), 'client', 'Y-m-d H:i:s', 'Y-m-d \a\t h:i A \u\s\i\n\g \U\T\C O') ?></i>
                                    </div>
                                </li>
                            </ul>

                        <hr/>
                        <button type="submit" href="javascript:void(0);" name="dosubmit" data-url="<?= base_url('users/edit'); ?>"  class="btn btn-primary pull-right"><?= __('Kaydet') ?></button>
                        </form>

                        </div><!--.settings-panel-->
                    </div><!--.col-->
                </div><!--.tab-pane #settings-->

            </div><!--.row-->
        </div><!--.user-layer-->
        <!-- END OF USER LAYER -->

    </div><!--.layer-container-->

    <!-- BEGIN GLOBAL AND THEME VENDORS -->
    <script src="<?= asset_url('globals/js/global-vendors.js'); ?>"></script>
    <!-- END GLOBAL AND THEME VENDORS -->

    <!-- JQUERY FORM -->
    <script src="<?= asset_url('globals/scripts/jquery.form.min.js'); ?>"></script>
    <!-- END JQUERY FORM -->
    <!-- MOMENTJS -->
    <script src="<?= asset_url('globals/plugins/momentjs/moment-with-locales.js'); ?>"></script>
    <!-- END MOMENTJS -->
    <script src="<?= asset_url('globals/js/master.js'); ?>"></script>
    <script src="<?= asset_url('globals/js/master_functions.js'); ?>"></script>
    <script src="<?= asset_url('globals/js/script.js'); ?>"></script>
    <script src="<?= asset_url('globals/js/message.js'); ?>"></script>
    <script src="<?= asset_url('globals/plugins/jasny-bootstrap/dist/js/jasny-bootstrap.min.js'); ?>"></script>
    <!-- BEGIN PLUGINS AREA -->
    
    <!-- DATATABLES -->
    <script src="<?= asset_url('globals/plugins/datatables/media/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?= asset_url('globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.js')?>"></script>
    <script src="<?= asset_url('globals/scripts/tables-datatables.js')?>"></script>
    <!-- END DATATABLES -->

    <!-- chosen -->
    <script src="<?= asset_url('globals/plugins/chosen/chosen.jquery.min.js')?>"></script>
    <!-- END chosen -->
    <!-- mask money -->
    <script src="<?= asset_url('globals/plugins/maskmoney/jquery.maskMoney.js')?>"></script>
    <script src="<?= asset_url('globals/plugins/autoNumeric/autoNumeric.js')?>"></script>
    <!-- END mask money -->

    <!-- bootstrap-datetimepicker -->
    <script src="<?= asset_url('globals/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')?>"></script>
    <script src="<?= asset_url('globals/plugins/bootstrap-datetimepicker/js/locales/tr.js')?>"></script>
    <!-- END -bootstrap-datetimepicker -->

    <?php if(isset($this->theme_plugin['js'])): ?>
        <?php foreach ($this->theme_plugin['js'] as $key => $js): ?>
           <script type="text/javascript" src="<?= preg_match('/^(http:|https:|www.)/', $js) ? $js : asset_url($js); ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if(isset($this->theme_plugin['scripts'])): ?>
        <?php foreach($this->theme_plugin['scripts'] as $key => $script): ?>
          <script type="text/javascript" src="<?= preg_match('/^(http:|https:|www.)/', $script) ? $script : asset_url($script); ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php foreach ($this->_assets->js as $key => $js): ?>
        <script type="text/javascript" src="<?= preg_match('/^(http:|https:|www.)/', $js) ? $js : asset_url($js); ?>"></script>
    <?php endforeach; ?>
    <script type="text/javascript"><?= $this->_assets->script ?></script>
    <!-- END PLUGINS AREA -->



    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
        <script type="text/javascript">

        </script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->

    <!-- PLEASURE -->
    <script src="<?= asset_url('globals/js/pleasure.js'); ?>"></script>
    <!-- ADMIN 1 -->
    <script src="<?= asset_url('panel/js/layout.js'); ?>"></script>

    <!-- BEGIN INITIALIZATION-->
    <script>
    $(document).ready(function () {
        Pleasure.init();
        Layout.init();
        Message.init();
        <?php if(isset($this->theme_plugin['start'])): ?>
            <?= $this->theme_plugin['start']; ?>
        <?php endif; ?>
    });
    </script>
    <!-- END INITIALIZATION-->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter50851199 = new Ya.Metrika2({
                        id:50851199,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/50851199" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

</body>
</html>
