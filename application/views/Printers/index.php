<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Printers</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>" class="active">Printers</a></li>
                    <li><a href="#"><?= $current_location ? @$this->user->locations_array[$current_location]['title'] : 'ALL LOCATIONS' ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $current_location, 'printers/index/', 'Locations'); ?>
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Printers</h4>
                        <div class="btn-group pull-right">
                            <?php if ($current_location): ?>
                                <a href="<?php ECHO base_url('printercounts/multiple/'.$current_location); ?>" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add End Of Day</a>
                            <?php endif; ?>
                            <a href="<?php ECHO base_url('printers/add/'.$current_location); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> New Printer</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Name</th>
                                    <th class="col-md-3">Location</th>
                                    <th class="col-md-3">Created</th>
                                    <th class="col-md-1">Status</th>
                                    <th class="col-md-2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($printers as $printer): ?>
                                    <tr>
                                        <td><?= $printer->title ?></td>
                                        <td><?= $printer->location ?></td>
                                        <td><?= dateConvert($printer->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($printer->active) ?></td>
                                        <td>
                                            <a class="btn btn-info btn-xs" href="<?php ECHO base_url('printercounts/index/'.$printer->id); ?>"> <i class="fa fa-paper-plane-o"></i> </a>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('printers/edit/'.$printer->location_id.'/'.$printer->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <!-- <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('printers/delete/'.$printer->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button> -->
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                Filter <span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="" method="get" class="form-horizontal" role="form">
                                    <div class="form-content">

                                        <div class="col-xs-12 col-sm-12 col-md-6">

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> Date From</label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dFrom" class="form-control datetimepicker-basic" value="<?php ECHO dateConvert($date_from, 'client', dateFormat()); ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> To</label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?php ECHO dateConvert($date_to, 'client', dateFormat()); ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 text-right">
                                                    <button type="submit" class="btn btn-primary">Apply</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= ($current_location ? 'Waste for ' . $this->user->locations_array[ $current_location ][ 'title' ] : 'Select a location.') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Unsold</th>
                                    <th>Waste</th>
                                    <th>Comps</th>
                                    <th>Unseen</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($wastes): ?>
                                    <?php foreach ($wastes as $waste): ?>
                                        <tr>
                                            <td><?= dateConvert($waste->date, 'client', dateFormat()) ?></td>
                                            <td><?= $waste->unsold ?></td>
                                            <td><?= $waste->waste ?></td>
                                            <td><?= $waste->comps ?></td>
                                            <td><?= $waste->unseen ?></td>
                                            <td>
                                                <a class="btn btn-primary btn-xs" href="<?= base_url('printers/waste/'.$current_location.'/'.$waste->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
