<div class="container-fluid tab-body">
	<div class="col-md-12 col-xs-12 nav-row">
		<div class="col-md-3 col-sm-3 col-xs-2 p-10">      
		</div>

		<div class="col-md-6 col-sm-6 col-xs-8 text-center p-10 pl-0">
			<span id="tableNameInterimPayment" class="home-table text-center" style="color:white;"><?= __('MASA ADI') ?></span>    
		</div>

		<div class="col-md-3 col-sm-3 col-xs-2 p-10 text-right">
		</div>
	</div>
	<div id="order-complete-tab">
		<div class="text-center">
			<h4 class="order-complete-title"></h4>
	        <img id="order-okay" src="<?= asset_url('okay.svg'); ?>"></img>
			<h3 class="text-center"><?= __('SİPARİŞ İLETİLDİ') ?></h3>
	        <img id="cancel-order" class="icon-svg-size" src="<?= asset_url('cancel.svg'); ?>" style = "margin-top: 50px;"></img>
		</div>
	</div>
</div>