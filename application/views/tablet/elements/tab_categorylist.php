<div class="container-fluid tab-body body-bg">
	<div class="col-md-12 col-xs-12 nav-row">
		<div class="col-md-3 col-sm-3 col-xs-2 p-10">      
            <img class="locked-click icon-svg-size" src="<?= asset_url('unlocked.svg'); ?>"></img>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-8 text-center p-10 pl-0">
			<span class="home-table text-center" style="color:white;"><?= __('MASA ADI') ?></span>    
		</div>

		<div class="col-md-3 col-sm-3 col-xs-2 p-10 text-right">
            <img class="basket-click icon-svg-size" src="<?= asset_url('basket.svg'); ?>"></img>
		</div>
	</div>
	<div id="category-box" class="alignment col-md-12 col-xs-12">
	
	</div>

</div>
