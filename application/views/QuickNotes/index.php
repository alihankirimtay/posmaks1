<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Hızlı Not"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Hızlı Not"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Hızlı Notlar') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="createQuickNote" href="<?=base_url('quicknotes/add/')?>" class="btn btn-warning btn-xs"><i class="fa fa-plus"></i> <?= __('Hızlı Not Ekle') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-4"><?= __("İsim"); ?></th>
                                    <th class="col-md-4"><?= __("Oluşturuldu"); ?></th>
                                    <th class="col-md-2"><?= __("Durum"); ?></th>
                                    <th class="col-md-2"><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($quickNotes as $quickNote): ?>
                                    <tr>
                                        <td><?php ECHO $quickNote->title; ?></td>
                                        <td><?=dateConvert($quickNote->created, 'client', dateFormat())?></td>
                                        <td><?php ECHO get_status($quickNote->active); ?></td>
                                        <td> 
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('quicknotes/edit/'.$quickNote->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('quicknotes/delete/'.$quickNote->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>