<?php 
 ?>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Ürünler') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url()?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ürünler') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                Filtre <span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="" method="get" class="form-horizontal" role="form">
                                    <div class="form-content">

                                        <div class="col-xs-12 col-sm-12 col-md-6" id="filter-product-type">
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-location"></i> <?= __('Restoran') ?></label>
                                                <div class="col-md-9">
                                                    <select name="location_id" class="chosen-select">
                                                        <?php foreach ($this->user->locations_array as $location): ?>
                                                            <option value="<?= $location['id']; ?>" <?= selected($location_id, $location[ 'id' ]) ?>>
                                                                <?= $location['title'] ;?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-category"></i> <?= __('Kategori') ?></label>
                                                <div class="col-md-9">
                                                    <select name="category_id" class="chosen-select">
                                                        <option value="0"><?= __('Tüm Kategoriler') ?></option>
                                                        <?php foreach ($categories as $category): ?>
                                                            <option value="<?= $category->id ?>"><?= $category->title ?></option>
                                                        <?php endforeach ?>

                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                                    <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Ürünler') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="product-passive" href="#" class="btn btn-danger btn-xs"><i class="fa fa-close"></i> <?= __('Tüm ürünleri pasif yap') ?></a>
                            <a id="product-active" href="#" class="btn btn-primary btn-xs"><i class="fa fa-check"></i> <?= __('Tüm ürünleri aktif yap') ?></a>
                            <a href="<?=base_url('quicknotes')?>" class="btn btn-warning btn-xs"><i class="fa fa-sticky-note-o" aria-hidden="true"></i><?= __('Hızlı Notlar') ?></a>
                            <a id="urunOlustur" href="<?=base_url('products/add/')?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Ürün Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" id="product-form">
                        <div class="overflow-table">
                            <table class="display datatables-basic" id="product-table">
                                <thead>
                                    <tr>
                                        <th><?= __('Tür') ?></th>
                                        <th><?= __('İsim') ?></th>
                                        <th><?= __('Fiyat') ?></th>
                                        <th><?= __('Oluşturuldu') ?></th>
                                        <th><?= __('Durum') ?></th>
                                        <th><?= __('İşlem') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($products as $product): ?>
                                        <?php
                                            $type_class = 'yellow text-black"';

                                            $type_title = '<i class="fa fa-th-large"></i> ' . __('Menü');

                                            if ($product->type == 'product') {

                                                $type_class = 'red';

                                                $type_title = '<i class="fa fa-square"></i> ' . __('Ürün');
                                            }
                                        ?>
                                        <tr data-id="">
                                            <td><h6><span class="label label-<?=$type_class?>"><?=$type_title?></span></h6></td>
                                            <td><?=$product->title?></td>
                                            <td><?= addTax($product->price, $product->tax) ?></td>
                                            <td class="small"><?=dateConvert($product->created, 'client', dateFormat())?></td>
                                            <td>
                                                <div class="radioer form-inline">
                                                    <input type="radio" name="status[<?=$product->id?>]" id="active<?=$product->id ?>" data-id="<?=$product->id?>" value="1" <?= checked($product->active, 1) ?>>
                                                    <label for="active<?=$product->id ?>"><?= __('Aktif') ?></label>
                                                </div>
                                                <div class="radioer form-inline">
                                                    <input type="radio" name="status[<?=$product->id?>]" id="pasif<?=$product->id ?>" data-id="<?=$product->id?>" value="0" <?= checked($product->active, 0) ?>>
                                                    <label for="pasif<?=$product->id ?>"><?= __('Pasif') ?></label>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="btn btn-primary btn-xs" href="<?=base_url('products/edit/'.$product->id)?>"> <i class="fa fa-pencil"></i> </a>
                                                <button class="btn btn-danger btn-xs" data-url="<?=base_url('products/delete/'.$product->id)?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>

