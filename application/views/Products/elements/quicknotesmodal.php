<div id="quicknotesmodal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="wizard-header">
                <div class="col-lg-10 header-title">
                    <span>HIZLI NOT EKLE</span>
                </div>
                <div class="col-lg-2 close-button" data-dismiss="modal">
                    <span class="pull-right">X</span>
                </div>
            </div>
            <div class="modal-body">
                <div class="tab-content" style="overflow-y:auto;">
                    <form action="#" class="form-horizontal" id="quickNotesForm">
                        <div class="form-content">
                            <div class="form-group">
                                <label class="control-label col-md-1"><?= __('Not') ?></label>
                                <div class="col-md-11">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" placeholder="<?= __('Notunuzu Giriniz'); ?>">
                                            <input type="hidden" name="active" value="1">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="btn-group col-md-12 p-a-0">
                                <button id="saveQuickNotes" type="button" class="br-0 btn btn-ripple btn-success btn-xl col-md-6" ><?= __('Kaydet') ?></button>
                                <button data-dismiss="modal" type="button" class="br-0 btn btn-danger btn-ripple btn-xl col-md-6" ><?= __('İptal') ?></button>
                            </div>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
    
    </div>
</div>
