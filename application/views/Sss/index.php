<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Yardım') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?=base_url()?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Yardım') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    


    <!--  -->
    <div class="container sss">

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <?= __('Yeni Restoran Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?= __('Yeni Restoran oluştururken, aşağıdaki resimde görüldüğü gibi, bulunduğunuz bölgenin saat dilimi, restoran adınız, faaliyet alanınızın kısa açıklaması, ürünlerde kullanılacak vergi oranı, kullanacağınız para biriminin simgesi ve restoranınızın aktif yada pasif durumda
                        olduğunu seçebileceğiz alanları doldurmanız gerekmekedir. Yeni restoran oluşturma sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('locations/add') ?>" target="_blank"><?= __('Yeni Restoran oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks1.PNG') ?>" class="img-responsive">   


                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <?= __('Kat ve Bölge Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <?= __('Kat oluştururken, işletmenizde bulunan kat sayısı kadar kat oluşturmanız gerekmektedir. Aşağıdaki resimde görüldüğü gibi kat oluşturuken, kat ismi, kısa açıklaması ve restoran seç alanlarını doldurmanız gerekmektedir. Kat oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('floors/add') ?>" target="_blank"><?= __('Yeni Kat oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks2.PNG') ?>" class="img-responsive"> 
                        <?= __('Bölge oluşturuken, işletmenizde bulunan katlara atanmış bölgeler oluşturmanız gerekmektedir. Aşağıdaki resimde görüldüğü gibi bölge oluşturuken, bölge ismi, kısa açıklaması, restoran seç ve bölgenin bulunduğu kat seç alanlarını doldurmanız gerekmektedir. Bölge oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('zones/add') ?>" target="_blank"><?= __('Yeni Bölge oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks3.PNG') ?>" class="img-responsive"> 
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <?= __('Yeni Masa Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <?= __('Yeni masa oluştururken, işletmenizde bulunan kat, o kata tanımlı bölge ve o bölgeye tanımlı masa oluşturmanız gerekmektedir. Aşağıdaki resimde görüldüğü gibi masa oluşturuken, masa ismi, kısa açıklaması ve restoran seç, kat seç, bölge seç alanlarını doldurmanız gerekmektedir. Masa oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('points/add') ?>" target="_blank"><?= __('Yeni Masa oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks4.PNG') ?>" class="img-responsive"> 
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <?= __('Kasa Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                        <?= __('Kasa oluştururken, Aşağıdaki resimde görüldüğü gibi kasa ismi, kısa açıklaması ve restoran seç alanlarını doldurmanız gerekmektedir. Kasa oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('pos/add') ?>" target="_blank"><?= __('Yeni Kasa oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks5.PNG') ?>" class="img-responsive">
                    </div>
                </div>
            </div>    
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <?= __('Roller ve Yetkiler') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                    <div class="panel-body">
                        <?= __('Rol oluştururken, işletmenizde bulunan farklı kullanıcı tiplerini oluşturmanız gerekmektedir. Bir sonraki adımda bu oluşturan kullanıcı tiplerine yetkiler tanımlanacaktır. Aşağıdaki resimde görüldüğü gibi rol oluşturuken, rol ismi, aktif/pasif alanlarını doldurmanız gerekmektedir. Rol oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('roles/add') ?>" target="_blank"><?= __('Yeni Rol oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks6.PNG') ?>" class="img-responsive"> 
                        <?= __('Yetki oluşturuken, işletmenize tanımlı kullanıcı tipi ( Rol ) olması gerekmektedir. Aşağıdaki resimde yetki tablosunun bir bölümü gözükmektedir. Burada üstte rol ve altında yetkileri sıralanmıştır. Rol için hangi yetkiler verilecekse 
                        o yetkilerin işaretlenmesi ve sağ altta kaydet butonuna basıp işlemin tamamlanması gerekmektedir. Yetkiler tablosuna buradan ulaşabilirsiniz.') ?>
                        <a href="<?= base_url('roles/permission') ?>" target="_blank"><?= __('Yetkiler Tablosu.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks7.PNG') ?>" class="img-responsive">
                    </div>
                </div>
            </div>        
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSix">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <?= __('Çalışan Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                    <div class="panel-body">
                        <?= __('Çalışan oluştururken, Aşağıdaki resimde görüldüğü gibi çalışanın ismi, hesap ismi, kişisel e-posta adresi (şifre unutulması halinde yeni şifrenin mail adresine gönderilmesi için gerekli) posmaks oturum açma şifresi, pin/ pos oturum şifresi ( Bu şifre oturumlar arası hızlı geçiş için gereklidir ) Rol ve Restotan seçimi alanlarını doldurmanız gerekmektedir. Çalışan  oluşturma
                        sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('employes/add') ?>" target="_blank"><?= __('Çalışan oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks8.PNG') ?>" class="img-responsive">
                    </div>
                </div>
            </div>    
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingSeven">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            <?= __('Ürün Kategorisi / Yeni Ürün Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                    <div class="panel-body">
                        <?= __('Yeni Ürün oluştururken, önce ürünün bağlı olduğu kategori oluşturulur. Resimde görüldüğü gibi Ürün kategorisi ismi, kısa açıklama alanlarını doldurmanız gerekmektedir. Yeni Ürün Kategorisi oluşturma sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('producttypes/add') ?>" target="_blank"><?= __('Yeni Ürün Kategorisi') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks9.PNG') ?>" class="img-responsive">
                        <?= __('Yeni Ürün oluştururken, Resimde görüldüğü gibi Ürün ismi, kısa açıklama, ürünün bağlı olduğu kategori, restoran seçimi, fiyatı, ürün tipi ( Bu bölümde `Ürün` seçeneği seçildiğinde aşağıda o ürünle ilişkilenen stok içerikler çoklu olarak seçilir. Eğer `Menü` seçeneği seçilirse, o menü de bulunacak ürünler çoklu olarak seçilir. )ürün adeti, ürün kutuçuğunun arkaplan rengi ve ürün resmi seç alanlarının doldurulması gerekmektedir. Yeni Ürün oluşturma sekmesine buradan ulaşabilirsiniz.') ?><a href="<?= base_url('products/add') ?>" target="_blank"><?= __('Yeni Ürün') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks10.PNG') ?>" class="img-responsive">
                    </div>
                </div>
            </div> 

               
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingEight">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            <?= __('Demirbaş Oluşturma') ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                    <div class="panel-body">
                        <?= __('Demirbaş oluştururken, Resimde görüldüğü gibi demirbaşın ismi, kısa açıklaması, bulunduğu restorant, seri/barkod kodu, adet ve garanti bitiş tarihi alanlarını doldurmanız gerekmektedir.') ?>
                        <a href="<?= base_url('inventory/add') ?>" target="_blank"><?= __('Demirbaş oluştur.') ?></a>
                        <img src="<?= base_url('assets/sss/posmaks11.PNG') ?>" class="img-responsive">
                    </div>
                </div>
            </div> 
            <!--   
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Raporları Görüntüleme
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>       
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Kat ve Bölge Oluşturma
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>    
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                            Yeni Masa Oluşturma
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>
            --> 

        </div>


        <style type="text/css">
            .sss .panel-group .panel { 
                margin-bottom: 10px;
            }

        </style>
        <style type="text/css">
            .sss .body { font-size: 16px;
            }
        </style>

    </div>
    <!--  -->


</div>
