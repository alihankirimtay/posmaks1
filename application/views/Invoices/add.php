<div class="content">
    
    <div class="row">
        <form action="#" class="form-inline p-y-2 col-sm-12">       
            <div class="form-content">
                <div class="form-group b-1 p-x-2">
                    <label><?= __("Başlık:"); ?></label>
                    <input type="text" name="title" class="form-control" placeholder="<?= __("Başlıksız Şablon"); ?>">
                </div>

                <div class="form-group b-1 p-x-2">
                    <label><?= __("Restoran:"); ?></label>
                    <select name="location_id" class="form-control">
                        <option value=""><?= __("Restoran seç"); ?></option>
                        <?php foreach ($this->user->locations_array as $location): ?>
                            <option value="<?= $location['id'] ?>"><?= $location['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-sm btn-success btn-ripple submit-invoice" data-url="<?= base_url('invoices/add') ?>">
                        <i class="fa fa-floppy-o"></i> <?= __("Kaydet"); ?>
                    </button>
                    <div class="dropdown pull-right p-l-02">
                        <button class="btn btn-sm btn-success btn-ripple dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                            <li>
                                <a href="#" class="submit-invoice small" data-url="<?= base_url('invoices/add') ?>"><?= __("Farklı Kaydet"); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            
                <input type="hidden" name="default" value="0">
                <input type="hidden" name="json" value=''>
            </div>
        </form>
    </div>
    
    <?php $this->load->view('Invoices/elements/invoice_editor'); ?>
    
</div>