<div class="content">
    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok Sayımı Oluştur') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('stockcounts'); ?>"><?= __('Stok Sayımı') ?></a></li>
                    <li><a href="#" class="active"><?= __('Stok Sayımı Oluştur') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Satış Detayları') ?></h4>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-3">
                                    <select name="location_id" class="chosen-select">
                                        <option value=""><?= __('Restoran seç') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                                <option value="<?= $location['id']; ?>" <?= selected($location_id, $location[ 'id' ]) ?>>
                                                    <?= $location['title'] ;?>
                                                </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                           

                            <div class="form-group">
                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                    <div class="col-md-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" name="start_date" class="form-control" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                    <div class="col-md-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" name="end_date" class="form-control datetimepicker-basic" value="<?php ECHO dateConvert($date_to, 'client', dateFormat()); ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div id="stocksInputs" class="col-md-12">
                                    
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('stockcounts'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('stockcounts/add');?>" data-return="<?= base_url('stockcounts/index');?>"><?= __('Raporu Kaydet') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
