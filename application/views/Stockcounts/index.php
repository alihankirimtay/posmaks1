<?php
  
    $start_date = dateConvert2($start_date, 'client', 'Y-m-d H:i:s', dateFormat());
    $end_date = dateConvert2($end_date, 'client', 'Y-m-d H:i:s', dateFormat());
?>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok Sayımı') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Stok Sayımı') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <!-- FILTER -->
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle " type="button" id="dropdownFilter" data-toggle="dropdown" style="margin-bottom: 2rem; border-radius: 6px;"><?= __('Filtrele') ?> <span class="caret"></span>    
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                    <form action="" class="form-horizontal" role="form">
                        <div class="form-content">
                            <div class="col-xs-12 col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                    <div class="col-md-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" name="d" class="form-control datetimepicker-basic" value="<?= $start_date ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                    <div class="col-md-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?= $end_date ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3"><i class="ion-location"></i> <?= __('Restoran') ?></label>
                                    <div class="col-md-9">
                                        <select name="l" class="chosen-select">
                                            <option value=""><?= __('Seçiniz') ?></option>
                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                <option value="<?= $location['id']; ?>" <?= selected($location_id, $location[ 'id' ]) ?>>
                                                    <?= $location['title'] ;?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                        <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                        <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>

                </ul>
            </div>                        
        </div>
    </div>

     <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Points</h4>
                        <div class="btn-group pull-right">
                            <a id="stokOlustur" href="<?php ECHO base_url('stockcounts/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Stok Sayımı Oluştur') ?></a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('Başlangıç Tarihi') ?></th>
                                    <th><?= __('Bitiş Tarihi') ?></th>
                                    <th><?= __('Oluşturan Kişi') ?></th>
                                    <th><?= __('Oluşturma Tarihi') ?></th>
                                    <th><?= __('Detay') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stock_counts as $stock): ?>
                                    <tr>
                                        <td><?= dateConvert($stock->start_date, 'client'); ?></td>
                                        <td><?= dateConvert($stock->end_date, 'client'); ?></td>
                                        <td><?= $stock->user_name; ?></td>
                                        <td><?= dateConvert($stock->created, 'client'); ?></td>
                                        <td><a href="<?= base_url('stockcounts/view/'.$stock->id); ?>"><button class="btn btn-success"><?= __('Raporu Görüntüle') ?></button></a> <a href="<?= base_url('stockcounts/edit/'.$stock->id) ?>"><button class="btn btn-primary" id="reportEdit"><?= __('Düzenle') ?></button></a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>