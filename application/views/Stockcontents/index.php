<?php
    
    $total = function($content)
    {
        return ($content->price + $content->price * $content->tax / 100) * $content->quantity;
    };
    
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Stok İşlemleri') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?php ECHO base_url('stocks'); ?>"><?= __('Stoklar') ?></a></li>
                    <li><a href="#" class="active"><?= $current_stock->title ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Stok İşlemleri') ?></h4>
                        <div class="btn-group pull-right">
                            <!-- <a href="<?php ECHO base_url('stockcontents/add/' . $current_stock->id); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Stok İşlemi Oluştur</a> -->
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('Miktar') ?></th>
                                    <th><?= __('Tutar') ?></th>
                                    <th><?= __('İşlemi Yapan') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                   <!--  <th>İşlem</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stock_contents as $stock_content): ?>
                                    <tr>
                                        <td><?= $stock_content->quantity ?></td>
                                        <td><?= number_format($total($stock_content), 2) ?> </td>
                                        <td><?= $stock_content->user ?> </td>
                                        <td><?= dateConvert($stock_content->created, 'client', dateFormat())?></td>
                                       <!--  <td>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('stockcontents/edit/'.$current_stock->id.'/'.$stock_content->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('stockcontents/delete/'.$current_stock->id.'/'.$stock_content->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td> -->
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>