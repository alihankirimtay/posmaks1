<?php 
    $stock_number = 1;
    $selectedCustomer = function($customer, $selected_customer) {
        if ($customer == $selected_customer) {
            return 'selected="selected"';
        }  
            
    };

    $temp_transaction = (object) [
        'bank_title' => "",
        'pos_title' => "",
        'id' => null,
        'bank_id' => null,
        'pos_id' => null,
        'customer_id' => null,
        'amount' => 0,
        'desc' => "",
        'related_transaction_id' => null,
        'date' => null,
        'created' => null,
        'modified' => null,
        'increment' => 0
    ];

    $payment_div = $paid_div = $paid_html ="";

    if (isset($transaction)) {

        $payment_div = "hidden";
        $temp_transaction = $transaction;

        if ($transaction->bank_id) {

            $paid_html = $transaction->bank_title ." Hesabı";

        } else if ($transaction->pos_title != null) {

            $paid_html = $transaction->pos_title ." Hesabı";

        }

    } else {
        $paid_div = "hidden";
    }

?>      
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1> <?= __("Giderler Düzenle"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("İşlemler"); ?></a></li>
                    <li><a href="<?= base_url('expenses'); ?>"><?= __("Giderler"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Giderler Düzenle"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Gider Detayları"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Tür"); ?></label>
                                <div class="col-md-9">
                                    <select name="expense_type_id" class="chosen-select">
                                        <option value=""><?= __("Tür seç"); ?></option>
                                        <?php foreach ($expense_types as $expense_type): ?>
                                            <option value="<?= $expense_type[ 'id' ]; ?>" <? selected($expense_type[ 'id' ], $expense->expense_type_id) ?>><?= $expense_type[ 'title' ] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <option value=""><?= __("Restoran seç"); ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <? selected($location[ 'id' ], $expense->location_id) ?>><?= $location['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Tedarikçi"); ?></label>
                                <div class="col-md-9">
                                    <select name="customer_id" class="chosen-select">
                                        <?php foreach ($customers as $customer): ?>
                                            <option value="<?php ECHO $customer['id']; ?>"<?= $selectedCustomer($customer, $expense->customer_id) ?>><?php ECHO $customer['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Fiş/Fatura Tarihi"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="receipt_date" class="form-control datetimepicker-basic" value="<?= dateConvert($expense->receipt_date, 'client', dateFormat()) ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group paid-div <?= $paid_div ?>">
                                <label class="control-label col-md-3"><?= __("Ödeme Durumu"); ?></label> 
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <span class="title text-green paid-span"><?= $temp_transaction->amount . "₺ Ödendi, " .$temp_transaction->date ." - " .$paid_html  ?></span>
                                            <button type="button" class="btn btn-danger btn-ripple btn-xs delete-transaction pull-right" data-id="<?= $temp_transaction->id ?>" data-customer="<?= $expense->customer_id ?>" data-expense="<?= $expense->id ?>"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="payment-div <?= $payment_div ?>">

                                <div class="form-group">
                                    <label class="control-label col-md-3"><?= __("Ödeme Durumu"); ?></label>
                                    <div class="btn-group col-md-6 btn-background" data-toggle="buttons">
                                        <a href="#stepPayable" class="btn btn-danger active col-md-3 m-r-3" data-toggle="tab">
                                            <input type="radio" name="paid-type" value="payable" checked="checked" autocomplete="off"/><?= __("Ödenecek"); ?>
                                        </a>
                                        <a href="#stepPaid" class="btn btn-primary col-md-3" data-toggle="tab">
                                            <input type="radio" name="paid-type" value="paid" autocomplete="off"/><?= __("Ödendi"); ?>                            
                                        </a>
                                    </div>

                                </div>

                                <div class="form-group">

                                    <div class="tab-content tab-content-0">
                                        <div class="tab-pane active" id="stepPayable">
                                            <label class="col-md-3 control-label"><?= __("Ödeme Tarihi"); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="payable_date" class="form-control datetimepicker-basic" value="<?= dateConvert($expense->payment_date, 'client', dateFormat()) ?>"/>
                                            </div>   
                                        </div>

                                        <div class="tab-pane" id="stepPaid">
                                            <label class="col-md-3 control-label"><?= __("Ödeme Tarihi"); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="paid_date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d h:i:s'), 'client', dateFormat()) ?>"/>  
                                            </div>
                                            <div class="col-md-offset-3 col-md-3 m-t-2">
                                                <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                                                    <a href="#stepPaymentBank" class="btn btn-primary active" data-toggle="tab">
                                                        <input type="radio" name="account-type" value="bank" checked="checked" autocomplete="off"/><?= __("Bankalar"); ?>
                                                    </a>
                                                    <a href="#stepPaymentPos" class="btn btn-primary" data-toggle="tab">
                                                        <input type="radio" name="account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>
                                                    </a>
                                                </div>
                                            </div>  

                                            <div class="tab-content tab-content-0 col-md-12 m-t-2">
                                                <div class="tab-pane active" id="stepPaymentBank">
                                                    <label class="col-md-3 control-label"><?= __("Banka"); ?></label>
                                                    <div class="col-md-3">
                                                        <select name="bank[id]" class="chosen-select" style="display: none;">
                                                            <option value=""><?= __("Banka Seçiniz"); ?></option>
                                                            <?php foreach ($banks as $bank): ?>
                                                                <option value="<?= $bank['id']; ?>"><?= $bank['title'] ;?></option>
                                                            <?php endforeach; ?>
                                                        </select>                    
                                                    </div>   
                                                </div>

                                                <div class="tab-pane" id="stepPaymentPos">
                                                    <label class="col-md-3 control-label"><?= __("Banka"); ?></label>
                                                    <div class="col-md-3">
                                                        <select name="pos[id]" class="chosen-select" style="display: none;">
                                                            <option value=""><?= __("Kasa Seçiniz"); ?></option>
                                                            <?php foreach ($pos as $single_pos): ?>
                                                                <option value="<?= $single_pos['id']; ?>"><?= $single_pos['title'] ;?></option>
                                                            <?php endforeach; ?>
                                                        </select>                    
                                                    </div>    
                                                </div>
                                            </div>  
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Tutar"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" class="form-control" name="amount" placeholder="0.00" autocomplete="off" value="<?= number_format($expense->amount, 2) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Vergi"); ?></label>
                                <div class="col-md-9">
                                <select name="tax" class="chosen-select">
                                        <?php foreach ($this->taxes as $key => $tax): ?>
                                            <option value="<?= $key ?>" <?= selected($key, $expense->tax) ?>><?= $tax ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("İsim"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?= $expense->title ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Açıklama"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?= $expense->desc ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="type_id" value="3">


                            <!-- <div class="form-group">
                                <label class="control-label col-md-3">Attachments</label>
                                <div class="col-md-9 note note-warning note-left-striped eUploadAttachments">
                                    <div class="col-xs-12 col-md-12 well well-sm attachments-dropzone">
                                        <center><small><i>Drag/Drop File or Click Here!</i></small></center>
                                    </div>
                                </div>
                            </div> -->

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('expenses'); ?>" class="btn btn-grey"><?= __("İptal"); ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('expenses/edit/' . $expense->id);?>" ><?= __("Kaydet"); ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('expenses/edit/' . $expense->id);?>" data-return="<?= base_url('expenses'); ?>" ><?= __("Kaydet & Çık"); ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
