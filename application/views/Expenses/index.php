<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Giderler"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("İşlemler"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Giderler"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                 <?= __("Filtrele"); ?><span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="" method="get" class="form-horizontal" role="form">
                                    <div class="form-content">

                                        <div class="col-xs-12 col-sm-12 col-md-6">

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= __("Tarih"); ?></label>
                                                <div class="col-md-9">
                                                    <select name="d" class="chosen-select">
                                                        <?php foreach ($this->days as $day): ?>
                                                            <option value="<?= $day['id']; ?>" <?php ECHO (@$_GET['d']==$day['id'] ? 'selected' : ''); ?>>
                                                                <?= $day['title'] ;?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __("Başlangıç"); ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dFrom" class="form-control datetimepicker-basic" value="<?php ECHO @$_GET['dFrom']; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __("Bitiş"); ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?php ECHO @$_GET['dTo']; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= __("Tür"); ?></label>
                                                <div class="col-md-9">
                                                    <select name="t" class="chosen-select">
                                                        <option value="0"><?= __("Hepsi"); ?></option>
                                                        <?php foreach ($expense_types as $expense_type): ?>
                                                            <option value="<?= $expense_type->id; ?>" <?php ECHO (@$_GET['t']==$expense_type->id ? 'selected' : ''); ?>>
                                                                <?= $expense_type->title ;?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                                <div class="col-md-9">
                                                    <select name="l" class="chosen-select">
                                                        <option value="0"><?= __("Hepsi"); ?></option>
                                                        <?php foreach ($this->user->locations_array as $location): ?>
                                                            <option value="<?= $location['id']; ?>" <?php ECHO (@$_GET['l']==$location['id'] ? 'selected' : ''); ?>>
                                                                <?= $location['title'] ;?>
                                                            </option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><?= __("Personel"); ?></label>
                                                <div class="col-md-9">
                                                    <select name="o" class="chosen-select">
                                                        <option value="0"><?= __("Hepsi"); ?></option>
                                                    </select>
                                                </div>
                                            </div>

                                            

                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="col-sm-12 text-right">
                                                    <button type="submit" class="btn btn-primary"><?= __("Uygula"); ?></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </ul>
                        </div>                        
                    </div>
                </div>

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Tüm Giderler"); ?></h4>
                        <div class="btn-group pull-right">
                            <a class="btn btn-primary btn-xs" id="print-expense-button" data-tutorialize="print"><span class="ion-android-print"></span> <?= __("Yazdır"); ?></a>
                            <a class="btn btn-success btn-xs" href="<?php ECHO base_url('cari'); ?>"><span class="fa fa-plus"></span> <?= __("Cari Hesap"); ?></a>
                            <a id="normalGider" href="<?php ECHO base_url('expenses/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __("Gider Oluştur"); ?></a>
                            <a id="stokluGider" href="<?php ECHO base_url('expenses/addwithstock'); ?>" class="btn btn-green btn-xs"><i class="fa fa-plus"></i> <?= __("Faturalı Gider Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-2"><?= __("Tarih"); ?></th>
                                    <th class="col-md-1"><?= __("No."); ?></th>
                                    <th class="col-md-2"><?= __("İsim"); ?></th>
                                    <th class="col-md-1"><?= __("Tür"); ?></th>
                                    <th class="col-md-2"><?= __("Çalışan"); ?></th>
                                    <th class="col-md-1"><?= __("Restoran"); ?></th>
                                    <th class="col-md-1"><?= __("Toplam"); ?></th>
                                    <th class="col-md-2 no-print"><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $footer_total = 0; ?>
                                <?php foreach ($expenses as $expense): ?>
                                    <?php $footer_total += $expense->amount; ?>
                                    <tr>
                                        <td><?= dateConvert($expense->payment_date, 'client', dateFormat()) ?></td>
                                        <td><?php ECHO $expense->id; ?></td>
                                        <td><?php ECHO $expense->title; ?></td>
                                        <td><?php ECHO $expense->type; ?></td>
                                        <td><?php ECHO $expense->user; ?></td>
                                        <td><?php ECHO $expense->location; ?></td>
                                        <td class="text-right"><?php ECHO number_format($expense->amount, 2); ?></td>
                                        <td class="no-print">
                                            <?php if ($expense->has_stock): ?>
                                                <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('expenses/editwithstock/'.$expense->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <?php else: ?>
                                                <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('expenses/edit/'.$expense->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                                <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('expenses/delete/'.$expense->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                            <?php endif ?>

                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= __("Toplam:"); ?></th>
                                    <th><?= number_format($footer_total, 2) ?></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container hidden" id="print-container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <th class="col-md-2"><?= __("Tarih"); ?></th>
                                        <th class="col-md-1"><?= __("No."); ?></th>
                                        <th class="col-md-2"><?= __("İsim"); ?></th>
                                        <th class="col-md-1"><?= __("Tür"); ?></th>
                                        <th class="col-md-1"><?= __("Çalışan"); ?></th>
                                        <th class="col-md-1"><?= __("Restoran"); ?></th>
                                        <th class="col-md-1"><?= __("Toplam"); ?></th>
                                        <th class="col-md-1"><?= __("Ödeme Türü"); ?></th>
                                        <th class="col-md-2 no-print"><?= __("İşlem"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $footer_total = 0; ?>
                                    <?php foreach ($expenses as $expense): ?>
                                        <?php $footer_total += $expense->amount; ?>
                                        <tr class="col-md-12">
                                            <td><?= dateConvert($expense->payment_date, 'client', dateFormat()) ?></td>
                                            <td><?php ECHO $expense->id; ?></td>
                                            <td><?php ECHO $expense->title; ?></td>
                                            <td><?php ECHO $expense->type; ?></td>
                                            <td><?php ECHO $expense->user; ?></td>
                                            <td><?php ECHO $expense->location; ?></td>
                                            <td class="text-right"><?php ECHO number_format($expense->amount, 2); ?></td>
                                            <td><?php ECHO $expense->payment_method; ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                
                                
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><?= __("Toplam:"); ?></th>
                                        <th><?= number_format($footer_total, 2) ?></th>
                                        <th></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
