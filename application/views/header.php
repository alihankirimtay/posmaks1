<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->page_title . ' ' . $this->website->name; ?></title>

    <meta name="description" content="Responsive, content management system.">
    <meta name="author" content="Kodar">

    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-touch-fullscreen" content="yes">

    <!-- BEGIN CORE CSS -->
    <link rel="stylesheet" href="<?= asset_url('panel/css/admin1.css'); ?>">
    <link rel="stylesheet" href="<?= asset_url('globals/css/elements.css'); ?>">
    <!-- END CORE CSS -->

    <!-- BEGIN PLUGINS CSS -->
    
    
    <!-- Home Plugin -->
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/rickshaw/rickshaw.min.css');?>">
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/bxslider/jquery.bxslider.css'); ?>">
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/jasny-bootstrap/dist/css/jasny-bootstrap.min.css'); ?>">
    <!-- Home Plugin -->

    <!-- Datatable Plugin -->
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/datatables/media/css/jquery.dataTables.min.css');?>">
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/datatables/themes/bootstrap/dataTables.bootstrap.css'); ?>">
    <!-- Datatable Plugin -->

    <!-- Choosen Plugin -->
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/chosen/chosen.min.css'); ?>">
    <!-- Choosen Plugin -->

    <!-- bootstrap datetimepicker -->
    <link rel="stylesheet" href="<?= asset_url('globals/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css'); ?>">
    <!-- bootstrap datetimepicker -->


    <!-- JS BASE URL -->
    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
        var user_id  = <?= $this->user->id; ?>;
        var base_date_format  = "<?= dateFormat(true) ?>";
        var number_format = {
            "decimal": "<?= $this->number_format->decimal ?>",
            "thousands": "<?= $this->number_format->thousands ?>"
        };
        var LANG = {
            'Seçim Yapınız' : "<?= __('Seçim Yapınız') ?>",
            'Seçimler Yapınız' : "<?= __('Seçimler Yapınız') ?>",
            'Arama sonucu bulunamadı.' : "<?= __('Arama sonucu bulunamadı.') ?>"
        };
    </script>


    <?php if(isset($this->theme_plugin['css'])): ?>
        <?php foreach ($this->theme_plugin['css'] as $key => $css): ?>
            <link rel="stylesheet" type="text/css" href="<?= asset_url($css) ?>">
        <?php endforeach; ?>
    <?php endif; ?>
    <?php foreach ($this->_assets->css as $key => $css): ?>
        <link rel="stylesheet" type="text/css" href="<?= asset_url($css) ?>">
    <?php endforeach; ?>

    <link rel="stylesheet" href="<?= asset_url('globals/css/plugins.css'); ?>">
    <!-- END PLUGINS CSS -->

    <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?= asset_url('globals/img/icons/favicon.ico'); ?>">
    <link rel="apple-touch-icon" href="<?= asset_url('globals/img/icons/apple-touch-icon.png'); ?>">
    <!-- END SHORTCUT AND TOUCH ICONS -->

    <script src="<?= asset_url('globals/plugins/modernizr/modernizr.min.js'); ?>"></script>
</head>
<body class="theme-deep-blue body-back">

    <div class="nav-bar-container dp-none">

        <!-- BEGIN ICONS -->
        <div class="nav-menu">
            <div class="hamburger">
                <span class="patty"></span>
                <span class="patty"></span>
                <span class="patty"></span>
                <span class="patty"></span>
                <span class="patty"></span>
                <span class="patty"></span>
            </div><!--.hamburger-->
        </div><!--.nav-menu-->

        <div class="nav-user">
            <div class="user">
                <img src="<?= ($this->user->image != NULL ? base_url('assets/uploads/profile/'.$this->user->image) : base_url('assets/uploads/profile/no_image.png')); ?>" alt="">
                <span class="badge">0</span>
            </div><!--.user-->
            <div class="cross">
                <span class="line"></span>
                <span class="line"></span>
            </div><!--.cross-->
        </div><!--.nav-user-->
        <!-- END OF ICONS -->

        <div class="nav-bar-border"></div><!--.nav-bar-border-->

        <!-- BEGIN OVERLAY HELPERS -->
        <div class="overlay">
            <div class="starting-point">
                <span></span>
            </div><!--.starting-point-->
            <div class="logo"><?= $this->website->name ?></div><!--.logo-->
        </div><!--.overlay-->

        <div class="overlay-secondary"></div><!--.overlay-secondary-->
        <!-- END OF OVERLAY HELPERS -->

    </div><!--.nav-bar-container-->
