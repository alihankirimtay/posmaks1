<?php 
    
    $date_start = dateConvert2($date_start, 'client', 'Y-m-d H:i:s', dateFormat());

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">

            <div class="panel-heading">
                <div class="panel-title">

                    <div class="dropdown">
                        <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                            <?= __('Filter') ?> <span class="caret"></span>    
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                            <form action="" method="get" class="form-horizontal" role="form">
                                <div class="form-content">

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                            <div class="col-md-9">
                                                <select name="location_id" class="chosen-select">
                                                    <?php foreach ($this->user->locations_array as $location): ?>
                                                        <option value="<?= $location['id']; ?>" <?php selected($location_id, $location['id']); ?>>
                                                            <?= $location['title'] ;?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"><?= __('Başlangıç') ?></label>
                                            <div class="col-md-9">
                                                <div class="inputer">
                                                    <div class="input-wrapper">
                                                        <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= $date_start ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12 text-right">
                                                <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                <input type="hidden" name="tab" value="status">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>

                        </ul>
                    </div>                        
                </div>
            </div>


            <div class="panel-heading">
                <div class="panel-title">
                    

                    <div class="col-md-4" >
                        <div class="card card-iconic card-green">
                            <div class="card-full-icon fa fa-plus-square"></div>

                            <div class="card-body text-left">
                                <i class="card-icon fa fa-plus-square"></i>
                                <h4><?= __('Satış') ?></h4>
                                <p><?= $currency . number_format($total_sale, 2) ?></p>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="card card-iconic card-purple">
                            <div class="card-full-icon fa fa-minus-square"></div>

                            <div class="card-body text-left">
                                <i class="card-icon fa fa-minus-square"></i>
                                <h4><?= __('Gider') ?></h4>
                                <p><?= $currency . number_format($total_expense, 2) ?></p>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="card card-iconic card-red">
                            <div class="card-full-icon fa fa-refresh"></div>

                            <div class="card-body text-left">
                                <i class="card-icon fa fa-refresh"></i>
                                <h4><?= __('İade') ?></h4>
                                <p><?= $currency . number_format($total_refund, 2) ?></p>
                            </div>

                        </div>
                    </div>

                    <?php foreach ($payment_types as $payment_type): ?>
                        <div class="col-md-3" >
                            <div class="card card-iconic card-teal">
                                <div class="card-full-icon <?= $payment_type->icon ?>"></div>
                                <div class="card-body text-left">
                                    <i class="card-icon <?= $payment_type->icon ?>"></i>
                                    <h4><?= $payment_type->title ?></h4>
                                    <p><?= $currency . number_format($payments->{$payment_type->alias}, 2) ?></p>
                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>
					
                </div>
            </div>

            <div class="panel-body">
                <div class="overflow-table">
                    <table class="display datatables-basic">
                        <thead>
                            <tr>
                                <th><?= __('Tarih') ?></th>
                                <th><?= __('No.') ?></th>
                                <th><?= __('İsim') ?></th>
                                <th><?= __('Tür') ?></th>
                                <th><?= __('Personel') ?></th>
                                <th><?= __('Toplam') ?></th>
                                <th><?= __('Ödeme Türü') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($expenses as $expense): ?>
                                <tr>
                                    <td><?= dateConvert($expense->payment_date, 'client', dateFormat()) ?></td>
                                    <td><?= $expense->id ?></td>
                                    <td><?= $expense->title ?></td>
                                    <td><?= $expense->type_title ?></td>
                                    <td><?= $expense->user_name ?></td>
                                    <td><?= $currency . ($expense->amount + $expense->amount * $expense->tax  / 100); ?></td>
                                    <td><?= $expense->payment_type_title ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>