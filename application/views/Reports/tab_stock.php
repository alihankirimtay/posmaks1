<div class="row">
    <div class="col-md-12">

        <div class="panel">
            <div class="panel-body">


                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">
                        
                        <div class="col-xs-4 col-sm-3">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Restoran:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <select name="location" class="form-co1ntrol chosen-select">
                                        <option value="0"><?= __('Tümü') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?php selected(@$location_id, $location['id']) ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-3">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Başlangıç Tarihi:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_from" class="form-control datetimepicker-basic" value="<?= $date_client_from ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-3">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Bitiş Tarihi:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_to" class="form-control datetimepicker-basic" value="<?= $date_client_to ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-2 hidden-xs">
                            <div class="form-group">
                                <div class="col-md-9 col-xs-10">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    <input type="hidden" name="tab" value="stock">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>


        <div class="col-xs-12 col-sm-12">
            <div class="list-group">
                <a href="#" class="list-group-item active"><?= __('Malzemeler/Ürünler') ?></a>
                <div class="list-group-item">
                    <!-- <h5 class="list-group-item-heading">Total Overall Sales</h5> -->
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th><?= __('Malzeme/Ürün Adı') ?></th>
                                <th><?= __('Harcanan Miktar') ?></th>
                                <th><?= __('Toplam Stok Girişi') ?></th>
                                <th><?= __('Eldeki Güncel Miktar') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stocks as $stock): ?>
                                <?php $unit = ucfirst(@$this->units[$stock['unit']]); ?>
                                <tr>
                                    <td><?= $stock['title'] ?></td>
                                    <td><?= (int) @$output[$stock['id']] ?> <b><?= $unit ?></td>
                                    <td><?= (int) @$input[$stock['id']] ?> <b><?= $unit ?></b></td>
                                    <td><?= $stock['quantity'] ?> <b><?= $unit ?></b></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        
    </div>
</div>