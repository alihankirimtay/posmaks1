<?php 

    $date_start = dateConvert2($date_start, 'client', 'Y-m-d H:i:s', dateFormat());
    $date_end = dateConvert2($date_end, 'client', 'Y-m-d H:i:s', dateFormat());

    $sold_product_quantity = 0;
    $sold_product_amount = 0;
    $returned_product_quantity = 0;
    $returned_product_amount = 0;

?>
<div class="row">

    <div class="col-md-12">
        <!-- FILTER -->
        <div class="dropdown">
            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                <?= __('Filtrele') ?> <span class="caret"></span>    
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">

                        <div class="col-xs-12 col-sm-12 col-md-6">

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= $date_start ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_end" class="form-control datetimepicker-basic" value="<?= $date_end ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-location"></i> <?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?= selected($location_id, $location[ 'id' ]) ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                    <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    <input type="hidden" name="tab" value="home">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </ul>
        </div>                        
    </div>
    <!-- FILTER -->


    <!-- Sales -->
    <div class="col-md-12">
        <div class="panel green">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h4>Restoran: <?= $this->user->locations_array[$location_id]['title'] ?></h4>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                            <a class="btn btn-primary btn-xs" id="print-dashboard" ><span class="ion-android-print"></span> <?= __('Yazdır') ?></a> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body p-a-0">
                <div class="col-md-12 p-a-0">
                    <div class="card card-iconic card-green">
                        <div class="card-full-icon fa fa-try"></div>

                        <div class="card-body text-left">                            
                            <div class="col-xs-4">
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('Satış') ?></h2></div>
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?=$currency . number_format($total_sale, 2)?></h2></div>
                            </div>
                            
                            <div class="col-xs-4">
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('İade') ?></h2></div>
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?=$currency . number_format($total_refund, 2)?></h2></div>
                            </div>

                            <div class="col-xs-4 m-b-3">
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('İkram') ?></h2></div>
                                <div class="col-xs-12 text-center"><h2 class="text-white"><?=$currency . number_format($total_gift, 2)?></h2></div>
                            </div>

                            <div class="col-xs-12 bg-dark-green m-y-3">
                                <?php foreach ($payment_types as $payment_type): ?>
                                    <div class="col-xs-2 text-center">
                                        <span><?= $payment_type->title ?> <br/> <?= $currency . number_format($payments->{$payment_type->alias}, 2) ?></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Sales -->
    
    <!-- Categorized Products -->
    <div class="col-md-12">
        <div class="panel blue-grey">
            <div class="panel-heading">
                <div class="panel-title">
                    <div class="col-xs-8">
                        <h4><p><?= __('Kategoriler') ?></p></h4>
                    </div>
                    <div class="col-xs-2 text-right">
                        <h4><?= __('Miktar') ?></h4>
                    </div>
                    <div class="col-xs-2 text-right">
                        <h4><?= __('Toplam') ?></h4>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                <!-- Main Categories -->
                <div class="panel-group" id="accordion_main_cat" role="tablist" aria-multiselectable="true">
                    <?php foreach ($main_categories as $main_category): ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading_main_cat_<?= $main_category->id ?>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion_main_cat" href="#collapse_main_cat_<?= $main_category->id ?>" aria-expanded="true" aria-controls="collapseOne">
                                        <div class="col-xs-8 p-b-1">
                                        <?= $main_category->title ?>
                                        </div>
                                        <div class="col-xs-2 text-right">
                                            <?= $main_category->count ?>
                                        </div>
                                        <div class="col-xs-2 text-right">
                                            <?= number_format($main_category->amount, 2) ?>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_main_cat_<?= $main_category->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_main_cat_<?= $main_category->id ?>">
                                <div class="panel-body p-y-0">

                                    <!-- Sub Categories -->
                                    <div class="panel-group" id="accordion_sub_cat" role="tablist" aria-multiselectable="true">
                                        <?php foreach ($main_category->subCategories as $sub_category): ?>
                                            <div class="panel panel-info">
                                                <div class="panel-heading" role="tab" id="heading_sub_cat_<?= $sub_category->id ?>">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion_sub_cat" href="#collapse_sub_cat_<?= $sub_category->id ?>" aria-expanded="true" aria-controls="collapseOne">
                                                            <div class="col-xs-8 p-b-1">
                                                                <?= $sub_category->title ?>
                                                            </div>
                                                            <div class="col-xs-2 text-right">
                                                                <?= $sub_category->count ?>
                                                            </div>
                                                            <div class="col-xs-2 text-right">
                                                                <?= number_format($sub_category->amount, 2) ?>
                                                            </div>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_sub_cat_<?= $sub_category->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_sub_cat_<?= $sub_category->id ?>">
                                                    <div class="panel-body p-a-0">

                                                        <!-- Products -->
                                                        <table class="table table-bordered table-striped table-condensed">
                                                            <thead>
                                                                <tr>
                                                                    <th><?= __('Ürün İsmi') ?></th>
                                                                    <th class="text-right"><?= __('Fiyat') ?></th>
                                                                    <th class="text-right"><?= __('Miktar') ?></th>
                                                                    <th class="text-right"><?= __('Toplam') ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php foreach ($sub_category->products as $product): ?>
                                                                    <tr>
                                                                        <td><?= $product->title ?></td>
                                                                        <td class="text-right"><?= number_format(addTax($product->price, $product->tax), 2) ?></td>
                                                                        <td class="text-right"><?= $product->count ?></td>
                                                                        <td class="text-right"><?= number_format($product->amount, 2) ?></td>
                                                                    </tr>
                                                                <?php endforeach; ?>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>

                                            </div>
                                        <?php endforeach; ?>
                                    </div>

                                </div>
                            </div>
            
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </div>
    <!-- END - Categorized Products -->

    <!-- Product Sales -->
    <div class="col-md-12">
        <div class="panel blue-grey">
            <div class="panel-heading">
                <div class="panel-title"><h4><?= __('Ürünler') ?></h4></div>
            </div><!--.panel-heading-->
            <div class="panel-body">
                <div class="overflow-table">
                    <table class="display datatables-basic">
                        <thead>
                            <tr>
                                <th class="cols-xs-6"><?= __('Ürün İsmi') ?></th>
                                <th class="col-xs-2"><?= __('Fiyat') ?></th>
                                <th class="col-xs-2"><?= __('Miktar') ?></th>
                                <th class="col-xs-2"><?= __('Toplam') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($main_categories as $main_category): ?>
                                <?php $sold_product_quantity += $main_category->count; ?>
                                <?php $sold_product_amount += $main_category->amount; ?>
                                <?php foreach ($main_category->subCategories as $sub_category): ?>
                                    <?php foreach ($sub_category->products as $product): ?>
                                        <tr>
                                            <td><?= $product->title ?></td>
                                            <td><?= number_format(addTax($product->price, $product->tax), 2) ?></td>
                                            <td><?= $product->count ?></td>
                                            <td><?= number_format($product->amount, 2) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <table class="table">
                    <tfoot>
                        <tr>
                            <th class="cols-xs-6"></th>
                            <th class="col-xs-2"><?= __('Toplam:') ?></th>
                            <th class="col-xs-2"><?= $sold_product_quantity ?></th>
                            <th class="col-xs-2"><?= number_format($sold_product_amount, 2) ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- Product Sales -->

    <!-- Product Refunds -->
    <div class="col-md-12">
        <div class="panel blue-grey">
            <div class="panel-heading">
                <div class="panel-title"><h4><?= __('İade Ürünler') ?></h4></div>
            </div><!--.panel-heading-->
            <div class="panel-body">
                <div class="table-responsive" style="max-height: 400px;">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th class="cols-xs-6"><?= __('Ürün İsmi') ?></th>
                                <th class="col-xs-2 text-right"><?= __('Fiyat') ?></th>
                                <th class="col-xs-2 text-right"><?= __('Miktar') ?></th>
                                <th class="col-xs-2 text-right"><?= __('Toplam') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($main_categories as $main_category): ?>
                                <?php $returned_product_quantity += $main_category->return_count; ?>
                                <?php $returned_product_amount += $main_category->return_amount; ?>
                                <?php foreach ($main_category->subCategories as $sub_category): ?>
                                    <?php foreach ($sub_category->products as $product): ?>
                                        <?php if ($product->return_count): ?>
                                            <tr>
                                                <td><?= $product->title ?></td>
                                                <td class="text-right"><?= number_format(addTax($product->price, $product->tax), 2) ?></td>
                                                <td class="text-right"><?= $product->return_count ?></td>
                                                <td class="text-right"><?= number_format($product->return_amount, 2) ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <table class="table">
                    <tfoot>
                        <tr>
                            <th class="cols-xs-6"></th>
                            <th class="col-xs-2 text-right"><?= __('Toplam:') ?></th>
                            <th class="col-xs-2 text-right"><?= $returned_product_quantity ?></th>
                            <th class="col-xs-2 text-right"><?= number_format($returned_product_amount, 2) ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- Product Refunds -->

</div>            
</div>



<!-- PRINT MODAL -->
<div class="modal fade" id="print-dashboard-modal">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">&nbsp;</h4>
        </div>
        <div class="modal-body" >
            
                <!-- START -->
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->user->locations_array[$location_id]['title'] ?>
                    </div>
                    <div class="col-md-6 text-right">
                        <?= $date_start ?><br>
                        <?= $date_end ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td><strong><?= __('Satışlar') ?></strong></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('Satış:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($total_sale, 2)?></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('İade:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($total_refund, 2)?></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('İkram:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($total_gift, 2)?></td>
                                            </tr>

                                            <?php foreach ($payment_types as $payment_type): ?>
                                                <tr>
                                                    <td><?= $payment_type->title ?>:</td>
                                                    <td class="text-right"><?= $currency . number_format($payments->{$payment_type->alias}, 2) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END -->

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
            <button type="button" class="btn btn-primary" id="print-dashboard2"><span class="ion-android-print"></span> <?= __('Yazdır') ?></button>
        </div>
    </div>
</div>