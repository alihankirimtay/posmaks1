<?php 
    
    $date_start = dateConvert2($date_start, 'client', 'Y-m-d H:i:s', dateFormat());
    $date_end = dateConvert2($date_end, 'client', 'Y-m-d H:i:s', dateFormat());
    $totals = (Object) [
        'total' => 0,
        'exists' => 0,
        'over_short' => 0,
        'cash_opening' => 0,
    ];

    foreach ($payment_types as $payment_type) {
        $totals->{$payment_type->alias} = 0;
        $totals->{"{$payment_type->alias}_exists"} = 0;
    }

    $calculate = function ($session) use (&$totals, $payment_types) {

        $session->total = 0;
        $session->total_exists = 0;
        $session->over_short = 0;
        $session->label = null;
        $session->color = null;
        $session->disabled_actions = null;
        $session->values = (Object) json_decode($session->values);

        foreach ($payment_types as $payment_type) {

            $totals->{$payment_type->alias} += $session->{$payment_type->alias};
            $totals->{"{$payment_type->alias}_exists"} = $session->{"{$payment_type->alias}_exists"};

            $session->total += $session->{$payment_type->alias};
            $session->total_exists += $session->{"{$payment_type->alias}_exists"};
        }
        $session->over_short = $session->total_exists - $session->total - $session->cash_opening;

        $totals->total += $session->total;
        $totals->exists += $session->total_exists;
        $totals->over_short += $session->total_exists - $session->total - $session->cash_opening;
        $totals->cash_opening += $session->cash_opening;

        $session->json = json_encode($session);

        if (is_null($session->status)) {
            $session->label = 'Açık';
            $session->color = 'text-red';
            $session->disabled_actions = 'disabled';
        } elseif ($session->status == 1) {
            $session->label = 'Kapalı';
            $session->color = 'text-green';
        } elseif ($session->status == 0) {
            $session->label = 'Kapalı';
            $session->color = 'text-orange';
        }

        if ($session->auto_closed == 1) {
            $session->label .= '<span class="label label-danger">Auto</span>';
        }

        return $session;
    };
?>


    <div class="col-md-12">

        <div class="dropdown">
            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                Filter <span class="caret"></span>    
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">

                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Restoran') ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="chosen-select">
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?php selected($location_id, $location['id']); ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Başlangıç') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= $date_start ?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Bitiş') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_end" class="form-control datetimepicker-basic" value="<?= $date_end ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                    <input type="hidden" name="tab" value="daily">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </ul>
        </div> 


        <div class="panel-body">
            <div class="overflow-table">
                <table class="display datatables-basic">
                    <thead>
                        <tr>
                            <th><?= __('İşlem No') ?></th>
                            <th><?= __('Personel') ?></th>
                            <th><?= __('Başlangıç') ?></th>
                            <th><?= __('Bitiş') ?></th>
                            <?php foreach ($payment_types as $payment_type): ?>
                                <th><?= $payment_type->title ?></th>
                            <?php endforeach; ?>
                            <th><?= __('Açılış') ?></th>
                            <th><?= __('Toplam') ?></th>
                            <th><?= __('Fark') ?></th>
                            <th><?= __('Durum') ?></th>
                            <th><?= __('İşlem') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sessions as $session): ?>
                            <?php $session = $calculate($session); ?>
                            <tr class="<?= $session->color ?>">
                                <td><?= $session->id ?></td>
                                <td><?= $session->user_name ?></td>
                                <td><?= dateConvert($session->created, 'client', dateFormat()) ?></td>
                                <td><?= dateConvert($session->closed, 'client', dateFormat()) ?></td>
                                <?php foreach ($payment_types as $payment_type): ?>
                                    <td>
                                        <?= number_format($session->{"{$payment_type->alias}_exists"}, 2) ?>
                                        <p class="small">Pos: <?= number_format($session->{$payment_type->alias}, 2) ?></p>
                                    </td>
                                <?php endforeach; ?>
                                <td><?= number_format($session->cash_opening, 2) ?></td>
                                <td>
                                    <?= number_format($session->total_exists - $session->cash_opening, 2) ?>
                                    <p class="small">Pos: <?= number_format($session->total, 2) ?></p>
                                </td>
                                <td><?= number_format($session->over_short, 2) ?></td>
                                <td><?= $session->label ?></td>
                                <td>
                                    <a class="btn btn-info btn-xs view" href='<?= $session->json ?>' <?= $session->disabled_actions ?>><i class="fa fa-search"></i></a>
                                    <a class="btn btn-primary btn-xs edit" href="#" data-id="<?= $session->id ?>" <?= $session->disabled_actions ?>><i class="fa fa-pencil"></i></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th class="text-right"><?= __('Toplam:') ?></th>
                            <?php foreach ($payment_types as $payment_type): ?>
                                <td>
                                    <?= number_format($totals->{"{$payment_type->alias}_exists"}, 2) ?>
                                    <p class="small">Pos: <?= number_format($totals->{$payment_type->alias}, 2) ?></p>
                                </td>
                            <?php endforeach; ?>
                            <td><?= number_format($totals->cash_opening, 2) ?></td>
                            <th class="bg-green">
                                <?= number_format($totals->exists - $totals->cash_opening, 2) ?>
                                <p class="small">Pos: <?= number_format($totals->total, 2) ?></p>
                            </th>
                            <th class="bg-orange"><?= number_format($totals->over_short, 2) ?></th>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>





<div class="modal" id="modal-view">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <h3 class="text-center" id="cc-username"></h3>
                <div id="viewContent" class="row text-right moneyInputs">
                    <!-- Content -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Düzenle') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row text-right moneyInputs">
                    <form>
                        <div id="editContent">
                            <!-- Content -->
                        </div>
                        <input type="hidden" name="session_id" value="">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
                <button type="button" class="btn btn-green" id="update_session"><?= __('Kaydet') ?></button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    #modal-view p {
        border: 1px solid black;
        font-size: 3rem;
    }

    .moneyInputs input {
        border: 1px solid silver;
        text-align: right;
        padding-right: 3px;
    }
    .moneyInputs input.form-control[readonly] {
        background-color: #FFEFBE;
    }
</style>