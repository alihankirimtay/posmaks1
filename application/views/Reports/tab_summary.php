<?php 
    $date_client_today_to = dateReFormat($date_client_today_to, 'Y-m-d H:i:s', dateFormat());
?>


<div class="row">
    <div class="col-md-12">

        <div class="panel">
            <div class="panel-body">


                <form action="" method="get" class="form-horizontal" role="form">
                    <div class="form-content">
                        
                        <div class="col-xs-4 col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-xs-12"><?= __('Restoran:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <select name="location" class="form-co1ntrol chosen-select">
                                        <option value="0"><?= __('Tümü') ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>" <?php selected(@$location_id, $location['id']) ?>>
                                                <?= $location['title'] ;?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-3">
                            <div class="form-group hidden">
                                <label class="control-label col-md-3 col-xs-12"><?= __('PAX:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="form-control"><?= $pax_today ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4 col-sm-4">
                            <div>
                                <label class="control-label col-md-3 col-xs-12"><?= __('İşletme Açılışı:') ?> </label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= $date_client_today ?>"/>
                                            <p class="help-block small"><small><?= __('Kapanış Zamanı (24 Saat):') ?> <?= $date_client_today_to ?></small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1 hidden-xs">
                            <div class="form-group">
                                <div class="col-md-9 col-xs-10">
                                    <button type="submit" class="btn btn-primary"><?= __('GÖSTER') ?></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>
		
		
		
		
		
		
		
		<div>
				<div class="panel indigo">
					<div class="panel-heading theme-dark-blue">
						<div class="panel-title">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<h4><?= __('Restoran:') ?> <?= $this->user->locations_array[$location_id]['title'] ?></h4>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
									<a class="btn btn-primary btn-xs btn-ripple" id="print-summary"><span class="ion-android-print"></span> <?= __('Yazdır') ?></a> 
								</div>
							</div>
						</div>
					</div>
                    <div class="panel-body p-a-0">
                        <div class="col-md-12 p-a-0">
                            <div class="card card-iconic card-green">
                                <div class="card-full-icon fa fa-try"></div>

                                <div class="card-body text-left">                            
                                    <div class="col-xs-4">
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('Satış') ?></h2></div>
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= $currency . number_format($sales_today, 2) ?></h2></div>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('İade') ?></h2></div>
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= $currency . number_format($sales_today_return, 2) ?></h2></div>
                                    </div>

                                    <div class="col-xs-4 m-b-3">
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= __('İkram') ?></h2></div>
                                        <div class="col-xs-12 text-center"><h2 class="text-white"><?= $currency . number_format($sales_today_gift, 2) ?></h2></div>
                                    </div>

                                    <div class="col-xs-12 bg-dark-green m-y-3">
                                        <?php foreach ($payment_types as $payment_type): ?>
                                            <div class="col-xs-2 text-center">
                                                <span><?= $payment_type->title ?> <br/> <?= $currency . number_format($payment[$payment_type->alias], 2) ?></span>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
			     </div>
			<div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="panel red">
                            <div class="panel-heading">
                                <div class="panel-title"><h4><?= __('ADİSYONLAR') ?></h4></div>
                            </div>
                            <div class="panel-body" style="height: 300px !important;">
                                <canvas id="adisyonChart" class="chartjs-pie chartjs-charts chartjs"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel pink">
                            <div class="panel-heading">
                                <div class="panel-title"><h4><?= __('ORTALAMA KAZANÇ') ?></h4></div>
                            </div>
                            <div class="panel-body text-center total-alignment">
                                <h1 class="text-pink"><?= $currency . number_format($sale_today_alignment, 2) ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel purple">
                            <div class="panel-heading">
                                <div class="panel-title"><h4><?= __('SATIŞ TÜRLERİ') ?></h4></div>
                            </div>
                            <div class="panel-body text-center " style="height: 300px !important;">
                                <canvas id="paymentTypesChart" class="chartjs-pie chartjs-charts chartjs"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

		
            <!-- VES -->
			
            <div class=" col-xs-12 col-sm-6 col-md-6">
                <div class="list-group">
                    <div class="list-group-item bg-light-blue text-white"><?= __('KATEGORİLER') ?></div>
                    <div class="list-group-item">
                        <div id="piechart-category" style="width: 100%; height: 100%;"></div>
                    </div>
                    <?php $tmp_total = 0; ?>
                    <div class="list-group-item p-a-0 reports-col-scroll">
                        <?php foreach ($categorized_products as $key => $product): ?>
                            <div class="list-group-item">
                                <h5 class="list-group-item-heading"><?= $product[ 'title' ] ?></h5>
                                <p class="list-group-item-text"><?= $currency . number_format($product[ 'total' ], 2) ?></p>
                            </div>
                        <?php $tmp_total += $product[ 'total' ]; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Toplam Satış') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - VES -->

            <!-- PRODUCT SALES -->
            <div class=" col-xs-12 col-sm-3 col-md-6">
                <div class="list-group">
                    <div class="list-group-item bg-teal text-white"><?= __('SATILAN ÜRÜNLER') ?></div>
                    <div class="list-group-item">
                        <div id="piechart-category-sales" style="width: 100%; height: 100%;"></div>
                    </div>
                    <div class="list-group-item reports-col-scroll">
                        <?php $tmp_total = 0; ?>
                        <?php foreach ($categorized_product_sales as $product_sale): ?>
                            <p class="list-group-item-text"><?= $product_sale[ 'title' ] ?>: <span class="pull-right"><?= $currency . number_format($product_sale[ 'total' ], 2) ?></span></p>
                        <?php $tmp_total += $product_sale[ 'total' ]; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Toplam Satış') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - PRODUCT SALES -->

            <!-- ENTRANCE -->
            <div class="col-xs-12 col-sm-3 col-md-6">
                <div class="list-group">
                    <div class="list-group-item bg-blue-grey text-white"><?= __('KASALAR') ?></div>
                    <div class="list-group-item">
                        <div id="piechart-entrance" style="width: 100%; height: 100%;"></div>
                    </div>
                    <?php $tmp_total = 0; ?>
                    <div class="list-group-item p-a-0 reports-col-scroll">
                        <?php foreach ($categorized_entrances as $key => $entrance): ?>
                            <div class="list-group-item">
                                <h5 class="list-group-item-heading"><?= $entrance[ 'title' ] ?></h5>
                                <p class="list-group-item-text"><?= $currency . number_format($entrance[ 'total' ], 2) ?></p>
                            </div>
                        <?php $tmp_total += $entrance[ 'total' ]; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading"><?= __('PPS') ?></h5>
                        <p class="list-group-item-text"><?= $currency . number_format($tmp_total / $pax_today, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - ENTRANCE -->
        <!-- </div> -->


        <!-- <div class="col-md-12"> -->
            <!-- TOTAL SALES -->
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="list-group">
                   
					<div class="list-group-item bg-brown text-white"><?= __('KARŞILAŞTIRMALAR') ?></div>
					
                    <div class="list-group-item">
                        <!-- <h5 class="list-group-item-heading">Total Overall Sales</h5> -->
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th><h6 momentjs="utc2local" datetime="<?= $date_server_today ?>" format="ddd L LT"></h6><?= __('Bugün') ?></th>
                                    <th><h6 momentjs="utc2local" datetime="<?= $date_server_lyear_today ?>" format="ddd L LT"></h6><?= __('Geçen Yıl') ?></th>
                                    <th>Fark</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_today, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_today, 2) ?></td>
                                    <td><?= $currency . number_format($sales_today - $sales_last_year_today, 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Ay') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_month, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year_month, 2) ?></td>
                                    <td><?= $currency . number_format($sales_this_month - $sales_last_year_month, 2) ?></td>
                                </tr>
                            </tbody>
                            <thead>
                                <tr>
                                    <th><?= __('Bu Yıl') ?></th>
                                    <th><?= __('Geçen Yıl') ?></th>
                                    <th><?= __('Fark') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $currency . number_format($sales_this_year, 2) ?></td>
                                    <td><?= $currency . number_format($sales_last_year, 2) ?></td>
                                    <td><?= $currency . number_format($sales_this_year - $sales_last_year, 2) ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END - TOTAL SALES -->
        

            <!-- CAPTURE -->
            <div class="col-xs-12 col-sm-3 col-md-3 hidden">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('CAPTURE %') ?></a>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('PAX:') ?></h5>
                        <p class="list-group-item-text"><?= $pax_today ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('NEGS:') ?></h5>
                        <p class="list-group-item-text"><?= $negs_today ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('CAPTURE %:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($negs_today / $pax_today, 2) ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('People Per Pic.:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($negs_today / $gallery_printed, 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - CAPTURE -->

            <!-- SALES -->
            <div class="col-xs-12 col-sm-3 col-md-3 hidden">
                <div class="list-group">
                    <a href="#" class="list-group-item active"><?= __('SATIŞLAR %') ?></a>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Total Gallery Printed:</h5>
                        <p class="list-group-item-text"><?= number_format($gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item">
                        <h5 class="list-group-item-heading"><?= __('Satış %:') ?></h5>
                        <p class="list-group-item-text"><?= number_format($master_product_sold / $gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Unseen %:</h5>
                        <p class="list-group-item-text"><?= number_format($unseen / $gallery_printed, 2) ?></p>
                    </div>
                    <div class="list-group-item hidden">
                        <h5 class="list-group-item-heading">Persentage of sales from seen image:</h5>
                        <p class="list-group-item-text"><?= number_format($master_product_sold / ($gallery_printed - $unseen), 2) ?></p>
                    </div>
                </div>
            </div>
            <!-- END - SALES -->


            <div class="col-sm-12 text-right small">
                <i><?= 'Compiled in ' . $this->benchmark->elapsed_time() . '<?= __(\'sec.\') ?>' ?></i>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="print-summary-modal">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">&nbsp;</h4>
        </div>
        <div class="modal-body" >
            
                <!-- START -->
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->user->locations_array[$location_id]['title'] ?>
                    </div>
                    <div class="col-md-6 text-right">
                        <?= $date_client_today ?><br>
                        <?= $date_client_today_to ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td><strong><?= __('Satışlar') ?></strong></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('Satış:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($sales_today, 2)?></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('İade:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($sales_today_return, 2)?></td>
                                            </tr>
                                            <tr>
                                                <td><?= __('İkram:') ?></td>
                                                <td class="text-right"><?=$currency . number_format($sales_today_gift, 2)?></td>
                                            </tr>

                                            <?php foreach ($payment_types as $payment_type): ?>
                                                <tr>
                                                    <td><?= $payment_type->title ?>:</td>
                                                    <td class="text-right"><?= $currency . number_format($payment[$payment_type->alias], 2) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END -->

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
            <button type="button" class="btn btn-primary" id="print-summary2"><span class="ion-android-print"></span> <?= __('Yazdır') ?></button>
        </div>
    </div>
</div>
