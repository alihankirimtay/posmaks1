<?php
    
    $totalPrice = 0;
    $totalBonus = 0;
    
?>
<div class="col-md-12">
    <div class="col-md-12">
        <div class="panel blue-grey">

            <div class="panel-heading">
                <div class="panel-title"><h4><?= __('Ürünler') ?></h4></div>
            </div><!--.panel-heading-->
            <div class="panel-body">
                <table class="table table-bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th><?= __('Ürün İsmi') ?></th>
                            <th><?= __('Fiyat') ?></th>
                            <th><?= __('Miktar') ?></th>
                            <th><?= __('Prim Fiyatı') ?></th>
                            <th><?= __('Toplam') ?></th>
                            <th><?= __('Toplam Prim') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(isset($products)): ?>
                            <?php foreach ($products as  $product): ?>
                                <tr>
                                    <td><?php ECHO $product['title']; ?></td>
                                    <td class="numeric"><?= number_format($product['price'], 2) ?></td>
                                    <td class="numeric"><?= $product['quantity'] ?></td>
                                    <td class="numeric"><?= number_format($product['bonus'], 2) ?></td>
                                    <td class="numeric"><?= number_format($product['total'], 2) ?></td>
                                    <td class="numeric"><?= number_format($product['totalbonus'], 2) ?></td>
                                </tr>
                                <?php $totalPrice += $product['total']; ?>
                                <?php $totalBonus += $product['totalbonus']; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?= number_format($totalPrice, 2) ?> ₺</th>
                            <th><?= number_format($totalBonus, 2) ?> ₺</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>