<div class="content">
    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Vardiya Çizelgesi') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Vardiya Çizelgesi') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $location, 'shiftschedule/index/', 'Restoranlar'); ?>
                        <div class="pull-right">
                            <a class="btn btn-primary btn-xl" data-toggle="modal" href='#modal-schedule_reports'><?= __('Rapor') ?></a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <form class="form-inline" action="<?php base_url('shiftschedule/index/' . $location) ?>">
                            <div class="input-group">
                                <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= dateConvert2($dates[1]['start'], 'client', 'Y-m-d H:i:s', dateFormat1()) ?>" placeholder="<?= __('Tarih') ?>">
                            </div>
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary"><?= __('SEÇ') ?></button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel-body" data-tutorialize="shiftschedule">

                    <div class="table-responsive">
                        <table class="table table-hover schedule-table" data-location=<?= $location ?>>
                            <thead>
                                <tr>
                                    <th><?= __('Tarih') ?></th>
                                    <th><?= __('Kullanıcı') ?></th>
                                    <?php for ($i = 0; $i < 60*60*24; $i += 60*30): ?>
                                        <th class="rotate-90"><small><?= date('H:i', $i) ?></small></th>
                                    <?php endfor; ?>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th><?= __('Tarih') ?></th>
                                    <th><?= __('Kullanıcı') ?></th>
                                    <?php for ($i = 0; $i < 60*60*24; $i += 60*30): ?>
                                        <th class="rotate-90"><small><?= date('H:i', $i) ?></small></th>
                                    <?php endfor; ?>
                                </tr>
                            </tfoot>
                            <tbody>
                            
                            <?php $tmp = null; ?>
                            <?php foreach ($dates as $day): ?>
                                <tr>
                                    <td rowspan="<?= $users_count ?>"><?= dateConvert2($day['start'], 'client', 'Y-m-d H:i:s', dateFormat1()) ?></td>
                                </tr>
                                
                                <?php foreach ($users as $user): ?>
                                    <tr class="scedule-click" data-user="<?= $user->id ?>" data-date="<?= $day['start'] ?>">
                                        <td>
                                            <small><?= $user->name ?></small>
                                        </td>
                                        <?php for ($i = 0; $i < 60*60*24; $i += 60*30): ?>
                                            <td>
                                                <?php $date = strtotime('+'.$i.' seconds', strtotime($day['start'])); ?>
                                                <?php foreach ($schedules as $schedule): ?>
                                                    <?php if ($user->id == $schedule->user_id && between( $date, strtotime($schedule->start_date), strtotime($schedule->end_date) )): ?>
                                                        <i class="fa fa-square schedule" data-schedule="<?= $schedule->id ?>"></i>                                                        
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                                <?php if (!isset($tmp[$day['start'] .'_'. $user->id .'_'. $date])): ?>
                                                    <i class="date" data-date="<?= date('Y-m-d H:i:s', $date) ?>"></i>
                                                    <?php $tmp[$day['start'] .'_'. $user->id .'_'. $date] = 1; ?>
                                                <?php endif; ?>
                                            </td>
                                        <?php endfor; ?>
                                    </tr>
                                <?php endforeach; ?>

                            <?php endforeach; ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>



<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-label"><?= __('Vardiya') ?></h4>
            </div>
            <div class="modal-body" style="min-height: 550px;">
                
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true"><?= __('İptal') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-schedule_reports">
    <div class="modal-dialog">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Vardiya Raporu') ?></h4>
            </div>
            <div class="modal-body">
                
                <!-- FORM -->
                <div class="form-container">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Personel') ?></label>
                                <div class="col-md-9">
                                    <select name="user[]" class="chosen-select" multiple>
                                        <?php foreach ($users as $user): ?>
                                            <option value="<?= $user->id ?>" selected>
                                                <?= $user->name ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_start" class="form-control datetimepicker-basic" value="<?= dateConvert2($dates[1]['start'], 'client', 'Y-m-d H:i:s', dateFormat1()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date_end" class="form-control datetimepicker-basic" value="<?= dateConvert2($dates[1]['end'], 'client', 'Y-m-d H:i:s', dateFormat1()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-success create-report"><?= __('Rapor Oluştur') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <!-- END - FORM -->

                <!-- RESULT -->
                <div class="result-container hidden">
                    <button type="button" class="btn btn-success pull-right print-report"><i class="fa fa-print"></i> <?= __('Yazdır') ?></button>
                    <div class="table-responsive">
                        <table class="table table-hover" id="scheduleTable">
                            <thead>
                                <tr>
                                    <th colspan="2" id="table-dates"></th>
                                </tr>
                                <tr>
                                    <th><?= __('Personel') ?></th>
                                    <th><?= __('Çalışma Süresi') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= __('test') ?></td>
                                    <td><?= __('data') ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END - RESULT -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
            </div>
        </div>
    </div>
</div>