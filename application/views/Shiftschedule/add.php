<div class="row">
    <div class="col-md-12">
        <div class="panel">

            <div class="panel-body">
                <form action="#" class="form-horizontal">
                    <div class="form-content">

                        <div class="form-group">
                            <label class="control-label col-md-3"><?= __('Personel') ?></label>
                            <div class="col-md-9">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" class="form-control" value="<?= $user->name ?>" disabled>
                                        <input type="hidden" name="user" value="<?= $user->id ?>">
                                        <input type="hidden" name="location" value="<?= $location ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                            <div class="col-md-9">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="date_start" class="form-control datetimepicker-schedule" value="<?= dateConvert2($date_start, 'client', 'Y-m-d H:i', dateFormat()) ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                            <div class="col-md-9">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="text" name="date_end" class="form-control datetimepicker-schedule" value="<?= dateConvert2($date_end, 'client', 'Y-m-d H:i', dateFormat()) ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="btn-group pull-right">
                                <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('shiftschedule/add');?>" data-success="Shiftschedule._onSuccess()"><?= __('Kaydet') ?></button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>