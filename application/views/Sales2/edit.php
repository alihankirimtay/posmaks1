    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Edit Location <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('locations'); ?>">Locations</a></li>
                    <li><a href="#" class="active">Edit Location</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Location Properties</h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?php ECHO $location->title; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Description</label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?php ECHO $location->desc; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Status</label>
                                <div class="col-md-9">
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active1" <?php checked($location->active, 1); ?> value="1" >
                                        <label for="active1">Active</label>
                                    </div>
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active0" <?php checked($location->active, 0); ?> value="0" >
                                        <label for="active0">Passive</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('locations'); ?>" class="btn btn-grey">Cancel</a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('locations/edit/'.$location->id);?>" >Save</button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('locations/edit/'.$location->id);?>" data-return="<?= base_url('locations'); ?>" >Save & Exit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
