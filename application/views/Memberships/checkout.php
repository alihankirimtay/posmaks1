<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Satın Al"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('memberships') ?>"><?= __("Abonelik Bilgileri"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Satın Al"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Satın Al"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <div id="iyzipay-checkout-form" class="responsive"></div>
                    <?= isset($form['checkoutFormContent']) ? $form['checkoutFormContent'] : $error_message ?>
                </div>

            </div>
        </div>
    </div>

</div>