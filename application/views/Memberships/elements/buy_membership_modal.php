<div class="modal fade" id="modal-buy_membership" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?= __("Paket Seçimini Onaylayın"); ?></h4>
			</div>
			<div class="modal-body" style="max-height: unset;">

				<div class="hidden">
					<a href="#tab_hasCreditCard" data-toggle="tab">hasCreditCard</a>
					<a href="#tab_addNewCreditCard" data-toggle="tab">addNewCreditCard</a>
					<a href="#tab_success" data-toggle="tab">success</a>
				</div>

				<div class="tab-content">
					<strong><?= __("Ürün Bilgileri"); ?></strong>

					<br/>

					<p> <?= __("Paket Adı :"); ?><span id="package_name"></span></p>
					<p><?= __("Paket Tutarı :"); ?> <span id="price"></span></p>
					<p id="DiscountInput"> <?= __("İndirim Kodu :"); ?>
						<input type="text" class="discountCode" name="code">
						<a class="btn btn-success discountBtn"><?= __("Kullan"); ?></a>
					</p>

					<p style="display: none" id="DiscountTotal">
						<?= __("İndirim Tutarı :"); ?> <span id="discount"> 0.00 TL</span> 
						<a class="btn btn-danger cancelDiscount"><?= __("İptal Et"); ?></a>
					</p>
					
				</div>
				
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane" id="tab_hasCreditCard">
						<p><?= __("Yeni paketiniz kayıtlı kredi kartınızdan çekim yapılarak etkinleştirilecektir."); ?></p>
						<div class="well">
							<strong><?= __("Kayıtlı kredi kartınız:"); ?></strong>
							<p id="binNumber">xxxx xxxx xxxx xxxx</p>

						</div>
						<a href="#" class="btn btn-brown btn-ripple btn-block completeOrderWithExistsCard" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Lütfen bekleyin...">
							<?= __("Kayıtlı kartı kullanarak paketi seç"); ?>
						</a>

						<p class="text-center margin-top-20">
							<a href="#tab_addNewCreditCard" data-toggle="tab" class="small"><?= __("Başka bir kredi kartı kullanmak için tıklayın"); ?></a>
						</p>
					</div>

					<div role="tabpanel" class="tab-pane" id="tab_addNewCreditCard">
						<form action="#" class="form-horizontal">
							<div class="form-content">
								<div class="row">
									<?php $this->load->view('Memberships/elements/settings_form_content'); ?>
								</div>

								<a href="#" class="btn btn-brown btn-ripple btn-block completeOrderWithNewCard" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Lütfen bekleyin...">
									<?= __("Posmaks Aboneliğimi Başlat"); ?>
								</a>

							</div>
						</form>
					</div>

					<div role="tabpanel" class="tab-pane text-center" id="tab_success">
						<h1><i class="fa fa-check-circle text-green"></i></h1>
						<p>
							<?= __("Tebrikler"); ?> <strong id="membership_title"></strong> <?= __("başarılı şekilde satın alma işleminiz tamamlandı."); ?><br>
							<?= __("Posmaks aboneliğinizi hemen kullanmaya başlayabilirsiniz."); ?><br>
							<?= __("Posmaks aboneliğiniz sonlandığında kayıtlı kredi kartınız üzerinden aboneliğiniz otomatik olarak yenilenecektir."); ?>
						</p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>