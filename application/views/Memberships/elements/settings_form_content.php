<div class="col-sm-6">
    <div class="panel panel-with-border">
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label"><?= __("Kart Üzerindeki İsim"); ?></label>
                <div class="inputer">
                    <div class="input-wrapper">
                        <input type="text" name="card_holdername" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><?= __("Kredi Kartı Numarası"); ?></label>
                <div class="inputer">
                    <div class="input-wrapper">
                        <input type="text" name="card_number" class="form-control">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><?= __("Son Kullanma Tarihi"); ?></label>
                <div class="row">
                    <div class="col-sm-6">
                        <select name="card_month" class="form-control">
                            <option value=""><?= __("Ay"); ?></option>
                            <?php foreach (range(1, 12) as $month): ?>
                                <option value="<?= $month ?>"><?= sprintf("%02d", $month) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <select name="card_year" class="form-control">
                            <option value=""><?= __("Yıl"); ?></option>
                            <?php foreach (range(date('Y'), date('Y', strtotime('+ 10 year'))) as $year): ?>
                                <option value="<?= $year ?>"><?= $year ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 content-right">
                    <img src="<?= base_url('assets//globals/img/services/iyzico.png') ?>" class="img-responsive" alt="Iyzico">
                </div>
                <div class="col-xs-6 content-left">
                    <img src="<?= base_url('assets//globals/img/services/pci.png') ?>" class="img-responsive" alt="Iyzico">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-sm-6">
    <div class="panel panel-with-border">
        <div class="panel-body">
            <div class="form-group">
                <label class="control-label"><?= __("Firma / Şahıs"); ?></label>
                <select name="bill_type" class="form-control">
                    <option value="firm"><?= __("Firma adına fatura istiyorum"); ?></option>
                    <option value="person"><?= __("Şahısım adına fatura istiyorum"); ?></option>
                </select>
            </div>
            
            <div class="inputs_firm row">
                <div class="form-group col-sm-12">
                    <label class="control-label"><?= __("Ünvan"); ?></label>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_company" class="form-control">
                        </div>
                    </div>
                </div>
            
                <div class="form-group col-sm-6">
                    <label class="control-label"><?= __("Vergi Dairesi"); ?></label>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_tax_office" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label class="control-label"><?= __("Vergi No"); ?></label>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_tax_number" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="inputs_person hidden">
                <div class="form-group">
                    <label class="control-label"><?= __("İsim Soyisim"); ?></label>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_name" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label"><?= __("T.C. Kimlik No"); ?></label>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_identity_number" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><?= __("Adres"); ?></label>
                <div class="inputer">
                    <div class="input-wrapper">
                        <textarea name="bill_address" class="form-control js-auto-size"></textarea>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="inputer">
                    <div class="input-wrapper">
                        <input type="text" name="bill_zip_code" class="form-control" placeholder="<?= __("Posta Kodu"); ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-6">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_district" class="form-control" placeholder="<?= __("İlçe"); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="text" name="bill_city" class="form-control" placeholder="<?= __("İl"); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>