<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Ürün Kategorisi') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ürünler') ?></a></li>
                    <li><a href="#" class="active"><?= __('Ürün Kategorisi') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Ürün Kategorisi') ?></h4>
                        <div class="btn-group pull-right">
                            <a id="yeniUrunKategorisi" href="<?php ECHO base_url('producttypes/add/'); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Yeni Ürün Kategorisi') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('İsim') ?></th>
                                    <th><?= __('Üst Kategori') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                    <th><?= __('Durum') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($producttypes as $producttype): ?>
                                    <tr>
                                        <td><?php ECHO $producttype->title; ?></td>
                                        <td><?= $producttype->parent ? $producttype->parent : '<b><?= __(\'Ana Kategori\') ?></b>' ?></td>
                                        <td><?=dateConvert($producttype->created, 'client', dateFormat())?></td>
                                        <td><?php ECHO get_status($producttype->active); ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('producttypes/edit/'.$producttype->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('producttypes/delete/'.$producttype->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
