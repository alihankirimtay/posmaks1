    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Ürün Kategorisi Düzenle') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ürünler') ?></a></li>
                    <li><a href="<?= base_url('producttypes'); ?>"><?= __('Ürün Kategorisi') ?></a></li>
                    <li><a href="#" class="active"><?= __('Ürün Kategorisi Düzenle') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Ürün Kategorisi Detayları') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Üst Kategori') ?></label>
                                <div class="col-md-9">
                                    <select name="parent_id" class="chosen-select">
                                        <option value="" class="text-bold"><?= __('Ana Kategori') ?></option>
                                        <?php foreach ($types as $type): ?>
                                            <option value="<?= $type['id'] ?>" <?= selected($producttype->parent_id, $type['id']) ?>><?= $type['title'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('İsim') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="title" class="form-control" value="<?php ECHO $producttype->title; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Açıklama') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <textarea name="desc" class="form-control js-auto-size valid"><?php ECHO $producttype->desc; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Kategori Sıralaması') ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="sort" class="form-control" value="<?php ECHO $producttype->sort; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Kategori Resmi"); ?></label>
                                <div class="col-md-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                                        <?php if($producttype->image): ?>
                                            <img src="<?= base_url($producttype->image); ?>" width="100%">
                                        <?php endif; ?>
                                        </div>
                                        <div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileinput-new"><?= __("Resim Seç"); ?></span>
                                                <span class="fileinput-exists"><?= __("Değiştir"); ?></span>
                                                <input type="file" name="category_image">
                                            </span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput"><?= __("Kaldır"); ?></a>
                                            <small class="help-block"><?= __("Dosya en fazla 2MB boyutunda ve Jpg, Png veya Gif uzantılı olmalı."); ?></small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __('Durum') ?></label>
                                <div class="col-md-9">
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active1" <?php checked($producttype->active, 1); ?> value="1" >
                                        <label for="active1"><?= __('Aktif') ?></label>
                                    </div>
                                    <div class="radioer form-inline">
                                        <input type="radio" name="active" id="active0" <?php checked($producttype->active, 0); ?> value="0" >
                                        <label for="active0"><?= __('Pasif') ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('producttypes'); ?>" class="btn btn-grey"><?= __('İptal') ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('producttypes/edit/'.$producttype->id);?>" ><?= __('Kaydet') ?></button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('producttypes/edit/'.$producttype->id);?>" data-return="<?= base_url('producttypes'); ?>" ><?= __('Kaydet & Çık') ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
