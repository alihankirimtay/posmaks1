<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Envanter Geçmişi"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('inventory'); ?>"><?= __("Envanter"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Envanter Geçmişi"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-body">
                    <div class="overflow-table">

                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th><?= __("Başlık"); ?></th>
                                    <th><?= __("Lokasyon"); ?></th>
                                    <th><?= __("Barkod"); ?></th>
                                    <th><?= __("Adet"); ?></th>
                                    <th><?= __("Fiyat"); ?></th>
                                    <th><?= __("Garanti Bit."); ?></th>
                                    <th><?= __("Oluşturulma Tar."); ?></th>
                                    <th><?= __("Durum"); ?></th>
                                    <th><?= __("Güncellenme Tar."); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?= $item->title ?></td>
                                    <td><?= $item->location ?></td>
                                    <td><?= $item->barcode ?></td>
                                    <td><?= $item->quantity ?></td>
                                    <td><?= $item->price ?></td>
                                    <td><?= $item->warranty_exp ? dateReFormat($item->warranty_exp, 'Y-m-d H:i:s', dateFormat()) : ""; ?></td>
                                    <td><?= dateConvert($item->created, 'client', dateFormat()) ?></td>
                                    <td><?= get_status($item->active) ?></td>
                                    <td><b>Geçerli</b></td>
                                </tr>

                                <?php foreach ($histories as $data): ?>
                                    <?php $history = json_decode($data->history); ?>
                                    <tr  class="danger">
                                        <td><?= $history->title ?></td>
                                        <td><?= $history->location ?></td>
                                        <td><?= $history->barcode ?></td>
                                        <td><?= $history->quantity ?></td>
                                        <td><?= $history->price ?></td>
                                        <td><?= $history->warranty_exp ? dateReFormat($history->warranty_exp, 'Y-m-d H:i:s', dateFormat()) : "" ?></td>
                                        <td><?= dateConvert($history->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($history->active) ?></td>
                                        <td><?= dateConvert($history->updated_at, 'client', dateFormat()) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>