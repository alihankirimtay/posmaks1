<?php
    function setAccountType($type) {

        return ($type == "bank") ? "Banka" : "Kasa";

    }
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Kasa Ve Bankalar"); ?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">

                        <div class="col-md-10">
                            <!-- FILTER START -->
                            <div class="dropdown">
                                <button class="btn btn-default btn-xl dropdown-toggle m-b-1" type="button" id="dropdownFilter" data-toggle="dropdown">
                                    <?= __('Filtrele') ?> <span class="caret"></span>    
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                    <form action="" method="get" class="form-horizontal" role="form">
                                        <div class="form-content">

                                            <div class="col-xs-12 col-sm-12 col-md-6">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3"><i class="ion-location"></i> <?= __('Restoran') ?></label>
                                                    <div class="col-md-9">
                                                        <select name="location_id" class="chosen-select">
                                                            <option value="0">Tüm Restoranlar</option>
                                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                                <option value="<?= $location['id']; ?>" <?= selected($location_id, $location[ 'id' ]) ?>>
                                                                    <?= $location['title'] ;?>
                                                                </option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                                        <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                                        <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>

                                </ul>
                            </div>                        
                        </div>
                        <div class="btn-group pull-right">
                            <a id="transferTransaction" class="btn btn-info m-r-1"><?=__("TRANSFER"); ?></a>
                            <a href="<?php ECHO base_url('banks/add/'); ?>" class="btn btn-success"><?= __("BANKA EKLE"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("Hesap İsmi"); ?></th>
                                    <th><?= __("Hesap Tipi"); ?></th>
                                    <th><?= __("İşletme"); ?></th>
                                    <th><?= __("Bakiye"); ?></th>
                                    <th><?= __("İşlemler"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($accounts as $account): ?>
                                <tr data-id="<?= $account->id ?>">
                                    <td><?= $account->title ?></td>
                                    <td><?= setAccountType($account->type) ?></td>
                                    <td><?= $this->user->locations_array[$account->location_id]['title'] ?></td>
                                    <td><span class=""><?= numberFormat($account->amount,2) ?></span></td>
                                    <td>
                                        <?php if($account->type == "bank"): ?>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('banks/edit/'.$account->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <a class="btn btn-info btn-xs" href="<?php ECHO base_url('banks/detail/'.$account->id); ?>"> <i class="fa fa-search"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('banks/delete/'.$account->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        <?php else: ?>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('pos/edit/'.$account->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <a class="btn btn-info btn-xs" href="<?php ECHO base_url('pos/detail/'.$account->id); ?>"> <i class="fa fa-search"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('pos/delete/'.$account->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div id="cari-transfer-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <form action = "#" class="form-horizontal">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><?= __("Transfer"); ?></h4>
            </div>
            <div class="modal-body">   

                <div class="form-group">
                    <label class="text-center"><?= __('Gönderici Hesap') ?></label>
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-2"><?= __("Restoran"); ?></label>
                    <div class="col-md-10">
                        <select name="sender_location_id" data-placeholder="<?= __("Restoran seç"); ?>" class="chosen-select">
                            <?php foreach ($this->user->locations_array as $location): ?>
                                <option value="<?php ECHO $location['id']; ?>"><?php ECHO $location['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                        <a href="#stepSenderBank" class="btn btn-primary active" data-toggle="tab">
                            <input type="radio" name="sender-account-type" value="bank" autocomplete="off"/><?= __("Bankalar"); ?>
                        </a>
                        <a href="#stepSenderPos" class="btn btn-primary" data-toggle="tab">
                            <input type="radio" name="sender-account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>                            
                        </a>
                    </div>

                </div>

                <div class="form-group">

                    <div class="tab-content tab-content-0" style="z-index:999">
                        <div class="tab-pane active" id="stepSenderBank">
                            <label class="col-md-2 control-label"><?= __("Banka"); ?></label>
                            <div class="col-md-10">
                                <select name="bank_sender_id" class="chosen-select" style="display: none;"></select>                    
                            </div>   
                        </div>

                        <div class="tab-pane" id="stepSenderPos">
                            <label class="col-md-2 control-label"><?= __("Kasa"); ?></label>
                            <div class="col-md-10">
                                <select name="pos_sender_id" class="chosen-select" style="display: none;"></select>                    
                            </div>    
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="text-center"><?= __('Alıcı Hesap') ?></label>
                </div> 

                <div class="form-group">
                    <label class="control-label col-md-2"><?= __("Restoran"); ?></label>
                    <div class="col-md-10">
                        <select name="recipient_location_id" data-placeholder="<?= __("Restoran seç"); ?>" class="chosen-select">
                            <?php foreach ($this->user->locations_array as $location): ?>
                                <option value="<?php ECHO $location['id']; ?>"><?php ECHO $location['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                        <a href="#stepRecipientBank" class="btn btn-primary active" data-toggle="tab">
                            <input type="radio" name="recipient-account-type" value="bank" autocomplete="off"/><?= __("Bankalar"); ?>
                        </a>
                        <a href="#stepRecipientPos" class="btn btn-primary" data-toggle="tab">
                            <input type="radio" name="recipient-account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>                            
                        </a>
                    </div>

                </div>

                <div class="form-group">

                    <div class="tab-content tab-content-0">
                        <div class="tab-pane active" id="stepRecipientBank">
                            <label class="col-md-2 control-label"><?= __("Banka"); ?></label>
                            <div class="col-md-10">
                                <select name="bank_recipient_id" class="chosen-select" style="display: none;"></select>                    
                            </div>   
                        </div>

                        <div class="tab-pane" id="stepRecipientPos">
                            <label class="col-md-2 control-label"><?= __("Kasa"); ?></label>
                            <div class="col-md-10">
                                <select name="pos_recipient_id" class="chosen-select" style="display: none;"></select>                    
                            </div>    
                        </div>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Tutar"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="amount" placeholder="0.00" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Açıklama"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="desc" placeholder="Açıklama" autocomplete="off">
                    </div>
                </div>
                <input type="hidden" name="increment" value="0">
            </div>
            <div class="modal-footer">
                <div class ="btn-group-fixed">
                    <button type="button" class="btn btn-success col-md-12 btn-block complete-transfer"><?= __("Transfer Yap"); ?></button>
                    <button type="button" class="btn btn-danger col-md-12 btn-block" data-dismiss="modal"><?= __("Vazgeç"); ?></button>
                </div>
            </div>
        </div>
    </form>

  </div>
</div>
