<?php 
    $dFrom = dateConvert($date['date_from'], 'client', dateFormat());
    $dTo = dateConvert($date['date_to'], 'client', dateFormat());
    function selectedTypes($transaction, $types)
    {
        if(isset($transaction->cancelled) && $transaction->cancelled == 1 && $transaction->amount < 0)
            return "İade";

        foreach ($types as $type) {
            if ($type['id'] == $transaction->type) {
                return $type['title'];
            }
        }
    }
?>
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Cari Hesap"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?php ECHO base_url('cari'); ?>" class="active"><?= __("Cari Hesap"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                    <h1 class="pull-right m-b-0"><?= $customer->name ?></h1>
                    </div>
                    <div class="col-md-10">
                        <!-- FILTER START -->
                        <div class="dropdown">
                            <button class="btn btn-default btn-xl dropdown-toggle" type="button" id="dropdownFilter" data-toggle="dropdown">
                                <?= __('Filtrele') ?> <span class="caret"></span>    
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownFilter" style="width: 100%;">

                                <form action="" method="get" class="form-horizontal" role="form">
                                    <div class="form-content">

                                        <div class="col-xs-12 col-sm-12 col-md-6">

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Başlangıç Tarihi') ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="d" class="form-control datetimepicker-basic" value="<?php ECHO $dFrom ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"><i class="ion-android-calendar"></i> <?= __('Bitiş Tarihi') ?></label>
                                                <div class="col-md-9">
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input type="text" name="dTo" class="form-control datetimepicker-basic" value="<?php ECHO $dTo ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
                                                    <a href="<?php ECHO base_url(); ?>" class="btn btn-default"><?= __('Temizle') ?></a>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                                    <button type="submit" class="btn btn-primary"><?= __('Uygula') ?></button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            </ul>
                        </div>                        
                    </div>
                    <div class="col-md-12">
                        <h3 class="pull-right m-b-0"><?= numberFormat($customer->amount,2); ?>₺</h3>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-6"><?= __("İşlem Tarihi"); ?></th>
                                    <th class="col-md-2"><?= __("İşlem Türü"); ?></th>
                                    <th class="col-md-2"><?= __("Açıklama"); ?></th>
                                    <th class="col-md-2"><?= __("Meblağ"); ?></th>
                                    <th class="col-md-2"><?= __("Bakiye"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($all_transactions as $transaction): ?>
                                <tr>
                                    <td><?= dateConvert($transaction->payment_date, 'client', dateFormat()) ?></td>
                                    <td><?= selectedTypes($transaction, $types) ?></td>
                                    <td><?= $transaction->desc ?></td>
                                    <td><?= ($transaction->increment) ? "+" : "-" ?><?= numberFormat($transaction->amount, 2); ?></td>
                                    <td><?= numberFormat($transaction->customer_amount, 2); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
