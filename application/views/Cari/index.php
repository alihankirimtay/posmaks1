<?php 

$total = $minus_total = $plus_total = 0;

?>

<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Cari Hesap"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Cari Hesap"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Cari Hesap"); ?></h4>
                        <div class="btn-group pull-right">
                            <a href="<?php ECHO base_url('banks/index'); ?>" class="btn btn-red"><?= __("KASALAR VE BANKALAR"); ?></a>
                            <a href="#" class="btn btn-primary open-payment-modal"><?= __("ÖDEME"); ?></a>
                            <a href="#" class="btn btn-success open-receipt-modal"><?= __("TAHSİLAT EKLE"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-3"><?= __("Ad Soyad"); ?></th>
                                    <th class="col-md-3"><?= __("Bakiye"); ?></th>
                                    <th class="col-md-6"><?= __("İşlemler"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($customers as $customer): ?>
                                <?php 
                                    $total += $customer->amount;

                                    if($customer->amount > 0) {
                                        $plus_total += $customer->amount; 
                                    } else {
                                        $minus_total += $customer->amount; 
                                    }
                                ?>
                                    <tr data-id="<?= $customer->id ?>">
                                        <td><?= $customer->name ?></td>
                                        <td><span class=""><?= numberFormat($customer->amount,2) ?></span> <?= ($customer->amount < 0) ? "( Ödenecek )" : "" ?></td>
                                        <td>
                                            <a class="btn btn-blue" href="<?php ECHO base_url('cari/customerDetail/'.$customer->id); ?>">İŞLEM GEÇMİŞİ</a>
                                            <a class="btn btn-danger" href="<?php ECHO base_url('expenses/addwithstock/'.$customer->id); ?>">ALIŞ FİŞ/FATURASI OLUŞTUR</a>
                                            <a class="btn btn-success open-payment-modal" href="#">ÖDEME EKLE</a>
                                            <a class="btn btn-primary open-receipt-modal" href="#">TAHSİLAT EKLE</a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th><?= __("Toplam Alacak : "); ?><?= numberFormat($plus_total,2); ?></th>
                                    <th><?= __("Toplam Ödenecek : "); ?><?= numberFormat($minus_total,2); ?></th>
                                    <th><?= __("Kalan Bakiye : "); ?><?= numberFormat($total,2); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div id="cari-add-payment" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <form action = "#" class="form-horizontal">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= __("Ödeme Ekle"); ?></h4>
            </div>
            <div class="modal-body">    
                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Ünvan"); ?></label>
                    <div class="col-md-10">
                        <select name="customers" class="chosen-select" style="display: none;"></select>                    
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2"><?= __("Restoran"); ?></label>
                    <div class="col-md-10">
                        <select name="location_id" data-placeholder="<?= __("Restoran seç"); ?>" class="chosen-select location">
                            <?php foreach ($this->user->locations_array as $location): ?>
                                <option value="<?php ECHO $location['id']; ?>"><?php ECHO $location['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Ödeme Tarihi"); ?></label>
                    <div class="col-md-10">
                        <input type="text" name="date" class="form-control datetimepicker-basic valid" value="<?= dateConvert(date('Y-m-d h:i:s'), 'client', dateFormat()) ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                        <a href="#stepPaymentBank" class="btn btn-primary active" data-toggle="tab">
                            <input type="radio" name="account-type" value="bank" autocomplete="off"/><?= __("Bankalar"); ?>
                        </a>
                        <a href="#stepPaymentPos" class="btn btn-primary" data-toggle="tab">
                            <input type="radio" name="account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>                            
                        </a>
                    </div>

                </div>

                <div class="form-group">

                    <div class="tab-content tab-content-0">
                        <div class="tab-pane active" id="stepPaymentBank">
                            <label class="col-md-2 control-label"><?= __("Banka"); ?></label>
                            <div class="col-md-10">
                                <select name="bank[id]" class="chosen-select" style="display: none;"></select>                    
                            </div>   
                        </div>

                        <div class="tab-pane" id="stepPaymentPos">
                            <label class="col-md-2 control-label"><?= __("Kasa"); ?></label>
                            <div class="col-md-10">
                                <select name="pos[id]" class="chosen-select" style="display: none;"></select>                    
                            </div>    
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Tutar"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="amount" placeholder="0.00" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Açıklama"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="desc" placeholder="Açıklama" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class ="btn-group-fixed">
                    <button type="button" class="btn btn-success col-md-12 btn-block complete-transaction" data-type="payment"><?= __("ÖDEME EKLE"); ?></button>
                    <button type="button" class="btn btn-danger col-md-12 btn-block" data-dismiss="modal"><?= __("VAZGEÇ"); ?></button>
                </div>
            </div>
            <input type="hidden" name="increment" value="0">
            <input type="hidden" name="type_id" value="1">

        </div>
    </form>

  </div>
</div>

<div id="cari-add-receipt" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <form action = "#" class="form-horizontal">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= __("Tahsilat Ekle"); ?></h4>
            </div>
            <div class="modal-body">    
                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Ünvan"); ?></label>
                    <div class="col-md-10">
                        <select name="customers" class="chosen-select" style="display: none;"></select>                    
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2"><?= __("Restoran"); ?></label>
                    <div class="col-md-10">
                        <select name="location_id" data-placeholder="<?= __("Restoran seç"); ?>" class="chosen-select location">
                            <?php foreach ($this->user->locations_array as $location): ?>
                                <option value="<?php ECHO $location['id']; ?>"><?php ECHO $location['title'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Ödeme Tarihi"); ?></label>
                    <div class="col-md-10">
                        <input type="text" name="date" class="form-control datetimepicker-basic valid" value="<?= dateConvert(date('Y-m-d h:i:s'), 'client', dateFormat()) ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="btn-group btn-group-justified btn-background" data-toggle="buttons">
                        <a href="#stepReceiptBank" class="btn btn-primary active" data-toggle="tab">
                            <input type="radio" name="account-type" value="bank" autocomplete="off"/><?= __("Bankalar"); ?>
                        </a>
                        <a href="#stepReceiptPos" class="btn btn-primary" data-toggle="tab">
                            <input type="radio" name="account-type" value="pos" autocomplete="off"/><?= __("Kasalar"); ?>                            
                        </a>
                    </div>

                </div>

                <div class="form-group">

                    <div class="tab-content tab-content-0">
                        <div class="tab-pane active" id="stepReceiptBank">
                            <label class="col-md-2 control-label"><?= __("Banka"); ?></label>
                            <div class="col-md-10">
                                <select name="bank[id]" class="chosen-select" style="display: none;"></select>                    
                            </div>   
                        </div>

                        <div class="tab-pane" id="stepReceiptPos">
                            <label class="col-md-2 control-label"><?= __("Kasa"); ?></label>
                            <div class="col-md-10">
                                <select name="pos[id]" class="chosen-select" style="display: none;"></select>                    
                            </div>    
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Tutar"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="amount" placeholder="0.00" autocomplete="off">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"><?= __("Açıklama"); ?></label>
                    <div class="col-md-10">
                        <input type="text" class="form-control" name="desc" placeholder="Açıklama" autocomplete="off">
                    </div>
                </div>
                <input type="hidden" name="increment" value="1">
                <input type="hidden" name="type_id" value="2">
            </div>
            <div class="modal-footer">
                <div class ="btn-group-fixed">
                    <button type="button" class="btn btn-success col-md-12 btn-block complete-transaction" data-type="receipt"><?= __("TAHSİLAT EKLE"); ?></button>
                    <button type="button" class="btn btn-danger col-md-12 btn-block" data-dismiss="modal"><?= __("Vazgeç"); ?></button>
                </div>
            </div>
        </div>
    </form>

  </div>
</div>
