<div class="container-fluid tab-body">
	<div class="col-md-12 col-xs-12 nav-row">
		<div class="col-md-3 col-sm-3 col-xs-2 p-10 pl-0">      
            <img id="backOrderTab" class="icon-svg-size" src="<?= asset_url('left.svg'); ?>"></img>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-8 text-center p-10 pl-0">
			<span id="tableNameInterimPayment" class="home-table text-center" style="color:white;"><?= __("MASA ADI"); ?></span>    
		</div>

		<div class="col-md-3 col-sm-3 col-xs-2 p-10 text-right">
            <img id="backHomePageInterimPayment" class="icon-svg-size" src="<?= asset_url('home-01.svg'); ?>"></img>
		</div>
	</div> <!-- col-md-12 -->

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bb-0 h-50 bb-3">
		<ul class="nav nav-tabs b-0" id="interim-nav">
			<li class="active" id="firstTabHistoryPayment"><a  href="#1" data-toggle="tab"><?= __("ARA ÖDEME"); ?></a></li>
			<li><a href="#2" data-toggle="tab"><?= __("GEÇMİŞ"); ?></a></li>
		</ul>
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
		<div class="tab-content p-0 b-0">
			<div class="tab-pane active" id="1">
					<table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table table-hover" style="margin-top: 0 !important; margin-bottom: -7px !important;">
						<thead>
							<tr>
								<th class="active col-sm-3"><?= __("Adet"); ?></th>
								<th class="active col-sm-6"><?= __("Ürün"); ?></th>
								<th class="active col-sm-3"><?= __("Fiyat"); ?></th>
							</tr>
							<tr class="interim_payments-info">
								<th colspan="3" class="danger" style="text-align: center;"><?= __("Ödemesi Yapılmayan Siparişler"); ?></th>
							</tr>
						</thead>
					</table>
				<div class="table-scroll">
					<table id="interimPayment" class="table table-hover interim_payments-unpaid mt-0">
						
						<tbody>
						</tbody>
					</table>
				</div> <!-- table scroll -->
					<table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table table-hover" style="margin-top: 0 !important; margin-bottom: -7px !important;">
						<thead>
							<tr style ="visibility: hidden;">
								<th class="active col-sm-3"></th>
								<th class="active col-sm-6"></th>
								<th class="active col-sm-3"></th>
							</tr>
							<tr>
								<th colspan="3" class="warning" style="text-align: center;"><?= __("Ödemesi Yapılacak Siparişler"); ?></th>
							</tr>
						</thead>
					</table>
				<div class="table-scroll">
					<table id="interimPayable" class="table table-hover interim_payments-payable">	
						<tbody>
							<tr class="interim_payments-info">
								<td colspan="3" style="text-align: center;"><h4><?= __("Ürün Seçiniz"); ?></h4></td>
							</tr>           
						</tbody>
					</table>
				</div> <!-- table scroll -->
				<br>
				<div class="button-interim-group">
					<div class="btn-group btn-group-justified interim_payments-submit" role="group">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-light-green btn-ripple btn-xxl" data-payment="cash"><i class="fa fa-money"></i> <?= __("NAKIT"); ?></button>
						</div>
						<div class="btn-group" role="group" style="text-align: center;">
							<span id="interimPaymentTotalPrice" style="font-size: xx-large;">0.00₺</span>                  
						</div>
					</div>
					<div class="btn-group btn-group-justified interim_payments-submit" role="group">
						<div class="btn-group" role="group">
							<button type="button" class="btn btn-orange btn-ripple btn-xxl" data-payment="credit"><i class="fa fa-credit-card"></i> <?= __("KREDİ K."); ?></button>                   
						</div>

						<div class="btn-group" role="group">
							<a class="btn btn-blue btn-ripple btn-xxl" data-toggle="modal" data-target="#modal-product-payment"> <?= __("DİĞER"); ?> </a>                   
						</div>
						
					</div>

				</div>

			</div> <!-- tab pane close -->
			<div class="tab-pane" id="2">
				<table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table table-hover m-t-0" style="margin-top: 0 !important; margin-bottom: -7px !important;">
					<thead>
						<tr>
							<th class="active col-sm-3"><?= __("Adet"); ?></th>
							<th class="active col-sm-6"><?= __("Ürün"); ?></th>
							<th class="active col-sm-3"><?= __("Fiyat"); ?></th>
						</tr>
						<tr class="interim_payments-info">
							<th colspan="3" class="danger" style="text-align: center;"><?= __("Ödemesi Yapılmış Olan Siparişler"); ?></th>
						</tr>
					</thead>	
				</table>
				<div class="table-scroll-history">
					<table id="interimPaid" class="table table-hover">
						
						<tbody>
						</tbody>
					</table>
				</div>
			</div> <!-- tab-pane close -->
		</div> <!-- tab-content close -->
	</div>
</div>