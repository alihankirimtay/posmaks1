<div class="container-fluid tab-body body-bg">
	<div class="row">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-header">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-10">      
            	<img class="home-click icon-svg-size" id="backHome" src="<?= asset_url('home-01.svg'); ?>"></img>
        	</div>

			<div class="col-md-6 col-sm-6 col-xs-6 nav-span-home p-10">
				<span class="home-table text-center" id="spanTableName"></span>    
			</div>

			<div class="col-md-3 col-sm-3 col-xs-3 p-10 text-right">
				<!-- <a class="pull-right " data-toggle="dropdown" href="#" style="color: white;"><span id="orderCheck" class="home-check glyphicon glyphicon-check"></span></a> -->
            	<img class="basket-click icon-svg-size" src="<?= asset_url('basket.svg'); ?>"></img>

			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bb-0 h-50 bg-nav bb-3">
			<ul class="nav nav-tabs b-0" id="categorylist" style="display: flex">
			</ul>
		</div>

		<div class="category-nav col-lg-12 col-md-12 col-sm-12 col-xs-12 bb-0 h-50 bb-3 hidden" style="background: #e8edee;">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-10 text-left">
				<span class="back-category-list"><i class="fa fa-chevron-left"></i> <?= __("GERİ"); ?></span>      
        	</div>
        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-10 text-right">
        		<span class="category-name"><?= __("KATEGORİ ADI"); ?></span>
        	</div>
		</div>

		<div class="list-group gallery col-lg-12 col-md-12 col-xs-12 col-sm-12 product-list" id="productlist">
			<div class="tab-content p-8">
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-nav product-footer hidden">
			<div class ="col-md-3 col-sm-3 col-xs-3 col-lg-3" style="line-height: 1.2; padding: 10px">
				<span style="color: white;"><small><?= __("TOPLAM"); ?></small></span>
				<span class="order-price" id="span-order-price">0.00₺</span>
			</div>

			<div class ="col-md-3 col-sm-3 col-xs-6 col-lg-3 pull-right order-div">
				<a id="pos-order" class="col-md-12 col-xs-12 col-lg-12 btn btn-blue btn-ripple btn-xl br-30"><?= __("SİPARİŞ"); ?></a>
			</div>
		</div>

	</div> <!-- row close -->
</div> 