<div id="numpad" class="content-center">
        
    <div class="container-fluid tab-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-10" style="background-color: #EB212F;">
                <span class="text-center text-white fs-30"><?= __("KULLANICI GİRİŞİ"); ?></span>    
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                    
                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 operator-input-block">
                        <input name="password-login" type="password" class="form-control text-center fa-5x operator-input" readonly>
                    </div>

                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 p-0">
                    
                        <div class="nums">
                            <div class="btn-group btn-group-justified">
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="1">1</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="2">2</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="3">3</a>
                            </div>

                            <div class="btn-group btn-group-justified">
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="4">4</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="5">5</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="6">6</a>
                            </div>

                            <div class="btn-group btn-group-justified">
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="7">7</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="8">8</a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="9">9</a>
                            </div>

                            <div class="btn-group btn-group-justified">
                                <a href="#" class="btn btn-danger btn-ripple btn-xxl" data-value="delete" style="background-color: #EB212F;"><i class="fa fa-long-arrow-left"></i></a>
                                <a href="#" class="btn btn-default btn-ripple btn-xxl" data-value="0">0</a>
                                <a href="#" class="btn btn-success btn-ripple btn-xxl" data-value="submit"><i class="fa fa-check"></i></a>
                            </div>
                        </div>

                    </div>

            </div>
        </div>
    </div>
    
</div>