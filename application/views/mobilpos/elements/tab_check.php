<div class="container-fluid tab-body">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bg-header">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-10 pl-0">      
				<!-- <a class="pull-left" style="color:white; background-color: #e74c3c;"><span id="backOrderCheckTab" class="back-table glyphicon glyphicon-chevron-left"></span></a> -->
            	<img id="backOrderCheckTab" class="icon-svg-size" src="<?= asset_url('left.svg'); ?>"></img>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-6 nav-order-span p-10">
				<span id="spanCheckOrder" class="home-table text-center" style="color:white;"></span> 
			</div>

			<div class="col-md-3 col-sm-3 col-xs-3 p-10 text-right">
            	<img id="backHomePage1" class="icon-svg-size" src="<?= asset_url('home-01.svg'); ?>"></img>
			</div>
		</div> <!-- col-md-12 -->

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inputs">

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-setcart" class="btn btn-info col-md-12 col-xs-12 "><?= __("SETCARD"); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-setcart" class="form-control" placeholder="<?= __("SETCARD"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-setcart" class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[setcart][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div><!-- /.col-lg-6 -->


			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-ticket" class="btn btn-danger col-md-12 col-xs-12 "><?= __("TICKET"); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-ticket" class="form-control" placeholder="<?= __("TICKET"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-ticket" class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[ticket][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-sodexo" class="btn btn-purple btn-ripple col-md-12 col-xs-12"><?= __("SODEXO"); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-sodexo" class="form-control" placeholder="<?= __("SODEXO"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-sodexo"  class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[sodexo][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-credit" class="btn btn-amber col-md-12 col-xs-12"><?= __("KREDİ K."); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-credit" class="form-control" placeholder="<?= __("KREDİ KARTI"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-credit" class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[credit][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-cash" class="btn btn-green col-md-12 col-xs-12"><?= __("NAKİT"); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-cash" class="form-control" placeholder="<?= __("NAKİT"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-cash" class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[cash][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 mt-10"> 
				<button type="button" id="add-multinet" class="btn btn-blue-grey col-md-12 col-xs-12"><?= __("MULTINET"); ?></button>
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 mt-10">
				<div class="inputer inputer-red">
					<div class="input-wrapper">
						<input type="number" name="add-multinet" class="form-control" placeholder="<?= __("MULTINET"); ?>">
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-4 col-xs-4 mt-10">
				<div class="input-group">
					<span class="input-group-btn">
						<button id="btn-multinet" class="btn btn-danger" type="button"><i class="fa fa-minus-square-o"></i></button>
					</span>
					<input type="text" name="payments[multinet][]" class="form-control text-right" value="0" readonly>
				</div><!-- /input-group -->
			</div>

			<div class="col-md-12 col-xs-12 mt-10">

				<li class="list-group-item text-right small p-y-0 mt-10" style="line-height: 3;">
					<span class="pull-left" style="font-size: 30px;"><?= __("Kalan"); ?></span> <strong id="posRest" class="small" style="font-size:30px;">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("Para Üstü"); ?></span> <strong id="posChange" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("Ödenen"); ?></span> <strong id="sumAmount" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("Aratoplam"); ?></span> <strong id="subtotalCheck" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("KDV"); ?></span> <strong id="kdv" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("Toplam"); ?></span> <strong id="sumCheck" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("Ara Ödeme"); ?></span> <strong id="subtotal" class="small fs-20">0.00</strong>
				</li>
				<li class="list-group-item text-right small p-y-0 mt-10">
					<span class="pull-left fs-20"><?= __("İndirim"); ?></span><strong id="discountSpan" class="small fs-20">0.00</strong>
				</li>
				
			</div>

			
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 p-bottom footer-button">
			<a id="pos-complete" class="col-md-12 col-xs-12 col-lg-12 btn btn-green btn-ripple btn-xxl"><?= __("HESAP AL"); ?></a>
		</div>
	</div> <!--row close -->
</div>