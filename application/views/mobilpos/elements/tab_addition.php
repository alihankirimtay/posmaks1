
<div class="panel panel-default">
        
    <div class="panel-body p-a-0">
        <div class="container">
            <div class="invoice">
                <div id="mobile-adisyon-uyarlama"></div>
                <div class="invoice-heading">
                    <div class="row date-row">
                        <div class="col-md-3 col-xs-4 invoice-id">
                            <p class="invoice-table-name text-left"><?= __("Masa Adı"); ?></p>
                            <p class="invoice-zone-name text-left"><?= __("Bölge Adı"); ?></p>
                        </div>
                        <div class="col-md-6 col-xs-4 text-center">
                            <img class="hidden" id="barcode" src="" alt="">
                            <img src="<?= base_url($location_invoice_template->logo) ?>" style="max-width:100px">
                        </div>
                        <div class="col-md-3 col-xs-4 invoice-id">
                            <p class="text-right">No: #<span class="invoice-no">000000</span></p>
                            <p class="invoice-datetime">01 01 2001</p>
                            <p class="adisyon-username"><?= __('Kullanıcı Adı') ?></p>
                        </div>
                    </div>
                    <div class="row text-center">
                        <?= $location_invoice_template->header ?>
                    </div>
                </div>
                <div class="invoice-body">
                    <table id="tblAddition" class="table table-hover">
                        <thead>
                          <tr>
                            <th class="col-lg-4 col-md-4 col-sm-4 col-xs-6"><?= __("Ürün"); ?></th>
                            <th class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><?= __("Adet"); ?></th>
                            <th class="col-lg-4 col-md-4 col-sm-4 col-xs-3"><?= __("Toplam"); ?></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>
                                <td class="text-right"><strong><?= __("Ara Toplam:"); ?></strong></td>
                                <td class="invoice-subtotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td class="text-right"><strong><?= __("Vergi:"); ?></strong></td>
                                <td class="invoice-taxtotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td class="text-right"><strong><?= __("İndirim:"); ?></strong></td>
                                <td class="invoice-discounttotal">0.00</td>
                            </tr>
                            <tr>
                                <td colspan="1"></td>
                                <td class="text-right"><strong><?= __("TOPLAM:"); ?></strong></td>
                                <td class="invoice-amount">0.00</td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_name p-b-0" colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_phone p-y-0" colspan="5"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_address p-t-0" colspan="5"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <?= $location_invoice_template->footer ?>
                </div>
            </div>
        </div>
    </div>
</div>