<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->page_title . ' | ' . $this->website->name; ?></title>

    <script type="text/javascript">
        var base_url = '<?= base_url(); ?>';
    </script>

    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- BEGIN CORE CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/panel/css/admin1.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/elements.css">
    <!-- END CORE CSS -->

    <!-- BEGIN PLUGINS CSS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/plugins/bootstrap-social/bootstrap-social.css">
    <!-- END PLUGINS CSS -->

    <!-- FIX PLUGINS -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/globals/css/plugins.css">
    <!-- END FIX PLUGINS -->

    <!-- BEGIN SHORTCUT AND TOUCH ICONS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/globals/img/icons/favicon.ico">
    <link rel="apple-touch-icon" href="<?= base_url(); ?>assets/globals/img/icons/apple-touch-icon.png">
    <!-- END SHORTCUT AND TOUCH ICONS -->

    <script src="<?= base_url(); ?>assets/globals/plugins/modernizr/modernizr.min.js"></script>
</head>
<body class="bg-login printable">

    <div class="login-screen">
	<div class="login-logo">
	<img src="<?= base_url(); ?>assets/posmaks-login-logo.png">
	</div>
        <div class="panel-login blur-content">
		
            <div class="panel-heading"></div>


            <form id="pane-new-password" class="panel-body active" method="post">
                <?php if (isset($error)): ?>
                    <h2 class="p-a-3 m-t-3">
                        <?= $error ?><br>
                        <i class="fa fa-exclamation-triangle fa-4x p-t-3"></i>
                    </h2>
                <?php else: ?>
                    <h2> <?= __("Merhaba"); ?><strong><?= $user->name ?></strong><br> <?= __("Lütfen yeni şifrenizi belirleyin."); ?></h2>
                    <div class="form-group">
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input type="text" name="token" value="<?= $user->password_token ?>" class="hidden">
                                <input name="password" type="password" class="form-control text-white" placeholder="<?= __("Şifre"); ?>">
                            </div>
                        </div>
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input name="password_confirm" type="password" class="form-control text-white" placeholder="<?= __("Şifre Doğrulama"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons clearfix p-a-2">
                        <button type="submit" class="btn btn-success btn-block submit" data-loading-text="Gönderiyor"><?= __("Kaydet"); ?></button>
                    </div>
                <?php endif; ?>
            </form>

        </div><!--.blur-content-->
    </div><!--.login-screen-->

    <div class="bg-blur dark">
        <div class="overlay"></div><!--.overlay-->
    </div><!--.bg-blur-->

    <svg version="1.1" xmlns='http://www.w3.org/2000/svg'>
        <filter id='blur'>
            <feGaussianBlur stdDeviation='7' />
        </filter>
    </svg>

    <!-- BEGIN GLOBAL AND THEME VENDORS -->
    <script src="<?= base_url(); ?>assets/globals/js/global-vendors.js"></script>
    <!-- END GLOBAL AND THEME VENDORS -->

    <!-- JQUERY FORM -->
    <script src="<?= base_url(); ?>assets/globals/scripts/jquery.form.min.js"></script>
    <!-- END JQUERY FORM -->

    <!-- PLUGINS INITIALIZATION AND SETTINGS -->
    <script src="<?= base_url(); ?>assets/globals/scripts/user-pages.js"></script>
    <!-- END PLUGINS INITIALIZATION AND SETTINGS -->

    <!-- PLEASURE Initializer -->
    <script src="<?= base_url(); ?>assets/globals/js/pleasure.js"></script>
    <!-- ADMIN 1 Layout Functions -->
    <script src="<?= base_url(); ?>assets/panel/js/layout.js"></script>
    <script src="<?= base_url(); ?>assets/globals/js/master.js"></script>

    <!-- BEGIN INITIALIZATION-->
    <script type="text/javascript">
    $(function() {
        Pleasure.init();
        Layout.init();
        UserPages.login();


        $("#pane-new-password .submit").on("click", function(e) {
            e.preventDefault();

            let button = $(this);
            button.button('loading');

            $.post(base_url + "login/processNewPassword", $("#pane-new-password").serializeArray(), function(json) {
                if (!json.result) {
                    button.button('reset');
                    Pleasure.handleToastrSettings('Hata!', json.message, 'error');
                } else {
                    $("#pane-new-password").html(`<h2 class="p-a-3 m-t-3">${json.message}<br><i class="fa fa-check-square-o fa-4x p-t-3"></i></h2>`);
                    setTimeout(function() {
                        location.href = base_url + "login";
                    }, 2000);
                }
            }, "json");
        });
    });
    </script>
    <!-- END INITIALIZATION-->
</body>
</html>
