<div class="modal full-height" id="modal-adisyon">
	<div class="modal-dialog modal-lg1">
		<div class="modal-content">
			<div class="modal-header">
				<div class="col-md-12">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title col-md-6"><?= __('Adisyon') ?></h4>
				</div>
				<div class="col-md-12">
					<a class="btn btn-primary pull-right btn-ripple col-md-6 adisyon-print"><?= __('Yazdır') ?></a>
				</div>
				
			</div>
			<div class="modal-body">
				<div class="panel-body p-a-0">
			        <div class="container-fluid" style="width: 8cm !important; font-size: 14px !important; padding: 0 !important;">
			            <div class="invoice"  style="padding:0 !important;">
			                <div class="invoice-heading">
			                    <div class="row date-row">
			                        <div class="col-md-6 col-xs-6">
			                            <img id="barcode" src="" alt="">
			                        </div>
			                        <div class="col-md-6 col-xs-6 invoice-id">
			                            <h4><?= __('No:') ?> #<span class="invoice-no">000000</span></h4>
			                            <h5 class="invoice-datetime">01 01 2001</h5>
			                            <h5 class="adisyon-username"><?= __('Kullanıcı Adı') ?></h5>
			                        </div>
			                    </div>
			                </div>
			                <div class="invoice-body">
			                    <table class="table table-condensed" style="width: 100%;">
			                        <thead>
			                            <tr>
			                                <th><?= __('Ürün') ?></th>
			                                <th><?= __('Miktar') ?></th>
			                                <th><?= __('Fiyat') ?></th>
			                                <th><?= __('Toplam') ?></th>
			                            </tr>
			                        </thead>
			                        <tbody>
			                           
			                        </tbody>
			                        <tfoot>
			                            <tr>
			                                <td colspan="2" class="text-right"><strong><?= __('Ara Toplam:') ?></strong></td>
			                                <td colspan="2" class="text-right invoice-subtotal">0.00</td>
			                            </tr>
			                            <tr>
			                                <td colspan="2" class="text-right"><strong><?= __('Vergi:') ?></strong></td>
			                                <td colspan="2" class="text-right invoice-taxtotal">0.00</td>
			                            </tr>
			                            <tr>
			                                <td colspan="2" class="text-right"><strong><?= __('İndirim:') ?></strong></td>
			                                <td colspan="2" class="text-right invoice-discounttotal">0.00</td>
			                            </tr>
			                            <tr>
			                                <td colspan="2" class="text-right"><strong><?= __('TOPLAM:') ?></strong></td>
			                                <td colspan="2" class="text-right invoice-amount">0.00</td>
			                            </tr>
			                            <tr>
			                                <td class="invoice-customer_name p-b-0" colspan="5"></td>
			                            </tr>
			                            <tr>
			                                <td class="invoice-customer_phone p-y-0" colspan="5"></td>
			                            </tr>
			                            <tr>
			                                <td class="invoice-customer_address p-t-0" colspan="5"></td>
			                            </tr>
			                        </tfoot>
			                    </table>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>