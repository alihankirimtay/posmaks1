<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Çalışan Oluştur"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url() ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('employes') ?>"><?= __("Çalışanlar"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Çalışan Oluştur"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Çalışan Detayları"); ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form id="employesForm" action="#" class="form-horizontal">
                        <div class="form-content">

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Rol"); ?></label>
                                <div class="col-md-9">
                                    <select name="role_id" class="chosen-select">
                                        <?php foreach ($roles as $role): ?>
                                            <option value="<?= $role->id; ?>"><?= $role->title ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select multiple="multiple" name="locations[id][]" class="chosen-select location-zone">
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>"><?= $location['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group" id="zones">
                                <label class="control-label col-md-3"><?= __("Bölge"); ?></label>
                                <div class="col-md-9">
                                    <select multiple="multiple" name="zones[id][]" data-placeholder="Bölge seç" class="chosen-select zones-list">
                                        <option value="0"><?= __("Bölge Seç"); ?></option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Ad Soyad"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Hesap adı"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="username" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Eposta"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="email" name="email" class="hidden">
                                            <input type="email" name="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("POSMAKS Oturum Şifresi"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Pin (POS Oturum Şifresi)"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="number" name="pin" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Durum"); ?></label>
                                <div class="col-md-9">
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active1" checked="checked" value="1" >
                                        <label for="active1"><?= __("Aktif"); ?></label>
                                    </div>
                                    <div class="radioer">
                                        <input type="radio" name="active" id="active0" value="0" >
                                        <label for="active0"><?= __("Pasif"); ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('employes'); ?>" class="btn btn-grey"><?= __("İptal"); ?></a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('employes/add') ?>" data-return="<?= base_url('employes') ?>" ><?= __("Kaydet"); ?></button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
