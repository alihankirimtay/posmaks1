<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Katlar"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __("Katlar"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $location_id, 'floors/index/', 'Restoranlar'); ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __("Tüm Katlar"); ?></h4>
                        <div class="btn-group pull-right" id="katOlustur">
                            <a href="<?= base_url('zones/add/');?>" class="btn btn-warning btn-xs" target="_blank"><i class="fa fa-plus-circle"></i> <?= __("Bölge Oluştur");?></a>
                            <a href="<?= base_url('points/add/');?>" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-plus-square"></i> <?= __("Masa Oluştur");?></a>
                            <a href="<?= base_url('floors/add/');?>" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-plus"></i> <?= __("Kat Oluştur"); ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __("İsim"); ?></th>
                                    <th><?= __("Restoran"); ?></th>
                                    <th><?= __("Oluşturuldu"); ?></th>
                                    <th><?= __("Durum"); ?></th>
                                    <th><?= __("İşlem"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($floors as $floor): ?>
                                    <tr>
                                        <td><?php ECHO $floor->title; ?></td>
                                        <td><?php ECHO $floor->location_title; ?></td>
                                        <td><?=dateConvert($floor->created, 'client', dateFormat())?></td>
                                        <td><?php ECHO get_status($floor->active); ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?php ECHO base_url('floors/edit/'.$floor->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?php ECHO base_url('floors/delete/'.$floor->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>