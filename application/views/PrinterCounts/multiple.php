    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>New Multiple Printer Counts<small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('printers/index/'.$location->id) ?>">Printers</a></li>
                    <li><a href="<?= base_url('printers/index/'.$location->id) ?>"><?= $location->title ?></a></li>
                    <li><a href="#" class="active">New Multiple Printer Count</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Location: <?= $location->title ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <div class="form-content">
                            
                            <div class="form-group">
                                <label class="control-label col-sm-3">Date</label>
                                <div class="col-sm-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="date" class="form-control datetimepicker-basic" value="<?= dateConvert(date('Y-m-d H:i:s'), 'client', dateFormat()) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="note note-primary note-top-striped">
                                
                                <h4>Printers</h4>
                                
                                <?php foreach ($printers as $key => $printer): ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3">
                                            <div class="checkboxer">
                                                <input type="checkbox" name="printers[]" id="check<?= $printer->id ?>" value="<?= $printer->id ?>" checked>
                                                <label for="check<?= $printer->id ?>"><?= $printer->title ?></label>
                                            </div>
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="inputer">
                                                <div class="input-wrapper">
                                                    <input type="number" name="values[<?= $printer->id ?>]" class="form-control" value="0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <div class="note note-primary note-top-striped">
                                <h4>Photos</h4>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-3" title="Pictures printed but not sold.">Unsold</label>
                                    <div class="col-sm-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="number" placeholder="Pictures printed but not sold." name="unsold" class="form-control" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" title="Pictures printed for test.">Waste</label>
                                    <div class="col-sm-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="number" placeholder="Pictures printed for test." name="waste" class="form-control" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" title="Pictures given for free.">Comps</label>
                                    <div class="col-sm-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="number" placeholder="Pictures given for free." name="comps" class="form-control" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" title="Pictures given for free.">Unseen</label>
                                    <div class="col-sm-9">
                                        <div class="inputer">
                                            <div class="input-wrapper">
                                                <input type="number" placeholder="Pictures Unseen." name="unseen" class="form-control" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-buttons-fixed">
                                <a type="button" href="<?= base_url('printers/index/'.$location->id); ?>" class="btn btn-grey">Cancel</a>
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('printercounts/multiple/'.$location->id);?>" >Save</button>
                                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('printercounts/multiple/'.$location->id);?>" data-return="<?= base_url('printers/index/'.$location->id); ?>" >Save & Exit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
