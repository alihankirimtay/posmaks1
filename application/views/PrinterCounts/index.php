<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>Printer Counts</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>">Printers</a></li>
                    <li><a href="<?= base_url('printers/index/'.$current_location) ?>"><?= @$this->user->locations_array[$current_location]['title'] ?></a></li>
                    <li><a href="<?= base_url('printercounts/index/'.$current_printer) ?>"><?= $printer->title ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>All Print Counts</h4>
                        <div class="btn-group pull-right">
                            <a href="<?= base_url('printercounts/add/'.$current_printer); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> New Print Count</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th class="col-md-3">Date</th>
                                    <th class="col-md-3">Count</th>
                                    <th class="col-md-3">Created</th>
                                    <th class="col-md-3">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($printercounts as $printer): ?>
                                    <tr>
                                        <td><?= dateConvert($printer->date, 'client', dateFormat()) ?></td>
                                        <td><?= (int)$printer->count ?></td>
                                        <td><?= dateConvert($printer->created, 'client', dateFormat()) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('printercounts/edit/'.$current_printer.'/'.$printer->id); ?>"> <i class="fa fa-pencil"></i> </a>
                                            <!-- <button class="btn btn-danger btn-xs" data-url="<?= base_url('printercounts/delete/'.$current_printer.'/'.$printer->id); ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button> -->
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
