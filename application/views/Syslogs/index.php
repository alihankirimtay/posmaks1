<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Loglar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Ayarlar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Loglar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Tüm Sistem Logları') ?></h4>
                        <div class="btn-group pull-right">
                        <?php if ($this->uri->segment(3) > 1): ?>
                            <a href="<?php ECHO base_url('syslogs/index/'.($this->uri->segment(3)-1)); ?>" class="btn btn-success btn-xs"><i class="fa fa-chevron-left"></i> <?= __('Geri') ?></a>
                        <?php endif ?>
                            <a href="<?php ECHO base_url('syslogs/index/'.($this->uri->segment(3) ? $this->uri->segment(3)+1 : 2)); ?>" class="btn btn-success btn-xs"><i class="fa fa-chevron-right"></i> <?= __('İleri') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        
                        <div class="table-responsive">
                            <table class="table table-hover" style="font-size: small; font-family: monospace;">
                                <thead>
                                    <tr>
                                        <th class="col-md-2"><?= __('Kullanıcı') ?></th>
                                        <th class="col-md-5"><?= __('Mesaj') ?></th>
                                        <th class="col-md-2"><?= __('Ip') ?></th>
                                        <th class="col-md-3"><?= __('Tarih') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($logs as $log): ?>
                                    <tr>
                                        <td><?php ECHO $log->user; ?></td>
                                        <td><a href='javascript:void(0);' class="open-log-modal" data-id="<?php ECHO $log->id; ?>"><?php ECHO $log->message; ?></a></td>
                                        <td><?php ECHO $log->ip; ?></td>
                                        <td><?php ECHO $log->created; ?></td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>


<div class="modal fade" id="logModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Değerler') ?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Kapat') ?></button>
            </div>
        </div>
    </div>
</div>