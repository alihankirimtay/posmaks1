    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Masa Düzeni') ?> <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('points'); ?>"><?= __('Masalar') ?></a></li>
                    <li><a href="#" class="active"><?= __('Masa Düzeni') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row" id="points-container">
        <!-- <div class="col-sm-12"> -->
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __('Masa Düzeni') ?></h4>
                    </div>
                </div>

                <div class="panel-body">
                    <form id="settings" class="form-inline">
                        <div class="form-group">
                            <select name="location" data-placeholder="Restoran seç" class="chosen-select location">
                                <option value="0"><?= __('Restoran seç') ?></option>
                                <?php foreach ($this->user->locations_array as $location): ?>
                                    <option value="<?= $location['id'] ?>"><?= $location['title'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="floor" data-placeholder="Kat seç" class="chosen-select floor">
                                <option value="0"><?= __('Kat seç') ?></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="zone" data-placeholder="Bölge seç" class="chosen-select zone">
                                <option value="0"><?= __('Bölge seç') ?></option>
                            </select>
                        </div>
                    </form>
                    <div class="p-t-2" id="points">
                        
                    </div>
                </div>

                <div class="panel">
                    <form id="planner-form">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <button type="button" class="btn btn-info btn-sm background hidden"><i class="fa fa-image"></i> <?= __('Arka Plan Yükle') ?></button>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success hidden" name="dosubmit" data-url="<?= base_url('points/ajax/savePlan') ?>" ><?= __('Kaydet') ?></button>
                                    <input type="hidden" name="location">
                                    <input type="hidden" name="floor">
                                    <input type="hidden" name="zone">
                                    <input type="file" name="background" accept="image/*" class="hidden">
                                </div>
                            </div>
                        </div>

                        <div class="panel-body p-a-0">
                            <div id="planner">
                                
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        <!-- </div> -->

    </div>

</div>



<style type="text/css">
    #planner {
        min-height: 600px;
        background-color: #3b863e;
        background-image: linear-gradient(0deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent), linear-gradient(90deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent);
        height:100%;
        background-size:50px 50px;
        position: relative;
    }
    #planner.has-background {
        /*background-size: cover !important;*/
        /*background-position: center !important;*/
        background-repeat: no-repeat !important;
    }

    .pnt {
        background: #ffffff;
        border: 1px dashed black;
        min-height: 75px;
        min-width: 75px;
    }

    #planner .pnt {
        position: absolute !important;
    }

    #points .pnt {
        background: #ffe4e2;
        float: left;
        margin: 2px;
    }

    #planner .pnt-delete {
        cursor: pointer;
        position: absolute;
        right: 0;
        top: 0;
        background: #fff;
        border-radius: 50%;
        z-index: 1;
    }

    .pnt-delete {
        display: none;
    }
    #planner .pnt:hover .pnt-delete {
        display: initial;
    }
</style>