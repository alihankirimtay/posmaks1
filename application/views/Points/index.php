<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __('Masalar') ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#" class="active"><?= __('Masalar') ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <?php ECHO dropdownMenu($this->user->locations_array, $location_id, 'points/index/', 'Restoranlar'); ?>
                    </div>

                    <div class="panel-title">
                        <h4><?= __('Tüm Masalar') ?></h4>
                        <div class="btn-group pull-right">
                            <a href="<?= base_url('floors/add') ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-plus-circle"></i> <?= __('Kat Düzeni') ?></a>
                            <a href="<?= base_url('zones/add') ?>" class="btn btn-primary btn-xs" target="_blank"><i class="fa fa-plus-square"></i> <?= __('Bölge Düzeni') ?></a>
                            <a id="masaDuzeni" href="<?= base_url('points/planner') ?>" class="btn btn-warning btn-xs"><i class="fa fa-square"></i> <?= __('Masa Düzeni') ?></a>
                            <a id="masaOlustur" href="<?= base_url('points/add/') ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= __('Masa Oluştur') ?></a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="overflow-table">
                        <table class="display datatables-basic">
                            <thead>
                                <tr>
                                    <th><?= __('İsim') ?></th>
                                    <th><?= __('Restoran') ?></th>
                                    <th><?= __('Kat') ?></th>
                                    <th><?= __('Bölge') ?></th>
                                    <th><?= __('Oluşturuldu') ?></th>
                                    <th><?= __('Durum') ?></th>
                                    <th><?= __('İşlem') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($points as $point): ?>
                                    <tr>
                                        <td><?= $point->title ?></td>
                                        <td><?= $point->location_title ?></td>
                                        <td><?= $point->floor_title ?></td>
                                        <td><?= $point->zone_title ?></td>
                                        <td><?= dateConvert($point->created, 'client', dateFormat()) ?></td>
                                        <td><?= get_status($point->active) ?></td>
                                        <td>
                                            <a class="btn btn-primary btn-xs" href="<?= base_url('points/edit/'.$point->id) ?>"> <i class="fa fa-pencil"></i> </a>
                                            <button class="btn btn-danger btn-xs" data-url="<?= base_url('points/delete/'.$point->id) ?>" name="dodelete"> <i class="fa fa-trash-o"></i> </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>