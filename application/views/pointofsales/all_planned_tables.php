<?php foreach ($floors as $key => $floor): ?>

    <div class="panel-group" id="floor-tab-<?= $key.$DIFF ?>">

        <div class="panel panel-info">
            <div class="panel-heading">
                <a data-toggle="collapse" data-parent="#floor-tab-<?= $key.$DIFF ?>" href="#floor-<?= $floor->id.$DIFF ?>">
                    <h4 class="panel-title"><?= $floor->title ?></h4>
                </a>
            </div>
            <div id="floor-<?= $floor->id.$DIFF ?>" class="panel-collapse collapse">

            	<!-- 1 PANEL BODY -->
                <div class="panel-body">


		                <?php foreach ($zones as $key1 => $zone): ?>

		                	<?php if ($zone->floor_id != $floor->id): ?>
		                		<?php continue; ?>
		                	<?php endif; ?>

		                	<!-- 2 PANEL -->
		                    <div class="panel-group" id="zone-tab-<?= $key1.$DIFF ?>">
		          
			                    <div class="panel panel-success">
			                        <div class="panel-heading">
			                            <a data-toggle="collapse" data-parent="#zone-tab-<?= $key1.$DIFF ?>" href="#zone-<?= $zone->id.$DIFF ?>">
			                                <h4 class="panel-title"><?= $zone->title ?></h4>
			                            </a>
			                        </div>
			                        <div id="zone-<?= $zone->id.$DIFF ?>" class="panel-collapse collapse">
			                        	<!-- 2 PANEL BODY -->
			                            <div class="panel-body">

			                            	<?php if ($tables): ?>

												<!-- <div class="table-responsive"> -->
													<!-- TABLE -->
			                                        <div class="points-ground">

			                                            	<?php foreach ($tables as $key => $table): ?>
			                                            		<?php if ($table->floor_id == $floor->id && $table->zone_id == $zone->id): ?>
			                                            			
			                                            		<?php $opened_at = '-'; ?>
			                                            		<?php $amount = 0; ?>
			                                            		<?php $status = 'pnt-green'; ?>
		                                    
		                                    					<?php if ($table->status): ?>
		                                    						<?php
		                                    							$sale = $this->db->select('amount')->where([
								                                            'active'    => 1,
								                                            'point_id'  => $table->id,
								                                            'return_id' => null,
								                                            'status !=' => 'cancelled'
							                                        	])->order_by('id', 'DESC')->get('sales', 1)->row();

							                                        	$status    = 'pnt-red';
								                                        $amount    = number_format(@$sale->amount, 2);
								                                        $opened_at = dateConvert($table->opened_at, 'client', dateFormat());
		                                    						?>
		                                    					<?php endif; ?>

		                                    					<?php $button = "<a href=\"".base_url('pointofsales/index/' . $table->id)."\" >"; ?>
		                                    					<?php if ($this->input->get('empty_tables')): ?>
		                                    						<?php if ((int) $this->input->post('current_table') == $table->id): ?>
		                                    							<?php $button = "<a href=\"javascript:void(0);\">"; ?>
		                                    						<?php else: ?>
		                                    							<?php $button = "<a href=\"javascript:void(0);\" data-target=\"{$table->id}\" class=\"move-table\">"; ?>
		                                    						<?php endif; ?>
		                                    					<?php endif; ?>

		                                    					<?= $button ?>
			                                    					<div class="pnt <?= $status ?>" style="<?= $table->planner_settings ?>">
			                                    						<div><?= $table->title ?></div>
			                                    						<div><?= $amount ?> TL</div>
			                                    					</div>
			                                    				</a>

		                           								<?php endif; ?>
		                            						<?php endforeach; ?>


					                                </div> <!-- END TABLE -->
					                            <!-- </div> -->

		                            		<?php endif; ?>

			                            </div> <!-- END 2 PANEL BODY -->
			                        </div>
			                    </div>
		                    </div> <!-- END 2 PANEL -->

		                <?php endforeach; ?>


                </div> <!-- END 1 PANEL BODY -->
            </div>
        </div>
    </div>

<?php endforeach; ?>



<style type="text/css">
	.points-ground {
		background: #3b863e;
		background-image: linear-gradient(0deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent), linear-gradient(90deg, transparent 24%, rgba(255, 255, 255, .05) 25%, rgba(255, 255, 255, .05) 26%, transparent 27%, transparent 74%, rgba(255, 255, 255, .05) 75%, rgba(255, 255, 255, .05) 76%, transparent 77%, transparent);
		background-size: 50px 50px;
		min-height: 500px;
		width: 100%;
	}
	
	.pnt {
    	border: 1px solid black;
    	font-size: 1.5rem;
		height: 75px;
		width: 75px;
		position: absolute;
	}
	.pnt-green {
		background: green;	
		color: white;
	}
	.pnt-red {
		background: #E74B4B;
		color: white;	
	}

	.modal-fullscreen .modal-dialog {
		margin: 0;
		margin-right: auto;
		margin-left: auto;
		width: 100%;
	}
</style>