<div class="content">

    <div class="page-header full-content m-b-0">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <h1>
                    <button data-toggle="modal" data-target="#modal-session" class="btn btn-success btn-md"><i class="fa fa-lock" aria-hidden="true"></i> <?= __("Oturumlar"); ?></button>
                    <?= __("POS"); ?> <small></small>
                </h1>
            </div>
            <div class="col-xs-12 col-md-8 text-right">
                <button data-toggle="modal" data-target="#modal-sound-recorder" class="btn btn-amber btn-md"><i class="fa fa-microphone" aria-hidden="true"></i> <?= __("SESLİ NOT AL"); ?></button>
                <button data-toggle="modal" data-target="#pos-modal-note" class="btn btn-deep-orange btn-md"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?= __("NOT AL"); ?></button>
                <button id="print-adisyon" class="btn btn-white btn-md"><i class="fa fa-check-square-o" aria-hidden="true"></i> <?= __("ADİSYON"); ?></button>
                <button id="exit_operator_session" class="btn btn-red btn-lg"><i class="fa fa-lock"></i></button>
            </div>
        </div> 
    </div>

    <div class="row no-gutters" style="background: #fff; margin-left: -40px; margin-right: -40px;">
        <form id="pos">

            <?php if ($table): ?>
                <input type="hidden" name="table_id" value="<?= @$table->id ?>">
                <input type="hidden" name="sale_id" value="<?= isset($sale->id) ? $sale->id : '' ?>">
            <?php endif; ?>

            <input type="hidden" name="location_currency" value="<?= $location_currency ?>">
            <input type="hidden" id="pos-note" name="pos-note" value="<?= isset($sale->desc) ? $sale->desc : null ?>">
            <input type="hidden" name="customer_id" value="<?= (isset($customer->id) ? $customer->id : '') ?>">
            
            <div class="dropdown">
                <button type="button" class="btn btn-flat btn-grey btn-xs btn-ripple dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> <?= __("Menü"); ?></button>
                <ul class="dropdown-menu">
                    <li class="hidden"><a href="#activation-zipboox-modal" data-toggle="modal" class="small"><h4><i class="fa fa-check-circle"></i> <?= __("Aktivasyon"); ?></h4></a></li>
                    <li><a href="#return-product-modal" data-toggle="modal" class="small"><h4><i class="fa fa-undo"></i> <?= __("Ürün İadesi"); ?></h4></a></li>
                    <li class="hidden"><a href="#" class="small" id="reset-ves"><h4><i class="fa fa-archive"></i> Reopen VES</h4></a></li>
                    <li class="divider"></li>
                    <li><a href="<?= base_url('pointofsales/changePos')  ?>" class="small"><h4><i class="fa fa-eject"></i> <?= __("Kasayı Değiştir"); ?></h4></a></li>
                </ul>
                <b class="pull-right pos-title p-r-2"><?= $this->user->name ?> - <i></i>&nbsp;</b>
            </div>
            

            <!-- SUMMARY -->
            <div class="col-sm-5">
                <div class="panel">

                    <div class="btn-group btn-group-justified">
                        <a data-toggle="modal" href='#table-select-modal' class="btn btn-orange btn-ripple btn-xxl"><i class="fa fa-square-o"></i> <?= __("MASALAR"); ?> <?= $table ? ' - '.$table->title : null ?></a>
                        <a data-toggle="modal" href='#modal-move-table' class="btn btn-blue-grey btn-ripple btn-xxl"><i class="fa fa-arrows"></i> <?= __("MASA TAŞI"); ?></a>
                        <a data-toggle="modal" href='#modal-package_service' class="btn btn-purple btn-ripple btn-xxl"><i class="fa fa-shopping-cart"></i> <?= __("PAKET SERVİS"); ?></a>
                    </div>

                    <div class="panel-body" id="pos-list" style="height: 270px;  overflow-y: auto; padding: 0;">
                        <!-- TICKET -->
                        <!-- <input type="text" name="ticket" class="form-control text-center" placeholder="Ticket Number" autocomplete="off" autofocus="true"> -->
                        <!-- END - TICKET -->
                        

                        <div class="table">
                            <table class="table table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="small col-sm-1"></th>
                                        <th class="small col-sm-2"><?= __("Adet"); ?></th>
                                        <th class="small col-sm-4"><?= __("Ürün"); ?></th>
                                        <th class="small col-sm-2 text-right"><?= __("Fiyat"); ?></th>
                                        <th class="small col-sm-2 text-right"><?= __("Toplam"); ?></th>
                                        <th class="small col-sm-1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($sale->products)): ?>
                                        <?php foreach ($sale->products as $product): ?>
                                            <tr data-index="<?= $product->id ?>" class="pr-container">
                                                <td><span class="fa fa-pencil small btn btn-primary btn-xs edit" aria-hidden="true"></span></td>
                                                <td class="pr-quantity">
                                                    <b><?= $product->quantity ?></b>
                                                    <input type="hidden" name="products[<?= $product->id ?>][product_id]" value="<?= $product->product_id ?>">
                                                    <input type="hidden" name="products[<?= $product->id ?>][sale_product_id]" value="<?= $product->id ?>">
                                                    <input type="hidden" name="products[<?= $product->id ?>][quantity]" value="<?= $product->quantity ?>" price="<?= $product->price ?>" tax="<?= $product->tax ?>">
                                                    <?php if ($product->type == 'product'): ?>
                                                        <?php foreach ($product->stocks as $stock): ?>
                                                            <?php if (!$stock->in_use): ?>
                                                                <input type="hidden" name="products[<?= $product->id ?>][stocks][<?= $stock->stock_id ?>]" value="<?= $stock->stock_id ?>">
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <?php foreach ($product->additional_products as $additional_product): ?>
                                                        <input type="hidden" name="products[<?= $product->id ?>][additional_products][<?= $additional_product->product_id ?>]" value="<?= $additional_product->product_id ?>" price="<?= $additional_product->price ?>" tax="<?= $additional_product->tax ?>">
                                                    <?php endforeach; ?>
                                                </td>
                                                <td class="small pr-title"><?= $product->title ?> </td>
                                                <td class="small text-right pr-price"><?= $product->price ?></td>
                                                <td class="small text-right pr-total"><?= number_format($product->price * $product->quantity, 2) ?></td>
                                                <td class="text-right"><span class="fa fa-times small btn btn-danger btn-xs trash" aria-hidden="true"></span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-body p-a-0" id="pos-summary">
                        <div class="col-sm-12">
                            <ul class="list-group">
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Aratoplam:"); ?></span> <strong id="subtotal" class="small"><?= numberFormat(isset($sale->subtotal) ? $sale->subtotal : 0, 2) ?></strong>
                                </li>
                                <!-- <li class="list-group-item text-right small">
                                    <span class="pull-left">Discount:</span> <strong id="discount" class="small"><i id="discount-add" class="btn btn-sm btn-default fa fa-plus-square"></i> 0.00</strong>
                                </li> -->
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Toplam Vergi:"); ?></span><a href="#" id="button-remove-tax" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i></a> <strong id="tax-total" class="small"><?= numberFormat(isset($sale->amount) ? $sale->amount - $sale->subtotal : 0, 2) ?></strong>
                                </li>
                                <li class="list-group-item text-center p-y-0">
                                    <h3 class="m-b-0"><strong id="total"><?= numberFormat(isset($sale->amount) ? $sale->amount : 0, 2) ?></strong></h3>
                                </li>
                                <li class="list-group-item p-a-0">
                                    <div class="btn-group btn-group-justified">
                                        <a href="#modal-interim_payments" class="btn btn-red btn-ripple btn-xxl" data-toggle="modal"><i class="fa fa-cubes"></i> <?= __("ARA ÖDEME"); ?></a>
                                        <a href="#modal-other_payments" class="btn btn-cyan btn-ripple btn-xxl" data-toggle="modal"><i class="fa fa-cart-plus"></i> <?= __("DİĞER"); ?></a>
                                        <a id="add-credit" class="btn btn-primary btn-ripple btn-xxl"><i class="fa fa-credit-card"></i> <?= __("K. KARTI"); ?></a>
                                        <a id="add-cash" class="btn btn-light-green btn-ripple btn-xxl"><i class="fa fa-money"></i> <?= __("NAKİT"); ?></a>
                                    </div>
                                </li>
                                <li class="list-group-item clearfix p-a-0">
                                    
                                    <input name="payments[cash][]" type="hidden" class="hidden" value="0">
                                    
 
                                    <div id="payments-in-credit" class="col-sm-3 col-sm-offset-6">
                                        <?php if (isset($sale->credit)): ?>
                                            <?php if ($sale->credit + 0): ?>
                                                <div class="input-group"> <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span>
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input name="payments[credit][]" type="text" class="form-control text-right" value="<?= numberFormat($sale->credit, 2) ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>


                                    <div id="payments-in-cash" class="col-sm-3">
                                        <?php if (isset($sale->cash)): ?>
                                            <?php if ($sale->cash + 0): ?>
                                                <div class="input-group"> <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span>
                                                    <div class="inputer">
                                                        <div class="input-wrapper">
                                                            <input name="payments[cash][]" type="text" class="form-control text-right" value="<?= numberFormat($sale->cash, 2) ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    

                                    <!-- OTHER PAYMENTS -->
                                    <div class="modal fade in" id="modal-other_payments">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title"><?= __("Diğer Ödemeler"); ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="btn-group btn-group-justified">
                                                        <a id="add-voucher" class="btn btn-cyan btn-ripple btn-xxl"><i class="fa fa-gift"></i> <?= __("İKRAM"); ?></a>
                                                        <a id="add-ticket" class="btn btn-yellow btn-ripple btn-xxl"><i class="fa fa-credit-card-alt"></i> <?= $this->Globals->payment_types->ticket->title ?></a>
                                                        <a id="add-sodexo" class="btn btn-indigo btn-ripple btn-xxl"><i class="fa fa-credit-card-alt"></i> <?= $this->Globals->payment_types->sodexo->title ?></a>
                                                    </div>

                                                    

                                                    <div id="payments-in-voucher" class="col-sm-4">
                                                        <?php if (isset($sale->voucher)): ?>
                                                            <?php if ($sale->voucher + 0): ?>
                                                                <div class="input-group"> <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span>
                                                                    <div class="inputer">
                                                                        <div class="input-wrapper">
                                                                            <input name="payments[voucher][]" type="text" class="form-control text-right" value="<?= numberFormat($sale->voucher, 2) ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div id="payments-in-ticket" class="col-sm-4">
                                                        <?php if (isset($sale->ticket)): ?>
                                                            <?php if ($sale->ticket + 0): ?>
                                                                <div class="input-group"> <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span>
                                                                    <div class="inputer">
                                                                        <div class="input-wrapper">
                                                                            <input name="payments[ticket][]" type="text" class="form-control text-right" value="<?= numberFormat($sale->ticket, 2) ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>

                                                    <div id="payments-in-sodexo" class="col-sm-4">
                                                        <?php if (isset($sale->sodexo)): ?>
                                                            <?php if ($sale->sodexo + 0): ?>
                                                                <div class="input-group"> <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span>
                                                                    <div class="inputer">
                                                                        <div class="input-wrapper">
                                                                            <input name="payments[sodexo][]" type="text" class="form-control text-right" value="<?= numberFormat($sale->sodexo, 2) ?>">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal"><?= __("Kapat"); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- OTHER PAYMENTS -->
                
                                </li>
                                <?php $paid = isset($sale->cash) ? $sale->cash + $sale->credit + $sale->voucher : 0.00 ?>
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Ara Ödeme:"); ?></span> <strong id="pos-interim_paid" class="small"><?= numberFormat($paid, 2) ?></strong>
                                </li>
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Ödenen:"); ?></span> <strong id="pos-paid" class="small"><?= numberFormat($paid, 2) ?></strong>
                                </li>
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Kalan:"); ?></span> <strong id="pos-rest" class="small"><?= numberFormat(isset($sale->amount) ? abs($paid - $sale->amount) : 0, 2) ?></strong>
                                </li>
                                <li class="list-group-item text-right small p-y-0">
                                    <span class="pull-left"><?= __("Para Üstü:"); ?></span> <strong id="pos-change" class="small"><?= numberFormat(isset($sale->amount) && $paid ? $sale->amount - $paid : 0, 2) ?></strong>
                                </li>
                                <li class="list-group-item p-x-0">
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <button id="pos-create-order" class="btn btn-success btn-blue btn-ripple btn-xxl"><i class="fa fa-shopping-basket"></i> <?= __("SİPARİŞ"); ?></button>
                                        </div>
                                        <div id="pos-delete-order-container" class="btn-group <?= (isset($sale->id) ?: 'hidden') ?>">
                                            <button id="pos-delete-order" data-id="<?= @$table->id ?>" class="btn btn-success btn-red btn-ripple btn-xxl"><i class="fa fa-ban"></i> <?= __("İPTAL ET"); ?></button>
                                        </div>
                                        <div class="btn-group">
                                            <button id="pos-complete" class="btn btn-success btn-ripple btn-xxl"><i class="fa fa-check"></i> <?= __("HESAP AL"); ?></button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- SUMMARY -->


            <!-- LIST -->
            <div class="col-sm-7">
                <div class="panel">

                    
                    <!-- PRODUCTS -->
                    <div class="panel-heading">
                        <div id="location-select" class="input-group hidden"> 
                            <span class="input-group-addon"><?= __("Restoran:"); ?></span>
                            <select name="location_id" class="form-control">
                                <?php foreach ($locations as $location): ?>
                                    <option value="<?= $location['id'] ?>"><?= $location['title'] ?></option>
                                <?php endforeach; ?>
                            </select>

                            <span class="input-group-addon"><?= __("POS Point:"); ?></span>
                            <select name="pos_id" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="panel-body p-a-0" id="pos-products">
                        <div class="thumbnail">
                            <div class="caption text-center">
                                <h3><?= __("Ürünler Yükleniyor..."); ?></h3>
                            </div>
                        </div>
                    </div>
                    <!-- PRODUCTS -->

                </div>
            </div>
            <!-- LIST -->
            
            
        </form>


        <form id="return" style="display: none;">

            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title"><b>#ID</b> <i id="sale-id">0</i></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table">
                            <table class="table table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="4"><?= __("Ürünler"); ?></th>
                                        <th colspan="1"><?= __("İade"); ?></th>
                                    </tr>
                                    <tr>
                                        <th class="info"><?= __("İsim"); ?></th>
                                        <th class="info"><?= __("Fiyat"); ?></th>
                                        <th class="info"><?= __("Miktar"); ?></th>
                                        <th class="info"><?= __("Toplam"); ?></th>
                                        <th class="success"><?= __("Miktar"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" class="text-right"><b><?= __("İade Ara Toplam:"); ?></b></td>
                                        <td colspan="1" id="return-subtotal">100E</td>
                                    </tr>
                                     <tr>
                                        <td colspan="4" class="text-right"><b><?= __("İade Vergi:"); ?></b></td>
                                        <td colspan="1" id="return-tax">40E</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right"><b><?= __("İade Toplamı:"); ?></b></td>
                                        <td colspan="1" id="return-total">140E</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right"><b><?= __("İade Miktarı:"); ?></b></td>
                                        <td colspan="1" id="return-payable">40E</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea name="desc" class="form-control" rows="2" placeholder="<?= __("İade Açıklaması..."); ?>" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-offset-7 col-sm-5">
                                <div class="input-group"> 
                                    <select name="payment-type" class="form-control">
                                        <option value=""><?= __("Ödeme Türü"); ?></option>
                                        <option value="cash"><?= __("Nakit"); ?></option>
                                        <option value="credit"><?= __("Kart"); ?></option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="return-complete" class="btn btn-success btn-ripple"><i class="fa fa-check"></i> <?= __("TAMAMLA"); ?></button>
                                    </span>

                                </div>
                            </div>
                        </div>                     
                    </div>
                </div>
            </div>

            <input type="hidden" name="location_id" value="">
            <input type="hidden" name="saleid" value="">
            <input type="hidden" name="session" value="">
            
        </form>


    </div>
</div>



<div class="modal fade" id="return-product-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("İade"); ?></h4>
            </div>
            <div class="modal-body">

                <form id="return-form">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="<?= __("Kullanıcı/Eposta"); ?>" autocomplete="off">
                    </div>
                
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="<?= __("Şifre"); ?>" autocomplete="off">
                    </div>

                    <button type="button" class="btn btn-primary" id="return-stp1"><?= __("Kaydet"); ?></button>
                </form>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="activation-zipboox-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Activation Zip-boox Account</h4>
            </div>
            <div class="modal-body">

                <form>
                    <div class="form-group">
                        <input type="text" name="ticket" class="form-control text-center" placeholder="Ticket Number" autocomplete="off">
                    </div>
                    
                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('pointofsales/zipbooxTicket');?>">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>



<div class="modal modal-fullscreen fade in" id="table-select-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Masa Seç"); ?></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


<div class="modal modal-fullscreen fade in" id="modal-move-table">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Masa Taşı"); ?></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


<a id="completed-orders-modal" class="btn btn-success btn-ripple btn-xxl hidden" data-toggle="modal" href="#modal-completed-orders"><i class="fa fa-clock-o"></i> <?= __("Sipariş Hazır"); ?></a>
<div class="modal fade" id="modal-completed-orders">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Hazırlanan Siparişler"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th><?= __("Ürün"); ?></th>
                                <th><?= __("Adet"); ?></th>
                                <th><?= __("Masa"); ?></th>
                                <th><?= __("Personel"); ?></th>
                                <th><?= __("Oluşturuldu"); ?></th>
                                <th><?= __("İşlem"); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-sound-recorder" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __("Ses Notu"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 text-center">
                    <h2 class="counter">00:00:00</h2>
                </div>

                <div class="col-sm-6">
                    <button type="button" class="btn btn-blue btn-ripple btn-xxl" id="record"><i class="fa fa-microphone"></i> <span><?= __("Kayıt"); ?></span></button>
                </div>
                <div class="col-sm-6 text-right">
                    <button type="button" class="btn btn-red btn-ripple btn-xxl" id="moda-sound-record-close"><i class="fa fa-times"></i> <?= __("İptal"); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-session">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Oturum Yönetimi"); ?></h4>
            </div>
            <div class="modal-body">

                <form class="form-inline" id="form-session">
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="text" name="user" class="form-control" placeholder="<?= __("Kullanıcı/Eposta"); ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="password" name="password" class="form-control" placeholder="<?= __("Şifre"); ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Oturum Aç">
                        </div>
                    </div>
                </form>
                
                
                <?php if ($this->session->userdata('sub-sessions')): ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th colspan="3"><?= __("Açık Oturumlar"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->session->userdata('sub-sessions') as $id => $sub_session): ?>
                                <tr>
                                    <td><?= ($this->user->id == $id) ? '<span class="glyphicon glyphicon-check" aria-hidden="true"></span>' : '' ?></td>
                                    <td><?= $sub_session['name'] ?></td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-red unset_session" data-id="<?= $id ?>"><?= __("KAPAT"); ?></button>
                                        <button type="button" class="btn btn-blue select_session" data-id="<?= $id ?>"><?= __("SEÇ"); ?></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php endif; ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Kapat"); ?></button>
            </div>
        </div>
    </div>
</div>



<div id="pos-modal-note" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> <?= __("Not Ekle"); ?></h4>
      </div>
      <div class="modal-body">
        <form name="form" class="form-horizontal">
          <textarea id="text_note" class="form-control js-auto-size valid keypad" rows="7" placeholder="<?= __("Not"); ?>" style="font-size: 36px; line-height: 1;"></textarea>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><?= __("Kapat"); ?></button>
        <button id="pos-modal-note-submit" type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i> <?= __("Kaydet"); ?></button>
      </div>
    </div>
  </div>
</div>



<div id="adisyon-table" class="hidden">

    <div class="container-fluid" style="width: 8cm !important; font-size: 16px !important; overflow: hidden !important;">
        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="<?= base_url('assets/logo1.svg') ?>" class="img-responsive center-block" alt="Image" style="width: 50%;">
            </div>
            <div class="col-xs-12">
                <table>
                    <thead>
                        <tr>
                            <th colspan="3"><?= $this->user->name ?></th>
                        </tr>
                        <tr>
                            <th class="adisyon_date"></th>
                            <th colspan="2"></th>
                            <th class="text-right"><?= $table ? $table->title : null ?></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="text-right"><?= __("Miktar"); ?></th>
                            <th class="text-right"><?= __("Fiyat"); ?></th>
                            <th class="text-right"><?= __("Tutar"); ?></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="3"><?= __("Toplam"); ?></th>
                            <th class="adisyon_subtotal text-right"></th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3"><?= __("Ara Ödeme"); ?></th>
                            <th class="adisyon_interim_payment text-right"></th>
                        </tr>
                        <tr class="hidden">
                            <th class="text-right" colspan="3"><?= __("KDV"); ?></th>
                            <th class="adisyon_tax text-right"></th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3"><?= __("Genel Toplam"); ?></th>
                            <th class="adisyon_total text-right"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-xs-12">
                <?= @$invoice->footer ?>
            </div>
        </div>
    </div>

</div>




<div class="modal fade" id="modal-interim_payments">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                <h4 class="modal-title"><?= __("Ara Ödemeler"); ?></h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <h3 class="text-center">
                    <span id="interim_payments-total">0.00</span> <?= $location_currency ?>
                </h3>
                <div class="btn-group btn-group-justified interim_payments-submit" role="group">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-pink btn-ripple btn-xxl" data-payment="voucher"><i class="fa fa-gift"></i> <?= __("İKRAM"); ?></button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-yellow btn-ripple btn-xxl" data-payment="ticket"><i class="fa fa-credit-card-alt"></i> <?= $this->Globals->payment_types->ticket->title ?></button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-indigo btn-ripple btn-xxl" data-payment="sodexo"><i class="fa fa-credit-card-alt"></i> <?= $this->Globals->payment_types->sodexo->title ?></button>
                    </div>
                </div>
                <div class="btn-group btn-group-justified interim_payments-submit" role="group">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary btn-ripple btn-xxl" data-payment="credit"><i class="fa fa-credit-card"></i> <?= __("K.KART"); ?></button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-light-green btn-ripple btn-xxl" data-payment="cash"><i class="fa fa-money"></i> <?= __("NAKIT"); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Select Product Details -->
<div class="modal" id="modal-product_content">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span></span><i class="fa fa-spinner fa-spin"></i></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">

                    <div class="form-group">
                        <b class="control-label col-sm-2" style="text-align: left;"><?= __("Ürün:"); ?></b>
                        <div class="col-sm-10">
                            <select name="product_id" id="portions" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <b class="control-label col-sm-2" style="text-align: left;"><?= __("Adet:"); ?></b>
                        <div class="col-sm-10">
                            <input type="number" name="quantity" class="form-control" value="1">
                        </div>
                    </div>
                    
                    <div class="well well-sm">
                        <b><?= __("Malzemeler:"); ?></b>
                        <div class="checkbox" id="stocks">

                        </div>
                    </div>

                    <div class="well well-sm">
                        <b><?= __("İlave Ürünler:"); ?></b>
                        <div id="additional_products">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("İptal"); ?></button>
                <button type="button" class="btn btn-primary process_product"><?= __("Ekle"); ?></button>
            </div>
        </div>
    </div>
</div>



<style type="text/css">
    #pos .btn-xxl {
        font-size: 16px;
    }
    #pos .btn-xl {
        font-size: 16px;
        padding: 12px 32px;
    }
    .e-number-spin input[type=number]::-webkit-outer-spin-button,
    .e-number-spin input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .e-number-spin input[type=number] {
        -moz-appearance:textfield;
    }
</style>


<!-- Customer selection modal -->
<?php $this->load->view('pointofsales/elements/package_service'); ?>