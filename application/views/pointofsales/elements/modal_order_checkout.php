<div class="modal" id="modal-order-checkout">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" role="group" style="top: 100px;">
        <button id="print-invoice" type="button" class="btn btn-primary btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 50px;"><i class="fa fa-print"></i> YAZDIR</button>
        <button id="new-order" type="button" class="btn btn-green btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12" data-order-type ="" style="margin-top: 50px;"><i class="fa fa-check"></i> YENİ SİPARİŞ</button>
      </div>

      <div class="modal-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="border-top: 0; margin-top: 50px;">
      </div>
    </div>
  </div>
</div>

