<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group backButton">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Fatura"); ?>
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-primary invoice-print"><i class="fa fa-print"></i> <?= __("Yazdır"); ?></a>
            </div>
        </h3>
    </div>
        
    <div class="panel-body p-a-0">
        <div class="container">
            <div class="invoice">
                <div class="invoice-heading">
                    <div class="row date-row">
                        <div class="col-md-6 col-xs-6">
                            <img class="hidden" id="barcode" src="" alt="">
                            <img src="<?= $location_invoice_template->logo ?>" style="width:100px">
                        </div>
                        <div class="col-md-6 col-xs-6 invoice-id">
                            <h4>No: #<span class="invoice-no">000000</span></h4>
                            <h5 class="invoice-datetime">01 01 2001</h5>
                        </div>
                    </div>
                    <div class="row">
                        <?= $location_invoice_template->header ?>
                    </div>
                </div>
                <div class="invoice-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th><?= __("Ürün"); ?></th>
                                    <th><?= __("Miktar"); ?></th>
                                    <th><?= __("Vergi"); ?></th>
                                    <th><?= __("Fiyat"); ?></th>
                                    <th><?= __("Toplam"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                              
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right"><strong><?= __("Ara Toplam:"); ?></strong></td>
                                    <td class="invoice-subtotal">0.00</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right"><strong><?= __("Vergi:"); ?></strong></td>
                                    <td class="invoice-taxtotal">0.00</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right"><strong><?= __("İndirim:"); ?></strong></td>
                                    <td class="invoice-discounttotal">0.00</td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right"><strong><?= __("TOPLAM:"); ?></strong></td>
                                    <td class="invoice-amount">0.00</td>
                                </tr>
                                <tr>
                                    <td class="invoice-customer_name p-b-0" colspan="5"></td>
                                </tr>
                                <tr>
                                    <td class="invoice-customer_phone p-y-0" colspan="5"></td>
                                </tr>
                                <tr>
                                    <td class="invoice-customer_address p-t-0" colspan="5"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="row text-center" style="margin-top: -20px; font-weight: bold">
                    <?= $location_invoice_template->footer ?>
                </div>
            </div>
        </div>
    </div>
</div>