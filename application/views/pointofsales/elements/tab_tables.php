<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title p-l-1">
            <div class="btn-group hidden backButton">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Masa Seçiniz"); ?>
        </h3>
    </div>

    <div class="left-pos-buttons">
        <ul class="list-group">
            <li class="list-group-item"><button id="self-service-button" class="service-buttons btn btn-pink btn-light-red btn-xxl font-18px" data-service="self"><i class="fa fa-cutlery fa-2x"></i><br><?= __("SELF<br>SERVİS"); ?></button></li>
            <li class="list-group-item"><button id="package-service-button" class="service-buttons btn btn-orange btn-xxl font-18px" data-service="package"><i class="fa fa-truck fa-2x"></i><br><?= __("PAKET<br>SERVİS"); ?></button></li>
        </ul>  
    </div>
        
    <div class="panel-body p-a-0 p-l-1">
        <div id="tab_buttons_container"></div>
        <div id="tab_contents_container"></div>
    </div>
</div>