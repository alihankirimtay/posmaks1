<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group backButton">
                <a href="#" class="btn btn-danger openTab" data-tab="tables"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Servis Sipariş Yönetimi"); ?>
        </h3>
    </div>
        
    <div class="panel-body">
        
        <div class="row">
            <div class="col-sm-4">
                <div class="panel panel-warning" id="orders-ys">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center bg-deep-orange text-white"><?= __("Yemek Sepeti"); ?></h3>
                    </div>
                    <div class="panel-body p-a-1" >
                        <div class="row orders-ys package-orders-panel" >
                            <!-- Contents -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-primary" id="orders-pending">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center bg-blue"><?= __("Bekleyen Siparişler"); ?></h3>
                    </div>
                    <div class="panel-body p-a-1">
                        <div class="row orders package-orders-panel" >
                            <!-- Contents -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="panel panel-success" id="orders-shipped">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center bg-green text-white"><?= __("Dağıtımdaki Siparişler"); ?></h3>
                    </div>
                    <div class="panel-body p-a-1">
                        <div class="row orders">
                            <!-- Contents -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>