<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group">
                <a href="#tab-pos" data-toggle="tab" class="btn btn-danger"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Ara Ödeme Yap"); ?>
        </h3>
    </div>
        
    <div class="panel-body p-a-0">
            
        <div class="col-md-4">
            <!-- Unpaid products -->
            <table class="table table-hover interim_payments-unpaid">
                <thead>
                    <tr>
                        <th colspan="3" class="success"><i class="fa fa-plus"></i> <?= __("Ürün Seçin"); ?></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <!-- END - Unpaid products -->
        </div>


        <div class="col-md-4">
            <!-- Payable products(basket) -->
            <form>

                <div class="left-pos-buttons">
                    <ul class="list-group">
                        <?php foreach ($payment_types as $key => $payment_type): ?>
                        <?php if($payment_type->alias != "cari"): ?>
                            <li class="list-group-item">
                                <button type="button" class="btn <?= $payment_type->color ?> btn-ripple btn-xxl btn-block interim_payments-submit" data-payment="<?= $payment_type->alias ?>">
                                    <i class="<?= $payment_type->icon ?>"></i><br><?= $payment_type->title ?>
                                </button>
                            </li>
                        <?php endif; ?>
                        <?php endforeach; ?>
                        <li class="list-group-item"><button type="button" class="btn btn-pink btn-ripple btn-xxl btn-block interim_payments-submit" data-payment="gift"><i class="fa fa-gift"></i> <?= __("İKRAM"); ?></button></li>


                    </ul>  
                </div>
                <table class="table table-hover interim_payments-payable">
                    <thead>
                        <tr>
                            <th colspan="3" class="danger"><i class="fa fa-cart-plus"></i> <?= __("Seçilen Ürünler"); ?></th>
                        </tr>
                    </thead>
                    <tbody>    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="interim_payments-info">
                                <h2 class="text-center p-y-3"><i class="fa fa-cart-plus fa-2x"></i><br><?= __("Ürün Seçiniz"); ?></h2>
                            </td>
                        </tr> 
                        <tr>
                            <th colspan="2">
                                <h2><?= __("Toplam:"); ?></h2>
                            </th>
                            <th colspan="1" class="text-right p-r-2">
                                <h2 class="interim_payments-total">0.00</h2>
                            </th>
                        </tr>
                        <tr>
                            <!-- <td colspan="3" class="interim_payments-submit">
                                <?php foreach ($payment_types as $key => $payment_type): ?>
                                    <button type="button" class="btn btn-light-blue btn-ripple btn-xxl btn-block " data-payment="<?= $payment_type->alias ?>">
                                        <i class="<?= $payment_type->icon ?>"></i> <?= $payment_type->title ?>
                                    </button>
                                <?php endforeach; ?>
                                <button type="button" class="btn btn-pink btn-ripple btn-xxl btn-block" data-payment="gift"><i class="fa fa-gift"></i> İKRAM</button>
                            </td> -->
                        </tr>
                    </tfoot>
                </table>
                <input type="hidden" name="point_id" value="">
                <input type="hidden" name="sale_id" value="">
                <input type="hidden" name="payment_type" value="">
            </form>
            <!-- END - Payable products(basket) -->
        </div>

        
        <div class="col-md-4">
            <!-- Paid products -->
            <table class="table table-hover interim_payments-paid">
                <thead>
                    <tr>
                        <th colspan="3" class="info"><i class="fa fa-history"></i> <?= __("Geçmiş Ödemeler"); ?></th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
            <!-- END - Paid products -->
        </div>

    </div>
</div>