<div class="modal" id="modal_ys_reject_note" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="reject-form" class="form-horizontal" role="form">
	            <div class="modal-body">
	            	<textarea id="text-note" name="note" class="form-control note-height keypad keypad-textarea" rows="4" placeholder="<?= __("Notunuz..."); ?>"></textarea>
	            </div>
	            <div class="modal-footer">
	            	<div class="btn-group btn-group-justified">
	                    <a class="btn btn-danger btn-xxl" data-dismiss ="modal"><?= __("İptal"); ?></a>
	                    <a id="checkNote" class="btn btn-success btn-xxl"><?= __("Kaydet"); ?></a>
	                </div>
	            </div>
	            <div id="hiddenInput">
	            	<input type="hidden" name="order_id"></input>
	            </div>
	        </form>
    	</div>
   	</div>
</div>