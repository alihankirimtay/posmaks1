<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <?= __("Sipariş Fişi"); ?>
            <div class="btn-group pull-right">
                <a href="#" class="btn btn-primary invoice-print"><i class="fa fa-print"></i> <?= __("Yazdır"); ?></a>
            </div>
        </h3>
    </div>
        
    <div class="panel-body p-a-0">
        <div class="container-fluid" style="padding:0px !important;">
            <div class="invoice" style="padding:0px !important;">
                <div class="invoice-heading">
                    <div class="row date-row">
                        <div class="col-md-12 col-xs-12 text-center">
                            <h4 class="invoice-production-title">Production Name</h4>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <h4 class="invoice-point">POINT NAME</h4>
                            <h5 class="invoice-zone">ZONE NAME</h5>
                            <h5 class="invoice-username">USER NAME</h5>
                        </div>
                        <div class="col-md-5 col-xs-5 invoice-id">
                            <h4><?= __('No') ?>: #<span class="invoice-no">000000</span></h4>
                            <h5 class="invoice-datetime">01 01 2001</h5>
                        </div>
                    </div>
                </div>
                <div class="invoice-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?= __('Ürün') ?></th>
                                <th class=""><?= __('Miktar') ?></th>
                                <th class="">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?= __("Ürün Adı"); ?>
                                    <p class="invoice-additional_title"></p>
                                </td>
                                <td class="">quantity</td>
                                <td class="">&nbsp;</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="invoice-customer_name p-b-0" colspan="3"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_phone p-y-0" colspan="3"></td>
                            </tr>
                            <tr>
                                <td class="invoice-customer_address p-t-0" colspan="3"></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>