<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="btn-group backButton">
                <a href="#" class="btn btn-danger openPreviousTab"><i class="fa fa-arrow-left"></i> <?= __("Geri"); ?></a>
            </div>
            <?= __("Hazır Siparişler"); ?>
        </h3>
    </div>
        
    <div class="panel-body">
        <div class="container-fluid">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th><?= __("Adet"); ?></th>
                        <th><?= __("Ürün"); ?></th>
                        <th><?= __("Malzemeler"); ?></th>
                        <th><?= __("İlave Ürünler"); ?></th>
                        <th><?= __("Masa"); ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="call-accounts">

                </tbody>

                <tbody id="products">

                </tbody>
            </table>
        </div>
    </div>
</div>