<div class="modal full-height" id="modal-shipping_users">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?= __("Kurye Seçiniz"); ?></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">					
					
					<?php if (!empty($couriers)): ?>
						<?php foreach ($couriers as $courier): ?>
							<div class="btn btn-primary btn-xxl btn-block courier">
								<input type="radio" name="courier_id" value="<?= $courier->id ?>" class="hidden">
								<?= $courier->name ?>
							</div>
						<?php endforeach; ?>
					<?php else: ?>
						<div class="well well-lg text-center">
							<h3><i class="fa fa-exclamation-triangle fa-2x"></i><br><?= __("Kurye Bulunamadı."); ?></h3>
						</div>
					<?php endif; ?>

					<input type="hidden" name="point_id" value="">
				</form>
			</div>
		</div>
	</div>
</div>