<div class="modal" id="modal_ys_detail" data-keyboard="false" data-backdrop="static">
    <form id="ys-order">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
            	<div class="modal-header col-md-12">
            		<span class="pull-left customer-name"><?= __("Müşteri Adı"); ?></span>
            		<span class="pull-right delivery-time"><?= __("Teslim Tarihi"); ?></span>
            	</div>
                <div class="modal-body col-md-12">
                	<table id="tblYsProduct" class="table table-striped table-hover col-md-12">
                        <thead>
                            <tr>
                                <th><?= __("Ürün Adı"); ?></th>
                                <th><?= __("Adet"); ?></th>
                                <th><?= __("Malzemeler"); ?></th>
                                <th><?= __("İlave Ürün"); ?></th>
                                <th><?= __("Fiyat"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                    <div class="unsync">
                        <p style="font-weight: bold"><?= __("Eşleşmeyen ürünler ve özellikler"); ?></p>
                        <span></span>
                    </div>
                </div>
                <div class="modal-footer">
                	<div class="adress text-left col-md-6">
                		<label class=""><b><?= __("Adres :"); ?> </b></label>
                		<textarea class="text-left customer-address" style="width: 100%"><?= __("Müşteri Adresi"); ?></textarea>
                	</div>
                    <div class="adress-desc text-left col-md-6">
                        <label class=""><b><?= __("Adres Tarifi :"); ?> </b></label>
                        <textarea class="text-left customer-address-desc" style="width: 100%"></textarea>
                    </div>
                	<div class="adress text-left col-md-6">
                		<label class=""><b><?= __("Not :"); ?> </b></label>
                		<textarea class="text-left payment-note" style="width: 100%"><?= __("Not"); ?></textarea>
                	</div>
                	<div class="adress text-left col-md-6">
                		<label class=""><b><?= __("Sipariş Notu :"); ?> </b></label>
                		<textarea class="text-left order-note" style="width: 100%"><?= __("Sipariş Notu"); ?></textarea>
                	</div>

                	<!-- <p class="pull-right order-total">Order Total</p> -->

                	<div class="btn-group btn-group-justified col-md-12">
                        <a class="btn btn-danger btn-xxl col-md-6" data-dismiss ="modal"><?= __("KAPAT"); ?></a>
                        <a href="#" class="btn btn-success btn-ripple btn-lg col-md-6 check"><i class="fa fa-check"></i> <?= __("ONAYLA"); ?></a>
                    </div>
                </div>
                <div id="hiddenInput">
                    <input type="hidden" name="order_id"></input>
                    <input type="hidden" name="customer_id"></input>
                    <input type="hidden" name="name"></input>
                    <input type="hidden" name="phone"></input>
                    <input type="hidden" name="address"></input>
                	<input type="hidden" name="point_id"></input>
                </div>
        	</div>
       	</div>
   </form>
</div>