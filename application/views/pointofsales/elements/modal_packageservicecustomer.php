<div class="modal full-height" id="modal-package_service_customer">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?= __("Paket Servis"); ?></h4>
			</div>
			<div class="modal-body h--150">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<b><?= __("Telefon:"); ?></b>
						<input type="hidden" name="phone">
						<input type="text" name="phone" class="form-control bg-light-grey1 input-lg p-l-1" value="" autocomplete="off">
					</div>
					<div class="form-group">
						<b><?= __("Ad Soyad:"); ?></b>
						<input type="text" name="name" class="form-control bg-light-grey1 input-lg p-l-1" value="" autocomplete="off">
					</div>
					<div class="form-group">
						<b><?= __("Adres:"); ?></b>
						<textarea type="text" name="address" class="form-control bg-light-grey1 p-l-1" rows="3"></textarea>
					</div>
				</form>
				<table class="table table-hover">
					<tbody class="customerlist">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-xxl" data-dismiss="modal"><?= __("İptal"); ?></button>
				<button type="button" class="btn btn-primary btn-xxl" id="create_customer"><?= __("Yeni Müşteri Ekle"); ?></button>
			</div>
		</div>
	</div>
</div>