<div id="numpad" class="content-center" style="background-image : url(<?= base_url($this->website->screen_saver) ?>);">
        
    <div class="container">
        <div class="rowv m-t-3 p-t-3">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                <form role="form">
                    
                    <div class="form-group bg-white m-b-0">
                        <input name="password" type="password" class="form-control input-lg2 text-center fa-5x" readonly>
                    </div>
                    
                    <div class="nums">
                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="1">1</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="2">2</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="3">3</a>
                        </div>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="4">4</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="5">5</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="6">6</a>
                        </div>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="7">7</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="8">8</a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="9">9</a>
                        </div>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-blue-grey btn-ripple btn-xxl" data-value="delete"><i class="fa fa-long-arrow-left"></i></a>
                            <a href="#" class="btn btn-primary btn-ripple btn-xxl" data-value="0">0</a>
                            <a href="#" class="btn btn-success btn-ripple btn-xxl" data-value="submit"><i class="fa fa-check"></i></a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    
</div>