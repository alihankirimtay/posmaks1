<form id="return">

    <input type="hidden" name="sale_id" value="">
    <input type="hidden" name="session" value="">

    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">
                <?= __("İade"); ?>
            </h3>
        </div>
        

        <!-- SUMMARY -->
        <div class="col-sm-5">
            <div class="panel">

                <div class="panel-body" style="height: 270px;  overflow-y: auto; padding: 0;">              

                    <div class="table">
                        <table class="table table-condensed table-striped">
                            <thead>
                                <tr>
                                    <th class="small col-sm-2"><?= __("Adet"); ?></th>
                                    <th class="small col-sm-6"><?= __("Ürün"); ?></th>
                                    <th class="small col-sm-2 text-right"><?= __("Fiyat"); ?></th>
                                    <th class="small col-sm-2 text-right"><?= __("Toplam"); ?></th>
                                </tr>
                            </thead>
                            <tbody id="return-basket">

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel-body p-a-0" id="return-summary">
                    <div class="col-sm-12">
                        <ul class="list-group">
                            <li class="list-group-item text-right small p-y-0">
                                <span class="pull-left"><?= __("Aratoplam:"); ?></span> <strong id="subtotal" class="small">0.00</strong>
                            </li>
                            <li class="list-group-item text-right small p-y-0">
                                <span class="pull-left"><?= __("Toplam Vergi:"); ?></span> <strong id="tax-total" class="small">0.00</strong>
                            </li>
                            <li class="list-group-item text-right small p-y-0">
                                <span class="pull-left"><?= __("İndirim:"); ?></span> <strong id="discount-total" class="small">0.00</strong>
                            </li>
                            <li class="list-group-item text-center p-y-0">
                                <h3 class="m-b-0"><strong id="total">0.00</strong></h3>
                            </li>
                            <li class="list-group-item p-a-0">
                                <div data-toggle="buttons">
                                    <?php foreach ($grouped_payment_types as $grouped_payment_type): ?>
                                        <div class="btn-group btn-group-justified">
                                            <?php foreach ($grouped_payment_type as $payment_type): ?>
                                                <label class="btn <?= $payment_type->color ?> btn-light-blue-active btn-ripple btn-xxl p-x-0">
                                                    <input type="radio" name="payment_type" value="<?= $payment_type->alias ?>">
                                                    <i class="<?= $payment_type->icon ?>"></i> 
                                                    <?= $payment_type->title ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </li>
                            <li class="list-group-item bg-light-grey1">
                                <textarea name="desc" class="form-control" rows="3" placeholder="<?= __("Açıklama giriniz..."); ?>"></textarea>
                            </li>
                            <li class="list-group-item p-x-0">
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button id="return-cancel" class="btn btn-success btn-red btn-ripple btn-xxl"><i class="fa fa-ban"></i> <?= __("İPTAL"); ?></button>
                                    </div>
                                    <div class="btn-group">
                                        <button id="return-complete" class="btn btn-success btn-ripple btn-xxl"><i class="fa fa-check"></i> <?= __("İADE ET"); ?></button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- SUMMARY -->


        <!-- LIST -->
        <div class="col-sm-7">
            <div class="panel">

                
                <!-- PRODUCTS -->
                <div class="panel-body p-a-0">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th class="small col-sm-2 p-l-1"><?= __("Adet"); ?></th>
                                <th class="small col-sm-6 p-l-1"><?= __("Ürün"); ?></th>
                                <th class="small col-sm-4 text-right p-r-2"><?= __("Fiyat"); ?></th>
                            </tr>
                        </thead>
                        <tbody id="return-products">

                        </tbody>
                    </table>
                </div>
                <!-- PRODUCTS -->

            </div>
        </div>
        <!-- LIST -->
        
        
    </div>
</form>