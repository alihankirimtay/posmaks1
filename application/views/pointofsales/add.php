
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1>New Sale <small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('sales'); ?>">Sales</a></li>
                    <li><a href="#" class="active">New Sale</a></li>
                </ol>
            </div>
        </div> 
    </div>
    <div class="row">
            <div class="col-md-12">
                <div class="panel bs-wizard bs-wizard-steps">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>New Sales</h4>
                                
                            </div><!--.panel-title-->
                        </div><!--.panel-heading-->
                        <div class="panel-body">

                            <form id="form2" method="post" action="#" class="" novalidate="novalidate">

                                <div class="tab-content">
                                    <div class="tab-pane active" id="form2_tab1">

                                        <div class="form-group">
                                            <div class="inputer">
                                                <div class="input-wrapper">
                                                    <input type="text" name="ticked" class="form-control" placeholder="Ticket" required="" aria-required="true">
                                                </div>
                                            </div>
                                        </div><!--.form-group-->


                                        <div class="row">
                                        <br/>
                                            <div class="form-group">
                                            <label class="col-md-12">Select Location</label>
                                                <div class="col-md-5">
                                                    <select name="location" class="chosen-select">
                                                        <?php foreach($this->user->locations_array as $location): ?>
                                                            <option value="<?= $location['id']; ?>"><?= $location['title']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div><!--.form-group-->
                                        </div>


                                        <div class="row pack-list">
                                        <?php foreach($packs as $pack): ?>
                                            <div class="col-md-3 packs">
                                            <img style="position: absolute;z-index: 2;width: 50px; top: 45px; left:45px;" src="https://cdn2.iconfinder.com/data/icons/minimalism/128/HDD_USB_White.png">
                                            <input type="radio" id="prd-<?= $pack->id;?>" required="" aria-required="true" name="product" value="<?= $pack->id?>" style="opacity: 0;">
                                            <label style="width: 100%;" for="prd-<?=$pack->id?>">
                                                <div class="card card-pricing card-pricing-dark">

                                                    <div class="card-heading">
                                                        <h4><?= $pack->title; ?></h4>
                                                        <h5>
                                                            <span><i class="currency">$</i><i class="price"><?= $pack->price?></i> <i class="renewal"></i></span>
                                                        </h5>
                                                    </div><!--.card-heading-->

                                                    <div class="card-body">
                                                        <span style="margin-top: 15px; padding: 0px 12px 0px 12px; margin-bottom: 15px; max-height: 140px; min-height: 140px; overflow: hidden;" class="text-center"><?= $pack->desc; ?></span>
                                                    </div><!--.card-body-->

                                                    <div class="card-footer">
                                                        <a href="#" class="btn btn-block <?= ($pack->type == 'product' ? 'btn-success' : 'btn-primary'); ?> btn-ripple"><?= strtoupper($pack->type)?></a>
                                                    </div><!--.card-footer-->

                                                </div><!--.card-->

                                                </label>
                                                
                                            </div><!--.col-md-3--> 
                                        <?php endforeach; ?>


                                        </div>
                                    </div><!--.tab-pane-->

                                    <div class="tab-pane" id="form2_tab2">
                                        <div class="col-md-12">
                                            <div class="panel indigo">
                                                <div class="panel-heading">
                                                    <div class="panel-title"><h4>Payment Type</h4></div>
                                                </div><!--.panel-heading-->
                                                <div class="panel-body text-center">
                                                <?php foreach($payments as $payment): ?>
                                                    <div class="col-md-4 col-md-offset-1">
                                                    <label class="col-md-12" for="<?=$payment->title?>">
                                                        <img src=" <?= $payment->icon; ?>">
                                                        <br/>
                                                        <?= $payment->title ?>
                                                        <br/>
                                                        </label>
                                                        <input type="radio" id="<?= $payment->title ?>" class="icheck-square-green" checked="checked" name="payment-type" value="<?= $payment->id ?>">
                                                    </div>

                                                <?php endforeach; ?>
                                                    
                                                </div><!--.panel-body-->
                                            </div><!--.panel-->
                                        </div>

                                        <div class="col-md-12">
                                            <div class="panel indigo">
                                                <div class="panel-heading">
                                                    <div class="panel-title"><h4>Discount</h4></div>
                                                </div><!--.panel-heading-->
                                                <div class="panel-body">
                                                    

                                                <div class="container">
                                                    <div class="row vertical-offset-100">
                                                        <div class="col-md-4 col-md-offset-4 discount">
                                                            <div class="panel form-signin">
                                                                <div class="panel-heading">
                                                                <p></p>                               
                                                                    <img src="https://cdn0.iconfinder.com/data/icons/winter-lollipop/128/Map.png" class="img-responsive text-center" alt="Conxole Admin"/>
                                                                    <strong class="text-center"> Manager Login</strong>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <form accept-charset="UTF-8" role="form">
                                                                        <fieldset>
                                                                            <input class="form-control" placeholder="E-mail" id="M_email" type="text">
                                                                            <input class="form-control" placeholder="Password" id="M_pass" type="password">
                                                                            <a class="btn btn-lg btn-success btn-block"  href="javascript:void(0)" id="mLogin" >Login</a>
                                                                        </fieldset>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div><!--.panel-body-->
                                        </div><!--.panel-->
                                    </div>                                        
                                </div><!--.tab-pane-->

                                <div class="tab-pane" id="form2_tab3">
                                    <div class="col-md-12">

                                        <div class="invoice">
                                            <div class="invoice-body">

                                                <div class="container">
                                                    <div class="row">
                                                        <div class="span4 well">
                                                            <table class="invoice-head">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="pull-right"><strong>Ticket #</strong></td>
                                                                        <td id="ticket"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="pull-right"><strong>Date</strong></td>
                                                                        <td><?= date('d.m.Y'); ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="pull-right"><strong>Payment Type</strong></td>
                                                                        <td id="payment_type"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="span8">
                                                            <h2>Invoice</h2>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="span8 well invoice-body">
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Product</th>
                                                                        <th>Type</th>
                                                                        <th>Amount</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td id="product"></td>
                                                                        <td id="type"></td>
                                                                        <td id="amounth"></td>
                                                                    </tr><tr>
                                                                    <td>&nbsp;</td>
                                                                    <td><strong>Discount</strong></td>
                                                                    <td><strong id="discount"></strong></td>
                                                                    <td><strong>Total</strong></td>
                                                                    <td><strong id="total"></strong></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                        </div><!--.invoice-body-->
                                    </div><!--.invoice-->

                                </div><!--.tab-pane-->
                            </div><!--.tab-content-->

                        </form>

                        </div><!--.panel-body-->
                        <div class="panel-footer">
                            <ul class="wizard clearfix">
                                <li class="bs-wizard-prev pull-left disabled" style="display: none;"><button name="back" class="btn  btn-warning btn-ripple">Previous</button></li>
                                <li class="bs-wizard-next pull-right"><button name="next" class="btn btn-success btn-ripple">Next</button></li>
                                <li class="bs-wizard-next pull-right" style="display: none;"><button type="submit" class="btn btn-blue btn-ripple">Complete setup</button></li>
                            </ul>
                        </div><!--.panel-footer-->
                </div><!--.panel-->
            </div><!--.col-md-6-->

            
        </div>

</div>
