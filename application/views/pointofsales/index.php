<?php

    $i = 0;
    $grouped_payment_types = [];
    foreach ($payment_types as $key => $payment_type) {
        if ($key % 3 === 0) $i++;
        $grouped_payment_types[$i][$payment_type->id] = $payment_type;
    }

?>
<div class="content">

    <div class="page-header full-content m-b-0">
        <div class="row m-x-0 p-x-0" style="background: white;">
            <div class="col-xs-12 col-md-5">
                <div id="pos-details" style="line-height: 2;">
                    <div class="col-md-4">
                        <div class=""><b><?= __('OTURUM'); ?></b> : <span id="pos-username"></span></div>
                        <div class=""><b><?= __('MASA'); ?> :</b> <span id="pos-tablename"></span></div>
                    </div>
                    <div class="col-md-3 text-right">
                        <img id="pos-courier-img" src="<?= asset_url('kurye.svg'); ?>" style="height: 75px;"></img>
                    </div>
                    <div class="col-md-5">
                        <div class="hidden" id="pos-customer"><b><?= __('Müşteri'); ?> :</b> <span></span></div>
                        <div class="hidden" id="pos-customer-tel"><b><?= __('Tel'); ?> :</b> <span></span></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7 text-right p-r-0 p-l-0 theme-dark-blue">
                <!-- <button class="btn btn-primary btn-md openTab" data-tab="packageserviceorders"><i class="fa fa-truck"></i> PAKET SERVİS TAKİP</button> -->
                <button href="#" class="btn btn-pink btn-ripple btn-lg pull-left" id="open_package_service_customer"><i class="fa fa-map-marker fa-2x"></i></button>
               <!--  <button id="handle-adisyon" class="btn btn-primary btn-md"><i class="fa fa-file-text-o" aria-hidden="true"></i> ADİSYON</button>  -->
                <button href="#" class="btn btn-orange btn-ripple btn-md openTab" data-tab="tables"><i class="fa fa-square-o"></i> <?= __("MASALAR"); ?></button>
                <button id="ordertrack_notification" class="btn btn-primary btn-md openTab disabled" data-tab="ordertrack"><?= __("SİPARİŞLER"); ?> <span class="text-center bg-red count">0</span></button>
                <button id="exit_operator_session" class="btn btn-deep-red btn-lg"><i class="fa fa-lock"></i></button>
            </div>
        </div> 
    </div>

    <div class="hidden">
        <a href="#tab-pos" data-toggle="tab">[pos]</a>
        <a href="#tab-tables" data-toggle="tab">[tables]</a>
        <a href="#tab-movetable" data-toggle="tab">[movetable]</a>
        <a href="#tab-interimpayments" data-toggle="tab">[interimpayments]</a>
        <a href="#tab-invoice" data-toggle="tab">[invoice]</a>
        <a href="#tab-adisyon" data-toggle="tab">[adisyon]</a>
        <a href="#tab-return" data-toggle="tab">[return]</a>
        <a href="#tab-returninvoice" data-toggle="tab">[returninvoice]</a>
        <a href="#tab-operatorsession" data-toggle="tab">[operatorsession]</a>
        <a href="#tab-cashclosing" data-toggle="tab">[cashclosing]</a>
        <a href="#tab-ordertrack" data-toggle="tab">[ordertrack]</a>
        <a href="#tab-packageserviceorders" data-toggle="tab">[packageserviceorders]</a>
    </div>

    <div class="row no-gutters m-y--40 tab-content p-a-0" id="main-tabs">
        <div class="tab-pane" id="tab-pos">
            <?php $this->load->view('pointofsales/elements/tab_pos'); ?>
        </div>
        <div class="tab-pane active" id="tab-tables">
            <?php $this->load->view('pointofsales/elements/tab_tables'); ?>
        </div>
        <div class="tab-pane" id="tab-movetable">
            <?php $this->load->view('pointofsales/elements/tab_movetable'); ?>
        </div>
        <div class="tab-pane" id="tab-interimpayments">
            <?php $this->load->view('pointofsales/elements/tab_interimpayments'); ?>
        </div>
        <div class="tab-pane" id="tab-invoice">
            <?php $this->load->view('pointofsales/elements/tab_invoice'); ?>
        </div>
        <div class="tab-pane" id="tab-adisyon">
            <?php $this->load->view('pointofsales/elements/tab_adisyon'); ?>
        </div>
        <div class="tab-pane" id="tab-return">
            <?php $this->load->view('pointofsales/elements/tab_return', compact('grouped_payment_types')); ?>
        </div>
        <div class="tab-pane" id="tab-returninvoice">
            <?php $this->load->view('pointofsales/elements/tab_returninvoice'); ?>
        </div>
        <div class="tab-pane" id="tab-operatorsession">
            <?php $this->load->view('pointofsales/elements/tab_operatorsession'); ?>
        </div>
        <div class="tab-pane" id="tab-cashclosing">
            <?php $this->load->view('pointofsales/elements/tab_cashclosing'); ?>
        </div>
        <div class="tab-pane" id="tab-ordertrack">
            <?php $this->load->view('pointofsales/elements/tab_ordertrack'); ?>
        </div>
        <div class="tab-pane" id="tab-packageserviceorders">
            <?php $this->load->view('pointofsales/elements/tab_packageserviceorders'); ?>
        </div>
    </div>
</div>


<!-- Select Product Details -->
<div class="modal full-height" id="modal-product_content">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body h--150">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <b class="control-label col-sm-2" style="text-align: left;"><?= __("Ürün"); ?>:</b>
                        <div class="col-sm-10">
                            <select name="product_id" id="portions" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <b class="control-label col-sm-2" style="text-align: left;"><?= __("Adet"); ?>:</b>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="number" name="quantity" class="form-control" value="1" step="1" min="1">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-ripple btn-danger btn-xl minus">-</button>
                                    <button type="button" class="btn btn-ripple btn-success btn-xl plus">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="well well-sm">
                        <b><?= __("Malzemeler"); ?>:</b>
                        <div class="checkbox" id="stocks">

                        </div>
                    </div>
                    <div class="well well-sm">
                        <b><?= __("Menü İçeriği"); ?>:</b>
                        <div id="package_products">
                        </div>
                    </div>
                    <div class="well well-sm">
                        <b><?= __("İlave Ürünler"); ?>:</b>
                        <div id="additional_products">
                        </div>
                    </div>
                    <div class="well well-sm">
                        <b><?= __("Ürün Notları"); ?>:</b>
                        <div class="list-group" id="quick_notes">
                        </div>
                        <div class="list-group" id="product_notes">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <span class="input-group-btn pull-left">
                    <button type="button" class="btn btn-ripple btn-grey btn-xxl" id="create-soundnote"><i class="fa fa-microphone"></i></button>
                    <button type="button" class="btn btn-ripple btn-grey btn-xxl" id="create-textnote"><i class="fa fa-pencil-square-o"></i></button>
                </span>
                <button type="button" class="btn btn-danger btn-xxl" data-dismiss="modal"><?= __("İptal"); ?></button>
                <button type="button" class="btn btn-primary btn-xxl process_product"><?= __("Ekle"); ?></button>
            </div>
        </div>
    </div>
</div>



<!-- Product Notes Modal -->
<div class="modal" id="modal-product_note" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __("Not Ekle"); ?></h4>
            </div>
            <div class="modal-body">

                <div class="hidden">
                    <a href="#notesound" data-toggle="tab"><?= __("Sesli Not"); ?></a>
                    <a href="#notetext" data-toggle="tab"><?= __("Not"); ?></a>
                </div>
            
                <div class="tab-content p-a-0">
                    <div class="tab-pane" id="notesound">
                        <h1 class="text-center note-height">
                            <b class="timer">00:00:00</b><br>
                            <i class="fa fa-microphone fa-flash"></i>
                        </h1>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-danger btn-xxl cancel"><?= __("İptal"); ?></a>
                            <a href="#" class="btn btn-success btn-xxl save"><?= __("Kaydet"); ?></a>
                        </div>
                    </div>

                    <div class="tab-pane" id="notetext">
                        <textarea name="note" class="form-control note-height keypad keypad-textarea" rows="4" placeholder="<?= __('Notunuz...') ?>"></textarea>

                        <div class="btn-group btn-group-justified">
                            <a href="#" class="btn btn-danger btn-xxl cancel"><?= __("İptal"); ?></a>
                            <a href="#" class="btn btn-success btn-xxl save"><?= __("Kaydet"); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Return Login Modal -->
<div class="modal" id="modal-return_login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Ürün İadesi Yetkili Girişi"); ?></h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="location_id">
                    <div class="form-group">
                        <input type="text" name="sale_id" class="form-control input-lg" placeholder="<?= __("#Satış No"); ?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="text" name="username" class="hidden">
                        <input type="text" name="username" class="form-control input-lg" placeholder="<?= __("Kullanıcı Adı veya Eposta"); ?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control input-lg" placeholder="<?= __("Şifre"); ?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-xxl btn-block" id="submit-return-login"><?= __("Giriş"); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Discount Login Modal -->
<div class="modal" id="modal-discount_login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("İndirim Oluştur"); ?></h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label class=""><?= __('Yüzdelik Seçimi') ?></label>
                        <div class="btn-group btn-group-justified">
                            <a class="percent-buttons first btn btn-light-blue btn-xxl">5</a>
                            <a class="percent-buttons btn btn-light-green btn-xxl">10</a>
                            <a class="percent-buttons btn btn-danger btn-xxl">15</a>
                            <a class="percent-buttons btn btn-warning btn-xxl">20</a>
                        </div>
                        <div class="input-group">
                            <input type="number" name="discount_amount" class="form-control input-lg" placeholder="<?= __("İndirim miktarı"); ?>" autocomplete="off">
                            <span class="input-group-btn">
                                <select name="discount_type" class="form-control-static btn btn-lg">
                                    <option value="percent">%</option>
                                    <option value="amount"><?= $location['currency'] ?></option>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="username" class="hidden">
                        <div id="username-password-group">
                            <input type="text" name="username" class="form-control input-lg" placeholder="Kullanıcı Adı veya Eposta" autocomplete="off">
                            <input type="password" name="password" class="form-control input-lg" placeholder="Şifre" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-xxl btn-block" id="submit-discount-login"><?= __('İndirim Ekle') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<!-- Modal Package Service -->
<?php $this->load->view('pointofsales/elements/modal_packageservicecustomer'); ?>


<!-- Modal Shipping Users -->
<?php $this->load->view('pointofsales/elements/modal_shippingusers'); ?>


<!-- Modal YS Reject Note -->
<?php $this->load->view('pointofsales/elements/modal_ys_reject_note'); ?>

<!-- Modal YS Order Detail -->
<?php $this->load->view('pointofsales/elements/modal_ys_detail'); ?>

<!-- Modal After Order Checkout -->
<?php $this->load->view('pointofsales/elements/modal_order_checkout'); ?>







































































<div class="modal" id="modal-case">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="btn-group col-lg-12 col-md-12 col-sm-12 col-xs-12" role="group">
          <div class="btn-group" role="group">
            <a href="#" id="cancelSave" data-dismiss="modal" class="btn btn-orange btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12"><h4 class="text-white"><i class="fa fa-times"></i> <?= __('Masa İptali') ?></h4></a>
            <a href="#modal-return_login" data-toggle="modal" data-dismiss="modal" class="btn btn-danger btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12"><h4 class="text-white"><i class="fa fa-undo"></i> <?= __('Ürün İadesi') ?></h4></a>
            <a href="#" data-dismiss="modal" class="btn btn-blue btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12 openTab" data-tab="cashclosing"><h4 class="text-white"><i class="fa fa-money"></i> <?= __('Kasa Sayımı') ?></h4></a>
            <a href="#" data-dismiss="modal" id="change_pos" class="btn btn-green btn-ripple btn-xxl col-lg-12 col-md-12 col-sm-12 col-xs-12"><h4 class="text-white"><i class="fa fa-power-off"></i> <?= __('Kasayı Değiştir') ?></h4></a>
          </div>
        </div>

      </div>
      <div class="modal-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="border-top: 0; margin-top: 100px;">
        <img class="modal-img-close" src="<?= asset_url('cancel.svg'); ?>" style="max-height: 50px;" data-dismiss="modal"></img>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="activation-zipboox-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Activation Zip-boox Account</h4>
            </div>
            <div class="modal-body">

                <form>
                    <div class="form-group">
                        <input type="text" name="ticket" class="form-control text-center" placeholder="Ticket Number" autocomplete="off">
                    </div>
                    
                    <button type="submit" class="btn btn-blue" name="dosubmit" data-url="<?= base_url('pointofsales/zipbooxTicket');?>">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>



<div class="modal modal-fullscreen fade in" id="table-select-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Masa Seç') ?></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


<div class="modal modal-fullscreen fade in" id="modal-move-table">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Masa Taşı') ?></h4>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


<a id="completed-orders-modal" class="btn btn-success btn-ripple btn-xxl hidden" data-toggle="modal" href="#modal-completed-orders"><i class="fa fa-clock-o"></i> <?= __('Sipariş Hazır') ?></a>
<div class="modal fade" id="modal-completed-orders">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Hazırlanan Siparişler') ?></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th><?= __('Ürün') ?></th>
                                <th><?= __('Adet') ?></th>
                                <th><?= __('Masa') ?></th>
                                <th><?= __('Personel') ?></th>
                                <th><?= __('Oluşturuldu') ?></th>
                                <th><?= __('İşlem') ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modal-sound-recorder" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __('Ses Notu') ?></h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 text-center">
                    <h2 class="counter">00:00:00</h2>
                </div>

                <div class="col-sm-6">
                    <button type="button" class="btn btn-blue btn-ripple btn-xxl" id="record"><i class="fa fa-microphone"></i> <span><?= __('Kayıt') ?></span></button>
                </div>
                <div class="col-sm-6 text-right">
                    <button type="button" class="btn btn-red btn-ripple btn-xxl" id="moda-sound-record-close"><i class="fa fa-times"></i> <?= __('İptal') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-session">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Oturum Yönetimi') ?></h4>
            </div>
            <div class="modal-body">

                <form class="form-inline" id="form-session">
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="text" name="user" class="form-control" placeholder="<?= __('Kullanıcı/Eposta') ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="password" name="password" class="form-control" placeholder="<?= __('Şifre') ?>">
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <div class="col-sm-12">
                            <input type="submit" class="btn btn-success" value="Oturum Aç">
                        </div>
                    </div>
                </form>
                
                
                <?php if ($this->session->userdata('sub-sessions')): ?>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th colspan="3">Açık Oturumlar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->session->userdata('sub-sessions') as $id => $sub_session): ?>
                                <tr>
                                    <td><?= ($this->user->id == $id) ? '<span class="glyphicon glyphicon-check" aria-hidden="true"></span>' : '' ?></td>
                                    <td><?= $sub_session['name'] ?></td>
                                    <td class="text-right">
                                        <button type="button" class="btn btn-red unset_session" data-id="<?= $id ?>"><?= __('KAPAT') ?></button>
                                        <button type="button" class="btn btn-blue select_session" data-id="<?= $id ?>"><?= __('SEÇ') ?></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php endif; ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div id="pos-modal-note" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> <?= __('Not Ekle') ?></h4>
      </div>
      <div class="modal-body">
        <form name="form" class="form-horizontal">
          <textarea id="text_note" class="form-control js-auto-size valid keypad" rows="7" placeholder="<?= __('Not') ?>" style="font-size: 36px; line-height: 1;"></textarea>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><?= __('Kapat') ?></button>
        <button id="pos-modal-note-submit" type="button" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i> <?= __('Kaydet') ?></button>
      </div>
    </div>
  </div>
</div>



<div id="adisyon-table" class="hidden">

    <div class="container-fluid" style="width: 8cm !important; font-size: 16px !important; overflow: hidden !important;">
        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="<?= base_url('assets/logo1.svg') ?>" class="img-responsive center-block" alt="Image" style="width: 50%;">
            </div>
            <div class="col-xs-12">
                <table>
                    <thead>
                        <tr>
                            <th colspan="3"><?= $this->user->name ?></th>
                        </tr>
                        <tr>
                            <th class="adisyon_date"></th>
                            <th colspan="2"></th>
                            <th class="text-right"><?= $table ? $table->title : null ?></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th class="text-right"><?= __('Miktar') ?></th>
                            <th class="text-right"><?= __('Fiyat') ?></th>
                            <th class="text-right"><?= __('Tutar') ?></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="3"><?= __('Toplam') ?></th>
                            <th class="adisyon_subtotal text-right"></th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3"><?= __('Ara Ödeme') ?></th>
                            <th class="adisyon_interim_payment text-right"></th>
                        </tr>
                        <tr class="hidden">
                            <th class="text-right" colspan="3"><?= __('KDV') ?></th>
                            <th class="adisyon_tax text-right"></th>
                        </tr>
                        <tr>
                            <th class="text-right" colspan="3"><?= __('Genel Toplam') ?></th>
                            <th class="adisyon_total text-right"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-xs-12">
                <?= @$invoice->footer ?>
            </div>
        </div>
    </div>

</div>











<style type="text/css">
    #pos .btn-xxl {
        font-size: 16px;
    }
    #pos .btn-xl {
        font-size: 16px;
        padding: 12px 32px;
    }
    .e-number-spin input[type=number]::-webkit-outer-spin-button,
    .e-number-spin input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .e-number-spin input[type=number] {
        -moz-appearance:textfield;
    }
</style>