    
<div class="content">

    <div class="page-header full-content">
        <div class="row">
            <div class="col-sm-6">
                <h1><?= __("Kasa Seçimi"); ?><small></small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?= base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="#"><?= __("Uyarı"); ?></a></li>
                    <li><a href="#" class="active"><?= __("Kasa Ekranı"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h4><?= __("Uyarı"); ?></h4>
                        <p><small><?= __("Bu bilgisayar için Kasa seçimi yapılmamış. Lütfen seçim yapıp giriş yapınız."); ?></small></p>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#" class="form-horizontal">
                        <fieldset>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Restoran"); ?></label>
                                <div class="col-md-9">
                                    <select name="location_id" class="form-control">
                                        <option value=""><?= __("Restoran seç"); ?></option>
                                        <?php foreach ($this->user->locations_array as $location): ?>
                                            <option value="<?= $location['id']; ?>"><?= $location['title'] ;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Kasa"); ?></label>
                                <div class="col-md-9">
                                    <select name="pos_id" class="form-control">
                                        <option value="0"><?= __("Kasa seç"); ?></option>
                                    </select>
                                </div>
                            </div>

                            <!-- <input type="text" name="username" class="hidden"> -->
                            <!-- <input type="password" name="password" class="hidden"> -->

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Yetkili kullanıcı/eposta"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="text" name="username" class="form-control" autocomplete="false">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3"><?= __("Şifre"); ?></label>
                                <div class="col-md-9">
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <input type="password" name="password" class="form-control" autocomplete="false">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-buttons-fixed">
                                <div class="btn-group pull-right">
                                    <button type="submit" class="btn btn-success" name="dosubmit" data-url="<?= base_url('pointofsales/index');?>" data-return="<?= base_url('pointofsales');?>"><?= __("Giriş"); ?></button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>