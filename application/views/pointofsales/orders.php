<div class="content">

    <div class="page-header full-content">
        <div class="row p-t-2">
            <div class="col-sm-6">
                <h1><?= __("Siparişler"); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb">
                    <li><a href="<?php ECHO base_url(); ?>" ><i class="ion-home"></i></a></li>
                    <li><a href="<?= base_url('pointofsales') ?>" class="active"><?= __("POS"); ?></a></li>
                    <li><a href="#"><?= __("Siparişler"); ?></a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel">

                <div class="panel-heading">
                    <div class="panel-title ">
                        <form id="filter">
                            <div class="form-group">
                                <div class="row text-pink">
                                    <div class="col-md-4">
                                        <label><?= __("Restoran"); ?></label>
                                        <select name="location_id" class="form-control input-lg" data-tutorialize="location">
                                            <?php foreach ($this->user->locations_array as $location): ?>
                                                <option value="<?= $location['id'] ?>" <?= selected($location_id, $location['id']) ?>><?= $location['title'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label><?= __("Üretim Yeri"); ?></label>
                                        <select name="production_id" class="form-control input-lg" data-tutorialize="production">
                                            <?php foreach ($productions as $production): ?>
                                                <option value="<?= $production->id ?>" <?= selected($production_id, $production->id) ?>><?= $production->title ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label><?= __("Siparişler"); ?></label>
                                        <select name="status" class="form-control input-lg" data-tutorialize="status">
                                            <option value="pending"><?= __("Mevcut Siparişler"); ?></option>
                                            <option value="completed-today"><?= __("Bugün Tamamlanan Siparişler"); ?></option>
                                            <option value="completed"><?= __("Geçmiş Tamamlanan Siparişler"); ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><?= __("Masa"); ?></th>
                                <th><?= __("Adet"); ?></th>
                                <th><?= __("Ürün"); ?></th>
                                <th><?= __("Malzemeler"); ?></th>
                                <th><?= __("İlave Ürünler"); ?></th>
                                <th><?= __("Personel"); ?></th>
                                <th><?= __("Oluşturuldu"); ?></th>
                                <th><?= __("İşlem"); ?></th>
                            </tr>
                        </thead>
                        <tbody id="orders">
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>



<div class="modal full-height" id="modal-notes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __("Ürün Notları"); ?></h4>
            </div>
            <div class="modal-body h--150">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xxl" data-dismiss="modal"><?= __("Kapat"); ?></button>
            </div>
        </div>
    </div>
</div>


<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="order_invoice">
        <?php $this->load->view('pointofsales/elements/order_invoice'); ?>
    </div>
</div>