<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Yemeksepetihelpers
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function filteredYsProducts($ys_products)
    {
        $products = [];
        $options = [];

        if (isset($ys_products->{'@attributes'})) {

        	$products[0] = $ys_products->{'@attributes'};
        	$products[0]->options = [];

	        if ($ys_products->{'@attributes'}->Options) {

	        	if (isset($ys_products->option->{'@attributes'})) {

	        		$products[0]->options[] = $ys_products->option->{'@attributes'};
	        		$options[0] = $ys_products->option->{'@attributes'};

	        	} else {

	        		foreach ($ys_products->option as $key => $option) {

	        			$products[0]->options[] = $option->{'@attributes'};
	        			$options[$key] = $option->{'@attributes'};
	        			
	        		}

	        	}

	        }


        } else {

        	foreach ($ys_products as $key => $ys_product) {

        		$product = $ys_product->{'@attributes'};

        		$products[$key] = (object) [
	    			'id' => $product->id,
		            'Name' => $product->Name,
		            'Price' => $product->Price,
		            'Quantity' => $product->Quantity,
		            'ProductOptionId' => $product->ProductOptionId
        		];

        		$products[$key]->options = [];

        		if ($product->Options) {

	        		if (isset($ys_product->option->{'@attributes'})) {

	        			$products[$key]->options[] = $ys_product->option->{'@attributes'};
	        			$options[0] = $ys_product->option->{'@attributes'};

	        		} else if (isset($ys_product->{'0'})){
	        			$products[$key]->options[] = $ys_product->{'0'}->{'@attributes'};
	        			$options[0] = $ys_product->{'0'}->{'@attributes'};

	        		} else {

	        			foreach ($ys_product->option as $option) {

							$products[$key]->options[] = $option->{'@attributes'};
							$options[$key] = $option->{'@attributes'};
	        				
	        			}

	        		}

        		}
        		
        	}

        }

        return $products;
 
    }

	

}

/* End of file Yemeksepetihelpers.php */
/* Location: ./application/libraries/Yemeksepetihelpers.php */
