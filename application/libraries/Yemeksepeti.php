<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once dirname(__FILE__) . '/yemeksepeti/YemekSepetiAutoload.php';

class Yemeksepeti
{
	protected $ci;

	private $Username; 
    private $Password;

    private $service;

    private $orderStateList = ["Approved","Accepted","Rejected","Cancelled","OnDelivery","Delivered"];
    private $RestaurantStateList = ["Open" , "Closed" , "HugeDemand"];


	public function __construct($params = [])
	{
        $this->ci =& get_instance();

	}

	public function init($username, $password)
	{

        $this->Username = $username;
        $this->Password = $password;


        $this->service  = $yemekSepetiServiceGet = new YemekSepetiServiceGet();

		$this->service->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader($this->Username , $this->Password));

	}

	public function initByLocationId($location_id)
	{
		$yemeksepeti = $this->isAvaliable($location_id);
		if ($yemeksepeti) {
			$this->init($yemeksepeti->username, $yemeksepeti->password);
			return true;
		}
	}

	public function isAvaliable($location_id)
	{
		if (isset($this->ci->user->locations_array[$location_id])) {

			$yemeksepeti = $this->ci->user->locations_array[$location_id]['yemeksepeti'];
			$yemeksepeti = json_decode($yemeksepeti);
			if (!empty($yemeksepeti->username) && !empty($yemeksepeti->password)) {
				return $yemeksepeti;
			}

		}
		
	}

	public function GetMenu()
	{
		if($this->service->GetMenu()):
			$arr =  $this->parseXml("GetMenuResult");
			return $this->get_result_message(true , $arr->Menu);
		else:
			return $this->get_error_message();
		endif;
	}

	public function GetList()
	{
		if($this->service->GetRestaurantList())
		{	
			$arr =  $this->parseXml("GetRestaurantListResult");
			return $this->get_result_message(true , $arr->RestaurantList);
		} else {
			return $this->get_error_message();
		}
	}

	public function GetPaymentTypes()
	{
		if($this->service->GetPaymentTypes()):
			$arr =  $this->parseXml("GetPaymentTypesResult");
			return $this->get_result_message(true , $arr->NewDataSet->PaymentMethods);
		else:
			return $this->get_error_message();
		endif;

	}

	public function MessageSuccesful($messageId_ = 0)
	{	
		if($messageId_ == 0) return $this->get_result_message(false , [] , "Hatalı Mesaj ID");

		$this->service = new YemekSepetiServiceMessage();
		$this->service->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader($this->Username , $this->Password));
		
		if($this->service->MessageSuccessful(new YemekSepetiStructMessageSuccessful($messageId_))){
			return $this->get_result_message(true);
		} else {
			return $this->get_error_message();
		}


	}


	public function GetAllMessages()
	{	
		if($this->service->GetAllMessages()):

			$arr = $this->parseXml("GetAllMessagesResult");

			if(isset($arr->order)){
				return $this->get_result_message(true , is_array($arr->order)  ? $arr->order : ["0" => $arr->order]);

			} else {
				return $this->get_result_message(true);
			}
		else:
			return $this->get_error_message();
		endif;

	}

	public function GetAllMessagesV2()
	{	
		if($this->service->GetAllMessagesV2()):

			$arr = $this->parseXml("GetAllMessagesV2Result");

			if(isset($arr->order)){
				return $this->get_result_message(true , is_array($arr->order)  ? $arr->order : ["0" => $arr->order]);

			} else {
				return $this->get_result_message(true);
			}
		else:
			return $this->get_error_message();
		endif;

	}


	public function UpdateOrder($orderID = null , $state = null, $reason = null){

		$this->service =new YemekSepetiServiceUpdate();
		$this->service->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader($this->Username , $this->Password));


		if($orderID == null || $state == null) 
			return $this->get_result_message(false,[],"Order ID or State not empty");

		if(!in_array($state, $this->orderStateList))
			return $this->get_result_message(false,[],"Unknow State \" $state \"");


		if($this->service->UpdateOrder(new YemekSepetiStructUpdateOrder($orderID , $state, $reason))){
			
			$arr =  $this->parseXml("UpdateOrderResult");
			return $this->get_result_message($arr);
		} else {
			return $this->get_error_message();
		}
	}


	public function UpdateState($state = true , $catalog = null , $category = null)
	{
		
		if($state == null || $catalog == null || $category == null) 
			return $this->get_result_message(false,[],"State , Catalog and Category not empty");

		if(!in_array($state, $this->RestaurantStateList))
			return $this->get_result_message(false,[],"Unknow State \" $state \"");

		$this->service = new YemekSepetiServiceUpdate();
		$this->service->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader($this->Username , $this->Password));

		$params = new YemekSepetiStructUpdateRestaurantState($state , $catalog , $category);


		if($this->service->UpdateRestaurantState($params)){
			$arr =  $this->parseXml("UpdateRestaurantStateResult");
			return $this->get_result_message($arr);
		} else {
			return $this->get_error_message();
		}

	}

	public function ServiceTime($time = null , $catalog = null , $category = null){

		if($time == null || $catalog == null || $category == null) 
			return $this->get_result_message(false,[],"Time , Catalog and Category not empty");



		$this->service = new YemekSepetiServiceSet();
		$this->service->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader($this->Username , $this->Password));


		$params = new YemekSepetiStructSetRestaurantServiceTime($time , $catalog , $category);

		if($this->service->SetRestaurantServiceTime($params)){
			$arr =  $this->parseXml("SetRestaurantServiceTimeResult");
			return $this->get_result_message($arr);
		} else {
			return $this->get_error_message();
		}
	}


	private function parseXml($key = null)
	{
		if($key == null){
			 return $this->get_error_message("Key not empty");
		}

		if(isset($this->service->getResult()->{$key})){
			$result = $this->service->getResult()->{$key};

				if(isset($result->any)){
					
					return ($this->checkXml($result->any) ? json_decode(json_encode( new SimpleXMLElement($result->any))):$result->any);
				} else {
					return ($this->checkXml($result) ? json_decode(json_encode( new SimpleXMLElement($result))): $result);
				}
			
		} else {
			return $this->get_error_message("Wrong Key");
		}
		
	}


	private function checkXml($val)
	{

		if(substr($val, 0, 1) == "<"){ 
			return true;
		} else {
			return false;
		}
	}


	private function get_error_message($_message = null)
	{
		if($_message != null) {
			return $_message;
		}
		$error = $this->service->getLastError();

		$errorVal = array_values($error)[0];
		$message  = $errorVal->getMessage();


		return $message;
	}

	private function get_result_message($status = true , $items = [] , $message = null)
	{

		$result = [
			'status' => $status,
			'message' => $message,
			'items'   => $items
		];

		return $result;
	}


}

/* End of file Yemeksepeti.php */
/* Location: ./application/libraries/Yemeksepeti.php */
