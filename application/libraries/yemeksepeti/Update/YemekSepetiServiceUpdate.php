<?php
/**
 * File for class YemekSepetiServiceUpdate
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiServiceUpdate originally named Update
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiServiceUpdate extends YemekSepetiWsdlClass
{
    /**
     * Sets the AuthHeader SoapHeader param
     * @uses YemekSepetiWsdlClass::setSoapHeader()
     * @param YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader
     * @param string $_nameSpace http://tempuri.org/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthHeader(YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader,$_nameSpace = 'http://tempuri.org/',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'AuthHeader',$_yemekSepetiStructAuthHeader,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named UpdateRestaurantState
     * Documentation : Updates restaurant state.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructUpdateRestaurantState $_yemekSepetiStructUpdateRestaurantState
     * @return YemekSepetiStructUpdateRestaurantStateResponse
     */
    public function UpdateRestaurantState(YemekSepetiStructUpdateRestaurantState $_yemekSepetiStructUpdateRestaurantState)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->UpdateRestaurantState($_yemekSepetiStructUpdateRestaurantState));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named UpdateOrder
     * Documentation : Updates order state.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructUpdateOrder $_yemekSepetiStructUpdateOrder
     * @return YemekSepetiStructUpdateOrderResponse
     */
    public function UpdateOrder(YemekSepetiStructUpdateOrder $_yemekSepetiStructUpdateOrder)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->UpdateOrder($_yemekSepetiStructUpdateOrder));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see YemekSepetiWsdlClass::getResult()
     * @return YemekSepetiStructUpdateOrderResponse|YemekSepetiStructUpdateRestaurantStateResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
