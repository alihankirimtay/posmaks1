<?php
/**
 * File for class YemekSepetiStructUpdateRestaurantState
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructUpdateRestaurantState originally named UpdateRestaurantState
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructUpdateRestaurantState extends YemekSepetiWsdlClass
{
    /**
     * The restaurantState
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var YemekSepetiEnumRestaurantStates
     */
    public $restaurantState;
    /**
     * The catalogName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $catalogName;
    /**
     * The categoryName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $categoryName;
    /**
     * Constructor method for UpdateRestaurantState
     * @see parent::__construct()
     * @param YemekSepetiEnumRestaurantStates $_restaurantState
     * @param string $_catalogName
     * @param string $_categoryName
     * @return YemekSepetiStructUpdateRestaurantState
     */
    public function __construct($_restaurantState,$_catalogName = NULL,$_categoryName = NULL)
    {
        parent::__construct(array('restaurantState'=>$_restaurantState,'catalogName'=>$_catalogName,'categoryName'=>$_categoryName),false);
    }
    /**
     * Get restaurantState value
     * @return YemekSepetiEnumRestaurantStates
     */
    public function getRestaurantState()
    {
        return $this->restaurantState;
    }
    /**
     * Set restaurantState value
     * @uses YemekSepetiEnumRestaurantStates::valueIsValid()
     * @param YemekSepetiEnumRestaurantStates $_restaurantState the restaurantState
     * @return YemekSepetiEnumRestaurantStates
     */
    public function setRestaurantState($_restaurantState)
    {
        if(!YemekSepetiEnumRestaurantStates::valueIsValid($_restaurantState))
        {
            return false;
        }
        return ($this->restaurantState = $_restaurantState);
    }
    /**
     * Get catalogName value
     * @return string|null
     */
    public function getCatalogName()
    {
        return $this->catalogName;
    }
    /**
     * Set catalogName value
     * @param string $_catalogName the catalogName
     * @return string
     */
    public function setCatalogName($_catalogName)
    {
        return ($this->catalogName = $_catalogName);
    }
    /**
     * Get categoryName value
     * @return string|null
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }
    /**
     * Set categoryName value
     * @param string $_categoryName the categoryName
     * @return string
     */
    public function setCategoryName($_categoryName)
    {
        return ($this->categoryName = $_categoryName);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructUpdateRestaurantState
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
