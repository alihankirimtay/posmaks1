<?php
/**
 * File for class YemekSepetiServiceGet
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiServiceGet originally named Get
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiServiceGet extends YemekSepetiWsdlClass
{
    /**
     * Sets the AuthHeader SoapHeader param
     * @uses YemekSepetiWsdlClass::setSoapHeader()
     * @param YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader
     * @param string $_nameSpace http://tempuri.org/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthHeader(YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader,$_nameSpace = 'http://tempuri.org/',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'AuthHeader',$_yemekSepetiStructAuthHeader,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named GetPaymentTypes
     * Documentation : Gets the all avaliable payment types.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetPaymentTypes $_yemekSepetiStructGetPaymentTypes
     * @return YemekSepetiStructGetPaymentTypesResponse
     */
    public function GetPaymentTypes()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetPaymentTypes());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetRestaurantList
     * Documentation : Gets all related restaurants list.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetRestaurantList $_yemekSepetiStructGetRestaurantList
     * @return YemekSepetiStructGetRestaurantListResponse
     */
    public function GetRestaurantList()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetRestaurantList());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetRestaurantPointsAndComments
     * Documentation : Gets the restaurant points and comments.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetRestaurantPointsAndComments $_yemekSepetiStructGetRestaurantPointsAndComments
     * @return YemekSepetiStructGetRestaurantPointsAndCommentsResponse
     */
    public function GetRestaurantPointsAndComments(YemekSepetiStructGetRestaurantPointsAndComments $_yemekSepetiStructGetRestaurantPointsAndComments)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetRestaurantPointsAndComments($_yemekSepetiStructGetRestaurantPointsAndComments));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetMessage
     * Documentation : Gets top one unreaded messages in XML format.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetMessage $_yemekSepetiStructGetMessage
     * @return YemekSepetiStructGetMessageResponse
     */
    public function GetMessage()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetMessage());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetAllMessages
     * Documentation : Gets all unreaded messages in XML format.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetAllMessages $_yemekSepetiStructGetAllMessages
     * @return YemekSepetiStructGetAllMessagesResponse
     */
    public function GetAllMessages()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetAllMessages());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }

     public function GetAllMessagesV2()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetAllMessagesV2());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetTopMessages
     * Documentation : Gets top unreaded messages in XML format.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetTopMessages $_yemekSepetiStructGetTopMessages
     * @return YemekSepetiStructGetTopMessagesResponse
     */
    public function GetTopMessages(YemekSepetiStructGetTopMessages $_yemekSepetiStructGetTopMessages)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetTopMessages($_yemekSepetiStructGetTopMessages));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetMenu
     * Documentation : Gets the restaurant menu. The result objects includes products and options.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetMenu $_yemekSepetiStructGetMenu
     * @return YemekSepetiStructGetMenuResponse
     */
    public function GetMenu()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetMenu());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named GetRestaurantPromotions
     * Documentation : Gets the restaurant promotions.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructGetRestaurantPromotions $_yemekSepetiStructGetRestaurantPromotions
     * @return YemekSepetiStructGetRestaurantPromotionsResponse
     */
    public function GetRestaurantPromotions()
    {
        try
        {
            return $this->setResult(self::getSoapClient()->GetRestaurantPromotions());
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see YemekSepetiWsdlClass::getResult()
     * @return YemekSepetiStructGetAllMessagesResponse|YemekSepetiStructGetMenuResponse|YemekSepetiStructGetMessageResponse|YemekSepetiStructGetPaymentTypesResponse|YemekSepetiStructGetRestaurantListResponse|YemekSepetiStructGetRestaurantPointsAndCommentsResponse|YemekSepetiStructGetRestaurantPromotionsResponse|YemekSepetiStructGetTopMessagesResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
