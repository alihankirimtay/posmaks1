<?php
/**
 * File for class YemekSepetiStructGetAllMessages
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetAllMessages originally named GetAllMessages
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetAllMessages extends YemekSepetiWsdlClass
{
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
