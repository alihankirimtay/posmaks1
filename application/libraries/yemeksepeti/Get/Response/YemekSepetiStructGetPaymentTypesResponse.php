<?php
/**
 * File for class YemekSepetiStructGetPaymentTypesResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetPaymentTypesResponse originally named GetPaymentTypesResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetPaymentTypesResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetPaymentTypesResult
     * @var YemekSepetiStructGetPaymentTypesResult
     */
    public $GetPaymentTypesResult;
    /**
     * Constructor method for GetPaymentTypesResponse
     * @see parent::__construct()
     * @param YemekSepetiStructGetPaymentTypesResult $_getPaymentTypesResult
     * @return YemekSepetiStructGetPaymentTypesResponse
     */
    public function __construct($_getPaymentTypesResult = NULL)
    {
        parent::__construct(array('GetPaymentTypesResult'=>$_getPaymentTypesResult),false);
    }
    /**
     * Get GetPaymentTypesResult value
     * @return YemekSepetiStructGetPaymentTypesResult|null
     */
    public function getGetPaymentTypesResult()
    {
        return $this->GetPaymentTypesResult;
    }
    /**
     * Set GetPaymentTypesResult value
     * @param YemekSepetiStructGetPaymentTypesResult $_getPaymentTypesResult the GetPaymentTypesResult
     * @return YemekSepetiStructGetPaymentTypesResult
     */
    public function setGetPaymentTypesResult($_getPaymentTypesResult)
    {
        return ($this->GetPaymentTypesResult = $_getPaymentTypesResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetPaymentTypesResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
