<?php
/**
 * File for class YemekSepetiStructGetRestaurantPromotionsResponse
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructGetRestaurantPromotionsResponse originally named GetRestaurantPromotionsResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructGetRestaurantPromotionsResponse extends YemekSepetiWsdlClass
{
    /**
     * The GetRestaurantPromotionsResult
     * @var YemekSepetiStructGetRestaurantPromotionsResult
     */
    public $GetRestaurantPromotionsResult;
    /**
     * Constructor method for GetRestaurantPromotionsResponse
     * @see parent::__construct()
     * @param YemekSepetiStructGetRestaurantPromotionsResult $_getRestaurantPromotionsResult
     * @return YemekSepetiStructGetRestaurantPromotionsResponse
     */
    public function __construct($_getRestaurantPromotionsResult = NULL)
    {
        parent::__construct(array('GetRestaurantPromotionsResult'=>$_getRestaurantPromotionsResult),false);
    }
    /**
     * Get GetRestaurantPromotionsResult value
     * @return YemekSepetiStructGetRestaurantPromotionsResult|null
     */
    public function getGetRestaurantPromotionsResult()
    {
        return $this->GetRestaurantPromotionsResult;
    }
    /**
     * Set GetRestaurantPromotionsResult value
     * @param YemekSepetiStructGetRestaurantPromotionsResult $_getRestaurantPromotionsResult the GetRestaurantPromotionsResult
     * @return YemekSepetiStructGetRestaurantPromotionsResult
     */
    public function setGetRestaurantPromotionsResult($_getRestaurantPromotionsResult)
    {
        return ($this->GetRestaurantPromotionsResult = $_getRestaurantPromotionsResult);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructGetRestaurantPromotionsResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
