<?php
/**
 * File for class YemekSepetiServiceIs
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiServiceIs originally named Is
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiServiceIs extends YemekSepetiWsdlClass
{
    /**
     * Sets the AuthHeader SoapHeader param
     * @uses YemekSepetiWsdlClass::setSoapHeader()
     * @param YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader
     * @param string $_nameSpace http://tempuri.org/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthHeader(YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader,$_nameSpace = 'http://tempuri.org/',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'AuthHeader',$_yemekSepetiStructAuthHeader,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named IsRestaurantOpen
     * Documentation : Returns whether a restaurant open or not.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructIsRestaurantOpen $_yemekSepetiStructIsRestaurantOpen
     * @return YemekSepetiStructIsRestaurantOpenResponse
     */
    public function IsRestaurantOpen(YemekSepetiStructIsRestaurantOpen $_yemekSepetiStructIsRestaurantOpen)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->IsRestaurantOpen($_yemekSepetiStructIsRestaurantOpen));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see YemekSepetiWsdlClass::getResult()
     * @return YemekSepetiStructIsRestaurantOpenResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
