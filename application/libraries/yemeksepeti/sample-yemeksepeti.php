<?php
/**
 * Test with YemekSepeti for 'http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL'
 * @package YemekSepeti
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
ini_set('memory_limit','512M');
ini_set('display_errors',true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/YemekSepetiAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a YemekSepetiWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[YemekSepetiWsdlClass::WSDL_URL] = 'http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL';
 * $wsdl[YemekSepetiWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[YemekSepetiWsdlClass::WSDL_TRACE] = true;
 * $wsdl[YemekSepetiWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[YemekSepetiWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new YemekSepetiWsdlClass($wsdl);
 */
/**
 * Examples
 */


/***********************************
 * Example for YemekSepetiServiceGet
 */
$yemekSepetiServiceGet = new YemekSepetiServiceGet();
// sample call for YemekSepetiServiceGet::setSoapHeaderAuthHeader() in order to initialize required SoapHeader
$yemekSepetiServiceGet->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader(/*** update parameters list ***/));
// sample call for YemekSepetiServiceGet::GetPaymentTypes()
if($yemekSepetiServiceGet->GetPaymentTypes())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetRestaurantList()
if($yemekSepetiServiceGet->GetRestaurantList())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetRestaurantPointsAndComments()
if($yemekSepetiServiceGet->GetRestaurantPointsAndComments(new YemekSepetiStructGetRestaurantPointsAndComments(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetMessage()
if($yemekSepetiServiceGet->GetMessage())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetAllMessages()
if($yemekSepetiServiceGet->GetAllMessages())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetTopMessages()
if($yemekSepetiServiceGet->GetTopMessages(new YemekSepetiStructGetTopMessages(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetMenu()
if($yemekSepetiServiceGet->GetMenu())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());
// sample call for YemekSepetiServiceGet::GetRestaurantPromotions()
if($yemekSepetiServiceGet->GetRestaurantPromotions())
    print_r($yemekSepetiServiceGet->getResult());
else
    print_r($yemekSepetiServiceGet->getLastError());

/**********************************
 * Example for YemekSepetiServiceIs
 */
$yemekSepetiServiceIs = new YemekSepetiServiceIs();
// sample call for YemekSepetiServiceIs::setSoapHeaderAuthHeader() in order to initialize required SoapHeader
$yemekSepetiServiceIs->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader(/*** update parameters list ***/));
// sample call for YemekSepetiServiceIs::IsRestaurantOpen()
if($yemekSepetiServiceIs->IsRestaurantOpen(new YemekSepetiStructIsRestaurantOpen(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceIs->getResult());
else
    print_r($yemekSepetiServiceIs->getLastError());

/**************************************
 * Example for YemekSepetiServiceUpdate
 */
$yemekSepetiServiceUpdate = new YemekSepetiServiceUpdate();
// sample call for YemekSepetiServiceUpdate::setSoapHeaderAuthHeader() in order to initialize required SoapHeader
$yemekSepetiServiceUpdate->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader(/*** update parameters list ***/));
// sample call for YemekSepetiServiceUpdate::UpdateRestaurantState()
if($yemekSepetiServiceUpdate->UpdateRestaurantState(new YemekSepetiStructUpdateRestaurantState(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceUpdate->getResult());
else
    print_r($yemekSepetiServiceUpdate->getLastError());
// sample call for YemekSepetiServiceUpdate::UpdateOrder()
if($yemekSepetiServiceUpdate->UpdateOrder(new YemekSepetiStructUpdateOrder(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceUpdate->getResult());
else
    print_r($yemekSepetiServiceUpdate->getLastError());

/***************************************
 * Example for YemekSepetiServiceMessage
 */
$yemekSepetiServiceMessage = new YemekSepetiServiceMessage();
// sample call for YemekSepetiServiceMessage::setSoapHeaderAuthHeader() in order to initialize required SoapHeader
$yemekSepetiServiceMessage->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader(/*** update parameters list ***/));
// sample call for YemekSepetiServiceMessage::MessageSuccessful()
if($yemekSepetiServiceMessage->MessageSuccessful(new YemekSepetiStructMessageSuccessful(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceMessage->getResult());
else
    print_r($yemekSepetiServiceMessage->getLastError());

/***********************************
 * Example for YemekSepetiServiceSet
 */
$yemekSepetiServiceSet = new YemekSepetiServiceSet();
// sample call for YemekSepetiServiceSet::setSoapHeaderAuthHeader() in order to initialize required SoapHeader
$yemekSepetiServiceSet->setSoapHeaderAuthHeader(new YemekSepetiStructAuthHeader(/*** update parameters list ***/));
// sample call for YemekSepetiServiceSet::SetRestaurantServiceTime()
if($yemekSepetiServiceSet->SetRestaurantServiceTime(new YemekSepetiStructSetRestaurantServiceTime(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceSet->getResult());
else
    print_r($yemekSepetiServiceSet->getLastError());
// sample call for YemekSepetiServiceSet::SetRestaurantForOrder()
if($yemekSepetiServiceSet->SetRestaurantForOrder(new YemekSepetiStructSetRestaurantForOrder(/*** update parameters list ***/)))
    print_r($yemekSepetiServiceSet->getResult());
else
    print_r($yemekSepetiServiceSet->getLastError());
