<?php
/**
 * File for class YemekSepetiServiceMessage
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiServiceMessage originally named Message
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiServiceMessage extends YemekSepetiWsdlClass
{
    /**
     * Sets the AuthHeader SoapHeader param
     * @uses YemekSepetiWsdlClass::setSoapHeader()
     * @param YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader
     * @param string $_nameSpace http://tempuri.org/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthHeader(YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader,$_nameSpace = 'http://tempuri.org/',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'AuthHeader',$_yemekSepetiStructAuthHeader,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named MessageSuccessful
     * Documentation : Set message received successfully.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructMessageSuccessful $_yemekSepetiStructMessageSuccessful
     * @return YemekSepetiStructMessageSuccessfulResponse
     */
    public function MessageSuccessful(YemekSepetiStructMessageSuccessful $_yemekSepetiStructMessageSuccessful)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->MessageSuccessful($_yemekSepetiStructMessageSuccessful));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see YemekSepetiWsdlClass::getResult()
     * @return YemekSepetiStructMessageSuccessfulResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
