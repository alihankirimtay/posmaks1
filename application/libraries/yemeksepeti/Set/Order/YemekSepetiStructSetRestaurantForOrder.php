<?php
/**
 * File for class YemekSepetiStructSetRestaurantForOrder
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiStructSetRestaurantForOrder originally named SetRestaurantForOrder
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiStructSetRestaurantForOrder extends YemekSepetiWsdlClass
{
    /**
     * The orderId
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 1
     * @var decimal
     */
    public $orderId;
    /**
     * The categoryName
     * Meta informations extracted from the WSDL
     * - maxOccurs : 1
     * - minOccurs : 0
     * @var string
     */
    public $categoryName;
    /**
     * Constructor method for SetRestaurantForOrder
     * @see parent::__construct()
     * @param decimal $_orderId
     * @param string $_categoryName
     * @return YemekSepetiStructSetRestaurantForOrder
     */
    public function __construct($_orderId,$_categoryName = NULL)
    {
        parent::__construct(array('orderId'=>$_orderId,'categoryName'=>$_categoryName),false);
    }
    /**
     * Get orderId value
     * @return decimal
     */
    public function getOrderId()
    {
        return $this->orderId;
    }
    /**
     * Set orderId value
     * @param decimal $_orderId the orderId
     * @return decimal
     */
    public function setOrderId($_orderId)
    {
        return ($this->orderId = $_orderId);
    }
    /**
     * Get categoryName value
     * @return string|null
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }
    /**
     * Set categoryName value
     * @param string $_categoryName the categoryName
     * @return string
     */
    public function setCategoryName($_categoryName)
    {
        return ($this->categoryName = $_categoryName);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see YemekSepetiWsdlClass::__set_state()
     * @uses YemekSepetiWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return YemekSepetiStructSetRestaurantForOrder
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
