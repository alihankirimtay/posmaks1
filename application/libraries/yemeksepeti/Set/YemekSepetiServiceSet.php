<?php
/**
 * File for class YemekSepetiServiceSet
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiServiceSet originally named Set
 * @package YemekSepeti
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiServiceSet extends YemekSepetiWsdlClass
{
    /**
     * Sets the AuthHeader SoapHeader param
     * @uses YemekSepetiWsdlClass::setSoapHeader()
     * @param YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader
     * @param string $_nameSpace http://tempuri.org/
     * @param bool $_mustUnderstand
     * @param string $_actor
     * @return bool true|false
     */
    public function setSoapHeaderAuthHeader(YemekSepetiStructAuthHeader $_yemekSepetiStructAuthHeader,$_nameSpace = 'http://tempuri.org/',$_mustUnderstand = false,$_actor = null)
    {
        return $this->setSoapHeader($_nameSpace,'AuthHeader',$_yemekSepetiStructAuthHeader,$_mustUnderstand,$_actor);
    }
    /**
     * Method to call the operation originally named SetRestaurantServiceTime
     * Documentation : Sets restaurant service time.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructSetRestaurantServiceTime $_yemekSepetiStructSetRestaurantServiceTime
     * @return YemekSepetiStructSetRestaurantServiceTimeResponse
     */
    public function SetRestaurantServiceTime(YemekSepetiStructSetRestaurantServiceTime $_yemekSepetiStructSetRestaurantServiceTime)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->SetRestaurantServiceTime($_yemekSepetiStructSetRestaurantServiceTime));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named SetRestaurantForOrder
     * Documentation : Redirects order another restaurant of the account.
     * Meta informations extracted from the WSDL
     * - SOAPHeaderNames : AuthHeader
     * - SOAPHeaderNamespaces : http://tempuri.org/
     * - SOAPHeaderTypes : {@link YemekSepetiStructAuthHeader}
     * - SOAPHeaders : required
     * @uses YemekSepetiWsdlClass::getSoapClient()
     * @uses YemekSepetiWsdlClass::setResult()
     * @uses YemekSepetiWsdlClass::saveLastError()
     * @param YemekSepetiStructSetRestaurantForOrder $_yemekSepetiStructSetRestaurantForOrder
     * @return YemekSepetiStructSetRestaurantForOrderResponse
     */
    public function SetRestaurantForOrder(YemekSepetiStructSetRestaurantForOrder $_yemekSepetiStructSetRestaurantForOrder)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->SetRestaurantForOrder($_yemekSepetiStructSetRestaurantForOrder));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see YemekSepetiWsdlClass::getResult()
     * @return YemekSepetiStructSetRestaurantForOrderResponse|YemekSepetiStructSetRestaurantServiceTimeResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
