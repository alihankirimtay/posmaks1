<?php
/**
 * File for class YemekSepetiEnumOrderStates
 * @package YemekSepeti
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * This class stands for YemekSepetiEnumOrderStates originally named OrderStates
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://messaging.yemeksepeti.com/MessagingWebService/Integration.asmx?WSDL}
 * @package YemekSepeti
 * @subpackage Enumerations
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiEnumOrderStates extends YemekSepetiWsdlClass
{
    /**
     * Constant for value 'Accepted'
     * @return string 'Accepted'
     */
    const VALUE_ACCEPTED = 'Accepted';
    /**
     * Constant for value 'Rejected'
     * @return string 'Rejected'
     */
    const VALUE_REJECTED = 'Rejected';
    /**
     * Constant for value 'Cancelled'
     * @return string 'Cancelled'
     */
    const VALUE_CANCELLED = 'Cancelled';
    /**
     * Constant for value 'OnDelivery'
     * @return string 'OnDelivery'
     */
    const VALUE_ONDELIVERY = 'OnDelivery';
    /**
     * Constant for value 'Delivered'
     * @return string 'Delivered'
     */
    const VALUE_DELIVERED = 'Delivered';
    /**
     * Constant for value 'TechnicalRejected'
     * @return string 'TechnicalRejected'
     */
    const VALUE_TECHNICALREJECTED = 'TechnicalRejected';
    /**
     * Return true if value is allowed
     * @uses YemekSepetiEnumOrderStates::VALUE_ACCEPTED
     * @uses YemekSepetiEnumOrderStates::VALUE_REJECTED
     * @uses YemekSepetiEnumOrderStates::VALUE_CANCELLED
     * @uses YemekSepetiEnumOrderStates::VALUE_ONDELIVERY
     * @uses YemekSepetiEnumOrderStates::VALUE_DELIVERED
     * @uses YemekSepetiEnumOrderStates::VALUE_TECHNICALREJECTED
     * @param mixed $_value value
     * @return bool true|false
     */
    public static function valueIsValid($_value)
    {
        return in_array($_value,array(YemekSepetiEnumOrderStates::VALUE_ACCEPTED,YemekSepetiEnumOrderStates::VALUE_REJECTED,YemekSepetiEnumOrderStates::VALUE_CANCELLED,YemekSepetiEnumOrderStates::VALUE_ONDELIVERY,YemekSepetiEnumOrderStates::VALUE_DELIVERED,YemekSepetiEnumOrderStates::VALUE_TECHNICALREJECTED));
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
