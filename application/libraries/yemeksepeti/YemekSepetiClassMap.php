<?php
/**
 * File for the class which returns the class map definition
 * @package YemekSepeti
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
/**
 * Class which returns the class map definition by the static method YemekSepetiClassMap::classMap()
 * @package YemekSepeti
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20140325-01
 * @date 2015-02-01
 */
class YemekSepetiClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'AuthHeader' => 'YemekSepetiStructAuthHeader',
  'GetAllMessages' => 'YemekSepetiStructGetAllMessages',
  'GetAllMessagesV2' => 'YemekSepetiStructGetAllMessagesV2',
  'GetAllMessagesResponse' => 'YemekSepetiStructGetAllMessagesResponse',
  'GetAllMessagesResponseV2' => 'YemekSepetiStructGetAllMessagesResponseV2',
  'GetMenu' => 'YemekSepetiStructGetMenu',
  'GetMenuResponse' => 'YemekSepetiStructGetMenuResponse',
  'GetMenuResult' => 'YemekSepetiStructGetMenuResult',
  'GetMessage' => 'YemekSepetiStructGetMessage',
  'GetMessageResponse' => 'YemekSepetiStructGetMessageResponse',
  'GetPaymentTypes' => 'YemekSepetiStructGetPaymentTypes',
  'GetPaymentTypesResponse' => 'YemekSepetiStructGetPaymentTypesResponse',
  'GetPaymentTypesResult' => 'YemekSepetiStructGetPaymentTypesResult',
  'GetRestaurantList' => 'YemekSepetiStructGetRestaurantList',
  'GetRestaurantListResponse' => 'YemekSepetiStructGetRestaurantListResponse',
  'GetRestaurantListResult' => 'YemekSepetiStructGetRestaurantListResult',
  'GetRestaurantPointsAndComments' => 'YemekSepetiStructGetRestaurantPointsAndComments',
  'GetRestaurantPointsAndCommentsResponse' => 'YemekSepetiStructGetRestaurantPointsAndCommentsResponse',
  'GetRestaurantPointsAndCommentsResult' => 'YemekSepetiStructGetRestaurantPointsAndCommentsResult',
  'GetRestaurantPromotions' => 'YemekSepetiStructGetRestaurantPromotions',
  'GetRestaurantPromotionsResponse' => 'YemekSepetiStructGetRestaurantPromotionsResponse',
  'GetRestaurantPromotionsResult' => 'YemekSepetiStructGetRestaurantPromotionsResult',
  'GetTopMessages' => 'YemekSepetiStructGetTopMessages',
  'GetTopMessagesResponse' => 'YemekSepetiStructGetTopMessagesResponse',
  'IsRestaurantOpen' => 'YemekSepetiStructIsRestaurantOpen',
  'IsRestaurantOpenResponse' => 'YemekSepetiStructIsRestaurantOpenResponse',
  'MessageSuccessful' => 'YemekSepetiStructMessageSuccessful',
  'MessageSuccessfulResponse' => 'YemekSepetiStructMessageSuccessfulResponse',
  'OrderStates' => 'YemekSepetiEnumOrderStates',
  'RestaurantStates' => 'YemekSepetiEnumRestaurantStates',
  'SetRestaurantForOrder' => 'YemekSepetiStructSetRestaurantForOrder',
  'SetRestaurantForOrderResponse' => 'YemekSepetiStructSetRestaurantForOrderResponse',
  'SetRestaurantServiceTime' => 'YemekSepetiStructSetRestaurantServiceTime',
  'SetRestaurantServiceTimeResponse' => 'YemekSepetiStructSetRestaurantServiceTimeResponse',
  'UpdateOrder' => 'YemekSepetiStructUpdateOrder',
  'UpdateOrderResponse' => 'YemekSepetiStructUpdateOrderResponse',
  'UpdateRestaurantState' => 'YemekSepetiStructUpdateRestaurantState',
  'UpdateRestaurantStateResponse' => 'YemekSepetiStructUpdateRestaurantStateResponse',
);
    }
}
