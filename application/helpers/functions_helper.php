<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function pr($array, $exit = TRUE) {
	echo '<pre>'; print_r($array); if($exit) exit;
}

function __($string, $params = [])
{
    $CI =& get_instance();

    $string = strtr($string, ["'" => '’', '"' => '”']);
    $line = $CI->lang->line($string);

    if (!$line) {
        insertLangLine($string);
        if ($params) {
            $line = strtr($string, $params);
        } else {
            $line = $string;
        }
    } else if ($params) {
        $line = strtr($line, $params);
    }

    return $line;
}

function insertLangLine($line)
{
    foreach (['turkish', 'english'] as $language) {

        $file = APPPATH . "language/{$language}/my_lang.php";

        $newline = '$lang[\''.$line.'\'] = \''.$line.'\';' . PHP_EOL;
        file_put_contents($file, $newline, FILE_APPEND);
    }
}

function arrayMerge()
{
    $new_array = [];
    $arrays = func_get_args();

    foreach ($arrays as $array) {
        foreach ($array as $row) {
            $new_array[] = $row;
        }
    }

    return $new_array;
}

function object_to_array($object)
{
    $array = [];
    foreach($object as $k => $v) {
        if(strlen($k)) {
            if(is_object($v)) {
                $array[$k] = object_to_array($v); //RECURSION
            } else {
                $array[$k] = $v;
            }
        }
    }
    return $array;
}

function arrayColumnValue($array, $column_name)
{
    if (!count($array)) {
        return array();
    } elseif (! is_object($array)) {
        $array = object_to_array($array);
    }

    $tmp = [];
    foreach ($array as $key => $value) {
        if (!array_key_exists($column_name, $value)) {
            exit($column_name . ', key does not exist.');
        } elseif (array_key_exists($value[ $column_name ], $tmp)) {
            exit($column_name . ', should be unique.');
        }

        $tmp[ $value[ $column_name ] ] = $value;
    }

    return $tmp;
}

function hasPermisson($permisson)
{
    $CI =& get_instance();

    if ((int) $CI->user->role_id === 1) {
        return true;
    }

    if (isset($CI->user->permissions[ $permisson ])) {
        return true;
    }

    return false;
}

function second2String($seconds)
{
    $ret = "";

    /*** get the days ***/
    $days = intval(intval($seconds) / (3600*24));
    if($days> 0) {
        $ret .= "$days Gün ";
    }

    /*** get the hours ***/
    $hours = (intval($seconds) / 3600) % 24;
    if($hours > 0) {
        $ret .= "$hours Saat ";
    }

    /*** get the minutes ***/
    $minutes = (intval($seconds) / 60) % 60;
    if($minutes > 0) {
        $ret .= "$minutes dakika ";
    }

    /*** get the seconds ***/
    // $seconds = intval($seconds) % 60;
    // if ($seconds > 0) {
    //     $ret .= "$seconds seconds";
    // }

    return $ret;
}

function numberFormat($number, $decimals)
{
    $CI =& get_instance();
    return number_format($number, $decimals, $CI->number_format->decimal, $CI->number_format->thousands);
}
function numberUnFormat($number)
{
    $CI =& get_instance();

    // 9,999.99 --> 9999.99
    if ($CI->number_format->thousands == ',') {
        $number = str_replace(',', '', $number);
    }

    // 9.999,99 --> 9999.99
    if ($CI->number_format->decimal == ',') {
        $number = str_replace(',', '.', str_replace('.', '', $number));
    }

    return doubleval($number);
}

function id_format($id = 0, $length = 4, $fill = '0')
{
    return str_pad((string) $id, $length, $fill, STR_PAD_LEFT);
}

function sanitize($string, $trim = false) {
    $string = strip_tags( stripslashes( trim( filter_var($string, FILTER_SANITIZE_STRING) ) ) );
    $string = str_replace(array('‘','’','“','”'), array("'","'",'"','"'), $string);
    return ($trim) ? substr($string, 0, $trim) : $string;
}

function isGet($key)  { return isset($_GET[$key]) ? (trim($_GET[$key]) ? $_GET[$key] : FALSE) : FALSE; }
function isPost($key) { return isset($_POST[$key]) ? (trim($_POST[$key]) ? $_POST[$key] : FALSE) : FALSE; }

function get_status($status, $labels = array('Aktif', 'Pasif') ) {
	return $status
	? '<span class="label label-success label-mini">'.$labels[0].'</span>'
	: '<span class="label label-danger label-mini">'.$labels[1].'</span>';
}

function get_text_color($hex) {

    $hex       = ltrim($hex, '#');
    $threshold = 105;

    if (!preg_match('/^[a-fA-F0-9]{6}$/', $hex)) {
        return '#000000';
    }

    $R = hexdec(substr($hex, 0, 2));
    $G = hexdec(substr($hex, 2, 2));
    $B = hexdec(substr($hex, 4, 2));

    $delta = intval(($R * 0.299) + ($G * 0.587) + ($B * 0.114));

    return '#' . (255 - $delta < $threshold ? '000000' : 'ffffff');
}

function _date($format = 'Y-m-d H:i:s') {
	return date($format);
}

function doDate($date, $format = 'long') {
    if(!$date) return 'NaN';

    $CI =& get_instance();

    $format = in_array($format, ['long', 'short']) ? $CI->config->website->{'date_'.$format} : 'Y-m-d H:i:s';

    $date = new DateTime( $date );
    $date->setTimezone( new DateTimeZone( $_SESSION['dtz'] ) );
    
    return $date->format( $format );
}

// $convert2 = server or client
function dateConvert($date, $convert2 = 'server', $target_date_format = 'Y-m-d H:i:s') {

    if(!$date || !in_array($convert2, ['server', 'client'])) {
        return 'NaN';
    }

    $CI =& get_instance();

    // Convert date format - NOT: Sunucu saat dilimine dönüştrüülüyorsa kullanıcının formatı sunucu formatına dönüştürlüyor.
    if ($convert2 == 'server') {

        $date = DateTime::createFromFormat( dateFormat(), $date )->format( 'Y-m-d H:i:s' );
    }
    

    // Convert time to target time zone
    $current_zone = new DateTimeZone( ($convert2 == 'server' ? $CI->config->dtz[ 'client' ] : $CI->config->dtz[ 'server' ]) );
    $target_zone  = new DateTimeZone( $CI->config->dtz[ $convert2 ] );

    $date = new DateTime( $date, $current_zone );
    $date->setTimezone( $target_zone );
    
    return $date->format( $target_date_format );
}

// Farkı, gelen tarih tipinin de belirtilmesi
function dateConvert2($date, $convert2 = 'server', $source_date_format = 'Y-m-d H:i:s', $target_date_format = 'Y-m-d H:i:s') {

    if(!$date || !in_array($convert2, ['server', 'client'])) {
        return 'NaN';
    }

    $CI =& get_instance();


    $date = DateTime::createFromFormat( $source_date_format, $date )->format( 'Y-m-d H:i:s' );

    // Convert time to target time zone
    $current_zone = new DateTimeZone( ($convert2 == 'server' ? $CI->config->dtz[ 'client' ] : $CI->config->dtz[ 'server' ]) );
    $target_zone  = new DateTimeZone( $CI->config->dtz[ $convert2 ] );

    $date = new DateTime( $date, $current_zone );
    $date->setTimezone( $target_zone );
    
    return $date->format( $target_date_format );
}

function dateConvert3($source_dtz = 'UTC', $source_date_format = 'Y-m-d H:i:s', $date, $target_dtz = 'UTC', $target_date_format = 'Y-m-d H:i:s') {

    $d = DateTime::createFromFormat($source_date_format, $date, new DateTimeZone($source_dtz));
    $d->setTimeZone(new DateTimeZone($target_dtz));
    return $d->format($target_date_format);
}

function dateFormat($isJS = FALSE)
{
    $CI =& get_instance();

    if (preg_match('/^America\//', $CI->config->dtz[ 'client' ])) {
        return ($isJS) ? 'MM/DD/YYYY hh:mm A' : 'm/d/Y h:i A';
    }
    
    else {
        return ($isJS) ? 'DD-MM-YYYY HH:mm' : 'd-m-Y H:i';
        // return ($isJS) ? 'YYYY-MM-DD HH:mm' : 'Y-m-d H:i';
    }
}

function yearMonthFormat($isJS = FALSE)
{
    $CI =& get_instance();

    if (preg_match('/^America\//', $CI->config->dtz[ 'client' ])) {
        return ($isJS ? 'MM/YYYY' : 'm/Y');
    } else {
        return ($isJS ? 'YYYY-MM' : 'Y-m');
    }
}

function dateFormat1($isJS = FALSE)
{
    $CI =& get_instance();

    if (preg_match('/^America\//', $CI->config->dtz[ 'client' ])) {
        return ($isJS) ? 'MM/DD/YYYY' : 'm/d/Y';
    }
    
    else {
        return ($isJS) ? 'DD-MM-YYYY' : 'd-m-Y';
        // return ($isJS) ? 'YYYY-MM-DD HH:mm' : 'Y-m-d H:i';
    }
}

function dateReFormat($date, $source_date_format = 'Y-m-d H:i:s', $target_date_format = 'D m/d/Y')
{
    return date_format(date_create_from_format($source_date_format, $date), $target_date_format);
}

function between($value, $min, $max)
{
    return (bool) ($value >= $min && $value <= $max);
}

function isValidDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function checked($value, $check) {
	echo $value == $check ? 'checked="checked"' : NULL;
}

function isEqual($val1, $val2, $string) {
    echo $val1 == $val2 ? $string : NULL;
}

function selected($value, $check) {
    echo $value == $check ? 'selected' : NULL;
}

function messageAJAX($type='success', $message = NULL, $data = NULL, $content_type = null) {
    if ($content_type) {
        header('Content-Type: application/' . $content_type);
    }
	exit(json_encode( array('result' => ($type == 'success' ? 1 : 0), 'message' => $message, 'data' => $data) ));
}

function api_messageError($str = NULL) {
    $json = ['status' => 'ERROR', 'message' => (is_array($str) ? NULL : $str), 'count' => 0, 'items' => []];
    if(is_array($str)) {
    	$json = array_merge($json, $str);
    }
    exit(json_encode($json));
}

function api_messageOk($str = NULL) {
	$json = ['status' => 'SUCCESS', 'message' => (is_array($str) ? NULL : $str), 'count' => 0, 'items' => []];
    if(is_array($str)) {
    	$json = array_merge($json, $str);
    }
    exit(json_encode($json));
}

function encode_id($id = NULL, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
    $chars = str_split($chars);
    $first  = (int) ($id/count($chars));
    $second = (int) ($id-($first*count($chars)));
    return @$chars[$first] . @$chars[$second];
}

function decode_id($id = NULL, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
    $ids    = str_split($id);
    $first  = strpos($chars, @$ids[0]);
    $second = strpos($chars, @$ids[1]);
    return $first*strlen($chars)+$second;
}
// BUNUN YERINE logs_ kullanılacak !!!
function logs($user = NULL, $message = NULL, $add = NULL) {
    $CI =& get_instance();
    $CI->db->insert('logs', ['user_id' => $user, 'message' => $add . ($add?' - ':NULL) . $message, 'ip' => $CI->input->ip_address(), 'created' => _date()]);
    return $message;
}

function logs_($user = NULL, $message = NULL, $json = NULL, $type = NULL, $location = NULL) {
    $CI =& get_instance();
    $CI->db->insert('logs', [
        'user_id'       => $user,
        'message'       => $message,
        'location_id'   => $location,
        'type'          => $type,
        'json'          => ((is_array($json) || is_object($json)) ? json_encode( $json ) : $json), 
        'ip'            => $CI->input->ip_address(), 
        'created'       => _date(), 
        'url_coming'    =>@$_SERVER['HTTP_REFERER'], 
        'url_current'   =>current_url()]);
    return $message;
}

function error_($page) {
    require_once (FCPATH . 'application/views/errors/html/MY_templates/error_'.$page.'.php');
    exit(4);
}


function dropdownMenu($items, $current, $url, $label, $class='btn-primary btn-xl') {
    $items= array_column($items, 'title', 'id');
    $url  = trim($url, '/');

    $html = '<div class="btn-group">';
    $html.=     '<button type="button" class="btn '.$class.' dropdown-toggle" data-toggle="dropdown">';
    $html.=         (isset($items[$current]) ? $items[$current] : $label).' <span class="caret"></span>';
    $html.=     '</button>';
    $html.=     '<ul class="dropdown-menu" role="menu">';
    $html.=         '<li><a href="'.base_url($url).'">Tüm '.$label.'</a></li>';
    foreach ($items as $k=>$i):
    $html.=         '<li '.($current==$k?'class="active"':'').'>';
    $html.=             '<a href="'.base_url($url.'/'.$k).'">'.$i.'</a>';
    $html.=         '</li>';
    endforeach;
    $html.=     '</ul>';
    $html.= '</div>';
    echo $html;
}

function eUploadAttachments($data = array('name'=>'', 'file'=>'', 'input'=>''))
{
    echo    '<div class="col-xs-12 col-md-12">
                <div class="col-xs-2 col-sm-2 col-md-1 attachments-remove">x</div>
                <div class="col-xs-2 col-sm-2 col-md-1 attachments-percent"></div>
                <div class="col-xs-8 col-sm-8 col-md-10 attachments-url"><a href="'.$data['file'].'" target="_blank">'.$data['name'].'</a></div>
                <input type="hidden" name="_files[]" value="'.$data['input'].'">
            </div>';
}


function time_ago($ptime)
{

    $etime = time() - strtotime($ptime);

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'min',
                       'second' => 'sec'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

/*
    Return prefix + or -
 */
function prefixSign ($value = null)
{
    return (substr($value, 0, 1) == '-') ? $value : '+' . $value;
}

function addTax($price, $tax)
{
    return round($price + ($price * $tax / 100), 2);
}

function posmaksCmsUrl($url = null)
{
    return 'https://posmaks.com/'. $url;
}

function pointSizeCalc($i) {
    
    $left = 100;
    $top = 100;

    if ($left*$i < 1000) {

        $left = ($i * $left) + 20; 

    } else {
        $t =  floor($i*$left/1000) +1;
        $i = $i%10;
        $top = $top*$t;
        $left = ($i * $left) + 20;

    }
    
    $plan = 'top: '.$top.'px; left: '.$left. 'px; width: 75px; height: 75px;';
    return $plan;


}