var Shiftschedule = {
    init: function() {
        this.handle_table_hover();
        this.handle_form();
        this.hadnle_datetimepicker_inputs();
        this.hadnle_close_modal();

        this.handle_report_modal();
        this.handle_print_report();
    },


    handle_table_hover: function () {

        $('td').hover(function() {
            var t = parseInt($(this).index()) + 1;
            $('td:nth-child(' + t + ')', $(this).closest('table')).addClass('highlighted');
        },
        function() {
            var t = parseInt($(this).index()) + 1;
            $('td:nth-child(' + t + ')').removeClass('highlighted');
        });
    },

    handle_form: function () {

        $("body").on("click", "tr.scedule-click td:not(:first-child)", function(){
            
            var td = $(this),
            tr = td.closest("tr"),

            user = tr.data("user"),
            schedule = td.find("i.schedule").data("schedule"),
            date = td.find("i.date").data("date"),
            location = $(".schedule-table").data("location");

            var form = {
                "user":user,
                "date":date,
                "location":location,
                "schedule":schedule // only for edit
            };

            // ADD
            console.log(schedule);
            if (schedule === undefined) {
                $.get(base_url + 'shiftschedule/add', $.param(form), function(data) {
                    $("#modal-form .modal-body").html(data);
                });
            }

            // EDIT
            else {
                $.get(base_url + 'shiftschedule/edit/' + schedule, $.param(form), function(data) {
                    $("#modal-form .modal-body").html(data);
                });
            }

            $("#modal-form").modal("toggle");
        });
    },

    hadnle_datetimepicker_inputs: function () {

        $('body').on('focus', ".datetimepicker-schedule", function() {
            $(this).datetimepicker({
                format: base_date_format,
                stepping: 30,
            });
        });
    },

    hadnle_close_modal: function () {
        
        $('#modal-form').on('hidden.bs.modal', function () {
            if (! $(this).hasClass("reload")) {
                return;
            }
            setTimeout(function () {
                window.location.href = window.location;
            }, 100);
        });

    },

    // Master function data-success eval function. do not delete.
    _onSuccess: function() {
        $('#modal-form').addClass("reload");
         $('#modal-form .close').click();
    },









    handle_report_modal: function () {
        
        var modal = $('#modal-schedule_reports'),
        container_form = modal.find(".form-container"),
        container_result = modal.find(".result-container");

        modal.on('show.bs.modal', function () {

            container_form.removeClass("hidden").css("height", "auto");
            container_result.removeClass("hidden").addClass("hidden").css("height", 0);
        });


        $("body").on("click", ".create-report", function(){
            
            var form = $(this).closest("form").serializeArray();
            var location = $(".schedule-table").data("location");

            var date_start = $('#modal-schedule_reports input[name="date_start"]').val(),
                date_end = $('#modal-schedule_reports input[name="date_end"]').val();

            form.push({ "name": "location", "value": location });


            $.post(base_url + "shiftschedule/report", form, function(data) {
                
                var json = $.parseJSON(data);

                if (json.result) {

                    $("#table-dates").html(date_start + " - " + date_end + " tarihleri arasında");
                    container_result.find("tbody").html(json.data);

                    container_form.animate({height:0}, 200, function () {
                        container_form.addClass("hidden");

                        container_result.removeClass("hidden");
                        container_result.animate({height:"200"}, 300, function () {
                            $(this).height('auto');
                        });
                    });

                } else {
                    Pleasure.handleToastrSettings('Hata!', json.message, 'error');
                }

            });
        });
    },

    handle_print_report: function () {
        
        $("body").on("click", ".print-report", function(){
            console.log($("#modal-schedule_reports .result-container table"));
            let selector = $("#modal-schedule_reports .result-container table");
            printJS('scheduleTable','html');
        });
    }
    
}