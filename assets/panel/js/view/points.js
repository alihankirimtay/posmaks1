var Points = {

    config: {
        planner: false,
    },

    init: function(config) {

        if (typeof config !== "undefined") {
            $.extend(Points.config, config);
        }
        
        Points.floors();
        Points.zones();

        if (config.planner) {
            Points.planner();
            Points.zonesChanged();
            Points.hadnleBackgroundInputChanged();
        }
    },

    planner: function () {
        
        $('.zone').on("change", Points.fillPointZones);

        Points.handleDroppableZones();

        $("#planner-form button[name=dosubmit]").on("click", Points.mergeFormsOnSubmit);
        $("body").on("click", "#planner .pnt-delete", Points.handleDelete);
        $("body").on("click", "#planner-form .background", Points.handleSelectImageDialog);

    },

    fillPointZones: function () {

        $("#points, #planner").html("");
        $("#planner-form button[name=dosubmit]").addClass("hidden");

        $.post(base_url+"points/ajax/findPointsByData", $("form#settings").serializeArray(), function(json) {
            $.each(json.data.points, function(key, point) {
                
                if (!!point.planner_settings) {
                    Points.add2Planner(point);
                } else {
                    Points.add2Points(point);
                }
            });
        }, "json");
    },

    handleSelectImageDialog: function () {
        $("#planner-form input[name=background]").trigger("click");
    },

    hadnleBackgroundInputChanged: function () {
        $("#planner-form input[name=background]").on("change", function(){

            let input = this;

            if (input.files && input.files[0]) {

                Points.hasChanges();

                let reader = new FileReader();

                reader.onload = function(e) {
                    $('#planner').addClass("has-background").css('background', `url(${e.target.result})`);
                }
                reader.readAsDataURL(input.files[0]);
            }
        });
    },

    handleDelete: function () {

        Points.hasChanges();
        

        var
        parent = $(this).closest(".pnt"),
        point_title = $("div:first", parent).html(),
        input_name = $("input", parent).attr("name"),
        item = $(`
            <div class="pnt pnt-default">
                <span class="fa fa-times-circle pnt-delete"></span>
                <div>${point_title}</div>
                <input type="hidden" name="${input_name}" value="">
            </div>
        `);

        item
        .draggable({
            containment: '#points-container',
            stack: '#points .pnt',
            cursor: 'move',
            revert: 'invalid',
            opacity: 0.4,
        })
        .appendTo("#points");

        parent.remove();
    },

    handleDroppableZones: function () {
        
        $("#planner").droppable({
            accept: '#points .pnt, #planner .pnt',
            drop: function(event, ui) {

                Points.hasChanges();

                var
                droppedItem = ui.helper.clone(),
                width = parseFloat(ui.helper.width()),
                height = parseFloat(ui.helper.height());

                if (droppedItem.hasClass("dropped-component")) {
                    let
                    left = parseInt($(this).offset().left),
                    top = parseInt($(this).offset().top);

                    droppedItem.find("input").val(
                        `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                    );
                } else {

                    let
                    left = parseInt(ui.offset.left - $(this).offset().left),
                    top = parseInt(ui.offset.top - $(this).offset().top);
                    left = left - left%5;
                    top = top - top%5;

                    droppedItem.find("input").val(
                        `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                    );

                    droppedItem
                    .addClass("dropped-component")
                    .css({
                        left: left,
                        top: top,
                        opacity: 1,
                        width: width,
                        height: height,
                    })
                    .appendTo($(this));

                    ui.helper.remove();

                    droppedItem.draggable({
                        containment: '#planner',
                        stack: '#planner .pnt',
                        cursor: 'move',
                        revert: 'invalid',
                        opacity: 0.4,
                        grid: [5, 5],
                        stop: function (event, ui) {
                            let
                            top = ui.position.top,
                            left = ui.position.left,
                            width = $(this).width(),
                            height = $(this).height();
                            $(this).find("input").val(
                                `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                            );
                        }
                    })
                    .resizable({
                        containment: '#planner',
                        grid: 5,
                        stop: function(event, ui) {
                            let
                            top = ui.position.top,
                            left = ui.position.left,
                            width = ui.size.width,
                            height = ui.size.height;

                            $(this).find("input").val(
                                `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                            );

                            Points.hasChanges();
                        }
                    });
                }
            }
        });
    },

    add2Points: function (point) {
        
        let item = $(`
            <div class="pnt pnt-default">
                <span class="fa fa-times-circle pnt-delete"></span>
                <div>${point.title}</div>
                <input type="hidden" name="points[${point.id}]" value="">
            </div>
        `);

        item
        .draggable({
            containment: '#points-container',
            stack: '#points .pnt',
            cursor: 'move',
            revert: 'invalid',
            opacity: 0.4,
        })
        .appendTo("#points");
    },

    add2Planner: function (point) {
       
        let item = $(`
            <div class="pnt pnt-default dropped-component" style="${point.planner_settings}">
                <span class="fa fa-times-circle pnt-delete"></span>
                <div>${point.title}</div>
                <input type="hidden" name="points[${point.id}]" value="${point.planner_settings}">
            </div>
        `);

        item
        .draggable({
            containment: '#planner',
            stack: '#planner .pnt',
            cursor: 'move',
            revert: 'invalid',
            opacity: 0.4,
            grid: [5, 5],
            stop: function (event, ui) {
                let
                top = ui.position.top,
                left = ui.position.left,
                width = $(this).width(),
                height = $(this).height();

                top = top - top%5;
                left = left - left%5;

                $(this).css({
                    top: top + 'px',
                    left: left + 'px',
                });

                $(this).find("input").val(
                    `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                );
            }
        })
        .resizable({
            containment: '#planner',
            grid: 5,
            stop: function(event, ui) {
                let
                top = ui.position.top,
                left = ui.position.left,
                width = ui.size.width,
                height = ui.size.height;

                $(this).find("input").val(
                    `top: ${top}px; left: ${left}px; width: ${width}px; height: ${height}px; `
                );

                Points.hasChanges();
            }
        })
        .appendTo("#planner");
    },

    mergeFormsOnSubmit: function () {

        $("#planner-form input[name=location]").val($("#settings select[name=location]").val());
        $("#planner-form input[name=floor]").val($("#settings select[name=floor]").val());
        $("#planner-form input[name=zone]").val($("#settings select[name=zone]").val());

        $("#planner-form button[name=dosubmit]").addClass("hidden");
    },

    hasChanges: function () {
        $("#planner-form button[name=dosubmit]").removeClass("hidden");
    },

    floors: function() {
        $(".location").on("change", function() {

            $.post(base_url + 'floors/ajax/getFloorsByLocationId', {
                location_id: $(this).val()
            }, function(data) {

                floor_list = $(".floor");
                floor_list.find('option').not(':first').remove();
                $(".zone").find('option').not(':first').remove();
                $(".zone").trigger("chosen:updated");
                
                if (data.result) {
                    $.each(data.data.floors, function(index, val) {
                        floor_list.append($("<option></option>").val(val.id).html(val.title));
                    });
                }
                floor_list.trigger("chosen:updated");
                $("#planner-form .background").addClass("hidden");
            }, "json");
        });
    },
    zones: function() {
        $(".floor").on("change", function() {

            $.post(base_url + 'zones/ajax/getZonesByFloorId', {
                location_id: $(".location").val(),
                floor_id: $(".floor").val(),
            }, function(data) {
                zone_list = $(".zone");
                zone_list.find('option').not(':first').remove();

                
                if (data.result) {
                    $.each(data.data.zones, function(index, val) {
                        zone_list.append($("<option></option>").attr("background", val.background).val(val.id).html(val.title));
                    });
                }
                zone_list.trigger("chosen:updated");
            }, "json");
        });
    },
    zonesChanged: function () {
        $(".zone").on("change", function() {
            let zone = $(this),
            val = parseInt(zone.val()),
            background = $(":selected", zone).attr("background");

            $("#planner-form .background").addClass("hidden");
            $("#planner").removeClass("has-background").css("background", "");

            if (val) {
                $("#planner-form .background").removeClass("hidden");
                if (!!background) {
                    $("#planner").addClass("has-background").css("background", `url(${base_url + background})`);
                }
            }
        }); 
    },
};