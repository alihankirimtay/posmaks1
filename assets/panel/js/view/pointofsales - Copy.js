var Pointofsales = {

    selectPack:function(){
        
        $('body').on('click', '.prodcut-label' ,  function(){
            
           $(this).closest('.card').find('.product-btn').trigger('click'); 
        });
        
        $('body').on('click' , '.product-btn' , function(event){
            
          
          target = $(this).closest('.packs');

        
          if (target.find('.card').hasClass('active') == true) {
            
            target.find('input[name="each"]').val(0).attr('disabled' ,'disabled');
            target.find('.card').removeClass('active');
            
          }else{
            
             target.find('input[name="each"]').val(1).removeAttr('disabled','disabled');
             target.find('.card').addClass('active');
            
          }
        });
        
    },

  wizardStep:function(){

    $('button[name="next"]').on('click' , function(){
      
      
      // Aktif olan Step ID alınıyor 
      step = $('.tab-pane:visible').attr('id');
      
      // Step 1
      
      if(step == 'form2_tab1'){
        
        // Değerler;
        
        ticket          = $('input[name="ticked"]').val(); // <- Ticket ID
        currency        = $('.packs').find('.active').find('.currency').html(); // Para Birimi
        location_id     = $('select[name="location"]').val(); // <- Lokasyon ID
        product_name    = $('.packs').find('.active').find('h4').html(); // <- Ürün İsmi
        product_type    = $('.packs').find('.active').find('.btn-ripple').html(); // <- Ürün Tipi
        product_amount = $('.packs').find('.active').find('.price'); // <- Ürün Fiyatının Bulunduğu Alan
        product     = []; // Ürün için değişken oluşturuluyor
        each            = []; // Ürün Adeti için değişken oluşturuluyor
        total           = 0; // Ürün toplam fiyatı için değişken oluşturuluyor
              
        
        
        // Seçilmiş olan ürünleri topluyor [product_id // each]
        
        $('input[name="product[]"]:checked').each(function(e,a){
                product.push({product_id:$(this).val() , each:$(this).parent().find('input[name="each"]').val()});
        });
        
        product = product;
        
        // Ürünün toplam fiyatı çıkıyor
        $.each(product_amount , function(e,a){
            each = $(this).closest('.card').find('input[name="each"]').val();
            total += parseFloat($(this).html()*each);
        });
        
        // Ürünün toplam fiyatı
        total = total.toFixed(2);
        
        
        // Ürünün toplam fiyatı step 2 de gösterilmek için yazdırılıyor 
        $('#step2-total').html(currency+' '+total);
        
        
        // Php tarafına gönderilicek veriler
        data  = {product:product , ticket:ticket  , location_id:location_id};
        
        // Step
        index  = 1;
        
        // Bir sonraki step
        next_step = '#form2_tab2';
        
        // Geri buttonu aktif oluyor
        $('button[name="back"]').show();

      }
    
      
      // Step 2
      
      if(step == 'form2_tab2'){
        
        // Değerler ;
        
        payment                = $('input[name="payment-type"]:checked').val(); // Ödeme Tipi
        payment_title          = $('input[name="payment-type"]:checked').attr('id'); // Ödeme tipi ismi
        discount_type          = $('select[name="discount_type"]').val(); // İndirim tipi [Default : amount]
        discount_amounth       = $('input[name="amounth"]').val(); // İndirim Tutarı [Default : 0]
        session                = $('input[name="session"]').val(); // Eğer indirim için manager giriş yapmış ise session tutuluyor
        
        
        // İndirim Yoksa Default Veriler Oluşturuluyor
        if(typeof discount_type == 'undefined'){
          discount_type = currency;
        }

        if(typeof discount_amounth == 'undefined'){
          discount_amounth = 0;
        }
        ///
        
        // Php tarafına gidicek veriler 
        data = {payment:payment , discount_type:discount_type ,amounth:discount_amounth,session:session };
        
        // Step
        index = 2;
        
        // Bir Sonraki Step
        next_step = '#form2_tab3';
        
        

        // Fatura Önizleme //
        
        // Ürün , Ürün Tipi , Adet , Birim Fiyatı ve Toplam Tutarı
        
        $.each($('.packs').find('.active') , function(e , a){
            
            //Ürün İsmi
            product_name = $(this).find('h4').html();
            
            // Ürün Tipi
            product_type = $(this).data('type');
            
            //Ürün Adeti
            product_each = $(this).find('input[name="each"]').val();
            
            //Birim Fiyatı
            unit_price = $(this).find('.price').html();
            
            //Ürün Toplam Fiyatı
            total_price = parseFloat(unit_price*product_each).toFixed(2);
            
            $('#p_detail').append('\
                                <tr>\
                                    <td>'+product_name+'</td>\
                                    <td>'+product_type+'</td>\
                                    <td>'+product_each+'</td>\
                                    <td>'+ currency +' '+unit_price+'</td>\
                                    <td>'+ currency +' '+total_price+'</td>\
                                </tr>\
                            ');
             
        });
        
        // İndirim Hesaplama
        if(typeof discount_type == 'undefined'){
          gtotal = total.toFixed(2);
        }
        if(discount_type == currency){

          gtotal = total-discount_amounth;
        }

        if(discount_type == '%'){
          gtotal = total-(total*(discount_amounth/100));
        }  
        
        
        // Ara Toplam
        $('#stotal').html(currency +' ' + total);
        
        // İndirim
        $('#discount').html(discount_type+' '+discount_amounth);
        
        // Toplam
        $('#total').html(currency+' ' + parseFloat(gtotal).toFixed(2));
        
        //Ticket
        $('#ticket').html(ticket);
        
        // Ödeme Tipi
        $('#payment_type').html(payment_title);
        
        

      }


      if(step == 'form2_tab3'){
        
        values = {
          ticket:ticket,
          location_id:location_id,
          product:product,
          payment:payment,
          session:session,
          amounth:total,
          disciont_amounth:discount_amounth,
          discount_type:discount_type,
        }

        $.ajax({
          url: base_url+'pointofsales/index',
          data: values,
          type: 'post',
          dataType:'json',
          success:function(json){
            if(json.result == 0){
              Pleasure.handleToastrSettings('Error!', json.message, 'error');
            }else{
              window.location.href= base_url+'pointofsales/view/'+json.data;
            }
          }
        });

        return false;
      }

        $.ajax({
          url: base_url+'pointofsales/SaleControl/'+index,
          data: data,
          type:'post',
          success:function(json){
            json = $.parseJSON(json);

            if(json.result == 0){
              Pleasure.handleToastrSettings('Error!', json.message, 'error');
            }else{
              $('.tab-pane:visible').removeClass('active');
              $(next_step).addClass('active');
              $('.bs-wizard-prev').show();
            }
          }
          
          });

      });

      $('body').on('click' , 'button[name="back"]' , function(){
          step = $('.tab-pane:visible').attr('id');

          if(step == 'form2_tab3'){

              $('.tab-pane:visible').removeClass('active');
              $('#form2_tab2').addClass('active');
          }

          if(step == 'form2_tab2'){
            $('.tab-pane:visible').removeClass('active');
            $('#form2_tab1').addClass('active');
            $('button[name="back"]').hide();
          }
      })

  },


  managerLogin:function(){

    $('#mLogin').on('click',function(){
       location_id = $('select[name="location"]').val();
       pass  = $('#M_pass').val();

       $.ajax({

          url: base_url+'pointofsales/managerLogin',
          data: {location_id:location_id , password:pass},
          type:'post',
          dataType: 'json',
          success:function(json){
            if(json.result == 0){
              Pleasure.handleToastrSettings('Error!', json.message, 'error');
            }else{
              $('.discount').html('\
                <div class="col-md-6">\
                  <label>Discount Type</label>\
                  <select class="form-control" name="discount_type">\
                    <option>$</option>\
                    <option>%</option>\
                  </select></div>\
                <div class="col-md-6">\
                <label>Discount Amount</label>\
                  <input type="text" class="form-control" name="amounth" />\
                </div>\
                <input type="hidden" value="'+json.message+'" name="session" />')
            }
          }

       });
    });

  },


  getPacks:function(){
    $('select[name="location"]').on('change',function(){
       id = $(this).find('option:selected').val();

       $.ajax({
          url: base_url+'pointofsales/getPacks/'+id,
          success:function(result){
              $('.pack-list').html(result);
          }
       });
    })
  },





    
    init:function(){
        this.selectPack();
        this.wizardStep();
        this.managerLogin();
        this.getPacks();

        // $('button[type="submit"]').on('click' , function(){
        //     window.open(base_url+'sales/invoice' , '_blank');
        // });
    }
}
