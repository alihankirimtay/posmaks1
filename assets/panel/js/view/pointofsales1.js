function _loading (action) {
  if(action == "add") $("body").append("<div class='loading-spinner'> <div class='loading-spinner-bussy'></div> </div>");
  else if(action == "delete") $(".loading-spinner").remove();
}

var Pointofsales = {

  settings: {
    website_virtual_keyboard: false
  },

  init: function(settings) {

    if (typeof settings !== "undefined") {
      $.extend(Pointofsales.settings, settings);
    }
    
    this._css();

    this.onSelect_location();
    this.onClick_product();
    this.onChange_productPortion();
    this.onClick_processProduct();
    this.onClick_editProduct();
    this.onClick_remove_tax();
    this.onClick_add_payment();
    this.onClick_remove_payment();


    this.handle_note();
    this.onClick_complete();
    this.on_click_delete_order();

    this.on_open_tables_modal();
    this.handle_move_table();
    
    this.handleVESCheck();
    this.handleResetVES();

    this.handleCompletedOrders();
    this.on_click_do_seen_order();

    this.handle_sound_record();

    this.handle_session_managment();

    this.on_click_print_adisyon();

    this.onShown_interimPaymentsModal();

    this.handle_OperatorSessions();
  },

  _css: function() {

  },

  on_open_tables_modal: function() {
    // After modal openedn get tables
    $('#table-select-modal').on('shown.bs.modal', function () {
      $.post(base_url+'pointofsales/getTables', {
        location: $("select[name='location_id']").val()
      }, function(data) {

        data = $.parseJSON(data);
        $("#table-select-modal .modal-body").html(data.data);
      });
    });
  },

  // handle location select
  onSelect_location: function() {

    $("select[name='location_id']").on("change", function(){
      Pointofsales.get_Products( $(this).val() );
      //Pointofsales.reset_pos();
    });
  },

  get_Products: function(location) {

    $.post(base_url+'pointofsales/getProducts', {
      location: location
    }, function(data) {

      data = $.parseJSON(data);
      $("#pos-products").html(data.data.html);

      Pointofsales.setPoroductsScroolSize();
      Pointofsales.handleBarCodeReader();
    });

  },

  setPoroductsScroolSize: function () {
    
    var offset = $(".tab-content div[id^='tab-products-']:first").offset(),
    height = $(window).height() - offset.top - 10;

    $(".tab-content div[id^='tab-products-']").css("max-height", height + "px");
  },

  productPortions: {},
  productEditingIndex: 0,
  onClick_product: function() {

    $("body").on("click", "#pos-products .product", function() {

      let
      product_id = $(this).data("id"),
      modal = $("#modal-product_content");

      Pointofsales.productPortions = {};
      Pointofsales.productEditingIndex = 0;
      $(".modal-title i", modal).removeClass("hidden");
      $(".modal-title span", modal).html("");
      $('input[name="quantity"]', modal).val(1);
      $("#portions", modal).html("");
      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");
      modal.modal("toggle");
      $(".process_product", modal).html("Ekle");

      $.post(base_url + "pointofsales/ajax/findProductPortions", {
        product_id: product_id
      }, function(json) {

        $(".modal-title i", modal).addClass("hidden");

        if (!json.result) return;

        Pointofsales.productPortions = json.data.products;

        $.each(json.data.products, function(key, product) {
          $("#portions", modal).append($("<option>").val(product.id).html(product.title));     
        });

        $("#portions", modal).trigger("change");

      }, "json");


      return;

      

      // Check the table is contain same row.
      var include_same_product = 0;
      $.each($("#pos-list table tbody tr"), function(index, val) {
        if ($(this).data('id') == id) {
          include_same_product = id;

          container = $("#pos-list");
          scrollTo = $(this);
          container.stop().animate({
            scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
          }, 500);

          $(this)
          .find('input[name^="products"]')
          .val(function (i, old_value) {
            return parseInt(old_value) + 1;
          })
          .focus()
          .select()
          .trigger("input");
        }
      });

      if (include_same_product) {
        return;
      };
      // END - Check the table is contain same row.


      _loading("add");
      
      $.post(base_url+'pointofsales/getProductById', {
        location: $("select[name='location_id']").val(),
        product: id
      }, function(data) {

        _loading("delete");

        data = $.parseJSON(data);
        row  = $(data.data);

        $("#pos-list tbody").append(row);
        $("#pos-list").stop().animate({
          scrollTop: $("#pos-list tbody")[0].scrollHeight
        }, 1000);

        row.find('input[name^="products"]').focus().select();

        Pointofsales.calculate();
      });
    });

    // Pointofsales.onClick_product_spin();
    Pointofsales.onClick_product_remove();
  },

  onChange_productPortion: function () {
    
    let modal = $("#modal-product_content");

    $("#portions", modal).on("change", function(){

      let
      product_id = $(this).val(),
      product = Pointofsales.productPortions[product_id];

      $(".modal-title span", modal).html(product.title);

      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");

      $.each(product.stocks, function(index, val) {
        $("#stocks", modal).append(
          `<label>
              <input type="checkbox" name="stocks[${val.id}]" value="${val.id}" class="strikethrough hidden">
              <span>${val.title}</span>
          </label>`
        );
      });

      $.each(product.additional_products, function(index, val) {
        $("#additional_products", modal).append(
          `<div class="checkbox">
            <label>
                <input type="checkbox" name="additional_products[${val.id}]" value="${val.id}">
                <span>${val.title} <small>(${val.price} TL)</small></span>
            </label>
          </div>`
        );
      });

    });

  },

  onClick_processProduct: function () {

    $("#modal-product_content").on("click", ".process_product", function() {

      let
      modal = $("#modal-product_content"),
      form = $("form", modal),
      product_id = $("#portions", form).val(),
      quantity = $("input[name=quantity]", form).val(),
      index = -new Date(),
      product = Pointofsales.productPortions[product_id],
      price = parseFloat(product.price),
      total = parseFloat(product.price * quantity),
      tax = parseFloat(product.tax),
      row = null,
      items = "",
      total_additional_products = 0;

      // product is editing 
      if (Pointofsales.productEditingIndex !== 0) {

        index = Pointofsales.productEditingIndex;

        $(`#pos-list input[name^="products[${index}][stocks]"]`).remove();
        $(`#pos-list input[name^="products[${index}][additional_products]"]`).remove();
      }

      if ($('input[name^="stocks["]:checked', form).length) {
        $.each($('input[name^="stocks["]:checked', form), function(key, val) {
          let stock_id = $(this).val();
          items += `<input type="hidden" name="products[${index}][stocks][${stock_id}]" value="${stock_id}">`;
        });
      }

      if ($('input[name^="additional_products["]:checked', form).length) {
        $.each($('input[name^="additional_products["]:checked', form), function(key, val) {
          let 
          additional_product_id = $(this).val(),
          additional_product_price = product.additional_products[additional_product_id].price,
          additional_product_tax = product.additional_products[additional_product_id].tax;
          items += `<input type="hidden" name="products[${index}][additional_products][${additional_product_id}]" value="${additional_product_id}" price="${additional_product_price}" tax="${additional_product_tax}">`;
          total_additional_products += parseFloat(additional_product_price);
        });
      }

      

      total = total + total_additional_products * quantity;
      if (total_additional_products) {
        total_additional_products = ` (+${total_additional_products.toFixed(2)} TL)`;
      } else {
        total_additional_products = "";
      }
      
      // product is editing 
      if (Pointofsales.productEditingIndex !== 0) {

        let tr = $(`#pos-list tr[data-index="${index}"]`);

        $(".pr-quantity b", tr).html(quantity);
        $(`input[name="products[${index}][quantity]"]`, tr).val(quantity);
        $(".pr-title", tr).html(`${product.title} ${total_additional_products}`);
        $(".pr-price", tr).html(product.price);
        $(".pr-total", tr).html(total.toFixed(2));
        $(".pr-quantity", tr).append(items);

      } else {

        row = `
          <tr data-index="${index}" class="pr-container">
            <td><span class="fa fa-pencil small btn btn-primary btn-xs edit" aria-hidden="true"></span></td>
            <td class="pr-quantity">
              <b>${quantity}</b>
              <input type="hidden" name="products[${index}][product_id]" value="${product_id}">
              <input type="hidden" name="products[${index}][sale_product_id]" value="">
              <input type="hidden" name="products[${index}][quantity]" value="${quantity}" price="${price}" tax="${tax}">
              ${items}
            </td>
            <td class="small pr-title">${product.title} ${total_additional_products}</td>
            <td class="small text-right pr-price">${product.price}</td>
            <td class="small text-right pr-total">${total.toFixed(2)}</td>
            <td class="text-right"><span class="fa fa-times small btn btn-danger btn-xs trash" aria-hidden="true"></span></td>
          </tr>
        `;

        $("#pos-list tbody").append(row);
      }

      modal.modal("toggle");
      
      $("#pos-list").stop().animate({
        scrollTop: $("#pos-list tbody")[0].scrollHeight
      }, 1000);

      Pointofsales.calculate();
    });

  },

  onClick_editProduct: function () {

    $("body").on("click", "#pos-list .edit", function(){

      let
      modal = $("#modal-product_content"),
      row = $(this).closest("tr"),
      index = row.data("index"),
      product_id = $(`input[name="products[${index}][product_id]"]`, row).val();
      quantity = $(`input[name="products[${index}][quantity]"]`, row).val();

      Pointofsales.productPortions = {};
      Pointofsales.productEditingIndex = index;
      $(".modal-title i", modal).removeClass("hidden");
      $(".modal-title span", modal).html("");
      $('input[name="quantity"]', modal).val(quantity);
      $("#portions", modal).html("");
      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");
      modal.modal("toggle");
      $(".process_product", modal).html("Düzenle");

      $.post(base_url + "pointofsales/ajax/findProductPortions", {
        product_id: product_id
      }, function(json) {

        $(".modal-title i", modal).addClass("hidden");

        if (!json.result) return;

        Pointofsales.productPortions = json.data.products;

        $("#portions", modal).append(
          $("<option>")
          .val(json.data.products[product_id].id)
          .html(json.data.products[product_id].title)
        );

        $("#portions", modal).trigger("change");

        let stocks = $(`input[name^="products[${index}][stocks]"]`, row);
        $.each(stocks, function(key, row) {
          let stock_id = $(this).val();
          $(`input[name="stocks[${stock_id}]"]`, modal).prop("checked", true);
        });

        let additional_products = $(`input[name^="products[${index}][additional_products]"]`, row);
        $.each(additional_products, function(key, row) {
          let additional_product_id = $(this).val();
          $(`input[name="additional_products[${additional_product_id}]"]`, modal).prop("checked", true);
        });
        
      }, "json");

    });
  },

  /* Yeni sistemle beraber iptal oldu
  onClick_product_spin: function() {
    $("body").on("click", '.e-number-spin',function(){
      $(this).find('input[name^="products"]').trigger("input");
    });

    $("body").on("input propertychange", 'input[name^="products"]',function(){
      
      var form = $("#pos"),
      form_data = form.serializeArray()
      input = $(this),
      product_id = input.closest("tr").data("id")
      product_quantity = $("#pos input[name='products["+product_id+"]']").val();

      form_data.push({"name": "product_id", "value": product_id});
      form_data.push({"name": "product_quantity", "value": product_quantity});

      $.post(base_url+"pointofsales/checkProductInSale", form_data, function(json) {
        
        if (json.result) {

          if (json.data.quantity === 0) {
            input.closest("tr").remove();
          } else{
            input.closest("tr").find("td:eq(3)").html(json.data.price);
          }
          
          Pointofsales.calculate();

          // ürün iptal inputları eklenecek.
          Pointofsales.handleCancelledProductInputs(json);

        } else {

          quantity = product_quantity | 0;
          input.val(parseInt(quantity) + 1);
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');

        }

      }, "json");

    });
  },*/

  onClick_product_remove: function() {
    $("body").on("click", "#pos-list tbody tr .trash", function(){

      var form = $("#pos"),
      form_data = form.serializeArray()
      button = $(this),
      product_id = button.closest("tr").data("id");
      form_data.push({"name": "product_id", "value": product_id});
      form_data.push({"name": "product_quantity", "value": 0});

      $.post(base_url+"pointofsales/checkProductInSale", form_data, function(json) {
        
        if (json.result) {

          button.closest("tr").remove();         
          Pointofsales.calculate();

          // ürün iptal inputları eklenecek.
          Pointofsales.handleCancelledProductInputs(json);

        } else {

          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
        }

      }, "json");

    });
  },

  handleCancelledProductInputs: function(json) {

    // ürün iptal inputları eklenecek.
    if (typeof json.data.canceled.product_id !== "undefined") {

      // input varsa
      if ($("#pos input[name='cancelled_completed_products["+json.data.canceled.product_id+"]']").length) {
        $("#pos input[name='cancelled_completed_products["+json.data.canceled.product_id+"]']").val(json.data.canceled.quantity);
      } else {
        $("#pos").append($('<input>', {
          "type": "hidden",
          "name": "cancelled_completed_products["+json.data.canceled.product_id+"]",
          "value": json.data.canceled.quantity
        }));
      }

      // console.log("eklendi", "iptal sayısı:" + json.data.canceled.quantity, "kalan:" + $("#pos input[name^='cancelled_completed_products']").first().val());


      // sessionu ekle
      if ($("#pos input[name='session_cancelcompletedproduct']").length) {
        $("#pos input[name='session_cancelcompletedproduct']").val(json.data.canceled.session);
      } else {
        $("#pos").append($('<input>', {
          "type": "hidden",
          "name": "session_cancelcompletedproduct",
          "value": json.data.canceled.session
        }));
      }

    }

  },

  onClick_remove_tax: function() {
    $("body").on("click", "#button-remove-tax", function(){

      modal = ' \
        <div id="pos-modal" class="modal fade" tabindex="-1" role="dialog"> \
          <div class="modal-dialog"> \
            <div class="modal-content"> \
              <div class="modal-header"> \
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
                <h4 class="modal-title">Lütfen Yönetici Hesabıyla Giriş Yapınız</h4> \
              </div> \
              <div class="modal-body"> \
                <form name="form" id="pos-discount-form" class="form-horizontal" enctype="multipart/form-data" method="POST"> \
                  <div class="input-group"> \
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> \
                    <input type="text" class="form-control input-sm" name="pos-username" value="" placeholder="Kullanıcı/Eposta" autocomplete="off"> \
                  </div> \
                  <div class="input-group"> \
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span> \
                    <input type="password" class="form-control input-sm" name="pos-password" placeholder="Şifre" autocomplete="off"> \
                  </div> \
                </form> \
              </div> \
              <div id="pos-modal-alert" class=""> \
              </div> \
              <div class="modal-footer"> \
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Kapat</button> \
                <button id="pos-modal-submit" type="button" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-log-in"></i> Kaydet</button> \
              </div> \
            </div> \
          </div> \
        </div>';
        
        if (! $("#pos-modal").length) {
          $("body").append(modal);

          $("body").on("click", "#pos-modal #pos-modal-submit", function(){

            $.post(base_url+'pointofsales/managerAccount', {
              location: $("select[name='location_id']").val(),
              username: $("#pos-modal input[name='pos-username']").val(),
              password: $("#pos-modal input[name='pos-password']").val()
            }, function(json) {

              result = $.parseJSON(json);

              if (result.result) {

                $("#pos-modal").modal("toggle").remove();
                $("body").removeClass("modal-open").css("padding-right", "0");

                $('#tax-total').css("text-decoration", "line-through");
                $('#pos').append('<input type="hidden" name="tax_ramoved" value="'+result.data.session+'">');
                $("#button-remove-tax").remove();


              } else {

                $("#pos-modal-alert")
                  .removeClass("alert alert-danger")
                  .addClass("alert alert-danger")
                  .html(result.message);
              }

              Pointofsales.calculate();
            });
          });
        }

        $("#pos-modal").modal({
          backdrop: false,
          keyboard: false,
          show: true,
        });



      // Pointofsales.calculate();
    });
  },


  onClick_add_payment: function () {

    $("body").on("click", "#add-cash, #add-credit, #add-voucher, #add-ticket, #add-sodexo",function(){

      // if (!Pointofsales.table_exists()) return;

      type = $(this).attr("id");
      rest_value = $("#pos-rest").html();
      rest = parseFloat(rest_value.replace(/,/g, "")) ? rest_value : "0.00";
      rest = (rest < 0) ? "0.00" : rest;

      var input;

      if (type == "add-credit") {

        input = $(' \
        <div class="input-group"> \
            <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span> \
            <div class="inputer"> \
                <div class="input-wrapper"> \
                    <input name="payments[credit][]" type="text" class="form-control text-right numpad" value="'+rest+'"> \
                </div> \
            </div> \
        </div>');

        $("#payments-in-credit").append(input);

      }
      else if (type == "add-cash") {

        input = $(' \
        <div class="input-group"> \
            <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span> \
            <div class="inputer"> \
                <div class="input-wrapper"> \
                    <input name="payments[cash][]" type="text" class="form-control text-right numpad" value="'+rest+'"> \
                </div> \
            </div> \
        </div>');

        $("#payments-in-cash").append(input);

      }
      else if (type == "add-voucher") {

        input = $(' \
        <div class="input-group"> \
            <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span> \
            <div class="inputer"> \
                <div class="input-wrapper"> \
                    <input name="payments[voucher][]" type="text" class="form-control text-right numpad" value="'+rest+'"> \
                </div> \
            </div> \
        </div>');

        $("#payments-in-voucher").append(input);

      }
      else if (type == "add-ticket") {

        input = $(' \
        <div class="input-group"> \
            <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span> \
            <div class="inputer"> \
                <div class="input-wrapper"> \
                    <input name="payments[ticket][]" type="text" class="form-control text-right numpad" value="'+rest+'"> \
                </div> \
            </div> \
        </div>');

        $("#payments-in-ticket").append(input);

      }
      else if (type == "add-sodexo") {

        input = $(' \
        <div class="input-group"> \
            <span class="input-group-addon"><i class="remove-payment btn btn-xs fa fa-minus-square"></i></span> \
            <div class="inputer"> \
                <div class="input-wrapper"> \
                    <input name="payments[sodexo][]" type="text" class="form-control text-right numpad" value="'+rest+'"> \
                </div> \
            </div> \
        </div>');

        $("#payments-in-sodexo").append(input);

      }

    if (Pointofsales.settings.website_virtual_keyboard) {
      input.find('input')
      .keyboard({
          lang: "tr",
          layout: 'numpad',
          reposition : true,
          usePreview: true,
          autoAccept: true,
          css: {
            buttonDefault: 'btn btn-xl',
          },
          change : function(event, keyboard, input) {
            $(input).trigger("input");
          }
        }).addTyping({
          showTyping: true,
          delay: 250
        });
      }

      input.find('input').focus().select();


      Pointofsales.calculate();
    });



    $("body").on("input propertychange", 'input[name^="payments["]',function(){
      Pointofsales.calculate();
    });

  },

  onClick_remove_payment: function() {

    $("body").on("click", ".remove-payment",function(){
      $(this).closest(".input-group").remove();

      Pointofsales.calculate();
    });
  },

  calculate: function () {

    let
    total = 0,
    subtotal = 0,
    taxtotal = 0;

    // Loop products in the basket.
    $.each($("#pos tr.pr-container"), function(key, tr) {

      let
      index = $(tr).data("index"),
      product = $(`input[name="products[${index}][quantity]"]`),
      quantity = parseFloat(product.val()),
      price = parseFloat(product.attr("price")),
      tax = parseFloat(product.attr("tax"));

      tax = price * tax / 100;
      tax *= quantity;
      price *= quantity;

      taxtotal += tax;
      subtotal += price;
      total += price + tax;

      // Loop additional products in the basket.
      $.each($(`input[name^="products[${index}][additional_products]"]`), function(key, _additional_product) {

        let
        additional_product = $(_additional_product)
        additional_product_price = parseFloat(additional_product.attr("price")),
        additional_product_tax = parseFloat(additional_product.attr("tax"));

        additional_product_tax = additional_product_price * additional_product_tax / 100;
        additional_product_tax *= quantity;
        additional_product_price *= quantity;

        taxtotal += additional_product_tax;
        subtotal += additional_product_price;
        total += additional_product_price + additional_product_tax;

      });

    });

    $("#subtotal").html(subtotal.toFixed(2));
    $("#tax-total").html(taxtotal.toFixed(2));
    $("#total").html(total.toFixed(2));

    return;

    $.post(base_url+'pointofsales/calculate',  $('#pos').serialize(), function(json) {

      result = $.parseJSON(json);

      $("#subtotal").html(result.data.subtotal_formated);
      $("#tax-total").html(result.data.tax_formated);
      $("#total").html(result.data.location_currency + " " + result.data.total_formated);

      $("#pos-paid").html(result.data.paid_formated);
      $("#pos-rest").html(result.data.rest_formated);

      $("#pos-interim_paid").html(result.data.interim_payment_formated);
      // $("#pos-complete, #pos-complete-zipboox").prop('disabled', result.data.status);

      if (result.data.rest < 0) {
        $("#pos-change").html( result.data.rest_formated.replace(/^-/g, "") );
        $("#pos-complete, #pos-complete-zipboox").removeClass("btn-success, btn-danger").addClass("btn-danger");
      } else {
        $("#pos-change").html("0.00");
        $("#pos-complete, #pos-complete-zipboox").removeClass("btn-success, btn-danger").addClass("btn-success");
      }

    });
    
  },

  onClick_complete: function(){
    
    $("body").on("click", "#pos-complete, #pos-create-order",function(e) {

      e.preventDefault();

      var is_order = $(this).attr('id') == "pos-create-order" ? 1 : 0;

      if (is_order && !Pointofsales.table_exists()) return;

      _loading("add");

      $.post(base_url+'pointofsales/complete?action=' + (is_order ? 'order' : 'checkout'),  $('#pos').serialize(), function(result) {

        _loading("delete");

        if (!result.result) {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
          return;
        }

        Pleasure.handleToastrSettings('Başarılı', result.message, 'success');

        if (typeof result.data.new_sale_id !== "undefined") {
          $("input[name=sale_id]", "#pos").val(result.data.new_sale_id);
        }

        $.each(result.data.inserted_product_ids, function(key, val) {
          $(`input[name="products[${val.index}][sale_product_id]"]`, "#pos-list").val(val.sale_product_id);
        });

        if (result.data.status === "completed") {
          Pointofsales.show_invoice(result.data.sale_id, null);
        }
return;

        if (!result.data.is_order) {
          Pointofsales.show_invoice(result.data.id, null);
        } else {
          $("#pos-delete-order-container").removeClass("hidden");
        }

        if ($('#pos input[name=sale_id]').length === 0) {
          $('#pos').append('<input type="hidden" name="sale_id" value="' + result.data.id + '">');
        }

        $("#pos input[name^='cancelled_completed_products']").remove();
        $("#pos input[name='session_cancelcompletedproduct']").remove();
        // console.log("silindi", "kalan:" + $("#pos input[name^='cancelled_completed_products']").length);
      }, "json");
      
    });
  },

  on_click_delete_order: function () {
    
    $("body").on("click", "#pos-delete-order", function(e){

      if (!confirm('Sipariş iptal edilsin mi?')) {return;}

      var id = $(this).data("id"),
      location = $("#pos select[name=location_id]").val(),
      pos = $("#pos select[name=pos_id]").val();

      $.post(base_url + 'pointofsales/cancelOrder', {
        id:id,
        location:location,
        pos:pos
      }, function (json) {
        
        result = $.parseJSON(json);

        if (result.result) {

          setTimeout(function () {
            window.location.href = window.location;
          }, 500);
        } else {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }
      });
      e.preventDefault();
    });

  },

  show_invoice:function(id, _html) {

	$.post(base_url+'pointofsales/invoice/' + id,  {}, function(json) {

		result = $.parseJSON(json);

		modal = ' \
        <div id="pos-modal-invoice" class="modal fade" tabindex="-1" role="dialog"> \
         	<div class="modal-dialog"> \
	            <div class="modal-content"> \
	             	<div class="modal-body"> \
	             	<div class="keys"></div> \
	             	<div class="invoice"></div> \
	              	</div> \
	              	<div class="modal-footer"> \
	                	<button type="button" class="btn btn-default btn-sm" onclick="location.reload();"><i class="fa fa-times"></i> Kapat</button> \
	                	<button type="button" class="btn btn-primary btn-sm printInvoice" ><i class="fa fa-print"></i> Yazdır</button> \
	              	</div> \
	            </div> \
         	</div> \
        </div>';

        if (! $("#pos-modal-invoice").length) {
        	$("body").append(modal);
    	}

    	$("#pos-modal-invoice .modal-body .keys").html(_html);
    	$("#pos-modal-invoice .modal-body .invoice").html(result.data);

        // Invoice.init(id+1000);
        // $("#invoice_barcode").barcode(id+1000, "code39", {barWidth:3, barHeight:80});

      // $('#pos-modal-invoice .modal-body .invoice').print();
  		$("body").on("click", "#pos-modal-invoice .printInvoice",function(){
  			$('#pos-modal-invoice .modal-body .invoice').print();
  		});

      $("#pos-modal-invoice").modal({
        backdrop: false,
        keyboard: false,
        show: true,
      });

	});
  },

  table_exists: function() {

    if ($('#pos input[name=table_id]').length !== 0) {
      return true;
    }

    Pleasure.handleToastrSettings('Hata!', 'Masa Seçiniz.', 'error');
  },

  reset_pos: function () {

    // CLEAR POS-LIST
    $("#pos-list tbody").html("");

    // RESET DISCOUNT SECTION
    // $("#discount").html('<i id="discount-add" class="btn btn-sm btn-default fa fa-plus-square"></i> 0.00');

    // CLEAR VALUES
    $("#subtotal").html("0.00");
    $("#tax-total").html("0.00");
    $("#total").html("0.00");
    $("#pos-rest").html("0.00");
  },

  handle_note: function() {

    $('#pos-modal-note').on('shown.bs.modal', function () {

      if (!Pointofsales.table_exists()) return;

      $("#pos-modal-note").find("textarea").html($("#pos-note").val());

    });


    $("body").on("click", "#pos-modal-note-submit", function(){
      
      var modal = $("#pos-modal-note"),
      note = modal.find("#text_note").val();

      $.post(base_url+'pointofsales/updateTextNote', {
        note: note,
        sale_id: $("#pos").find("input[name=sale_id]").val()
      }, function() {
        $("#pos-note").val(note);
        modal.modal("toggle");
      });
      
    });
  },

  handlePoints: function(points) {

    var points = points;

    $("body").on("change", "select[name=location_id]", function(e){

      $location = $(this).val();
      pos = $("select[name=pos_id]");
      pos.html("").trigger("chosen:updated");

      if ($location) {
        $.each(points, function(index, el) {
          if ($location == el.location_id) {
            pos.append("<option value='"+el.id+"'>"+el.title+"</option>");
          }
        });
      }

      pos.trigger("chosen:updated");

    });
  },

  handlePointsAndLocation: function (cookie) {

    var cookie = cookie;

    setTimeout(function () {
      $("select[name=location_id]").val(cookie.location).trigger("change");
      $("select[name=pos_id]").val(cookie.pos);
      $(".pos-title i").html( $("select[name=location_id] option:selected").html() + "/" + $("select[name=pos_id] option:selected").html() );
    }, 500);
  },

  handleVES: function (html) {
    
    modal = '<div class="modal fade" id="modal-ves" data-backdrop="static" data-keyboard="false"></div>';

    if (!$("#modal-ves").length) {
      $("body").append(modal);
    }

    $("#modal-ves").html(html);
    $("#modal-ves").modal("toggle");
  },

  handleVESCheck: function () {
    
    $("body").on("click", "input[id^=checkProduct]", function(){

      var checkbox = $(this);

      $.post(base_url+'pointofsales/checkingVes', $.parseJSON($(this).val()), function(json) {

        if (json.result) {
          Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
        } else {
          if (json.data.uncheck !== 'undefined') {
            checkbox.prop('checked', 'true');
          }

          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
        }
        

      }, 'json');
    
    });
  },

  handleResetVES: function() {
    
    $("body").on("click", "#reset-ves", function(e){
      
      e.preventDefault();

      $('#pos').find("input[name=ves_form_checked_in]").remove();
      $('#pos-complete').click();

    });
  },

  handleCompletedOrders: function () {
    
    setInterval(function () {
      
      var location = $("#pos select[name=location_id]").val();

      if (!location) {
        return;
      }

      $.post(base_url+'pointofsales/getCompletedOrders', {
        location: location,
      }, function(json) {
        
        if (json) {
          $("#completed-orders-modal").removeClass("hidden");
          // SoundPlayer.play("bell_ring");
        } else {
          $("#completed-orders-modal").removeClass("hidden").addClass("hidden");
        }

        $("#modal-completed-orders tbody").html(json);

      });


    }, 3000);
  }, 

  on_click_do_seen_order: function () {
    
    $("body").on("click", "button.doseenorder", function(){
      
      var tr = $(this).closest("tr");


      $.post(base_url+'pointofsales/doSeenOrder/', {
        'id' : $(this).data('id')
      },function(json) {

        result = $.parseJSON(json);

        if (result.result) {
          tr.remove();
          Pleasure.handleToastrSettings('Başarılı', result.message, 'success');
        } else {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }

      });

    });
  },

  handle_move_table: function () {

    $('#modal-move-table').on('shown.bs.modal', function () {

      if (!Pointofsales.table_exists()) {
        $('#modal-move-table').modal("toggle");
        return;
      }

      $.post(base_url+'pointofsales/getTables?empty_tables=1', {
        location: $("select[name='location_id']").val(),
        current_table: $("#pos input[name=table_id]").val()
      }, function(data) {

        data = $.parseJSON(data);
        $("#modal-move-table .modal-body").html(data.data);
      });
    });

    this.on_click_move_table();
  },

  on_click_move_table:function () {
    $("body").on("click", ".move-table", function() {

      var data = $('#pos').serializeArray();
      data.push({
        "name" : "target_table_id",
        "value" : $(this).data("target")
      });

      $.post(base_url + "pointofsales/moveTable", data, function(json) {
        var result = $.parseJSON(json);

        if (result.result) {
          Pleasure.handleToastrSettings('Başarılı', result.message, 'success');
          setTimeout(function () {
            window.location.href = window.location;
          }, 500);
        } else {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }
      });

    }); 
  },

  handle_sound_record:function () {

    var audioInput, audioGain, audioRecorder,
    mediaStream, mediaInitialized, timer;

    $("#record").on("click", function() {

      // Stop
      if ($(this).hasClass('btn-red')) {

        $("#record").removeClass("btn-red").addClass("btn-blue").find("span").html("Kayıt");
        completeRecording();
      }
      // Start
      else {

        $(this).removeClass("btn-blue").addClass("btn-red").find("span").html("Durdur");
        startRecording();
      }

    });

    $("#moda-sound-record-close").on("click", function () {
      $("#modal-sound-recorder").modal("toggle");
      clearInterval(timer);
      $("#modal-sound-recorder .counter").html("00:00:00");
      $("#record").removeClass("btn-red btn-blue").addClass("btn-blue").find("span").html("Kayıt");
      audioRecorder.stop();
      audioRecorder.clear();
    });


    setupMedia();

    function supportsMedia() {
      return true;
    };

    function setupMedia() {
      if (supportsMedia()) {

        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext = new AudioContext();

        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        navigator.getUserMedia({
          audio: true
        },
        function (localMediaStream) {

            // setup audio recorder
            audioInput = audioContext.createMediaStreamSource(localMediaStream);

            audioGain = audioContext.createGain();
            audioGain.gain.value = 0;
            audioInput.connect(audioGain);
            audioGain.connect(audioContext.destination);

            audioRecorder = new Recorder(audioInput);
            mediaStream = localMediaStream;
        },
        function (error) {
          console.log(error);
          //alert(error.message);
        });
      }
    };

    function startRecording() {
      // begin recording audio
      audioRecorder.record();

      var s = 0, m = 0, h = 0;
      timer = setInterval(function () {
        
        if(s < 59) {
          s = s + 1;
        } else {

          s = 0; 

          if(m < 59) {
            m = m + 1;
          }
          else {
            m = 0;
            h = h + 1;
          }

        }

        $("#modal-sound-recorder .counter").html(
          (h < 10 ? "0" + h : h) + ":" + 
          (m < 10 ? "0" + m : m) + ":" + 
          (s < 10 ? "0" + s : s)
        );

      }, 1000);
      
    };

    function completeRecording() {

      clearInterval(timer);
      $("#modal-sound-recorder .counter").html("00:00:00");

      audioRecorder.stop();
      audioRecorder.exportWAV(function (audioBlob) {

        var location = $("#pos select[name=location_id]").val();
        var data = new FormData();
        data.append('audio', audioBlob);

        $.each($('#pos').serializeArray(), function(key, value) {
          data.append(value.name, value.value);
        });

        $.ajax({
          url :  base_url+'pointofsales/saveSound',
          type: 'POST',
          data: data,
          contentType: false,
          processData: false,
          success: function(json) {
            result = $.parseJSON(json);

            if (result.result) {
              Pleasure.handleToastrSettings('Başarılı', result.message, 'success');
            } else {
              Pleasure.handleToastrSettings('Hata!', result.message, 'error');
            }
          }
        });
      });
      audioRecorder.clear();
      
    };
  },



  handle_session_managment: function () {
    
    // LOG IN
    $("body").on("submit", "#form-session", function(event){

      event.preventDefault();

      var data = $("#form-session").serializeArray();

      $.post(base_url + 'pointofsales/loginSession', data, function(json) {
        
        result = $.parseJSON(json);

        if (result.result) {

          setTimeout(function () {
            window.location.href = window.location;
          }, 500);
        } else {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }

      });
    });

    // LOG OUT
    $("body").on("click", "#modal-session .unset_session", function(){
      var id = $(this).data("id");

      $.post(base_url + 'pointofsales/logoutSession/' + id, function() {
        setTimeout(function () {
            window.location.href = window.location;
          }, 500);
      });
    });

    // CHANGE SESSION
    $("body").on("click", "#modal-session .select_session", function(){

      var id = $(this).data("id");


      $.post(base_url + 'pointofsales/selectSession/' + id, function() {
        
        setTimeout(function () {
          window.location.href = window.location;
        }, 500);

      });
    });


  },




  on_click_print_adisyon: function () {
    
    $("body").on("click", "#print-adisyon", function(e){

      e.preventDefault();
      
      if (!Pointofsales.table_exists()) return;

      var rows = $("#pos-list tr.pr-container");
      var table = $("#adisyon-table");
      var tbody = $("tbody", table);
      var dt = new Date();

      tbody.html("");

      $.each(rows, function(index, element) {
        
        product = $("td", element).eq(1).html();
        quantity = $("td", element).eq(0).find("input[name^=products]").val();
        price = $("td", element).eq(2).html();
        amount = $("td", element).eq(3).html();

        tbody
        .append($('<tr>')
            .append($('<td>').text(product))
            .append($('<td>').text(quantity + "x").addClass("text-center"))
            .append($('<td>').text(price).addClass("text-right"))
            .append($('<td>').text(amount).addClass("text-right"))
        );

      });

      // Thead
      table.find(".adisyon_date").html(
        dt.getDate() +"/"+ (dt.getMonth()+1) +"/"+ dt.getFullYear()
        +" "+
        dt.getHours() +":"+ dt.getMinutes() +":"+ dt.getSeconds()
      );

      // Tbody
      tbody.html(tbody.html());

      // Tfoot
      var subtotal = $("#pos-summary #subtotal").html() || 0;
      var interim_payment = $("#pos-summary #pos-interim_paid").html();
      var tax = $("#pos-summary #tax-total").html() || 0;
      var total = $("#pos-summary #total").html() || 0;
      var currency = $("#pos input[name=location_currency]").val();


      // subtotal = subtotal.replace(/[^0-9\.]/g, "");
      // interim_payment = interim_payment.replace(/[^0-9\.]/g, "");
      // tax = tax.replace(/[^0-9\.]/g, "");
      // total = total.replace(/[^0-9\.]/g, "");

      // subtotal = parseFloat(subtotal).toFixed(2);
      interim_payment = numberUnFormat(interim_payment);
      // tax = parseFloat(tax).toFixed(2);
      total = numberUnFormat(total);

      table.find(".adisyon_subtotal").html(subtotal);
      table.find(".adisyon_interim_payment").html(interim_payment);
      table.find(".adisyon_tax").html(tax);
      table.find(".adisyon_total").html(numberFormat(total - interim_payment, 2) + currency);

      $("div", table).eq(0).print();
    });

  },


  onShown_interimPaymentsModal: function () {
    
    $('#modal-interim_payments').on('shown.bs.modal', function () {

      $("#interim_payments-total").html("0.00");

      if (!Pointofsales.table_exists()) {
        $('#modal-interim_payments').modal("toggle");
        return;
      }

      $("#modal-interim_payments .modal-body").html("<h1 class='text-center'><i class='fa fa-spinner'></i></h1>");

      $.post(base_url+'pointofsales/ajax/getInterimPaymentProducts', $("#pos").serializeArray(), function(json) {

        if (json.result) {
          modal_content = json.data.html;
        } else {
          modal_content = "<h3 class='text-center'>"+json.message+"</h3>";
        }

        $("#modal-interim_payments .modal-body").html(modal_content);

      }, "json");
    });

    this.onClick_interimPaymentUnpaidProducts();
    this.onClick_interimPaymentPayableProducts();
    this.onClick_interimPaymentSubmit();

    Pointofsales.calculate();
  },

  onClick_interimPaymentUnpaidProducts: function () {
    
    var unpaid_basket = "#modal-interim_payments .interim_payments-unpaid tbody";

    $("body").on("click", unpaid_basket + " tr", function(){
      
      var payable_basket = $("#modal-interim_payments .interim_payments-payable tbody"),
      unpaid_row = $(this),
      payable_clone = unpaid_row.clone(),
      data_id = unpaid_row.data("id"),
      quantity = unpaid_row.find("input[name^='sale_has_products']").val() | 0
      quantity = parseInt(quantity);

      if (quantity < 1) {
        return false;
      }

      unpaid_row.find("td.quantity").html(quantity - 1);
      unpaid_row.find("input[name^='sale_has_products']").val(quantity - 1);

      // Does exist same product?
      var payable_row = $("tr[data-id=" + data_id + "]", payable_basket);
      if (!payable_row.length) {

        payable_clone.find("td.quantity").html(1);
        payable_clone.find("input[name^='sale_has_products']").val(1);

        payable_basket.append(payable_clone);

      } else {

        val = payable_row.find("input[name^='sale_has_products']").val() | 0;
        val = parseInt(val);

        payable_row.find("td.quantity").html(val + 1);
        payable_row.find("input[name^='sale_has_products']").val(val + 1);
      }

      $("tr.interim_payments-info", payable_basket).removeClass("hidden");
      if ($("tr:not(tr.interim_payments-info)", payable_basket).length) {
        $("tr.interim_payments-info", payable_basket).addClass("hidden");
      }

      Pointofsales.calculate_interimPayment(numberUnFormat(unpaid_row.find("td.total").html()));

    });
  },

  onClick_interimPaymentPayableProducts: function () {
    
    var payable_basket = "#modal-interim_payments .interim_payments-payable tbody";

    $("body").on("click", payable_basket + " tr:not(tr.interim_payments-info)", function(){
      
      var unpaid_basket = $("#modal-interim_payments .interim_payments-unpaid tbody"),
      payable_row = $(this),
      data_id = payable_row.data("id"),
      quantity = payable_row.find("input[name^='sale_has_products']").val() | 0
      quantity = parseInt(quantity),
      unpaid_row = $("tr[data-id=" + data_id + "]", unpaid_basket);


      unpaid_val = unpaid_row.find("input[name^='sale_has_products']").val() | 0;
      unpaid_val = parseInt(unpaid_val);

      unpaid_row.find("td.quantity").html(unpaid_val + 1);
      unpaid_row.find("input[name^='sale_has_products']").val(unpaid_val + 1);

      if (quantity <= 1) {

        payable_row.remove();

      } else {

        payable_row.find("td.quantity").html(quantity - 1);
        payable_row.find("input[name^='sale_has_products']").val(quantity - 1);
      }

      $(payable_basket + " tr.interim_payments-info").removeClass("hidden");
      if ($(payable_basket + " tr:not(tr.interim_payments-info)").length) {
        $(payable_basket + " tr.interim_payments-info").addClass("hidden");
      }

      Pointofsales.calculate_interimPayment(-1 * numberUnFormat(payable_row.find("td.total").html()));

    });
  },

  calculate_interimPayment: function (value) {

    var total_label = $("#interim_payments-total"),
    total = total_label.html();
    total = numberUnFormat(total);

    total = total + value;
    tax = 0;//parseFloat($("#pos input[name=location_tax]").val()) / 100;
    total_label.html(numberFormat(total + total * tax, 2));
  },


  onClick_interimPaymentSubmit: function () {
    
    $("body").on("click", "#modal-interim_payments .interim_payments-submit button", function(){

      if (!Pointofsales.table_exists()) return;

      var form = $("#modal-interim_payments form"),
      form_data = form.serializeArray(),
      payment_type = $(this).data("payment");
      form_data.push({"name": "payment_type", "value": payment_type});
      form_data.push({"name": "sale_id", "value": $("#pos input[name=sale_id]").val()});

      $.post(base_url + 'pointofsales/ajax/processInterimPayment', form_data, function(json) {
        
        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pleasure.handleToastrSettings('Başarılı', json.message, 'success');

        $("#modal-interim_payments").modal("toggle");

        Pointofsales.calculate();

      }, "json");
    });

  },





  handle_OperatorSessions: function () {

    // Check Session
    var getOperatorSession = function () {
        
      $.post(base_url + 'pointofsales/ajax/getOperatorSession', function(json) {
        if (! json.result && ! $("#screen_saver_page").length) {
          $.get(base_url + 'pointofsales/ajax/getScreenSaver', function(json1) {
            $("body").append(json1.data.template);
          }, "json");
        }
      }, "json");
    };

    // Unset Session
    var unSetOperatorSession = function () {
      $("body").on("click", "#exit_operator_session", function(){
        $.post(base_url + 'pointofsales/ajax/unSetOperatorSession');
        getOperatorSession();
      });
    };

    getOperatorSession();
    setInterval(getOperatorSession, 1000);
    unSetOperatorSession();
    this.onClick_ScreenSaverKeys();

  },

  onClick_ScreenSaverKeys: function () {
    
    $("body").on("click", "#screen_saver .ss_keys button", function() {

      var button = $(this),
      value = button.data('value');

      if (value === "erase") {

        $("#screen_saver input[name=password]").val(function () {
          return $(this).val().slice(0, -1);
        });

      } else if (value === "submit") {

        // Set Operator Session
        $.post(base_url + 'pointofsales/ajax/setOperatorSession', $("#screen_saver form").serializeArray(), function(json) {
          if (json.result) {
            $("#screen_saver_page").remove();
          } else {
            Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          }
        }, "json");

      } else {

        $("#screen_saver input[name=password]").val(function () {
          return $(this).val() + value;
        });

      }

    });
  },




  handleBarCodeReader: function () {

    $(document).scannerDetection({
      timeBeforeScanTest: 500, // wait for the next character for upto 200ms
      avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
      // startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
      // endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
      // ignoreIfFocusOn: 'input', // turn off scanner detection if an input has focus
      // scanButtonKeyCode: 116, // the hardware scan button acts as key 116 (F5)
      // scanButtonLongPressThreshold: 5, // assume a long press if 5 or more events come in sequence
      // onScanButtonLongPressed: showKeyPad, // callback for long pressing the scan button
      onComplete: function(barcode, qty) {
        
        console.log("BarCode detected: " + barcode);

        $("#pos-products .product").each(function(index, val) {
          if (!!$(this).data("bar-code") && $(this).data("bar-code") === barcode) {
            $(this).trigger("click");
          }
        });

      },
      onError: function(string) {
        console.log('Error: ' + string);
      }
    });

  },

};



















var ReturnPOS = {

  init: function() {

    this.managerLogin();
    this.getSaleById();
    this.on_changeQuantity();
    this.complete();

  },

  managerLogin: function() {

    $("body").on("click", "#return-stp1", function(e) {

      form_data = $("#return-form").serializeArray();
      form_data.push({
        "name" : "location",
        "value" : $("select[name='location_id']").val() 
      });

      $.post(base_url+'pointofsales/managerAccount2Return', form_data, function(json) {

        result = $.parseJSON(json);

        if (result.result) {

          $("#return-form")
          .html(' \
            <div class="form-group"> \
              <b>#ID</b> \
              <input type="number" name="saleid" class="form-control" placeholder="Satış ID" autocomplete="off"> \
            </div> \
            <input type="hidden" name="session" value="' + result.data.session + '"> \
            <button type="button" class="btn btn-primary" id="return-stp2">Kaydet</button>'
          );

          $("#return-form").find("input[name=saleid]").focus().select();
          return true;
        }

        Pleasure.handleToastrSettings('Hata!', result.message, 'error');

      });
      
    });
  },

  getSaleById: function() {

    $("body").on("click", "#return-stp2", function(e) {

      form_data = $("#return-form").serializeArray();

      $.post(base_url+'pointofsales/getSaleById', form_data, function(json) {

        result = $.parseJSON(json);

        if (result.result) {

          $("#return-product-modal").modal("toggle");
          $("#pos").slideUp(function() {
            $("#return").slideDown();
          });
          $("#sale-id").html(result.data.sale.sale.id);

          $("#return").find("input[name=location]").val(result.data.sale.sale.location_id);
          $("#return").find("input[name=saleid]").val(result.data.sale.sale.id);
          $("#return").find("input[name=session]").val(result.data.data.session);

          $("#return").find("tbody").html(result.data.html);

          ReturnPOS.calculate();

        } else {

          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }

      });

    });

  },

  on_changeQuantity: function() {

    $("body").on("change", "select[name^=products]", function() {
      ReturnPOS.calculate();
    });

  },

  calculate: function() {

    $.post(base_url+'pointofsales/calculateReturn',  $('#return').serialize(), function(json) {

      result = $.parseJSON(json);

      $("#return-subtotal").html(result.data.location_currency + " " + result.data.subtotal_formated);
      $("#return-tax").html(result.data.location_currency + " " + result.data.tax_formated);
      $("#return-total").html(result.data.location_currency + " " + result.data.total_formated);

      $("#return-payable").html(result.data.location_currency + " " + result.data.payable_formated);
    });
  },

  complete: function() {

    $("body").on("click", "#return-complete", function(e){
      $.post(base_url+'pointofsales/completeReturn',  $('#return').serialize(), function(json) {
        result = $.parseJSON(json);

        if (result.result) {

          $("#return").find("input[name=session]").val("");

          Pleasure.handleToastrSettings('Başarılı', result.message, 'success');

          ReturnPOS.show_invoice(result.data.invoice);

        } else {

          Pleasure.handleToastrSettings('Hata!', result.message, 'error');

        }


      });

      e.preventDefault();
    });
    
  },

  show_invoice:function(_html) {

    modal = ' \
      <div id="pos-modal-return-invoice" class="modal fade" tabindex="-1" role="dialog"> \
        <div class="modal-dialog"> \
            <div class="modal-content"> \
              <div class="modal-body"> \
                <div class="invoice">' + _html + '</div> \
              </div> \
              <div class="modal-footer"> \
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Kapat</button> \
                <button type="button" class="btn btn-primary btn-sm printReturnInvoice" ><i class="fa fa-print"></i> Yazdır</button> \
              </div> \
            </div> \
        </div> \
      </div>';

      $("body").append(modal);


      $('#pos-modal-return-invoice .modal-body .invoice').print();

      $("body").on("click", "#pos-modal-return-invoice .printReturnInvoice",function(){
        $('#pos-modal-return-invoice .modal-body .invoice').print();
      });

      $("#pos-modal-return-invoice").modal({
        backdrop: false,
        keyboard: false,
        show: true,
      });
  }
};


var Orders = {

  init: function(location) {
    this.getOrders(location);
    this.on_click_complete_order();
    this.on_click_sound_modal();
    this.on_click_play_pause();
    this.on_click_text_note_button();
  },

  getOrders: function (location) {
      var _latest_tab = null;
      var _latest_sale_amount = 0;

      setInterval(function () {

        var tab = $("#order-tabs").find("li.active > a").attr("href").replace("#", "");

        $.get(base_url+'pointofsales/orders/' + location + window.location.search.replace(/\//g, "") + "&tab=" + tab, function(data) {

          data = data ? $.parseJSON(data) : null;

          if (!data) {
            $("#orders tbody").html("");
            return;
          }

          $("#orders tbody").html(data.data.html);

          if (_latest_tab != data.data.tab || _latest_sale_amount != data.data.last_total_amount) {
            _latest_tab = data.data.tab;
            _latest_sale_amount = data.data.last_total_amount;
            

            $("#new_order_sound").remove();

            if (data.data.tab == "today-active") {
              $("body").append('<audio controls autoplay loop class="hidden" id="new_order_sound"> \
                <source src="' + base_url + "assets/globals/plugins/ionsound/sounds/bell_ring.mp3" + '" type="audio/mpeg"> \
              </audio>').prop("volume", 1);
            }

            $("#new_order_sound").prop("volume", 1);
          }
          

        });

      }, 2000);

      // remove sound
      $("body").on("click", function(){
        $("#new_order_sound").remove();
      });
  }, 
  on_click_complete_order: function () {
    
    $("body").on("click", "button.docompleteorder", function() {
      
      var tr = $(this).closest("tr");

      $.post(base_url+'pointofsales/completeOrder/', {
        'id' : $(this).data('id'),
        'quantity' : $(this).data('quantity')
      },function(json) {

        result = $.parseJSON(json);

        if (result.result) {
          tr.fadeOut(500, function () { tr.remove(); });
          Pleasure.handleToastrSettings('Başarılı', result.message, 'success');
        } else {
          Pleasure.handleToastrSettings('Hata!', result.message, 'error');
        }

      });

    });

  },



  on_click_sound_modal: function () {

    $("body").on("click", ".modal-sounds", function(){

      var id = $(this).data('id');

      $.post(base_url+'pointofsales/getSounds', {
        id: id
      }, function(data) {

        data = $.parseJSON(data);
        $("#modal-sounds").modal("toggle");
        $("#modal-sounds .modal-body tbody").html(data.data.html);
      });
    });

  },

  on_click_play_pause:function() {

    var modal = $("#modal-sounds"), 
    player = modal.find('audio');

    $("body").on("click", ".play-sound-note", function() {

      var button = $(this),
      src        = button.data("sound-src"),
      icon       = button.find("i"),
      id         = button.data("id");


      player.attr("src", src);
      player.get(0).load();

      if (button.hasClass("pause")) {

        button.removeClass("btn-info pause").addClass("btn-primary");
        icon.removeClass("ion-pause").addClass("ion-play");
        player.get(0).pause();
      } else {

        $(".play-sound-note").each(function (key, value) {
          $(value).removeClass("btn-info pause").addClass("btn-primary");
          $(value).find("i").removeClass("ion-pause").addClass("ion-play");
        });

        button.removeClass("btn-primary").addClass("btn-info pause");
        icon.removeClass("ion-play").addClass("ion-pause");

        player.get(0).play();

        // Do seen.
        $.get(base_url + 'pointofsales/doSeenSound/' + id);
        button.closest("tr").find("td:eq( 1 )").html("");
      }

    });

    $(modal).on('hidden.bs.modal', function () {
      player.get(0).pause();
    });
  },


  on_click_text_note_button: function () {
     
     $('body').on('click', ".modal-show_note_button", function () {
      $("#modal-show_note .modal-body").html($(this).data("note"));
      $("#modal-show_note").modal("toggle");
    }); 
  }
};







var PackageService = {

  config: {
    servicePointId: null,
  },

  modal: "#modal-package_service",
  form: "#form-package_service",

  init: function (config) {

    if (typeof config !== "undefined")
      $.extend(PackageService.config, config);
    
    PackageService.customerSearch();

    $("#process-user", PackageService.form).on("click", PackageService.processUser);
    $("#select-user", PackageService.form).on("click", PackageService.selectUser);
    $("#cancel-user", PackageService.form).on("click", PackageService.cancelUser);
  },


  customerSearch: function () {

    $("select[name=user]", PackageService.form)
    .select2({
      placeholder: "Müşteri Arama",
      language: "tr",
      minimumInputLength: 1,
      ajax: {
        url: base_url + "customers/ajax/search",
        delay: 500,
        type: "POST",
        dataType: 'json',
        data: function (params) { return { q: params.term }; },
        processResults: function (data, params) { return { results: data.items }; },
        cache: true
      },
      escapeMarkup: function (markup) { return markup; },
      templateResult: PackageService.formatRepo,
      templateSelection: PackageService.formatRepoSelection,
    })
    .on("select2:select", function (obj) {
      // fill the form inputs with selected customer
      PackageService.fillForms(obj);
    });
  },

  processUser: function () {

    var formData = $(PackageService.form).serializeArray();

    $.post(base_url + "customers/ajax/process", formData, function(json) {

      if (!json.result) {
        Pleasure.handleToastrSettings('Hata!', json.message, 'error');
        return;
      }

      if (json.data.process === "insert") {
        // set customer id in the form.
        $("input[name=id]", PackageService.form).val(json.data.id);
        // assign the customer into the POS form.     
        PackageService.selectUser();
      }

      Pleasure.handleToastrSettings('Başarılı', json.message, 'success');

    }, "json");

  },


  selectUser: function () {

    var customer_id = $("input[name=id]", PackageService.form).val();

    if (parseInt(customer_id) === 0) {
      Pleasure.handleToastrSettings('Hata!', "Öncelikle hesabı oluşturmalısınız.", 'error');
      return;
    }
    PackageService.goToDesk(PackageService.config.servicePointId + "?customer_id=" + customer_id);
    $("#pos input[name=customer_id]").val(customer_id);

    $("#cancel-user", PackageService.form).removeClass("hidden");
    PackageService.closeModal();
  },


  cancelUser: function () {
    if (parseInt($("input[name=id]", PackageService.form).val())) {
      Pleasure.handleToastrSettings('Başarılı', "Müşteri seçimi iptal edildi!", 'warning');
    }
    PackageService.goToDesk("");
    $(PackageService.form)[0].reset();
    $("select[name=user]", PackageService.form).val(null).trigger("change");
    $("#process-user", PackageService.form).html("Oluştur");
    $("#cancel-user", PackageService.form).addClass("hidden");
  },


  closeModal: function () {
    $(PackageService.modal).modal("toggle");
  },

  fillForms: function (obj) {
    
    var customer = obj.params.data;

    $("input[name=id]", PackageService.form).val(customer.id);
    $("input[name=name]", PackageService.form).val(customer.name);
    $("input[name=phone]", PackageService.form).val(customer.phone);
    $("textarea[name=address]", PackageService.form).val(customer.address);

    $("#process-user", PackageService.form).html("Güncelle");
    $("#cancel-user", PackageService.form).removeClass("hidden");
  },

  formatRepo: function (data) {
    if (data.loading)
      return data.text;
    return "<div>" + data.name + " <span class='pull-right'>" + data.phone + "</span></div> <div>" + data.address + "</div>";
  },

  formatRepoSelection: function (data) {
    return data.name || data.text;
  },

  goToDesk: function (point_id) {
    location.href = base_url + "pointofsales/index/" + point_id;
  },
};