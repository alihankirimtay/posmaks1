var Pointofsales = {

  settings: {
    location: {},
    pos_id: null,
    floor_id: null,
    zone_id: null,
    point_id: null, // -> TABLE
    point_title: null,
    is_mobile: false,
    payment_types: {},
    cash_types: {},
    sale: {
      amount: 0,
      subtotal: 0,
      gift_amount: 0,
      interim_payment_amount: 0,
      discount_amount: 0,
      discount_type: null,
    },
    operator_session: {
      user_id: null,
      user_name: null,
      user_role: null,
      permissions: null,
    },
    package_service: false,
    yemeksepeti_status:false,
  },
  variables: {
    activeFloorId: null,
    activeZoneId: null,
  },

  previousTab: null,

  systemProducts: {},
  basketProducts: {},

  posForm: "#pos",

  init: function(settings) {

    if (typeof settings !== "undefined") {
      $.extend(Pointofsales.settings, settings);
      
    }

    Pointofsales.handleKeyPad();
    Pointofsales.BarCodeScanner.init();
    Pointofsales.Pos.init();
    Pointofsales.Return.init();
    Pointofsales.Tables.init();
    Pointofsales.MoveTable.init();
    Pointofsales.InterimPayments.init();
    Pointofsales.Invoice.init();
    Pointofsales.ProductNotes.init();
    Pointofsales.Discount.init();
    Pointofsales.OperatorSession.init();
    Pointofsales.PackageServiceCustomer.init();
    Pointofsales.CashClosing.init();
    Pointofsales.OrderTrack.init();
    Pointofsales.PackageServiceOrders.init();
    Pointofsales.JSDesktopApp.init();

    if (Pointofsales.settings.yemeksepeti_status) {
      Pointofsales.YemekSepeti.init();
    }

    if (Pointofsales.JSDesktopApp.status) {
      Pointofsales.JSDesktopApp.setUpProductionPages(Pointofsales.settings.location.id);
    }

    let
    point_id = Pointofsales.Helpers.$_HASH("point"),
    phone = Pointofsales.Helpers.$_HASH("phone");
    point_id = Number(point_id);

    if (!!point_id) {
      Pointofsales.setPointId(point_id);
      Pointofsales.Pos.loadTab();
    }
    // Load package service by phone number (Caller ID)
    else if (!!phone) {
      Pointofsales.PackageServiceCustomer.setUrlPhoneNumber(phone);
      Pointofsales.Tables.openPackageServiceTable(phone);
    } else {
      Pointofsales.Tables.loadTab();
    }

    // Tab Events
    $("body").on("click", ".openTab", Pointofsales.onClickOpenTab);
    $("body").on("click", ".openPreviousTab", Pointofsales.onClickOpenPreviousTab);
    
  },

  setPointId: function (id) {

    Pointofsales.settings.point_id = parseInt(id);
  },

  getPointId: function () {

    return Pointofsales.settings.point_id;
  },

  onClickOpenTab: function (event) {

    event.preventDefault();

    let tab = $(this).data("tab");

    switch (tab) {
      case "pos": Pointofsales.Pos.loadTab();
        break;

      case "tables": Pointofsales.Tables.loadTab();
        break;

      case "movetable": Pointofsales.MoveTable.loadTab();
        break;

      case "interimpayments": Pointofsales.InterimPayments.loadTab();
        break;

      case "cashclosing": Pointofsales.CashClosing.loadTab();
        break;

      case "ordertrack": Pointofsales.OrderTrack.loadTab();
        break;

       case "packageserviceorders": Pointofsales.PackageServiceOrders.loadTab();
        break; 
    }
  },

  onClickOpenPreviousTab: function (event) {
    event.preventDefault();

    let tab = Pointofsales.Helpers.getPreviousTab();

    if (!!tab) {
      $(`a[href='#${tab}']`).trigger("click");
    } else {
      Pointofsales.Tables.loadTab();
    }
  },

  handleKeyPad: function () {
    Pointofsales.Helpers.keypad($(".keypad"));
  }
};


Pointofsales.Pos = {

  container: "#tab-pos",

  init: function () {
    Pointofsales.Pos.loadCategoriedProducts();

    Pointofsales.OperatorSession.Helpers.displayOperatorName(Pointofsales.settings.operator_session.user_name);

    if (Pointofsales.settings.operator_session.permissions) {
      Pointofsales.OperatorSession.Helpers.displayButtonsByPermissions();
    }

    Pointofsales.Pos.Helpers.displayPackageServiceTrackButton();

    Pointofsales.Pos.Events.init();
  },
  
  loadTab: function () {
    $("a[href='#tab-pos']").trigger("click");
    Pointofsales.Pos.loadTable();
    Pointofsales.Pos.setPoroductBasketScroolSize();
    Pointofsales.Pos.setPoroductsScroolSize();
  },

  loadTable: function () {

    Pointofsales.Helpers.addLoader(Pointofsales.Pos.container);
     
    Pointofsales.Pos.emptyTable();

    $("input[name='point_id']", Pointofsales.posForm).val(Pointofsales.getPointId());

    Pointofsales.Helpers.postForm("#pos", "getTableWithSale", function (json) {

      Pointofsales.Helpers.removeLoader(Pointofsales.Pos.container);

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        Pointofsales.setPointId(0);
        Pointofsales.Tables.loadTab();
        return;
      }

      Pointofsales.setPointId(json.data.point.id);
      Pointofsales.settings.point_title = json.data.point.title;
      Pointofsales.settings.sale_id = json.data.point.sale_id;
      Pointofsales.settings.floor_id = json.data.point.floor_id;
      Pointofsales.settings.zone_id = json.data.point.zone_id;
      if (!!json.data.point.sale_id) {
        Pointofsales.settings.sale.amount = parseFloat(json.data.sale.amount);
        Pointofsales.settings.sale.subtotal = parseFloat(json.data.sale.subtotal);
        Pointofsales.settings.sale.gift_amount = parseFloat(json.data.sale.gift_amount);
        Pointofsales.settings.sale.interim_payment_amount = parseFloat(json.data.sale.interim_payment_amount);
        Pointofsales.settings.sale.discount_amount = parseFloat(json.data.sale.discount_amount);
        Pointofsales.settings.sale.discount_type = json.data.sale.discount_type;
        $("input[name='customer_id']", Pointofsales.posForm).val(json.data.sale.customer_id);
        if (json.data.point.service_type === "2") {
          Pointofsales.Pos.Helpers.displayCustomerName(json.data.sale.customer);
        }
      }

      $("input[name='sale_id']", Pointofsales.posForm).val(Pointofsales.settings.sale_id);
      
      Pointofsales.Pos.Helpers.displayTableName(Pointofsales.settings.point_title);

      // Set visibility of PackageServiceCustomer button
      if (json.data.point.service_type === "2") {
        Pointofsales.PackageServiceCustomer.Helpers.showButton(true);
      } else {
        Pointofsales.PackageServiceCustomer.Helpers.showButton(false);
      }

      if (!!json.data.sale) {

        Pointofsales.basketProducts = json.data.sale.products;

        $.each(json.data.sale.products, function(index, product) {
          product.index = product.id;
          Pointofsales.Pos.Templates.basketProductRow(product, function (row) {
            $("#pos-basket", Pointofsales.posForm).append(row);
          });
        });
      }

      Pointofsales.Pos.calculate();
    });
  },

  emptyTable: function () {

    // Variables
    Pointofsales.basketProducts = {};
    Pointofsales.settings.sale.amount = 0;
    Pointofsales.settings.sale.subtotal = 0;
    Pointofsales.settings.sale.gift_amount = 0;
    Pointofsales.settings.sale.interim_payment_amount = 0;
    Pointofsales.settings.sale.discount_amount = 0;
    Pointofsales.settings.sale.discount_type = null;

    // Inputs
    $("input[name='sale_id']", Pointofsales.posForm).val("");
    $("input[name='point_id']", Pointofsales.posForm).val("");
    $("input[name='customer_id']", Pointofsales.posForm).val("");
    $("input[name='discount_key']", Pointofsales.posForm).val("");

    // Payment Inputs
    $("#pos-summary [id=payments-in]").html("");

    // Html Contents
    $("#pos-basket", Pointofsales.posForm).html("");
    $("#subtotal", Pointofsales.posForm).html("0.00");
    $("#discount-total", Pointofsales.posForm).html("0.00");
    $("#tax-total", Pointofsales.posForm).html("0.00");
    $("#total", Pointofsales.posForm).html("0.00");
    $("#pos-interim_paid", Pointofsales.posForm).html("0.00");
    $("#pos-paid", Pointofsales.posForm).html("0.00");
    $("#pos-rest", Pointofsales.posForm).html("0.00");
    $("#pos-change", Pointofsales.posForm).html("0.00");

    // Elements
    $("#pos-delete-order", Pointofsales.posForm).addClass("hidden");
    $("#pos-customer", "#pos-details").addClass("hidden");
    $("#pos-customer-tel", "#pos-details").addClass("hidden");
  },

  calculate: function () {
    let
    total = 0,
    subtotal = 0,
    taxtotal = 0,
    paidtotal = 0,
    intermpaymetntotal = Pointofsales.settings.sale.interim_payment_amount,
    discountamount = Pointofsales.settings.sale.discount_amount,
    discounttype = Pointofsales.settings.sale.discount_type,
    rest = 0,
    change = 0;

    // Loop products in the basket.
    $.each($("#pos tr.pr-container"), function(key, tr) {

      let
      index = $(tr).data("index"),
      product = $(`input[name="products[${index}][quantity]"]`),
      quantity = parseInt(product.val()),
      price = parseFloat(product.attr("price")),
      tax = parseFloat(product.attr("tax")),
      is_gift = (product.attr("is_gift") === "1");

      tax = Pointofsales.Helpers.calcTax(price, tax);

      tax = Pointofsales.Helpers.round(tax, 2);
      tax *= quantity;
      price = Pointofsales.Helpers.round(price, 2);
      price *= quantity;

      if (!is_gift) {
        taxtotal += tax;
        subtotal += price;
        total += price + tax;
      }

      // Loop additional products in the basket.
      $.each($(`input[name^="products[${index}][additional_products]"]`), function(key, _additional_product) {

        let
        additional_product = $(_additional_product)
        additional_product_price = parseFloat(additional_product.attr("price")),
        additional_product_tax = parseFloat(additional_product.attr("tax"));

        additional_product_tax = additional_product_price * additional_product_tax / 100;
        additional_product_tax = Pointofsales.Helpers.round(additional_product_tax, 3);
        additional_product_tax *= quantity;
        additional_product_price *= quantity;

        if (!is_gift) {
          taxtotal += additional_product_tax;
          subtotal += additional_product_price;
          total += additional_product_price + additional_product_tax;
        }

      });
    });

    // Calculate Paids in cash, credit, voucher, ticket, sodexo
    $.each(Pointofsales.settings.payment_types, function(index, payment_type) {
      $.each($(`input[name*="payments[${payment_type.alias}]"]`, Pointofsales.posForm), function(index, input) {
          let paid = $(input).val().replace(/[^0-9\.]/g, "");
          paid = Pointofsales.Helpers.round(paid, 2);
          paidtotal += parseFloat(paid);
        }
      );
    });

    if (discounttype === "amount") {
      total -= discountamount;
    } else if (discounttype === "percent") {
      total -= Pointofsales.Helpers.round(total * discountamount / 100, 2);
    }

    rest = total - (intermpaymetntotal + paidtotal);
    change = (intermpaymetntotal + paidtotal) - total;
    if (change < 0) {
      change = 0;
    }

    // subtotal = Pointofsales.Helpers.round(subtotal, 2);
    // taxtotal = Pointofsales.Helpers.round(taxtotal, 2);
    // total = Pointofsales.Helpers.round(total, 2);
    // intermpaymetntotal = Pointofsales.Helpers.round(intermpaymetntotal, 2);
    // paidtotal = Pointofsales.Helpers.round(paidtotal, 2);
    // rest = Pointofsales.Helpers.round(rest, 3);
    // change = Pointofsales.Helpers.round(change, 2);

    $("#subtotal").html(subtotal.toFixed(2));
    $("#tax-total").html(taxtotal.toFixed(2));
    $("#total").html(total.toFixed(2) +" "+ Pointofsales.settings.location.currency);
    $("#pos-interim_paid").html(intermpaymetntotal.toFixed(2));
    $("#pos-paid").html(paidtotal.toFixed(2));
    $("#pos-rest").html(rest.toFixed(2));
    $("#pos-change").html(change.toFixed(2));
    $("#discount-total", Pointofsales.Pos.container).html(Pointofsales.Discount.displayDiscount(discountamount, discounttype));
  },

  loadCategoriedProducts: function () {

    Pointofsales.Helpers.postForm(null, "getCategoriesWithProducts", function (json) {

      if (!!!json.data.categories) {
        $("#pos-products").html(Pointofsales.Templates.noContentFound("Ürün Bulunamadı."));
        return;
      }

      let 
      tabs = "",
      tabs_contents = "";

      categories = $.map(json.data.categories, (value, key) => { 
        value.sort = Number(value.sort);
        if (value.sort === 0) {
          value.sort = 9999999;
        }
        return [value]; 
      });

      categories.sort((a,b) => {
        if (a.sort === b.sort) {
            if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
            if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
        } else {
            if (a.sort > b.sort) return 1;
            if (a.sort < b.sort) return -1;
        }

        return 0;
      });

      $.each(categories, function(index, category) {
        tabs += `
          <li class="p-b-1">
            <a class="btn btn-grey btn-sm" data-toggle="pill" href="#tab-products-${category.id}">${category.title}</a>
          </li>
        `;

        tabs_contents += `<div id="tab-products-${category.id}" class="tab-pane" style="overflow-y: auto;">
                            <div class="panel-body p-a-0 p-x-1">`;

        products = $.map(category.products, (value, key) => { 
          value.sort = Number(value.sort);
          if (value.sort === 0) {
            value.sort = 9999999;
          }
          return [value]; 
        });

        products.sort((a,b) => {
          if (a.sort === b.sort) {
              if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
              if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
          } else {
              if (a.sort > b.sort) return 1;
              if (a.sort < b.sort) return -1;
          }

          return 0;
        });                   


        $.each(products, function(product_id, product) {

          product.price = Pointofsales.Helpers.addTax(product.price, product.tax);

          let background = "background-color:white;";
          if (product.image !== null) {
            background = `background-image: url(${base_url+product.image}); background-size: cover; background-position: 50% 50%;`;
          } else if (product.color !== null) {
            background = `background-color:${product.color};`;
          }

          tabs_contents += `
            <div class="col-sm-6 col-md-3">
                <div class="product thumbnail btn btn-ripple m-b-0" data-id="${product.id}" data-bar-code="${product.bar_code}" style="height: 150px;${background}">
                    <div class="caption caption-footer">
                        <b class="small">${product.title}</b>
                        <p class="small" style="font-size: 1em;">${product.price.toFixed(2)} ${Pointofsales.settings.location.currency}</p>
                    </div>
                </div>
            </div>
          `;
        });
        tabs_contents += `  </div>
                          </div>`;
      });

      tabs = `<ul class="nav nav-pills">${tabs}</ul>`;
      tabs_contents = `<div class="tab-content p-a-0">${tabs_contents}</div>`;

      $("#pos-products").html("").html(tabs + tabs_contents);
      $("ul li:first a", "#pos-products").trigger("click");
      
      
    });
  },

  setPoroductsScroolSize: function () {

    let 
    tabs = $("#pos-products > .nav"),
    y = tabs.offset(),
    h = tabs.height();

    if (!!!y) {
      setTimeout(Pointofsales.Pos.setPoroductsScroolSize, 300);
      return;
    }
    let height = $(window).height() - y.top - h;

    $(".tab-content div[id^='tab-products-']").css("max-height", height + "px");
    
  },

  setPoroductBasketScroolSize: function () {
    let 
    height = $("#pos-summary").height(),
    heightHeader = $(".page-header").height(),
    winHeight = $(window).height() - height -heightHeader -3;

    
    $("#pos-list").css("min-height", winHeight + "px");
  },

  Events: {
    init: function () {
      // Product Events
      $("body").on("click", "#pos-products .product", Pointofsales.Pos.Events.onClickProduct);
      $("body").on("change", "#modal-product_content #portions", Pointofsales.Pos.Events.onChangeProductPortion);
      $("body").on("click", "#modal-product_content .process_product", Pointofsales.Pos.Events.onClickProcessProduct);
      $("body").on("click", "#modal-product_content .plus", Pointofsales.Pos.Events.onClickPlus);
      $("body").on("click", "#modal-product_content .minus", Pointofsales.Pos.Events.onClickMinus);
      $("body").on("click", "#pos-list .edit", Pointofsales.Pos.Events.onClickEditProduct);
      $("body").on("click", "#pos-list .remove", Pointofsales.Pos.Events.onClickRemoveProduct);
      $("body").on("click", "#modal-case #cancelSave", Pointofsales.Pos.Events.onClickCancelSale);
      $("body").on("click", "#modal-order-checkout #print-invoice", Pointofsales.Pos.Events.onClickModalPrintInvoice);
      $("body").on("click", "#modal-order-checkout #new-order", Pointofsales.Pos.Events.onClickModalNewOrder);

      // Payment Events
      Pointofsales.settings.payment_types.map(function (row) {
        $("body").on("click", `#add-${row.alias}`, Pointofsales.Pos.Events.onClickAddPayment);
      });
      //Cari Events
      $("body").on("click", '#payment-cari', Pointofsales.Pos.Events.onClickPaymentCari);
      $("body").on("click", '#addCari', Pointofsales.Pos.Events.onClickAddCari);


      $("body").on("change input", "#pos input[name^='payments']", Pointofsales.Pos.Events.onChangePaymentInput);
      $("body").on("click", ".remove-payment", Pointofsales.Pos.Events.onClickRemovePayment);

      // Order/Check-Out Events
      $("body").on("click", "#pos-create-order, #pos-complete", Pointofsales.Pos.Events.onClickComplete);

      $("body").on("click", "#change_pos", Pointofsales.Setup.Events.onClickRemovePosSetup);
    },

    productPortions: {},
    productEditingIndex: 0,
    interimPaidCount: 0,
    completedCount: 0,
    onClickProduct: function() {

      let
      product_id = $(this).data("id"),
      modal = $("#modal-product_content"),
      query_string = `product_id=${product_id}`;

      Pointofsales.Pos.Events.productPortions = {};
      Pointofsales.Pos.Events.productEditingIndex = 0;
      Pointofsales.Pos.Events.interimPaidCount = 0;
      Pointofsales.Pos.Events.completedCount = 0;
      $(".modal-title", modal).html("");
      $('input[name="quantity"]', modal).val(1);
      $("#portions", modal).html("");
      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");
      $("#product_notes", modal).html("");
      modal.modal("toggle");
      $(".process_product", modal).html("Ekle");

      Pointofsales.Helpers.addLoader($(".modal-content", modal));

      Pointofsales.Helpers.postForm("#pos", `findProductPortions?${query_string}`, function(json) {

        Pointofsales.Helpers.removeLoader($(".modal-content", modal));

        if (!json.result) return;

        let
        products = json.data.products,
        lengths = {
          product: 0,
          stock: 0,
          additional_product: 0,
        };

        Pointofsales.Pos.Events.productPortions = products;

        $.each(products, function(key, product) {
          $("#portions", modal).append($("<option>").val(product.id).html(product.title));

          lengths.product++;

          $.each(product.stocks, () => {lengths.stock++});

          $.each(product.additional_products, () => {lengths.additional_product++});
        });

        $("#portions", modal).trigger("change");

        if (lengths.product === 1 && lengths.stock === 0 && lengths.additional_product === 0) {
          $(".process_product", modal).trigger("click");
        } else {
          
        }

      });
    },

    onChangeProductPortion: function () {

      let
      modal = $("#modal-product_content"),
      product_id = $(this).val(),
      product = Pointofsales.Pos.Events.productPortions[product_id],
      disable_items = "";

      $(".modal-title", modal).html(product.title);

      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");
      $("#quick_notes", modal).html("");
      $('#package_products', modal).html("");

      if (Pointofsales.Pos.Events.interimPaidCount > 0) {
        disable_items += "disabled";
      }


      if (product.products) {
        
        var package_products = [];

        package_products = $.map( product.products, function( item, i ) {
          return item;
        });

        let length = package_products.length,
            comma = ",";

        $.each(package_products, function(index, val) {
          if (index === length -1) {
            comma = "";
          }
          $("#package_products", modal).append(
          `<span> ${val.title} ${comma} </span>`
          );  
        });

      }

      $.each(product.stocks, function(index, val) {
        $("#stocks", modal).append(
          `<label class="reverse-checbox ${disable_items}">
            <input type="checkbox" name="stocks[${val.id}]" value="${val.id}" ${disable_items}>
            <span>${val.title}</span>
          </label>`
        );
      });

      $.each(product.additional_products, function(index, val) {

        add_price = Pointofsales.Helpers.addTax(val.price, val.tax);

        $("#additional_products", modal).append(
          `<div class="checkbox ${disable_items}">
            <label class="checkboxer checkboxer-deep-blue form-inline">
              <input id="inputFor${val.id}" type="checkbox" name="additional_products[${val.id}]" value="${val.id}" ${disable_items}>
              <label for="inputFor${val.id}">${val.title} <small>(${add_price} ${Pointofsales.settings.location.currency})</small></label>
            </label>
          </div>`
        );
      });

      $.each(product.quick_notes, function(index, val) {

        $("#quick_notes", modal).append(
          `<button type="button" class="btn btn-primary addtoquickNotes" style="margin-left: 10px;margin-top: 10px;" value="${val.title}">${val.title}</button>`
        );
      });

    },

    onClickProcessProduct: function () {

      let
      modal = $("#modal-product_content"),
      form = $("form", modal),
      product_id = parseInt($("#portions", form).val()),
      quantity = parseInt($("input[name=quantity]", form).val()),
      index = -new Date(),
      product = Pointofsales.Pos.Events.productPortions[product_id],
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, tax),
      row = null,
      items = "",
      note_items = "",
      selected_stocks = $('input[name*=stocks]:checked', form),
      selected_additional_products = $('input[name*=additional_products]:checked', form),
      product_notes = $('input[name*="[sound_notes]"], input[name*="[text_notes]"]', form),
      sameProductExists = false,
      tmp_stocks = {},
      tmp_additional_products = {};

      price = Pointofsales.Helpers.round(price, 2);
      total = price * quantity,

      product.product_id = product.id;
      product.id = "";
      product.quantity = quantity;

      if (quantity < Pointofsales.Pos.Events.interimPaidCount) {
        Pleasure.handleToastrSettings("Hata!", Pointofsales.Pos.Events.interimPaidCount + " adet ara ödemesi yapılmış ürün iptal edilemez.", "error");
        return;
      }
      if (quantity < Pointofsales.Pos.Events.completedCount) {
        if (!Pointofsales.Helpers.hasPermission("pointofsales.forceproductdecrement")) {
          Pleasure.handleToastrSettings("Hata!", Pointofsales.Pos.Events.completedCount + " adet hazırlanmış ürün iptal edilemez.", "error");
          return;
        }
      }

      // product is editing 
      if (Pointofsales.Pos.Events.productEditingIndex !== 0) {

        product.index = Pointofsales.Pos.Events.productEditingIndex;

      }
      // product is creating 
      else {

        $.each($(`#pos-basket input[name*="[product_id]"][value="${product_id}"]`), function(key, basket_product) {

          if (sameProductExists !== false)
            return;

          Pointofsales.Pos.Helpers.hasSameProductInBasket($(basket_product).closest("tr"), selected_stocks, selected_additional_products, function (product_in_basket) {
            sameProductExists = product_in_basket;
          });
        });

        // if same product exists in the basket, then use the its data-index to update..
        if (sameProductExists !== false) {
          product.index = sameProductExists.data("index");
        } else {
          product.index = index;
        }
      }

      if (product.type === "product") {
        $.each(selected_stocks, function(key, selected_stock) {
          let 
          stock_id = parseInt($(selected_stock).val()),
          stock = product.stocks[stock_id];
          items += `<input type="hidden" name="products[${product.index}][stocks][${stock_id}]" value="${stock_id}">`;

          stock.stock_id = stock_id;
          tmp_stocks[stock_id] = stock;
        });
        product.stocks = tmp_stocks;
      }

      $.each(selected_additional_products, function(key, selected_additional_product) {
        let 
        additional_product_id = parseInt($(selected_additional_product).val()),
        additional_product = product.additional_products[additional_product_id],
        additional_product_price = additional_product.price,
        additional_product_tax = additional_product.tax;
        items += `<input type="hidden" name="products[${product.index}][additional_products][${additional_product_id}]" value="${additional_product_id}" price="${additional_product_price}" tax="${additional_product_tax}">`;
        
        additional_product.product_id = additional_product.id;
        tmp_additional_products[additional_product_id] = additional_product;
      });
      product.additional_products = tmp_additional_products;


      $.each(product_notes, function(index, note_input) {
        let
        input = $(note_input),
        input_name = input.attr("name"),
        input_value = input.val(),
        sound_url = input.data("url");

        if (input_name.indexOf("[text_notes]") > -1) {
          note_items += `<input type="hidden" name="products[${product.index}]${input_name}" value="${input_value}">`;
        } else if (input_name.indexOf("[sound_notes]") > -1) {
          note_items += `<input type="hidden" name="products[${product.index}]${input_name}" value="${input_value}" data-url="${sound_url}">`;
        }
      });
      

      // product is editing 
      if (Pointofsales.Pos.Events.productEditingIndex !== 0) {

        row = $(`#pos-list tr[data-index="${product.index}"]`);

        $('input[name*="[stocks]"]', row).remove();
        $('input[name*="[additional_products]"]', row).remove();
        $('input[name*="[sound_notes]"]', row).remove();
        $('input[name*="[text_notes]"]', row).remove();

        $(".pr-quantity b", row).html(quantity);
        $("input[name*='[quantity]']", row).val(quantity);
        $(".pr-total", row).html(total.toFixed(2));
        $(".pr-quantity", row).append(items + note_items);

        row.effect("highlight", {color: "yellow"}, 800);

      } else {

        // Same product exists in the basket. Editing...
        if (sameProductExists) {
          let
          basket_product_quantity = $("input[name*='[quantity]']", sameProductExists).val();
          basket_product_quantity = parseInt(basket_product_quantity);

          row = sameProductExists;
          quantity += basket_product_quantity;
          total = price * quantity;

          $(`input[name*="[stocks]"]`, row).remove();
          $(`input[name*="[additional_products]"]`, row).remove();

          $(".pr-quantity b", row).html(quantity);
          $("input[name*='[quantity]']", row).val(quantity);
          $(".pr-total", row).html(total.toFixed(2));
          $(".pr-quantity", row).append(items + note_items);

          row.effect("highlight", {color: "yellow"}, 800);
        }
        // product is creating 
        else {

          Pointofsales.Pos.Templates.basketProductRow(product, function (full_row) {
            row = full_row;
          });

          $(".pr-quantity", row).append(note_items);
          $("#pos-list tbody").append(row);

          row.effect("highlight", {color: "green"}, 800);
        }
      }

      $(".pr-additional_title, .pr-additional_price, .pr-additional_total", row).html("");
      $.each(selected_additional_products, function(key, selected_additional_product) {
        let 
        additional_product_id = parseInt($(selected_additional_product).val()),
        additional_product_title = product.additional_products[additional_product_id].title,
        additional_product_price = parseFloat(product.additional_products[additional_product_id].price),
        additional_product_tax = parseFloat(product.additional_products[additional_product_id].tax),
        additional_product_total = 0;

        additional_product_price = Pointofsales.Helpers.addTax(additional_product_price, additional_product_tax);
        additional_product_total = additional_product_price * quantity;

        $(".pr-additional_title", row).append(additional_product_title + "<br>");
        $(".pr-additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
        $(".pr-additional_total", row).append(additional_product_total.toFixed(2) + "<br>");
      });

      if (modal.hasClass('in')) {
        modal.modal("toggle");
      }
      
      $("#pos-list").stop().animate({
        scrollTop:  row.offset().top - $("#pos-list tbody").offset().top + $("#pos-list tbody").scrollTop()
      }, 300);

      Pointofsales.Pos.calculate();


    },

    onClickEditProduct: function () {

      let
      modal = $("#modal-product_content"),
      row = $(this).closest("tr"),
      index = row.data("index"),
      product_id = $('input[name*="[product_id]"]', row).val(),
      quantity = $('input[name*="[quantity]"]', row).val(),
      interim_payments = $('input[name*="[quantity]"]', row).attr("interim_payments"),
      product_completed = $('input[name*="[quantity]"]', row).attr("completed"),
      product_notes = $('input[name*="[sound_notes]"], input[name*="[text_notes]"]', row),
      query_string = `product_id=${product_id}`;

      Pointofsales.Pos.Events.productPortions = {};
      Pointofsales.Pos.Events.productEditingIndex = index;
      Pointofsales.Pos.Events.interimPaidCount = 0;
      $(".modal-title", modal).html("");
      $('input[name="quantity"]', modal).val(quantity);
      $("#portions", modal).html("");
      $("#stocks", modal).html("");
      $("#additional_products", modal).html("");
      $("#product_notes", modal).html("");
      modal.modal("toggle");
      $(".process_product", modal).html("Düzenle");

      Pointofsales.Helpers.addLoader($(".modal-content", modal));
      Pointofsales.Helpers.postForm(null, `getProductPortions?${query_string}`, function (json) {

        Pointofsales.Helpers.removeLoader($(".modal-content", modal));

        if (!json.result) {
          modal.modal("toggle");
          return;
        }

        Pointofsales.Pos.Events.productPortions = json.data.products;
        Pointofsales.Pos.Events.interimPaidCount = parseInt(interim_payments);
        Pointofsales.Pos.Events.completedCount = parseInt(product_completed);

        $("#portions", modal).append(
          $("<option>")
          .val(json.data.products[product_id].id)
          .html(json.data.products[product_id].title)
        );

        $("#portions", modal).trigger("change");

        let stocks = $(`input[name^="products[${index}][stocks]"]`, row);
        $.each(stocks, function(key, row) {
          let stock_id = $(this).val();
          $(`input[name="stocks[${stock_id}]"]`, modal).prop("checked", true);
        });

        let additional_products = $(`input[name^="products[${index}][additional_products]"]`, row);
        $.each(additional_products, function(key, row) {
          let additional_product_id = $(this).val();
          $(`input[name="additional_products[${additional_product_id}]"]`, modal).prop("checked", true);
        });


        $.each(product_notes, function(index, note_input) {
          let
          input = $(note_input),
          input_name = input.attr("name"),
          input_value = input.val(),
          template = "";

          if (input_name.indexOf("[text_notes]") > -1) {
            template = Pointofsales.ProductNotes.Templates.newTextNote(input_value);
          } else if (input_name.indexOf("[sound_notes]") > -1) {
            template = Pointofsales.ProductNotes.Templates.newSoundNote({
              "id": input_value,
              "sound": input.data("url")
            });
          }

          Pointofsales.ProductNotes.Templates.newNote(template);
        });
      });
    },

    onClickPlus: function () {
      let
      input = $("#modal-product_content input[name=quantity]"),
      quantity = parseInt(input.val());

      quantity++;
      input.val(quantity)
    },

    onClickMinus: function () {
      let
      input = $("#modal-product_content input[name=quantity]"),
      quantity = parseInt(input.val());

      quantity--;

      if (quantity > 0) {
        input.val(quantity);
      }
    },

    onClickRemoveProduct: function() {
      let
      button = $(this),
      row = button.closest("tr.pr-container"),
      interim_payments = parseInt($("input[name*='[quantity]']", row).attr("interim_payments")),
      product_completed = parseInt($("input[name*='[quantity]']", row).attr("completed"));

      if (interim_payments > 0) {
        Pleasure.handleToastrSettings("Hata!", "Ara ödemesi yapılmış ürün iptal edilemez.", "error");
        return;
      }
      if (product_completed > 0) {
        if (!Pointofsales.Helpers.hasPermission("pointofsales.forceproductdecrement")) {
          Pleasure.handleToastrSettings("Hata!", "Hazırlanmış ürün iptal edilemez.", "error");
          return;
        }
      }

      row.fadeOut(300, function() {
        row.remove();
        Pointofsales.Pos.calculate();
      });
    },

    onClickAddPayment: function () {
      
      let
      id = $(this).attr("id"),
      type = id.replace(/add-/, ""),
      total = $("#total").html(),
      interim_paid = $("#pos-interim_paid").html(),
      paid = $("#pos-paid").html(),
      payment = 0,
      input,
      div,
      bg_color = "";


      total = parseFloat(total.replace(/[^0-9\.]/g, ""));
      interim_paid = parseFloat(interim_paid.replace(/[^0-9\.]/g, ""));
      paid = parseFloat(paid.replace(/[^0-9\.]/g, ""));
      payment = total - (interim_paid + paid);
      payment = (payment < 0) ? 0 : payment;

      // input = $(`
      //   <div class="input-group">
      //     <span class="input-group-addon p-l-1"><i class="remove-payment btn btn-xs fa fa-times"></i></span>
      //     <div class="inputer">
      //         <div class="input-wrapper">
      //             <input name="payments[${type}][]" type="text" class="form-control text-right numpad calculator-input" value="${payment.toFixed(2)}">
      //         </div>
      //     </div>
      //   </div>
      // `);
      

      if (type === "cash") {
        bg_color = "#82C258";
      } else if (type === "credit") {
        bg_color = "#007CB3";
      } else {
        bg_color = "#00BED1";
      }
      

      div = $(`
        <div class="input-group col-sm-3" style="margin: 0 !important;display: inline-table; margin-left: 2px !important;">
            <span class="input-group-addon p-a-0"><i class="remove-payment btn btn-danger fa fa-times fa-2x br-0" style="height: 75px !important; display: table-cell;"></i></span>
            <div class="inputer">
                <div class="input-wrapper">
                    <input name="payments[${type}][]" type="text" class="form-control text-right numpad calculator-input text-center text-white" style="height: 75px !important; font-weight:bold; background: ${bg_color};" value="${payment.toFixed(2)}">
                </div>
            </div>
        </div>
      
      `)

      $(`#payments-in`).append(div);

      Pointofsales.Helpers.customNumpad($("input", div));

      div.find('input').focus().select().trigger("input");

    },

    onClickPaymentCari: function () {

      let 
      modal = $('#modal-cari'),
      rest = $('#pos-rest').text();

      $('input', modal).val(rest);

      //Müşterilerin doldurulması
      $('#customerSelect', modal).html("");

      Pointofsales.Helpers.postForm(null, "getCustomer", function (json) {
        $.each(json.data.customers, function (index, customer) { 
          
          $('#customerSelect', modal).append(`
            <option value="${customer.id}">${customer.name}</option>
          `);

        });

        $('#customerSelect', modal).trigger("chosen:updated");

      });

      modal.modal('toggle');

    },

    onClickAddCari: function () {

      let
      modal = $('#modal-cari'),
      payment = $('input[name="payment_cari"]', modal).val(),
      customer_id = $('#customerSelect option:selected', modal).val(),
      payment_input = $(`#payments-in input[name="payments[cari][${customer_id}]"]`),
      total = 0;

      if(payment_input.length != 0) {
        total = parseFloat(payment_input.val()) + parseFloat(payment);
        payment_input.val(total);
        payment_input.focus().select().trigger("input");

      } else {

      div = $(`
        <div class="input-group col-sm-3" style="margin: 0 !important;display: inline-table; margin-left: 2px !important;">
            <span class="input-group-addon p-a-0"><i class="remove-payment btn btn-danger fa fa-times fa-2x br-0" style="height: 75px !important; display: table-cell;"></i></span>
            <div class="inputer">
                <div class="input-wrapper">
                    <input name="payments[cari][${customer_id}]" type="text" class="form-control text-right numpad calculator-input text-center text-white" style="height: 75px !important; font-weight:bold; background: #FD8024" value="${payment}">
                </div>
            </div>
        </div>
      
      `)
      
      }

      $(`#payments-in`).append(div);
      div.find('input').focus().select().trigger("input");

      



    },

    onClickCancelSale: function () {

      Pointofsales.Helpers.addLoader(Pointofsales.Pos.container);
      Pointofsales.Helpers.postForm("#pos", "cancelSale", function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.Pos.container);

       if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        Pleasure.handleToastrSettings("Başarılı", json.message, "success");
        Pointofsales.Pos.loadTable();

      });

    },

    onChangePaymentInput: function () {

      Pointofsales.Pos.calculate();
    },

    onClickRemovePayment: function() {
      $(this).closest(".input-group").remove();
      Pointofsales.Pos.calculate();
    },

    onClickComplete: function (event) {
      
      event.preventDefault();

      if (Pointofsales.Pos.Helpers.getPosStatus() === true) {
        Pointofsales.Pos.Helpers.setPosStatus(false);
      } else {
        return;
      }

      let
      button = $(this),
      button_id = button.attr("id"),
      action = "order";

      if (button_id === "pos-complete") {
        action = "checkout";
      }

      Pointofsales.Helpers.addLoader(Pointofsales.Pos.container);
      Pointofsales.Helpers.postForm("#pos", `complete?action=${action}`, function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.Pos.container);
        
        Pointofsales.Pos.Helpers.setPosStatus(true);

        if (json.result) {
          Pleasure.handleToastrSettings("Başarılı", json.message, "success");
        } else {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        Pointofsales.settings.sale_id = json.data.sale_id;

        if (typeof json.data.new_sale_id !== "undefined") {
          $("input[name=sale_id]", Pointofsales.posForm).val(json.data.new_sale_id);
        }

        $.each(json.data.inserted_product_ids, function(key, val) {
          $(`input[name="products[${val.index}][sale_product_id]"]`, "#pos-list").val(val.sale_product_id);
        });
        
        if (action === "checkout") {

          let modal = $('#modal-order-checkout'); 
          $('#new-order', modal).attr('data-order-type', json.data.sale.service_type);
          modal.modal('show');

          Pointofsales.Invoice.fill(json.data.sale);
          Pointofsales.Invoice.loadTab();
          Pointofsales.Pos.loadTable();
          
        }

        // Remove Product notes
        $('input[name*="[sound_notes]"]', "#pos-basket").remove();
        $('input[name*="[text_notes]"]', "#pos-basket").remove();
        
        // Remove discount key
        $("input[name='discount_key']", Pointofsales.posForm).val("");

      });
    },

    onClickModalPrintInvoice: function () {

      $('#tab-invoice .invoice-print').click();

      $('#modal-order-checkout').modal('hide');


    },

    onClickModalNewOrder: function () { 

      let service_type = $('#modal-order-checkout #new-order').attr('data-order-type');

      if ( service_type === "1") {

        Pointofsales.Tables.loadTab();

      } else if (service_type === "2") {

        Pointofsales.Tables.openPackageServiceTable();
      
      } else if (service_type === "3") {

        Pointofsales.Tables.openSelfServiceTable();

      }

      $('#modal-order-checkout').modal('hide');

    },
  },

  Templates: {
    basketProductRow: function (product, callback) {

      let 
      row = "",
      price = Pointofsales.Helpers.addTax(product.price, product.tax);
      product.quantity = parseInt(product.quantity),
      gift_label = (product.is_gift === "1") ? '<span class="label label-info">İKRAM</span>' : "",
      background = (!!product.id) ? "ordered" : "";

      price = Pointofsales.Helpers.round(price, 2);

      if (product.quantity === 0) {
        callback("");
        return;
      };

      row = $(`
        <tr data-index="${product.index}" class="pr-container ${background}">
          <td><span class="fa fa-pencil btn btn-primary btn-block edit" aria-hidden="true"></span></td>
          <td class="pr-quantity"><b>${product.quantity}</b></td>
          <td>
            <span class="pr-title">${product.title} ${gift_label}</span>
            <p class="pr-additional_title"></p>
          </td>
          <td class="text-right">
            <span class="pr-price">${price.toFixed(2)}</span>
            <p class="pr-additional_price"></p>
          </td>
          <td class="text-right">
            <span class="pr-total">${(price * product.quantity).toFixed(2)}</span>
            <p class="pr-additional_total"></p>
          </td>
          <td class="text-right "><span class="fa fa-times btn btn-danger btn-block remove" aria-hidden="true"></span></td>
        </tr>
      `);

      Pointofsales.Pos.Templates.basketProductRowInputs(row, product, function (full_row) {
        callback(full_row);
      });
      
    },

    basketProductRowInputs: function (row, product, callback) {

      let interim_payments = 0;
      Pointofsales.Helpers.countInterimPayments(product, function (count) {
        interim_payments = count;
      });

      $(".pr-quantity", row).append(`
        <input type="hidden" name="products[${product.index}][product_id]" value="${product.product_id}">
        <input type="hidden" name="products[${product.index}][sale_product_id]" value="${product.id}">
        <input type="hidden" name="products[${product.index}][quantity]" value="${product.quantity}" price="${product.price}" tax="${product.tax}" is_gift="${product.is_gift}" interim_payments="${interim_payments}" completed="${product.completed}">
      `);
      
      if (product.type === "product") {
        $.each(product.stocks, function(index, stock) {
          if (!parseInt(stock.in_use)) {
            $(".pr-quantity", row)
            .append(`<input type="hidden" name="products[${product.index}][stocks][${stock.stock_id}]" value="${stock.stock_id}">`);
          }
        });
      }

      let 
      permissions_delete = Pointofsales.Helpers.hasPermission("pointofsales.productdelete"),
      permissions_edit = Pointofsales.Helpers.hasPermission("pointofsales.productedit");

      if (permissions_delete) {
        $(".remove", row).removeClass('hidden');
      } else {
        $(".remove", row).addClass('hidden');
      }

      if (permissions_edit) {
        $(".edit", row).removeClass('hidden');
      } else {
        $(".edit", row).addClass('hidden');
      }


      

      $.each(product.additional_products, function(index, additional_product) {
        $(".pr-quantity", row)
        .append(`<input type="hidden" name="products[${product.index}][additional_products][${additional_product.product_id}]" value="${additional_product.product_id}" price="${additional_product.price}" tax="${additional_product.tax}">`);
        
        let
        quantity = parseInt(product.quantity),
        price = parseFloat(additional_product.price),
        tax = parseFloat(additional_product.tax),
        total = 0;

        price = Pointofsales.Helpers.addTax(price, tax);
        total = price * quantity;

        $(".pr-additional_title", row).append(additional_product.title + "<br>");
        $(".pr-additional_price", row).append(price.toFixed(2) + "<br>");
        $(".pr-additional_total", row).append(total.toFixed(2) + "<br>");
      });



      callback(row);
    },
  },

  Helpers: {
    hasSameProductInBasket: function (basket_product_row, selected_stocks, selected_additional_products, callback) {
      
      let
      hasSameStock = false,
      hasSameAdditionalProducts = false,
      countSameStock = 0,
      countSameAdditionalProducts = 0,
      basket_stocks = $("input[name*='[stocks]']", basket_product_row),
      basket_additional_products = $("input[name*='[additional_products]']", basket_product_row),
      is_gift = $("input[name*='[quantity]']", basket_product_row).attr("is_gift");

      if (is_gift === "1") {
        callback(false);
        return;
      }

      if (selected_stocks.length) {
        if (selected_stocks.length === basket_stocks.length) {
          $.each(selected_stocks, function(index, selected_stock) {
            $.each(basket_stocks, function(index, basket_stock) {

              if (parseInt($(selected_stock).val()) === parseInt($(basket_stock).val())) {
                countSameStock++;
                return;
              }
            });
          });
          if (countSameStock === selected_stocks.length) {
            hasSameStock = true;
          }
        }
      } else if (!basket_stocks.length) {
        hasSameStock = true;
      }

      if (!hasSameStock) {
        callback(false);
        return;
      }

      if (selected_additional_products.length) {
        if (selected_additional_products.length === basket_additional_products.length) {
          $.each(selected_additional_products, function(index, selected_additional_product) {
            $.each(basket_additional_products, function(index, basket_additional_product) {

              if (parseInt($(selected_additional_product).val()) === parseInt($(basket_additional_product).val())) {
                countSameAdditionalProducts++;
                return;
              }
            });
          });
          if (countSameAdditionalProducts === selected_additional_products.length) {
            hasSameAdditionalProducts = true;
          }
        }
      } else if (!basket_additional_products.length) {
        hasSameAdditionalProducts = true;
      }

      if (hasSameAdditionalProducts) {
        callback(basket_product_row);
      } else {
        callback(false);
      }
    },

    posStatus: true,
    getPosStatus: function (status) {
      return Pointofsales.Pos.Helpers.posStatus;
    },

    setPosStatus: function (status) {
      status = (status === true ? true : false);
      Pointofsales.Pos.Helpers.posStatus = status;
    },

    displayTableName: function (table_name) {
      $("#pos-tablename", "#pos-details").html(table_name);
    },

    displayCustomerName: function (customer) {
      $("#pos-customer", "#pos-details").removeClass("hidden");
      $("#pos-customer-tel", "#pos-details").removeClass("hidden");
      $("#pos-customer span", "#pos-details").html(`${customer.name}`);
      $("#pos-customer-tel span", "#pos-details").html(`${customer.phone}`);
    },

    displayPackageServiceTrackButton: function() {
      if (Pointofsales.settings.package_service) {
        $('#package_service_button_container').removeClass('hidden');
      } else {
        $('#package_service_button_container').addClass('hidden');
      }
    }
  }
};




Pointofsales.Return = {

  container: "#tab-return",
  sale: {},
  session: {
    session: "",
    sale_id: "",
  },

  init: function () {

    Pointofsales.Return.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-return']").trigger("click");
    Pointofsales.Return.loadSession();
  },

  loadSession: function () {

    $("input[name='sale_id']", Pointofsales.Return.container).val(Pointofsales.Return.session.sale_id);
    $("input[name='session']", Pointofsales.Return.container).val(Pointofsales.Return.session.session);

    Pointofsales.Return.emptySession();

    Pointofsales.Helpers.addLoader(Pointofsales.Return.container);

    Pointofsales.Helpers.postForm("#return", "getSaleForReturn", Pointofsales.Return.loadProducts);
  },

  emptySession: function () {
    
    // Variables
    Pointofsales.Return.sale = {};
    Pointofsales.Return.session.sale_id = "";
    Pointofsales.Return.session.session = "";

    // DOM Elements
    $("#return-products", Pointofsales.Return.container).html("");
    $("#return-basket", Pointofsales.Return.container).html("");
    $("#subtotal", Pointofsales.Return.container).html("0.00");
    $("#tax-total", Pointofsales.Return.container).html("0.00");
    $("#discount-total", Pointofsales.Return.container).html("0.00");
    $("#total", Pointofsales.Return.container).html("0.00 " + Pointofsales.settings.location.currency);
    $("textarea[name='desc']", Pointofsales.Return.container).val("");
    $("input[name='payment_type']", Pointofsales.Return.container).each(function(index, el) {
      $(this).prop("checked", false);
      $(this).closest("label").removeClass("active");
    });

    // Return Login Form
    $("input[name='sale_id'], input[name='username'], input[name='password']", "#modal-return_login").val("");
  },

  loadProducts: function (json) {
    
    Pointofsales.Helpers.removeLoader(Pointofsales.Return.container);

    if (!json.result) {
      Pleasure.handleToastrSettings('Hata!', json.message, 'error');
      return;
    }

    if (!!!json.data.sale.products) {
      $("#return-products", Pointofsales.Return.container).html(Pointofsales.Templates.noContentFound("Ürün Bulunamadı."));
      return;
    }

    Pointofsales.Return.sale = json.data.sale;

    $("#discount-total", Pointofsales.Return.container).html(
      Pointofsales.Discount.displayDiscount(
        Pointofsales.Return.sale.discount_amount, Pointofsales.Return.sale.discount_type
      )
    );

    $.each(json.data.sale.products, function(index, product) {
      Pointofsales.Return.Templates.productRow(product, function (row) {
        $("#return-products", Pointofsales.Return.container).append(row);
      });
    });
  },
  
  calculate: function () {
    let
    taxtotal = 0,
    subtotal = 0,
    total = 0,
    discountamount = Pointofsales.Return.sale.discount_amount,
    discounttype = Pointofsales.Return.sale.discount_type,
    basket_products = $("#return-basket .product-container", Pointofsales.Return.container);

    $.each(basket_products, function(index, basket_product) {

      let 
      id = $(this).data("id"),
      quantity = $("input[name^='products']", this).val();
      quantity = parseInt(quantity);

      $.each(Pointofsales.Return.sale.products, function(index, product) {
        if (id === parseInt(product.id)) {

          let 
          tax = parseFloat(product.tax),
          price = parseFloat(product.price);

          tax = Pointofsales.Helpers.calcTax(price, tax);
          tax *= quantity;
          price *= quantity;

          taxtotal += tax;
          subtotal += price;
          total += price + tax;


          $.each(product.additional_products, function(index, additional_product) {
            
            let 
            additional_product_tax = parseFloat(additional_product.tax),
            additional_product_price = parseFloat(additional_product.price);

            additional_product_tax = additional_product_price * additional_product_tax / 100;
            additional_product_tax = Pointofsales.Helpers.round(additional_product_tax, 3);
            additional_product_tax *= quantity;
            additional_product_price *= quantity;

            taxtotal += additional_product_tax;
            subtotal += additional_product_price;
            total += additional_product_price + additional_product_tax;

          });

          return;
        }
      });

    });

    if (discounttype === "amount") {
      total -= discountamount;
    } else if (discounttype === "percent") {
      total -= Pointofsales.Helpers.round(total * discountamount / 100, 2);
    }

    if (total < 0) {
      total = 0;
    }


    $("#subtotal", Pointofsales.Return.container).html(subtotal.toFixed(2));
    $("#tax-total", Pointofsales.Return.container).html(taxtotal.toFixed(2));
    $("#total", Pointofsales.Return.container).html(total.toFixed(2) +" "+ Pointofsales.settings.location.currency);
  },

  findInBasket: function (product_id, callback) {
      
    let 
    row = false,
    quantity = 0,
    input = $(`#return-basket input[name='products[${product_id}]']`, Pointofsales.Return.container);

    if (input.length > 0) {
      row = $(input).closest(".product-container");
      quantity = parseInt(input.val());
    }

    callback(row, quantity);
  },

  Events: {
    init: function () {
      $("body").on("click", "#return-products .product-container", Pointofsales.Return.Events.onClickProductInProducts);
      $("body").on("click", "#return-basket .product-container", Pointofsales.Return.Events.onClickProductInBasketProducts);
      $("body").on("click", "#return-complete", Pointofsales.Return.Events.onClickCompleteReturn);
      $("body").on("click", "#return-cancel", Pointofsales.Return.Events.onClickCancelReturn);
      $("body").on("click", "#submit-return-login", Pointofsales.Return.Events.onSubmitReturnLogin);
    },

    onClickProductInProducts: function () {
      
      let
      $this = $(this),
      sale_product_id = $this.data("id"),
      $quantity = $(".quantity", $this),
      quantity = parseInt($quantity.html()),
      is_gift = Number($this.attr("is_gift")),
      product;

      if (is_gift || quantity < 1)
        return;
      quantity--;

      $.each(Pointofsales.Return.sale.products, function(index, _product) {
        if (parseInt(_product.id) == sale_product_id) {
          product = _product;
          return;
        }
      });

      if (!!product) {

        $quantity.html(quantity);

        Pointofsales.Return.findInBasket(sale_product_id, function (basket_product_row, basket_product_quantity) {

          if (basket_product_row) {
            product.quantity = 1 + basket_product_quantity;
          } else {
            product.quantity = 1;
          }

          Pointofsales.Return.Templates.basketProductRow(product, function (row) {
            if (basket_product_row) {
              row.effect("highlight", {color: "yellow"}, 800);
              basket_product_row.replaceWith(row);
            } else {
              row.effect("highlight", {color: "green"}, 800);
              $("#return-basket", Pointofsales.Return.container).append(row);
            }

            $("#return-basket").stop().animate({
              scrollTop:  row.offset().top - $("#return-basket").offset().top + $("#return-basket").scrollTop()
            }, 300);
          });

        });
      }

      Pointofsales.Return.calculate();
    },

    onClickProductInBasketProducts: function () {
      
      let 
      basket_product_row = $(this),
      sale_product_id = basket_product_row.data("id"),
      sold_product_row = $(`#return-products .product-container[data-id='${sale_product_id}']`),
      basket_product_input = $(`input[name='products[${sale_product_id}]']`, basket_product_row),
      basket_product_quantity = parseInt(basket_product_input.val()),
      sold_product_quantity = parseInt($(".quantity", sold_product_row).html());

      basket_product_quantity--;
      sold_product_quantity++

      if (basket_product_quantity < 1) {
        basket_product_row.remove();
      } else {
        basket_product_input.val(basket_product_quantity);
        $(".quantity b", basket_product_row).html(basket_product_quantity);
      }

      $(".quantity", sold_product_row).html(sold_product_quantity);

      Pointofsales.Return.calculate();
    },

    onClickCompleteReturn: function (event) {
      event.preventDefault();
      
      Pointofsales.Helpers.addLoader(Pointofsales.Return.container);

      Pointofsales.Helpers.postForm("#return", "completeReturn", function (json) {

        Pointofsales.Helpers.removeLoader(Pointofsales.Return.container);

        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        // Invoice
        Pointofsales.Invoice.fillReturn(json.data.return_sale);
        Pointofsales.Invoice.loadReturnTab();

      });
    },

    onClickCancelReturn: function (event) {
      event.preventDefault();

      Pointofsales.Tables.loadTab();
    },

    onSubmitReturnLogin: function () {
      
      let
      modal = $(this).closest(".modal"),
      form = $(this).closest("form");

      Pointofsales.Helpers.postForm(form, "loginForReturnProduct", function (json) {

        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pointofsales.Return.session.session = json.data.session;
        Pointofsales.Return.session.sale_id = json.data.sale_id;

        modal.modal("toggle");
        Pointofsales.Return.loadTab();

      });
    },
  },

  Templates: {
    productRow: function (product, callback) {

      let 
      return_quantity = parseInt(product.return_quantity),
      quantity = parseInt(product.quantity) - return_quantity,
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, product.tax),
      total = price * quantity,
      row,
      gift_label = "",
      gift_class = "";

      if (product.is_gift === "1") {
        gift_label = '<span class="label label-info">İKRAM</span>';
        gift_class = 'cursor-no-drop';
      }

      if (quantity === 0) {
        callback("");
        return;
      }

      row = $(`
      <tr class="product-container ${gift_class}" data-id="${product.id}" is_gift="${product.is_gift}">
          <td class="quantity">${quantity}</td>
          <td>
            ${product.title} ${gift_label} <i class="stock_title p-l-1 small"></i>
            <p class="additional_title small"></p>
          </td>
          <td class="text-right">
            ${price.toFixed(2)}
            <p class="additional_price small"></p>
          </td>
      </tr>
      `);

      $.each(product.additional_products, function(index, additional_product) {
        let 
        additional_product_price = Pointofsales.Helpers.addTax(additional_product.price, additional_product.tax),
        additional_product_total = additional_product_price * quantity;

        $(".additional_title", row).append(additional_product.title + "<br>");
        $(".additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
      });

      if (product.type === "product") {
        let i = 0;
        $.each(product.stocks, function(index, stock) {
          if (!parseInt(stock.in_use)) {
            if (i++ === 0) {
              $(".stock_title", row).append("Malzemeler: " + stock.stock_name);
            } else {
              $(".stock_title", row).append("," + stock.stock_name);
            }
          }
        });
      }
      

      callback(row);
    },

    basketProductRow: function (product, callback) {

      let 
      row = "",
      price = Pointofsales.Helpers.addTax(product.price, product.tax);
      product.quantity = parseInt(product.quantity);

      row = $(`
        <tr class="product-container" data-id="${product.id}">
          <td class="quantity"><b>${product.quantity}</b></td>
          <td>
            <span>${product.title}</span>
            <p class="additional_title"></p>
          </td>
          <td class="text-right">
            <span>${price.toFixed(2)}</span>
            <p class="additional_price"></p>
          </td>
          <td class="text-right">
            <span class="pr-total">${(price * product.quantity).toFixed(2)}</span>
            <p class="additional_total"></p>
          </td>
        </tr>
      `);

      Pointofsales.Return.Templates.basketProductRowInputs(row, product, function (full_row) {
        callback(full_row);
      });
    },

    basketProductRowInputs: function (row, product, callback) {

      $(".quantity", row).append(`
        <input type="hidden" name="products[${product.id}]" value="${product.quantity}">
      `);

      $.each(product.additional_products, function(index, additional_product) {        
        let
        quantity = parseInt(product.quantity),
        price = parseFloat(additional_product.price),
        tax = parseFloat(additional_product.tax),
        total = 0;

        price = Pointofsales.Helpers.addTax(price, tax);
        total = price * quantity;

        $(".additional_title", row).append(additional_product.title + "<br>");
        $(".additional_price", row).append(price.toFixed(2) + "<br>");
        $(".additional_total", row).append(total.toFixed(2) + "<br>");
      });

      callback(row);
    },
  },
};




Pointofsales.Tables = {

  container: "#tab-tables",
  floorsAndZones: {},
  zonePoints: {},
  timer: null,

  init: function () {
    Pointofsales.Tables.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-tables']").trigger("click");

    Pointofsales.Tables.emptyTab();

    Pointofsales.Tables.setBackButton();

    Pointofsales.Helpers.addLoader(Pointofsales.Tables.container);

    Pointofsales.Tables.getFloorsWithZones(function (floors) {
      Pointofsales.Tables.handleFloorsAndZones(floors, function(html) {

        Pointofsales.Helpers.removeLoader(Pointofsales.Tables.container);

        $("#tab_buttons_container", Pointofsales.Tables.container).html(html);

        Pointofsales.Tables.selectActiveFloorAndZone(Pointofsales.Tables.container);
        
      });
    });
  },

  emptyTab: function () {
    $("#tab_buttons_container", Pointofsales.Tables.container).html("");
    $("#tab_contents_container", Pointofsales.Tables.container).html("");
  },

  getFloorsWithZones: function (callback) {
    Pointofsales.Helpers.postForm(null, "getFloorsWithZones", function (json) {
      if (json.result) {
        callback(json.data.floors);
      }
    });
  },

  selectActiveFloorAndZone: function (container) {

    if (Pointofsales.settings.floor_id && Pointofsales.settings.zone_id) {

      if ($(`.tab_floor[data-id=${Pointofsales.settings.floor_id}]`, container).length > 0) {
        $(`.tab_floor[data-id=${Pointofsales.settings.floor_id}]`, container).trigger("click");
      } else {
        $(".tab_floor:first", container).trigger("click");
      }

      if ($(`.tab_zone[data-id=${Pointofsales.settings.zone_id}]`, container).length > 0) {
        $(`.tab_zone[data-id=${Pointofsales.settings.zone_id}]`, container).trigger("click");
      } else {
        $(".tab_zone:first", container).trigger("click");
      }

    } else {
      $(".tab_floor:first", container).trigger("click");
      $(".tab_zone:first", container).trigger("click");
    }
  },

  handleFloorsAndZones: function (floors, callback) {

    let 
    prefix = +new Date(),
    floor_tabs = "",
    zone_tabs = "",
    floor_tab_contents = "",
    html = ""
    hasFloor = 0;

    floor_tabs += '<ul class="nav nav-pills">';
    $.each(floors, function(floor_index, floor) {

      if (floor.zones.length < 1) {
        return;
      }
      hasFloor++;

      floor_tabs += `
        <li class="p-b-1">
          <a class="btn btn-grey btn-sm tab_floor" data-toggle="pill" href="#tab-floors-${prefix}-${floor.id}" data-id="${floor.id}">${floor.title}</a>
        </li>
      `;

      zone_tabs = '<ul class="nav nav-pills">';
      $.each(floor.zones, function(zone_index, zone) {

        zone_tabs += `
          <li class="p-b-1">
            <a 
              class="btn btn-grey btn-sm tab_zone" 
              data-toggle="pill" 
              href="#tab-zones-${prefix}-${zone.id}" 
              data-id="${zone.id}" 
              data-background="${zone.background}">
              ${zone.title}
            </a>
          </li>
        `;
      });
      zone_tabs += '</ul>';

      floor_tab_contents += `
        <div id="tab-floors-${prefix}-${floor.id}" class="tab-pane tab-floors fade">
          ${zone_tabs}
        </div>
      `;

    });
    floor_tabs += '</ul>';

    floor_tab_contents = `<div class="tab-content p-a-0">${floor_tab_contents}</div>`;

    if (hasFloor > 0) {
      html = floor_tabs + floor_tab_contents;
    } else {
      html = Pointofsales.Templates.noContentFound("Bölge bulunamadı.");
    }

    callback(html);
  },

  handleZonePoints: function (callback) {

    if (Pointofsales.settings.is_mobile) {

      Pointofsales.Tables.Templates.zonePointRows(Pointofsales.Tables.zonePoints, function (rows) {
        Pointofsales.Tables.Templates.zonePointDataTable(rows, function (point_list) {
          callback(point_list);
        });
      });

    } else {

      Pointofsales.Tables.Templates.zonePointLayout(Pointofsales.Tables.zonePoints, function (table_layouts) {
        callback(table_layouts);
      });
    }
  },

  setBackButton: function () {
    if (Pointofsales.getPointId()) {
      $(".backButton", Pointofsales.Tables.container).removeClass("hidden");
    } else {
      $(".backButton", Pointofsales.Tables.container).addClass("hidden");
    }
  },

  openPackageServiceTable: function () {
    $("#tab-tables .service-buttons[data-service='package']").trigger("click");
  },

  openSelfServiceTable: function () {
    $("#tab-tables .service-buttons[data-service='self']").trigger("click");
  },

  getZonePoints: function (query_string, background) {

    Pointofsales.Helpers.postForm(null, `getZonePoints?${query_string}`, function (json) {
      Pointofsales.Helpers.removeLoader(Pointofsales.Tables.container);

      if (!json.result) return;

      Pointofsales.Tables.zonePoints = json.data.points;

      Pointofsales.Tables.handleZonePoints(function (tables) {
        $("#tab_contents_container", Pointofsales.Tables.container).html(tables);

        // Add Background
        if (!!background) {
          $("#tab_contents_container .points-ground", Pointofsales.Tables.container)
            .addClass("has-background")
            .css("background", `url(${base_url + background})`);
        }
      });

    });
  },

  Events: {

    init: function () {
      $("body").on("click", "#tab-tables a.tab_zone", Pointofsales.Tables.Events.onClickZones);
      $("body").on("click", "#tab-tables #tab_contents_container a.load_table", Pointofsales.Tables.Events.onClickTable);
      $("body").on("click", "#tab-tables .service-buttons", Pointofsales.Tables.Events.onClickGetServicePoint);
    },

    onClickZones: function () {

      Pointofsales.Helpers.addLoader(Pointofsales.Tables.container);

      let 
      tab = $(this),
      zone_id = tab.data("id"),
      floor_id = tab.closest(".tab-floors").attr("id"),
      background = tab.data("background"),
      query_string = "";

      floor_id = $(`a[href=#${floor_id}]`, Pointofsales.Tables.container).data("id");
      query_string = `floor_id=${floor_id}&zone_id=${zone_id}`;

      // Remove Background
      $("#tab_contents_container .points-ground", Pointofsales.Tables.container)
      .removeClass("has-background")
      .css("background", "");

      Pointofsales.Tables.getZonePoints(query_string, background);
      
      clearInterval(Pointofsales.Tables.timer);
      
      Pointofsales.Tables.timer = setInterval(function () {

        Pointofsales.Tables.getZonePoints(query_string, background);

      }, 10000);
      
    },

    

    onClickTable: function (event) {

      event.preventDefault();

      let 
      table = $(this),
      id = table.data("id"),
      title = table.data("title");

      Pointofsales.setPointId(id);

      location.hash = `point:${id}`;

      Pointofsales.Pos.loadTab();
    },

    onClickGetServicePoint: function () {
      let
      button = $(this),
      service = button.data("service"),
      query_string = `service_type=${service}`;

      Pointofsales.Helpers.addLoader(Pointofsales.Tables.container);

      Pointofsales.Helpers.postForm(null, `getServicePoint?${query_string}`, function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.Tables.container);

        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pointofsales.setPointId(json.data.point.id);
        location.hash = `point:${json.data.point.id}`;
        Pointofsales.Pos.loadTab();

        if (service === "package") {
          Pointofsales.PackageServiceCustomer.loadModal();
        }
      });
    }
  },

  Templates: {
    zonePointRows: function (points, callback) {
      
      let rows = "";
      $.each(Pointofsales.Tables.zonePoints, function(index, point) {

        let 
        status = (point.status == 1 ? '<span class="label label-danger">DOLU</span>' : '<span class="label label-success">BOŞ</span>'),
        active = (Pointofsales.getPointId() == point.id ? "pnt-active" : "");

        point.sale_amount = parseFloat(point.sale_amount) || 0;

        rows += `
          <tr class="${active}">
              <td>${point.title}</td>
              <td>${point.created}</td>
              <td>${point.sale_amount.toFixed(2)} ${Pointofsales.settings.location.currency}</td>
              <td>${status}</td>
              <td class="text-right">
                <a href="#" data-id="${point.id}" data-title="${point.title}" class="btn btn-success btn-blue btn-ripple load_table">SEÇ</a>
              </td>
          </tr>
        `;
      });

      callback(rows);
    },

    zonePointDataTable: function (rows, callback) {
      
      let point_list = Pointofsales.Templates.noContentFound("Masa bulunamadı.");

      if (rows) {
        point_list = `
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Masa</th>
                    <th>Açılış</th>
                    <th>Fiyat</th>
                    <th>Durum</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  ${rows}
                </tbody>
              </table>
            </div>
          </div>
        `;
      }

      callback(point_list);
    },

    zonePointLayout: function (points, callback) {
      
      let
      layout = Pointofsales.Templates.noContentFound("Masa bulunamadı."),
      tables = "";

      if (points.length) {
        $.each(points, function(index, point) {

          point.id = parseInt(point.id);
          point.status = parseInt(point.status);
          point.sale_amount = parseFloat(point.sale_amount) - parseFloat(point.interim_payment_amount) || 0;

          let
          status = (point.status === 1 ? "pnt-red" : "pnt-green"),
          active = (Pointofsales.getPointId() == point.id ? "pnt-active" : "");

          tables += `
            <a href="#" data-id="${point.id}" data-title="${point.title}" class="text-center load_table">
              <div class="pnt ${status} ${active}" style="${point.planner_settings}">
                <div>${point.title}</div>
                <div>${point.sale_amount.toFixed(2)} ${Pointofsales.settings.location.currency}</div>
              </div>
            </a>
          `;

        });
        layout = `<div class="points-ground">${tables}</div>`;
      }

      callback(layout);
    },
  },
};




Pointofsales.MoveTable = {

  container: "#tab-movetable",

  init: function () {
    Pointofsales.MoveTable.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-movetable']").trigger("click");

    Pointofsales.MoveTable.emptyTab();

    Pointofsales.Helpers.addLoader(Pointofsales.MoveTable.container);

    Pointofsales.Tables.getFloorsWithZones(function (floors) {
      Pointofsales.Tables.handleFloorsAndZones(floors, function(html) {

        Pointofsales.Helpers.removeLoader(Pointofsales.MoveTable.container);

        $("#tab_buttons_container", Pointofsales.MoveTable.container).html(html);

        Pointofsales.Tables.selectActiveFloorAndZone(Pointofsales.MoveTable.container);
        
      });
    });
  },

  emptyTab: function () {
    $("#tab_buttons_container", Pointofsales.MoveTable.container).html("");
    $("#tab_contents_container", Pointofsales.MoveTable.container).html("");
  },

  Events: {
    init: function () {
      $("body").on("click", `${Pointofsales.MoveTable.container} a.tab_zone`, Pointofsales.MoveTable.Events.onClickZones);
      $("body").on("click", `${Pointofsales.MoveTable.container} #tab_contents_container a.load_table`, Pointofsales.MoveTable.Events.onClickTable);
    },

    onClickZones: function (event) {

      Pointofsales.Helpers.addLoader(Pointofsales.MoveTable.container);

      let 
      tab = $(this),
      zone_id = tab.data("id"),
      floor_id = tab.closest(".tab-floors").attr("id"),
      background = tab.data("background"),
      query_string = "";

      floor_id = $(`a[href=#${floor_id}]`, Pointofsales.MoveTable.container).data("id");
      query_string = `floor_id=${floor_id}&zone_id=${zone_id}`;


      // Remove Background
      $("#tab_contents_container .points-ground", Pointofsales.MoveTable.container)
      .removeClass("has-background")
      .css("background", "");

      Pointofsales.Helpers.postForm(null, `getZonePoints?${query_string}`, function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.MoveTable.container);

        if (!json.result) return;

        Pointofsales.Tables.zonePoints = json.data.points;

        Pointofsales.Tables.handleZonePoints(function (tables) {
          $("#tab_contents_container", Pointofsales.MoveTable.container).html(tables);
          
          // Add Background
          if (!!background) {
            $("#tab_contents_container .points-ground", Pointofsales.MoveTable.container)
            .addClass("has-background")
            .css("background", `url(${base_url + background})`);
          }
        });
      });
    },

    onClickTable: function (event) {

      event.preventDefault();

      let 
      table = $(this),
      id = table.data("id"),
      query_string = `target_point_id=${id}`;

      Pointofsales.Helpers.addLoader(Pointofsales.MoveTable.container);

      Pointofsales.Helpers.postForm("#pos", `mergeTables?${query_string}`, function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.MoveTable.container);

        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pleasure.handleToastrSettings('Başarılı', json.message, 'success');

        Pointofsales.setPointId(id);
        location.hash = `point:${id}`;
        Pointofsales.Pos.loadTab();
        
      });
    },
  }
};




Pointofsales.InterimPayments = {

  container: "#tab-interimpayments",

  init: function () {
    Pointofsales.InterimPayments.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-interimpayments']").trigger("click");
    Pointofsales.InterimPayments.loadProducts();
  },

  loadProducts: function () {

    Pointofsales.Helpers.addLoader(Pointofsales.InterimPayments.container);

    Pointofsales.InterimPayments.emptyProducts();

    let 
    unPaid = $(".interim_payments-unpaid > tbody", Pointofsales.InterimPayments.container),
    paid = $(".interim_payments-paid > tbody", Pointofsales.InterimPayments.container);

    unPaid.html("");
    paid.html("");

    Pointofsales.Helpers.postForm("#pos", "getTableWithSale", function (json) {

      Pointofsales.Helpers.removeLoader(Pointofsales.InterimPayments.container);

      if (!!!json.data.point.sale_id) {
        $("a[href='#tab-pos']").trigger("click");
        return;
      }

      $.each(json.data.sale.products, function(index, product) {

        if (product.is_gift === "1")
          return;

        Pointofsales.Helpers.countInterimPayments(product, function (paid_quantity) {
          
        let 
        unpaid_quantity = parseInt(product.quantity) - paid_quantity,
        additional_products = {
          total: 0, 
          titles: ""
        };

        $.each(product.additional_products, function(index1, additional_product) {
          additional_products.total += Pointofsales.Helpers.addTax(additional_product.price, additional_product.tax);
          additional_products.titles += `<footer>${additional_product.title}</footer>`;
        });


        if (unpaid_quantity > 0) {
          product.quantity = unpaid_quantity;
          unPaid.append(Pointofsales.InterimPayments.Templates.unPaidProductRow(product, additional_products));
        }
        if (paid_quantity > 0) {
          product.quantity = paid_quantity;
          paid.append(Pointofsales.InterimPayments.Templates.paidProductRow(product, additional_products));
        }
        
        });
      });

    });
  },

  emptyProducts: function () {
    $('.interim_payments-payable tbody', Pointofsales.InterimPayments.container).html("");
    Pointofsales.InterimPayments.calculate();
  },

  calculate: function () {
    let
    products = $('.interim_payments-payable tbody input[name^="products"]', Pointofsales.InterimPayments.container),
    total_element = $('.interim_payments-payable tfoot .interim_payments-total', Pointofsales.InterimPayments.container),
    info_row = $('.interim_payments-payable tfoot .interim_payments-info', Pointofsales.InterimPayments.container)
    total = 0;

    $.each(products, function(index, product) {
      let
      price = parseFloat($(product).attr("price")),
      quantity = parseFloat($(product).val());

      total += price * quantity;
    });


    if (total) {
      info_row.addClass("hidden");
    } else {
      info_row.removeClass("hidden");
    }

    total_element.html(total.toFixed(2) +" "+ Pointofsales.settings.location.currency);
  },

  Events: {
    init: function () {
      $("body").on("click", `${Pointofsales.InterimPayments.container} .interim_payments-unpaid tbody tr`, Pointofsales.InterimPayments.Events.onClickUnPaidProduct);
      $("body").on("click", `${Pointofsales.InterimPayments.container} .interim_payments-payable tbody tr`, Pointofsales.InterimPayments.Events.onClickPayableProduct);
      $("body").on("click", `${Pointofsales.InterimPayments.container} .interim_payments-submit`, Pointofsales.InterimPayments.Events.onClickCompleteInterimPayment);
    },

    onClickUnPaidProduct: function (event) {
      
      let
      source = $(this),
      source_input = $("input", source),
      source_sale_product_id = source.data("id"),
      source_price = parseFloat(source_input.attr("price")),
      source_quantity = parseInt(source_input.val()),
      target_products = `${Pointofsales.InterimPayments.container} .interim_payments-payable tbody`,
      target_input = $(`input[name="products[${source_sale_product_id}]"]`, target_products),
      target_quantity = 0;

      if (source_quantity < 1)
        return;

      source_input.val(source_quantity - 1);
      if (target_input.length) {
        target_quantity = parseInt(target_input.val());
        target_input.val(target_quantity + 1);
      } else {
        let clone = source.clone();
        $("input", clone).val(1)
        $(target_products).append(clone);
      }

      Pointofsales.InterimPayments.calculate();
    },

    onClickPayableProduct: function (event) {
      
      let
      source = $(this),
      source_input = $("input", source),
      source_sale_product_id = source.data("id"),
      source_price = parseFloat(source_input.attr("price")),
      source_quantity = parseInt(source_input.val()),
      target_products = `${Pointofsales.InterimPayments.container} .interim_payments-unpaid tbody`,
      target_input = $(`input[name="products[${source_sale_product_id}]"]`, target_products),
      target_quantity = parseInt(target_input.val());

      if (source_quantity < 1)
        return;

      source_input.val(source_quantity - 1);
      target_input.val(target_quantity + 1);
      if (source_quantity - 1 < 1) {
        source.remove();
      }

      Pointofsales.InterimPayments.calculate();
    },

    onClickCompleteInterimPayment: function (event) {
      let
      button = $(this),
      payment_type = button.data("payment"),
      form = button.closest("form");

      if (Pointofsales.InterimPayments.Helpers.getInterimPaymentStatus() === true) {
        Pointofsales.InterimPayments.Helpers.setInterimPaymentStatus(false);
      } else {
        return;
      }


      $("input[name=sale_id]", Pointofsales.InterimPayments.container).val(Pointofsales.settings.sale_id);
      $("input[name=point_id]", Pointofsales.InterimPayments.container).val(Pointofsales.getPointId());
      $("input[name=payment_type]", Pointofsales.InterimPayments.container).val(payment_type);

      if (payment_type === "gift") {
        Pointofsales.Helpers.postForm(form, "createGift", function(json) {

          Pointofsales.InterimPayments.Helpers.setInterimPaymentStatus(true);

          if (!json.result) {
            Pleasure.handleToastrSettings('Hata!', json.message, 'error');
            return;
          }

          Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
          Pointofsales.Pos.loadTab();

        }, "json");
        return;
      }

      Pointofsales.Helpers.postForm(form, "completeInterimPayment", function(json) {

        Pointofsales.InterimPayments.Helpers.setInterimPaymentStatus(true);

        if (!json.result) {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
          return;
        }

        Pleasure.handleToastrSettings('Başarılı', json.message, 'success');
        Pointofsales.Pos.loadTab();

      }, "json");
    },
  },

  Templates: {
    unPaidProductRow: function (product, additional_products) {
      let 
      price = Pointofsales.Helpers.addTax(product.price, product.tax);
      price += additional_products.total;

      return `
        <tr data-id="${product.id}">
          <td>${product.title} <blockquote class="p-a-0 p-l-1">${additional_products.titles}</blockquote></td>
            <td class="quantity">
              <input type="text" name="products[${product.id}]" value="${product.quantity}" price="${price}" readonly>
            </td>
            <td class="total p-r-2 text-right">${price.toFixed(2)} ${Pointofsales.settings.location.currency}</td>
        </tr>
      `;
    },

    paidProductRow: function (product, additional_products) {
      let 
      price = Pointofsales.Helpers.addTax(product.price, product.tax);
      price += additional_products.total;

      return `
        <tr>
          <td>${product.title} <blockquote class="p-a-0 p-l-1">${additional_products.titles}</blockquote></td>
            <td class="quantity">
              ${product.quantity}
            </td>
            <td class="total p-r-2 text-right">${price.toFixed(2)} ${Pointofsales.settings.location.currency}</td>
        </tr>
      `;
    },
  },

  Helpers: {

    interimPaymentStatus: true,
    getInterimPaymentStatus: function (status) {
      return Pointofsales.InterimPayments.Helpers.interimPaymentStatus;
    },

    setInterimPaymentStatus: function (status) {
      status = (status === true ? true : false);
      Pointofsales.InterimPayments.Helpers.interimPaymentStatus = status;
    },
  }
};




Pointofsales.Invoice = {

  container: "#tab-invoice",
  container_adisyon: "#tab-adisyon",
  container_return: "#tab-returninvoice",

  init: function () {
    Pointofsales.Invoice.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-invoice']").trigger("click");
  },

  loadReturnTab: function () {
    $("a[href='#tab-returninvoice']").trigger("click");
  },

  fill: function (sale) {

    sale.subtotal = parseFloat(sale.subtotal);
    sale.amount = parseFloat(sale.amount);
    sale.taxtotal = parseFloat(sale.taxtotal);
    sale.discount_amount = parseFloat(sale.discount_amount);
    //sale.discount_type = (sale.discount_type === "amount" ? Pointofsales.settings.location.currency : "%");

    $(".invoice-no", Pointofsales.Invoice.container).html(sale.id);
    $(".invoice-datetime", Pointofsales.Invoice.container).html(sale.payment_date_client);
    $(".invoice-subtotal", Pointofsales.Invoice.container).html(sale.subtotal.toFixed(2));
    $(".invoice-amount", Pointofsales.Invoice.container).html(sale.amount.toFixed(2));
    $(".invoice-taxtotal", Pointofsales.Invoice.container).html(sale.taxtotal.toFixed(2));
    $(".invoice-discounttotal", Pointofsales.Invoice.container).html(
      Pointofsales.Discount.displayDiscount(sale.discount_amount, sale.discount_type)
    );

    sale.customer.name = sale.customer.name || "";
    sale.customer.phone = sale.customer.phone || "";
    sale.customer.address = sale.customer.address || "";
    $(".invoice-customer_name", Pointofsales.Invoice.container).html(sale.customer.name);
    $(".invoice-customer_phone", Pointofsales.Invoice.container).html(sale.customer.phone);
    $(".invoice-customer_address", Pointofsales.Invoice.container).html(sale.customer.address);

    $(".invoice-body tbody", Pointofsales.Invoice.container).html("");
    $.each(sale.products, function(index, product) {
      Pointofsales.Invoice.Templates.productRowInvoice(product, function (row) {
        $(".invoice-body tbody", Pointofsales.Invoice.container).append(row);
      });
    });
  },

  fillAdisyon: function (sale) {

    sale.subtotal = parseFloat(sale.subtotal);
    sale.amount = parseFloat(sale.amount);
    sale.taxtotal = parseFloat(sale.taxtotal);
    sale.discount_amount = parseFloat(sale.discount_amount);
    sale.interim_payment_amount = parseFloat(sale.interim_payment_amount);
    //sale.discount_type = (sale.discount_type === "amount" ? Pointofsales.settings.location.currency : "%");

    $(".invoice-no", Pointofsales.Invoice.container_adisyon).html(sale.id);
    $(".invoice-datetime", Pointofsales.Invoice.container_adisyon).html(sale.payment_date_client);
    $(".invoice-subtotal", Pointofsales.Invoice.container_adisyon).html(sale.subtotal.toFixed(2));
    $(".invoice-amount", Pointofsales.Invoice.container_adisyon).html(sale.amount.toFixed(2));
    //$(".invoice-taxtotal", Pointofsales.Invoice.container_adisyon).html(sale.taxtotal.toFixed(2));
    $(".invoice-interim-payment", Pointofsales.Invoice.container_adisyon).html(sale.interim_payment_amount);
    $(".invoice-discounttotal", Pointofsales.Invoice.container_adisyon).html(
      Pointofsales.Discount.displayDiscount(sale.discount_amount, sale.discount_type)
    );
    $(".invoice-table-name", Pointofsales.Invoice.container_adisyon).html(sale.point_title);
    $(".invoice-zone-name", Pointofsales.Invoice.container_adisyon).html(sale.zone_title);


    sale.customer.name = sale.customer.name || "";
    sale.customer.phone = sale.customer.phone || "";
    sale.customer.address = sale.customer.address || "";
    $(".invoice-customer_name", Pointofsales.Invoice.container_adisyon).html(sale.customer.name);
    $(".invoice-customer_phone", Pointofsales.Invoice.container_adisyon).html(sale.customer.phone);
    $(".invoice-customer_address", Pointofsales.Invoice.container_adisyon).html(sale.customer.address);
    

    $(".invoice-body tbody", Pointofsales.Invoice.container_adisyon).html("");
    $.each(sale.products, function(index, product) {
      Pointofsales.Invoice.Templates.productRow(product, function (row) {
        $(".invoice-body tbody", Pointofsales.Invoice.container_adisyon).append(row);
      });
    });
  },

  fillReturn: function (sale) {

    sale.subtotal = parseFloat(sale.subtotal);
    sale.amount = parseFloat(sale.amount);
    sale.taxtotal = parseFloat(sale.taxtotal);
    sale.discount_amount = parseFloat(sale.discount_amount);
    //sale.discount_type = (sale.discount_type === "amount" ? Pointofsales.settings.location.currency : "%");

    $(".invoice-no", Pointofsales.Invoice.container_return).html(sale.id);
    $(".invoice-datetime", Pointofsales.Invoice.container_return).html(sale.payment_date_client);
    $(".invoice-subtotal", Pointofsales.Invoice.container_return).html(sale.subtotal.toFixed(2));
    $(".invoice-amount", Pointofsales.Invoice.container_return).html(sale.amount.toFixed(2));
    $(".invoice-taxtotal", Pointofsales.Invoice.container_return).html(sale.taxtotal.toFixed(2));
    $(".invoice-discounttotal", Pointofsales.Invoice.container_return).html(
      Pointofsales.Discount.displayDiscount(sale.discount_amount, sale.discount_type)
    );
    

    $(".invoice-body tbody", Pointofsales.Invoice.container_return).html("");
    $.each(sale.products, function(index, product) {
      Pointofsales.Invoice.Templates.productRow(product, function (row) {
        $(".invoice-body tbody", Pointofsales.Invoice.container_return).append(row);
      });
    });
  },

  handleAdisyon: function () {
    Pointofsales.Helpers.addLoader(Pointofsales.Pos.container);

    Pointofsales.Helpers.postForm("#pos", "getTableWithSale", function (json) {
      Pointofsales.Helpers.removeLoader(Pointofsales.Pos.container);

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      if (json.data.point.sale_id) {
        json.data.sale.payment_date_client = Pointofsales.Helpers.getDateNow();
        Pointofsales.Invoice.fillAdisyon(json.data.sale);
        $(`${Pointofsales.Invoice.container_adisyon} .invoice-print`).trigger("click");
      }
      
    });
  },

  Events: {
    init: function () {
      $("body").on("click", `${Pointofsales.Invoice.container} .invoice-print`, Pointofsales.Invoice.Events.onClickPrint);

      // Adisyon
      $("body").on("click", "#handle-adisyon", Pointofsales.Invoice.handleAdisyon);
      $("body").on("click", `${Pointofsales.Invoice.container_adisyon} .invoice-print`, Pointofsales.Invoice.Events.onClickPrintAdisyon);

      // Return
      $("body").on("click", `${Pointofsales.Invoice.container_return} .invoice-print`, Pointofsales.Invoice.Events.onClickPrintReturnInvoice);
    },

    onClickPrint: function (event) {
      event.preventDefault();

      Pointofsales.Helpers.doPrintAction(
        $(`${Pointofsales.Invoice.container} .invoice`));

    },

    onClickPrintAdisyon: function () {

      Pointofsales.Helpers.doPrintAction(
        $(`${Pointofsales.Invoice.container_adisyon} .invoice`));
    },

    onClickPrintReturnInvoice: function () {

      Pointofsales.Helpers.doPrintAction(
        $(`${Pointofsales.Invoice.container_return} .invoice`));
    },
  },

  Templates: {
    productRow: function (product, callback) {

      let 
      quantity = parseInt(product.quantity),
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, product.tax),
      total = price * quantity,
      row,
      gift_label = (product.is_gift === "1" ? '<span class="label label-info">İKRAM</span>' : "");

      if (quantity === 0) {
        callback("");
        return;
      }

      row = $(`
      <tr>
          <td>
              ${product.title} ${gift_label}
              <p class="invoice-additional_title"></p>
          </td>
          <td style="vertical-align: top !important;" class="text-center">${quantity}</td>
          <td>
            ${price.toFixed(2)}
            <p class="invoice-additional_price"></p>
          </td>
          <td class="text-right">
            ${total.toFixed(2)}
            <p class="invoice-additional_total"></p>
          </td>
      </tr>
      `);

      $.each(product.additional_products, function(index, additional_product) {
        let 
        additional_product_tax = parseFloat(additional_product.tax),
        additional_product_price = Pointofsales.Helpers.addTax(additional_product.price, additional_product.tax),
        additional_product_total = additional_product_price * quantity;

        $(".invoice-additional_title", row).append(additional_product.title + "<br>");
        $(".invoice-additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
        $(".invoice-additional_total", row).append(additional_product_total.toFixed(2) + "<br>");
      });

      callback(row);
    },
    productRowInvoice: function (product, callback) {

      let 
      quantity = parseInt(product.quantity),
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, product.tax),
      product_tax = Pointofsales.Helpers.calcTax(product.price, product.tax),
      total = price * quantity,
      row,
      gift_label = (product.is_gift === "1" ? '<span class="label label-info">İKRAM</span>' : "");

      if (quantity === 0) {
        callback("");
        return;
      }

      row = $(`
      <tr>
          <td>
              ${product.title} ${gift_label}
              <p class="invoice-additional_title"></p>
          </td>
          <td>${quantity}</td>
          <td>
            ${product_tax.toFixed(2)}
            <p class="invoice-additional_tax"></p>
          </td>
          <td>
            ${price.toFixed(2)}
            <p class="invoice-additional_price"></p>
          </td>
          <td class="text-right">
            ${total.toFixed(2)}
            <p class="invoice-additional_total"></p>
          </td>
      </tr>
      `);

      $.each(product.additional_products, function(index, additional_product) {
        let 
        additional_product_tax = parseFloat(additional_product.tax),
        additional_product_price = Pointofsales.Helpers.addTax(additional_product.price, additional_product.tax),
        additional_product_tax_price = Pointofsales.Helpers.calcTax(additional_product.price, additional_product.tax),
        additional_product_total = additional_product_price * quantity;

        $(".invoice-additional_title", row).append(additional_product.title + "<br>");
        $(".invoice-additional_tax", row).append(additional_product_tax_price.toFixed(2) + "<br>");
        $(".invoice-additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
        $(".invoice-additional_total", row).append(additional_product_total.toFixed(2) + "<br>");
      });

      callback(row);
    },
  },

};




Pointofsales.ProductNotes = {

  modal: "#modal-product_note",
  timer: null,

  init: function () {
    Pointofsales.SoundRecorder.init();

    Pointofsales.ProductNotes.Events.init();
  },

  loadModal: function () {
    Pointofsales.ProductNotes.emptyModal();
    $(Pointofsales.ProductNotes.modal).modal("toggle");
  },

  emptyModal: function () {

    $(".timer", Pointofsales.ProductNotes.modal).html("00:00:00");
    $("textarea[name='note']", Pointofsales.ProductNotes.modal).val("");
  },

  startTimer: function () {
    let second = 1;
    Pointofsales.ProductNotes.timer = setInterval(function () {
      $(".timer", Pointofsales.ProductNotes.modal).html(
        Pointofsales.Helpers.secondsToHms(second++)
      );
    }, 1000);
  },

  stopTimer: function () {
    clearInterval(Pointofsales.ProductNotes.timer);
  },

  Events: {
    init: function () {
      // Sound Note
      $("body").on("click", "#create-soundnote", Pointofsales.ProductNotes.Events.onClickCreateSoundNote);
      $("body").on("click", "#notesound .save", Pointofsales.ProductNotes.Events.onClickSaveSoundNote);
      $("body").on("click", "#notesound .cancel", Pointofsales.ProductNotes.Events.onClickCancelSoundNote);

      // Text Note
      $("body").on("click", "#create-textnote", Pointofsales.ProductNotes.Events.onClickCreateTextNote);
      $("body").on("click", "#notetext .save", Pointofsales.ProductNotes.Events.onClickSaveTextNote);
      $("body").on("click", "#notetext .cancel", Pointofsales.ProductNotes.Events.onClickCancelTextNote);
      $("body").on("click", "#modal-product_content .addtoquickNotes", Pointofsales.ProductNotes.Events.onClickAddQuickNotes);
      $("body").on("click", "#modal-product_content .plusQuickNote, #modal-product_content .minusQuickNote", Pointofsales.ProductNotes.Events.onClickMinusPlusQuickNote);

      $("body").on("click", "#modal-product_content #product_notes .remove", Pointofsales.ProductNotes.Events.onClickRemoveNote);

      
      $('#modal-product_content').on('hidden.bs.modal', Pointofsales.ProductNotes.Events.onCloseProductModal);

    },

    onClickCreateSoundNote: function (event) {
      event.preventDefault();
      Pointofsales.ProductNotes.loadModal();
      $("a[href='#notesound']", Pointofsales.ProductNotes.modal).click();

      Pointofsales.ProductNotes.startTimer();
      Pointofsales.SoundRecorder.start();
    },

    onClickCreateTextNote: function (event) {
      event.preventDefault();
      Pointofsales.ProductNotes.loadModal();
      $("a[href='#notetext']", Pointofsales.ProductNotes.modal).click();
    },

    onClickRemoveNote: function (event) {
      event.preventDefault();
      let
      button = $(this),
      container = button.closest(".list-group-item");

      container.fadeOut(300, function() {
        container.remove();
      });
    },

    onClickSaveSoundNote: function (event) {
      event.preventDefault();

      Pointofsales.Helpers.addLoader($(".modal-content", Pointofsales.ProductNotes.modal));
      Pointofsales.ProductNotes.stopTimer();

      Pointofsales.SoundRecorder.stop(function (blob) {
        Pointofsales.SoundRecorder.save(blob, base_url+"pointofsales/ajax/saveSoundNote", function (json) {
          
          Pointofsales.Helpers.removeLoader($(".modal-content", Pointofsales.ProductNotes.modal));
          $(Pointofsales.ProductNotes.modal).modal("toggle");

          if (!json.result) {
            Pleasure.handleToastrSettings("Hata!", json.message, "error");
            return;
          }

          json.data.sound_note.sound = base_url + json.data.sound_note.sound;

          Pointofsales.ProductNotes.Templates.newNote(Pointofsales.ProductNotes.Templates.newSoundNote(json.data.sound_note));

        });

      });
    },

    onClickCancelSoundNote: function (event) {
      event.preventDefault();
      Pointofsales.ProductNotes.stopTimer();
      $(Pointofsales.ProductNotes.modal).modal("toggle");

      Pointofsales.SoundRecorder.stop();
    },

    onCloseProductModal: function() {
      $("#new_order_sound").remove();
    },


    onClickSaveTextNote: function (event) {
      event.preventDefault();
      $(Pointofsales.ProductNotes.modal).modal("toggle");

      let
      note = $("textarea[name='note']", Pointofsales.ProductNotes.modal).val();
      note = $.trim(note);

      if (note.length > 0) {
        note = Pointofsales.ProductNotes.Templates.newTextNote(note);
        Pointofsales.ProductNotes.Templates.newNote(note);
      }
    },

    onClickCancelTextNote: function (event) {
      event.preventDefault();
      $(Pointofsales.ProductNotes.modal).modal("toggle");
    },

    onClickAddQuickNotes: function () {

      let button = $(this);
      let note = button.val();
      note = $.trim(note);

      if (note.length > 0) {
        note = Pointofsales.ProductNotes.Templates.newDynamicTextNote(note);
        Pointofsales.ProductNotes.Templates.newNote(note);
      }
    },

    onClickMinusPlusQuickNote: function () {
      let button = $(this),
          container = button.closest('.quickNote'),
          quickNoteText = $('.quickNoteText', container),
          quickNoteInput = $("input[name='[text_notes][]']", container),
          quantity = container.data('quantity'),
          note = container.data('note'),
          isPlus = button.hasClass('plusQuickNote');
          
          if (isPlus) {
            quantity++;
          } else {
            quantity--;
          }

          if (quantity <= 0) {
            return;
          }

          quickNoteText.html(quantity + " " + note);
          quickNoteInput.val(quantity + " " + note);
          container.data('quantity', quantity);
    },

  },

  Helpers: {
  },

  Templates: {
    newSoundNote: function (sound) {
      return `
        <audio controls style="width: 100%;">
          <source src="${sound.sound}" type="audio/mp3">
        </audio>
        <input type="hidden" name="[sound_notes][]" value="${sound.id}" data-url="${sound.sound}">
      `;
    },

    newTextNote: function (note) {
      return `
        <i class="small">${note}</i>
        <input type="hidden" name="[text_notes][]" value="${note}">
      `;
    },

    newDynamicTextNote: function (note) {
      return `
        <div class="row quickNote" data-quantity="1" data-note="${note}">
          <div class="col-xs-8">
            <i class="small quickNoteText">1 ${note}</i>
            <input type="hidden" name="[text_notes][]" value="1 ${note}">
          </div>
          <div class="col-xs-2">
            <div class="input-group">
              <span class="input-group-btn">
                  <button type="button" class="btn btn-ripple btn-success btn-xl plusQuickNote">+</button>
                  <button type="button" class="btn btn-ripple btn-danger btn-xl minusQuickNote">-</button>
              </span>
            </div>
          </div>
        </div>
      `;
    },

    newNote: function (content) {
      $("#modal-product_content #product_notes").append(`
        <div class="list-group-item">
          <div class="row">
            <div class="col-xs-10">
              ${content}
            </div>
            <div class="col-xs-2 text-right">
              <a href="#" class="btn btn-danger remove">
                <i class="fa fa-trash"></i>
              </a>
            </div>
          </div>
        </div>
      `);
    },
  }
};




Pointofsales.Discount = {

  modal: "#modal-discount_login",

  init: function () {
    Pointofsales.Discount.Events.init();
  },

  loadModal: function () {
    Pointofsales.Discount.emptyModal();

    $(Pointofsales.Discount.modal).modal("toggle");
  },

  displayDiscount: function (amount, type) {
    let prefix = "";

    amount = parseFloat(amount) || 0;
    if (amount) {
      type = (type === "percent" ? " %" : " " + Pointofsales.settings.location.currency);
      prefix = "-";
    } else {
      type = "";
    }

    return type +" "+ prefix + amount.toFixed(2);
  },

  emptyModal: function () {
    $("input[name='discount_amount'], input[name='username'], input[name='password']", Pointofsales.Discount.modal).val("");
  },

  Events: {
    init: function () {
      $("body").on("click", "#add-discount", Pointofsales.Discount.Events.onClickAdd);
      $("body").on("click", "#submit-discount-login", Pointofsales.Discount.Events.onClickSubmit);
      $("body").on("click", ".percent-buttons", Pointofsales.Discount.modal, Pointofsales.Discount.Events.onClickRadioButton);
    },

    onClickAdd: function (event) {
      event.preventDefault();

      Pointofsales.Discount.loadModal();

      $('.percent-buttons.first', Pointofsales.Discount.modal).trigger('click');

    },

    onClickSubmit: function (event) {
      Pointofsales.Helpers.addLoader($(".modal-content", Pointofsales.Discount.modal));
      Pointofsales.Helpers.postForm($("form", Pointofsales.Discount.modal), "loginForDiscountKey", function (json) {
        Pointofsales.Helpers.removeLoader($(".modal-content", Pointofsales.Discount.modal));

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        $("input[name='discount_key']", Pointofsales.Pos.container).val(json.data.discount_key);

        Pointofsales.settings.sale.discount_amount = parseFloat(json.data.discount_amount);
        Pointofsales.settings.sale.discount_type = json.data.discount_type;

        Pointofsales.Pos.calculate();

        Pleasure.handleToastrSettings("Başarılı", json.message, "success");
        $(Pointofsales.Discount.modal).modal("toggle");
      });
    },

    onClickRadioButton: function () {

      let percentValue = $(this).text();

      $('input[name="discount_amount"]', Pointofsales.Discount.modal).val(percentValue);
    }
  }
};




Pointofsales.Setup = {

  container: "#pos-setup",
  elements: {},

  settings: {
    pos_points: {}
  },

  init: function (settings) {

    if (typeof settings !== "undefined") {
      $.extend(Pointofsales.Setup.settings, settings);
    }

    Pointofsales.Setup.elements = {
      submit_setup: `${Pointofsales.Setup.container} #submit-setup`,
      select_location_id: `${Pointofsales.Setup.container} select[name='location_id']`,
      select_pos_id: `${Pointofsales.Setup.container} select[name='pos_id']`,
    };

    Pointofsales.JSDesktopApp.init();
    if (Pointofsales.JSDesktopApp.status) {
      Pointofsales.JSDesktopApp.fillProductionsPrinters();
    } else {
      Pointofsales.JSDesktopApp.hideList();
    }

    Pointofsales.Setup.Events.init();

    let 
    option_location = $(`${Pointofsales.Setup.container} select[name='location_id']`)
    option_pos = $(`${Pointofsales.Setup.container} select[name='pos_id']`)
    option_location.val(1);
    option_location.trigger("change");
    option_pos.val(1);
    option_pos.trigger("change");

  },

  fillPosPoints: function (location_id) {
    location_id = Number(location_id);

    $(`${Pointofsales.Setup.elements.select_pos_id} option:not(:first)`).remove();

    $.each(Pointofsales.Setup.settings.pos_points, function(index, pos) {
      if (Number(pos.location_id) === location_id) {
        $(Pointofsales.Setup.elements.select_pos_id).append($("<option>").val(pos.id).html(pos.title));
      }
    });
  },

  Events: {
    init: function () {
      $("body").on("click", Pointofsales.Setup.elements.submit_setup, Pointofsales.Setup.Events.onClickSubmit);
      $("body").on("change", Pointofsales.Setup.elements.select_location_id, Pointofsales.Setup.Events.onChangeLocation);
    },

    onClickSubmit: function (event) {

      Pointofsales.Helpers.addLoader(Pointofsales.Setup.container);
      Pointofsales.Helpers.postForm($("form", Pointofsales.Setup.container), "loginForSetup", function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.Setup.container);
        
        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        if (Pointofsales.JSDesktopApp.status) {
          try {
            Pointofsales.JSDesktopApp.assignProductionsAndPrintersForAutoPrint();
          } catch (e) {
            alert(e);
          }
        }

        let device = json.data.device;

        if (device === "garson") {
          location.href = base_url + "mobilpos";
        } else if (device === "tablet" ) {
          location.href = base_url + "mobilpos/tablet";
        } else {
          location.href = base_url + "pointofsales";
        }

      });      
    },

    onChangeLocation: function () {
      let
      location_id = $(this).val();

      Pointofsales.Setup.fillPosPoints(location_id);
    },

    onClickRemovePosSetup: function (event) {
      event.preventDefault();

      Pointofsales.Helpers.addLoader(Pointofsales.Pos.container);
      Pointofsales.Helpers.postForm(null, "removePosSetup", function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.Pos.container);
        
        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }


        location.href = base_url + "pointofsales";
      });
    },
  }
};





Pointofsales.PackageServiceOrders = {

  container: "#tab-packageserviceorders",
  modal_shipping_users: "#modal-shipping_users",

  init: function () {

    setInterval(
      Pointofsales.PackageServiceOrders.getOrders, 3000);

    Pointofsales.PackageServiceOrders.Events.init();
  },

  loadTab: function () {
    if (!Pointofsales.settings.package_service) {
      return;
    }

    $("a[href='#tab-packageserviceorders']").trigger("click");
  },

  emptyOrders: function() {
    $("#orders-pending .orders").html("");
    $("#orders-shipped .orders").html("");
  },

  getOrders: function () {

    if (!Pointofsales.settings.package_service) {
      Pointofsales.PackageServiceOrders.emptyOrders();
      return;
    }
    
    Pointofsales.Helpers.postForm(null, "getPackageServiceOrders", function (json) {

      if (!json.result) {
        Pointofsales.PackageServiceOrders.emptyOrders();
        return;
      } else if (json.data.sales.length === 0) {
        Pointofsales.PackageServiceOrders.emptyOrders();
      }

      // Append
      $.each(json.data.sales, function(index, sale) {
        Pointofsales.PackageServiceOrders.Templates.orderRow(sale, function (row) {
          if (sale.status === "pending") {
            if ($(`#orders-pending .orders .order[data-sale_id="${sale.id}"]`).length === 0) {
              $("#orders-pending .orders").append(row);
            }
          } else if (sale.status === "shipped") {
            if ($(`#orders-shipped .orders .order[data-sale_id="${sale.id}"]`).length === 0) {
              $("#orders-shipped .orders").append(row);
            } else {
              $(`#orders-shipped .orders .order[data-sale_id="${sale.id}"] .courier_name`).html(sale.courier_name + " / ");
            }
          }
        });
      });

      let offset = $('#orders-pending .orders').offset();
      let package_height = $(window).height() - offset.top -20;

      $('#orders-pending .orders').css("max-height", package_height + "px");
      $('#orders-pending .orders').css("overflow-y", "auto");


      // Remove
      $.each($("#orders-pending .orders .order"), function(index, row) {
        let hasSale = false;
        $.each(json.data.sales, function(index, sale) {
          if (sale.id == $(row).data("sale_id")) {
            hasSale = true;
            return;
          }
        });

        if (!hasSale) {
          $(row).remove();
        }
      });

      // Remove
      $.each($("#orders-shipped .orders .order"), function(index, row) {
        let hasSale = false;
        $.each(json.data.sales, function(index, sale) {
          if (sale.id == $(row).data("sale_id")) {
            hasSale = true;
            return;
          }
        });

        if (!hasSale) {
          $(row).remove();
        }
      });
    });
  },

  Events: {

    selectedOrderRow: null,

    init: function () {
      $("body").on("click", "#orders-shipped .view, #orders-pending .view", Pointofsales.PackageServiceOrders.Events.onClickViewOrder);
      $("body").on("click", "#orders-pending .ready", Pointofsales.PackageServiceOrders.Events.onClickReadyOrder);
      $("body").on("click", `${Pointofsales.PackageServiceOrders.modal_shipping_users} .courier`, Pointofsales.PackageServiceOrders.Events.onClickCourier);
    },

    onClickReadyOrder: function (event) {
      event.preventDefault();

      let
      row = $(this).closest(".order"),
      point_id = row.data("point_id");

      Pointofsales.PackageServiceOrders.selectedOrderRow = row;

      $("input[name='point_id']", Pointofsales.PackageServiceOrders.modal_shipping_users).val(point_id);

      $("input[type=radio]", Pointofsales.PackageServiceOrders.modal_shipping_users).prop("checked", false);

      $(Pointofsales.PackageServiceOrders.modal_shipping_users).modal("toggle");
    },

    onClickCourier: function (event) {

      $("input[type=radio]", this).prop("checked", true);

      Pointofsales.Helpers.postForm($("form", Pointofsales.PackageServiceOrders.modal_shipping_users), "shippingOrder", function (json) {
        
        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        Pointofsales.PackageServiceOrders.getOrders();

        $(Pointofsales.PackageServiceOrders.modal_shipping_users).modal("toggle");

        Pointofsales.PackageServiceOrders.selectedOrderRow.remove();

      });
    },

    onClickViewOrder: function (event) {
      event.preventDefault();

      let
      point_id = $(this).closest(".order").data("point_id");

      Pointofsales.setPointId(point_id);
      location.hash = `point:${point_id}`;
      Pointofsales.Pos.loadTab();
    },
  },

  Templates: {
    orderRow: function (sale, callback) {
      let
      _customer = (sale.customer).split("{SEPERATOR}"),
      customer = {
        name: _customer[0],
        phone: _customer[1],
        address: _customer[2],
      },
      amount = Number(sale.amount),
      row = $(`
        <div class="col-xs-12 p-a-1 m-b-1 small order" data-point_id="${sale.point_id}" data-sale_id="${sale.id}">
          <div class="pull-right title"><span class="courier_name"> ${sale.courier_name} / </span>${amount.toFixed(2)}${Pointofsales.settings.location.currency}</div>
          <div class="cols-xs-6">${customer.name}</div>
          <div class="cols-xs-6">${customer.phone}</div>
          <div class="cols-xs-12">${customer.address}</div>
          <div class="btn-group btn-group-justified p-t-1">
            <a href="#" class="btn btn-orange btn-ripple btn-xl view"><i class="fa fa-search"></i> GÖSTER</a>
            <a href="#" class="btn btn-success btn-ripple btn-xl ready"><i class="fa fa-check"></i> HAZIR</a>
          </div>
        </div>
      `);

      callback(row);
    },
  }
};


Pointofsales.YemekSepeti = {

  //order_ids: [],

  container: "#tab-packageserviceorders",
  reject_modal: "#modal_ys_reject_note",
  detail_modal: "#modal_ys_detail",

  init: function () {

    setInterval(Pointofsales.YemekSepeti.getOrders,3000);
    Pointofsales.YemekSepeti.Events.init();

  },

  getOrders: function () {
    Pointofsales.Helpers.postForm(null, "getYemekSepetiOrders", function (json) {

      let 
      items = json.data.sales_ys.items,
      complete_ys_sales = json.data.complete_ys_sales || [];

      $('#orders-ys .orders-ys').html("");

      $.each(items, function(index, val) {

        let exist = false;

        $.each(complete_ys_sales, function(index, ys_order) {
           if (val['@attributes'].Id === ys_order.ys_order_id) {
              exist = true;
           }
        });

        if (!exist) {
          Pointofsales.YemekSepeti.Templates.orderRow(val['@attributes'], function(row) {
            $('.detail', row).attr("data-items", JSON.stringify(val['@attributes']));
            $('.detail', row).attr("data-products", JSON.stringify(val['product']));
            $('#orders-ys .orders-ys').append(row);
          });
        }

      });

    });

  },

  Events: {

    init: function () {
      $("body").on("click", "#orders-ys .reject", Pointofsales.YemekSepeti.Events.onClickReject);
      $("body").on("click", "#orders-ys .detail", Pointofsales.YemekSepeti.Events.onClickDetail);
      $("body").on("click", `${Pointofsales.YemekSepeti.detail_modal} .check`, Pointofsales.YemekSepeti.Events.onClickCheck);
    },

    onClickReject: function () {
      let reject_modal = $(Pointofsales.YemekSepeti.reject_modal);

      reject_modal.modal('show');

      $('#text-note', reject_modal).val("");
      $('#hiddenInput input[name=order_id]', reject_modal).val("");

      let
      _this = $(this),
      row = _this.closest('div'),
      order_id = row.data("order-id");


      $('#hiddenInput input[name=order_id]', reject_modal).val(order_id);

      $('#checkNote',reject_modal).on('click', function() {

        Pointofsales.Helpers.postForm("#reject-form", "rejectYsOrders", function (json) {

          if (!json.result) {
            Pleasure.handleToastrSettings("Hata!", json.message, "error");
            return;
          }

          Pleasure.handleToastrSettings("Başarılı!", json.message, "success");

          reject_modal.modal('hide');

        });

      });

    },

    onClickDetail: function () {
      let detail_modal = $(Pointofsales.YemekSepeti.detail_modal);

      detail_modal.modal('show');

      //hidden inputların temizlenmesi
      $('#hiddenInput', detail_modal).html("");

      let 
      items = $(this).data("items"),
      products = $(this).data("products");

      ys_products = JSON.stringify(products);

       //Details modalının doldurulması
      $('.customer-name', detail_modal).text(items.CustomerName);
      $('.delivery-time', detail_modal).text(items.DeliveryTime);
      $('.customer-address', detail_modal).text(items.Address + `  - Tel : ${items.CustomerPhone}  -  Tel 2 : ${items.CustomerPhone2}`);
      $('.customer-address-desc', detail_modal).text(items.AddressDescription);
      $('.payment-note', detail_modal).text(items.PaymentNote);
      $('.order-note', detail_modal).text(items.OrderNote);
      $('.order-total', detail_modal).text(items.OrderTotal);

      //hidden inputların forma basılması
      // $('input[name="order_id"]',detail_modal).val(items.Id);
      // $('input[name="customer_id"]',detail_modal).val(items.CustomerId);
      // $('input[name="name"]',detail_modal).val(items.CustomerName);
      // $('input[name="phone"]',detail_modal).val(items.CustomerPhone);
      // $('input[name="address"]',detail_modal).val(items.Address + ` - Adres Tarifi : ${items.AddressDescription}`);

      // //hidden inputların forma basılması
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="order_id" value="${items.Id}">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="customer_id" value="${items.CustomerId}">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="name" value="${items.CustomerName}">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="phone" value="${items.CustomerPhone}">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="address" value="${items.Address}">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="point_id" value="">`);
      $('#hiddenInput', detail_modal).append(`<input type="hidden" name="customer_id" value="">`);

      $.post(base_url+'pointofsales/ajax/getYsOrdersProducts', {products: ys_products}, 

      function(json) {

       let sync_products = json.data.sync_products;

       $('#tblYsProduct tbody').html("");
       $('.unsync span').html("");

       $.each(sync_products, function(index_key, sync_product) {

        let 
        stocks = "",
        additionals = "",
        portions = "",
        unsynchronized = "",
        hidden_items = "";

        //Stokların toplanıp, gösterilmesi
        if (sync_product.stocks) {
          $.each(sync_product.stocks, function(index, stock) {

            if (index !== sync_product.stocks.length -1) {
              stocks += stock.Name+",";
              hidden_items += `<input type="hidden" name="products[${index_key}][stocks][${stock.sys_stock_id}]" value="${stock.sys_stock_id}">`;
            } else {
              stocks += stock.Name;
              hidden_items += `<input type="hidden" name="products[${index_key}][stocks][${stock.sys_stock_id}]" value="${stock.sys_stock_id}">`;
            }

          });
        }

        hidden_items += `<input type="hidden" name="products[${index_key}][quantity]" value="${sync_product.Quantity}">`;

        if (sync_product.additionals) {
          $.each(sync_product.additionals, function(index, additional) {
            if (index !== sync_product.additionals.length -1) {

              additionals += additional.Name+",";
              hidden_items += `<input type="hidden" name="products[${index_key}][additional_products][${additional.sys_additional_id}]" value="${additional.sys_additional_id}">`;

            } else {

              additionals += additional.Name;
              hidden_items += `<input type="hidden" name="products[${index_key}][additional_products][${additional.sys_additional_id}]" value="${additional.sys_additional_id}">`;

            }
          });
        }

        //Senkronize edilemeyen ürünlerin gösterilmesi
        if (sync_product.unsynchronized) {
          $.each(sync_product.unsynchronized, function(index, unsync) {

            if (unsync.Name !== "1 Porsiyon") {

              if (index !== sync_product.unsynchronized.length -1) {            
                unsynchronized += unsync.Name+",";
              } else {
                unsynchronized += unsync.Name;
              }

            }

          });
        }

        if (sync_product.portions) {
          $.each(sync_product.portions, function(index, portion) {
            hidden_items += `<input type="hidden" name="products[${index_key}][product_id]" value="${portion.sys_portion_id}">`;
            portions += portion.Name;
          });
        } else {
          hidden_items += `<input type="hidden" name="products[${index_key}][product_id]" value="${sync_product.sys_id}">`;
        }

          $('#tblYsProduct tbody').append(`
            <tr>
              <td>${sync_product.Name} ${portions} </td>
              <td class="quantity">${sync_product.Quantity}</td>
              <td class="stocks">${stocks}</td>
              <td class="additonal">${additionals}</td>
              <td>${sync_product.Price}₺</td>
              <td class="hidden">${hidden_items}</td>
            </tr>
          `);

          $('.unsync span').append(`${unsynchronized}`);
       });
        
      });
      
    
    },

    onClickCheck: function () {

      Pointofsales.YemekSepeti.Events.getCustomerAndPointId();
      
    },

    getCustomerAndPointId: function () {

      Pointofsales.Helpers.postForm("#ys-order", `getSaleYsProduct`, function (json) {
        $('#hiddenInput input[name="point_id"]').val(json.data.point_id);
        $('#hiddenInput input[name="customer_id"]').val(json.data.customer_id);
        Pointofsales.YemekSepeti.Events.ysSaleComplete();
        
      });

    },

    ysSaleComplete: function() {

      Pointofsales.Helpers.postForm("#ys-order", `complete?action=order`, function (json) {
        if (json.result) {

          Pointofsales.Helpers.postForm("#ys-order", `acceptYsOrders?sale_id=${json.data.sale_id}`, function (json) {

            if (!json.result) {
              Pleasure.handleToastrSettings("Hata!", json.message, "error");
              return;
            }

              Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
              $('#modal_ys_detail').modal('toggle');

            });

        } else {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }
        
      });

    }

  },

  Templates: {
    orderRow: function (items, callback) {
      
      row = $(`
        <div class="col-xs-12 p-a-1 m-b-1 small ys-order">
          <div class="pull-right title"></span>${items.OrderTotal}${Pointofsales.settings.location.currency}</div>
          <div class="cols-xs-6">${items.CustomerName}</div>
          <div class="cols-xs-6">${items.CustomerPhone}</div>
          <div class="cols-xs-12">${items.Region}<span class="pull-right">${items.DeliveryTime}</span></div>
          <div class="hidden order-id">${items.Id}</div>
          <div class="btn-group btn-block p-t-1 col-md-12" data-order-id="${items.Id}">
            <a href="#" class="btn btn-danger btn-ripple btn-lg col-md-6 reject"><i class="fa fa-times"></i> RET</a>
            <a href="#" class="btn btn-warning btn-ripple btn-lg col-md-6 detail" data-items=""><i class="fa fa-info-circle"></i> DETAY</a>
          </div>
        </div>
      `);

      callback(row);
    },
  },

};





Pointofsales.Orders = {

  counts: {
    product_count: 0,
    product_amount: 0,
    stock_count: 0,
    stock_amount: 0,
    text_note_count: 0,
    sound_note_count: 0,
    gift_count: 0,
  },
  status: "pending",

  init: function () {
    Pointofsales.Orders.Events.init();

    // If production pages opened by Desktop app url should has production_id
    if (document.URL.match(/production_id=[0-9]+/)) {
      Pointofsales.JSDesktopApp.init();
    }

    if (Pointofsales.JSDesktopApp.status) {

      let production_id = $('select[name=production_id] option:selected').val().toString();
      Pointofsales.JSDesktopApp.setSelectedProductionPrinter(production_id);
    }

    setInterval(Pointofsales.Orders.get, 4000);
  },

  hasAnyChange: function (counts) {
    let anyChangeResult = 
    Pointofsales.Orders.counts.product_count !== counts.product_count || Pointofsales.Orders.counts.product_amount !== counts.product_amount ||
    Pointofsales.Orders.counts.stock_count !== counts.stock_count || Pointofsales.Orders.counts.stock_amount !== counts.stock_amount ||
    Pointofsales.Orders.counts.text_note_count !== counts.text_note_count || Pointofsales.Orders.counts.sound_note_count !== counts.sound_note_count ||
    Pointofsales.Orders.counts.gift_count !== counts.gift_count;


    let anyNewProdcutResult = 
    Pointofsales.Orders.counts.product_count < counts.product_count || Pointofsales.Orders.counts.product_amount < counts.product_amount || Pointofsales.Orders.counts.gift_count < counts.gift_count;

    if (anyChangeResult) {
      Pointofsales.Orders.counts.product_count = counts.product_count;
      Pointofsales.Orders.counts.product_amount = counts.product_amount;
      Pointofsales.Orders.counts.stock_count = counts.stock_count;
      Pointofsales.Orders.counts.stock_amount = counts.stock_amount;
      Pointofsales.Orders.counts.text_note_count = counts.text_note_count;
      Pointofsales.Orders.counts.sound_note_count = counts.sound_note_count;
      Pointofsales.Orders.counts.gift_count = counts.gift_count;

    }

    if (anyNewProdcutResult === true) {
      Pointofsales.Orders.Helpers.playSound();
    }
    

    return anyChangeResult;
  },

  setStatus: function () {
    Pointofsales.Orders.status = $("#filter select[name='status']").val();
  },

  getStatus: function () {
    return Pointofsales.Orders.status;
  },

  get: function () {
    
    Pointofsales.Orders.setStatus();

    Pointofsales.Helpers.postForm("#filter", "getOrders", function (json) {
      Pointofsales.Orders.compare(json.data.sales, function (counts, sales) {

        if (Pointofsales.Orders.hasAnyChange(counts) === false)
          return;

        // Desktop App
        if (Pointofsales.JSDesktopApp.status && !!Pointofsales.JSDesktopApp.SelectedProductionPrinter) {

          let production_id = Number($("select[name='production_id']").val());

          $.each(sales, function (key, sale) {

            Pointofsales.JSDesktopApp.printProductionInvoice(sale);

            // Ready products of sales
            $.each(sale.products, function (key1, product) {
              Pointofsales.Orders.completeProductReady({
                sale_id: sale.id,
                product_id: product.id,
                location_id: sale.location_id,
                production_id: production_id,
              }, function (result) {
                $(`#orders .completed tr[data-product_id="${sale.id}"]`).remove();
              });
            });

          });

        } else {

          Pointofsales.Orders.fill(sales);

          Pointofsales.Helpers.addLoader($("#orders").closest(".panel"));
          setTimeout(function() {
            Pointofsales.Helpers.removeLoader($("#orders").closest(".panel"));
          }, 200);
        }

      });
    });
  },

  compare: function (sales, callback) {
    
    let
    production_id = Number($("select[name='production_id']").val()),
    salesArr = [],
    counts = {
      product_count: 0,
      product_amount: 0,
      stock_count: 0,
      stock_amount: 0,
      text_note_count: 0,
      sound_note_count: 0,
      gift_count: 0,
    };

    // Sales
    for (let s = 0; s < sales.length; s++) {

      let sale = sales[s],
          productsArr = [];

      // products
      for (let p = 0; p < sale.products.length; p++) {

        let product = sale.products[p];
        product.production_id = Number(product.production_id);
        product.price = Number(product.price);
        product.quantity = Number(product.quantity);
        product.completed = Number(product.completed);

        if (product.type === "product") {

          if (product.production_id !== production_id)
            continue;

          if (Pointofsales.Orders.getStatus() === "pending") {
            product.quantity = product.quantity - product.completed;
          } else { // get Completed
            product.quantity = product.completed;
          }
          if (product.quantity < 1)
            continue;

        } else if (product.type === "package") {

          let packageProductsArr = [];
          product.quantity = 0;

          for (let pp = 0; pp < product.products.length; pp++) {

            let package_product = product.products[pp];
            package_product.production_id = Number(package_product.production_id);

            if (package_product.production_id !== production_id)
              continue;

            package_product.production_id = Number(package_product.production_id);
            package_product.quantity = Number(package_product.quantity);
            package_product.completed = Number(package_product.completed);

            if (Pointofsales.Orders.getStatus() === "pending") {
              product.quantity = package_product.quantity - package_product.completed;
            } else { // get Completed
              product.quantity = package_product.completed;
            }
            if (product.quantity < 1)
              continue;
            
            packageProductsArr.push(package_product); 
          }

          if (packageProductsArr.length < 1) {
            continue;
          }

          product.products = packageProductsArr;
        }

        productsArr.push(product);

        // COUNTS -----
        if (product.is_gift === "1") {
          counts.gift_count += product.quantity;
        } else {
          counts.product_count += product.quantity;
        }

        counts.product_amount += product.price * product.quantity;

        for (let ap = 0; ap < product.additional_products.length; ap++) {
          let additional_product = product.additional_products[ap];
          additional_product.price = Number(additional_product.price);
          if (product.is_gift === "1") {
            counts.gift_count += product.quantity;
          } else {
            counts.product_count += product.quantity;
          }
          counts.product_amount += additional_product.price * product.quantity;
        }

        if (product.type === "product") {
          for (let ps = 0; ps < product.stocks.length; ps++) {
            let product_stock = product.stocks[ps];
            product_stock.quantity = Number(product_stock.quantity);

            counts.stock_count += product.quantity;
            counts.stock_amount += product_stock.quantity * product.quantity;
          }
        }

        for (let pn = 0; pn < product.notes.length; pn++) {
          let product_note = product.notes[pn];
          if (product_note.type === "1") { // Text
            counts.text_note_count++;
          } else if (product_note.type === "2") { // Sound
            counts.sound_note_count++;
          }
        }

        counts.product_amount = Pointofsales.Helpers.round(counts.product_amount, 2);
        counts.stock_amount = Pointofsales.Helpers.round(counts.stock_amount, 2);
        // END - COUNTS -----
      }

      if (productsArr.length < 1)
        continue;

      sale.products = productsArr;
      salesArr.push(sale)
    }

    callback(counts, salesArr);
  },

  fill: function (sales) {

    $("#orders").html("");

    $.each(sales, function(index, sale) {

      $("#orders").append(`
        <tr class="theme-dark-blue" data-sale_id="${sale.id}">
          <td colspan="7" class="p-t-2"><b class="text-white">${sale.point_title} ( ${sale.zone_title} )</b></td>
          <td>
            <button type="button" class="btn btn-white btn-lg current_order_invoice"><i class="fa fa-print"></i> Fiş Yazdır</button>
          </td>
        </tr>
      `);

      $.each(sale.products, function(index, product) {
        Pointofsales.Orders.Templates.saleProductRow(sale, product, function (row) {
          $("#orders").append(row);
        });
      });

      Pointofsales.Helpers.removeLoader($("#orders").closest(".panel"));
    });
  },

  loadNoteModal: function (notes) {
    Pointofsales.Orders.emptyNoteModal();

    $("#modal-notes").modal("toggle");
    notes = $.map(notes, (value, key) => { 
      return [value]; 
    });

    notes.reverse();

    $.each(notes, function(index, note) {
      $("#modal-notes .modal-body").append(Pointofsales.Orders.Templates.noteRow(note));
    });
  },

  emptyNoteModal: function () {
    $("#modal-notes .modal-body").html("");
  },

  fillInvoice: function (sale) {

    let production_name = $('select[name="production_id"] option:selected').text() || "";

    $(".invoice-no").html(sale.id);
    $(".invoice-datetime").html(Pointofsales.Helpers.utcToLocal(sale.modified || sale.created));
    $(".invoice-point").html(sale.point_title);
    $(".invoice-zone").html(sale.zone_title);


    $(".invoice-body tbody").html("");
    $.each(sale.products, function(index, product) {
      Pointofsales.Orders.Templates.InvoiceProductRow(product, function (row) {
        $(".invoice-body tbody").append(row);
      });
    });

    sale.username = sale.username || "";
    sale.customer.name = sale.customer.name || "";
    sale.customer.phone = sale.customer.phone || "";
    sale.customer.address = sale.customer.address || "";
    $(".invoice-customer_name").html(sale.customer.name);
    $(".invoice-customer_phone").html(sale.customer.phone);
    $(".invoice-customer_address").html(sale.customer.address);
    $(".invoice-production-title").html(production_name);
    $(".invoice-username").html(sale.username);
  },

  completeProductReady: function (data, callback) {
    
    let query_string = $.param(data);

    Pointofsales.Helpers.postForm(null, `orderedProductCompleted?${query_string}`, function (json) {

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        callback(json);

      });
  },

  Events: {
    init: function () {
      $("body").on("click", "#orders .completed", Pointofsales.Orders.Events.onClickCompleted);
      $("body").on("click", "#orders .current_order_invoice", Pointofsales.Orders.Events.onClickPrintCurrentOrder);
      $("body").on("click", "#orders .notes", Pointofsales.Orders.Events.onClickNotes);
      $("body").on("hidden.bs.modal", "#modal-notes", Pointofsales.Orders.Events.onCloseNoteModal);
      $("body").on("click", "#order_invoice .invoice-print", Pointofsales.Orders.Events.onClickPrintOrderInvoice);
      $("body").on("click", "*", Pointofsales.Orders.Helpers.stopSound);
    },

    onClickCompleted: function (event) {
      let
      row = $(this).closest("tr"),
      sale_id = row.data('sale_id'),
      product_id = row.data('product_id'),
      location_id = $("select[name='location_id']").val(),
      production_id = $("select[name='production_id']").val(),
      query_string = `location_id=${location_id}&sale_id=${sale_id}&product_id=${product_id}&production_id=${production_id}`;

      Pointofsales.Orders.completeProductReady({
        sale_id: sale_id,
        product_id: product_id,
        location_id: location_id,
        production_id: production_id,
      }, function (result) {
        
        row.fadeOut(300, function() {
          row.remove();
        });
      });
    },

    onClickPrintCurrentOrder: function (event) {
      let
      row = $(this).closest("tr"),
      sale_id = row.data('sale_id'),
      location_id = $("select[name='location_id']").val(),
      query_string = `location_id=${location_id}&sale_id=${sale_id}`;

      Pointofsales.Helpers.postForm(null, `getSaleForOrderInvoice?${query_string}`, function (json) {
        
        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        Pointofsales.Orders.fillInvoice(json.data.sale);

          $("#order_invoice .invoice-print").trigger("click");

      });
    },

    onClickNotes: function (event) {
      let
      notes = $(this).data("notes"),
      notes_id = {},
      row = $(this).closest("tr"),
      sale_id = row.data('sale_id'),
      product_id = row.data('product_id'),
      location_id = $("select[name='location_id']").val();

      $.each(notes, function(index, note) {
         notes_id[index] = note.id;
      });

      notes_id = JSON.stringify(notes_id);

      if (!!!notes)
        return;

      let query_string = `location_id=${location_id}&sale_id=${sale_id}&product_id=${product_id}&notes_id=${notes_id}`;

      Pointofsales.Orders.loadNoteModal(notes);

      Pointofsales.Helpers.postForm(null,`getNoteViewed?${query_string}`, function(json) {

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

      });

    },

    onCloseNoteModal: function () {
      Pointofsales.Orders.emptyNoteModal();
    },

    onClickPrintOrderInvoice: function (event) {
      event.preventDefault();

      $("#order_invoice").print();
    },
  },

  Helpers: {
    playSound: function () {
      Pointofsales.Orders.Helpers.stopSound();

      $("body").append(`
        <audio controls autoplay loop class="hidden" id="new_order_sound">
          <source src="${base_url}assets/globals/plugins/ionsound/sounds/bell_ring.mp3" type="audio/mpeg">
        </audio>
      `).prop("volume", 1);
    },

    stopSound: function () {
      $("#new_order_sound").remove();
    },
  },

  Templates: {
    saleProductRow: function (sale, product, callback) {

      let
      additional_products = "",
      stocks = "",
      notes = "",
      notes_button = "hidden",
      completed_button = "hidden";

      if (product.type === "product") {
        $.each(product.stocks, function(index, stock) {
          if (stock.in_use === "1") {
            stocks += stock.stock_name + "<br>"
          }
        });
      }

      if (product.notes.length !== 0) {
        notes_button = "";
      }

      if (Pointofsales.Orders.getStatus() === "pending") {
        completed_button = "";
      }

      $.each(product.additional_products, function(index, additional_product) {
        additional_products += additional_product.title + "<br>"
      });

      callback(`
        <tr data-sale_id="${product.sale_id}" data-product_id="${product.id}">
          <td>
            <button class="btn btn-warning btn-lg ${notes_button} notes" data-notes='${JSON.stringify(product.notes)}'><i class="fa fa fa-pencil-square-o"></i> Ürün Notu</button>
          </td>
          <td>${product.quantity}</td>
          <td>${product.title}</td>
          <td>${stocks}</td>
          <td>${additional_products}</td>
          <td>${product.user_name}</td>
          <td>${Pointofsales.Helpers.utcToLocal(product.created)}</td>
          <td>
            <button class="btn btn-success btn-lg completed ${completed_button}"><i class="fa fa-thumbs-up"></i> Hazırlandı</button>
          </td>
        </tr>
      `);
    },

    noteRow: function (note) {
      let viewed_class = "";

      if (note.viewed === "0") {
        viewed_class = "bg-green text-white";
      }

      if (note.type === "1") { // Text
        return `
        <p class="well ${viewed_class}">
          <i>${note.note}</i>
        </p>`;
      } else if (note.type === "2") { // Sound
        return `
        <p class="well ${viewed_class}">
          <audio controls controlsList="nodownload" style="width: 100%;">
            <source src="${base_url + note.sound}" type="audio/mp3">
          </audio>
        </p>`;
      }
    },

    InvoiceProductRow: function (product, callback) {

      let 
      quantity = Number(product.quantity),
      completed = Number(product.completed),
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, product.tax),
      total = price * quantity,
      row,
      notes = "",
      gift_label = (product.is_gift === "1" ? '<span class="label label-info">İKRAM</span>' : ""),
      production_id = Number($('select[name="production_id"] option:selected').val()),
      package_products = "",
      stocks = "";

      product.production_id = Number(product.production_id);

      if (product.type === "product") {

        if (product.production_id !== production_id) {
          callback("");
          return;
        }

        if (Pointofsales.Orders.getStatus() === "pending") {
          quantity -= completed;
        } else {
          quantity = completed;
        }

      } else if (product.type === "package") {

        quantity = 0;

        let packageProductIsInThisProduction = false;
        $.each(product.products, function(key2, package_product) {

          package_product.production_id = Number(package_product.production_id);
          package_product.quantity = Number(package_product.quantity);
          package_product.completed = Number(package_product.completed);

          if (package_product.production_id === production_id) {

            package_products += `${package_product.title}, `;

            if (packageProductIsInThisProduction === false) { // Just first packages product quantity is enough for main product quantity!
              packageProductIsInThisProduction = true;
              if (Pointofsales.Orders.getStatus() === "pending") {
                quantity = package_product.quantity - package_product.completed;
              } else {
                quantity = package_product.completed;
              }
            }
          }
        });

        if (!packageProductIsInThisProduction) {
          callback("");
          return;
        }
      }

      if (quantity === 0) {
        callback("");
        return;
      }

      if (product.notes) {
        $.each(product.notes, function(index, note) {
           notes += note.note;
        });
      }

      if (product.stocks) {
        $.each(product.stocks, function(index, stock) {

          if (stock.optional === "1") {

            if (stock.in_use === "1") {

              stocks += `${stock.stock_name},`;

            } else if (stock.in_use === "0") {

              stocks += `(x)<del>${stock.stock_name}</del>,`;

            }

          }
        });
      }

      if (package_products) { 
        package_products = `( ${package_products.slice(0, -2)} )`;
      }

      if (stocks) {
        stocks = `<br>( ${stocks} )`
      }

      if (notes) { 
        notes = `<br>(<b>Not:</b> ${notes} )`;
      }

      row = $(`
      <tr>
          <td>
              ${product.title} ${stocks} ${gift_label} ${package_products} ${notes} 
              <p class="invoice-additional_title"></p>
          </td>
          <td class="">${quantity}</td>
          <td class="">&nbsp;</td>
      </tr>
      `);

      $.each(product.additional_products, function(index, additional_product) {
        let 
        additional_product_tax = parseFloat(additional_product.tax),
        additional_product_price = Pointofsales.Helpers.addTax(additional_product.price, additional_product.tax);

        $(".invoice-additional_title", row).append(additional_product.title + "<br>");
        $(".invoice-additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
      });

      callback(row);
    },
  },
};





Pointofsales.OrderTrack = {

  container: "#tab-ordertrack",
  notification: "#ordertrack_notification",

  init: function () {

    setInterval(Pointofsales.OrderTrack.getCompletedUnDeliveredProducts, 3000);

    Pointofsales.OrderTrack.Events.init();
  },

  loadTab: function () {
    Pointofsales.Helpers.setPreviousTab();
    $("a[href='#tab-ordertrack']").trigger("click");
  },

  getCompletedUnDeliveredProducts: function () {
    
    Pointofsales.Helpers.postForm(null, "getCompletedUnDeliveredProducts", function (json) {

      if (!json.result) {
        Pointofsales.OrderTrack.displayNotification(0);
        Pointofsales.OrderTrack.fillProducts({});
        return;
      }

      Pointofsales.OrderTrack.displayNotification(json.data.products.length + json.data.call_accounts.length);
      Pointofsales.OrderTrack.fillProducts(json.data.products);
      Pointofsales.OrderTrack.fillCallAccounts(json.data.call_accounts);
    });
  },

  fillProducts: function (products) {

    $("#products", Pointofsales.OrderTrack.container).html("");

    $.each(products, function(index, product) {
      Pointofsales.OrderTrack.Templates.productRow(product, function (row) {
        $("#products", Pointofsales.OrderTrack.container).append(row);
      });
    });
  },

  fillCallAccounts: function (sales) {

    $("#call-accounts", Pointofsales.OrderTrack.container).html("");

    $.each(sales, function(index, sale) {
      Pointofsales.OrderTrack.Templates.callAccountRow(sale, function (row) {
        $("#call-accounts", Pointofsales.OrderTrack.container).append(row);
      });
    });
  },

  displayNotification: function (count) {
    count = Number(count);

    $(".count", Pointofsales.OrderTrack.notification).html(count);

    if (count > 0) {
      $(Pointofsales.OrderTrack.notification).removeClass("disabled");
    } else {
      $(Pointofsales.OrderTrack.notification).addClass("disabled");
    }
  },

  Events: {
    init: function () {
      $("body").on("click", `${Pointofsales.OrderTrack.container} .delivered`, Pointofsales.OrderTrack.Events.onClickDelivered);
      $("body").on("click", `${Pointofsales.OrderTrack.container} .callaccount`, Pointofsales.OrderTrack.Events.onClickCallAccount);
    },

    onClickDelivered: function (event) {
      event.preventDefault();

      let
      row = $(this).closest("tr"),
      sale_id = row.data("sale_id"),
      product_id = row.data("product_id"),
      query_string = `sale_id=${sale_id}&product_id=${product_id}`;

      Pointofsales.Helpers.addLoader($("#products", Pointofsales.OrderTrack.container));
      Pointofsales.Helpers.postForm(null, `productDelivered?${query_string}`, function (json) {
        
        Pointofsales.Helpers.removeLoader($("#products", Pointofsales.OrderTrack.container));

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        row.fadeOut(300, function() {
          row.remove();
        });

      });
    },

    onClickCallAccount: function (event) {
      event.preventDefault();

      let
      row = $(this).closest("tr"),
      sale_id = row.data("sale_id"),
      query_string = `sale_id=${sale_id}`;

      Pointofsales.Helpers.addLoader($("#call-accounts", Pointofsales.OrderTrack.container));
      Pointofsales.Helpers.postForm(null, `setViewedCallAccount?${query_string}`, function (json) {
        
        Pointofsales.Helpers.removeLoader($("#call-accounts", Pointofsales.OrderTrack.container));

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }

        row.fadeOut(300, function() {
          row.remove();
        });

      });
    },
  },

  Templates: {
    productRow: function (product, callback) {
      let
      row = "",
      stocks = "",
      additional_products = "";

      if (product.type === "product") {
        $.each(product.stocks, function(index, stock) {
          if (stock.in_use === "1") {
            stocks += stock.stock_name + "<br>"
          }
        });
      }

      $.each(product.additional_products, function(index, additional_product) {
        additional_products += additional_product.title + "<br>"
      });

      row = $(`
        <tr data-sale_id="${product.sale_id}" data-product_id="${product.id}">
          <td>${product.quantity}</td>
          <td>${product.title}</td>
          <td>${stocks}</td>
          <td>${additional_products}</td>
          <td>${product.point_title} ( ${product.zone_title} )</td>
          <td class="text-right">
            <a href="#" class="btn btn-success btn-xxl btn-ripple delivered"><i class="fa fa-thumbs-up"></i>TESLİM EDİLDİ</a>
          </td>
        </tr>
      `);

      callback(row);
    },

    callAccountRow: function (sale, callback) {
      let row = "";

      row = $(`
        <tr data-sale_id="${sale.id}" >
          <td>-</td>
          <td><label class="label label-danger" style ="font-size: 20px;">Hesap İsteniyor</label></td>
          <td>-</td>
          <td>-</td>
          <td>${sale.point_title}</td>
          <td class="text-right">
            <a href="#" class="btn btn-warning btn-xxl btn-ripple callaccount"><i class="fa fa-check"></i> İSTEK ALINDI</a>
          </td>
        </tr>
      `);

      callback(row);
    },
  },
};





Pointofsales.OperatorSession = {

  container: "#tab-operatorsession",

  init: function () {

    setInterval(Pointofsales.OperatorSession.checkActiveSession, 3000);

    Pointofsales.OperatorSession.Events.init();
  },

  loadTab: function () {
    $("a[href='#tab-operatorsession']").trigger("click");

    Pointofsales.OperatorSession.emptyTab();
  },

  emptyTab: function () {
    $('input[name="password"]', Pointofsales.OperatorSession.container).val("");
  },

  loadTabByStatus: function (json) {
    if (!json.result && !Pointofsales.Helpers.activeTabIs("tab-operatorsession")) {
      Pointofsales.OperatorSession.loadTab();
    } else if (json.result && Pointofsales.Helpers.activeTabIs("tab-operatorsession")) {
      Pointofsales.Tables.loadTab();
    }
  },

  checkActiveSession: function () {
    Pointofsales.Helpers.postForm(null, "checkOperatorSession", function(json) {
      Pointofsales.OperatorSession.loadTabByStatus(json);
    }, "json");
  },

  login: function () {
    Pointofsales.Helpers.postForm($("form", Pointofsales.OperatorSession.container), "loginForOperatorSession", function (json) {

      if (!json.result) {
        Pleasure.handleToastrSettings("Hata!", json.message, "error");
        return;
      }

      Pointofsales.settings.operator_session = {
        user_id: json.data.user.id,
        user_name: json.data.user.username,
        user_role: json.data.user.role_alias,
        permissions: json.data.user.permissions
      };
      Pointofsales.settings.package_service = json.data.package_service;
      Pointofsales.OperatorSession.Helpers.displayOperatorName(json.data.user.username);
      Pointofsales.Pos.Helpers.displayPackageServiceTrackButton();
      Pointofsales.OperatorSession.Helpers.displayButtonsByPermissions();


      Pointofsales.Tables.loadTab();
    });
  },

  Events: {
    init: function () {
      $("body").on("click", `${Pointofsales.OperatorSession.container} .nums .btn`, Pointofsales.OperatorSession.Events.onClickNumPadButtons);
      $("body").on("click", "#exit_operator_session", Pointofsales.OperatorSession.Events.onClickExit);
    },

    onClickNumPadButtons: function (event) {
      event.preventDefault();

      let 
      button = $(this),
      value = button.data("value");

      if ("1234567890".indexOf(value) > -1) {
        Pointofsales.OperatorSession.Helpers.addNumber(value);
      } else if (value === "delete") {
        Pointofsales.OperatorSession.Helpers.deleteNumber();
      } else if (value === "submit") {
        Pointofsales.OperatorSession.login();
      }
    },

    onClickExit: function (event) {
      event.preventDefault();

      Pointofsales.Helpers.postForm(null, "logoutForOperatorSession", function (json) {
        Pointofsales.OperatorSession.loadTab();
      });
    }
  },

  Helpers: {
    addNumber: function (number) {
      let 
      input = $('input[name="password"]', Pointofsales.OperatorSession.container),
      value = input.val();

      input.val(value +""+ number);
    },

    deleteNumber: function () {
      let 
      input = $('input[name="password"]', Pointofsales.OperatorSession.container),
      value = input.val();
      value = value.slice(0, -1);

      input.val(value);
    },

    displayOperatorName: function (username) {
      $("#pos-username", "#pos-details").html(username);
      $(".adisyon-username", Pointofsales.Invoice.container_adisyon).html(username);
    },

    displayButtonsByPermissions: function() {
      Pointofsales.Helpers.visible("#pos #pos-complete", Pointofsales.Helpers.hasPermission("pointofsales.check"));
      Pointofsales.Helpers.visible("#pos .left-pos-buttons #interim-payments", Pointofsales.Helpers.hasPermission("pointofsales.interimpayment"));
      Pointofsales.Helpers.visible("#pos .left-pos-buttons #move-table", Pointofsales.Helpers.hasPermission("pointofsales.tablemove"));
      Pointofsales.Helpers.visible("#modal-case #cancelSave", Pointofsales.Helpers.hasPermission("pointofsales.tablecancel"));
      Pointofsales.Helpers.visible("#modal-case #change_pos", Pointofsales.Helpers.hasPermission("pointofsales.changepos"));
      Pointofsales.Helpers.visible("#package-service-button", Pointofsales.Helpers.hasPermission("pointofsales.packageservicebutton"));
      Pointofsales.Helpers.visible("#self-service-button", Pointofsales.Helpers.hasPermission("pointofsales.selfservicebutton"));
      Pointofsales.Helpers.visibleInput("#modal-discount_login #username-password-group", Pointofsales.Helpers.hasPermission("pointofsales.adddiscount"));

    }
  }
};





Pointofsales.PackageServiceCustomer = {

  modal: "#modal-package_service_customer",
  customers: {},

  // The phone number from url (Caller ID)
  urlPhoneNumber: null,

  init: function () {
    Pointofsales.PackageServiceCustomer.Events.init();
  },

  loadModal: function () {
    Pointofsales.PackageServiceCustomer.emptyModal();

    // Load the phone number fetched from url (Caller ID)
    if (!!Pointofsales.PackageServiceCustomer.getUrlPhoneNumber()) {
      Pointofsales.PackageServiceCustomer.fillPhoneNumberInput(Pointofsales.PackageServiceCustomer.getUrlPhoneNumber());
    }

    $(Pointofsales.PackageServiceCustomer.modal).modal("toggle");
  },

  emptyModal: function () {
    $("input[name='phone'], input[name='name'], textarea[name='address']", Pointofsales.PackageServiceCustomer.modal).val("");
    Pointofsales.PackageServiceCustomer.Helpers.emptyCustomerList();
  },

  getUrlPhoneNumber: function () {
    return Pointofsales.PackageServiceCustomer.urlPhoneNumber;
  },

  setUrlPhoneNumber: function (phone) {
    Pointofsales.PackageServiceCustomer.urlPhoneNumber = phone;
  },

  fillPhoneNumberInput: function (phone) {
    $(`${Pointofsales.PackageServiceCustomer.modal} input[name='phone']:last`)
    .val(phone)
    .trigger("keyup");
  },

  Events: {

    init: function () {
      $("body").on("click", "#open_package_service_customer", Pointofsales.PackageServiceCustomer.Events.onClickOpenModal);

      $("body").on("keyup", `${Pointofsales.PackageServiceCustomer.modal} input[name='phone']`, Pointofsales.PackageServiceCustomer.Events.onSearchCustomer);
      $("body").on("keyup", `${Pointofsales.PackageServiceCustomer.modal} input[name='name']`, Pointofsales.PackageServiceCustomer.Events.onSearchCustomer);

      $("body").on("click", `${Pointofsales.PackageServiceCustomer.modal} .customerlist .select`, Pointofsales.PackageServiceCustomer.Events.onClickSelectCustomer);

      $("body").on("click", `${Pointofsales.PackageServiceCustomer.modal} #create_customer`, Pointofsales.PackageServiceCustomer.Events.onClickCreateCustomer);
    },

    onClickOpenModal: function (event) {
      event.preventDefault();
      Pointofsales.PackageServiceCustomer.loadModal();
    },

    timer: null,
    onSearchCustomer: function (event) {
      let
      input = $(this),
      input_name = input.attr("name"),
      value = input.val(),
      query_string = `field=${input_name}`;

      value = $.trim(value);
      if (value.length < 3) {
        Pointofsales.PackageServiceCustomer.Helpers.emptyCustomerList();
        return;
      }

      if (!!Pointofsales.PackageServiceCustomer.Events.timer) {
        clearTimeout(Pointofsales.PackageServiceCustomer.Events.timer);
      }

      Pointofsales.PackageServiceCustomer.Events.timer = setTimeout(function() {
        Pointofsales.Helpers.postForm(`${Pointofsales.PackageServiceCustomer.modal} form`, `findCustomer?${query_string}`, function (json) {

          Pointofsales.PackageServiceCustomer.customers = {};
          Pointofsales.PackageServiceCustomer.Helpers.emptyCustomerList();

          if (!json.result) return;

          Pointofsales.PackageServiceCustomer.customers = json.data.customers;

          $.each(json.data.customers, function(index, customer) {
            $(".customerlist", Pointofsales.PackageServiceCustomer.modal).append(
              Pointofsales.PackageServiceCustomer.Templates.customerRow(customer)
            );
          });
        });
      }, 500);
    },

    onClickSelectCustomer: function () {
      let
      row = $(this).closest("tr"),
      id = Number(row.data("id"));

      $.each(Pointofsales.PackageServiceCustomer.customers, function(index, customer) {
        if (Number(customer.id) === id) {
          $("input[name='customer_id']", Pointofsales.Pos.container).val(customer.id);
          Pointofsales.Pos.Helpers.displayCustomerName(customer);
          $(Pointofsales.PackageServiceCustomer.modal).modal("toggle");
          return;
        }
      });
    },

    onClickCreateCustomer: function () {
      Pointofsales.Helpers.addLoader(Pointofsales.PackageServiceCustomer.modal);
      Pointofsales.Helpers.postForm($("form", Pointofsales.PackageServiceCustomer.modal), "addCustomer", function (json) {
        Pointofsales.Helpers.removeLoader(Pointofsales.PackageServiceCustomer.modal);

        if (!json.result) {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          return;
        }
        Pleasure.handleToastrSettings("Başarılı", json.message, "success");

        $("input[name='customer_id']", Pointofsales.Pos.container).val(json.data.customer.id);
        Pointofsales.Pos.Helpers.displayCustomerName(json.data.customer);
        $(Pointofsales.PackageServiceCustomer.modal).modal("toggle");

      });
    },
  },

  Helpers: {
    emptyCustomerList: function () {
      $(".customerlist", Pointofsales.PackageServiceCustomer.modal).html("");
    },

    showButton: function (status) {
      if (status === true) {
        $("#open_package_service_customer").removeClass("hidden");
        $("#pos-courier-img").removeClass("hidden");
      } else {
        $("#open_package_service_customer").addClass("hidden");
        $("#pos-courier-img").addClass("hidden");
      }
    },
  },

  Templates: {
    customerRow: function (customer) {
      return `
        <tr data-id="${customer.id}">
          <td class="col-sm-3">${customer.name}</td>
          <td class="col-sm-3">${customer.phone}</td>
          <td class="col-sm-4 small">${customer.address}</td>
          <td class="col-sm-2">
            <button type="button" class="btn btn-primary btn-xxl select">Seç</button>
          </td>
        </tr>
      `;
    },
  }
};





Pointofsales.CashClosing = {

  container: "#tab-cashclosing",
  tabs: {
    moneys: "#cct-moneys",
    approval: "#cct-approval",
    print: "#cct-print",
  },

  init: function () {
    Pointofsales.CashClosing.createPaymentInputs();
    Pointofsales.CashClosing.createCashInputs();

    Pointofsales.CashClosing.Events.init();
  },

  loadTab: function (tab) {
    $("a[href='#tab-cashclosing']").trigger("click");

    $("#cc-username", Pointofsales.CashClosing.tabs.moneys).html(Pointofsales.settings.operator_session.user_name);

    Pointofsales.CashClosing.loadInnerTab(tab);
  },

  loadInnerTab: function (tab) {
    switch (tab) {
      case "approval":
        Pointofsales.CashClosing.tabApproval();
        break;
      case "print":
        Pointofsales.CashClosing.tabPrint();
        break;
      case "moneys":
      default:
        Pointofsales.CashClosing.tabMoneys();
        break;
    }
  },

  tabMoneys: function () {
    $("a[href='#cct-moneys']").trigger("click");
  },
  tabApproval: function () {
    $("a[href='#cct-approval']").trigger("click");
  },
  tabPrint: function () {
    $("a[href='#cct-print']").trigger("click");
  },

  createPaymentInputs: function () {
    $.each(Pointofsales.settings.payment_types, function(index, payment_type) {
      $("#payment_inputs", Pointofsales.CashClosing.tabs.moneys).append(Pointofsales.CashClosing.Templates.paymentInput(payment_type));
    });
  },

  createCashInputs: function () {

    var sortedDesc = [];
    for (var val in Pointofsales.settings.cash_types)
      sortedDesc.push([val, Pointofsales.settings.cash_types[val]]);
    sortedDesc.sort(function(a, b) { return b[1] - a[1]; });

    $.each(sortedDesc, function(key, value) {
      $("#cash_inputs", Pointofsales.CashClosing.tabs.moneys).append(Pointofsales.CashClosing.Templates.cashInput(value[0], value[1]));
    });

    $("#cash_inputs", Pointofsales.CashClosing.tabs.moneys).append(Pointofsales.CashClosing.Templates.cashDropInput());
  },

  fillPrintContent: function (session) {
    
    
    Pointofsales.CashClosing.Templates.printContent(session, function (content) {
      $("#print-content", Pointofsales.CashClosing.tabs.print).html(content);
    });
  },

  Events: {
    init: function () {
      // Saving moneys
      $("body").on("click", "#open-approval", Pointofsales.CashClosing.Events.onClickOpenApproval);

      // Submit the approval form
      $("body").on("click", "#submit-approval", Pointofsales.CashClosing.Events.onSubmitApproval);

      // Change Moneys
      $("input[name^=cashs], input[name='cash_opening']", Pointofsales.CashClosing.tabs.moneys).on("change input", Pointofsales.CashClosing.Events.onChangeCash);
      
      // Print session content
      $("#print-session", Pointofsales.CashClosing.tabs.print).on("click", Pointofsales.CashClosing.Events.onClickPrint);
    },

    onClickOpenApproval: function (event) {
      event.preventDefault();

      Pointofsales.CashClosing.loadInnerTab("approval");
    },

    onSubmitApproval: function (event) {
      event.preventDefault();
      
      Pointofsales.Helpers.addLoader(Pointofsales.CashClosing.container);

      Pointofsales.Helpers.postForm($("form", Pointofsales.CashClosing.container), "approveSession", function (json) {

        Pointofsales.Helpers.removeLoader(Pointofsales.CashClosing.container);
        
        if (!json.result) {
          Pleasure.handleToastrSettings('Error!', json.message, 'error');
          return;
        }

        if (json.data.status) {
          Pleasure.handleToastrSettings('Success', json.message, 'success');
          Pointofsales.CashClosing.loadInnerTab("print");
          Pointofsales.CashClosing.fillPrintContent(json.data.session);
        } else {
          Pleasure.handleToastrSettings('Error!', json.message, 'error');
          Pointofsales.CashClosing.loadInnerTab("moneys");
        }
      });
    },
    
    onChangeCash: function (event) {
      let total = 0;
      
      $.each($("input[name^=cashs]", Pointofsales.CashClosing.tabs.moneys), function(index, val) {
        
        let
        input = $(this),
        quantity = parseFloat(input.val()) || 0,
        value = input.data("value"),
        inputResult = input.parent().parent().find(".money_result"),
        amount = (value === "drop") ? quantity : (quantity * value);

        inputResult.val(amount.toFixed(2));

        total += amount;
      });

      let cash_opening = $("input[name='cash_opening']").val();
      cash_opening = parseFloat(cash_opening) || 0;

      total -= cash_opening;

      $("input[name='payments[cash]']", Pointofsales.CashClosing.tabs.moneys).val(total.toFixed(2));
    },

    onClickPrint: function (event) {
      event.preventDefault();
      
      $("#print-content", Pointofsales.CashClosing.tabs.print).print();
    },
  },

  Templates: {
    paymentInput: function (payment_type) {
      if (payment_type.alias === "cash") {
        return `
        <div class="col-sm-3">
          <h3 class="m-b-0">${payment_type.title}</h3>
          <p class="payments[cash]"><input type="text" name="payments[cash]" class="form-control text-center" placeholder="0.00" readonly></p>
        </div>`;
      } else {
        return `
        <div class="col-sm-3">
          <h3 class="m-b-0">${payment_type.title}</h3>
          <p class="payments[${payment_type.alias}]"><input type="text" name="payments[${payment_type.alias}]" class="form-control text-center" placeholder="0.00" autocomplete="off"></p>
        </div>`;
      }
    },

    cashInput: function (cash_alias, cash_value) {
      return `
      <div class="col-sm-3">
          <div class="col-sm-4"><h4>${cash_value} ${Pointofsales.settings.location.currency} x</h4></div>
          <div class="col-sm-4"><input type="text" name="cashs[${cash_alias}]" class="form-control" data-value="${cash_value}" autocomplete="off"></div>
          <div class="col-sm-4"><input type="text" class="form-control money_result" value="0.00" readonly></div>
      </div>
      `;
    },

    cashDropInput: function () {
      return `
      <div class="col-sm-3">
          <div class="col-sm-4"><h4 class="text-red">Miktar </h4></div>
          <div class="col-sm-4"><input type="text" name="cashs[drop]" class="form-control" data-value="drop" autocomplete="off"></div>
          <div class="col-sm-4"><input type="text" class="form-control money_result" value="0.00" readonly></div>
      </div>
      `;
    },

    printContent: function (session, callback) {
      let
      diff = 0,
      content = "";

      session.created = Pointofsales.Helpers.utcToLocal(session.created);
      session.closed = Pointofsales.Helpers.utcToLocal(session.closed);
      session.cash_opening = Number(session.cash_opening);

      content += `TARİH: ${session.closed}\n`;
      content += `İŞLEM NO: ${session.id}\n`;
      content += `RESTORAN: ${Pointofsales.settings.location.title}\n`;
      content += `PERSONEL: ${Pointofsales.settings.operator_session.user_name}\n`;
      content += `BAŞLANGIÇ: ${session.created}\n`;
      content += `BİTİŞ: ${session.closed}\n`;
      content += `AÇILIŞ MİKTARI = ${session.cash_opening.toFixed(2)}\n`;

      $.each(Pointofsales.settings.payment_types, function(index, payment_type) {

        session[payment_type.alias] = Number(session[payment_type.alias]);
        session[payment_type.alias + '_exists'] = Number(session[payment_type.alias + '_exists']);
        diff = session[payment_type.alias + '_exists'] - session[payment_type.alias];

        content += `${payment_type.title} (SİSTEM): ${session[payment_type.alias].toFixed(2)}\n`;
        content += `${payment_type.title} (ELDEKİ): ${session[payment_type.alias + '_exists'].toFixed(2)}\n`;

        if (payment_type.alias === "cash") {
          diff = diff - session.cash_opening;
          content += `${payment_type.title} (FARK): ${diff.toFixed(2)}\n`;
        } else {
          content += `${payment_type.title} (FARK): ${diff.toFixed(2)}\n`;
        }

      });


      callback(content);
    },
  }
};





Pointofsales.JSDesktopApp = {

  status: false,
  SystemPrinters: null,
  SelectedProductionsPrinters: null,
  SelectedProductionPrinter: null,

  init: function () {
    Pointofsales.JSDesktopApp.check();

    Pointofsales.JSDesktopApp.SystemPrinters = Pointofsales.Helpers.getCookie("JSDesktopApp_SystemPrinters");
    Pointofsales.JSDesktopApp.SelectedProductionsPrinters = Pointofsales.Helpers.getCookie("JSDesktopApp_SelectedProductionsPrinters");
  },

  check: function () {

    if (typeof JSDesktopApp !== "undefined") {
      Pointofsales.JSDesktopApp.status = true;
    }
  },

  setSelectedProductionPrinter: function (production_id) {

    try {

      let
      selected_productions_printers = JSON.parse(Pointofsales.JSDesktopApp.SelectedProductionsPrinters),
      pp = selected_productions_printers.find(o => (o.production_id === production_id));

      if (!!pp) {
        Pointofsales.JSDesktopApp.SelectedProductionPrinter = pp.printer;
      }
    } catch (e) {
      console.log("Pointofsales.JSDesktopApp.setCurrentProductionPrinter --> " + e);
    }
  },


  fillProductionsPrinters: function () {

    let 
    system_printers = Pointofsales.JSDesktopApp.SystemPrinters,
    selected_productions_printers = Pointofsales.JSDesktopApp.SelectedProductionsPrinters,
    rows = $('#productions_printers tbody tr', Pointofsales.Setup.container);

    try {

      system_printers = JSON.parse(system_printers);
      selected_productions_printers = JSON.parse(selected_productions_printers);

      $.each(rows, function (i, row) {

        let
        production_id = $("input[name^='productions']", row).val(),
        printers_list = $(`select[name="printers[${production_id}]"]`, row);

        if (!!system_printers) {
          $.each(system_printers, function (key, printer) {

            let
            checked = false,
            option = null;

            if (!!selected_productions_printers) {
              if (selected_productions_printers.findIndex(o => (o.production_id === production_id && o.printer === printer)) > -1) {
                checked = true;
                $(`input[name="autoprint[${production_id}]"]`, row).attr("checked", true);
              }
            }

            option = $("<option></option>").val(printer).html(printer).attr("selected", checked);
            $(printers_list).append(option);

          });
        }
      });

    } catch (e) {
      console.log("Pointofsales.JSDesktopApp.fillProductionsPrinters --> " + e);
    }
    
  },

  assignProductionsAndPrintersForAutoPrint: function () {
    
    let
    rows = $('#productions_printers tbody tr', Pointofsales.Setup.container),
    location_id = $("select[name='location_id']", Pointofsales.Setup.container).val(),
    printers = [];

    $.each(rows, function(index, row) {

      let
      production_id = $("input[name^='productions']", row).val(),
      printer = $(`select[name="printers[${production_id}]"]`, row).val(),
      autoprint = $(`input[name="autoprint[${production_id}]"]`, row).is(':checked');

      if(!!autoprint && !!printer) {
        printers.push({
          "production_id": production_id,
          "printer": printer,
        });
      }
    });

    Pointofsales.JSDesktopApp.SelectedProductionsPrinters = JSON.stringify(printers);

    Pointofsales.Helpers.setCookie("JSDesktopApp_SelectedProductionsPrinters", Pointofsales.JSDesktopApp.SelectedProductionsPrinters, 365);

    Pointofsales.JSDesktopApp.setUpProductionPages(location_id);
    Pointofsales.JSDesktopApp.restartApplicaton();
  },

  setUpProductionPages: function (location_id) {

    JSDesktopApp.setUpProductionPages(location_id, Pointofsales.JSDesktopApp.SelectedProductionsPrinters);
  },

  restartApplicaton: function () {

    JSDesktopApp.restartApplicaton();
  },

  printContent: function (html) {
    
    html = Pointofsales.JSDesktopApp.handleTemplate(html);

    let result = JSDesktopApp.printContentFromDefaultPrinter(html);

    console.log(result);
  },

  printProductionInvoice: function (sale) {

    let
    data = Pointofsales.JSDesktopApp.handleProductionInvoice(sale),
    result = JSDesktopApp.printProductionInvoice(JSON.stringify((data)));

    console.log(result);
  },

  hideList: function () {
    $("#productions_printers").addClass("hidden");
  },

  handleTemplate: function (html) {

    return `
      <head>
        <meta charset="utf-8">

        <!-- BEGIN CORE CSS -->
        <link rel="stylesheet" href="${base_url}assets/panel/css/admin1.css">
        <link rel="stylesheet" href="${base_url}assets/globals/css/elements.css">
        <!-- END CORE CSS -->

      </head>
      <body>
        <div class="content">
          ${html}
        </div>

        <!-- BEGIN GLOBAL AND THEME VENDORS -->
        <script src="${base_url}assets/globals/js/global-vendors.js"></script>
        <!-- END GLOBAL AND THEME VENDORS -->
      </body>`;
  },

  handleProductionInvoice: function (sale) {

    let data = {
      Data: null,
      ImageWidth: 570,
      IsDataImage: false,
      LogMe: false,
      PrinterType: 0,
      PrintJobId: null,
      PrinterName: Pointofsales.JSDesktopApp.SelectedProductionPrinter,
    };

    data.Data = JSON.stringify({
      Rows: Pointofsales.JSDesktopApp.handleProductionInvoiceRows(sale)
    });

    return data;
  },

  handleProductionInvoiceRows: function (sale) {
    
    let
    production_name = $('select[name="production_id"] option:selected').text() || "",
    data_time = Pointofsales.Helpers.utcToLocal(sale.modified || sale.created),
    rows = [],
    styleTitle1 = {
      FontName: "Arial",
      FontSize: 25,
      IsBold: true
    },
    styleTitle2 = {
      FontName: "Arial",
      FontSize: 20,
      IsBold: false
    },
    styleTitle2Bold = {
      FontName: "Arial",
      FontSize: 20,
      IsBold: true
    },
    styleText3 = {
      FontName: "Arial",
      FontSize: 17,
      IsBold: false
    };

    sale.username = sale.username || "";
    sale.customer.name = sale.customer.name || "";
    sale.customer.phone = sale.customer.phone || "";
    sale.customer.address = sale.customer.address || "";

    rows.push({
      DataRowColumn: [
        {
          Data: production_name,
          Font: styleTitle1,
          Percentage: 100,
          TextAlign: 1
        }
      ]
    });

    rows.push({
      DataRowColumn: [
        {
          Data: sale.point_title,
          Font: styleTitle2,
          Percentage: 50,
          TextAlign: 0
        },
        {
          Data: `No: #${sale.id}`,
          Font: styleTitle2,
          Percentage: 50,
          TextAlign: 2
        }
      ]
    });

    rows.push({
      DataRowColumn: [
        {
          Data: sale.zone_title,
          Font: styleTitle2,
          Percentage: 50,
          TextAlign: 0
        },
        {
          Data: data_time,
          Font: styleTitle2,
          Percentage: 50,
          TextAlign: 2
        }
      ]
    });

    rows.push({
      DataRowColumn: [
        {
          Data: sale.username,
          Font: styleTitle2,
          Percentage: 100,
          TextAlign: 0
        }
      ]
    });

    rows.push({
      DataRowColumn: [
        {
          Data: " ",
          Font: styleTitle2,
          Percentage: 100,
          TextAlign: 0
        }
      ]
    });

    rows.push({
      DataRowColumn: [
        {
          Data: "Ürün",
          Font: styleTitle2Bold,
          Percentage: 80,
          TextAlign: 0
        },
        {
          Data: "Miktar",
          Font: styleTitle2Bold,
          Percentage: 20,
          TextAlign: 2
        }
      ]
    });

    $.each(sale.products, function(index, product) {
      Pointofsales.JSDesktopApp.Templates.ProductionProductRow(product, function (row) {
        if (row) {
          $.each(row, function(index, r) {
            rows.push(r);
          });
        }
      });
    });


    if (sale.customer.name) {
      rows.push({
        DataRowColumn: [
          {
            Data: sale.customer.name,
            Font: styleText3,
            Percentage: 100,
            TextAlign: 1
          }
        ]
      });

      rows.push({
        DataRowColumn: [
          {
            Data: sale.customer.phone,
            Font: styleText3,
            Percentage: 100,
            TextAlign: 1
          }
        ]
      });

      rows.push({
        DataRowColumn: [
          {
            Data: sale.customer.address,
            Font: styleText3,
            Percentage: 100,
            TextAlign: 1
          }
        ]
      });
    }

    return rows;
  },

  Templates: {
    ProductionProductRow: function (product, callback) {
      
      let 
      quantity = Number(product.quantity),
      completed = Number(product.completed),
      tax = parseFloat(product.tax),
      price = Pointofsales.Helpers.addTax(product.price, product.tax),
      total = price * quantity,
      row = [],
      notes = "",
      gift_label = (product.is_gift === "1" ? '(İKRAM)' : ""),
      production_id = Number($('select[name="production_id"] option:selected').val()),
      package_products = "",
      stocks = "",
      styleText1 = {
        FontName: "Arial",
        FontSize: 20,
        IsBold: false
      },
      styleText2 = {
        FontName: "Arial",
        FontSize: 15,
        IsBold: false
      };

      product.production_id = Number(product.production_id);

      if (product.type === "package") {

        quantity = 0;

        let packageProductIsInThisProduction = false;
        $.each(product.products, function(key2, package_product) {

          package_product.production_id = Number(package_product.production_id);
          package_product.quantity = Number(package_product.quantity);
          package_product.completed = Number(package_product.completed);

          if (package_product.production_id === production_id) {

            package_products += `${package_product.title}, `;

            if (packageProductIsInThisProduction === false) { // Just first packages product quantity is enough for main product quantity!
              packageProductIsInThisProduction = true;
              if (Pointofsales.Orders.getStatus() === "pending") {
                quantity = package_product.quantity - package_product.completed;
              } else {
                quantity = package_product.completed;
              }
            }
          }
        });

        if (!packageProductIsInThisProduction) {
          callback("");
          return;
        }
      }

      if (quantity <= 0) {
        callback("");
        return;
      }

      row.push({
        DataRowColumn: [
          {
            Data: product.title + gift_label,
            Font: styleText1,
            Percentage: 90,
            TextAlign: 0
          },
          {
            Data: quantity,
            Font: styleText1,
            Percentage: 10,
            TextAlign: 2
          }
        ]
      });

      $.each(product.additional_products, function(index, additional_product) {
        row.push({
          DataRowColumn: [
            {
              Data: additional_product.title,
              Font: styleText2,
              Percentage: 100,
              TextAlign: 0
            }
          ]
        });
      });

      if (product.stocks) {
        $.each(product.stocks, function(index, stock) {
          if (stock.optional === "1") {
            if (stock.in_use === "1") {
              stocks += `${stock.stock_name}, `;
            } else if (stock.in_use === "0") {
              stocks += `(x)${stock.stock_name}, `;
            }
          }
        });

        row.push({
          DataRowColumn: [
            {
              Data: stocks,
              Font: styleText2,
              Percentage: 100,
              TextAlign: 0
            }
          ]
        });
      }

      if (package_products) { 
        row.push({
          DataRowColumn: [
            {
              Data: package_products,
              Font: styleText2,
              Percentage: 100,
              TextAlign: 0
            }
          ]
        });
      }

      if (product.notes) {   
        $.each(product.notes, function(index, note) {
          row.push({
            DataRowColumn: [
              {
                Data: "Not: " + note.note,
                Font: styleText2,
                Percentage: 100,
                TextAlign: 0
              }
            ]
          });
        });
      }


      row.push({
        DataRowColumn: [
          {
            Data: " ",
            Font: styleText2,
            Percentage: 100,
            TextAlign: 0
          }
        ]
      });

      callback(row);
    },
  },
};





Pointofsales.Helpers = {
  postForm: function (form, url, callback) {
    $.post(base_url + "pointofsales/ajax/" + url, $(form).serializeArray(), function(json) {
      if (!json.result && json.data !== null && typeof json.data.event !== "undefined") {
        if (json.data.event === "requiredSetup") {
          Pleasure.handleToastrSettings("Hata!", json.message, "error");
          location.href = base_url + "pointofsales";
        } else if (json.data.event === "requiredSession") {
          Pointofsales.OperatorSession.loadTabByStatus(json);
        }
        return;
      }
      callback(json);
    }, "json");
  },

  activeTabIs: function (tab) {
    let active_tab = Pointofsales.Helpers.getActiveTab() + "";
    tab = tab + ""
    return (active_tab === tab);
  },

  getActiveTab: function (tab) {
    return $("#main-tabs > .tab-pane.active").attr("id");
  },

  setPreviousTab: function (tab) {
    Pointofsales.previousTab = Pointofsales.Helpers.getActiveTab() + "";
  },

  getPreviousTab: function (tab) {
    return Pointofsales.previousTab;
  },

  round: function (value, decimals) {
    return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
  },

  addTax: function (amount, tax) {
    amount = parseFloat(amount),
    tax = parseFloat(tax);

    return Pointofsales.Helpers.round(amount + amount * tax / 100, 3);
  },

  calcTax: function (amount, tax) {
    amount = parseFloat(amount),
    tax = parseFloat(tax);

    return Pointofsales.Helpers.round(amount * tax / 100, 3);
  },

  countInterimPayments: function (product, callback) {
    let paids = 0;
    $.each(Pointofsales.settings.payment_types, function(index, payment_type) {
      paids += parseInt(product[`paid_${payment_type.alias}`])
    });
    callback(paids);
  },

  $_HASH: function (variable) {

    let 
    hash = location.hash.replace(/^.*?#/, ''),
    pairs = hash.split('&'),
    length = pairs.length;

    for (var i = 0; i < length; i++) {

      let val = pairs[i].split(':');

      if (typeof val[0] !== "undefined") {
        if (val[0] === variable) {
          return val[1];
        }
      }
    }
  },

  isEmpty: function (el){
    return !$.trim(el.html())
  },

  getDateNow: function () {
    let date = new Date();
    return `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
  },

  addLoader: function (target) {
    $(target).append(`
      <div class="loader-wrapper">
        <div class="loader-bussy food-loader-2"></div>
      </div>
    `)
  },

  removeLoader: function (target) {
    $(".loader-wrapper", target).remove();
  },

  secondsToHms: function (second) {
    second = Number(second);
    let 
    h = Math.floor(second / 3600)
    m = Math.floor(second % 3600 / 60),
    s = Math.floor(second % 3600 % 60),

    hDisplay = (h < 10 ? "0" : "") + h,
    mDisplay = (m < 10 ? "0" : "") + m,
    sDisplay = (s < 10 ? "0" : "") + s;

    return `${hDisplay}:${mDisplay}:${sDisplay}`; 
  },


  keypad: function (element) {
    $(element).keyboard({
      lang: "tr",
      layout: 'turkish-q',
      reposition : true,
      usePreview: true,
      autoAccept: true,
      css: {
        buttonDefault: 'btn btn-default btn-xl',
        container: 'keypad-container',
      }
    })
    // activate the typing extension
    .addTyping({
      showTyping: true,
      delay: 250
    })
    .addExtender({
      layout: 'numpad',
      showing: true,
      reposition: true
    });
  },

  numpad: function (element) {
    $(element).keyboard({
      lang: "tr",
      layout: 'numpad',
      reposition : true,
      usePreview: true,
      autoAccept: true,
      css: {
        buttonDefault: 'btn btn-xl',
        container: 'numpad-container',
      },
      beforeClick: function(e, keyboard, el){
        keyboard.$el.trigger('change');
      }
    })
    .addTyping({
      showTyping: true,
      delay: 250
    });
  },

  customNumpad: function (element) {
    $(element).keyboard({
      lang: "tr",
      layout: 'custom',
      reposition : true,
      usePreview: true,
      autoAccept: true,
      css: {
        buttonDefault: 'btn btn-xl',
        container: 'numpad-container',
      },
      customLayout: {
        'normal' : [
        '7 8 9 {b}',
        '4 5 6  {left} {right}',
        '1 2 3 {c}',
        '0 {dec} {clear} {a} '
        ]
      },
      restrictInput : true,
      beforeClick: function(e, keyboard, el){
        keyboard.$el.trigger('change');
      }
    })
    .addTyping({
      showTyping: true,
      delay: 250
    });
  },

  utcToLocal: function (date) {
    return moment
      .utc(date, "YYYY-MM-DD HH:mm:ss")
      .local()
      .format(base_date_format);
  },

  hasPermission: function (permission) {

    if (Pointofsales.settings.operator_session.user_role !== 'admin') {
        if ( Pointofsales.settings.operator_session.permissions.indexOf(permission) === -1 ) {
            return false;
        }
    }

    return true;
  },

  visible: function (selector, visibility ) {
    if (visibility === true) {
      $(selector).removeClass('hidden');
    } else {
      $(selector).addClass('hidden');
    }

  },

  visibleInput: function (selector, visibility ) {
    if (visibility === true) {
      $(selector).addClass('hidden');
    } else {
      $(selector).removeClass('hidden');
    }

  },

  setCookie: function(name,value,days) {
    var expires = "";
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
  },

  getCookie: function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  },

  doPrintAction: function (element) {

    if (Pointofsales.JSDesktopApp.status) {
        Pointofsales.JSDesktopApp.printContent(element.html());
      } else {
        element.print();
      }
  },

};




Pointofsales.Templates = {
  noContentFound: function (message) {
    return `
    <div class="jumbotron text-center">
      <h2><i class="fa fa-exclamation-circle fa-5x"></i></h2>
      <h4>${message}</h4>
    </div>`;
  }
};




Pointofsales.SoundRecorder = {
  audio_context: null,
  recorder: null,
  audio_stream: null,

  init: function () {
    let $this = this;
    try {
      // Monkeypatch for AudioContext, getUserMedia and URL
      window.AudioContext = window.AudioContext || window.webkitAudioContext;
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
      window.URL = window.URL || window.webkitURL;

      // Store the instance of AudioContext globally
      $this.audio_context = new AudioContext;
      console.log('Audio context is ready !');
      console.log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
      Pleasure.handleToastrSettings("Hata!", "Ses kayıt özelliği internet tarayıcınızda desteklenmiyor!", "warning");
    }
  },

  start: function () {
    let $this = this;
    // Access the Microphone using the navigator.getUserMedia method to obtain a stream
    navigator.getUserMedia({ audio: true }, function (stream) {
      // Expose the stream to be accessible globally
      $this.audio_stream = stream;
      // Create the MediaStreamSource for the Recorder library
      var input = $this.audio_context.createMediaStreamSource(stream);
      console.log('Media stream succesfully created');

      // Initialize the Recorder Library
      $this.recorder = new Recorder(input);
      console.log('Recorder initialised');

      // Start recording !
      $this.recorder && $this.recorder.record();
      console.log('Recording...');
    }, function (e) {
      console.error('No live audio input: ' + e);
    });
  },

  stop: function (callback, AudioFormat) {
    let $this = this;
    // Stop the recorder instance
    $this.recorder && $this.recorder.stop();
    console.log('Stopped recording.');

    // Stop the getUserMedia Audio Stream !
    $this.audio_stream.getAudioTracks()[0].stop();

    // Use the Recorder Library to export the recorder Audio as a .wav file
    // The callback providen in the stop recording method receives the blob
    if(typeof(callback) == "function"){

      /**
       * Export the AudioBLOB using the exportWAV method.
       * Note that this method exports too with mp3 if
       * you provide the second argument of the function
       */
      $this.recorder && $this.recorder.exportWAV(function (blob) {
        callback(blob);

        // create WAV download link using audio data blob
        // createDownloadLink();

        // Clear the Recorder to start again !
        $this.recorder.clear();
      }, (AudioFormat || "audio/wav"));
    }
  },

  save: function (blob, url, callback) {
    let
    formData = new FormData();
    formData.append('sound', blob);

    $.ajax({
      url: url,
      type: "POST",
      dataType: "json",
      data: formData,
      contentType: false,
      processData: false,
      success: function(json) {
        callback(json);
      }
    });
  }
};




Pointofsales.BarCodeScanner = {
  init: function () {
    $(document).scannerDetection({
      timeBeforeScanTest: 500, // wait for the next character for upto 200ms
      avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
      // startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
      // endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
      // ignoreIfFocusOn: 'input', // turn off scanner detection if an input has focus
      // scanButtonKeyCode: 116, // the hardware scan button acts as key 116 (F5)
      // scanButtonLongPressThreshold: 5, // assume a long press if 5 or more events come in sequence
      // onScanButtonLongPressed: showKeyPad, // callback for long pressing the scan button
      onComplete: function(barcode, qty) {
        
        console.log(`BarCode detected: ${barcode}`);

        $("#pos-products .product").each(function(index, product) {

          let
          product_bar_code = String($(product).data("bar-code"));
          scanned_bar_code = String(barcode);

          if (product_bar_code === scanned_bar_code) {
            $(product).trigger("click");
            return;
          }
        });

      },
      onError: function(string) {
        // console.log('Error: ' + string);
      }
    });
  },
};