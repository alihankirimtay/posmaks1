
var Reports = {

    index:function() {

    },

    charts:function(element, chart_data) {

        console.log(chart_data);

        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable(chart_data);

            var chart = new google.visualization.PieChart(document.getElementById(element));

            chart.draw(data);
        }
    },

    init:function (argument) {
        google.charts.load('current', {'packages':['corechart']});

        setTimeout(function () {
            $(window).trigger('resize');
        }, 1000);
    },



    init_Fullness:function (argument) {
        google.charts.load('current', {'packages': ['corechart', 'bar']});

        setTimeout(function () {
            $(window).trigger('resize');
        }, 1000);
    },
    charts_Fullness:function(element, chart_data) {

        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = new google.visualization.arrayToDataTable(chart_data);

            var chart = new google.visualization.ColumnChart(document.getElementById(element));
            console.log(data);
           //data.addColumn({ type: 'string', role: 'tooltip' });

            var options = {
                title: 'Günlük Doluluk Raporu (Saatlere Göre)',
                legend: {position: 'none'},
                vAxis: {textPosition: 'none'}
            };

            chart.draw(data, options);

        }
    },




    // endOfDay
    endOfDay: function () {
        
        $("body").on("click", "#create_end_of_day", function(e){
            e.preventDefault();
            
            var form = $(this).closest("form").serializeArray();

            $.post(base_url+'reports/ajax/createEndOfDay', form, function(data) {
                if (!data.result) {
                    Pleasure.handleToastrSettings('Hata!', data.message, 'error');
                } else {
                    window.location.reload()
                }
            }, "json");
        });
    },




    // Tickets
    tickets: function () {
        
        $("html, body").css("height", "100%");
        $("body .content").css("height", "80%");
    }
};

Reports.Adisyon = {
    settings:{
        sales_status_pie: {
            open: 0,
            close: 0
        },

        sale_service_type_doughnut: [],
        sale_service_type_amount: [],
        currency: ''
    },

    init: function(settings) {
        if (typeof settings !== "undefined") {
            $.extend(Reports.Adisyon.settings, settings);
        }
        Reports.Adisyon.pie();
        Reports.Adisyon.doughnut();
    },

    pie: function() {
        new Chart(document.getElementById("adisyonChart"), {
            type: 'pie',
            data: {
              labels: ["Açık Adisyonlar", "Kapalı Adisyonlar"],
              datasets: [{
                backgroundColor: ["#4FAD53", "#F1443C"],
                data: [Reports.Adisyon.settings.sales_status_pie.open, Reports.Adisyon.settings.sales_status_pie.close]
              }]
            },
            options: {
                legend: {
                    reverse:true
                }
            }
        });
    },

    doughnut: function() {
        new Chart(document.getElementById("paymentTypesChart"), {
            type: 'pie',
            data: {
                labels: [
                    `Normal Servis ( ${Reports.Adisyon.settings.sale_service_type_amount['1']} ${Reports.Adisyon.settings.currency} )`, 
                    `Paket Servis ( ${Reports.Adisyon.settings.sale_service_type_amount['2']} ${Reports.Adisyon.settings.currency} )`, 
                    `Self Servis ( ${Reports.Adisyon.settings.sale_service_type_amount['3']} ${Reports.Adisyon.settings.currency} )`],
              datasets: [{
                backgroundColor: ["#3e95cd", "#8e5ea2", "#e81d62"],
                data: [Reports.Adisyon.settings.sale_service_type_doughnut['1'], Reports.Adisyon.settings.sale_service_type_doughnut['2'], Reports.Adisyon.settings.sale_service_type_doughnut['3']]
              }]
            },
            options: {
                legend: {
                    reverse:true
                }
            }
        });

    }


};


Reports.Daily = {

    viewModal: "#modal-view",
    editModal: "#modal-edit",
    settings: {
        payment_types: {},
        cash_types: {},
    },

    init: function (settings) {

        if (typeof settings !== "undefined") {
            $.extend(Reports.Daily.settings, settings);
        }

        Reports.Daily.sortCashTypes();
        Reports.Daily.Events.init();
    },

    sortCashTypes: function () {
        var cashTypesSortDesc = [];
        for (var val in Reports.Daily.settings.cash_types)
            cashTypesSortDesc.push([val, Reports.Daily.settings.cash_types[val]]);
        cashTypesSortDesc.sort(function(a, b) { return b[1] - a[1]; });

        Reports.Daily.settings.cash_types = cashTypesSortDesc;
    },

    Events: {
        init: function () {
            $("body").on("click", ".view", Reports.Daily.Events.onClickViewSession);
            $("body").on("click", ".edit", Reports.Daily.Events.onClickEditSession);
            $("body").on("click", `${Reports.Daily.editModal} #update_session`, Reports.Daily.Events.onUpdateSession);
            $("body").on("change input", $("input[name^=cashs], input[name='cash_opening']", Reports.Daily.editModal), Reports.Daily.Events.onChangeCash);
        },

        onClickViewSession: function (event) {
            event.preventDefault();

            Reports.Daily.Templates.readonly = "readonly";

            try {

                let
                session = $(this).attr("href");
                session = $.parseJSON(session);

                Reports.Daily.Templates.sessionView(session, function (content) {
                    $("#cc-username", Reports.Daily.viewModal).html(session.user_name);
                    $("#viewContent", Reports.Daily.viewModal).html(content);
                });

                $(Reports.Daily.viewModal).modal("toggle");

            } catch (e) {
                console.error(e);
            }
        },

        onClickEditSession: function (event) {
            event.preventDefault();

            let
            id = $(this).data("id"),
            form = $("form", Reports.Daily.editModal);
            $("input[name='session_id']", form).val(id);

            Reports.Daily.Templates.readonly = "";

            Reports.Daily.Templates.sessionEdit(function (content) {
                $("#editContent", Reports.Daily.editModal).html(content);
            });

            $(Reports.Daily.editModal).modal("toggle");
        },

        onUpdateSession: function (event) {
            let 
            form = $("form", Reports.Daily.editModal);

            $.post(base_url + "reports/ajax/updateSession", form.serializeArray(), function(json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings('Hata!', json.message, 'error');
                    return;
                }

                window.location.reload();
                
            }, "json");
        },

        onChangeCash: function (event) {
            let total = 0;

            $.each($("input[name^=cashs]", Reports.Daily.editModal), function(index, val) {

                let
                input = $(this),
                quantity = parseFloat(input.val()) || 0,
                value = input.data("value"),
                inputResult = input.parent().parent().find(".money_result"),
                amount = (value === "drop") ? quantity : (quantity * value);

                inputResult.val(amount.toFixed(2));

                total += amount;
            });

            let cash_opening = $("input[name='cash_opening']", Reports.Daily.editModal).val();
            cash_opening = parseFloat(cash_opening) || 0;

            total -= cash_opening;

            $("input[name='payments[cash]']", Reports.Daily.editModal).val(total.toFixed(2));
        },
    },

    Templates: {
        readonly: "",

        sessionView: function (session, callback) {
            let
            content = "";

            $.each(session.values, function(index, value) {
                $.each(Reports.Daily.settings.cash_types, function(index1, cash_type) {
                    let
                    cashKey = cash_type[0],
                    cashValue = cash_type[1];

                    content += Reports.Daily.Templates.cashInput(cashValue, cashKey, value.cashs[cashKey]);
                });
                content += Reports.Daily.Templates.cashDropInput(value.cashs.drop);

                content += '<div class="col-sm-12 p-b-3 p-x-0">';
                $.each(Reports.Daily.settings.payment_types, function(index2, payment_type) {
                    content += Reports.Daily.Templates.paymentInput(payment_type, session[payment_type.alias]);
                });
                content += Reports.Daily.Templates.paymentOpeningInput(value.cash_opening);
                content += '</div>';
                
            });


            callback(content);
        },

        sessionEdit: function (callback) {
            let
            content = "";

            $.each(Reports.Daily.settings.cash_types, function(index1, cash_type) {
                let
                cashKey = cash_type[0],
                cashValue = cash_type[1];

                content += Reports.Daily.Templates.cashInput(cashValue, cashKey, "");
            });
            content += Reports.Daily.Templates.cashDropInput("");

            content += '<div class="col-sm-12 p-b-3 p-x-0">';
            $.each(Reports.Daily.settings.payment_types, function(index2, payment_type) {
                content += Reports.Daily.Templates.paymentInput(payment_type, "");
            });
            content += Reports.Daily.Templates.paymentOpeningInput("");
            content += '</div>';
                


            callback(content);
        },

        cashInput: function (cash_value, cash_alias, quantity) {
            return `
            <div class="col-sm-3 p-a-0">
                <div class="col-sm-4 p-a-0"><h4>${cash_value} x</h4></div>
                <div class="col-sm-4 p-a-0"><input type="text" class="form-control" name="cashs[${cash_alias}]" value="${quantity}" data-value="${cash_value}" autocomplete="off" ${Reports.Daily.Templates.readonly}></div>
                <div class="col-sm-4 p-a-0"><input type="text" class="form-control money_result" value="0.00" readonly></div>
            </div>`;
        },

        cashDropInput: function (value) {
            return `
            <div class="col-sm-3 p-a-0">
                <div class="col-sm-4 p-a-0"><h4 class="text-red">Miktar </h4></div>
                <div class="col-sm-4 p-a-0"><input type="text" class="form-control" name="cashs[drop]" value="${value}" data-value="drop" autocomplete="off" ${Reports.Daily.Templates.readonly}></div>
                <div class="col-sm-4 p-a-0"><input type="text" class="form-control money_result" value="0.00" readonly></div>
            </div>`;
        },

        paymentInput: function (payment_type, value) {
            value = Number(value);
            if (payment_type.alias === "cash") {
                return `
                <div class="col-sm-3 p-a-0">
                    <h3 class="m-b-0">${payment_type.title}</h3>
                    <p class="payments[cash]"><input type="text" name="payments[cash]" class="form-control text-center" value="${value.toFixed(2)}" placeholder="0.00" autocomplete="off" readonly></p>
                </div>`;
            } else {
                return `
                <div class="col-sm-3 p-a-0">
                    <h3 class="m-b-0">${payment_type.title}</h3>
                    <p class="payments[${payment_type.alias}]"><input type="text" name="payments[${payment_type.alias}]" class="form-control text-center" value="${value.toFixed(2)}" placeholder="0.00" autocomplete="off" ${Reports.Daily.Templates.readonly}></p>
                </div>`;
            }
        },

        paymentOpeningInput: function (value) {
            value = Number(value);
            return `
            <div class="col-sm-3 p-a-0">
                <h3 class="m-b-0 text-red">Açılış Miktarı</h3>
                <p class="cash_opening"><input type="text" name="cash_opening" class="form-control text-center" value="${value.toFixed(2)}" placeholder="0.00" autocomplete="off" ${Reports.Daily.Templates.readonly}></p>
            </div>`;
        }
    }
};



Reports.Home = {

    init: function () {
        Reports.Home.Events.init();
    },

    Events: {
        init: function () {
            $("body").on("click", "#print-dashboard", Reports.Home.Events.onClickPrint);
            $("body").on("click", "#print-dashboard2", Reports.Home.Events.onClickPrint);
        },

        onClickPrint: function () {
            $("#print-dashboard-modal").modal("toggle");
            $("#print-dashboard-modal .modal-body").print();
        },
    }
};

Reports.Summary = {

    init: function () {
        Reports.Summary.Events.init();
    },

    Events: {
        init: function () {
            $("body").on("click", "#print-summary", Reports.Summary.Events.onClickPrint);
            $("body").on("click", "#print-summary2", Reports.Summary.Events.onClickPrint);

        },

        onClickPrint: function () {
            $("#print-summary-modal").modal("toggle");
            $("#print-summary-modal .modal-body").print();
        },
    }
};