var Cari = {

    init: function () {
        Cari.Events.init();
    },

    Events: {

        init: function () {

            $('body').on('click', '.open-payment-modal', Cari.Events.openPaymentModal);
            $('body').on('click', '.open-receipt-modal', Cari.Events.openReceiptModal);
            $('body').on('click', '.complete-transaction', Cari.Events.onClickTransaction);
            $('body').on('click', '#transferTransaction', Cari.Events.onClickTransfer);
            $('body').on('click', '.complete-transfer', Cari.Events.onClickCompleteTransfer);

            $('body').on('change', '#cari-add-payment .location, #cari-add-receipt .location', Cari.Events.onChangeLocation);
            $('body').on('change', '#cari-transfer-modal select[name="sender_location_id"]', Cari.Events.onChangeTransferSenderLocation);
            $('body').on('change', '#cari-transfer-modal select[name="recipient_location_id"]', Cari.Events.onChangeTransferRecipientLocation);



        },

        onChangeLocation: function () {

            let
            modal = $(this).closest('.modal'),
            location_id = $(this).val();

            Cari.Fill.fillAccounts(modal, location_id);

            $('input[name="account-type"]:eq(0)', modal).trigger('click');

        },

        onChangeTransferSenderLocation: function () {
            let
            modal = $(this).closest('.modal'),
            location_id = $(this).val(),
            selectorsBank = 'select[name="bank_sender_id"]',
            selectorsPos = 'select[name="pos_sender_id"]';


            Cari.Fill.fillBanks(modal, selectorsBank, location_id);
            Cari.Fill.fillPos(modal, selectorsPos, location_id);

            $('input[name="sender-account-type"]:eq(0)', modal).trigger('click');

        },

        onChangeTransferRecipientLocation: function () {
            let
            modal = $(this).closest('.modal'),
            location_id = $(this).val(),
            selectorsBank = 'select[name="bank_recipient_id"]',
            selectorsPos = 'select[name="pos_recipient_id"]';


            Cari.Fill.fillBanks(modal, selectorsBank, location_id);
            Cari.Fill.fillPos(modal, selectorsPos, location_id);

            $('input[name="recipient-account-type"]:eq(0)', modal).trigger('click');

        },

        openPaymentModal: function () {

            let 
            modal = $('#cari-add-payment'),
            tr = $(this).closest('tr'),
            id = tr.data('id');

            modal.modal('toggle');

            Cari.Fill.fillCustomers(modal, id);

            $('select[name="location_id"]', modal).trigger('change');

        },

        openReceiptModal: function () {

            let
            modal = $('#cari-add-receipt'),
            tr = $(this).closest('tr'),
            id = tr.data('id');

            modal.modal('toggle');

            Cari.Fill.fillCustomers(modal, id);

            $('select[name="location_id"]', modal).trigger('change');

        },

        onClickTransaction: function () {

            let 
            btn = $(this),
            form = btn.closest('form'),
            modal = btn.closest('.modal');

            Cari.Helpers.postForm(form,"cari/ajax/complete", function (json) {


                if (json.result === 1) {

                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    modal.modal('toggle');
                    $('input[name=amount], input[name=desc]',modal).val("");
                    location.reload();

                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");

                }


            });


        },

        onClickTransfer: function () {

            let 
            modal = $('#cari-transfer-modal'),
            selectorsBanks = 'select[name="bank_sender_id"], select[name="bank_recipient_id"]',
            selectorsPos = 'select[name="pos_sender_id"], select[name="pos_recipient_id"]';

            modal.modal('toggle');

            $('select[name="sender_location_id"], select[name="recipient_location_id"]', modal).trigger('change');

            // Cari.Fill.fillBanks(modal, selectorsBanks);
            // Cari.Fill.fillPos(modal, selectorsPos);

            $('input[name="sender-account-type"]:eq(0)', modal).trigger('click');
            $('input[name="recipient-account-type"]:eq(0)', modal).trigger('click');

        },

        onClickCompleteTransfer: function () {

            let
            btn = $(this),
            form = btn.closest('form'),
            modal = btn.closest('.modal');

            Cari.Helpers.postForm(form, "cari/ajax/transfer", function (json) {

                if (json.result === 1) {

                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    modal.modal('toggle');
                    $('input[name=amount], input[name=desc]', modal).val("");

                } else {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                }


            });

        }

    },

    Fill: {

        fillCustomers(modal, id ) {

            Cari.Helpers.postForm(null,"cari/ajax/getCustomers", function(json) {

                $('select[name="customers"]', modal).html("");

                if (json.result === 1) {

                    $.each(json.data.customers, function (index, customer) { 
                        
                        $('select[name="customers"]', modal).append(`
                            <option value="${customer.id}">${customer.name}</option>
                        `)

                    });

                    $('select[name="customers"]', modal).trigger("chosen:updated");
                    
                    if (id) {
                        let option = $('select[name="customers"]', modal);
                        option.val(id);
                        option.trigger('chosen:updated');
                    }

                }

            });

        },


        fillAccounts(modal, location_id) {
            
            Cari.Helpers.postForm(null, `cari/ajax/getPosAndBanks?location_id=${location_id}`, function (json) {

                $('select[name="bank[id]"]', modal).html("");
                $('select[name="pos[id]"]', modal).html("");

                if (json.result === 1) {

                    $.each(json.data.banks, function (index, bank) {

                        $('select[name="bank[id]"]', modal).append(`
                            <option value="${bank.id}">${bank.title}</option>
                        `)

                    });

                    $.each(json.data.pos, function (index, pos) {

                        $('select[name="pos[id]"]', modal).append(`
                            <option value="${pos.id}">${pos.title}</option>
                        `)

                    });

                    $('select[name="bank[id]"]', modal).trigger("chosen:updated");
                    $('select[name="pos[id]"]', modal).trigger("chosen:updated");

                }

            });

            

        },

        fillBanks(modal, selector, location_id) {
            Cari.Helpers.postForm(null, `cari/ajax/getBanks?location_id=${location_id}`, function (json) {

                $(selector, modal).html("");

                if (json.result === 1) {

                    $.each(json.data.banks, function (index, bank) {

                        $(selector, modal).append(`
                            <option value="${bank.id}">${bank.title}</option>
                        `)

                    });

                    $(selector, modal).trigger("chosen:updated");

                }

            });


        },

        fillPos(modal, selector, location_id) {
            Cari.Helpers.postForm(null, `cari/ajax/getPos?location_id=${location_id}`, function (json) {

                $(selector, modal).html("");

                if (json.result === 1) {

                    $.each(json.data.pos, function (index, pos) {

                        $(selector, modal).append(`
                            <option value="${pos.id}">${pos.title}</option>
                        `)

                    });

                    $(selector, modal).trigger("chosen:updated");

                }

            });

        }

    },

    

};

Cari.Helpers = {

    postForm: function (form, url, callback) {
        $.post(base_url + url, $(form).serializeArray(), function (json) {
            callback(json);
        }, "json");
    },

    getDateNow: function () {
        let date = new Date();
        return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
    },

};