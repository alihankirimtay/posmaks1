var Sales = {

    container: "#sale",
    settings: {
        location_currency: "",
    },
    sale: {},
    payment_types: {},
    customers: {},

    init: function (sale) {

        if (typeof sale !== "undefined") {
            Sales.sale = sale;
        } else {
            Sales.sale = null;
        }

        

        Sales.createPaymentTypeInputs();

        Sales.Events.init();

        if (Sales.sale) {
            Sales.loadSale();

            if (Sales.sale.cari) {
                Sales.loadSaleCariInputs();
            }
        }
    },

    setPaymentTypes: function (payment_types) {
        Sales.payment_types = payment_types;
    },

    setCustomers: function (customers) {
        Sales.customers = customers;
    },

    loadSale: function () {
        $("select[name='location_id']", Sales.container).val(Sales.sale.location_id).trigger("chosen:updated").trigger("change");

        $("input[name='payment_date']", Sales.container).val(Sales.sale.payment_date_client);
        $("input[name='discount_amount']", Sales.container).val(Sales.sale.discount_amount);
        $("select[name='discount_type']", Sales.container).val(Sales.sale.discount_type);
    },

    loadSaleProducts: function () {
      $.each(Sales.sale.products, function(index, product) {

            let form = `<form><input name="product_id" value="${product.product_id}"></form>`;
            Sales.Helpers.postForm(form, "products/ajax/getProductWithRelatedItems", function (json) {

                if (json.result) {
                    Sales.Templates.productRow(json.data.product, function (product_template) {
                        $("#products", Sales.container).append(product_template);
                    }, product);
                }

                Sales.calculate();
            }); 
        });  
    },

    loadSaleCariInputs: function () {

        let editable = true;
        $.each(Sales.sale.sale_cari, function (index, cari) {

            let input = Sales.Templates.cariInput(editable);
            $(`${Sales.container} #payment_cari`).append(input);
            $(".chosen-select").chosen({ width: '100%' });
            
            let customer_element = $('select[name="customer_id"]', input);
            $.each(Sales.customers, function (index, customer) {
                customer_element.append(`<option value="${customer.id}">${customer.name}</option>`);

                if(cari.customer_id === customer.id) {
                    customer_element.val(customer.id).trigger('chosen:updated');

                    let
                    row = customer_element.closest('.cari-container'),
                    input = $('input[name*="payments[cari]"]', row);

                    input.attr('name', `payments[cari][${customer.id}]`);
                    input.val(cari.amount);
                    row.attr('data-id', cari.id);
                
                }
            });
            
            customer_element.trigger("chosen:updated");
            
        });

    },

    createPaymentTypeInputs: function () {

        let
        i = 0,
        grouped_payment_types = {};
        $.each(Sales.payment_types, function(index, _payment_type) {
            if (index % 3 === 0) {
                i++;
                grouped_payment_types[i] = {};
            }
            grouped_payment_types[i][_payment_type.id] = _payment_type;
        });

        $.each(grouped_payment_types, function(key1, payment_types) {
            $.each(payment_types, function(key2, payment_type) {

                let
                value = 0, 
                input;

                if (Sales.sale) {
                    value = Sales.sale[payment_type.alias];
                }

                if (payment_type.alias !== "cari")
                    input = $(Sales.Templates.paymentInput(payment_type, value));

                $("#payment_types").append(input);
            });  
        });  
    },

    calculate: function () {

        let
        total = 0,
        subtotal = 0,
        taxtotal = 0,
        paidtotal = 0,
        intermpaymetntotal = 0,
        discountamount = parseFloat($("input[name='discount_amount']", Sales.container).val()) || 0,
        discounttype = String($("select[name='discount_type']", Sales.container).val()),
        rest = 0,
        change = 0;

        $.each($(".product-container", Sales.container), function(key, row) {
            
            let
            product = $("input[name*='[quantity]']", row),
            quantity = parseInt(product.val()),
            price = parseFloat(product.attr("price")),
            tax = parseFloat(product.attr("tax")),
            is_gift = (product.attr("is_gift") === "1"),
            product_total = 0;

            tax = Sales.Helpers.calcTax(price, tax);
            tax *= quantity;
            price *= quantity;

            if (!is_gift) {
                taxtotal += tax;
                subtotal += price;
                total += price + tax;
                product_total = price + tax;
            }

            // Loop additional products
            $.each($(`input[name*="[additional_products]"]:checked`, row), function(key, _additional_product) {

                let
                additional_product = $(_additional_product)
                additional_product_price = parseFloat(additional_product.attr("price")),
                additional_product_tax = parseFloat(additional_product.attr("tax"));

                additional_product_tax = additional_product_price * additional_product_tax / 100;
                additional_product_tax = Sales.Helpers.round(additional_product_tax, 2);
                additional_product_tax *= quantity;
                additional_product_price *= quantity;

                if (!is_gift) {
                    taxtotal += additional_product_tax;
                    subtotal += additional_product_price;
                    total += additional_product_price + additional_product_tax;
                    product_total += additional_product_price + additional_product_tax;
                }

            });

            $(".product_total", row).val(product_total.toFixed(2));
        });

        // Calculate Paids in cash, credit, voucher, ticket, sodexo
        $.each($(`input[name^="payments"]`, Sales.container), function(index, input) {
            let paid = $(input).val().replace(/[^0-9\.]/g, "");
            paid = Sales.Helpers.round(paid, 2);
            paidtotal += parseFloat(paid);
        });

        if (discounttype === "amount") {
            total -= discountamount;
        } else if (discounttype === "percent") {
            total -= Sales.Helpers.round(total * discountamount / 100, 2);
        }

        rest = total - (intermpaymetntotal + paidtotal);
        change = (intermpaymetntotal + paidtotal) - total;
        if (change < 0) {
            change = 0;
        }

        $(".subtotal", Sales.container).val(subtotal.toFixed(2));
        $(".taxtotal", Sales.container).val(taxtotal.toFixed(2));
        $(".total", Sales.container).val(total.toFixed(2) +" "+ Sales.settings.location_currency);
        $(".paid", Sales.container).val(paidtotal.toFixed(2));
        $(".rest", Sales.container).val(rest.toFixed(2));
        $(".change", Sales.container).val(change.toFixed(2));
    },

    Events: {
        init: function () {
            $("body").on("change", `${Sales.container} select[name='location_id']`, Sales.Events.onChangeLocation);
            $("body").on("change", `${Sales.container} select[name='floor_id']`, Sales.Events.onChangeFloor);
            $("body").on("change", `${Sales.container} select[name='zone_id']`, Sales.Events.onChangeZone);
            $("body").on("change", `${Sales.container} select[name='product_id']`, Sales.Events.onChangeProduct);

            $("body").on("click", `${Sales.container} .remove`, Sales.Events.onClickRemoveProduct);

            //Cari 
            $("body").on("click", `${Sales.container} #addPaymentCari`, Sales.Events.onClickAddCari);
            $("body").on("click", `${Sales.container} .remove-cari`, Sales.Events.onClickRemoveCari);
            $("body").on("change", `${Sales.container} select[name='customer_id']`, Sales.Events.onChangeCustomer);
            $("body").on("click", `${Sales.container} .remove-cari-edit`, Sales.Events.onClickRemoveCariEdit);




            // Trigger Calculate
            $("body").on("change input", `${Sales.container} input[name^='payments']`, Sales.calculate);
            $("body").on("change input", `${Sales.container} input[name*='[quantity]']`, Sales.calculate);
            $("body").on("change input", `${Sales.container} input[name='discount_amount']`, Sales.calculate);
            $("body").on("change", `${Sales.container} select[name='discount_type']`, Sales.calculate);
            $("body").on("change", `${Sales.container} input[name*='[additional_products]']`, Sales.calculate);
        },

        onChangeLocation: function (event) {

            let
            location_id = Number($(this).val()),
            currency = $("option:selected", this).data("currency") || "#";

            Sales.settings.location_currency = currency;
            $("select[name='discount_type'] option[value='amount']").html(currency);

            Sales.Events.emptyInputsForLocation();

            Sales.calculate();

            if (!location_id)
                return;

            Sales.Helpers.postForm(Sales.container, "pos/ajax/getPosesByLocationId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let pos_element = $("select[name='pos_id']", Sales.container);
                $.each(json.data.poses, function(index, pos) {
                    pos_element.append($("<option>").val(pos.id).html(pos.title));
                });
                if (Sales.sale) {
                    pos_element.val(Sales.sale.pos_id);
                }
                pos_element.trigger("chosen:updated");
            });

            Sales.Helpers.postForm(Sales.container, "users/ajax/getAdminsAndUsersByLocationId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let completer_user_element = $("select[name='completer_user_id']", Sales.container);
                $.each(json.data.users, function(index, completer_user) {
                    completer_user_element.append($("<option>").val(completer_user.id).html(completer_user.username +" - "+ completer_user.email));
                });
                if (Sales.sale) {
                    completer_user_element.val(Sales.sale.completer_user_id);
                }
                completer_user_element.trigger("chosen:updated");
            });

            Sales.Helpers.postForm(Sales.container, "floors/ajax/getFloorsByLocationId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let floor_element = $("select[name='floor_id']", Sales.container);
                $.each(json.data.floors, function(index, floor) {
                    floor_element.append($("<option>").val(floor.id).html(floor.title));
                });
                if (Sales.sale) {
                    floor_element.val(Sales.sale.floor_id);
                }
                floor_element.trigger("chosen:updated").trigger("change");
            });

            Sales.Helpers.postForm(Sales.container, "products/ajax/getProductsByLocationId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let product_element = $("select[name='product_id']", Sales.container);
                $.each(json.data.products, function(index, product) {
                    product_element.append($("<option>").val(product.id).html(product.title));
                });
                product_element.trigger("chosen:updated").trigger("change");
            });
        },

        onChangeFloor: function (event) {

            Sales.Events.emptyInputsForFloor();

            let
            floor_id = Number($(this).val());

            if (!floor_id)
                return;

            Sales.Helpers.postForm(Sales.container, "zones/ajax/getZonesByFloorId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let zone_element = $("select[name='zone_id']", Sales.container);
                $.each(json.data.zones, function(index, zone) {
                    zone_element.append($("<option>").val(zone.id).html(zone.title));
                });
                if (Sales.sale) {
                    zone_element.val(Sales.sale.zone_id);
                }
                zone_element.trigger("chosen:updated").trigger("change");
            });
        },

        onChangeZone: function (event) {
            
            Sales.Events.emptyInputsForZone();

            let
            zone_id = Number($(this).val());

            if (!zone_id)
                return;

            Sales.Helpers.postForm(Sales.container, "points/ajax/getPointsByZoneId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let point_element = $("select[name='point_id']", Sales.container);
                $.each(json.data.points, function(index, point) {
                    point_element.append($("<option>").val(point.id).html(point.title));
                });
                if (Sales.sale) {
                    point_element.val(Sales.sale.point_id);
                    Sales.loadSaleProducts();
                }
                point_element.trigger("chosen:updated");

            });
        },

        onChangeProduct: function (event) {

            let
            product_id = Number($(this).val());

            if (!product_id)
                return;

            Sales.Helpers.postForm(Sales.container, "products/ajax/getProductWithRelatedItems", function (json) {

                $("select[name='product_id']", Sales.container).val("").trigger('chosen:updated');

                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                json.data.product.index = -new Date();
                json.data.product.quantity = 1;
                json.data.product.sale_product_id = "";

                Sales.Templates.productRow(json.data.product, function (product_template) {
                    $("#products", Sales.container).append(product_template);
                });

                Sales.calculate();
            });
        },

        onClickRemoveProduct: function (event) {
            let row = $(this).closest(".product-container");

            row.fadeOut(300, function() {
                row.remove();

                Sales.calculate();
            });
        },

        //Cari
        onClickAddCari: function () {

            let 
            editable =false,
            input = Sales.Templates.cariInput(editable);
            $(`${Sales.container} #payment_cari`).append(input);
            $(".chosen-select").chosen({ width: '100%' });

            let customer_element = $('select[name="customer_id"]', input);

            $.each(Sales.customers, function (index, customer) {
                customer_element.append(`<option value="${customer.id}">${customer.name}</option>`);
            });

            customer_element.trigger("chosen:updated");

        },

        onClickRemoveCari: function () {

            let row = $(this).closest('.cari-container');

            row.fadeOut(300, function () {
                row.remove();

                Sales.calculate();
            });
        },

        onClickRemoveCariEdit: function () {
            let 
            row = $(this).closest('.cari-container'),
            id = row.data('id');

            if (!confirm('Silmek istediğinize emin misiniz? Bu işlem geri alınamaz.')) return false;

            Sales.Helpers.postForm(null, `sales/ajax/salesCariDelete?id=${id}`, function (json) {
                if (json.result === 1) {
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");

                    row.fadeOut(300, function () {
                        row.remove();

                        Sales.calculate();
                    });

                } else {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");                        
                }
            });

        },

        onChangeCustomer: function () {

            let 
            id = $(this).val(),
            row = $(this).closest('.cari-container'),
            input = $('input[name*="payments[cari]"]', row);

            input.attr('name', `payments[cari][${id}]`);

        },

        
        emptyInputsForLocation: function () {
            // Selects
            let 
            selects = $("select[name='pos_id'], select[name='floor_id'], select[name='completer_user_id'], select[name='zone_id'], select[name='point_id'], select[name='product_id']", Sales.container);
            selects.find("option:not(:first)").remove();
            selects.trigger("chosen:updated");

            // Elements
            $("#products", Sales.container).html("");
        },

        emptyInputsForFloor: function () {
            // Selects
            let 
            selects = $("select[name='zone_id'], select[name='point_id']", Sales.container);
            selects.find("option:not(:first)").remove();
            selects.trigger("chosen:updated");

            // Elements
            $("#products", Sales.container).html("");
        },

        emptyInputsForZone: function () {
            // Selects
            let 
            selects = $("select[name='point_id']", Sales.container);
            selects.find("option:not(:first)").remove();
            selects.trigger("chosen:updated");

            // Elements
            $("#products", Sales.container).html("");
        },
    },

    Helpers: {
        postForm: function (form, url, callback) {
            $.post(base_url + url, $(form).serializeArray(), function(json) {
              callback(json);
            }, "json");
        },

        round: function (value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        },

        addTax: function (amount, tax) {
            amount = Number(amount),
            tax = Number(tax);

            return Sales.Helpers.round(amount + amount * tax / 100, 2);
        },

        calcTax: function (amount, tax) {
            amount = Number(amount),
            tax = Number(tax);

            return Sales.Helpers.round(amount * tax / 100, 2);
        },
    },

    Templates: {
        productRow: function (product, callback, editing_product) {
            let
            quantity = Number(product.quantity),
            price = Sales.Helpers.addTax(product.price, product.tax),
            row;

            if (typeof editing_product !== "undefined") {
                product.sale_product_id = editing_product.id;
                product.index = editing_product.id;
                quantity = editing_product.quantity;
            }

            row = $(`
            <div class="col-md-9 col-md-offset-3 alert alert-primary-transparent p-a-0 m-b-1 product-container">
                <input type="hidden" name="products[${product.index}][product_id]" value="${product.id}">
                <input type="hidden" name="products[${product.index}][sale_product_id]" value="${product.sale_product_id}">
                <div class="col-md-4">
                    <label class="control-label col-md-3">Ürün:</label>
                    <div class="col-md-9">
                        <select class="form-control" name="portion_id">
                            <option value="${product.id}">${product.title}</option>
                        </select>
                    </div>
                    <label class="control-label col-md-3">Fiyat:</label>
                    <div class="col-md-9">
                        <input type="text" value="${price.toFixed(2)}" class="form-control" readonly>
                    </div>
                    <label class="control-label col-md-3">Miktar:</label>
                    <div class="col-md-9">
                        <input type="number" name="products[${product.index}][quantity]" value="${quantity}" class="form-control" step="1" min="1" price="${product.price}" tax="${product.tax}" tax="${product.is_gift}">
                    </div>
                    <label class="control-label col-md-3">Toplam:</label>
                    <div class="col-md-9">
                        <input type="text" value="0.00" class="form-control product_total" readonly>
                    </div>
                </div>
                <div class="col-md-4 stocks">
                    <b>Malzemeler:</b>
                </div>
                <div class="col-md-4 additional_products">
                    <b>İlave Ürünler:</b>
                </div>
                <button type="button" class="btn btn-danger btn-xs remove"><i class="fa fa-trash"></i> Sil</button>
            </div>
            `);

            $.each(product.portions, function(index, portion) {
                $("select[name='portion_id']", row).append( $('<option></option>').val(portion.id).html(portion.title) );
            });

            if (product.type === "product") {
                Sales.Templates.productRowStocks(product, row, function (stocks) {
                    $(".stocks", row).append(stocks);
                }, editing_product);
            }

            Sales.Templates.productRowAdditionalProducts(product, row, function (additional_products) {
                $(".additional_products", row).append(additional_products);
            }, editing_product);

            callback(row);
        },

        productRowStocks: function (product, row, callback, editing_product) {
            let 
            stocks = "";

            $.each(product.stocks, function(index, stock) {

                if (stock.optional === "0") {
                    return;
                }

                stock.in_use = 1;

                if (typeof editing_product !== "undefined") {
                    $.each(editing_product.stocks, function(index1, _stock) {
                        if (_stock.stock_id === stock.id) {
                            stock.in_use = Number(_stock.in_use);
                            return;
                        }
                    });
                }

                stock.in_use = stock.in_use ? "" : "checked";

                stocks += `
                <div class="checkbox p-t-0">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="products[${product.index}][stocks][${stock.id}]" value="${stock.id}" class="strikethrough hidden" ${stock.in_use}>
                        <span class="text-black">${stock.title}</span>
                    </label>
                </div>
                `;
            });

            callback(stocks);
        },

        productRowAdditionalProducts: function (product, row, callback, editing_product) {
            let 
            additional_products = "";

            $.each(product.additional_products, function(index, additional_product) {
                let
                price = Sales.Helpers.addTax(additional_product.price, additional_product.tax);

                additional_product.selected = 0;

                if (typeof editing_product !== "undefined") {
                    $.each(editing_product.additional_products, function(index1, _additional_product) {
                        if (_additional_product.product_id === additional_product.id) {
                            additional_product.selected = 1;
                            return;
                        }
                    });
                }

                additional_product.selected = additional_product.selected ? "checked" : "";

                additional_products += `
                <div class="checkbox p-t-0">
                    <label>
                        <input type="checkbox" name="products[${product.index}][additional_products][${additional_product.id}]" value="${additional_product.id}" price="${additional_product.price}" tax="${additional_product.tax}" ${additional_product.selected}>
                        <span class="text-black">${additional_product.title} <small>(+${price.toFixed(2)} TL)</small></span>
                    </label>
                </div>
                `;
            });

            callback(additional_products);
        },

        paymentInput: function (payment_type, value) {
            return `
            <div class="col-md-4">
                <div class="input-group">
                    <span class="input-group-addon">${payment_type.title}</span>
                    <div class="inputer">
                        <div class="input-wrapper">
                            <input type="number" class="form-control text-right" min="0" step="0.01" name="payments[${payment_type.alias}][]" value="${value}" placeholder="0.00">
                        </div>
                    </div>
                </div>
            </div>`;
        },

        cariInput: function (editable) {
            let 
            edit = "",
            readonly = "";
            if (editable) {
                edit = "-edit";
                readonly = "readonly";
            }
            
            return $(`
            <div class="alert alert-primary-transparent col-md-12 cari-container">
                <div class="col-md-6">
                    <select name="customer_id" class="chosen-select">
                        <option value="0">Müşteri Seçiniz</option>
                    </select>
                </div>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"></span>
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input type="number" class="form-control text-right" min="0" step="0.01" name="payments[cari][]" value="" placeholder="0.00" ${readonly}>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-danger btn-xs remove-cari${edit}"><i class="fa fa-trash"></i> Sil</button>
                </div>
            </div>
            `);

        }
    },

    filter_container: "form#filter",
    pos_id: null,
    index:function(pos_id) {

        Sales.pos_id = pos_id;

        $("body").on("click", "input[name='date_start'], input[name='date_end']",function(){
            $("select[name='date_filter']").val("custome").trigger("chosen:updated");
        });

        // Get pos points by location.
        $("body").on("change", "select[name='location_id']", function() {


            Sales.Helpers.postForm(Sales.filter_container, "pos/ajax/getPosesByLocationId", function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let pos_element = $("select[name='pos_id']", Sales.filter_container);
                pos_element.find("option:not(:first)").remove();
                $.each(json.data.poses, function(index, pos) {
                    pos_element.append($("<option>").val(pos.id).html(pos.title));
                });
                if (Sales.pos_id) {
                    pos_element.val(Sales.pos_id);
                }
                pos_element.trigger("chosen:updated");
            });
        });
    },

    add:function() {

        // Get products by location.
        var currency = NaN;
        $("body").on("change", "select[name='location']", function(){

            var _tax = $(this).find('option[value="' + $(this).val() + '"]').data("tax");
            currency = $(this).find('option[value="' + $(this).val() + '"]').data("currency");

        	$.post( base_url + 'products/ajax', {
        		target: "products",
        		location: $(this).val()
        	}, function(data) {

        		var json = $.parseJSON(data);

        		var products = $("select[name='product']");
        		products.children("option").not("[value='']").remove();
        		if(json.data) {
	        		$.each(json.data, function(index, val) {

	        			products.append( $('<option data-amount="'+val.price+'" data-tax="'+val.tax+'"></option>').val(val.id).html(val.title) );
	        		});
        		}
        		products.trigger("chosen:updated");
                $("#product-list").html("");
                $("input[name='subtotal']").val("0.00");
        	});
        });

        // Get pos points ADN tables/desks by location.
        $("body").on("change", "select[name='location']", function(){

            $.post( base_url + 'sales/ajax', {
                target: "pos",
                location: $(this).val()
            }, function(data) {

                var json = $.parseJSON(data);
                var pos = $("select[name='pos']");

                pos.children("option").remove();
                if(json.data) {
                    $.each(json.data, function(index, val) {
                        pos.append( $('<option></option>').val(val.id).html(val.title) );
                    });
                }
                pos.trigger("chosen:updated");
            });


            $.post( base_url + 'sales/ajax', {
                target: "point",
                location: $(this).val()
            }, function(data) {

                var json = $.parseJSON(data);
                var point = $("select[name='point']");

                point.children("option").remove();
                if(json.data) {
                    $.each(json.data, function(index, val) {
                        point.append( $('<option></option>').val(val.id).html(val.title) );
                    });
                }
                point.trigger("chosen:updated");
            });
        });


        // Append row to table by selected product.
        var total = 0.00;
        $("body").on("change", "select[name='product']",function(evt, params){

            if(params.selected) {

                id = params.selected;
                text = $(this).find('option[value="' + id + '"]').html();
                amount = $(this).find('option[value="' + id + '"]').data("amount");
                tax = $(this).find('option[value="' + id + '"]').data("tax");
                currency = isNaN(currency) ? '' : currency;

                $("#product-list")
                .append('<tr id="tr'+id+'"> \
                            <td class="col-md-3 col-sm-3"><small>'+text+'</small></td> \
                            <td class="col-md-3 col-sm-3">'+currency+'<small>'+amount+'</small></td> \
                            <td class="col-md-3 col-sm-3">%'+(parseFloat(tax).toFixed(2))+'</td> \
                            <td> \
                                <div class="input-group"> \
                                    <span class="input-group-addon">x</span> \
                                    <input type="number" class="form-control" name="products['+id+']" data-tax="'+tax+'" placeholder="Quantity"  min="1" step="1" value="1"> \
                                </div> \
                            </td> \
                        </tr>');

            } else if(params.deselected) {

                $("#product-list > " + "#tr"+params.deselected).remove();
            }
            
            // Update amount value.
            calculate_amount();

        });
        
        // Update total value by quantities.
        $("body").on("input propertychange", "input[name^='products']", function() {
            if (parseInt( $(this).val() ) < 1) {
                return false;
            }
            calculate_amount();            
        });


        // Canlculate total amount.
        function calculate_amount () {
            var subtotal = 0,
            total = 0;

            $.each($("#product-list tr"), function(index, val) {

                subtotal += temp_subtotal = parseFloat($(val).find("td:eq( 1 ) > small").html()) * parseInt($(val).find("input[name^='products']").val());

                total += temp_subtotal + temp_subtotal * parseFloat($(val).find("input[name^='products']").data("tax")) / 100;

            });

            $("input[name='subtotal']").val(subtotal.toFixed(2));
            $("input[name='amount']").val((Math.round(total * 100) / 100).toFixed(2));
        }
    },

    table: function(url) {

        $url = url;
        $api = null;

        function fnUpdateTotal (argument) {
            console.log("argument"); 
        }

        $('.datatables-server').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": base_url + $url + window.location.search,
                "type": "POST"
            },
            "footerCallback": function (row, data, start, end, display) {

                $api    = this.api(),
                $footer = $( $api.column( 8 ).footer() ),
                $total  = this.DataTable().ajax.json().total;

                $footer.html( $total );                
            }
        } );

    },

    ScoreBoardConfigs: {
        location_id: null
    },

    ScoreBoard: function (location_id) {
        
        if (!location_id) return;

        Sales.ScoreBoardConfigs.location_id = location_id;

        Sales.getScoreBoard();
        setInterval(Sales.getScoreBoard, 3000);

    },

    getScoreBoard: function () {
        
        $.post(base_url + "sales/ajax/getScores", {
            location_id: Sales.ScoreBoardConfigs.location_id
        }, function(json) {
            Sales.fillScoreBoardTable(json.data.users, json.data.currency);
        }, "json");
    },

    fillScoreBoardTable: function (users, currency) {
        
        var table = $("#score-board tbody"),
        level = 0;

        table.html("");

        if (users.length !== 0) {
            // Re-map and re-sort array;  // Converts the Object to array
            users = $.map(users, (value, key) => { 
                value.total = Number(value.total);
                return [value]; 
            });
  
            users.sort((a,b) => {
                if (a.total === b.total) {
                    if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                    if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                } else {
                    if (a.total > b.total) return -1;
                    if (a.total < b.total) return 1;
                }

                return 0;
            });

            $.each(users, function(key, user) {

                let color = Sales.levelColor(++level);
                if (level >= 11) {
                    return;
                }

                table.append(`
                    <tr style="${color}">
                        <td>${user.name}</td>
                        <td class="text-right">${user.total} ${currency}</td>
                    </tr>
                `);
            });
        } else {
            table.append(
                '<tr>\
                    <td style="color: black; text-align: center;" colspan="2">\
                        <h1><i class="fa fa-exclamation-triangle"></i></h1>\
                        No content found.\
                    </td>\
                </tr>'
            );
        }
    },

    levelColor: function (level) {
        let colors = {
            1: '#008C00',
            2: '#008C00',
            3: '#008C00',
            4: '#FF8001',
            5: '#FF3F01',
            6: '#FF1100',
        };

        if (level > 5) {
            level = 6;
        }

        return `background-color: ${colors[level]};`;
    },

    invCall : function()
    {   

        var table = $('table');

        var tableDefaultH = table.data("default");
        var tableH        = table.height();
        var tableOffset   = table.position().top;


        if(tableH > tableDefaultH) {

            var offset = tableH - tableDefaultH;



            $('.inv_item').each(function(i,e){

                var eOffset = $(e).position().top;

                if(eOffset > tableOffset){
                    $(e).css({"top" : eOffset + offset});
                }
            });

            var pageH = $('#invoice_page').height();

            $('#invoice_page').height(pageH + offset);
        }

    },

    savePDF : function()
    {   
        $("#invoice_page").print();
    }


};





Sales.Invoice = {

    modal: "#modal-invoice",
    adisyonModal: "#modal-adisyon",

    init: function () {
        Sales.Invoice.Events.init();
    },

    fillModal: function (sale) {

        $("input[name='sale_id']", Sales.Invoice.modal).val(sale.id);
        $("select[name='customer_id']", Sales.Invoice.modal).val(sale.customer_id).trigger("chosen:updated").trigger("change");
        $("input[name='date']", Sales.Invoice.modal).val(sale.invoice_date_client.date);
        $("input[name='time']", Sales.Invoice.modal).val(sale.invoice_date_client.time);
    },

    fillAdisyonModal: function (sale) {
        sale.subtotal = parseFloat(sale.subtotal);
        sale.amount = parseFloat(sale.amount);
        sale.taxtotal = parseFloat(sale.taxtotal);
        sale.discount_amount = parseFloat(sale.discount_amount);
        sale.discount_type = (sale.discount_type === "amount" ? Sales.settings.location_currency : "%");

        $(".invoice-no", Sales.Invoice.adisyonModal).html(sale.id);
        $(".adisyon-username", Sales.Invoice.adisyonModal).html(sale.user_name);
        $(".invoice-datetime", Sales.Invoice.adisyonModal).html(sale.payment_date_client);
        $(".invoice-subtotal", Sales.Invoice.adisyonModal).html(sale.subtotal.toFixed(2));
        $(".invoice-amount", Sales.Invoice.adisyonModal).html(sale.amount.toFixed(2));
        $(".invoice-taxtotal", Sales.Invoice.adisyonModal).html(sale.taxtotal.toFixed(2));
        $(".invoice-discounttotal", Sales.Invoice.adisyonModal).html(sale.discount_amount +""+sale.discount_type  );
        

        $(".invoice-body tbody", Sales.Invoice.adisyonModal).html("");
        $.each(sale.products, function(index, product) {
          Sales.Invoice.Templates.productRow(product, function (row) {
            $(".invoice-body tbody", Sales.Invoice.adisyonModal).append(row);
          });
        });

    },

    Events: {
        init: function () {
            $("body").on("click", ".load_invoice", Sales.Invoice.Events.onClickLoadInvoice);
            $("body").on("click", "#process_invoice", Sales.Invoice.Events.onClickProcessInvoice);
            $("body").on("click", ".adisyon", Sales.Invoice.Events.onClickAdisyon);
            $("body").on("click", ".adisyon-print", Sales.Invoice.Events.onClickPrint);
        },

        onClickLoadInvoice: function (e) {
            e.preventDefault();

            let
            row = $(this).closest("tr"),
            id = row.data("id");

            $.post(base_url + "sales/ajax/getSaleInvoice", {
                id: id
            }, function(json) {
                
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                Sales.Invoice.fillModal(json.data.sale);

                $(Sales.Invoice.modal).modal("toggle");

            }, "json");
        },

        onClickProcessInvoice: function (e) {
            e.preventDefault();

            let
            row = $(this).closest("tr"),
            id = row.data("id");

            $.post(base_url + "sales/ajax/processSaleInvoice", $("form", Sales.Invoice.modal).serializeArray(), function(json) {
                
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                location.href = "sales/view/" + json.data.sale_id;

            }, "json");
        },

        onClickAdisyon: function(e) {
            e.preventDefault();

            let
            row = $(this).closest("tr"),
            id = row.data("id");

            $.post(base_url + "sales/ajax/getSaleAdisyonWithProducts", {
                id: id
            }, function(json) {
                
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                Sales.Invoice.fillAdisyonModal(json.data.sale);

                $(Sales.Invoice.adisyonModal).modal("toggle");

            }, "json");
        },

        onClickPrint: function(e) {
            e.preventDefault();
            $(".modal-body", Sales.Invoice.adisyonModal).print();
        }
    },

    Templates: {
        productRow: function (product, callback) {

          let 
          quantity = parseInt(product.quantity),
          tax = parseFloat(product.tax),
          price = Sales.Helpers.addTax(product.price, product.tax),
          total = price * quantity,
          row,
          gift_label = (product.is_gift === "1" ? '<span class="label label-info">İKRAM</span>' : "");

          if (quantity === 0) {
            callback("");
            return;
          }

          row = $(`
          <tr>
              <td>
                  ${product.title} ${gift_label}
                  <p class="invoice-additional_title"></p>
              </td>
              <td class="text-center">${quantity}</td>
              <td>
                ${price.toFixed(2)}
                <p class="invoice-additional_price"></p>
              </td>
              <td>
                ${total.toFixed(2)}
                <p class="invoice-additional_total"></p>
              </td>
          </tr>
          `);

          $.each(product.additional_products, function(index, additional_product) {
            let 
            additional_product_tax = parseFloat(additional_product.tax),
            additional_product_price = Sales.Helpers.addTax(additional_product.price, additional_product.tax),
            additional_product_total = additional_product_price * quantity;

            $(".invoice-additional_title", row).append(additional_product.title + "<br>");
            $(".invoice-additional_tax", row).append(`%${additional_product_tax.toFixed(2)}<br>`);
            $(".invoice-additional_price", row).append(additional_product_price.toFixed(2) + "<br>");
            $(".invoice-additional_total", row).append(additional_product_total.toFixed(2) + "<br>");
          });

          callback(row);
        }
   },
};