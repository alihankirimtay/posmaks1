var Memberships = {

  package_name : "",
  price : 0.00,
  discount : 0.00,
  currency : "",
  useCoupon : false,
  CouponCode: null,

  index: function() {
    Memberships.getMemberships();
    Memberships.getPayments();
    $("body").on("click", 'a[href="#modal-membership_info"]', Memberships.getMemebershipInfo);
    $("body").on("change", "select[name=bill_type]", Memberships.switchBillingInputs);
    $("body").on("click", 'a[href="#modal-buy_membership"]', Memberships.prepareMemebership);
    $("body").on("click", '.completeOrderWithExistsCard', Memberships.completeOrderWithExistsCard);
    $("body").on("click", '.completeOrderWithNewCard', Memberships.completeOrderWithNewCard);
    $('body').on('click' , '.discountBtn' , Memberships.usingDiscount);
    $('body').on('click' , '.cancelDiscount' , Memberships.cancelDiscount);
  },

  settings: function () {
    $("body").on("change", "select[name=bill_type]", Memberships.switchBillingInputs);
    Memberships.getSettings();
  },

  switchBillingInputs: function () {
    let container = $(this).closest("form");

    switch($(this).val()) {
      case "firm":
        $(".inputs_firm", container).removeClass("hidden");
        $(".inputs_person", container).addClass("hidden");
      break;
      case "person":
        $(".inputs_firm", container).addClass("hidden");
        $(".inputs_person", container).removeClass("hidden");
      break;
    }
  },

  getSettings: function () {

    $.getJSON(base_url + "memberships/ajax/getSettings", function(json) {

      if (json.result === 1) {
        Memberships.fillMembershipsForm(json.data.settings);
      } else {
        Pleasure.handleToastrSettings('Hata!', json.message, 'error');
      }
      
    });
  },

  getMemberships: function () {

    $.getJSON(base_url + "memberships/ajax/getMemeberships", function(json) {

      $("#memberships tbody").html("");

      if (json.result === 1) {
;
        Memberships.fillMembershipsTable(json.data.memberships);
      } else {
        $("#memberships tbody").html("<tr><td align='center'>" +json.message+ "</td></tr>");
      }
      
    });
  },

  getPayments: function () {

    $.getJSON(base_url + "memberships/ajax/getPayments", function(json) {

      $("#history tbody").html("");

      if (json.result === 1) {
        Memberships.fillPaymentsTable(json.data.payments);
      } else {
        $("#history tbody").html("<tr><td colspan='5' align='center'>" +json.message+ "</td></tr>");
      }
      
    });
  },


  usingDiscount : function()
  {
    var code = $('.discountCode').val();

    if(code == "" ||code == null){
       Pleasure.handleToastrSettings('Hata!', "Kupon kodu girmediniz" , 'error');
    }
    else if(Memberships.useCoupon) {
      Pleasure.handleToastrSettings('Hata!', "Sadece bir defa indirim kuponu kullanabilirsiniz" , 'error');
    }
    else {
      $.getJSON(base_url + "memberships/ajax/DiscountCoupon", {code : code} , function(json){

        if(json.result){
          Pleasure.handleToastrSettings('İndirim Kuponu', json.data.discount.price +"TL Değerinde indirim paketinize yansıtılmıştır" , 'Success');
          
          $('#DiscountInput').hide();
          $('#DiscountTotal').show();

          Memberships.CouponCode = code;
          Memberships.useCoupon = true;
          Memberships.discount = parseFloat(json.data.discount.price, 2);

          $('#price').html((Memberships.price - Memberships.discount).toFixed(2) + Memberships.currency);
          $('#discount').html(Memberships.discount + Memberships.currency);
          

        } else {
          Pleasure.handleToastrSettings('Hata!', json.message , 'error');
        }
        
        $('.discountCode').val('');
      });
    }
  },

  cancelDiscount : function()
  {
    Memberships.discount = 0.00;
    Memberships.useCoupon = false;


    $('#DiscountInput').show();
    $('#DiscountTotal').hide();

    $('#price').html((Memberships.price - Memberships.discount).toFixed(2) + Memberships.currency);
  },

  membership_id: null,
  prepareMemebership: function () {
    
    Memberships.membership_id = $(this).data("membership_id");
    Memberships.price = parseFloat($(this).data("price"), 2);
    Memberships.currency = $(this).data("price_formated").split(" ")[1];
    Memberships.package_name = $(this).data('name');

    $('#package_name').html(Memberships.package_name);
    $('#price').html((Memberships.price - Memberships.discount).toFixed(2) + " " + Memberships.currency);

    $.getJSON(base_url + "memberships/ajax/getSettings", function(json) {

      if (json.result === 1) {

        Memberships.fillMembershipsForm(json.data.settings);

        if (json.data.settings.iyzipay_credit_card.binNumber !== null && json.data.settings.billing_address.address !== null) {
          $('a[href="#tab_hasCreditCard"]').trigger("click");
          $("#tab_hasCreditCard #binNumber").html(json.data.settings.iyzipay_credit_card.binNumber + " xxxx xxxx xxxx");
        } else {
          $('a[href="#tab_addNewCreditCard"]').trigger("click");
        }
      } else {
        Pleasure.handleToastrSettings('Hata!', json.message, 'error');
        $("#modal-buy_membership").modal("toggle");
      }
    });

  },

  getMemebershipInfo: function () {

    $.getJSON(base_url + "memberships/ajax/getSettings", function(json) {

      if (json.result === 1) {
        Memberships.fillMembershipInfoModal(json.data.settings);
      } else {
        Pleasure.handleToastrSettings('Hata!', json.message, 'error');
      }
    });

  },

  completeOrderWithExistsCard: function () {
    
    let button = $(".completeOrderWithExistsCard"),
        button1 = $(".completeOrderWithNewCard");
    button.button('loading');
    button1.button('loading');

    $.getJSON(base_url + 'memberships/ajax/subscription', {
      "membership_id": Memberships.membership_id,
      "discountCode" : Memberships.CouponCode
    }, function(json) {
      
      button.button('reset');
      button1.button('reset');

      if (json.result === 1) {
        $('body').append(json.data.subscription.htmlContent);
      } else {
        Pleasure.handleToastrSettings('Hata!', json.message, 'error');
      }

    });

  },

  completeOrderWithNewCard: function () {
      
      let button = $(".completeOrderWithNewCard");
      button.button('loading');

      let formData = $("#tab_addNewCreditCard form");

      $.post(base_url + 'memberships/settings', formData.serializeArray(), function(json) {
        
        button.button('reset');

        if (json.result) {

          $(".completeOrderWithExistsCard").trigger("click");

        } else {
          Pleasure.handleToastrSettings('Hata!', json.message, 'error');
        }


      }, "json");

  },


  fillMembershipInfoModal: function (data) {

    let modal = $("#modal-membership_info");

    if (data.iyzipay_credit_card.binNumber !== null) {
      $("#binNumber", modal).html(data.iyzipay_credit_card.binNumber + " xxxx xxxx xxxx");
    } else {
      $("#binNumber", modal).html("<i>Tanımlı değil.</i>");
    }


    if (data.billing_address.address !== null) {
      if (data.billing_address.type === "firm") {
        $("#billingAddress", modal).html(
          data.billing_address.company + "<br>" +
          data.billing_address.tax_office + " - " + 
          data.billing_address.tax_number + "<br>" +
          data.billing_address.address + "<br>" +
          data.billing_address.zip_code + "<br>" +
          data.billing_address.district + "/" + 
          data.billing_address.city
        );
      } else if (data.billing_address.type === "person") {
        $("#billingAddress", modal).html(
          data.billing_address.name + "<br>" +
          data.billing_address.identity_number + "<br>" +
          data.billing_address.address + "<br>" +
          data.billing_address.zip_code + "<br>" +
          data.billing_address.district + "/" + 
          data.billing_address.city
        );
      }
    } else {
      $("#billingAddress", modal).html("<i>Tanımlı değil.</i>");
    }
    
  },

  fillMembershipsTable: function (data) {

    $.each(data, function(index, value) {

      $("#memberships tbody").append(" \
        <tr> \
          <td>"+value.title+"</td> \
          <td>"+value.price_formated+"</td> \
          <td class='text-right'> \
            <a href='#modal-buy_membership' data-toggle='modal' data-name='"+value.title+"' data-price='"+value.price+"' data-price_formated='"+value.price_formated+"' data-membership_id='" + value.id  + "' class='btn btn-green btn-ripple'>Satın Al</a> \
          </td> \
        </tr> \
      ");

    });
  },

  fillPaymentsTable: function (data) {

    $.each(data, function(index, value) {

      $("#history tbody").append(" \
        <tr> \
          <td>"+value.txn_id+"</td> \
          <td>"+value.created+"</td> \
          <td>"+value.membership_title+"</td> \
          <td>"+value.pp+"</td> \
          <td>"+value.price_formated+"</td> \
        </tr> \
      ");

    });
  },

  fillMembershipsForm: function (data) {
    
    $("select[name=bill_type]").val(data.billing_address.type).trigger("change");
    $("input[name=bill_company]").val(data.billing_address.company);
    $("input[name=bill_tax_office]").val(data.billing_address.tax_office);
    $("input[name=bill_tax_number]").val(data.billing_address.tax_number);
    $("input[name=bill_name]").val(data.billing_address.name);
    $("input[name=bill_identity_number]").val(data.billing_address.identity_number);
    $("input[name=bill_zip_code]").val(data.billing_address.zip_code);
    $("input[name=bill_district]").val(data.billing_address.district);
    $("input[name=bill_city]").val(data.billing_address.city);
    $("textarea[name=bill_address]").html(data.billing_address.address);
    
    $("input[name=card_holdername], input[name=card_number], select[name=card_month], select[name=card_year]").val("");
  },

  

  notify: function (data) {

    var modal = $(' \
    <div class="modal scale fade" id="modal-membership-notify" tabindex="-1" role="dialog" aria-hidden="true"> \
      <div class="modal-dialog"> \
        <div class="modal-content"> \
          <div class="modal-header"> \
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> \
            <h4 class="modal-title">POSMAKS</h4> \
          </div> \
          <div class="modal-body"> \
            POSMAKS CLOUD Lisansınız '+data.days+' gün sonra sonlanacak. \
          </div> \
          <div class="modal-footer"> \
            <a href="'+base_url+'memberships" class="btn btn-green btn-block">Hemen Yenile</a> \
            <a href="#" class="btn btn-danger btn-block remember_later">Sonra Hatırlat</a> \
          </div> \
        </div> \
      </div> \
    </div>');

    modal.appendTo("body").modal("toggle");

    modal.on("click", ".remember_later", function(){
      document.cookie = "membershipNotifyTime="+data.notifiedAt+"; expires=Thu, 18 Dec 2050 12:00:00 UTC; path=/";
      modal.modal("toggle");
    });

  },
};