var Expenses = {

    configs: {
        selected_user:0,
    },
    index:function() {

        $("body").on("click", "input[name='dFrom'], input[name='dTo']",function(){
            $("select[name='d']").val("custome").trigger("chosen:updated");
        });

        $("#print-expense-button").on("click", function(){
        	$('#print-container .row').print();
        });
    },

    init: function(configs) {

        if (typeof configs !== "undefined") {
            $.extend(Expenses.configs, configs);
        }

    	this.index();
        this.post();
        //this.fillAccounts(); //Banka ve Kasa için
    },

    post: function() {

        $('select[name=l]').on('change', function() {

        $('select[name=o]').html("");

            $.post(base_url+'Users/ajax/getAdminsAndUsersByLocationId',{location_id : $(this).val() },
                function(json) {

                    $.each(json.data.users, function(index, val) {
                        if (Expenses.configs.selected_user == val.id) {
                            $('select[name=o]').append('<option value="'+val.id+'" selected>'+val.username+'</option>'); 
                        } else {
                            $('select[name=o]').append('<option value="'+val.id+'">'+val.username+'</option>'); 
                        }
                    });

                    $('select[name=o]').trigger("chosen:updated");

                   

            }, "json");

        });

        $('select[name=l]').trigger('change');

    },

    fillAccounts: function() {

        $('select[name=location_id]').on('change', function () {

            let location_id = $(this).val();

            Expenses.postForm(null, `cari/ajax/getPosAndBanks?location_id=${location_id}`, function (json) {

                $('select[name="bank[id]"]').html("");
                $('select[name="pos[id]"]').html("");

                if (json.result === 1) {

                    $.each(json.data.banks, function (index, bank) {

                        $('select[name="bank[id]"]').append(`
                                <option value="${bank.id}">${bank.title}</option>
                            `)

                    });

                    $.each(json.data.pos, function (index, pos) {

                        $('select[name="pos[id]"]').append(`
                                <option value="${pos.id}">${pos.title}</option>
                            `)

                    });

                    $('select[name="bank[id]"]').trigger("chosen:updated");
                    $('select[name="pos[id]"]').trigger("chosen:updated");

                }

            });
        
        });

        $('select[name=location_id]').trigger('change');




    },

    postForm: function (form, url, callback) {
        $.post(base_url + url, $(form).serializeArray(), function (json) {
            callback(json);
        }, "json");
    },

};







var ExpenseWithStock = {

    config: {
        number: 0,
        rowTemplate: null,
        addFirstRow: false,
        triggerTheCalculating: false,
        method: null
    },
    stocksContainer: "#stocks",
    results: {},

    init: function (config) {


        if (typeof config !== "undefined") {
            $.extend(ExpenseWithStock.config, config);
        }

        $("#add_stock_row").on("click", ExpenseWithStock.addStockRow);
        $("body").on("change", ".stock_unit", ExpenseWithStock.onChangeUnit);

        //Listen --> .stock_content_quantity, .stock_content_price, .stock_content_tax, .stock_content_total
        $("body").on("input propertychange", ".calculate", ExpenseWithStock.calculate);
        $("body").on("change", ".f_money", ExpenseWithStock.formatInputs);

        $("body").on("click", ".delete_stock", ExpenseWithStock.delete);

        if (ExpenseWithStock.config.addFirstRow)
            ExpenseWithStock.addStockRow();

        if (ExpenseWithStock.config.triggerTheCalculating)
            ExpenseWithStock.triggerTheCalculating();
    },

    addStockRow: function () {
        
        ExpenseWithStock.config.number++;

        var template = ExpenseWithStock.config.rowTemplate
                        .replace(/{stock_number}/g, ExpenseWithStock.config.number);

        $("tbody", ExpenseWithStock.stocksContainer).append(template);

        ExpenseWithStock.stockSearch();

        $(".stock_unit").trigger("change");
    },

    stockSearch: function () {
      
        $(".stock_title:last", ExpenseWithStock.stocksContainer)
        .select2({
            placeholder: "Gider Adı",
            language: "tr",
            minimumInputLength: 1,
            ajax: {
                url: base_url + "stocks/ajax",
                delay: 10,
                type: "POST",
                dataType: 'json',
                data: function (params) {
                    return { 
                        target: "getStocksByTitle",
                        q: params.term,
                        location_id: $("select[name=location_id]").val(),
                    };
                },
                processResults: function (data, params) {
                    ExpenseWithStock.results = data.items;
                    return { results: data.items };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateResult: ExpenseWithStock.formatRepo,
            templateSelection: ExpenseWithStock.formatRepoSelection,
            tags: true,
            createTag: function (tag) {

                alreadyExists = false;
                $.each(ExpenseWithStock.results, function(index, val) {
                    if ((tag.term).match(new RegExp(val.title ,"i"))) {
                        alreadyExists = true;
                        return;
                    }
                });

                return {
                    id: tag.term,
                    text: tag.term,
                    isNew : true,
                    alreadyExists: alreadyExists
                };
            },
        })
        .on("select2:select", function (obj) {

            obj.row = $(this).closest("tr");

            ExpenseWithStock.fillInputs(obj);
        });
    },

    fillInputs: function (obj) {

        var data = obj.params.data;

        if (data.isNew) {

            $(".stock_title", obj.row).val(data.id);
            $(".stock_id", obj.row).val();
            $(".stock_unit", obj.row).prop("disabled", false);
            $(".stock_unit", obj.row).removeAttr("disabled");

        } else {

            $(".stock_title", obj.row).val(data.id);
            $(".stock_id", obj.row).val(data.id);
            $(".stock_unit", obj.row).prop("disabled", true).val(data.unit);
        }

        $(".stock_content_id", obj.row).val();
        $(".stock_content_quantity", obj.row).val();
        $(".stock_content_price", obj.row).val();
        $(".stock_content_tax", obj.row).val();
        $(".stock_content_total", obj.row).val();
    },

    // Add the unit value to hidden input, because select box disabled and does not work in the form.
    onChangeUnit: function () {
        $(this).closest("td").find(".stock_unit_hidden").val($(this).val());
    },

    delete: function () {
        
        var row = $(this).closest("tr"),
        stock_id = $(".stock_id", row).val(),
        input_name = $(".stock_id", row).attr("name");

        if (!!stock_id && ExpenseWithStock.config.method == "edit") {
            console.log(stock_id);

            input_name = input_name.replace(/\[id\]/, "[deleted]");

            row.append(
                $("<input>", {
                    type: "hidden",
                    name: input_name,
                    val: stock_id,
                    class: "stock_deleted"
                })
            );
        } else {
            row.remove();
        }

        row.fadeOut(400, function() {
            ExpenseWithStock.calculateTotals();
        });
    },

    calculate: function () {
        
        var row = $(this).closest("tr"),
        quantity = $(".stock_content_quantity", row),
        price = $(".stock_content_price", row),
        tax = $(".stock_content_tax", row),
        total = $(".stock_content_total", row),
        qu = 0, pr = 0, tx = 0, to = 0;

        // If the total input changed then calculate the unit price
        if ($(this).hasClass("stock_content_total")) {

            qu = ExpenseWithStock.unFormatMoney(quantity.val());
            to = ExpenseWithStock.unFormatMoney(total.val());
            tx = ExpenseWithStock.unFormatMoney(tax.val());

            pr = to / (1 + tx / 100);
            pr = ExpenseWithStock.round(pr,3);
            pr = pr / qu;

            price.val(ExpenseWithStock.formatMoney(pr, 3, ""));

        }
        // otherwise, the unit price changed. Calculate the total.
        else {

            qu = ExpenseWithStock.unFormatMoney(quantity.val());
            pr = ExpenseWithStock.unFormatMoney(price.val());
            tx = ExpenseWithStock.unFormatMoney(tax.val());

            to = pr + (pr * tx / 100);
            // to = ExpenseWithStock.round(to,2);
            to = to.toFixed(3);
            to = to * qu;

            total.val(ExpenseWithStock.formatMoney(to, 2, ""));
        }

        ExpenseWithStock.calculateTotals();
    },

    calculateTotals: function () {
        
        var 
        total_subtotal = 0;
        total_tax = 0,
        total_amount = 0,

        $(ExpenseWithStock.stocksContainer + " tbody tr").each(function(index, row) {

            if ($(".stock_deleted", row).length) {
                return true;
            }
            
            var 
            quantity = $(".stock_content_quantity", row),
            price = $(".stock_content_price", row),
            tax = $(".stock_content_tax", row),
            total = $(".stock_content_total", row),
            qu = 0, pr = 0, tx = 0, to = 0;

            qu = ExpenseWithStock.unFormatMoney(quantity.val());
            pr = ExpenseWithStock.unFormatMoney(price.val());
            tx = ExpenseWithStock.unFormatMoney(tax.val());
            to = ExpenseWithStock.unFormatMoney(total.val());

            if (!!pr && !!qu && !!to) {
                total_subtotal += pr * qu;
                total_tax += (pr * tx / 100) * qu;
                total_amount += to;
            }
        });

        $("#total_subtotal").html(ExpenseWithStock.formatMoney(total_subtotal, 2, ""));
        $("#total_tax").html(ExpenseWithStock.formatMoney(total_tax, 2, ""));
        $("#total_amount").html(ExpenseWithStock.formatMoney(total_amount, 2, ""));
    },

    triggerTheCalculating: function () {
        
        $(".stock_content_quantity", ExpenseWithStock.stocksContainer).trigger("input");
    },

    formatRepo: function (data) {
        if (data.loading) return data.text;

        if (data.isNew) {
            if (!data.alreadyExists) return "<div>" + data.text + " <span class='label label-warning pull-right'> Yeni </span></div>";
            else return null;
        }

        return "<b>" + data.title + "</b>";
    },

    formatRepoSelection: function (data) {
        return data.title || data.text;
    },

    formatMoney: function(number, places, symbol, thousand, decimal) {
        number = number || 0;
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : "$";
        thousand = thousand || ",";
        decimal = decimal || ".";
        var negative = number < 0 ? "-" : "",
            i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
    },

    unFormatMoney: function (price) {
        return parseFloat(price.replace(/[^0-9-.]/g, ''));
    },

    formatInputs: function () {
        $(this).val(
            ExpenseWithStock.formatMoney(
                ExpenseWithStock.unFormatMoney($(this).val()), 2, ""));  
    },

    round: function (value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    },

};

var ExpenseWithTransaction = {

    init: function () {

        ExpenseWithTransaction.Events.init();

    },

    createTransactionDiv: function (transaction) {
        $('.payment-div').addClass('hidden');
        $('.paid-div').removeClass('hidden');

        $('.paid-div .btn').data('id', transaction.id);
        
        let account = "";

        if (transaction.bank_id) {

            account = `${transaction.bank_title} Hesabı`;

        } else if (transaction.pos_title) {

            account = `${transaction.pos_title} Hesabı`;

        }

        $('.paid-div .paid-span').text(
            `${transaction.amount}₺ ödendi, ${transaction.date} - ${account}`
        );

    },

    Events: {

        init: function () {

            $("body").on("click", ".delete-transaction", ExpenseWithTransaction.Events.onClickDeleteTransaction);
            $("body").on("change", 'select[name="location_id"]', ExpenseWithTransaction.Events.onChangeLocation);

            $('select[name="location_id"]').trigger("change");

        },

        onClickDeleteTransaction: function () {

            if (!confirm('Silmek istediğinize emin misiniz? Bu işlem geri alınamaz.')) return false;

            let 
            transcation_id = $(this).data('id'),
            expense_id = $(this).data('expense'),
            customer_id = $(this).data('customer');
            
            $.post(base_url + 'Expenses/ajax/deleteTransaction', { 
                transaction_id: transcation_id, 
                customer_id: customer_id,
                expense_id: expense_id,
            },
            function (json) {

                if (json.result === 1) {

                    $('.paid-div').addClass('hidden');
                    $('.payment-div').removeClass('hidden');

                }

            }, "json");

        },

        onChangeLocation: function () {

            let location_id = $(this).val();

            Expenses.postForm(null, `cari/ajax/getPosAndBanks?location_id=${location_id}`, function (json) {

                $('select[name="bank[id]"]').html("");
                $('select[name="pos[id]"]').html("");

                if (json.result === 1) {

                    $.each(json.data.banks, function (index, bank) {

                        $('select[name="bank[id]"]').append(`
                                <option value="${bank.id}">${bank.title}</option>
                            `)

                    });

                    $.each(json.data.pos, function (index, pos) {

                        $('select[name="pos[id]"]').append(`
                                <option value="${pos.id}">${pos.title}</option>
                            `)

                    });

                    $('select[name="bank[id]"]').trigger("chosen:updated");
                    $('select[name="pos[id]"]').trigger("chosen:updated");

                }



            });





        }


    },
    

};