var PrinterCounts = {

  init: function() {
    

    this.get_last_count();

  },

  get_last_count: function() {

    $("body").on("dp.change", 'input[name="date"]', function(){


      $.post(base_url+'printercounts/getLastCount', {
          printer: $('input[name="printer"]').val(),
          date: $(this).val()
        }, function(data) {

          data = $.parseJSON(data);

          if (data.result) {
            $("input[name=counter]").closest(".input-wrapper").find(".help-block small").html("Last Counter: "+data.data.count+" on "+data.data.date);
          } else {
            $("input[name=counter]").closest(".input-wrapper").find(".help-block small").html("");
          }
      });

    });
  },
}