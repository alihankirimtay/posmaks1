var Ysproducts = {

	settings: {
		location: null,
	},

	init: function(location_id) {
		Ysproducts.settings.location = location_id;
		Ysproducts.loadYsProducts();

		$("body").on("change","select[name*=products]", Ysproducts.onChangeProduct);
	},

	loadYsProducts : function () {
		Ysproducts.Helpers.postForm("Yemeksepetisync/ajax/getYsMenu", {location_id :Ysproducts.settings.location}, function (json) {
                if (!json.result) {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");
                    return;
                }

                let 
                products = json.data.products,
                options = json.data.options,
                system_products = json.data.system_products;

                Ysproducts.fillProducts(products,system_products, options);


            });

	},

	fillProducts : function (products, system_products, options) {
		$('#ysProduct tbody').html("");

		console.log(options);

		Ysproducts.Helpers.createDropdown(system_products, function(dropdown){
				 
				$.each(products, function(index, product) {
					let desc = product.Description;

					if (typeof desc !== "object") {

						let _clone = dropdown.clone();
						let sync = $('option[data-ys-id="'+product.Id+'"]', _clone);

						if (sync.length > 0) {

							sync.attr('selected', true);

						} else {
							let sync_text = $('option[data-title="'+product.Name+'"]', _clone);
							sync_text.attr('selected', true);
						}

						_clone.attr('name','products['+product.Id+'][product]');

						let 
						portions = "",
						stocks = "",
						add_products = "";



						$.each(options, function(index, option) {
							
							if (option.ProductId === product.Id) {

								if (option.Type === "Serving") {
									portions += `<tr class="portions" data-id="${product.Id}"><td class="ys-portions" portion-id ="${option.Id}">${option.Name}</td><td class="sys-portions text-right"></td></tr>`;									
								}

								if (option.Type === "Ingredient") {
									stocks += `<tr class="stocks" data-id="${product.Id}"><td class="ys-stocks" stock-id ="${option.Id}">${option.Name}</td><td class="sys-stocks text-right"></td></tr>`;									
								}

								if (option.Type === "Product") {
									add_products += `<tr class="add-products" data-id="${product.Id}"><td class="ys-add-product" add-id="${option.Id}">${option.Name}</td><td class="sys-add-product text-right"></td></tr>`;
								}

							}

							if (option.Id === product.Id) {
								return;
							} 
						});


						$('#ysProduct tbody').append(`
							<tr class="main-products">
			                    <td class="ys-product" id="${product.Id}">${product.Name}</td>
			                    <td class="text-right">${_clone[0].outerHTML}</td>
			                </tr>
			                ${portions}
			                ${stocks}
			                ${add_products}
						`);

					}

				});

		});

		$('select[name*=products]').trigger('change');
	
	},

	onChangeProduct : function () {

		let 
		id = Number($(this).val()),
      	tr = $(this).closest('tr'),
      	ys_products_id = $('.ys-product', tr).attr('id'),
		ys_add_rows = $(`.add-products[data-id=${ys_products_id}]`), 
		ys_stock_rows = $(`.stocks[data-id=${ys_products_id}]`), 
		ys_portion_rows = $(`.portions[data-id=${ys_products_id}]`);

		// hidden input silme yapılacak
		if (!!!id) {
			return;
		}

		$('input[name*=products]').val("");

      	

		Ysproducts.Helpers.postForm(`Yemeksepetisync/ajax/getProductWithExtras`, {location_id : Ysproducts.settings.location, product_id: id}, function (json) {

			let system_products = json.data.system_products;

			$.each(system_products, function(index, product) {
			
				let 
				system_additional_products = product.additional_products,
				system_stocks = product.stocks,
				product_id = product.id,
				product_ys_id = product.ys_id,
				portion_id = product.portion_id,
				portion_title = product.title,
				sys_ys_id = product.ys_id;


				if (system_additional_products) {

					$.each(system_additional_products, function(index, add_product) {

						$.each(ys_add_rows, function(index, row) {

				      		let 
				      		additional_ys_id = $('.ys-add-product', $(row)).attr('add-id'),
				      		additional_ys_title = $('.ys-add-product', $(row)).text();

							if (add_product.ys_id === additional_ys_id && add_product.product_id == id) {

								$('.sys-add-product', $(row)).html(add_product.title);
								$(row).append(`<input type="hidden" name="products[${ys_products_id}][add_products][${additional_ys_id}]" value="${add_product.id}"></input>`);

							} else if (add_product.title === additional_ys_title && add_product.product_id == id) {

								$('.sys-add-product', $(row)).html(add_product.title);
								$(row).append(`<input type="hidden" name="products[${ys_products_id}][add_products][${additional_ys_id}]" value="${add_product.id}"></input>`);

							}

				      	});
						
					});

				}

				if (system_stocks) {

					$.each(system_stocks, function(index, stock) {

						$.each(ys_stock_rows, function(index, row) {

				      		let 
				      		stock_ys_id = $('.ys-stocks', $(row)).attr('stock-id'),
				      		stock_ys_title = $('.ys-stocks', $(row)).text();

							if (stock.ys_id === stock_ys_id && stock.product_id == id) {
								$('.sys-stocks', $(row)).html(stock.title);
								$(row).append(`<input type="hidden" name="products[${ys_products_id}][stocks][${stock_ys_id}]" value="${stock.id}"></input>`);
							
							} else if (stock.title === stock_ys_title && stock.product_id == id) {
								$('.sys-stocks', $(row)).html(stock.title);
								$(row).append(`<input type="hidden" name="products[${ys_products_id}][stocks][${stock_ys_id}]" value="${stock.id}"></input>`);

							}

				      	});
						
					});

				}

				if (portion_id) {

					console.log(product);

					$.each(ys_portion_rows, function(index, row) {
						let
						product_ys_product_id = $(row).data('id'),
						portion_ys_id = $('.ys-portions', $(row)).attr('portion-id'),
						portion_ys_title = $('.ys-portions', $(row)).text();

						portion_title = portion_ys_title.indexOf("1 Porsiyon");			

						if (product_ys_id === portion_ys_id ) {
							
							$('.sys-portions', $(row)).html(product.title);
							$(row).append(`<input type="hidden" name="products[${product_ys_product_id}][portions][${portion_ys_id}]" value="${product.id}"></input>`);


						} else if (portion_title === -1) {

							$('.sys-portions', $(row)).html(product.title);
							$(row).append(`<input type="hidden" name="products[${product_ys_product_id}][portions][${portion_ys_id}]" value="${product.id}"></input>`);

						}

					
					});

				}
				
			}); 
	

		});


	},




	Helpers: {
        postForm: function (url, data, callback) {
            $.post(base_url + url, data, function(json) {
              callback(json);
            }, "json");
        },

        createDropdown: function (products, callback) {
        	
        	var _select = $('<select name="system_products" class="chosen-select"></select>');

        	_select.append(
				$('<option></option>').val(0)
			);

			$.each(products, function(index, val) {
			    _select.append(
			        $('<option data-ys-id="'+val.ys_products_id+'" data-title="'+val.title+'"></option>').val(val.id).html(val.title)
			    );

			});

			callback(_select);



        	

        },
    },

};