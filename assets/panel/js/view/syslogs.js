var Syslogs = {

    index:function(){

        $("body").on("click", ".open-log-modal", function(){
            
            $.post( base_url + 'syslogs/ajax', {
                target: "json",
                log: $(this).data('id')
            }, function(data) {

                if(data) {

                    $("#logModal").find(".modal-body").html( data );
                    $("#logModal").modal('toggle');
                }
            });

        });
    },
}
