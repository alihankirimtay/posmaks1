var Wizard = {

    config: {
        show_modal: false,
        units: {},
        
    },
    location_id: null,
    

    init: function (config) {

        if (typeof config !== "undefined") {
            $.extend(Wizard.config, config);
        }

        if (config.show_modal) {
            Wizard.Helpers.modal();
        }

        Wizard.Events.init();
        Wizard.Locations.init();
        Wizard.Locations.show();
        Wizard.Productions.init();
        Wizard.Stocks.init();
        Wizard.Products.init();
        Wizard.FloorsAndZones.init();               
        
    },

    Events: {

        init: function () {
            $('body').on('click', '#homeWizard', Wizard.Events.onClickHomeWizard);
            $('body').on('click', '.footer-button-fixed', Wizard.Events.onClickNextTab);
            //$('body').on('click', '.footer-button-fixed', Wizard.Events.onClickNextTabZone);
            $("body").on("change", ".location-list input[name=location]", Wizard.Events.onChangeLocation);
            $('body').on('click', '#wizardModal .nav-pills > li > a', Wizard.Events.onClickNavTabs);
            $('body').on('click', '#wizardModal .wizard-header .close-button', Wizard.Events.onClickCloseModal);
            
        },

        onClickHomeWizard: function () {
            Wizard.Helpers.modal();
        },

        onClickNextTab: function () {

            let type = $(this).data('type');
            if (typeof type !== "undefined") {
                $('a[href="#addZone"]').click();
            } else {
            $('.nav-pills > .active').next('li').find('a').trigger('click');
            }
        },

        onClickNextTabZone: function () {

            Wizard.FloorsAndZones.show_zone();

        },

        onChangeLocation: function () {
            Wizard.location_id = $(this).closest('li').data('id');
            $('.footer-button-fixed').prop("disabled", false);
            Wizard.Helpers.selectedDiv($('.location-list li'), $(this).closest('li'));
        },

        onClickNavTabs: function () {
            
            let 
            btn = $(this),
            href = btn.attr('href');

            if (href !== "#stepLocation" && !Wizard.location_id) {
                alert("Restoran seçimi gerekli");
                return false;
            } else if (href === "#stepFloorsAndZones") {

                Wizard.FloorsAndZones.show();               

            } else if (href === "#stepProduction") {

                Wizard.Productions.show();

            } else if (href === "#stepStock") {

                Wizard.Stocks.show();

            } else if (href === "#stepProduct") {

                Wizard.Products.show();

            }

        },

        onClickCloseModal: function () {

            modal = $(this).closest('.modal');

            if (!confirm('"Hızlı Kurulum Sihirbazından Çıkmak İsterdiğiniz Emin misiniz?')) return false;

            modal.modal('toggle');
            showTutorial();
        },

    },

};

// Step 1
Wizard.Locations = {

    container: "#stepLocation",

    init: function () {

        Wizard.Locations.Events.init();

    },

    show: function () {
        Wizard.Locations.fillLocations();
        Wizard.Locations.showAddLocation();
    },

    showAddLocation: function () {
        $('a[href=#locationAdd]', Wizard.Locations.container).click();
        Wizard.Locations.clearAddLocationForm();

    },

    showEditLocation: function (id) {

        Wizard.Locations.clearEditLocationForm();

        Wizard.Helpers.postForm(null, `locations/ajax/getlocation?id=${id}`, function (json) {

            if (json.result === 1) {

                let form = $('#locationEdit form', Wizard.Locations.container);

                $('a[href=#locationEdit]', Wizard.Locations.container).click();
                Wizard.Locations.fillEditLocationForm(json.data);

            }

        });

    },

    fillEditLocationForm: function (location)  {
        let form = $('#locationEdit form', Wizard.Locations.container);

        $(`input[name="dtz"]`, form).val(location.dtz);
        $(`input[name="title"]`, form).val(location.title);
        $(`input[name="currency"]`, form).val(location.currency);
        $(`input[name="location_id"]`, form).val(location.id);
        $(`input[name="active"]`, form).val(location.active);


    },

    clearEditLocationForm: function () {

        let form = $('#locationEdit form', Wizard.Locations.container);

        $(`input[name="dtz"]`, form).val("");
        $(`input[name="title"]`, form).val("");
        $(`input[name="currency"]`, form).val("");
        $(`input[name="location_id"]`, form).val("");
        $(`input[name="active"]`, form).val("");

    },

    clearAddLocationForm: function () {

        let form = $('#locationAdd form', Wizard.Locations.container);

        $('input[name="title"]', form).val("");


    },


    fillLocations: function () {
        Wizard.Helpers.postForm(null, "locations/ajax/getLocations", function (json) {

            $(`${Wizard.Locations.container} .location-list .list-group`).html("");

            if (json.result === 1) {

                $.each(json.data, function (indexInArray, location) {

                    let row = Wizard.Locations.Templates.locationRow(location);
                    $(`${Wizard.Locations.container} .location-list .list-group`).append(row);

                });
                let li = $(`*[data-id = ${Wizard.location_id}]`, Wizard.Locations.container);
                $('input[name="location"]',li).trigger('click');
                $('#stepLocation input[name=location]:first').attr('checked', true).trigger('change');
            }

        });
    },

    Events: {

        init: function () {

            $('body').on('click', `${Wizard.Locations.container} #restaurantAdd`, Wizard.Locations.Events.onClickAddRestaurant);
            $('body').on('click', `${Wizard.Locations.container} .edit`, Wizard.Locations.Events.onClickEditRestaurant);
            $('body').on('click', `${Wizard.Locations.container} .remove`, Wizard.Locations.Events.onClickRemoveRestaurant);
            $('body').on('click', `${Wizard.Locations.container} #btnLocationEdit`, Wizard.Locations.Events.onClickEditSaveRestaurant);
            $('body').on('click', `${Wizard.Locations.container} #btnLocationCancel`, Wizard.Locations.Events.onClickEditCancelRestaurant);

        },

        onClickAddRestaurant: function () {

            let
                btn = $(this),
                form = btn.closest('form');

            Wizard.Helpers.postForm(form, "locations/ajax/add", function (json) {

                if (json.result === 1) {
                    Wizard.location_id = json.data.id;
                    Wizard.Locations.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");                    

                }

            });

        },

        onClickEditRestaurant: function () {

            let
            btn = $(this),
            id = btn.closest('li').data("id");

            Wizard.Locations.showEditLocation(id);
            Wizard.Helpers.selectedDiv($('.location-list li'), btn.closest('li'));
            
        },

        onClickRemoveRestaurant: function () {

            let
                btn = $(this),
                id = btn.closest('li').data("id");
            
            if (id === Wizard.location_id) {
                Pleasure.handleToastrSettings("Hata!", "Seçili olan restoran silinemez", "error");
            } else {

                if (!confirm('Silmek istediğinize emin misiniz?')) return false;

                Wizard.Helpers.postForm(null, `locations/delete/${id}`, function (json) {

                    if (json.result === 1) {
                        Wizard.Locations.fillLocations();
                        Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    }

                });

            }

        },

        onClickEditSaveRestaurant: function () {

            let
                form = $('#locationEdit form', Wizard.Locations.container),
                id = $('input[name="location_id"]', form).val();

            Wizard.Helpers.postForm(form, `locations/edit/${id}`, function (json) {

                if (json.result === 1) {
                    Wizard.Locations.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                }

            });

        },

        onClickEditCancelRestaurant: function () {

            Wizard.Locations.show();
            Wizard.Locations.clearEditLocationForm();

        }

    },

    Templates: {

        locationRow: function (location) {
            return `
                <li data-id="${location.id}" class="list-group-item d-flex justify-content-between align-items-center p-t-0 p-b-0 p-r-0">
                    <div class="row">
                        <div class="col-sm-10">
                        <div class="radio">
                            <input type="radio" class="hidden" name="location" id="inputFor${location.id}">
                            <label for="inputFor${location.id}" style="width:100%;">
                                <i class="fa fa-check fa-check-circle-o fa-lg m-r-1 text-green hidden"></i>
                                ${location.title}
                            </label>    
                        </div>
                                
                        </div>
                        <div class="col-sm-1 p-r-0" style="display:grid;">
                            <button class="btn btn-warning btn-ripple edit br-0 f-s-25"><i class="glyphicon glyphicon-edit"></i></button>
                        </div>
                        <div class="col-sm-1 p-l-0" style="display:grid;">
                            <button class="btn btn-danger btn-block btn-ripple remove br-0 f-s-25"><i class="glyphicon glyphicon-remove"></i></button>                        
                        </div>
                    </div>
                </li>
            
            `;
        }
    },

};

// Step 2
Wizard.FloorsAndZones = {

    container: "#stepFloorsAndZones",

    init: function () {
        Wizard.FloorsAndZones.Events.init();
    },

    show: function () {
        
        Wizard.FloorsAndZones.fillFloors();
        Wizard.FloorsAndZones.fillAddFormInputs();
        Wizard.FloorsAndZones.fillZones();

    },

    show_zone: function () {

        $('a[href="#zoneAdd"]', Wizard.FloorsAndZones.container).click();
        Wizard.FloorsAndZones.fillZones();

    },

    fillAddFormInputs: function () {

        $('#floorAdd input[name="location_id"]', Wizard.FloorsAndZones.container).val(Wizard.location_id);
        $('a[href=#floorAdd]', Wizard.FloorsAndZones.container).click();

    },

    showEditFloor: function (id) {

        Wizard.Helpers.postForm(null, `floors/ajax/getFloorById?id=${id}`, function (json) {

            if (json.result === 1) {

                $('a[href=#floorEdit]', Wizard.FloorsAndZones.container).click();
                Wizard.FloorsAndZones.fillEditFloorForm(json.data.floor);

            }

        });

    },

    showEditZone: function (id) {

        Wizard.Helpers.postForm(null, `zones/ajax/getZoneById?id=${id}`, function (json) {

            if (json.result === 1) {

                $('a[href=#zoneEdit]', Wizard.FloorsAndZones.container).click();
                Wizard.FloorsAndZones.fillEditZoneForm(json.data.zone);

            }

        });

    },

    fillEditFloorForm: function (floor) {

        let form = $('#floorEdit form', Wizard.FloorsAndZones.container);

        $('input[name=location_id]', form).val(floor.location_id);
        $('input[name=title]', form).val(floor.title);
        $('input[name=active]', form).val(floor.active);
        $('input[name=floor_id]', form).val(floor.id);

    },

    fillEditZoneForm: function (zone) {

        let form = $('#zoneEdit form', Wizard.FloorsAndZones.container);

        $('input[name=location_id]', form).val(zone.location_id);
        $('input[name=title]', form).val(zone.title);
        $('input[name=active]', form).val(zone.active);
        $('input[name=zone_id]', form).val(zone.id);
        $('input[name=table_count]', form).val(zone.zone_count);

        $('.floor-tab input[name=floor_id][value=' + zone.floor_id + ']').prop("checked", true);
    },


    fillFloors: function () {

        let form = $('<form></form>').append(
            $('<input>').attr({
                type: 'hidden',
                value: Wizard.location_id,
                name: 'location_id'
            })
        );

        $(`${Wizard.FloorsAndZones.container} .floor-list`).html("");
        $(`${Wizard.FloorsAndZones.container} .floor-tab`).html("");


        Wizard.Helpers.postForm(form, "floors/ajax/getFloorsByLocationId", function (json) {

            if (json.result === 1) {
                $.each(json.data.floors, function (indexInArray, floor) {

                    let
                        row = Wizard.FloorsAndZones.Templates.floorRow(floor),
                        floor_tab = Wizard.FloorsAndZones.Templates.floorRowByZones(floor);

                    $(`${Wizard.FloorsAndZones.container} .floor-list`).append(row);
                    $(`${Wizard.FloorsAndZones.container} .floor-tab`).append(floor_tab);

                });
                
                $('#stepFloorsAndZones .floor-tab input[name=floor_id]:first').attr('checked', true).trigger('change');

            }

        });

    },

    fillZones: function () {

        Wizard.Helpers.postForm(null, `zones/ajax/getZonesByLocationId?location_id=${Wizard.location_id}`, function (json) {

            $(`${Wizard.FloorsAndZones.container} .zone-list`).html("");
            if (json.result === 1) {

                $.each(json.data.zones, function (indexInArray, zone) {
                    let row = Wizard.FloorsAndZones.Templates.zoneRow(zone);
                    $(`${Wizard.FloorsAndZones.container} .zone-list`).append(row);

                });

            }

        });


    },

    clearEditFloorForm: function () {

        let form = $('#floorEdit form', Wizard.FloorsAndZones.container);

        $('input[name=location_id]', form).val("");
        $('input[name=title]', form).val("");
        $('input[name=active]', form).val("");
        $('input[name=floor_id]', form).val("");

    },

    clearAddZoneForm: function () {
        let form = $('#zoneAdd form', Wizard.FloorsAndZones.container);

        $('input[name="title"]', form).val("");
        $('input[name="floor_id"]', form).val("");
        $('input[name="location_id"]', form).val("");

        $('.floor-tab input[name=floor_id]:checked').prop("checked", false);

    },

    clearZoneForm: function (form) {

        $('input[name="title"]', form).val("");
        $('input[name="floor_id"]', form).val("");
        $('input[name="location_id"]', form).val("");
        $('input[name="table_count"]', form).val("");

        $('.floor-tab input[name=floor_id]:checked').prop("checked", false);


    },

    Events: {

        init: function () {
            $('body').on('click', `${Wizard.FloorsAndZones.container} #saveFloor`, Wizard.FloorsAndZones.Events.onClickSaveFloor);
            $('body').on('click', `${Wizard.FloorsAndZones.container} .floor-buttons .edit`, Wizard.FloorsAndZones.Events.onClickEditFloor);
            $('body').on('click', `${Wizard.FloorsAndZones.container} #btnFloorEdit`, Wizard.FloorsAndZones.Events.onClickEditSaveFloor);
            $('body').on('click', `${Wizard.FloorsAndZones.container} #btnFloorCancel`, Wizard.FloorsAndZones.Events.onClickEditCancelFloor);
            $('body').on('click', `${Wizard.FloorsAndZones.container} #zoneAdd #saveZone`, Wizard.FloorsAndZones.Events.onClickSaveZone);
            $('body').on('click', `${Wizard.FloorsAndZones.container} .zone-list .edit`, Wizard.FloorsAndZones.Events.onClickEditZone);
            $('body').on('click', `${Wizard.FloorsAndZones.container} #btnEditZone`, Wizard.FloorsAndZones.Events.onClickEditSaveZone);
            $('body').on('click', `${Wizard.FloorsAndZones.container} .floor-list .remove`, Wizard.FloorsAndZones.Events.onClickDeleteFloor);
            $('body').on('click', `${Wizard.FloorsAndZones.container} .zone-list .remove`, Wizard.FloorsAndZones.Events.onClickDeleteZone);
            $('body').on('click', `${Wizard.FloorsAndZones.container} #btnCancelZone`, Wizard.FloorsAndZones.Events.onClickEditCancelZone);
            

        },

        onClickSaveFloor: function () {
            let
            btn = $(this),
            form = btn.closest('form');

            Wizard.Helpers.postForm(form, "floors/ajax/add", function (json) {

                if (json.result === 1) {
                    Wizard.FloorsAndZones.fillFloors();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    $('input[name=title]', Wizard.FloorsAndZones.container).val("");
                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");

                }

            });
        },

        onClickEditFloor: function () {
            let
            btn = $(this),
            id = btn.closest('.floor-buttons').data('id');

            Wizard.FloorsAndZones.showEditFloor(id);

        },

        onClickEditSaveFloor: function () {
            let
                form = $('#floorEdit form', Wizard.FloorsAndZones.container),
                id = $('input[name="floor_id"]', form).val();


            Wizard.Helpers.postForm(form, `floors/edit/${id}`, function (json) {

                if (json.result === 1) {
                    Wizard.FloorsAndZones.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    Wizard.FloorsAndZones.clearEditFloorForm();

                }

            })
        },

        onClickEditCancelFloor: function () {
            Wizard.FloorsAndZones.clearEditFloorForm();
            Wizard.FloorsAndZones.show();
        },

        onClickSaveZone: function () {

            let
            btn = $(this),
            form = btn.closest('form'),
            floor_id = $('#addZone .floor-tab input[name=floor_id]:checked', Wizard.FloorsAndZones.container).val(),
            table_count = $('input[name="table_count"]',form).val();

            if (table_count <= 0) {

                Pleasure.handleToastrSettings("Hata!", "Masa Adeti Giriniz", "error");
                return;
                
            }

            $('input[name="location_id"]', form).val(Wizard.location_id);
            $('input[name="floor_id"]', form).val(floor_id);

            Wizard.Helpers.postForm(form, 'zones/ajax/add', function (zone) {

                if (zone.result === 1) {

                    Wizard.Helpers.postForm(form, `points/ajax/addMultiple?zone_id=${zone.data.id}`, function (json) {
                        if (json.result === 1) {
                            Wizard.FloorsAndZones.fillZones();
                            Wizard.FloorsAndZones.clearZoneForm(form);
                            Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                        } else {
                            Pleasure.handleToastrSettings("Hata!", json.message, "error");
                        }
                    });

                } else {
                    Pleasure.handleToastrSettings("Hata!", zone.message, "error");
                }

            });

        },

        onClickEditZone: function () {

            let
                btn = $(this),
                id = btn.closest('.zone-buttons').data('id');

            Wizard.FloorsAndZones.showEditZone(id);

        },

        onClickEditSaveZone: function () {

            let
                form = $('#zoneEdit form', Wizard.FloorsAndZones.container),
                floor_id = $('.floor-tab input[name="floor_id"]:checked', Wizard.FloorsAndZones.container).val(),
                zone_id = $('input[name=zone_id]', form).val();

            $('input[name=floor_id]', form).val(floor_id);


            Wizard.Helpers.postForm(form, `zones/edit/${zone_id}`, function (json) {

                if (json.result === 1) {
                    Wizard.FloorsAndZones.fillZones();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    Wizard.FloorsAndZones.clearZoneForm(form);
                    Wizard.FloorsAndZones.show_zone();

                }

            })


        },

        onClickDeleteFloor: function () {
            let
                btn = $(this),
                id = btn.closest('.floor-buttons').data('id');

            if (!confirm('Silmek istediğinize emin misiniz?')) return false;

            Wizard.Helpers.postForm(null, `floors/delete/${id}`, function (json) {

                if (json.result === 1) {
                    Wizard.FloorsAndZones.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                }

            });

        },

        onClickDeleteZone: function () {
            let
                btn = $(this),
                id = btn.closest('.zone-buttons').data('id');

            if (!confirm('Silmek istediğinize emin misiniz?')) return false;

            Wizard.Helpers.postForm(null, `zones/delete/${id}`, function (json) {

                if (json.result === 1) {
                    Wizard.FloorsAndZones.show_zone();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                }

            });

        },

        onClickEditCancelZone: function () {
            let
            form = $(this).closest('form');

            Wizard.FloorsAndZones.clearZoneForm(form);
            Wizard.FloorsAndZones.show_zone();

        },


    },

    Templates: {
        floorRow: function (floor) {
            return `
            <div class="col-md-3 m-b-1">
                <div class="col-md-10 bg-special-blue text-center padding-0 floor-title">
                    <p>${floor.title}</p>
                </div>
                <div data-id="${floor.id}" class="col-md-2 p-a-0 floor-buttons" style="height:100px;">
                    <div class="col-md-12 bg-orange edit" style="height:50px; display:table;">
                        <i class="glyphicon glyphicon-edit"></i>
                    </div>
                    <div class="col-md-12 bg-red remove" style="height:50px;">
                        <i class="glyphicon glyphicon-remove"></i>
                    </div>
                </div>
            </div>
            
            `;

        },

        floorRowByZones: function (floor) {
            return `
                <div class="col-md-2 m-b-1 p-l-0 choose-floor" style="margin-bottom:10px !important">
                    <label for="input${floor.id}">
                        <input id="input${floor.id}" type="radio" name="floor_id" class="hidden" value="${floor.id}">
                        <div class="bg-special-blue text-center padding-0 text-white">
                            <p class="text-overflow">${floor.title}</p>
                        </div>
                    </label>
                </div>
                
            `;

        },

        zoneRow: function (zone) {
            return `
            <div class="col-md-3 m-b-1">
                <div class="col-md-10 bg-special-blue text-center padding-0 zone-title">
                    <span>${zone.floor_title}</span>
                    <span>${zone.title}</span>
                    <span>${zone.zone_count} Masa</span>
                </div>
                <div data-id="${zone.id}" class="col-md-2 p-a-0 zone-buttons" style="height:100px;">
                    <div class="col-md-12 bg-orange edit" style="height:50px; display:table;">
                        <i class="glyphicon glyphicon-edit"></i>
                    </div>
                    <div class="col-md-12 bg-red remove" style="height:50px;">
                        <i class="glyphicon glyphicon-remove"></i>
                    </div>
                </div>
            </div>
            
            `;

        },
    },
};

Wizard.Productions = {

    container: "#stepProduction",

    init: function () {

        Wizard.Productions.Events.init();

    },

    show: function () {
        Wizard.Productions.fillProductions();
        Wizard.Productions.showAddProduction();

    },

    showAddProduction: function () {
        $('a[href=#productionAdd]', Wizard.Productions.container).click();

    },

    showEditProduction: function() {
        $('a[href=#productionEdit]', Wizard.Productions.container).click();
    },

    fillProductions: function () {

        Wizard.Helpers.postForm(null, "productions/ajax/getProductions", function (json) {

            $(`${Wizard.Productions.container} .production-list`).html("");

            if (json.result === 1) {

                $.each(json.data.productions, function (indexInArray, production) {

                    let row = Wizard.Productions.Templates.productionRow(production);
                    $(`${Wizard.Productions.container} .production-list`).append(row);

                });
            }

        });

    },

    fillAddFormInputs: function(production) {

        let form = $('#productionEdit form', Wizard.Productions.container);

        $('input[name=title]', form).val(production.title);
        $('input[name=active]', form).val(production.active);
        $('input[name=production_id]', form).val(production.id);


    },

    clearEditFormInputs: function() {
        let form = $('#productionEdit form', Wizard.Productions.container);

        $('input[name=title]', form).val("");
        $('input[name=active]', form).val("");
        $('input[name=production_id]', form).val("");

        Wizard.Productions.showAddProduction();

    },


    Events: {

        init: function () {

            $('body').on('click', `${Wizard.Productions.container} #addProduction`, Wizard.Productions.Events.onClickSaveProduction);
            $('body').on('click', `${Wizard.Productions.container} .production-list .edit`, Wizard.Productions.Events.onClickEditProduction);
            $('body').on('click', `${Wizard.Productions.container} #btnProductionEdit`, Wizard.Productions.Events.onClickEditSaveProduction);
            $('body').on('click', `${Wizard.Productions.container} #btnProductionCancel`, Wizard.Productions.Events.onClickEditCancelProduction);
            $('body').on('click', `${Wizard.Productions.container} .production-list .remove`, Wizard.Productions.Events.onClickRemoveProduction);
            
            


        },

        onClickSaveProduction: function () {
            let
            btn = $(this),
            form = btn.closest('form');

            Wizard.Helpers.postForm(form, "productions/add", function (json) {


                if (json.result === 1) {

                    $('#productionAdd input[name=title]').val("");
                    Wizard.Productions.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        
                    
                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");  
                    
                }
                
            });


        },

        onClickEditSaveProduction: function () {

            let
            btn = $(this),
            form = btn.closest('form'),
            id = $('input[name="production_id"]', form).val();

            Wizard.Helpers.postForm(form, `productions/edit/${id}`, function (json) {


                if (json.result === 1) {

                    Wizard.Productions.show();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        
                    
                } else {
                    
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");  

                }

            });

        },

        onClickEditProduction: function () {

            let
            btn = $(this),
            id = btn.closest('.production-buttons').data('id');

            Wizard.Helpers.postForm(null, `productions/ajax/getProductionById?id=${id}`, function (json) {


                if (json.result === 1) {
                    Wizard.Productions.showEditProduction();
                    Wizard.Productions.fillAddFormInputs(json.data.production);
                }

            });


        },

        onClickEditCancelProduction: function () {

            Wizard.Productions.clearEditFormInputs();

        },

        onClickRemoveProduction: function () {

            let 
            btn = $(this),
            id = btn.closest('.production-buttons').data('id');

            if (!confirm('Silmek istediğinize emin misiniz?')) return false;
            
            Wizard.Helpers.postForm(null, `productions/delete/${id}`, function (json) {


                if (json.result === 1) {
                    Wizard.Productions.fillProductions();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        
                }

            });


        }

    },

    Templates: {

        productionRow: function (production) {

            // return `
            // <li data-id="${production.id}" class="list-group-item d-flex justify-content-between align-items-center">
            //     <div class="row">
            //         <div class="col-sm-10">
            //             <span>${production.title}</span>
            //         </div>
            //         <div class="col-sm-1 btn btn-warning btn-ripple edit">
            //             <i class="glyphicon glyphicon-edit"></i>
            //         </div>
            //         <div class="col-sm-1">
            //             <button class="btn btn-danger btn-block btn-ripple remove"><i class="glyphicon glyphicon-remove"></i></button>                        
            //         </div>
            //     </div>
            // </li>
        
            // `;

            return `
            <div class="col-md-3 m-b-1">
                <div class="col-md-10 bg-special-blue text-center padding-0 production-title">
                    <p>${production.title}</p>
                </div>
                <div data-id="${production.id}" class="col-md-2 p-a-0 production-buttons" style="height:100px;">
                    <div class="col-md-12 bg-orange edit" style="height:50px; display:table;">
                        <i class="glyphicon glyphicon-edit"></i>
                    </div>
                    <div class="col-md-12 bg-red remove" style="height:50px;">
                        <i class="glyphicon glyphicon-remove"></i>
                    </div>
                </div>
            </div>
            
            `;

            

            

        }

    }


},

Wizard.Stocks = {

    container: "#stepStock",

    init: function () {

        Wizard.Stocks.Events.init();

    },

    show: function () {

        Wizard.Stocks.fillStocks();

    },

    showEditTab: function () {

        $('a[href="#stockEdit"]', Wizard.Stocks.container).click();

    },

    fillStocks: function () {

        let form = $('<form></form>').append(
            $('<input>').attr({
                type: 'hidden',
                value: Wizard.location_id,
                name: 'location',
            }),
            $('<input>').attr({
                type: 'hidden',
                value: "stocks",
                name: 'target',
            })
        );

        Wizard.Helpers.postForm(form, "stocks/ajax",function(json) {

            $('.stock-list', Wizard.Stocks.container).html("");

            if (json.result === 1) {
                
                $.each(json.data, function (indexInArray, stock) { 

                    let row = Wizard.Stocks.Templates.stockRow(stock);

                    $('.stock-list', Wizard.Stocks.container).append(row);

                    

                });

            }

        });

    },

    fillEditInputs: function (stock) {
        let 
        form = $('#stockEdit form', Wizard.Stocks.container),
        option = $('select[name=unit]', form);

        $('input[name="title"]', form).val(stock.title);
        $('input[name="quantity"]', form).val(stock.quantity);
        $('input[name="location_id"]', form).val(stock.location_id);
        $('input[name="sub_limit"]', form).val(stock.sub_limit);
        $('input[name="active"]', form).val(stock.active);
        $('input[name="stock_id"]', form).val(stock.id);

        option.val(stock.unit);
        option.trigger('chosen:updated');
    },

    clearAddFormInputs: function (form) { 

        $('input[name=title]', form).val("");
        $('input[name=location_id]', form).val("");
        $('input[name=quantity]', form).val("");
        $('input[name=target]', form).val("");        

    },

    clearEditFormInputs: function (form) {

        $('input[name=title]', form).val("");
        $('input[name=location_id]', form).val("");
        $('input[name=quantity]', form).val("");
        $('input[name=sub_limit]', form).val("");
        $('input[name=stock_id]', form).val("");
        $('input[name=target]', form).val("");

    },

    Events: {

        init: function () {

            $('body').on('click', `${Wizard.Stocks.container} #saveStock`, Wizard.Stocks.Events.onClickSaveStock);
            $('body').on('click', `${Wizard.Stocks.container} .stock-list .edit`, Wizard.Stocks.Events.onClickEditStock);
            $('body').on('click', `${Wizard.Stocks.container} #btnEditStock`, Wizard.Stocks.Events.onClickEditSaveStock);
            $('body').on('click', `${Wizard.Stocks.container} .stock-list .remove`, Wizard.Stocks.Events.onClickDeleteStock); 
            $('body').on('click', `${Wizard.Stocks.container} #btnCancelStock`, Wizard.Stocks.Events.onClickCancelEditStock);
            
        },

        onClickSaveStock: function () {

            let 
            btn = $(this),
            form = btn.closest('form');

            $('input[name="location_id"]', form).val(Wizard.location_id);
            $('input[name="target"]', form).val("addStockWithQuantity");

            Wizard.Helpers.postForm(form, "stocks/ajax", function(json) {

                if (json.result === 1 ) {

                    Wizard.Stocks.fillStocks();
                    Wizard.Stocks.clearAddFormInputs(form);
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        

                } else {
                    Pleasure.handleToastrSettings("Hata!", json.message, "error");                                            
                }

            })
        },

        onClickEditStock: function () {

            let
                btn = $(this),
                non_editable = btn.hasClass('non-editable'),
                id = btn.closest('.stock-buttons').data('id');

                let form = $('<form></form>').append(
                    $('<input>').attr({
                        type: 'hidden',
                        value: id,
                        name: 'id',
                    }),
                    $('<input>').attr({
                        type: 'hidden',
                        value: "getStockById",
                        name: 'target',
                    })
                );

            if(!non_editable) {

                Wizard.Helpers.postForm(form, "stocks/ajax", function(json) {

                    if (json.result === 1) {

                        Wizard.Stocks.fillEditInputs(json.data);
                        Wizard.Stocks.showEditTab();
                        
                    }

                });


            }

        },

        onClickEditSaveStock: function () {

            let 
            btn = $(this),
            form = btn.closest('form');

            $('input[name="target"]', form).val("updateStockWithQuantity");

            Wizard.Helpers.postForm(form, 'stocks/ajax',function (json) {

                if (json.result === 1) {

                    Wizard.Stocks.fillStocks();
                    Wizard.Stocks.clearEditFormInputs(form);
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        
                    
                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");                                            

                }

            });

        },

        onClickDeleteStock: function () {

            let
            btn = $(this),
            form = $('<form></form>'),
            id = btn.closest('.stock-buttons').data('id');

            form.append(
                $('<input>').attr({
                    type: 'hidden',
                    value: "deleteStockWithQuantity",
                    name: 'target',
                }),
                $('<input>').attr({
                    type: 'hidden',
                    value: id,
                    name: 'id',
                })
            );

            if (!confirm('Silmek istediğinize emin misiniz?')) return false;
            

            Wizard.Helpers.postForm(form, 'stocks/ajax', function (json) {

                if (json.result === 1) {
                    Wizard.Stocks.fillStocks();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");                        

                }

            });


        },

        onClickCancelEditStock: function () {
            let form = $(this).closest('form');
            Wizard.Stocks.clearEditFormInputs(form);
            $('a[href="#stockAdd"]', Wizard.Stocks.container).click();
            

        },

    },

    

    Templates: {

        stockRow: function (stock) {

            let isEditable = (stock.edit) ? "" : "non-editable";

            return `
            <div class="col-md-3 m-b-1">
                <div class="col-md-10 bg-special-blue text-center padding-0 stock-title">
                    <p>${stock.title}</p>
                </div>
                <div data-id="${stock.id}" class="col-md-2 p-a-0 stock-buttons" style="height:100px;">
                    <a href="#stepStock" class="col-md-12 bg-orange edit ${isEditable}" style="height:50px; display:table;">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    <div class="col-md-12 bg-red remove" style="height:50px;">
                        <i class="glyphicon glyphicon-remove"></i>
                    </div>
                </div>
            </div>
            
            `;

        },

    },

},

Wizard.Products = {

    container: "#stepProduct",
    stepAddContainer: "#stepProduct #addProduct",
    stepEditContainer:"#stepProduct #editProduct",

    init: function () {

        Wizard.Products.Events.init();

    },

    show: function () {

        Wizard.Products.fillProducts();
        $('a[href=#indexProduct]', Wizard.Products.contanier).click();

    },

    showProductTab: function () {
        
        Wizard.Products.fillCategories(Wizard.Products.stepAddContainer);
        Wizard.Products.fillProductions(Wizard.Products.stepAddContainer);
        Wizard.Products.fillStocks(Wizard.Products.stepAddContainer);
        $('a[href=#addProduct]', Wizard.Products.contanier).click();
        
    },

    showEditProduct: function (id) {

        Wizard.Products.fillCategories(Wizard.Products.stepEditContainer);
        Wizard.Products.fillProductions(Wizard.Products.stepEditContainer);
        Wizard.Products.fillStocks(Wizard.Products.stepEditContainer);
        $('a[href=#editProduct]', Wizard.Products.contanier).click();  

        let form = $('<form></form>').append(
            $('<input>').attr({
                type: 'hidden',
                value: id,
                name: 'product_id',
            }),
        );
        
        Wizard.Helpers.postForm(form, 'products/ajax/getProductWithRelatedItems', function (json) {

            Wizard.Products.fillProductEditForm(json.data.product);

        });
        

    },

    clearCategoryAddForm: function (form) {

        $('input[name=title]', form).val("");

    },

    clearProductForm: function (form) {

        $('input[name="title"]', form).val("");
        $('input[name="price"]', form).val("");
        $('input[name="price_tax"]', form).val("");
        $('input[name="location_id"]', form).val("");
        $('.stocks', form).val('').trigger("chosen:updated");
        $('.stock-values').remove();

    },

    fillProducts: function () {

        let form = $('<form></form>').append(
            $('<input>').attr({
                type: 'hidden',
                value: Wizard.location_id,
                name: 'location_id',
            }),
        );
        
        Wizard.Helpers.postForm(form, 'products/ajax/getProductsWithCategoryAndProduction', function (json) {

            $('#fillWizardProductTable tbody', Wizard.Products.container).html("");

            $.each(json.data.products, function (indexInArray, product) {
                
                let row = Wizard.Products.Templates.productRow(product);
                $('#fillWizardProductTable tbody', Wizard.Products.container).append(row);
                 
            });

        });

    },

    fillCategories: function (container) {

        Wizard.Helpers.postForm(null, 'products/ajax/getCategories', function (json) {

            $('select[name="product_type_id"]', container).html("");
            $('select[name="product_type_id"]', container).append('<option value="0">Kategori Seçiniz</option>');

            if (json.result === 1) {
                $.each(json.data.categories, function (indexInArray, category) {

                    $('select[name="product_type_id"]', container).append(`
                        <option value="${category.id}"">${category.title}</option>
                    `);

                });

                $('select[name="product_type_id"]', container).trigger("chosen:updated");
            
            }
          
        });

    },

    fillCategoryTypes: function () {

        Wizard.Helpers.postForm(null, 'products/ajax/getCategoryTypes', function (json) {

            $('select[name="parent_id"]', Wizard.Products.container).html("");
            $('select[name="parent_id"]', Wizard.Products.container).append(`
                <option value="" class="text-bold"></option>
            `);

            if (json.result === 1) {
                $.each(json.data.product_types, function (indexInArray, categoryType) {

                    $('select[name="parent_id"]', Wizard.Products.container).append(`
                        <option value="${categoryType.id}"">${categoryType.title}</option>
                    `);

                });

                $('select[name="parent_id"]', Wizard.Products.container).trigger("chosen:updated");

            }

        });

    },

    fillProductions: function (container) {

        Wizard.Helpers.postForm(null, 'productions/ajax/getProductions', function (json) {

            $('select[name="production_id"]', container).html("");
            $('select[name="production_id"]', container).append(`
                <option value="" class="text-bold">Üretim Yeri Seçiniz</option>
            `);

            if (json.result === 1) {

                $.each(json.data.productions, function (indexInArray, production) {

                    $('select[name="production_id"]', container).append(`
                        <option value="${production.id}">${production.title}</option>
                    `);

                });

                $('select[name="production_id"]', container).trigger("chosen:updated");

            }

        });

    },

    system_stocks: {},
    fillStocks: function (container) {

        let form = $('<form></form>').append(
            $('<input>').attr({
                type: 'hidden',
                value: Wizard.location_id,
                name: 'location',
            }),
            $('<input>').attr({
                type: 'hidden',
                value: "stocks",
                name: 'target',
            })
        );

        Wizard.Helpers.postForm(form, "stocks/ajax", function (json) {

            $('.wizard-stocks-container .stocks', container).html("");

            if (json.result === 1) {
                let 
                stock_unit = "";

                Wizard.Products.system_stocks = json.data;

                $.each(json.data, function (indexInArray, stock) {

                    stock_unit = String(stock.unit);

                    $('.wizard-stocks-container .stocks', container).append(`
                        <option value="${stock.id}" data-location_id="${stock.location_id}" data-unit="${stock_unit}">${stock.title}</option>
                    `);

                });

                $('.wizard-stocks-container .stocks', container).trigger("chosen:updated");

            }

        });

    },

    fillProductEditForm: function (product) {

        let 
        form = $('#editProduct form', Wizard.Products.container),
        category = $('select[name="product_type_id"]', form),
        production = $('select[name="production_id"]', form),
        tax = $('select[name="tax"]', form),
        select_stock = $('.stocks', form),
        additional = $('select[name="additional_products[]"]', form),
        portion = $('select[name="portion_products[]"]', form),
        checked = "";

        category.val(product.product_type_id);
        production.val(product.production_id);
        tax.val(Wizard.Helpers.round(product.tax,2));

        category.trigger('chosen:updated');
        production.trigger('chosen:updated');
        tax.trigger('chosen:updated');

        $('input[name="title"]', form).val(product.title);
        $('input[name="price"]', form).val(product.price);
        $('input[name="active"]', form).val(product.active);
        $('input[name="bonus_type"]', form).val(product.bonus_type);
        $('input[name="bonus"]', form).val(product.bonus);
        $('input[name="sort"]', form).val(product.sort);
        $('input[name="product_id"]', form).val(product.id);

        $(".wizard-stocks-container .stock-values", Wizard.Products.stepEditContainer).remove();

        $.each(product.stocks, function (indexInArray, stock) { 
             
            $.each(Wizard.Products.system_stocks, function (indexInArray, system_stock) { 
                if (system_stock.id === stock.id) {

                    if (stock.optional == 1) {
                        checked = "checked";
                    } else {
                        checked = "";
                    }

                    $('option[value='+stock.id+']',select_stock).prop('selected', true);

                    $(".wizard-stocks-container", Wizard.Products.stepEditContainer).append(`
                        <div class="form-group stock-values" id="stockContainer${stock.id}">
                            <label class="control-label col-sm-2">${stock.title}</label>
                            <div class="col-sm-2">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <input type="number" name="stocks[${stock.id}][quantity]" class="form-control" step="0.01" value="${stock.quantity}">
                                    </div>
                                </div>
                            </div>
                            <label class="col-sm-2"><b>${system_stock.unit}</b></label>
                            <label class="col-sm-3">
                                <div class="checkboxer">
                                    <input type="checkbox" name="stocks[${stock.id}][optional]" ${checked} value="1" id="stockCheckbox${stock.id}">
                                    <label for="stockCheckbox${stock.id}">İsteğe bağlı siparişten çıkarılabilir</label>
                                </div> 
                            </label>
                        </div>`
                    );

                }
            
            });

        });

        select_stock.trigger('chosen:updated');

        additional.html("");
        $.each(product.additional_products, function (indexInArray, additional_product) { 
            additional.append(`
                <option value="${additional_product.id}" data-location_id="${Wizard.location_id}" class="" selected="selected"></option>
            `)
             
        });

        portion.html("");
        $.each(product.portions, function (indexInArray, portion_product) {
            portion.append(`
                <option value="${portion_product.id}" data-location_id="${Wizard.location_id}" class="" selected="selected"></option>
            `)

        });

        Wizard.Helpers.triggerEditPriceChange();


    },

    Events: {

        init:function () {

            $('body').on('click', `${Wizard.Products.container} #createProduct`, Wizard.Products.Events.onClickCreateProduct);
            $('body').on('click', `${Wizard.Products.container} #newCategory`, Wizard.Products.Events.onClickNewCategory);
            $('body').on('click', `${Wizard.Products.container} #saveCategory`, Wizard.Products.Events.onClickSaveCategory);
            $('body').on('click', `${Wizard.Products.container} #cancelCategory`, Wizard.Products.Events.onClickCancelCategory);
            $('body').on('click', `${Wizard.Products.container} #saveProduct`, Wizard.Products.Events.onClickSaveProduct);
            $('body').on('change', `${Wizard.Products.container} .stocks`, Wizard.Products.Events.onClickSelectStock);
            $('body').on('click', `${Wizard.Products.container} #cancelProduct, #cancelEditProduct`, Wizard.Products.Events.onClickCancelProduct);
            $('body').on('click', `${Wizard.Products.container} #fillWizardProductTable .edit`, Wizard.Products.Events.onClickEditProduct);
            $('body').on('click', `${Wizard.Products.container} #saveEditProduct`, Wizard.Products.Events.onClickSaveEditProduct);
            $('body').on('click', `${Wizard.Products.container} #fillWizardProductTable .remove`, Wizard.Products.Events.onClickDeleteProduct);     
            
            $(".price-with-tax, .price-without-tax", Wizard.Products.container).on("change input", Wizard.Helpers.calculateTaxPrice);
            $("select[name=tax]", Wizard.Products.stepAddContainer).on("change", Wizard.Helpers.triggerAddPriceChange);
            $("select[name=tax]", Wizard.Products.stepEditContainer).on("change", Wizard.Helpers.triggerEditPriceChange);

        },

        onClickCreateProduct: function () {
            
            Wizard.Products.clearProductForm();
            Wizard.Products.showProductTab();

        },

        onClickNewCategory: function () {

            Wizard.Products.fillCategoryTypes();
            $('a[href=#newCategoryTab]', Wizard.Products.contanier).click();
            
        },

        onClickSaveCategory: function () {

            let 
            btn = $(this),
            form = btn.closest('form');

            Wizard.Helpers.postForm(form, 'producttypes/add', function (json) {

                if (json.result === 1) {
                    
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    Wizard.Products.clearCategoryAddForm(form);
                    Wizard.Products.fillCategories(Wizard.Products.stepAddContainer);
                    $('a[href=#addProduct]', Wizard.Products.contanier).click();

                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");                    

                }

            });
        
        },

        onClickCancelCategory: function () {

            let
            btn = $(this),
            form = btn.closest('form');

            Wizard.Products.clearCategoryAddForm(form);
            Wizard.Products.fillCategories(Wizard.Products.stepAddContainer);
            $('a[href=#addProduct]', Wizard.Products.contanier).click();
            
        },

        onClickSaveProduct: function () {
            let 
            btn = $(this),
            form = btn.closest('form');

            form.append(
                $('<input>').attr({
                    type: 'hidden',
                    value: Wizard.location_id,
                    name: 'location_id',
                }),
            );

            Wizard.Helpers.postForm(form, 'products/add', function (json) {

                if (json.result === 1) {

                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    Wizard.Products.clearProductForm(form);
                    Wizard.Products.show();

                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");

                }

            });

        },

        onClickSaveEditProduct: function () {

            let 
            btn = $(this),
            form = btn.closest('form'),
            id = $('input[name="product_id"]',form).val();

            form.append(
                $('<input>').attr({
                    type: 'hidden',
                    value: Wizard.location_id,
                    name: 'location_id',
                }),
            );

            Wizard.Helpers.postForm(form, `products/edit/${id}`, function (json) {


                if (json.result === 1) {

                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");
                    Wizard.Products.clearProductForm(form);
                    Wizard.Products.show();
                    

                } else {

                    Pleasure.handleToastrSettings("Hata!", json.message, "error");

                }

            });



        },

        onClickSelectStock: function(e,params) {
            
            let 
            id = $(this).closest('.active').attr('id');

            if (id === "addProduct") {
                container = Wizard.Products.stepAddContainer;
            } else {
                container = Wizard.Products.stepEditContainer;
            }

            if (params.selected !== undefined) {

                let
                    form = $(this).closest('form'),
                    id = params.selected,
                    stock = $(`${container} .stocks option[value=${id}]`),
                    title = stock.text(),
                    unit = stock.data('unit');

                $(`${container} .wizard-stocks-container`).append(`
                <div class="form-group stock-values" id="stockContainer${id}">
                    <label class="control-label col-sm-2">${title}</label>
                    <div class="col-sm-2">
                        <div class="inputer">
                            <div class="input-wrapper">
                                <input type="number" name="stocks[${id}][quantity]" class="form-control" step="0.01" value="0.00" placeholder="0.00">
                            </div>
                        </div>
                    </div>
                    <label class="col-sm-2"><b>${unit}</b></label>
                    <label class="col-sm-3">
                        <div class="checkboxer">
                            <input type="checkbox" name="stocks[${id}][optional]" value="1" id="stockCheckbox${id}">
                            <label for="stockCheckbox${id}">İsteğe bağlı siparişten çıkarılabilir</label>
                        </div> 
                    </label>
                </div>`);

            } else {
                let id = params.deselected;
                $(`${container} .wizard-stocks-container #stockContainer${id}`).remove();
            }


        },

        onClickEditProduct: function () {

            let 
            btn = $(this),
            id = btn.closest('td').data('id');

            Wizard.Products.showEditProduct(id);

        },

        onClickCancelProduct: function () {
            let 
            form = $(this).closest('form');
            Wizard.Products.show();
            Wizard.Products.clearProductForm(form);

        },

        onClickDeleteProduct: function () {

            let id = $(this).closest('tr').data('id');

            if (!confirm('Silmek istediğinize emin misiniz?')) return false;

            Wizard.Helpers.postForm(null, `products/delete/${id}`, function (json) {

                if (json.result === 1) {
                    Wizard.Products.fillProducts();
                    Pleasure.handleToastrSettings("Başarılı!", json.message, "success");

                }

            });

        },

    },

    Templates: {

        productRow: function (product) {
            let price = parseFloat(product.price) + (parseFloat(product.price) * parseFloat(product.tax))/100;
            price = Wizard.Helpers.round(price, 2);
            return `
                <tr data-id="${product.id}">
                    <td class="col-md-3">${product.title}</td>
                    <td class="col-md-3">${product.production_title}</td>
                    <td class="col-md-3">${product.category}</td>
                    <td class="col-md-1">${price.toFixed(2)}₺</td>
                    <td class="col-md-2" data-id="${product.id}">
                        <a class="btn btn-primary btn-xs btn-ripple edit" href="#"> <i class="fa fa-pencil"></i></a>
                        <a class="btn btn-danger btn-xs btn-ripple remove"> <i class="fa fa-trash-o"></i></a>
                    </td>
                </tr>
            `
        }

    },

},

Wizard.Helpers = {

    modal: function () {
        $('#wizardModal').modal('toggle');
    },

    postForm: function (form, url, callback) {
        $.post(base_url + url, $(form).serializeArray(), function (json) {
            callback(json);
        }, "json");
    },

    selectedDiv: function (removeSelector, addSelector) {
        removeSelector.removeClass('bg-disabled-color-white');
        removeSelector.removeAttr('style', 'color: black;');

        addSelector.addClass('bg-disabled-color-white');
        addSelector.attr('style', 'color: black;');

        $(".fa-check", removeSelector).addClass('hidden');
        $(".fa-check", addSelector).removeClass('hidden');

    },

    calculateTaxPrice: function () {
        let val = Number($(this).val()) || 0,
            form = $(this).closest('form'),        
            tax = Number($('select[name=tax] option:selected', form).val()),
            total = 0;


        if ($(this).hasClass('price-without-tax')) {
            total = val + (val * tax / 100);
            total = Wizard.Helpers.round(total, 2);
            $('.price-with-tax', form).val(total);

        } else if ($(this).hasClass('price-with-tax')) {
            total = val / (1 + tax / 100);
            total = Wizard.Helpers.round(total, 2);
            $('.price-without-tax', form).val(total);
        }

    },

    triggerAddPriceChange: function () {
        $(".price-without-tax", Wizard.Products.stepAddContainer).trigger('change');
    },
    triggerEditPriceChange: function () {
        $(".price-without-tax", Wizard.Products.stepEditContainer).trigger('change');
    },

    round: function (value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    },

};