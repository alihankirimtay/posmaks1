var default_id = $('#ch_manager').find('option:selected').val();

$.ajax({
    url: base_url + 'panel/Home/statistics/beacon/' + default_id,
    success: function(result) {
        result = $.parseJSON(result);
        if (typeof result !== 'object') return false;

        google.load('visualization', '1.1', {
            packages: ['line', 'corechart']
        });
        google.setOnLoadCallback(drawChart(result));
    }
})


function drawChart(beacon_data) {
   
    var button = document.getElementById('change-chart');
    var chartDiv = document.getElementById('chart_div');

    var data = new google.visualization.DataTable(beacon_data);

    var materialOptions = {
        chart: {
            title: 'Average Temperatures and Day in Iceland Throughout the Year'
        },
        width: "100%",
        height: 500,
        series: {
            // Gives each series an axis name that matches the Y-axis below.
            0: {
                axis: 'Request'
            },
            1: {
                axis: 'Day'
            }
        },
        axes: {
            // Adds labels to each axis; they don't have to match the axis names.
            y: {
                Request: {
                    label: 'Request'
                },
                Day: {
                    label: 'Day'
                }
            }
        }
    };

    var classicOptions = {
        title: 'Average Temperatures and Day in Iceland Throughout the Year',
        width: "100%",
        height: 500,
        // Gives each series an axis that matches the vAxes number below.
        series: {
            0: {
                targetAxisIndex: 0
            },
            1: {
                targetAxisIndex: 1
            }
        },
        vAxes: {
            // Adds titles to each axis.
            0: {
                title: 'Request '
            },
            1: {
                title: 'Day'
            }
        },
        hAxis: {
            ticks: [new Date(2014, 11), new Date(2015, 1), new Date(2015, 2), new Date(2015, 3),
                new Date(2015, 4), new Date(2015, 5), new Date(2015, 6), new Date(2015, 7),
                new Date(2015, 8), new Date(2015, 9), new Date(2015, 10), new Date(2014, 12)
            ]
        },
        vAxis: {
            viewWindow: {
                max: 30
            }
        }
    };

    function drawMaterialChart() {
        var materialChart = new google.charts.Line(chartDiv);
        materialChart.draw(data, materialOptions);
    }

    drawMaterialChart();

}


$('#ch_manager').on('change', function() {
    var type = 'beacon';
    var id = $(this).val();

    $.ajax({
        url: base_url + 'panel/Home/statistics/' + type + '/' + id,
        success: function(result) {
            drawChart($.parseJSON(result));
        }

    });
});
