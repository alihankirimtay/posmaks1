$(function() {
	
});

var MyFunction = {

    keyboard: function () {


        $('.keypad').
        keyboard({
            lang: "tr",
            layout: 'turkish-q',
            reposition : true,
            usePreview: false,
            autoAccept: true,
            css: {
                buttonDefault: 'btn btn-default btn-xl',
            }
        })
        // activate the typing extension
        .addTyping({
            showTyping: true,
            delay: 250
        })
        .addExtender({
            layout: 'numpad',
            showing: true,
            reposition: true
        });


        $('.numpad').keyboard({
            lang: "tr",
            layout: 'numpad',
            reposition : true,
            usePreview: false,
            autoAccept: true,
            css: {
                buttonDefault: 'btn btn-xl',
            },
            beforeClick: function(e, keyboard, el){
                keyboard.$el.trigger('change');
                console.log(keyboard);
            }
        })
        .addTyping({
            showTyping: true,
            delay: 250
        });

    },

    datepicker: function (type) {

        $.getScript(base_url + 'assets/globals/plugins/bootstrap-datepicker/js/datepicker.js', function(data, textStatus) {
            $('<link/>', { rel: 'stylesheet', type: 'text/css',
                href: base_url + 'assets/globals/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css'
            }).appendTo('head');

            $('.datepicker-basic').datepicker({ 
                format: "yyyy-mm-dd",
                autoclose: true,
                todayBtn: "linked",
            });
        });
    },

    datetimepicker: function (time_format) {

        if (typeof time_format === 'undefined') {
            time_format = 'YYYY-MM-DD hh:mm:ss';
        };

        $('.datetimepicker-basic').datetimepicker({
            format: time_format,
            locale: 'tr'
        });

        $('.datetimepicker-time').datetimepicker({
            format: "HH:mm",
            locale: 'tr'
        });

        $.each($('.datetimepicker'), function(index, input) {
            
            let
            $input = $(input),
            format = $input.attr("format") || 'YYYY-MM-DD hh:mm:ss';

            $input.datetimepicker({
                "format": format,
                "locale": "tr"
            });

        });
    },

    eNumberSpin: function () {

        $("body").on("click", ".e-number-spin a", function(){
            
            input = $(this).closest(".e-number-spin").find("input"),
            val   = parseInt(input.val())
            evnt  = $(this).data("do");

            input.val( (val ? val : 0) + (evnt=="plus" ? +1 : -1) );
        });
    },

    colorPickerWheel: function () {
        $('.minicolors').each( function() {
            $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                defaultValue: $(this).attr('data-defaultValue') || '',
                inline: $(this).attr('data-inline') === 'true',
                letterCase: $(this).attr('data-letterCase') || 'lowercase',
                opacity: $(this).attr('data-opacity'),
                position: $(this).attr('data-position') || 'bottom left',
                change: function(hex, opacity) {
                    if( !hex ) return;
                    if( opacity ) hex += ', ' + opacity;
                    try {
                        console.log(hex);
                    } catch(e) {}
                },
                theme: 'bootstrap'
            });
        });
    },
};