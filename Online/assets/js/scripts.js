$(document).ready(function(){
	$('.select2').select2();

	$("#restaurant").on("change",function(){
		
		$('#myTab').empty();
		$('#pills-tabContent').empty();

		//Restaurantla Alakalı Ürünlerin Ekrana Bastırıldığı Yer
		var restaurant_id = $(this).val();

		$.post(base_url+"panel/Locations/get_products_group", {restaurant_id: restaurant_id}, function (categories) {

			let
			tabs = $("<ul></ul>").addClass("nav nav-pills mb-3").attr("role", "tablist"),
			contents = $("<div></div>").addClass("tab-content").attr("role", "tablist");

			$.post(base_url+"panel/Locations/get_products", {restaurant_id: restaurant_id}, function (products) {

				$.each(categories,function(index, category){

					tabs.append($(`
						<li>
							<a class="nav-link" id="pills-${category.id}-tab" data-toggle="pill" href="#tab${category.id}" role="tab" aria-controls="pills-home" aria-selected="true">${category.title}</a>
						</li>
					`));


					$.each(products, function(index, product){

						if (product.product_type_id === category.id && product.active == 1) {

							if ($(`#tab${category.id}`, contents).length < 1) {
								contents.append(`<div class="tab-pane fade bg-success" id="tab${category.id}" role="tabpanel"></div>`);
							}
							var checked = "";
							if(typeof restaurant_edit !== 'undefined'){
								$.each(restaurant_products, function(index1,restaurant_product){

									if(restaurant_product.id === product.id)
									{
										checked = "checked";
										return;
										
									}
			
								});
							}
							else
							{
								checked = "checked";
							}
						
							$(`#tab${category.id}`, contents).append(`
								<div class="p-2 mb-2 w-30 bg-light text-black border border-primary rounded" style="width: 30%; height:100px; float: left; margin-left: 5px;">
									<input class="products" type ='checkbox' id='${product.id}' value='${product.id}' name='products[${product.id}]' ${checked} style="width: 40px; height: 30px;"> 
									<center>
									<img src="${service_url}${product.image}" class="img-thumbnail" style="width:100px; height:60px; margin-top:-40px;">
									<p>${product.title}</p>
									</center>	
								</div>
							`);
							
						}

					});

				});

				$('#myTab').append(tabs);
				$('#pills-tabContent').append(contents);


			}, "json");

		}, "json");
	});
	

	if(typeof restaurant_edit !== 'undefined')
	{
		$("#restaurant").trigger("change");
	
	}



	$(".statusRestaurant").on('click',function(e){
		var restaurant_id = $(this).val();
		$.post(base_url+"panel/Locations/RestaurantStatus", {restaurant_id:restaurant_id}, function (status) {
			
			var info = status;
			
			$.each(info,function(index,inf){
				var status = inf.status;
				if(status == 0)
				{
					status = 1;
					
					$.post(base_url+"panel/Locations/RestaurantStatusUpdate", {restaurant_id:restaurant_id, status:status }, function () {
						
					}, "json");
					$(e.delegateTarget).css("background-color","green");
					$(e.delegateTarget).text("Açık");
					
				}
				else
				{
					status = 0;
					
					$.post(base_url+"panel/Locations/RestaurantStatusUpdate", {restaurant_id:restaurant_id, status:status }, function () {
						
					}, "json");
					$(e.delegateTarget).css("background-color","red");
					$(e.delegateTarget).text("Kapalı");
				}
			});

		}, "json");
	});

	//Restaurant Ekleme
	$("#Add_Restaurant").on('click',function(e){
		e.preventDefault();
		var restaurant_id = $("#restaurant").val();
		

		$.post(base_url+"panel/Locations/addRestaurant", $('#Restaurant').serializeArray(), function (result) {

			if(result.status == "success"){
				success_warning();
			}
			else
			{
				var result_message = result.message;
				error_warning(result_message);
			}

		}, "json");

	});

	


	
	//Restaurant Düzenleme
	$("#Edit_Restaurant").on('click',function(e){
		e.preventDefault();
		var formData = $('#RestaurantEdit').serializeArray();
		var resultContainer = $('#RestaurantEdit .result');


		var restaurant_openTime = $('#RestaurantEdit .clock #openTime').val();
		var restaurant_closeTime = $('#RestaurantEdit .clock #closeTime').val();

		formData.map(function(val, key){
			if (val.name === "openTime" || val.name === "closeTime") {
				let openTime = moment(val.value, 'HH:mm:ss');		
				val.value = (moment(openTime).add(-(moment().utcOffset()), 'm').format('HH:mm:ss'));
			}
		});

		$.post(base_url+"panel/Locations/editRestaurant", formData , function (result) {


			if (result.status === "success") {
				success_warning();
	
			} else {
				var result_message = result.message;
				error_warning(result_message);
				
			}

			
		}, "json");

	});


	function success_warning()
	{
		$("#Panel_Warning").css("display","block");
		$("#Panel_Warning .status").removeClass("modal-content bg-danger text-white");
		$("#Panel_Warning .status").addClass("modal-content bg-success text-white");
		$("#Panel_Warning .status .modal-title").text("Kayıt Başarılı");
		setTimeout(warning_close, 2000);
	}

	function error_warning(result_message)
	{
		$("#Panel_Warning").css("display","block");
		$("#Panel_Warning .status").removeClass("modal-content bg-success text-white");
		$("#Panel_Warning .status").addClass("modal-content bg-danger text-white");
		$("#Panel_Warning .status .modal-title").text(result_message);
		setTimeout(warning_close, 2000);
	}

	function warning_close()
	{
		$("#Panel_Warning").css("display","none");
	}


	
	//UTC saatin local saate çevrilmesi	
	var restaurant_id = $(".Restaurant .Saat").val();
	$.post(base_url+"panel/Locations/ClockTime",{restaurant_id:restaurant_id}, function (restaurant_info) {
		
		$.each(restaurant_info,function(index,info){
			var openTime_ = info.open_time;
			var closeTime_ = info.close_time;

			var openTime = moment(openTime_, 'HH:mm:ss');    
			openTime = (moment(openTime).add(+(moment().utcOffset()), 'm').format('HH:mm:ss'));

			var closeTime = moment(closeTime_, 'HH:mm:ss');    
			closeTime = (moment(closeTime).add(+(moment().utcOffset()), 'm').format('HH:mm:ss'));
		
		
			$('.Restaurant .clock #openTime').val(openTime);
			$('.Restaurant .clock #closeTime').val(closeTime);
			
		});

	}, "json");
		
	

	//Restaurant Silme
	$(".delete").on('click',function(){
		var restaurant_id = $(this).attr("id");
		$.post(base_url+"panel/Locations/delete", {restaurant_id: restaurant_id}, function () {

		}, "json");
		location.reload();
	});



	//Seçilen şehire ait ilçelerin ekrana bastırıldığı yer(Restaurant Adresi)
	$(".Restaurant .Address .sehir").on('change',function(e){
		
		$('.Restaurant .Address .mahalle').empty();
		$('.Restaurant .Address .ilce').empty();

		var sehir_key = $('option:selected', this).attr('key');
		var sehir_id = $(this).val();

		$.post(base_url+"panel/Address/get_ilceler", {sehir_key: sehir_key}, function (ilceler) {
			$.each(ilceler,function(index,ilce){
				$('.Restaurant .Address .ilce').append(`
					<option value="${ilce.ilce_id}" key="${ilce.ilce_key}">${ilce.ilce_title}</option>
				`);
			});
		}, "json");
	
	});

	//Seçilen ilçeye ait mahallelerin ekrana bastırıldığı yer (Restaurant Adresi)
	$(".Restaurant .Address .ilce").on('change',function(e){
		$('.Restaurant .Address .mahalle').empty();

		var ilce_key = $('option:selected', this).attr('key');
		var ilce_id = $(this).val();
		if(ilce_key != "")
		{

			$.post(base_url+"panel/Address/get_mahalleler", {ilce_key: ilce_key}, function (mahalleler) {

				$.each(mahalleler,function(index,mahalle){
					$('.Restaurant .Address .mahalle').append(`
						<option class="mahalle_" value="${mahalle.mahalle_id}" data-ilce-key="${ilce_key}"  data-ilce-id="${ilce_id}" key="${mahalle.mahalle_key}">${mahalle.mahalle_title}</option>
					`);
				});
			}, "json");
		}
		
	});

	
	//Lokasyon Düzenleme Ekranında Önceden Seçilmiş Gönderim Bölgeleri Adresleri Ekrana Bastırılması
	var sehir_key = $(this).attr('key');
	var location_sehir;
	var location_ilce;
	var location_mahalle;

	$.post(base_url+"panel/Address/locationAddress", {restaurant_id: restaurant_id}, function (address) {
		location_sehir = address.sehir;
		location_ilce = address.ilce;
		location_mahalle = address.mahalle;
	}, "json").done(function(){
		location_address();
	});

	function location_address(){

		$.post(base_url+"panel/Address/get_sehirler", function (sehirler) {
			$.each(sehirler,function(index,sehir){
				var checked = "";
				$.each(location_sehir,function(index,loca_sehir){

					if(loca_sehir.sehir_id === sehir.sehir_id)
					{
						checked = "selected";
					}
				});

				$('.Restaurant .location .sehir').append(`
					<option value="${sehir.sehir_id}" ${checked} key="${sehir.sehir_key}">${sehir.sehir_title}</option>
				`);	

			});

		},"json").done(function(){
			selected_sehir();
		});

		
		function selected_sehir()
		{
			//Otomatik olarak seçili gelen şehire ait ilçelerin listelenmesi
			$('.location .sehir option').each(function() {
			    if (this.selected)
			    {
					var sehir_key = $(this).attr('key');
					
			    	$.post(base_url+"panel/Address/get_ilceler", {sehir_key: sehir_key}, function (ilceler) {

						$.each(ilceler,function(index,ilce){
							var checked = "";
							$.each(location_ilce,function(index,loc_ilce){

								if(loc_ilce.ilce_id === ilce.ilce_id)
								{
									checked = "selected";
								}
							});

							$('.Restaurant .location .ilce').append(`
								<option value="${ilce.ilce_id}" ${checked} key="${ilce.ilce_key}">${ilce.ilce_title}</option>
							`);	

						});

					}, "json").done(function(){
						mahalle();
					});
					

					function mahalle()
					{
						//Otomatik olarak seçili gelen ilçelere ait mahallelerin listelenmesi
						$('.location .ilce option').each(function() {
						    if (this.selected)
						    {
						    	var ilce_key = $(this).attr('key');
						    	var ilce_id = $(this).val();

						    	$.post(base_url+"panel/Address/get_mahalleler", {ilce_key: ilce_key}, function (mahalleler) {

									$.each(mahalleler,function(index,mahalle){
										var checked = "";
										$.each(location_mahalle,function(index,loc_mah){

											if(loc_mah.mahalle_id === mahalle.mahalle_id)
											{
												checked = "selected";
											}
										});

										$('.Restaurant .location .mahalle').append(`
											<option class="mahalle_" value="${mahalle.mahalle_id}" ${checked} data-ilce-key="${ilce_key}"  data-ilce-id="${ilce_id}" key="${mahalle.mahalle_key}">${mahalle.mahalle_title}</option>
										`);
									});
								}, "json");
						    }   
						});
					}
			    }   
			});
		}

	}

	




	//Seçilen şehire ait ilçelerin ekrana bastırıldığı yer
	$(".location .sehir").on('select2:select',function(e){
		var sehir_key = $(e.params.data.element).attr('key');
		$('.Restaurant .location .mahalle').empty();
		$('.Restaurant .location .ilce').empty();
		$('.Restaurant .RestaurantAreaPrice .addRestaurant').empty();

		$.post(base_url+"panel/Address/get_ilceler", {sehir_key: sehir_key}, function (ilceler) {
			$.each(ilceler,function(index,ilce){
				$('.Restaurant .location .ilce').append(`
					<option value="${ilce.ilce_id}" key="${ilce.ilce_key}">${ilce.ilce_title}</option>
				`);
			});
		}, "json");
			
	});



	//Seçilen ilçeye ait mahallelerin ekrana bastırıldığı yer
	$(".location .ilce").on('select2:select',function(e){
		var ilce_key = $(e.params.data.element).attr('key');
		var ilce_id = $(e.params.data.element).val();
		if(ilce_key != "")
		{
			$.post(base_url+"panel/Address/get_ilce_title", {ilce_key: ilce_key}, function (ilce_title) {
				$.each(ilce_title,function(index,title){
					$('.Restaurant .location .mahalle').append(`
						<optgroup data-ilce-key="${ilce_key}" label="${title.ilce_title}">
					`);
				});
				
			}, "json").done(function(){

			});

			$.post(base_url+"panel/Address/get_mahalleler", {ilce_key: ilce_key}, function (mahalleler) {

				$.each(mahalleler,function(index,mahalle){
					$('.Restaurant .location .mahalle').append(`
						<option class="mahalle_" value="${mahalle.mahalle_id}" data-ilce-key="${ilce_key}"  data-ilce-id="${ilce_id}" key="${mahalle.mahalle_key}">${mahalle.mahalle_title}</option>
					`);
				});
			}, "json");
		}
		
	});



	//Seçilen Mahalleye Göre Minimum Fİyat Belirleme
	$(".location #mahalle").on('select2:select',function(e){
		
		var ilce_id = $(e.params.data.element).attr('data-ilce-id');
		var mahalle_id = $(e.params.data.element).val();
		var mahalle_title = $(e.params.data.element).text();

		$('.Restaurant .RestaurantAreaPrice .addRestaurant').append(`
			<tr class="mahalle_" data-ilceid="${ilce_id}" data-mahalleid="${mahalle_id}">
				<td><input type="text" class="form-control" data-ilce="${ilce_id}" data-mahalle="${mahalle_id}" name="mahalle_title[]" style="width: 100%" aria-describedby=""  value="${mahalle_title}" disabled></td>
				<td><input type="text" class="area_price form-control"  style="width: 100%" aria-describedby=""  value="" name="area_price[${mahalle_id}]"></td>
				<input class="area_ilce" type="hidden"  name="area_ilce_id[${mahalle_id}]" value="${ilce_id}">
				<input class="area_mahalle" type="hidden"  name="area_mahalle_id[${mahalle_id}]" value="${mahalle_id}">
			</tr>
		`);

	});
	



	$(".location #ilce").on('select2:unselect',function(e){
		var ilce_key = $(e.params.data.element).attr('key');
		var ilce_id = $(e.params.data.element).val();		
		$(`.Restaurant .location #mahalle option[data-ilce-key="${ilce_key}"]`).remove();
		$(`.Restaurant .location #mahalle optgroup[data-ilce-key="${ilce_key}"]`).remove();
		$(`.RestaurantAreaPrice .mahalle_[data-ilceid="${ilce_id}"]`).remove();
	});

	$(".location #mahalle").on('select2:unselect',function(e){
		var mahalle_id = $(e.params.data.element).val();
		$(`.RestaurantAreaPrice .mahalle_[data-mahalleid="${mahalle_id}"]`).remove();
	
	});




	//Saat
	$('.clockpicker').clockpicker();

	//Tarih
	$('.datepicker').datepicker({
	    format: 'yyyy/dd/mm',
	    startDate: '-3d'
	});



	$(".preview_image_select").change(function(){
        readURL(this);
    });
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            	
                $(input).siblings("img").attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    



	//Slider Resmi Silme
	$('.slide_image .delete_slide_image').on('click',function(){
		var img_id = $(this).attr("data-id");
		var img_src = $(this).attr("data-src");

		$.post(base_url+"panel/slidemanagement/deleteImage", {img_id: img_id, img_src:img_src}, function () {
			
		}, "json");
		location.reload();
	});



	//Logo Resmi Silme
	$('.logo_ .delete_logo_image').on('click',function(){
		var img_id = $(this).attr("data-id");
		var img_src = $(this).attr("data-src");

		$.post(base_url+"panel/Settings/deleteLogo", {img_id: img_id, img_src:img_src}, function () {
			
		}, "json");
		location.reload();
	});

	//Arka Plan Resmi Silme
	$('.logo_ .delete_body_image').on('click',function(){
		var img_id = $(this).attr("data-id");
		var img_src = $(this).attr("data-src");

		$.post(base_url+"panel/Settings/deleteBodyImage", {img_id: img_id, img_src:img_src}, function () {
			
		}, "json");
		location.reload();
	});




	var count = 1;
	$('.add_new_discount').on("click",function(){
		$('.promotion .promotion_table .promotion_body').append(`

			<tr>
                <td><input type="text" class="form-control" name="add_which_discount[${count}]" style="width: 100%" value=""></td>
                <td><input type="text" class="form-control" name="" style="width: 100%" aria-describedby="" disabled  value="Sipariş İndirimi"></td>
                <td><input type="text" class="form-control" name="add_discount_rate[${count}]" style="width: 100%" aria-describedby=""  value=""></td>
                <td><input type="checkbox" class="form-control" name="add_active[${count}]" value="1" checked ></td>
                <input type="hidden" name="add_count[${count}]" value="${count}">
            </tr>

		`);
		count++;
	});



	$('.delete_discount').on("click",function(){
		var discount_id = $(this).attr("key");
		$.post(base_url+"panel/Settings/delete_Discount", {discount_id:discount_id}, function () {
			
		}, "json");
		location.reload();
	});


	$('.get_Customer_Order').on("click",function(){

		var start_date = $('.order_search .start_date').val();
		var finish_date = $('.order_search .finish_date').val();

		var orders;
		var restaurantPos;
		var customers;
		var payment_type;
		var customer_address;
		var pos_orders;
		var order_detail;
		
		$.post(base_url+"panel/Customers/order_search", {start_date:start_date , finish_date:finish_date}, function (data) {

			orders = data.orders;
			restaurantPos = data.restaurantPos;
			customers = data.customers;
			payment_type = data.payment_type;
			customer_address = data.customer_address;
			pos_orders = data.pos_orders;
			order_detail = data.order_detail;

		},"json").done(function(){
			order_list();
		});

		function order_list()
		{
			$('.orders .details').empty();

			$.each(orders,function(i,order){
				$('.orders .details').append(`
					<tr class="detail${i}">
					</tr>
				`);
				$.each(restaurantPos,function(index,restaurant){
					if(restaurant.id === order.location_id)
					{
						$(`.orders .details .detail${i}`).append(`
							<td>
								${restaurant.title}
							</td>
						`);
					}
				});

				$.each(customers,function(index,customer){
					if(customer.id === order.user_id)
					{
						$(`.orders .details .detail${i}`).append(`
							<td>
								${customer.first_name} ${customer.last_name} 
							</td>
						`);
					}
				});

				$.each(payment_type,function(index,pay){
					if(pay.id === order.payment_type)
					{
						$(`.orders .details .detail${i}`).append(`
							<td>
								${pay.title}
							</td>
						`);
					}
				});

				$.each(customer_address,function(index,address){
					if(address.id === order.address)
					{
						$(`.orders .details .detail${i}`).append(`
							<td>
								${address.sehir_title} / ${address.ilce_title} / ${address.mahalle_title}
							</td>
						`);
					}
				});
				
				$(`.orders .details .detail${i}`).append(`
					<td>${order.order_date}</td>
					<td>${order.order_note}</td>
					<td>${order.total_price}</td>
					<td>${order.total_discount_price}</td>
				`);

				$.each(pos_orders,function(index,pos_order){
					if(pos_order.id === order.sale_id)
					{
						if(pos_order.status === "pending")
						$(`.orders .details .detail${i}`).append(`
							<td>
								<div class="order_status p-3 mb-2 bg-warning text-white" style="margin-top: 10%;"><center>Beklemede</center></div>
							</td>
						`);
						if(pos_order.status === "completed")
						$(`.orders .details .detail${i}`).append(`
							<td>
                                <div class="order_status p-3 mb-2 bg-success text-white" style="margin-top: 10%;"><center>Tamamlandı</center></div>
							</td>
						`);
						if(pos_order.status === "shipped")
						$(`.orders .details .detail${i}`).append(`
							<td>
                                <div class="order_status p-3 mb-2 bg-info text-white" style="margin-top: 10%;"><center>Yola Çıktı</center></div>
							</td>
						`);
						if(pos_order.status === "cancelled")
						$(`.orders .details .detail${i}`).append(`
							<td>
                                <div class="order_status p-3 mb-2 bg-secondary text-white" style="margin-top: 10%;"><center>İptal Edildi</center></div>
							</td>
						`);
					}
				});

				$(`.orders .details .detail${i}`).append(`
					<td><button type="button" key="${order.sale_id}" class="order_detail btn btn-info" data-toggle="modal" data-placement="top" title="Sipariş İle İlgili Ürünleri Görmek İçin Tıklayınız" data-target="#user_order_detail_Modal" style="margin-top: 13%;">Sipariş Detayları</button></td>
				`);

			});
		}


	});

	
	$('body').on('click','.order_detail',function(){
		
		$('#user_order_detail_Modal .modal-body .table-cart .products').remove();
		$('#user_order_detail_Modal .modal-body .total_discount_price .col-5').remove();
		$('#user_order_detail_Modal .modal-body .total_price .col-5').remove();
		$('#user_order_detail_Modal .modal-body .table-cart .options').remove();


		var sale_id = $(this).attr("key");
		var details = {};
		var products = {};
		var additional_products = {};

		$.post(base_url+"panel/Customers/order_detail", {sale_id:sale_id}, function (order_detail) {
			
			details = order_detail;
			products = order_detail.products;

		}, "json").done(function(){
			order_detail();
		});

		function order_detail()
		{
			$.each(products,function(index,product){

				var tax_price = parseFloat(((product.price / 100) * product.tax));
				var product_price = parseFloat(product.price);
				var price = product_price + tax_price;
		
				$('#user_order_detail_Modal .modal-body .table-cart').append(`
					<tr class="products">
						<td class="qty"><b>${product.quantity}</b></td>
						<td class="title">
							<span class="name"><b>${product.title}</b></span>
						</td>
						<td class="price "><b>${price} ₺</b></td>
					</tr>
				`);

				additional_products = product.additional_products;
				
				$.each(additional_products,function(index,pro){

					var add_tax_price = parseFloat(((pro.price / 100) * pro.tax));
					var add_product_price = parseFloat(pro.price);
					var add_price = add_product_price + add_tax_price;
					
					$('#user_order_detail_Modal .modal-body .table-cart').append(`
						<tr class="options">
							<td></td>
							<td class="text-success"><i>${pro.title}</i></td>
							<td class="text-success"><i>${add_price} ₺</i></td>
						</tr>
					`);
				})
				

			});

			

			$('#user_order_detail_Modal .modal-body .total_price').append(`
				<div class="col-5"><strong>${(parseFloat(details.subtotal) + parseFloat(details.taxtotal)).toFixed(2)} ₺</strong></div>
			`);

			$('#user_order_detail_Modal .modal-body .total_discount_price').append(`
				<div class="col-5"><strong>${details.amount} ₺</strong></div>
			`);	

		}
		
	});




 });	


