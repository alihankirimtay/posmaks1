$(document).ready(function(){



var loadBasket 		= true;
var products_ 		= {};
var categories_ 	= {};
var edit_Products	= false;
var rowid 			= "";
var restaurant_id;
var product_warning;
var location_logo;

	if(loadBasket == true)
	{
		basket();
	}

	$('.select2').select2();

	$.post(base_url+"home/logo", function (logo_) {
		location_logo = logo_;
	}, "json");

	$("#restaurant").on("change",function(){

		restaurant_id = $(this).val();

		$.post(base_url+"home/get_FindProductsWithRelatedItems", {restaurant_id: restaurant_id}, function (system_products) {
			
			products_ = system_products;

			

			$.post(base_url+"home/get_products_group", {restaurant_id: restaurant_id}, function (categories) {
			
				categories_ = categories;

			}, "json").done(function(){
				main();
			});
			
			

		}, "json").done(function(){
			main();
		});
	});

	if(typeof restaurant_edit !== 'undefined')
	{
		$("#restaurant").trigger("change");
	}


		
	// Ürünlerin Ekrana Getirildiği Fonksiyon
	function main(){
		let
		tabs = $("<ul></ul>").addClass("nav nav-menu bg-dark dark"),
		mobile_tabs = $("<ul></ul>").addClass("nav nav-menu bg-dark dark"),
		contents = $("<div></div>").addClass("menu");

		//Kategorilerin Sıralanması
		categories = $.map(categories_, (value, key) => { 
	        value.sort = Number(value.sort);
	        if (value.sort === 0) {
	          value.sort = 9999999;
	        }
	        return [value]; 
	    });

	    categories.sort((a,b) => {
	        if (a.sort === b.sort) {
	            if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
	            if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
	        } else {
	            if (a.sort > b.sort) return 1;
	            if (a.sort < b.sort) return -1;
	        }

	        return 0;
	    });


		$.each(categories,function(index, category){


			tabs.append($(`
				<li>
					<a href="#tab${category.id}" class="category_tab" key="tab${category.id}" role="button" >${category.title}</a>
				</li>
			`));
			mobile_tabs.append($(`
				<li>
					<a href="#tab${category.id}" class="category_tab" key="tab${category.id}" role="button" >${category.title}</a>
				</li>
			`));
			$('.menu-desktop #menu-navigation').append(tabs);
			$('.dropdown-menu-mobile #menu-dropdown').append(mobile_tabs);


			$.each(products_, function(index, product){
				
				var tax_price = parseFloat(((product.price / 100) * product.tax));
				var product_price = parseFloat(product.price);
				var price = (product_price + tax_price).toFixed(2);
				var product_image;
				
				if (product.product_type_id === category.id && product.active == 1) {

					if ($(`#tab${category.id}`, contents).length < 1) {
						contents.append(`
							<div id="tab${category.id}" class="menu-category">	
								<div class="menu-category-title">
                        			<h2 class="title">${category.title}</h2>
                    			</div> 
                    			<div class="menu-category-content"></div>
                    		</div>`);
					}
					
					if(typeof restaurant_edit !== 'undefined'){
						$.each(restaurant_products, function(index1,restaurant_product){


							if(restaurant_product.id === product.id)
							{
								if(product.image == null || product.image == "")
								{
									product_image = "Online/assets/img/photos/no-image.png";
								}
								else
								{
									product_image = product.image;
								}

								$(`#tab${category.id}`, contents).append(`
									

										<div class="menu-item menu-list-item" style="">
		                                    <div class="row align-items-center">
											
												<div class="col-sm-2 mb-2 mb-sm-0">
													<a href="#" id="productAttributes" role="button" data-target="#productModal" data-toggle="modal" data-restaurantid="${restaurant_id}" data-productname="${product.title}" data-price="${price}" data-productdesc="${product.desc}" data-productid="${product.id}">
														<img src="${service_url}${product_image}" class="img-thumbnail" style="width:150px; height:100px;">
													</a>
												</div>
												
		                                        <div class="col-sm-6 mb-2 mb-sm-0">
		                                            <a href="#" id="productAttributes" role="button" data-target="#productModal" data-toggle="modal" data-restaurantid="${restaurant_id}" data-productname="${product.title}" data-price="${price}" data-productdesc="${product.desc}" data-productid="${product.id}"><h6 class="product_title mb-0">${product.title}</h6></a>
		                                           
		                                        </div>
		                                        <div class="col-sm-4 text-sm-right">
		                                            <span class="text-md mr-4"><span class="text-muted"></span> ${price} ₺</span>
		                                            <!-- <button class="btn btn-success btn-sm" data-target="#productModal" data-toggle="modal"><span>Sepete Ekle</span></button> -->
		                                            <button id="productAttributes" type="button" class="btn btn-success btn-sm" data-target="#productModal" data-toggle="modal" data-restaurantid="${restaurant_id}" data-productname="${product.title}" data-price="${price}" data-productdesc="${product.desc}" data-productid="${product.id}"><span>Sepete Ekle</span></button>
		                                        </div>
		                                    </div>
		                                </div>   

								`);	
								return;	
							}

						});
					}
				}
			});
		});

		
		$('.page-content .col-md-9').append(contents);

	}
	
	//Kategori Menü Dropdown 
	$('.menudropbtn').on('click',function(){
        $("#menu-dropdown").toggleClass();
	});

	
	//Kategori seçildikten sonra o kategoriye ait ürün tabinin gelirken top:130px uygulanması.
	//ve kategori başlığının gözükmesi
	$('body').on('click','.category_tab',function(){
		var tab_key = $(this).attr('key');

		$('.menu-category').css('margin-top','0px');
		setTimeout(function(){
			$('#'+tab_key).css('margin-top','130px');}, 100);

		if(tab_key == "tab1")
		{
			setTimeout(function(){
			$('#'+tab_key).css('margin-top','0px');}, 5000);
		}
	});



	

	//Ürün arama
	$('body').on('keyup','#search',function(){
		var title = $(this).val();

		$('.menu-item').removeClass('hidden-xs-up ');

		$.each($('.menu-item .product_title'),function(index,pro_title){
			var product_title = $(this).text();
			var parent = $(this).closest('.menu-item');

			 if(product_title.toLowerCase().indexOf(title.toLowerCase()) === -1)
			{
				parent.addClass('hidden-xs-up');
			}
		});
	});
	
	

		
	
	//Ürünün Sepete Eklenmeden Önce Özelliklerinin Seçildiği Ekrana Giden Fonksiyon
	$('body').on('click','#productAttributes',function(){
		
		edit_Products = false;

		$('#add_cart span').text("SEPETE EKLE");

		if(document.getElementById("productModal").style.display === "none")
		{
			$('.modal-content .col-12').remove();
			$('.modal-content .col-3').remove();
			$('.panel-details-content .col-sm-6 .form-group').remove();
			$(".panel-details #panelDetailsOther #productDetails").val("");
			$('#productModal .productQuantity #quantity').val(1);

		}

		var restaurant_id 	= $(this).data("restaurantid");
		var product_id 		= $(this).data("productid");
		var product_name 	= $(this).data("productname");
		var product_price 	= $(this).data("price");
		var product_desc	= $(this).data("productdesc");
		$.each(products_,function(index,pro){

			if(pro.id === String(product_id))
			{	
				
				var stocks_ = pro.stocks;
				var additional_products_ = pro.additional_products;
				var menu_products = pro.products;

				if(pro.image == null || pro.image == "")
				{
					$('.product_image_bg').css("background-image",`url(${service_url}Online/assets/img/photos/modal-add.jpg)`);	
				}

				else
				{
					$('.product_image_bg').css("background-image",`url(${service_url+pro.image})`);
				}
				

				if(menu_products === undefined)
				{
					$('#productModal .Menu').css("display","none");
				}
				else{

					$('#productModal .Menu').css("display","block");
					//MenuItems
					$.each(menu_products,function(index,m_pro){
						$('#panelDetailsMenu .panel-details-content .col-sm-6').append(`
							<div class="form-group">
								<span class="custom-control-description">${m_pro.title}</span><br>
							</div>
						`);
					});
					
				}
				if(stocks_ !== undefined)
				{
					if(stocks_.length != 0){

						//Materials
						$.each(stocks_,function(index,stock){

							let stock_optional_control = 0;
							if(stock.product_stock_optional == 1)
							{
								stock_optional_control++;
								console.log(stock_optional_control);
								
							}
							if(stock_optional_control >= 1)
							{
								console.log(stock_optional_control);
								$('#productModal .Materials').css("display","block");
								$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}">
											<span class="custom-control-description">${stock.title} </span>
										</label>
									</div>
								`);
							}
							else
							{
								$('#productModal .Materials').css("display","none");
								$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}" disabled>
											<span class="custom-control-description">${stock.title} </span>
										</label>
									</div>
								`);
							}

						});
					}
					else
					{
						$('#productModal .Materials').css("display","none");
					}
				}
				


				if(additional_products_.length != 0){
					$('#productModal .Additions').css("display","block");
					//Additional
					
		
					$.each(additional_products_,function(index,additional_product){

						var add_tax_price = parseFloat(((additional_product.price / 100) * additional_product.tax));
						var add_product_price = parseFloat(additional_product.price);
						var add_price = add_product_price + add_tax_price;

						$('#panelDetailsAdditions .panel-details-content .col-sm-6').append(`
							<div class="form-group">
								<label class="custom-control custom-checkbox">
									<input type="checkbox"  name=additional_products price="${add_price}" title="${additional_product.title}" value="${additional_product.id}">
									<span class="custom-control-description">${additional_product.title} </span>
									<span class="custom-control-description">  ${add_price} ₺</span>
								</label>
							</div>
						`);
					});
				}
				else
				{
					$('#productModal .Additions').css("display","none");
				}
				
			}

		});
		

		//Select Product
		$('.modal-content .modal-product-details .row').append(`
			<div class="col-12">
				<h6 class="mb-0">${product_name}</h6> 
				<p class="mb-1">${product_price} ₺</p>
				<select id="productSelect" class="custom-select mb-4 mr-sm-2 mb-sm-0" style="width: 100%" id="product_details" name="product_details">
					<option value="${product_id}" restaurant_id="${restaurant_id}" price="${product_price}" title="${product_name}"">${product_name}</option>	    
				</select>
				
			</div>
				
		`);

		//Select Porsion Product
		$.each(products_, function(index, product){

			var tax_price = parseFloat(((product.price / 100) * product.tax));
			var product_price = parseFloat(product.price);
			var price = product_price + tax_price;

			if(product.portion_id === String(product_id))
			{
				$('.modal-content .modal-product-details .custom-select').append(`
					<option value="${product.id}" restaurant_id="${restaurant_id}" price="${price}" title="${product.title}">${product.title}</option>		
				`);
			}

		});	
			
		

	});


	//Ürünlerin Porsiyonu Varsa Onun Seçildiği Ekran
	$('body').on('change','#productSelect',function(){

		var product_id = $(this).val();

		
			
		$('.modal-content .col-3').remove();
		$('.panel-details-content .col-sm-6 .form-group').remove();	

		$.each(products_,function(index,pro){
			if(pro.id === String(product_id))
			{
				var tax_price = parseFloat(((pro.price / 100) * pro.tax));
				var product_price = parseFloat(pro.price);
				var price = product_price + tax_price;

				$('.modal-content .modal-product-details .row .mb-0').empty();
				$('.modal-content .modal-product-details .row .mb-1').empty();

				$('.modal-content .modal-product-details .row .col-12 .mb-0').text(pro.title);
				$('.modal-content .modal-product-details .row .col-12 .mb-1').text(price + " ₺");
			}
			
		});


		$.each(products_,function(index,pro){
			if(pro.id === String(product_id))
			{
				
				var stocks_ = pro.stocks;
				var additional_products_ = pro.additional_products;

				//Additional
				if(stocks_.length != 0){
					$('#productModal .Materials').css("display","block");
					$.each(stocks_,function(index,stock){
						if(stock.product_stock_optional == 1)
						{
							$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}">
										<span class="custom-control-description">${stock.title} </span>
									</label>
								</div>
							`);
						}

						// else if(stock.product_stock_optional == 0)
						// {
						// 	$('#productModal .Materials').css("display","none");
						// 	$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
						// 		<div class="form-group">
						// 			<label class="custom-control custom-checkbox">
						// 				<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}" disabled checked>
						// 				<span class="custom-control-description">${stock.title} </span>
						// 			</label>
						// 		</div>
						// 	`);
						// }
					});
				}
				else
				{
					$('#productModal .Materials').css("display","none");
				}
				
				if(additional_products_.length != 0){
					$('#productModal .Additions').css("display","block");
					$.each(additional_products_,function(index,additional_product){

						var add_tax_price = parseFloat(((additional_product.price / 100) * additional_product.tax));
						var add_product_price = parseFloat(additional_product.price);
						var add_price = add_product_price + add_tax_price;

						$('#panelDetailsAdditions .panel-details-content .col-sm-6').append(`
							<div class="form-group">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" name="additional_products" id="${additional_product.id}" price="${add_price}" title="${additional_product.title}" value="${additional_product.id}">
									<span class="custom-control-description">${additional_product.title} </span>
									<span class="custom-control-description">${add_price} ₺</span>
								</label>
							</div>
						`);
					});
				}
				else
				{
					$('#productModal .Additions').css("display","none");
				}
				
			}

		});

	});

	

	//Farklı Lokasyondan Ürün Eklemede Sepeti Boşaltma Fonksiyonu
	$("#ProductWarning .ProductAdd").on("click",function(){
		
		$.post(base_url+"shopping_cart/destroy", function () {
			
		}, "json");
		
		product_warning = 1;
		add_cart();
		basket();
	
	});

	//Ürünün Sepete Eklenme İşleminin Yapıldığı Fonksiyon
	$('body').on('click','#add_cart',function(){

		add_cart();
		basket();

	});

	$('.quantity_plus').on("click",function(){
		var quantity = $('#quantity').val();
		quantity = parseFloat(quantity) + parseFloat(1);
		$('#quantity').val(quantity);
	});

	$('.quantity_minus').on("click",function(){
		var quantity = $('#quantity').val();
		quantity = parseFloat(quantity) - parseFloat(1);
		$('#quantity').val(quantity);
		if(quantity <= 0)
		{
			$('#quantity').val(0);
		}
	});



	//Seçilen Ürünleri Sepete Ekleme Fonksiyonu
	function add_cart()
	{


		var restaurant_id 	= $('option:selected','#productSelect').attr('restaurant_id');
		var product_id 		= $('#productSelect').val();
		var product_price ;
		var product_title ;
		var quantity 		= $('#quantity').val();

		var add_price;
		var add_title;

		if(quantity > 10000)
		{
			quantity = 10000;
		}

		$('#productModal .productQuantity #quantity').val(1);
		var basket_restaurant_id = $('#panel-cart #restaurant_id').val();

		$.each(products_,function(index,pro){
			if(product_id == pro.id){

				var tax_price = parseFloat(((pro.price / 100) * pro.tax));
				var pro_price = parseFloat(pro.price);
				product_price = pro_price + tax_price;
				product_title = pro.title;

			}
		});


		var additional_products = [];
		$("input:checkbox[name=additional_products]:checked").each(function(index) {
			additional_products[index] = [
				id 	  = $(this).val(),
				title = "",
				price = ""
			];	
			
		});

		

		$.each(additional_products,function(index,add_pro){
			$.each(products_,function(index,pro){
				if(add_pro[0] == pro.id){

					var tax_price = parseFloat(((pro.price / 100) * pro.tax));
					var pro_price = parseFloat(pro.price);
					add_price = pro_price + tax_price;
					add_title = pro.title;

					add_pro[1] = add_title;
					add_pro[2] = add_price;

				}
			});
		});

		var stock_products = [];
		$("input:checkbox[name=stocks]:checked").each(function(index) {
			stock_products[index] = [
				id 	  = $(this).val(),
				title = $(this).attr('title')	
			];
			
		});

		var product_details = $('#productDetails').val();


		if(restaurant_id == basket_restaurant_id || !basket_restaurant_id)
		{
			add_basket();	
		}
		else
		{
			$("#ProductWarning").modal();
			if(product_warning == 1)
			{
				add_basket();
				product_warning = 0;
			}
		}


		function add_basket(){
			if(edit_Products == true)
			{
				$.ajax({
					url:base_url+"shopping_cart/update",
					method:"POST",
					data:{rowid:rowid, restaurant_id:restaurant_id, product_id:product_id, product_title:product_title, product_price:product_price ,quantity:quantity, additional_products:additional_products, stock_products:stock_products, product_details:product_details},
					success:function(data)
					{
						edit_Products = false;
						basket();
					}
				});	
				
				
			}
			else
			{
				$.ajax({
					url:base_url+"shopping_cart/add",
					method:"POST",
					data:{restaurant_id:restaurant_id, product_id:product_id, product_title:product_title, product_price:product_price ,quantity:quantity, additional_products:additional_products, stock_products:stock_products, product_details:product_details},
					success:function(data)
					{
						basket();
					}
				});
				
			}
		}

	}




	
	
	//Sepetteki Ürünlerin Listelendiği Fonksiyon

	var totalPrice = 0;

	function basket()
	{
		
		
		var totalItem = 0;

		$.post(base_url+"shopping_cart/basket", {}, function (basket) {
			
			var basketProduct = basket;
			totalPrice = 0;

			

			$('.panel-cart-container .panel-cart-content .table-cart .products ').remove();
			$('.panel-cart-container .panel-cart-content .table-cart .options ').remove();

			$.each(basketProduct,function(index,product){
				$('#panel-cart #restaurant_id').val(product.restaurant_id);
				$('.order_cart .restaurant_id').val(product.restaurant_id);
		
				totalItem += product.qty;

				$('.panel-cart-container .panel-cart-content .table-cart ').append(`

					<tr class="products">
						<td class="qty"><b>${product.qty}</b></td>
						<td class="title">
							<span class="name"><b>${product.name}</b></span>
						</td>
						<td class="price "><b>${parseFloat(product.price).toFixed(2)} ₺</b></td>
						<td class="price "><b>${parseFloat(product.subtotal).toFixed(2)} ₺</b></td>
						<td class="actions">
							<button type="button"  data-target="#productModal" data-toggle="modal" data-restaurantid="${product.restaurant_id}" data-productname="${product.name}" data-price="${product.price}" data-productid="${product.id}"  data-rowid="${product.rowid}" data-qty="${product.qty}" data-notes="${product.details}" class="editProductAttributes ti ti-pencil"></button>
							<button type="button"  data-rowid="${product.rowid}" class="deleteProduct ti ti-close"></button>
						</td>
						
					</tr>
				`);

				totalPrice += product.price * product.qty;

				if(product.options != null)
				{
					$.each(product.options,function(index,option){
						var add_price = option.price * product.qty;
						$('.panel-cart-container .panel-cart-content .table-cart  ').append(`
							<tr class="options">
								<td></td>
								<td class="text-success"><i>${option.name}</i></td>
								<td class="text-success"><i>${option.price} ₺</i></td>
								<td class="text-success"><i>${parseFloat(add_price).toFixed(2)} ₺</i></td>
							</tr>
						`);
						totalPrice += option.price * product.qty;
					});
				}
			});


			$('.container .row .col-md-2 .module .cart-icon ').remove();
			$('.container .row .col-md-2 .module .cart-value ').remove();
			$('.panel-cart-container .panel-cart-content .cart-summary .row .col-5').remove();
			
			$('.container .row .col-md-2 .module ').append(`
				<span class="cart-icon">
					<i class="ti ti-shopping-cart"></i>
					<span class="notification">${totalItem}</span>
				</span>
			`);
			$('#header-mobile .notification').text(totalItem);
			$('.container .row .col-md-2 .module ').append(`
					<span class="cart-value">${parseFloat(totalPrice).toFixed(2)} ₺</span>
			`);
			$('.panel-cart-container .panel-cart-content .cart-summary .row ').append(`
				<div class="col-5"><strong>${parseFloat(totalPrice).toFixed(2)}  ₺</strong></div>
			`);

			

			if(basketProduct.length < 1){
				$('.go_sale_page span').text("Sipariş Verebilmek İçin Sepetinize Ürün Ekleyiniz");
				$('.go_sale_page').css({"opacity":".4" , "cursor":"default !important" , "pointer-events":"none"});
			}
			else
			{
				$('.go_sale_page span').text("Sipariş Ver");
				$('.go_sale_page').css({"opacity":"1" , "pointer-events":"auto"});
			}


		}, "json").done(function(){

			if(totalItem >= 1)
			{
				$('.mobile_basket_open').css('display','block'); //Mobilde en altta bulunan sepeti açma butonu.
			}
			else
			{
				$('.mobile_basket_open').css('display','none');
			}

			basket_discount();
		});
	}


	//İndirimlerin Hesaplanıp Ekrana Yeniden Basıldığı Yer
	function basket_discount()
	{
		

		var discount 			 = 0;
		var total_discount 		 = 0;
		var total_discount_price = 0;

		$.post(base_url+"shopping_cart/basket_discount", {}, function (data) {

		$('.panel-cart-container .panel-cart-content .order_cart-summary .discount ').empty();
		$('.panel-cart-container .panel-cart-content .order_cart-summary .total_price ').empty();
		$('.panel-cart-container .panel-cart-content .order_cart-summary .total_discount_price ').empty();
			
			var data_discount 	 = data.discount;
			var data_user_orders = data.user_orders;
			
			$.each(data_discount,function(index,data_disc){
				if(data_disc.active == 1 && data_disc.deleted_time == null && data_user_orders.length == (data_disc.which_discount - 1))
				{
					discount = (totalPrice / 100) * data_disc.discount_rate;
					total_discount += discount;

					$('.panel-cart-container .panel-cart-content .order_cart-summary .discount ').append(`
						<div class="col-7 text-right">${data_disc.which_discount}.${data_disc.title} (% ${data_disc.discount_rate})</div>
		                <div class="col-5"><strong>${parseFloat(discount).toFixed(2)} ₺</strong></div>
					`); 
				}

				if(data_disc.active == 1 && data_disc.deleted_time == null && data_disc.which_discount == 0)
				{
					discount = (totalPrice / 100) * data_disc.discount_rate;
					total_discount += discount;

					$('.panel-cart-container .panel-cart-content .order_cart-summary .discount ').append(`
						<div class="col-7 text-right">Her ${data_disc.title} (% ${data_disc.discount_rate})</div>
		                <div class="col-5"><strong>${parseFloat(discount).toFixed(2)} ₺</strong></div>
					`); 
				}
			});

			total_discount_price = totalPrice - total_discount;


			$('.panel-cart-container .panel-cart-content .order_cart-summary .total_price ').append(`
				<div class="col-7 text-right">Toplam Fiyat:</div>
				<div class="col-5"><strong>${parseFloat(totalPrice).toFixed(2)}  ₺</strong></div>
			`);

			$('.panel-cart-container .panel-cart-content .order_cart-summary .total_discount_price ').append(`
				<div class="col-7 text-right">Toplam İndirimli Fiyat:</div>
				<div class="col-5"><strong>${parseFloat(total_discount_price).toFixed(2)}  ₺</strong></div>
			`);
			$('.panel-cart-container .panel-cart-content .order_cart-summary #total_price_hidden ').val(parseFloat(totalPrice).toFixed(2));
			$('.panel-cart-container .panel-cart-content .order_cart-summary #total_discount_price_hidden ').val(parseFloat(total_discount_price).toFixed(2));


		},"json");


	}

	



	//Ürünün Düzenlendiği
	$('body').on('click','.editProductAttributes',function(e){
		e.preventDefault();//buton dinleme
		
		edit_Products = true;
		$('#add_cart span').text("SEPETİ GÜNCELLE");
		
		var restaurant_id 	= $(this).data("restaurantid");
		rowid 				= $(this).data("rowid");
		var product_id 		= $(this).data("productid");
		var product_name 	= $(this).data("productname");
		var product_price 	= $(this).data("price");
		var product_qty 	= $(this).data("qty");
		var product_notes 	= $(this).data("notes");

		var basketAdditional = {};
		var basketStock = {};

		$.post(base_url+"shopping_cart/editProduct", {rowid: rowid}, function (items) {
			basketAdditional = items.options;
			basketStock 	 = items.materials;
			
		}, "json").done(function(){
			getEditProduct();
		});
		
		if(document.getElementById("productModal").style.display === "none")
		{
			$('.modal-content .col-12').remove();
			$('.modal-content .col-3').remove();
			$('.panel-details-content .col-sm-6 .form-group').remove();
		}

		function getEditProduct(){

			$.each(products_,function(index,pro){
				if(pro.id === String(product_id))
				{
					var stocks_ = pro.stocks;
					var additional_products_ = pro.additional_products;
					var menu_products = pro.products;

					$('.productQuantity #quantity').val(product_qty);

					if(menu_products === undefined)
					{
						$('#productModal .Menu').css("display","none");

					}
					else{

						//MenuItems
						$('#productModal .Menu').css("display","block");

						$.each(menu_products,function(index,m_pro){
							$('#panelDetailsMenu .panel-details-content .menu').append(`
								<div class="form-group">
									<span class="custom-control-description">${m_pro.title}</span><br>
								</div>
							`);
						});
					}

					//Materials
					$.each(stocks_,function(index,stock){
						var checked = "";
						$.each(basketStock,function(index,bStock){
							if(bStock.id === stock.id)
							{
								checked = "checked";
							}
						});

						if(stocks_ != 0)
						{
							let stock_optional_control = 0;
							if(stock.product_stock_optional == 1)
							{
								stock_optional_control++;
								
							}
							if(stock_optional_control >= 1)
							{
								$('#productModal .Materials').css("display","block");
								$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}" ${checked}>
											<span class="custom-control-description">${stock.title} </span>
										</label>
									</div>
								`);
							}
							else
							{
								$('#productModal .Materials').css("display","none");
								$('#panelDetailsMaterials .panel-details-content .col-sm-6').append(`
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="stocks" id="${stock.id}" title="${stock.title}" value="${stock.id}" disabled>
											<span class="custom-control-description">${stock.title} </span>
										</label>
									</div>
								`);
							}

						}
						else
						{
							$('#productModal .Materials').css("display","none");
						}
						
						
					});
					
					
					//Additional
					$.each(additional_products_,function(index,additional_product){

						var checked = "";
						$.each(basketAdditional,function(index,item){
							if(item.id === additional_product.id)
							{
								checked = "checked";
							}
							
						});

						if(additional_products_ != 0)
						{
							var add_tax_price = parseFloat(((additional_product.price / 100) * additional_product.tax));
							var add_product_price = parseFloat(additional_product.price);
							var add_price = add_product_price + add_tax_price;
							
							$('#productModal .Additions').css("display","block");

							$('#panelDetailsAdditions .panel-details-content .additions').append(`
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox"  name=additional_products price="${add_price}" title="${additional_product.title}" value="${additional_product.id}" ${checked} >
										<span class="custom-control-description">${additional_product.title} </span>
										<span class="custom-control-description">${add_price} ₺</span>
									</label>
								</div>
							`);
						}
						else
						{
							$('#productModal .Additions').css("display","none");

						}

					});

					$(".panel-details #panelDetailsOther #productDetails").val(product_notes);
				}

			});
			

			//Select Product
			$('.modal-content .modal-product-details .row').append(`
				<div class="col-12">
					<h6 class="mb-0">${product_name}</h6> 
					<p class="mb-1">${product_price} ₺</p>
					<select id="productSelect" class="custom-select mb-4 mr-sm-2 mb-sm-0" style="width: 100%" id="product_details" name="product_details">
						<option value="${product_id}" restaurant_id="${restaurant_id}" price="${product_price}" title="${product_name}"">${product_name}</option>	    
					</select>
					
				</div>
					
			`);

			//Select Porsion Product
			$.each(products_, function(index, product){

				if(product.portion_id === String(product_id))
				{
					$('.modal-content .modal-product-details .custom-select').append(`
						<option value="${product.id}" restaurant_id="${restaurant_id}" price="${product.price}" title="${product.title}">${product.title}</option>		
					`);
				}

			});	
		}

	});

	//Sepetteki Seçilen Ürünü Silme
	$('body').on('click','.deleteProduct',function(){
		var rowid = $(this).data("rowid");
		$.ajax({
			url:base_url+"shopping_cart/delete",
			method:"POST",
			data:{rowid:rowid},
			success:function(data)
			{
				console.log("Başarılı");
				basket();
			}
		});
		
	});

	//Sepetteki Bütün Ürünleri Silme
	$('body').on('click','#deleteCart',function(e){
		e.preventDefault();//buton dinleme
		var rowid = $(this).data("rowid");
		$.ajax({
			url:base_url+"shopping_cart/destroy",
			method:"POST",
			data:{rowid:rowid},
			success:function(data)
			{
				console.log("Başarılı");
				basket();
			}
		});
		
	});


	//Otomatik Slider
	var slideIndex = 0;
	carousel();
	
	function carousel() {
		var i;
		var x = $(".mySlides");
		if(x.length >= 1)
		{
			for (i = 0; i < x.length; i++) {
			  x[i].style.display = "none"; 
			}
			slideIndex++;
			if (slideIndex > x.length) {slideIndex = 1} 
			x[slideIndex-1].style.display = "block"; 
			setTimeout(carousel, 5000); // Change image every 4 seconds
		}
		
	}
	

});