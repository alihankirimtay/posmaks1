$(document).ready(function(){

	$("#editModal #newAddress").on("click",function(){
		$('#editModal').removeClass().addClass('modal fade');
	});
	

	//Yeni adres eklerken şehirlerin listelendiği yer
	$('body').on('click','#newAddress',function(){

		let sehirler;
		$.post(base_url+"address/get_sehir", {}, function (information) {
			sehirler = information.sehirler;
			console.log(sehirler);

		}, "json").done(function(){
			sehir_list();
		});

		function sehir_list()
		{
			$.each(sehirler,function(index,sehir){
				console.log(sehir);
				$('.newAddress_ #sehir').append(`
					<option value="${sehir.sehir_id}" key="${sehir.sehir_key}">${sehir.sehir_title}</option>
				`);
			});
		}
	});


	//Kullanıcıya Ait Bilgilerin Getirilip Ekrana Basıldığı Fonksiyon
    $('body').on('click','#userEditModal',function(){
		
		var user_info;
		var sehirler;
		var user_address;

		$.post(base_url+"address/userEdit", {}, function (information) {

			user_info = information.user;
			sehirler = information.sehirler;
			user_address = information.address;

		}, "json").done(function(){
			userInformation();
		});
		
		function userInformation(){
			
			//Genel Kullanıcı Bilgileri
			$('#editModal #first_name').val(user_info[0].first_name);
			//$('#editModal #last_name').val(user_info[0].last_name);
			$('#editModal #email').val(user_info[0].email);
			$('#editModal #phone').val(user_info[0].phone);

			$('.newAddress_').css("display", "none");
			$('#newAddress').css("display", "block");

			$('.userAddress').remove();


			
			//Yeni adres eklerken şehirlerin listelendiği yer
			$('body').on('click','#newAddress',function(){
				$('#newAddress').css("display", "none");
				$('.newAddress_').css("display", "block");
				$.each(sehirler,function(index,sehir){
					$('.newAddress_ #sehir').append(`
						<option value="${sehir.sehir_id}" key="${sehir.sehir_key}">${sehir.sehir_title}</option>
					`);
				});
				
			});


			//Kullanıcının Kayıtlı Adresleri
			$.each(user_address,function(index,adres){
				if(adres.active == 0)
				{
					$('#editModal #address').append(`
					<div class="userAddress">
						<form action="<?= base_url('Address/update/')${adres.id}; ?>">
							<center>
								<div class="card text-black bg-light mb-2" style="width: 25rem;">
									<div class="card-body">
										<h5 class="card-title">${adres.adres_name}</h5>
										<p class="card-text">${adres.acik_adres}</p>
										<p class="card-text">${adres.mahalle_title} / ${adres.ilce_title} / ${adres.sehir_title} </p>
										<button type="button" class="editAddress btn btn-info" href="#editAddress_" data-target="#editAddress_" data-toggle="modal" data-addressid="${adres.id}" style="width: 40%"><span>Düzenle</span></button>
										<a href="${base_url}Address/delete/${adres.id}" class="card-link">
											<button type="button" class="btn btn-danger " style="width: 40%"><span>Adresi Sil</span></button>
										</a>
									</div>
								</div>
							</center>
						</form>
					</div>
					`);
				}
			});
			

			//Adres Güncellemede bütün şehirlerin listelendiği yer
			$.each(sehirler,function(index,sehir){
				$('#editAddress_  #sehirEdit').append(`
					<option value="${sehir.sehir_id}" key="${sehir.sehir_key}">${sehir.sehir_title}</option>
				`);
			});
		}
	});


    //Kullanıcının Geçimişteki Siparişleri
	$('body').on('click','#userOrdersModal_',function(){

		$('#userOrdersModal .orders').empty();

		var orders;
		var location;
		var payment_type;
		var customer_address;
		var pos_customer_order;

		$.post(base_url+"home/user_Orders", {}, function (user_orders) {
			orders = user_orders.user_orders;
			location = user_orders.restaurantPos;
			payment_type = user_orders.payment_type;
			customer_address = user_orders.customer_address;
			pos_customer_order = user_orders.pos_customer_order.result_object;


		}, "json").done(function(){
			userOrders();
		});

		function userOrders()
		{
			console.log(pos_customer_order);
			$.each(orders,function(index,order){

				$.each(location,function(index,loca){
					if(loca.id == order.location_id)
					{
						$('#userOrdersModal .orders').append(`
							<center><div class="card-header">Siparişin Verildiği Restaurant : ${loca.title}</div></center>
							<div class="card-body text-secondary">
						`);
					}
				});

				$.each(payment_type,function(index,pay_type){
					if(pay_type.id == order.payment_type)
					{
						$('#userOrdersModal .orders').append(`
							<h5 class="card-title">Ödeme Tipi :  ${pay_type.title}</h5>
						`);
					}
				});

				$.each(customer_address,function(index,cus_address){
					if(cus_address.id == order.address)
					{
						$('#userOrdersModal .orders').append(`
							<h5 class="card-title">Siparişin Verildiği Yer :  ${cus_address.sehir_title} / ${cus_address.ilce_title} / ${cus_address.mahalle_title}</h5>
						`);
					}
				});

				$('#userOrdersModal .orders').append(`
					<h5 class="card-title">Sipariş Notu : ${order.order_note} </h5>
					<h5 class="card-title">Sipariş Zamanı : ${order.order_date} </h5>
					<h5 class="card-title">Toplam Fiyat : ${order.total_price} ₺ </h5>
					<h5 class="card-title">İndirimli Fiyatı : ${order.total_discount_price} ₺ </h5>
				`);

				$.each(pos_customer_order,function(index,cus_order){

					if(cus_order.id == order.sale_id)
					{
						if(cus_order.status == "pending")
						{
							$('#userOrdersModal .orders').append(`
								<div class="p-3 mb-2 bg-warning text-white"><center>Beklemede</center></div>
							`);	
						}

						if(cus_order.status == "completed")
						{
							$('#userOrdersModal .orders').append(`
								<div class="p-3 mb-2 bg-success text-white"><center>Tamamlandı</center></div>
							`);	
						}

						if(cus_order.status == "shipped")
						{
							$('#userOrdersModal .orders').append(`
								<div class="p-3 mb-2 bg-info text-white"><center>Yola Çıktı</center></div>
							`);	
						}

						if(cus_order.status == "cancelled")
						{
							$('#userOrdersModal .orders').append(`
								<div class="p-3 mb-2 bg-secondary text-white"><center>İptal Edildi</center></div>
							`);	
						}
					}
						
				});
				

				$('#userOrdersModal .orders').append(`
					<button type="button" key="${order.sale_id}" class="orderDetailsButton btn btn-success btn-block" href="#UserOrderDetailsModal" data-target="#UserOrderDetailsModal" data-toggle="modal"><span>Sipariş Detayları</span></button>
					<br><br>
				`);

			});
		}

	});

	

	//Seçilen Siparişe Ait Detaylar(Ürünler, Toplam Fiyat vs.)
	$('body').on('click','.orderDetailsButton',function(){
		$('#userOrdersModal').removeClass().addClass('modal fade');

		$('#UserOrderDetailsModal .modal-body .order_detail .products').remove();
		$('#UserOrderDetailsModal .modal-body .total_discount_price .col-5').remove();
		$('#UserOrderDetailsModal .modal-body .total_price .col-5').remove();
		$('#UserOrderDetailsModal .modal-body .order_detail .options').remove();

		var sale_id = $(this).attr("key");
		var details = {};
		var products = {};
		var additional_products = {};

		$.post(base_url+"Home/user_order_detail", {sale_id:sale_id}, function (order_detail) {
			
			details = order_detail;
			products = order_detail.products;
			console.log(details);
			console.log(products);

		}, "json").done(function(){
			order_detail();
		});

		function order_detail()
		{
			$.each(products,function(index,product){

				var tax_price = parseFloat(((product.price / 100) * product.tax));
				var product_price = parseFloat(product.price);
				var price = product_price + tax_price;
		
				$('#UserOrderDetailsModal .modal-body .order_detail').append(`
					<tr class="products">
						<td class="qty"><b>${product.quantity}</b></td>
						<td class="title">
							<span class="name"><b>${product.title}</b></span>
						</td>
						<td class="price "><b>${price} ₺</b></td>
					</tr>
				`);

				additional_products = product.additional_products;
				
				$.each(additional_products,function(index,pro){

					var add_tax_price = parseFloat(((pro.price / 100) * pro.tax));
					var add_product_price = parseFloat(pro.price);
					var add_price = add_product_price + add_tax_price;

					$('#UserOrderDetailsModal .modal-body .order_detail').append(`
						<tr class="options">
							<td></td>
							<td class="text-success"><i>${pro.title}</i></td>
							<td class="text-success"><i>${add_price} ₺</i></td>
						</tr>
					`);
				})
				

			});

			

			$('#UserOrderDetailsModal .modal-body .total_price').append(`
				<div class="col-5"><strong>${(parseFloat(details.subtotal) + parseFloat(details.taxtotal)).toFixed(2)} ₺</strong></div>
			`);

			$('#UserOrderDetailsModal .modal-body .total_discount_price').append(`
				<div class="col-5"><strong>${details.amount} ₺</strong></div>
			`);	

		}


	});

	//Kullanıcı adresi güncelleme fonksiyonu
	$('body').on('click','.editAddress',function(){

		$('#editModal').removeClass().addClass('modal fade');

		if(document.getElementById("editAddress_").style.display === "none")
		{
			$('.form-group #sehirEdit #sehir').remove();
			$('.form-group #ilceEdit').empty();
			$('.form-group #mahalleEdit').empty();
		}

		var address_id 	= $(this).data("addressid");
	
		$.post(base_url+"address/addressEdit", {address_id: address_id}, function (address) {

			var address_ = address;
			console.log(address);
			
			$.each(address_,function(index,add){
				
				$('.example-box-content .addressName #adres_name').val(add.adres_name);
				
				$('.form-group #sehirEdit').append(`
					<option value="${add.sehir_id}" id="sehir" key="${add.sehir_key}" selected>${add.sehir_title}</option>		
				`);
				
				$('.form-group #ilceEdit').append(`
					<option value="${add.ilce_id}" key="${add.ilce_key}">${add.ilce_title}</option>		
				`);
				$('.form-group #mahalleEdit').append(`
					<option value="${add.mahalle_id}">${add.mahalle_title}</option>		
				`);
				$('.example-box-content .acikAdres #acik_adres').val(add.acik_adres);
				$('.example-box-content #address_id').val(address_id);
			});
			//$('.form-group #sehirEdit').change();
		}, "json");

	});

	$("#sehirEdit").on('change',function(){
		var sehir_key = $('option:selected', this).attr('key');
		var sehir_id = $(this).val();

			if(sehir_key ==""){
			}
			else
			{
				$.ajax({
					url: base_url +'Address/get_ilce',
					type:"POST",
					data: {'sehir_key' : sehir_key},
					dataType: 'json',
					success: function(data){
						$('#ilceEdit').empty();
						$('#mahalleEdit').empty();
						$('#ilceEdit').append(data);
						$('#ilceEdit').trigger("chosen:updated");
					},
					error: function(){
						alert('Error');
					}
				});
			}  
	});

	$("#ilceEdit").on('select2:select',function(e){
		var ilce_key = $(e.params.data.element).attr('key');
		var ilce_id = e.params.data.id;

			if(ilce_key ==""){
			}
			else
			{
				$.ajax({
					url: base_url +'Address/get_mahalle',
					type:"POST",
					data: {'ilce_key' : ilce_key},
					dataType: 'json',
					success: function(data){
						$('#mahalleEdit').empty();
						$('#mahalleEdit').append(data);
						$('#mahalleEdit').trigger("chosen:updated");
					},
					error: function(){
						alert('Error');
					}
				});
			}
	});
	
	
	$("#sehir").on('change',function(){
		var sehir_key = $('option:selected', this).attr('key');
		var sehir_id = $(this).val();

        $('#ilce').empty();
        $('#mahalle').empty();
            
            if(sehir_key ==""){
			}
			else
			{
				$.ajax({
					url: base_url +'Address/get_ilce',
					type:"POST",
					data: {'sehir_key' : sehir_key},
					dataType: 'json',
					success: function(data){
						$('#ilce').append(data);
						$('#ilce').trigger("chosen:updated");
					},
					error: function(){
						alert('Error');
					}
				});
			}  
	});
	
	$("#ilce").on('select2:select',function(e){
       
		var ilce_key = $(e.params.data.element).attr('key');
		var ilce_id = e.params.data.id;

			if(ilce_key ==""){
			}
			else
			{
				$.ajax({
					url: base_url +'Address/get_mahalle',
					type:"POST",
					data: {'ilce_key' : ilce_key},
					dataType: 'json',
					success: function(data){
                        $('#mahalle').empty();
						$('#mahalle').append(data);
						$('#mahalle').trigger("chosen:updated");
					},
					error: function(){
						alert('Error');
					}
				});
			}
    });

    //Görüş Derecelendirme
    var $star_rating = $('.star-rating .fa');
	var id;
	var siblings = ""

	$star_rating.on('click', function() {
		id = $(this).data("id");
		siblings = "input."+id;
		console.log(siblings);

		$star_rating.siblings(siblings).val($(this).data('rating'));
		
		return SetRatingStar();
	});
	
    var SetRatingStar = function() {
		
      	return $star_rating.each(function() {
		
			if($(this).data('id') == id)
			{
				if (parseInt($star_rating.siblings(siblings).val()) >= parseInt($(this).data('rating'))) 
				{
					return $(this).removeClass('fa-star-o').addClass('fa-star');
				} 
				else 
				{
					return $(this).removeClass('fa-star').addClass('fa-star-o');
				}
			}
			
      	});
    };
    
    SetRatingStar();
    

});