<?php $this->load->view('backend/template/header'); ?>

<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Müşteri Görüşleri</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">MÜŞTERİ GÖRÜŞLERİ</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>
<br>
<?php
    $flavor=0;
    $presentations=0;
    $interest=0;
    $service=0;
    $price=0;
    $cleaning=0;

    foreach($opinions as $opinion) 
    {

        $flavor += $opinion->flavor;
        $presentations += $opinion->presentations;
        $interest += $opinion->interest;
        $service += $opinion->service;
        $price += $opinion->price;
        $cleaning += $opinion->cleaning;

    }
    $count_opinion = count($opinions);
    $flavor = round($flavor/$count_opinion,2); 
    $presentations = round($presentations/$count_opinion,2); 
    $interest = round($interest/$count_opinion,2);  
    $service = round($service/$count_opinion,2);  
    $price = round($price/$count_opinion,2); 
    $cleaning = round($cleaning/$count_opinion,2);
    $total_average = round(($flavor+$presentations+$interest+$service+$price+$cleaning)/6,2); 
?>


<div class="Online_Admin_Panel_Content">
    <div class="total_average col-sm-12 col-md-12 col-lg-12">
        <center><h2>ORTALAMA</h2></center>
        <center><h1><?= $total_average ?></h1></center>
    </div>
    <div class="opinions_flavor col-sm-12 col-md-6 col-lg-3">
        <center><h2>LEZZET</h2></center>
        <center><h1><?= $flavor ?></h1></center>
    </div>

    <div class="opinions_presentations col-sm-12 col-md-6 col-lg-3">
        <center><h2>SUNUM</h2></center>
        <center><h1><?= $presentations ?></h1></center>
    </div>

    <div class="opinions_interest col-sm-12 col-md-6 col-lg-3">
        <center><h2>İLGİ</h2></center>
        <center><h1><?= $interest ?></h1></center>
    </div>

    <div class="opinions_service col-sm-12 col-md-6 col-lg-3">
        <center><h2>SERVİS HIZI</h2></center>
        <center><h1><?= $service ?></h1></center>
    </div>

    <div class="opinions_price col-sm-12 col-md-6 col-lg-3">
        <center><h2>FİYAT</h2></center>
        <center><h1><?= $price ?></h1></center>
    </div>

    <div class="opinions_cleaning col-sm-12 col-md-6 col-lg-3">
        <center><h2>TEMİZLİK</h2></center>
        <center><h1><?= $cleaning ?></h1></center>
    </div>

    <div class="main">
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th scope="col">AD SOYAD</th>
                    <th scope="col">EMAİL</th>
                    <th scope="col">MESAJ</th>
                    <th scope="col">LEZZET</th>
                    <th scope="col">SUNUM</th>
                    <th scope="col">İLGİ</th>
                    <th scope="col">SERVİS HIZI</th>
                    <th scope="col">FİYAT</th>
                    <th scope="col">TEMİZLİK</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($opinions as $opinion) { ?>
                <tr>
                <td><?= $opinion->first_name; ?></td>
                <td><?= $opinion->email; ?></td>
                <td><?= $opinion->message; ?></td>
                <td><?= $opinion->flavor; ?></td>
                <td><?= $opinion->presentations; ?></td>
                <td><?= $opinion->interest; ?></td>
                <td><?= $opinion->service; ?></td>
                <td><?= $opinion->price; ?></td>
                <td><?= $opinion->cleaning; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>

</div>
<?php $this->load->view('backend/template/footer'); ?>