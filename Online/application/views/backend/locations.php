<?php $this->load->view('backend/template/header'); ?>


<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Lokasyonlar</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">LOKASYONLAR</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">
	<?php foreach($restaurants as $restaurant) { ?>
		<?php foreach($restaurantPos as $restaurantP) { ?>
			<?php if($restaurant->id === $restaurantP['id']){ ?>
		
		
				<div class="p-2 mb-2 w-100 bg-light text-black ">
					<div class="location_button">
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-9"><p class="location_title"> <?= $restaurantP['title'] ?> </p></div>

							<?php if($restaurant->status == 1) { ?>
								<div class="col-sm-12 col-md-2 col-lg-1 ">
									<button type="button" value="<?= $restaurant->id ?>" class="statusRestaurant btn btn-success btn-block">Açık</button>
								</div>
							<?php } else { ?>
								<div class="col-sm-12 col-md-2 col-lg-1">
									<button type="button" value="<?= $restaurant->id ?>" class="statusRestaurant btn btn-danger btn-block">Kapalı</button>
								</div>
							<?php } ?>
							<div class="col-sm-12 col-md-2 col-lg-1">
								<a class="location_edit btn btn-warning btn-block" id="<?= $restaurant->id ?>" href="<?= base_url('panel/Locations/editPage/'.$restaurant->id); ?>">Düzenle</a>
							</div>
							<div class="col-sm-12 col-md-2 col-lg-1">
								<button type="button" id="<?= $restaurant->id ?>" class="delete btn btn-danger btn-block">Sil</button>
							</div>
						</div>
					</div>
				</div>
				<br>

			<?php } ?>
		<?php } ?>
	<?php } ?>
</div>
    
<?php $this->load->view('backend/template/footer'); ?>