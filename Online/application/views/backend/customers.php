<?php $this->load->view('backend/template/header'); ?>


<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Müşteriler</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">MÜŞTERİLER</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">

    <div class="main">
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th scope="col">AD</th>
                    <th scope="col">SOYAD</th>
                    <th scope="col">EMAİL</th>
                    <th scope="col">TELEFON</th>
                    <th scope="col">DURUM</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($customers as $customer) { ?>
                <?php foreach($groups as $group) { ?>

                    <?php if($customer->id == $group->user_id){ ?>
                        <?php if($group->user_id != 1) { ?>
                            <tr>
                            <td><?= $customer->first_name; ?></td>
                            <td><?= $customer->last_name; ?></td>
                            <td><?= $customer->email; ?></td>
                            <td><?= $customer->phone; ?></td>

                                <?php if($customer->active == 1) { ?>
                                    <td><button type="button" key="0" id="<?= $customer->id; ?>" class="CustomerStatus btn btn-success">Aktif</button></td>
                                <?php }else { ?>
                                    <td><button type="button" key="1" id="<?= $customer->id; ?>" class="CustomerStatus btn btn-danger">Pasif</button></td>
                                <?php } ?>
                            
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    

                    

                <?php } ?>
            <?php } ?>
            </tbody>
        </table>

    </div>
</div>

<?php $this->load->view('backend/template/footer'); ?>