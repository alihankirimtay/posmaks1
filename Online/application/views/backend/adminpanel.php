<?php $this->load->view('backend/template/header'); ?>

<?php $this->load->view('backend/warning'); ?>


<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/MainPanel'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Yeni Lokasyon Ekleme</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">YENİ LOKASYON EKLEME</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">
	<div class="Restaurant">
		<form id="Restaurant" >
			<h2>Restaurantlar</h2>
			<br>
			<div class="Restaurants">
			  	<select class="custom-select mb-2 mr-sm-2 mb-sm-0" style="width: 100%" id="restaurant" name="restaurant">
			  		<option value="">Restaurantlar</option>
			  		<?php foreach($restaurants as $restaurant) { ?>
				    <option value="<?= $restaurant['id']; ?>"><?= $restaurant['title'] ?></option>
				    <?php } ?>
			  	</select>
			</div>
			<br><br>

			
			<div class="location card card-block bg-faded" >
				<h2>Hizmet Vereceği Bölgeler</h2>

				<label for="sehir">Şehir</label>
			  	<select class="sehir custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="sehir" name="sehir">
			  		<option value="">Şehir...</option>
			  		<?php foreach($sehirler as $sehir) { ?>
				    <option value="<?= $sehir->sehir_id; ?>" key="<?= $sehir->sehir_key; ?>"><?= $sehir->sehir_title ?></option>
				    <?php } ?>
			  	</select>

				<br>
				<label class="mr-sm-2 " for="inlineFormCustomSelect">İlçe</label>
			  	<select class="ilce custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="ilce" name="ilce[]" multiple>
			  		
			  	</select>

			  	<br>
			  	<label class="mr-sm-2 " for="inlineFormCustomSelect">Mahalle</label>
			  	<select class="mahalle custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="mahalle" name="mahalle[]" multiple>
			  	</select>
			</div>
			<br><br>

			<h2>Ürünler</h2>
			<div class="product_group">
				<div id="myTab">
					
				</div>
				<div id="pills-tabContent">	
				</div>
			</div>
			<div class="result"></div>


		</form>
		<input type="button" id="Add_Restaurant" value="Kaydet" class="btn btn-success btn-lg btn-block"/>
	</div> 	
</div>
<?php $this->load->view('backend/template/footer'); ?>
