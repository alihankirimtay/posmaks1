<?php $this->load->view('backend/template/header'); ?>

<?php $this->load->view('backend/warning'); ?>


<script type="text/javascript">
	var restaurant_edit = true;
	var restaurant_products = <?= json_encode($products) ?>;
</script>


<div class="page_title">
    <div class="col-sm-6">
        <div class="back_icon">
            <a href="<?= base_url('panel/Locations/locations'); ?>" class="back_icon_button">
                <div class="back-card-full-icon fa fa-arrow-left"></div>
            </a>
        </div>
        <h4>Lokasyon Düzenleme</h4>
    </div>
    <div class="col-sm-6">
        <a href="#">LOKASYON DÜZENLEME</a>
        <a href="<?= base_url('panel/MainPanel'); ?>">
            <div class="card-full-icon fa fa-home"></div>
        </a>
    </div>
</div>

<div class="Online_Admin_Panel_Content">

	<div class="Restaurant">
		<form id="RestaurantEdit" >
		<?php foreach($restaurant_info as $info) { ?>
			<h3>Restaurant Adresi</h3>
			<div class="Address card card-block bg-faded" >
				<label for="sehir">Şehir</label>
			  	<select class="sehir custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="sehir" name="restaurant_sehir">
					<?php foreach($restaurant_address as $sehir_) { ?>
						<option value="<?= $info->sehir_id; ?>" key=""><?= $sehir_->sehir_title ?></option>
					<?php } ?>
					<?php foreach($sehirler as $seh) { ?>
						<option value="<?= $seh->sehir_id; ?>" key="<?= $seh->sehir_key; ?>"><?= $seh->sehir_title ?></option>
					<?php } ?>
			  	</select>

				<br>
				<label class="mr-sm-2 " for="inlineFormCustomSelect">İlçe</label>
			  	<select class="ilce custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="ilce" name="restaurant_ilce" >
				  	<?php foreach($restaurant_address as $ilce_) { ?>
					 	<option value="<?= $info->ilce_id; ?>" selected key=""><?= $ilce_->ilce_title ?></option>
					<?php } ?>
			  	</select>
			  
			  	<br>
			  	<label class="mr-sm-2 " for="inlineFormCustomSelect">Mahalle</label>
			  	<select class="mahalle custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="mahalle" name="restaurant_mahalle" >
				  <?php foreach($restaurant_address as $mahalle_) { ?>
					 	<option value="<?= $info->mahalle_id; ?>" selected key=""><?= $mahalle_->mahalle_title ?></option>
					<?php } ?>
			  	</select>
				
				<br>
			  	<label class="mr-sm-2 " for="inlineFormCustomSelect">Açık Adres</label>
			  	<input type="text" name="acik_adres" value="<?= $info->acik_adres; ?>" placeholder="Açık Adresi Yazınız">
			</div>
			<br><br>

			<div class="clock">
				<h3>Restaurant Çalışma Saatleri</h3>
					
				<div class="input-group clockpicker"  data-autoclose="true" style="width: 50%; float:left;" >
					<input type="text" class="form-control moment-to-local" id="openTime" name="openTime" value="<?= $info->open_time; ?>" placeholder="Açılış Saati">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
					</span>
				</div>
				<div class="input-group clockpicker"  data-autoclose="true" style="width: 50%" >
					<input type="text" class="form-control moment-to-local" id="closeTime" name="closeTime" value="<?= $info->close_time; ?>" placeholder="Kapanış Saati">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-time"></span>
					</span>
				</div>
				<input class="Saat" type="hidden"  name="" value="<?= $info->id; ?>">
					
			</div>
			<br><br>
			
			

			<div class="RestaurantPhone">
				<h3>Telefon Numarası</h3>
				<input type="text" class="phone form-control" name="phone" style="width: 100%" aria-describedby="" placeholder="Telefon Numarası Giriniz" value="<?= $info->phone; ?>">
			</div>
			<br><br>
			
			<div class="RestaurantMail">
				<h3>E-Mail Adresi</h3>
				<input type="text" class="mail form-control" name="mail" style="width: 100%" aria-describedby="" placeholder="E-Mail Giriniz" value="<?= $info->email; ?>">
			</div>
			<br><br>

			<div class="RestaurantMaps">

				<?php 
				    $settings = $this->settings->settings_model->index();
				    if($settings)
				    {
				        foreach ($settings as $map_api) {
				        	$map_api_key = $map_api->api_key;

				        }
				    } 
				?>

				<?php if($info->map_x != null && $info->map_y != null){ ?>
					<input type="hidden" name="lat" id="lat" value="<?= $info->map_x; ?>">
					<input type="hidden" name="lng" id="lng" value="<?= $info->map_y; ?>">
				<?php }else{ ?>
					<input type="hidden" name="lat" id="lat" value="40.99149345910838">
					<input type="hidden" name="lng" id="lng" value="29.022603929042816">
				<?php } ?>

				<h3>Harita</h3>
				<p>Bulunduğunuz lokasyonun yerini haritadan seçmek için kırmızı aracı sürükleyip bulunduğunuz yere taşımanız yeterli olacaktır</p>

				<div id="googleMap" style="width:100%;height:400px;"></div>

				<script>
					function myMap() {
						var lat_ = document.getElementById("lat");
						var lng_ = document.getElementById("lng");

						lat_ = lat_ ? parseFloat(lat_.value) : 40.99149345910838;
						lng_ = lng_ ? parseFloat(lng_.value) : 29.022603929042816;

						var map=new google.maps.Map(document.getElementById("googleMap"),{
							center:{
								lat: lat_,
								lng: lng_
							},
							zoom:15
						});

						var marker = new google.maps.Marker({
							position:{
								lat:lat_,
								lng:lng_
							},
							map:map,
							draggable: true
						});

						google.maps.event.addListener(marker,'dragend',function(){

							var lat = marker.getPosition().lat();
							var lng = marker.getPosition().lng();
							console.log(lat);
							console.log(lng);

							$('#lat').val(lat);
							$('#lng').val(lng);
						});

					
					}
				</script>

				<script src="https://maps.googleapis.com/maps/api/js?key=<?= "{$map_api_key}"; ?>&callback=myMap"></script>
			
			</div>

		<?php } ?>


			<select class="invisible " id="restaurant" name="restaurant">
				<option value="<?= $restaurant_id ?>"></option>
			</select>

			<h3>Gönderim Bölgeleri</h3>
			<div class="location card card-block bg-faded" >
				<label for="sehir">Şehir</label>
			  	<select class="sehir custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="sehir" name="sehir"></select>

				<br>
				<label class="mr-sm-2 " for="inlineFormCustomSelect">İlçe</label>
			  	<select class="ilce custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="ilce" name="ilce[]" multiple></select>
			  
			  	<br>
			  	<label class="mr-sm-2 " for="inlineFormCustomSelect">Mahalle</label>
			  	<select class="mahalle custom-select mb-2 mr-sm-2 mb-sm-0 select2" style="width: 100%" id="mahalle" name="mahalle[]" multiple></select>
			</div>

			<br><br>

			<div class="RestaurantAreaPrice">
				<h3>Gönderim Bölgelerine Göre Fiyatlandırma</h3>

				<table class="table table-bordered">
					<thead class="thead-light">
						<tr>
							<th scope="col">Gönderim Bölgesi</th>
							<th scope="col">Minimum Gönderim Fiyatı(₺)</th>
						</tr>
					</thead>
					<tbody class="addRestaurant">
				
					
						<?php foreach($area_address as $address) { ?>
						<tr class="mahalle_" data-ilceid="<?= $address->ilce_id ?>" data-mahalleid="<?= $address->mahalle_id ?>">
							<td><input type="text" class="form-control" data-ilce="<?= $address->ilce_id ?>" data-mahalle="<?= $address->mahalle_id ?>" name="mahalle_title[]" style="width: 100%" aria-describedby=""  value="<?= $address->mahalle_title ?>" disabled></td>
							<td><input type="text" class="area_price form-control"  style="width: 100%" aria-describedby=""  value="<?= $address->price ?>" name="area_price[<?= $address->mahalle_id ?>]"></td>
							<input class="area_ilce" type="hidden"  name="area_ilce_id[<?= $address->mahalle_id ?>]" value="<?= $address->ilce_id ?>">
							<input class="area_mahalle" type="hidden"  name="area_mahalle_id[<?= $address->mahalle_id ?>]" value="<?= $address->mahalle_id ?>">
						</tr>
						<?php } ?>
					
					</tbody>
				</table>
			</div>
			<br><br><br>


			<h3>Ürünler</h3>
			<div class="product_group">
				<div id="myTab">
					
				</div>

				<div id="pills-tabContent">	
				
				</div>
			</div>

			<div class="result"></div>

		</form>
		<input type="button" id="Edit_Restaurant" value="Kaydet" class="btn btn-success btn-lg btn-block"/>
	</div> 	
<div>

<?php $this->load->view('backend/template/footer'); ?>
