<script type="text/javascript">
var restaurant_edit = true;
var restaurant_products = <?= json_encode($products) ?>;
</script>   


<select class="invisible " id="restaurant" name="restaurant">
    <option value="<?= $restaurant_id ?>"></option>
</select>  

<?php $this->load->view('frontend/template/header'); ?>
    
    <?php $this->load->view('backend/warning'); ?>


    <?php $this->load->view('frontend/template/slider'); ?>
    
    <!--  Opinions      -->
    <?php $this->load->view('frontend/opinions'); ?>

    <!--   mobile header panel -->
    <?php $this->load->view('frontend/template/header_panel_mobile'); ?>
    
    <!--  UserEdit      -->
    <?php $this->load->view('frontend/auth/edit'); ?>

    <!-- UserOrders -->
    <?php $this->load->view('frontend/auth/user_orders'); ?>
    
    <!--  ShoppingCart      -->
    <?php $this->load->view('frontend/elements/cart'); ?>

    <!--   ProductAttributes      -->
    <?php $this->load->view('frontend/elements/productAttributes'); ?>

    <!-- User Login    -->
    <?php $this->load->view('frontend/auth/elements/login'); ?>


    <!-- User Create   -->
    <?php $this->load->view('frontend/auth/elements/register'); ?>

    <?php $this->load->library("cart"); ?>


    <!-- Body Overlay -->
    <div id="body-overlay"></div>

    </div>

<form action="<?= base_url("Sales/create_Sale/"); ?>" gourl="<?= base_url(); ?>">

<div class="container">
    <div class="order_cart bg-light text-dark">
        <br><br>
        <div class="panel-cart-title">
            <center><h3 class="title">Sepetim</h3></center>
        </div>
        
        <div class="panel-cart-container">
            <input type="hidden" class="restaurant_id" name="location_id" value="">
            <div class="panel-cart-content">
                <table class="table-cart">
                    <thead>
                        <th>Ürün Adedi</th>
                        <th>Ürün Adı</th>
                        <th>Ürün Birim Fiyatı</th>
                        <th>Toplam Fiyat</th>
                        <th>Düzenle</th>
                    </thead>
                    
                </table>

                <div class="order_cart-summary">
                    <br><br>
                    <div class="discount row text-lg" style="color: black;">
                       <div class="col-7 text-right">İndirim:</div>
                    </div>


                    <div class="total_price row text-lg" style="font-weight: bold; color: black;">
                        <div class="col-7 text-right">Toplam Fiyat:</div>
                    </div>
                    <div class="total_discount_price row text-lg" style="font-weight: bold; color: black;">
                        <div class="col-7 text-right">Toplam İndirimli Fiyat:</div>
                    </div>
                    <br><br>

                    <input type="hidden" id="total_price_hidden" name="total_price" value="3487">
                    <input type="hidden" id="total_discount_price_hidden" name="total_discount_price" value="3487">


                </div>
                <center><button id="deleteCart" class="btn btn-danger"><span>Sepeti Boşalt</span></button></center>
                <br><br>
            </div>
        </div>
    </div>    


    <br><br>
    <div class="adress bg-light text-dark">
        <center><h3 class="title">Gönderilecek Adresi Seçiniz</h3></center>
        <button type="button" id="newAddress" class="btn btn-info" style="width: 100%; display: block;" href="#newAddress_" data-target="#newAddress_" data-toggle="modal"><span>Yeni Adres Ekle</span></button>

        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col"></th>
                    <th scope="col">ADRES</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($address as $adres) { ?>
                <?php if($adres->active == 0){ ?>
                    <tr>
                        <td><input type="radio" id="<?= $adres->id; ?><?= $adres->adres_name ?>" name="active_address" value="<?= $adres->id;?>" style="width: 25;height: 25;"></td>
                        <td>
                        <label for="<?= $adres->id; ?><?= $adres->adres_name ?>">
                            <?= $adres->adres_name ?>/<?= $adres->acik_adres ?>/<?= $adres->mahalle_title ?> / <?= $adres->ilce_title ?>  / <?= $adres->sehir_title ?>
                                
                        </label>
                        </td>
                    </tr>
                <?php } ?>  
            <?php } ?>  
           
            </tbody>
        </table>
    </div>
    <br><br>

    <div class="payment_type bg-light text-dark" style="padding: 30px;">
        <center><h3 class="title">Ödeme Tipini Seçiniz</h3></center>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col"></th>
                    <th scope="col">ÖDEME TİPİ</th>
                </tr>
            </thead>
            <tbody>

                <?php if(count($payment_type) >= 1) { ?>
                    <?php foreach($payment_type as $type_) { ?>
                        <?php foreach($payment_type_pos as $type) { ?>
                            <?php if($type_->active == 1) { ?>
                                <?php if($type_->id === $type['id']) { ?>
                                    <tr>
                                        <td style="width: 60px;"><input type="radio" id="<?= $type_->id; ?><?= $type['title'] ?>" name="active_payment" value="<?= $type_->id; ?>" style="width:25; height:25;"></td>
                                        <td>
                                            <label for="<?= $type_->id; ?><?= $type['title'] ?>">
                                                <i class="<?= $type['icon'] ?> <?= $type['color'] ?>" style="margin-right: 15px;"></i><?= $type['title'] ?>
                                            </label>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?> 

            </tbody>
        </table>
    </div>
    <br><br>

    <div class="order_note bg-light text-dark" style="padding: 30px;">
        <center><h3>Sipariş Notları</h3></center>
        <!-- <input type="text" class="form-control" name="order_note" style="width: 100%" value=""> -->
        <textarea rows="4" id="productDetails" name="order_note" value="" placeholder="Sipariş ile ilgili notlarınız varsa yazınız"style="width: 100%"></textarea>

    </div>
    <br><br>

    <?php if ($this->ion_auth->logged_in()){ ?>

        <button type="button" name="doSubmit" class="btn btn-success btn-lg btn-block"><span>Siparişi Tamamla</span></button>

    <?php } ?>
</div>
</form>



	 

<?php $this->load->view('frontend/template/footer'); ?>