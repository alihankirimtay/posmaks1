<?php $this->load->library("cart"); ?>

<?php
    $locations = $this->locations->restaurants_model->restaurants();
    if(count($locations) == 1)
    {
        foreach ($locations as $location) {
            $select_location_id = $location->id;
        }
    }
    else
    {
      $select_location_id = $this->uri->segment(3);
    }
?>

<!-- Panel Cart -->
<div id="panel-cart">
    <div class="panel-cart-container">
        <div class="panel-cart-title">
            <center><h5 class="title">Sepetim</h5></center>
            <button class="close" data-toggle="panel-cart"><i class="ti ti-close"></i></button>
        </div>

        <input type="hidden" id="restaurant_id" name="restaurant_id" value="">
        <div class="panel-cart-content">

            <table class="table-cart">
                <thead>
                    <th>Ürün Adedi</th>
                    <th>Ürün Adı</th>
                    <th>Ürün Birim Fiyatı</th>
                    <th>Toplam Fiyat</th>
                    <th>Düzenle</th>
                </thead>
            </table>
            <div class="cart-summary">
                <hr class="hr-sm">
                <div class="row text-lg">
                    <div class="col-7 text-right text-muted">Toplam Fiyat:</div>
                    <div class="col-5">0 TL</strong></div>
                 </div>
            </div>
            <center><button id="deleteCart" class="btn btn-danger"><span>Sepeti Boşalt</span></button></center>
        </div>
    </div>
    
        <?php if ($this->ion_auth->logged_in()){ ?>

            <a href="<?= base_url("home/order/$select_location_id"); ?>" class="go_sale_page panel-cart-action btn btn-success btn-block btn-block btn-lg"><span>Sipariş Ver</span></a>

        <?php } else{ ?>

           <!-- <button type="button" class="panel-cart-action btn btn-secondary btn-block btn-lg" href="#LoginModal" data-target="#LoginModal" data-toggle="modal" style="background-color: black;"><span>Sipariş Verebilmek İçin Giriş Yapmalısınız</span></button> -->

            <div class="basket_login_and_sign_button">
              <button type="button" class="panel-cart-action btn btn-success btn-block btn-login" href="#LoginModal" data-target="#LoginModal" data-toggle="modal"><span>GİRİŞ YAP</span></button>

              <button type="button" class="panel-cart-action btn btn-warning btn-block" href="#RegisterModal" data-target="#RegisterModal" data-toggle="modal" data-dismiss="modal"><span>KAYIT OL</span></button>
            </div>
           
        <?php } ?>
   
    
</div>



  
  <!--Farklı Lokasyonların Ürünlerini Ekleme Uyarısı -->
  <div class="modal" id="ProductWarning">
    <div class="modal-dialog">
      <div class="modal-content">
      
        
        <div class="modal-header">
          <center><h4 class="modal-title" style="font-size: 1.4rem; font-family: cursive;">Sepetinizde başka bir restorandan ürün(ler) bulunmaktadır. Sepetinize bu restoranın ürününü eklemek için sepetinizi boşaltmak ister misiniz?</h4></center>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        
        <div class="modal-footer" style="padding: 1.5rem 11rem;">
          <button type="button" class="ProductAdd btn btn-success" data-dismiss="modal"><span>Tamam</span></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><span>Vazgeç</span></button>
        </div>
        
      </div>
    </div>
  </div>


