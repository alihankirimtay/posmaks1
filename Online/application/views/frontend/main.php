<script type="text/javascript">
var restaurant_edit = true;
var restaurant_products = <?= json_encode($products) ?>;
</script>   


<select class="invisible " id="restaurant" name="restaurant">
    <option value="<?= $restaurant_id ?>"></option>
</select> 


<?php $this->load->view('frontend/template/header'); ?>
    
    <!--  Slider And User      -->
    <?php $this->load->view('frontend/template/slider'); ?>


     <!--  Products      -->
    <?php $this->load->view('frontend/template/content'); ?>
    
    <!--   mobile header panel -->
    <?php $this->load->view('frontend/template/header_panel_mobile'); ?>

    <!--  Opinions      -->
    <?php $this->load->view('frontend/opinions'); ?>

    <!--  UserEdit      -->
    <?php $this->load->view('frontend/auth/edit'); ?>

    <!-- UserOrders -->
    <?php $this->load->view('frontend/auth/user_orders'); ?>


    <!--  ShoppingCart      -->
    <?php $this->load->view('frontend/elements/cart'); ?>

    <!--   ProductAttributes      -->
    <?php $this->load->view('frontend/elements/productAttributes'); ?>

    <!-- User Login    -->
    <?php $this->load->view('frontend/auth/elements/login'); ?>


    <!-- User Create   -->
    <?php $this->load->view('frontend/auth/elements/register'); ?>




<?php $this->load->view('frontend/template/footer'); ?>