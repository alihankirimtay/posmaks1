<!doctype html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css') ?>">

<link rel="stylesheet" href="<?= base_url('assets/plugins/bootstrap/dist/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/slick-carousel/slick/slick.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/animate.css/animate.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/animsition/dist/css/animsition.min.css') ?>">

<link rel="stylesheet" href="<?= base_url('assets/css/themify-icons.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css') ?>">
<link id="theme" rel="stylesheet" href="<?= base_url('assets/css/themes/theme-red.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/css/mainPage.css') ?>">


<script type="text/javascript">
    var base_url = "<?= base_url() ?>";
    var service_url = "<?= $this->configs->url ?>";
</script>


<title><?= $this->website->restaurant_name ?></title>
        

</head>

<?php if($this->website->bodyimage != null): ?>
    <body style="background-image:url(<?= base_url($this->website->bodyimage); ?>);">
<?php else: ?>
    <body>
<?php endif; ?>

<div class="container">


<!-- Body Wrapper -->
<div id="body-wrapper" class="animsition">

    <!-- Header -->
    <header id="header" class="light">

        <div class="container">
            <div class="row">
                <div class="col-md-3 header_logo">
                    <!-- Logo -->
                    <div class="module-logo">
                        <a href="<?= base_url(); ?>">
                            <?php 
                              $logo = $this->settings->settings_model->getLogo();
                              foreach ($logo as $image) { 
                            ?>
                              <img src="<?= base_url(); ?><?= $image->image; ?>" class="header_logo_image">
                            <?php } ?>
                        </a>
                    </div>
                </div>

                <?php
                    $locations = $this->locations->restaurants_model->restaurants(); 
                    if(count($locations) == 1)
                    {
                        $header_open = true;
                        foreach ($locations as $location) {
                            $select_location_id = $location->id;
                        }
                    }
                    else
                    {
                        $currentpage = $_SERVER['REQUEST_URI'];
                        if(strtolower($this->router->fetch_class()) == 'locations') 
                        {
                            $header_open = false;
                        } 
                        else 
                        {
                            $header_open = true;

                            $select_location_id = $this->uri->segment(3);

                        }   
                         
                    }
                ?>
                
                <?php 
                    if($header_open == true) 
                    { 
                        $open_time;
                        $close_time;
                        $now_time = date("H:i:s");
                        $status;

                        foreach($restaurant_info as $inf) {
                            $open_time = $inf->open_time;
                            $close_time = $inf->close_time;
                            $status = $inf->status;    
                        }
                ?>


                <div class="col-md-7 header_module">
                    <!-- Navigation -->
                    <nav class="module module-navigation right mr-4">
                        <ul id="nav-main" class="nav nav-main">
                            <?php if($status == 1) { ?>
                                <?php if(($now_time > $open_time && $now_time < $close_time)){ ?>
                                    <li>
                                        <div class="Location_status_open">
                                            <p>Açık</p>
                                        </div>
                                    </li>
                                <?php }else{ ?>
                                    <li>
                                        <div class="Location_status_close">
                                            <p>Kapalı</p>
                                        </div>
                                    </li>
                                <?php } ?>

                            <?php }else{ ?>
                                <li>
                                    <div class="Location_status_close">
                                        <p>Kapalı</p>
                                    </div>
                                </li>
                            <?php } ?>
                            <li><a href="<?= base_url(); ?>">HIZLI SİPARİŞ</a></li>
                            <li><a href="<?= base_url("info/location_info/"); ?><?= $select_location_id; ?>">HAKKIMIZDA</a></li>
                            <li><a href="#OpinionsModal" data-target="#OpinionsModal" data-toggle="modal" role="button" >GÖRÜŞLERİNİZ</a></li>
                            
                            <li><a href="<?= base_url("info/contact_info/"); ?><?= $select_location_id; ?>">İLETİŞİM</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-md-2 header_basket">
                    <a href="#" class="module module-cart right" data-toggle="panel-cart">
                        
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>

    </header>
    <!-- Header / End -->

    <?php if($header_open == true) { ?>
    <!-- Header -->
    <header id="header-mobile" class="light">

        <div class="module module-nav-toggle">
            <a href="#" id="nav-toggle" data-toggle="panel-mobile"><span></span><span></span><span></span><span></span></a>
        </div>    

        <div class="module module-logo">
            <a href="<?= base_url(); ?>">
                <?php 
                  $logo = $this->settings->settings_model->getLogo();
                  foreach ($logo as $image) { 
                ?>
                  <img src="<?= base_url(); ?><?= $image->image; ?>">
                <?php } ?>
            </a>
        </div>

        <a href="#" class="module module-cart" data-toggle="panel-cart">
            <i class="ti ti-shopping-cart"></i>
            <span class="notification">2</span>
        </a>

        <?php if($this->uri->segment(2) == "main" || $this->uri->segment(2) == "") { ?>
            <div class="mobile_basket_open">
                <button type="button" class="btn btn-success btn-block btn-lg" data-toggle="panel-cart"><span>Sipariş Ver</span></button>
            </div>
        <?php } ?>

    </header>

    <!-- Header / End -->
    <?php } ?>
   
      
