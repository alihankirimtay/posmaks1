<div class="slider_and_user">
  <div class="col-md-3" >
      <form action="#" class="validate-form">

        <?php if ($this->ion_auth->logged_in()){ ?>
            
            <div class="form-group form-submit">
               <center>
                  <div class="user_name">
                    <h3>
                      Hoşgeldin <br>
                      <?php
                        echo $this->user->first_name;
                      ?>
                    </h3>

                  </div>
               </center> 
            </div>

            
            <div class="form-group form-submit">
                  <button type="button" id="userEditModal" class="btn btn-warning btn-block" href="#editModal" data-target="#editModal" data-toggle="modal"><span>BİLGİLERİ GÜNCELLE</span></button>
            </div>

            <div class="form-group form-submit">
                <button type="button" id="userOrdersModal_" class="btn btn-success btn-block" href="#userOrdersModal" data-target="#userOrdersModal" data-toggle="modal"><span>SİPARİŞ GEÇMİŞİM</span></button>
            </div>

            <div class="form-group form-submit">
                <a href="<?= base_url('auth/logout'); ?>">
                  <button type="button" class="btn btn-danger btn-block"><span>ÇIKIŞ YAP</span></button>
                </a>
            </div>



        <?php } else{ ?>
             
             <style>
                @media only screen and (max-width: 767px){
                    .slider_and_user {
                        margin-top: 50px;
                        height: 230px;
                    }
                    .slider_and_user .col-md-9{
                      height: 200px;
                      display: flex;
                      flex-direction: row;
                      flex-wrap: wrap;
                      justify-content: center;
                      align-items: center;
                    }
                }
            </style>
            
             <div class="form-group form-submit">
                <button type="button" class="btn btn-success btn-block" href="#LoginModal" data-target="#LoginModal" data-toggle="modal"><span>GİRİŞ YAP</span></button>
            </div>
            
            <div class="form-group form-submit">
                <button type="button" class="btn btn-warning btn-block" href="#RegisterModal" data-target="#RegisterModal" data-toggle="modal" data-dismiss="modal"><span>KAYIT OL</span></button>
            </div> 
        
        <?php } ?>

        <?php if(count($this->locations->restaurants_model->restaurants()) >= 2): ?>
        <div class="form-group form-submit">
            <a href="<?= base_url(); ?>">
              <button type="button" class="btn btn-secondary btn-block"><span>ŞUBE DEĞİŞTİR</span></button>
            </a>
        </div>
      <?php endif; ?>
                      
      </form>
  </div>
      
  <div class="col-md-9">
                          
      <div class="slider">
        <?php $slider = $this->slides->slide_model->getImages(); ?>
         <?php  foreach ($slider as $image):?>
          <?php if($image->active == 1): ?> 
            <img class="mySlides" src="<?= base_url(); ?><?= $image->image; ?>" height="300px" width="800px" style="border-radius: 40px;">
          <?php endif; ?>
        <?php endforeach; ?>
      </div>

      

  </div>
</div>


