

<div class="modal fade" id="userOrdersModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Sipariş Geçmişim</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class=" example-box-content">

                    <div class="orders card border-secondary mb-3" style="max-width: 30rem;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade"  id="UserOrderDetailsModal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">SİPARİŞ DETAYLARI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="bg-light text-dark">
                    <div class="panel-cart-title">
                    
                    </div>

                    <div class="panel-cart-container">
                        <div class="panel-cart-content">
                            <center>
                            <table class="order_detail">

                                <thead>
                                    <th style="color:red;">Ürün Adedi</th>
                                    <th style="color:red;">Ürün Adı</th>
                                    <th style="color:red;">Ürün Birim Fiyatı</th>
                                </thead>
                                
                            </table>
                            </center>
                            <br><br>
                            <div class="summary">

                                <div class="total_price row text-lg" style="font-weight: bold; color: black;">
                                    <div class="col-7 text-right">Toplam Fiyat:</div>
                                </div>

                                <div class="total_discount_price row text-lg" style="font-weight: bold; color: black;">
                                    <div class="col-7 text-right">İndirimli Fiyat:</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
	