<!-- Modal / Login -->
<div class="modal fade" id="LoginModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Giriş Yap</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="example-box-content">

                     <form action="<?= base_url("Auth/login"); ?>" gourl="<?= base_url(); ?>">
                        <center>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="identity" aria-describedby="emailHelp" style="width: 75%" placeholder="Mail Adresi" value="<?= $this->input->post('identity') ?>">
                            </div>
                            <div class="form-group">
                                <label>Şifre</label>
                                <input type="password" class="form-control" name="password" style="width: 75%" placeholder="Şifre" value="<?= $this->input->post('password') ?>">
                            </div>
                            <div class="result"></div>
                            <button type="button" name="doSubmit" class="btn btn-primary" style="width: 25%"><span> Giriş Yap </span></button>         
                       
                        </center>
                    </form>
  
                </div>
            </div>
 
            
			<button type="button" class="modal-btn btn btn-link btn-block btn-lg" href="#PasswordSendModal" data-target="#PasswordSendModal" data-toggle="modal" data-dismiss="modal"><span>ŞİFREMİ UNUTTUM</span></button>
        </div>
    </div>
</div>

<!-- Modal / PasswordSend -->
<div class="modal fade" id="PasswordSendModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Şifremi Unuttum</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <form action="<?= base_url("auth/forgot_password/"); ?>">
                <div class="modal-product-details">
                    <div class="example-box-content">
                        
                        <div class="form-group">
                            <label>E-Mail Adresiniz</label>
                            <input name="identity" class="form-control" required="" type="email">
                        </div>
                        
                    </div>
                </div>
     
                <button type="button" name="doSubmit" class="modal-btn btn btn-success btn-block btn-lg" data-dismiss="modal"><span>GÖNDER</span></button>
                <button type="button" class="modal-btn btn btn-link btn-block btn-lg" href="#LoginModal" data-target="#LoginModal" data-toggle="modal" data-dismiss="modal"><span>GERİ</span></button>
            </form>
        </div>
    </div>
</div>