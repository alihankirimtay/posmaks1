<!-- Görüşleriniz -->


<div class="modal fade" id="OpinionsModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-lg dark bg-dark">
                <div class="bg-image"><img src="<?= base_url(); ?>/assets/img/photos/modal-add.jpg" alt=""></div>
                <h4 class="modal-title">Lütfen Bize Görüşlerinizi Bildiriniz</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ti-close"></i></button>
            </div>
            <div class="modal-product-details">
                <div class="example-box-content">


                    <form action="<?= base_url("Home/opinions/"); ?>" gourl="<?= $_SERVER['REQUEST_URI'] ?>">
                        <center>
                            <?php if ($this->ion_auth->logged_in()){ ?>
                                <div class="form-group">
                                    <label>Ad Soyad</label>
                                    <input type="text" class="form-control" name="first_name" style="width: 100%" placeholder="Adınızı ve Soyadınızı Giriniz" value="<?= $this->user->first_name; ?>">
                                </div>
                                <div class="form-group">
                                    <label>E-Mail</label>
                                    <input type="text" class="form-control" name="email" style="width: 100%" aria-describedby="emailHelp" placeholder="E-Mail Giriniz" value="<?= $this->user->email; ?>">
                                </div>
                            <?php }else{ ?>
                            <div class="form-group">
                                <label>Ad Soyad</label>
                                <input type="text" class="form-control" name="first_name" style="width: 100%" placeholder="Adınızı ve Soyadınızı Giriniz" value="">
                            </div>
                            <div class="form-group">
                                <label>E-Mail</label>
                                <input type="text" class="form-control" name="email" style="width: 100%" aria-describedby="emailHelp" placeholder="E-Mail Giriniz" value="">
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <label>Mesaj</label>
                                <textarea rows="4" cols="45" name="message" value="" placeholder="Mesajınızı Yazınız" class="opinions_message"></textarea>
                            </div>
                            
                            
                            <div class="star-rating"> 
                                <span>Lezzet:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="lezzet"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="lezzet"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="lezzet"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="lezzet"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="lezzet"></span>
                                <input type="hidden" name="lezzet" class="lezzet" value="0">
                            </div><br>
                            <div class="star-rating"> 
                                <span>Sunum:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="sunum"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="sunum"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="sunum"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="sunum"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="sunum"></span>
                                <input type="hidden" name="sunum" class="sunum" value="0">
                            </div><br>
                            <div class="star-rating"> 
                                <span>İlgi:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="ilgi"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="ilgi"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="ilgi"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="ilgi"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="ilgi"></span>
                                <input type="hidden" name="ilgi" class="ilgi" value="0">
                            </div><br>
                            <div class="star-rating"> 
                                <span>Servis Hızı:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="servis"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="servis"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="servis"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="servis"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="servis"></span>
                                <input type="hidden" name="servis" class="servis" value="0">
                            </div><br>
                            <div class="star-rating"> 
                                <span>Fiyat:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="fiyat"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="fiyat"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="fiyat"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="fiyat"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="fiyat"></span>
                                <input type="hidden" name="fiyat" class="fiyat" value="0">
                            </div><br>
                            <div class="star-rating"> 
                                <span>Temizlik:</span>
                                <span class="fa fa-star-o" data-rating="1" data-id="temizlik"></span>
                                <span class="fa fa-star-o" data-rating="2" data-id="temizlik"></span>
                                <span class="fa fa-star-o" data-rating="3" data-id="temizlik"></span>
                                <span class="fa fa-star-o" data-rating="4" data-id="temizlik"></span>
                                <span class="fa fa-star-o" data-rating="5" data-id="temizlik"></span>
                                <input type="hidden" name="temizlik" class="temizlik" value="0">
                            </div><br>
                            <br><br>

                            <button type="button" name="doSubmit" class="btn btn-success" style="width: 100%"><span> Gönder </span></button>
                         
                        </center>

                        <div class="result"></div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>