<?php $this->load->view('frontend/template/header'); ?>
	
	 <!--   mobile header panel -->
    <?php $this->load->view('frontend/template/header_panel_mobile'); ?>

	<!--  Opinions      -->
	<?php $this->load->view('frontend/opinions'); ?>

	<!--  ShoppingCart      -->
    <?php $this->load->view('frontend/elements/cart'); ?>

	<!--   ProductAttributes      -->
	<?php $this->load->view('frontend/elements/productAttributes'); ?>


	<div class="location_info" style="margin-top: 150px;">
		
		<center><h3>Hakkımızda</h3></center>
		<br>

		<div class="general_info bg-light text-dark" style="padding: 30px;">
			<h4>Genel Bilgiler</h4>

			<table class="table table-sm">
				<thead>
					<tr>
						<th scope="col">Adres</th>
						<th scope="col">Telefon Numarası</th>
						<th scope="col">Mail Adresi</th>

					</tr>
				</thead>
				<tbody>
				
					<tr>
						<?php foreach ($restaurant_address as $address) { ?>
							<td><?= $address->sehir_title; ?> / <?= $address->ilce_title; ?> / <?= $address->mahalle_title; ?> / <?= $address->acik_adres; ?> </td>
						<?php } ?>
						<?php foreach ($location_info as $info) { ?>
							<td><?= $info->phone; ?></td>
							<td><?= $info->email; ?></td>
						<?php } ?>
					</tr>
				
				</tbody>
			</table>
		</div>
		<br>

		<div class="location_time bg-light text-dark" style="padding: 30px;">
			<h4>Çalışma Saatleri</h4>

			<?php foreach ($location_info as $info) { ?>
				<?= $info->open_time; ?> / <?= $info->close_time; ?>
				 
			<?php } ?>
		</div>
		<br>

		<div class="location_time bg-light text-dark" style="padding: 30px;">
			<h4>Menü Kategorilerimiz</h4>

			<?php foreach ($categories as $category) { ?>
				<?= $category["title"]; ?> /	
			<?php } ?>

		</div>
		<br>

		<div class="location_discounts bg-light text-dark" style="padding: 30px;">
			<h4> İndirimler</h4>

			<table class="table table-sm">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">İndirim Tipi</th>
						<th scope="col">Fiyat</th>

					</tr>
				</thead>
				<tbody>
				<?php foreach ($discounts as $disc) { ?>

					<?php if($disc->active == 1 && $disc->deleted_time == null) { ?>

						<tr>
							<td><?= $disc->which_discount; ?></td>
							<td><?= $disc->title; ?></td>
							<td><?= $disc->discount_rate; ?> ₺</td>
						</tr>

					<?php } ?>
					 
				<?php } ?>

				</tbody>
			</table>

			
		</div>
		<br>

		<div class="location_payment_type bg-light text-dark" style="padding: 30px;">
			<h4>Ödeme Şekilleri</h4>

			<?php foreach ($payment_type_pos as $type_pos) {
				foreach ($payment_type as $type) {
					if($type_pos["id"] === $type->id && $type->active == 1) { ?>
						
						<?= $type_pos["title"]; ?> /

					<?php } ?>
				<?php } ?>
			<?php } ?>
		</div>
		<br>

		<div class="send_area bg-light text-dark" style="padding: 30px;">
			<h4>Gönderim Bölgeleri ve Minimum Gönderim Fiyatı</h4>

			<table class="table table-sm">
				<thead>
					<tr>
						<th scope="col">Bölge</th>
						<th scope="col">Fiyat</th>

					</tr>
				</thead>
				<tbody>
				<?php foreach ($min_area_price as $area) { ?>
					<tr>
						<td><?= $area->mahalle_title; ?></td>
						<td><?= $area->price; ?> ₺</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>




<?php $this->load->view('frontend/template/footer'); ?>