<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $configs;

	public function __construct()
	{
		parent::__construct();

		$this->db->query("SET time_zone='+0:00'");
		date_default_timezone_set('UTC');

		$root_app_url = str_replace('/Online', '', base_url());
		$root_app_url = rtrim($root_app_url, '/') . '/';

		$this->configs = (object) [
			'url' => $root_app_url, //'http://192.168.0.68/posmaks/'
		]; 

		$this->database_name = "online_";
		
		$this->load->library('Posmaks', [$this->configs->url], 'Posmaks');


		$this->slides = $this->load->model("backend/slide_model");
		$this->settings =$this->load->model("backend/settings_model");
		$this->locations = $this->load->model("backend/restaurants_model");
        $this->website = $this->db->get('online_settings', 1)->row();
        $this->user = $this->checkUser($this->getUserId());
        
		
	}

	public function isLoggedIn()
	{
		return $this->ion_auth->logged_in();
	}

	public function getUserId()
	{
		return $this->ion_auth->get_user_id();
	}

	public function checkUser($id)
	{
		return $this->db->where('id',$id)->get("{$this->database_name}users")->row();
	}
}




class Panel extends MY_Controller
{
	
	public function __construct()
	{

		parent::__construct();

		if (!$this->isLoggedIn()) {
			redirect(base_url("home/admin"));
		}
		else
		{
			if (!$this->ion_auth->is_admin()) {
				redirect(base_url("home/admin"));
			}			
		}

	}
}
