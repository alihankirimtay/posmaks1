<?php

class Posmaks
{
	private $url = null;

	public function __construct($url)
	{
		$this->url = $url[0];
	}


	public function getRestaurants()
	{
		return $this->apiRequest('Online/getrestaurants');
	}

	public function getProducts($inputs)
	{
		return $this->apiRequest('Online/getproducts', $inputs);
	}

    public function getProductsGroup()
    {
        return $this->apiRequest('Online/getproductsgroup');
    }

    public function getFindProductsWithRelatedItems($location_id)
    {
        return $this->apiRequest('Online/getFindProductsWithRelatedItems', $location_id);
    }

    public function getProducts_Additional($product_id)
    {
        return $this->apiRequest('Online/getProducts_Additional', $product_id);
    }

    public function getPayment_Types()
    {
        return $this->apiRequest('Online/getPayment_Types');
    }

    public function create_Customer($pos_Data)
    {
        return $this->apiRequest('Online/create_Customer',$pos_Data);
    }
    
    public function customer_edit($edit_data)
    {
        return $this->apiRequest('Online/customer_edit', $edit_data);
    }

    public function customer_Order($customer_id)
    {
        return $this->apiRequest('Online/customer_Order',$customer_id);
    }

    public function getAll_Order()
    {
        return $this->apiRequest('Online/getAll_Order');
    }

    

    public function create_Sale($sale_Data)
    {
        return $this->apiRequest('Online/create_Sale', $sale_Data);
    }

    public function getOrder_Detail($sale_id)
    {
        return $this->apiRequest('Online/getOrder_Detail',$sale_id);
    }


    

	private function apiRequest($service, $inputs = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->url}/Api/{$service}");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($inputs));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close($ch);

        $data = (Array) json_decode($server_output, true);

        $data = (Object) array_merge([
            'status' => 'ERROR',
            'message' => '500 Internal Server Error!',
            'items' => []
        ], $data);

        if ($data->status != 'SUCCESS') {

            throw new Exception($data->message);
        }
        return $data->items;
        
    }
}