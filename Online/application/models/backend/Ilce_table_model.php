<?php


class Ilce_table_model extends CI_Model
{
	public function ilce($sehir_key)
	{
		$ilce = $this->db->get_where("{$this->database_name}ilce",array('ilce_sehirkey' => $sehir_key));
		return $ilce->result();
	}

	public function ilce_title($ilce_key)
	{
		$ilce = $this->db->get_where("{$this->database_name}ilce",array('ilce_key' => $ilce_key));
		return $ilce->result();
	}

}