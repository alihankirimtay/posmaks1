<?php


class Restaurant_mahalle_model extends CI_Model
{
	public function mahalle($restaurant_id)
	{
 		$this->db->select('*');
		$this->db->from("{$this->database_name}mahalle");
		$this->db->join("{$this->database_name}restaurants_mahalle", "{$this->database_name}restaurants_mahalle.mahalle_id = {$this->database_name}mahalle.mahalle_id");
		$this->db->where("restaurant_id",$restaurant_id);
		$mahalle=$this->db->get();

		return $mahalle->result();
	}

	public function getMahalle($location_id)
	{
		$this->db->select('mahalle_id');
		$this->db->from("{$this->database_name}restaurants_mahalle");
		$this->db->where("restaurant_id",$location_id);
		$mahalle=$this->db->get();
		return $mahalle->result();
	}
	
}

