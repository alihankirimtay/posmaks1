<?php


class Address_control_model extends CI_Model
{
	public function address_control($sehir_id)
	{
		$this->db->select('*');
		$this->db->from("{$this->database_name}sehir");
		$this->db->join("{$this->database_name}ilce", "{$this->database_name}sehir.sehir_key = {$this->database_name}ilce.ilce_sehirkey");
		$this->db->join("{$this->database_name}mahalle", "{$this->database_name}ilce.ilce_key = {$this->database_name}mahalle.mahalle_ilcekey");
		$this->db->where("sehir_id",$sehir_id);
		$ilce=$this->db->get();
		return $ilce->result();
	}
}