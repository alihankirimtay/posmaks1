<?php


class min_area_price_model extends CI_Model
{
	public function area($restaurant_id)
	{
		$area = $this->db->get_where("{$this->database_name}min_area_price",array('restaurant_id' => $restaurant_id));
		return $area->result();
	}

	public function address($restaurant_id)
	{
		$area_address = $this->db
		->select('*')
		->select("(select mahalle_title from {$this->database_name}mahalle where mahalle_id = {$this->database_name}min_area_price.mahalle_id) as mahalle_title")
		->get_where("{$this->database_name}min_area_price",array('restaurant_id' => $restaurant_id));
		return $area_address->result();	
	}
	
}

