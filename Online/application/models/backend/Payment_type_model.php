<?php


class Payment_type_model extends CI_Model
{
	public function index()
	{
		$payment_type = $this->db->get("{$this->database_name}payment_type");
		return $payment_type->result();
	}
}