<?php


class Address_model extends CI_Model
{


	public function customers_address()
	{
		$address = $this->db
		->select('*')
		->select("(select sehir_title from {$this->database_name}sehir where sehir_id = {$this->database_name}address.sehir_id) as sehir_title")
		->select("(select ilce_title from {$this->database_name}ilce where ilce_id = {$this->database_name}address.ilce_id) as ilce_title")
		->select("(select mahalle_title from {$this->database_name}mahalle where mahalle_id = {$this->database_name}address.mahalle_id) as mahalle_title")
		->get("{$this->database_name}address");
		return $address->result();		
	}

	public function user_address($id)
	{
		$address = $this->db
		->select('*')
		->select("(select sehir_title from {$this->database_name}sehir where sehir_id = {$this->database_name}address.sehir_id) as sehir_title")
		->select("(select ilce_title from {$this->database_name}ilce where ilce_id = {$this->database_name}address.ilce_id) as ilce_title")
		->select("(select mahalle_title from {$this->database_name}mahalle where mahalle_id = {$this->database_name}address.mahalle_id) as mahalle_title")
		->get_where("{$this->database_name}address",array('user_id' => $id));
		return $address->result();		
	}

	public function addressEdit ($address_id)
	{
		$addressEdit = $this->db
		->select('*')
		->select("(select sehir_title from {$this->database_name}sehir where sehir_id = {$this->database_name}address.sehir_id) as sehir_title")
		->select("(select sehir_key from {$this->database_name}sehir where sehir_id = {$this->database_name}address.sehir_id) as sehir_key")
		->select("(select ilce_title from {$this->database_name}ilce where ilce_id = {$this->database_name}address.ilce_id) as ilce_title")
		->select("(select ilce_key from {$this->database_name}ilce where ilce_id = {$this->database_name}address.ilce_id) as ilce_key")
		->select("(select mahalle_title from {$this->database_name}mahalle where mahalle_id = {$this->database_name}address.mahalle_id) as mahalle_title")
		->get_where("{$this->database_name}address",array('id' => $address_id));
		return $addressEdit->result();	
	}

	public function address_control($sehir_id)
	{
		$this->db->select('*');
		$this->db->from("{$this->database_name}sehir");
		$this->db->join("{$this->database_name}ilce", "{$this->database_name}sehir.sehir_key = {$this->database_name}ilce.ilce_sehirkey");
		$this->db->join("{$this->database_name}mahalle", "{$this->database_name}ilce.ilce_key = {$this->database_name}mahalle.mahalle_ilcekey");
		$this->db->where("sehir_id",$sehir_id);
		$ilce=$this->db->get();
		return $ilce->result();
	}

	public function customer_mahalle($address_id)
	{
		$this->db->select('mahalle_id');
		$this->db->from("{$this->database_name}address");
		$this->db->where("id",$address_id);
		$mahalle=$this->db->get();
		return $mahalle->result();
	}

	public function add($data)
	{
		$add = $this->db->insert("{$this->database_name}address",$data);
		return $add;
	}

	public function update($id,$data)
	{
		$update = $this->db->where('id',$id)->update("{$this->database_name}address",$data);
		return $update;
	}

	public function delete($id)
	{
		$this->db->set('active', 1);
        $this->db->where('id', $id);
        $this->db->update("{$this->database_name}address");
	}

}