<?php


class Sale_model extends CI_Model
{

	public function get_Order($user_id)
	{
		$order = $this->db->order_by('sale_id','DESC')->get_where("{$this->database_name}order",array('user_id' => $user_id));
		return $order->result();
	}

	public function add($data)
	{
		$this->db->insert("{$this->database_name}order",$data);
	}

}