<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_cart extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("backend/discounts_model");
		$this->load->model("frontend/sale_model");
		$this->load->library("cart");
	}

	public function basket()
	{
		$basket = $this->cart->contents();
		echo json_encode($basket);
	}

	public function basket_discount()
	{
		if($this->ion_auth->logged_in())
		{
			$user_id = $this->ion_auth->user()->row()->id;

			$discount = $this->discounts_model->index();
			$user_orders = $this->sale_model->get_Order($user_id);
			$basket = $this->cart->contents();

			$data = array(
				"discount" => $discount,
				"user_orders" => $user_orders,
				"basket" => $basket
			);
			
			echo json_encode($data);
		}
		
	}

	public function add()
	{
		

		$main_products = array(
			"restaurant_id"	=> $_POST["restaurant_id"],
			"id" 			=> $_POST["product_id"],
			"name" 			=> $_POST["product_title"],
			"price" 		=> $_POST["product_price"],
			"qty" 			=> $_POST["quantity"],
			"options" 		=> null,
			"details" 		=> null,
			"materials" 	=> null
		);
		
		$product_details = $this->input->post("product_details");
		$main_products["details"] = $product_details;

		$stock_products = $this->input->post("stock_products[]");
		if($stock_products != null)
		{
			foreach ($stock_products as $stock_pro) 
			{
				$materials[] = array(
					"id"	=> $stock_pro[0],
					"name" 	=> $stock_pro[1]
				);
			}

			foreach($materials as $material)
			{
				$main_products["materials"][] = array(
					"id" 	=> $material['id'],
					"name" 	=> $material['name']
				);
			}
		}

		$additional_products = $this->input->post("additional_products[]");
		if($additional_products != null)
		{
			foreach($additional_products as $additional_product)
			{
				$additional[] = array(
					"id" 	=> $additional_product[0],
					"name" 	=> $additional_product[1],
					"price" => $additional_product[2]
				);
			}

			foreach($additional as $add)
			{
				$main_products["options"][] = array(
					"id" 	=> $add['id'],
					"name" 	=> $add['name'],
					"price" => $add['price'],
				);	
			}
			

			$this->cart->insert($main_products);
			
		}
		else
		{	
			$this->cart->insert($main_products);
		}
	}


	
	
	public function editProduct()
	{
		$rowid = $_POST["rowid"];

		foreach($this->cart->contents() as $items)
		{
			if($items['rowid'] == $rowid)
			{
				echo json_encode($items);
			}
		}
	}

	public function update()
	{
		$data = array(
			'rowid'   => $_POST["rowid"],
			'qty'     => 0
			);

		$this->cart->update($data);

		$main_products = array(
			"restaurant_id"	=> $_POST["restaurant_id"],
			"id" 			=> $_POST["product_id"],
			"name" 			=> $_POST["product_title"],
			"price" 		=> $_POST["product_price"],
			"qty" 			=> $_POST["quantity"],
			"options" 		=> null,
			"details" 		=> null,
			"materials"		=> null
		);

		$product_details = $this->input->post("product_details");
		$main_products["details"] = $product_details;

		$stock_products = $this->input->post("stock_products[]");
		if($stock_products != null)
		{
			foreach ($stock_products as $stock_pro) 
			{
				$materials[] = array(
					"id" 	=> $stock_pro[0],
					"name" 	=> $stock_pro[1]
				);
			}

			foreach($materials as $material)
			{
				$main_products["materials"][] = array(
					"id" 	=> $material['id'],
					"name" 	=> $material['name']
				);
			}
		}

		$additional_products = $this->input->post("additional_products[]");
		if($additional_products != null)
		{
			foreach($additional_products as $additional_product)
			{
				$additional[] = array(
					"id" 	=> $additional_product[0],
					"name" 	=> $additional_product[1],
					"price" => $additional_product[2]
				);
			}

			foreach($additional as $add)
			{
				$main_products["options"][] = array(
					"id" 	=> $add['id'],
					"name" 	=> $add['name'],
					"price" => $add['price']
				);	
			}
			

			$this->cart->insert($main_products);
		}
		else
		{	
			$this->cart->insert($main_products);
		}
	}


	public function delete()
	{
		$data = array(
			'rowid'   => $_POST["rowid"],
			'qty'     => 0
			);

		$this->cart->update($data);
	}

	public function destroy()
	{
		$this->cart->destroy();
	}

}
