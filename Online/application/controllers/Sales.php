<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model("frontend/address_model");
		$this->load->model("frontend/sale_model");
		$this->load->model("frontend/users_model");
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/products_model");
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/slide_model");
		$this->load->model("backend/settings_model");
		$this->load->model("backend/discounts_model");
		$this->load->model("backend/payment_type_model");
		$this->load->model("backend/restaurant_mahalle_model");
		$this->load->model("backend/min_area_price_model");

		$this->load->helper(array('form', 'url'));
		$this->load->library('ion_auth','form_validation');

	}


	public function create_Sale()
	{

	try {
		
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('location_id', 'Location', 'trim|required');
		$this->form_validation->set_rules('active_address', 'Address', 'required',array('required' => 'Adres Alanı Boş Olamaz'));
		$this->form_validation->set_rules('active_payment', 'Payment Type', 'required',array('required' => 'Ödeme Alanı Boş Olamaz'));
		



		

		$user_id 				= $this->ion_auth->user()->row()->id;
		$customer				= $this->users_model->customer_id($user_id);
		$location_id 			= $this->input->post('location_id');
		$payment_type 			= $this->input->post('active_payment');
		$payment_title			="";
		$address 				= $this->input->post('active_address');
		$order_date 			= date('Y-m-d H:i:s');
		$order_note 			= $this->input->post('order_note');
		$total_price 			= $this->input->post('total_price');
		$total_discount_price 	= $this->input->post('total_discount_price');

		$payment_type_pos 		= $this->Posmaks->getPayment_Types([]);
		$discounts 				= $this->discounts_model->index();
		$user_orders_count 		= count($this->sale_model->get_Order($user_id));

		$basket_products 		= $this->cart->contents();

		$customer_id = 0;
		foreach ($customer as $cus) {
			$customer_id = $cus;
		}


		//Lokasyon Açık-Kapalı Kontrolü
		$restaurant_info = $this->restaurants_model->restaurants_info($location_id);

		$open_time;
        $close_time;
        $now_time = date("H:i:s");

        foreach($restaurant_info as $inf) {
            $open_time = $inf->open_time;
            $close_time = $inf->close_time;

            if($inf->status == 1)
            {

	            if(($now_time > $open_time && $now_time < $close_time))
	            {

	            }
	            else
	            {
	            	throw new Exception("Şube Kapalı Olduğundan Şu Anda Hizmet Verememektedir");
	            }
            }

            else
            {
            	throw new Exception("Şube Kapalı Olduğundan Şu Anda Hizmet Verememektedir");
            }    
        }

		//Sepetteki Ürün Kontrolü        
		if(count($basket_products) < 1)
		{
			throw new Exception("Sepette Ürün Olmadığı İçin Sipariş Veremezsiniz");
		}

		
		//Validation Kontrolü		
		if(!$this->form_validation->run()) {
			throw new Exception(validation_errors());
		}



		//Adrese Gönderim Kontrolü(Seçilen adrese gönmderim varmı)
		$customer_select_mahalle = $this->address_model->customer_mahalle($address);
		$location_send_mahalle = $this->restaurant_mahalle_model->getMahalle($location_id);
		$area_price = $this->min_area_price_model->area($location_id);


		$mahalle_count = 0;
		foreach ($location_send_mahalle as $mahalle) {
			foreach ($customer_select_mahalle as $cus_mahalle) {
				if($mahalle->mahalle_id == $cus_mahalle->mahalle_id)
				{
					$mahalle_count = 1;
				}

			}
		}
	   	if($mahalle_count != 1)
	   	{
	   		throw new Exception("Adresinize Gönderim Bulunmamaktadır");
	   	}

	   	//Kullanıcı Adresinin Pos tarafında güncellenmesi için
	   	$customer_select_address = $this->address_model->addressEdit($address);
	   	foreach ($customer_select_address as $cus_address) {
	   		$address_data = $cus_address->acik_adres."/".$cus_address->sehir_title."/".$cus_address->ilce_title."/".$cus_address->mahalle_title;
	   	}




		//Minimum Gönderim Tutarı Kontrolü
		$min_area_price = 0;	   	
	   	foreach ($area_price as $min_price) {
	   		foreach ($customer_select_mahalle as $cust_mahalle) {
	   			if($min_price->mahalle_id == $cust_mahalle->mahalle_id)
	   			{
	   				$min_area_price = $min_price->price;
	   			}
	   		}
	   	}

	   	if($total_discount_price < $min_area_price)
	   	{
	   		throw new Exception("Minimum Gönderim Tutarının Altındasınız");
	   	}

	   	

	   	foreach ($payment_type_pos as $pos_paymemnt) {
   			if($pos_paymemnt['id'] == "11")
   			{
   				$payment_title = $pos_paymemnt['title'];
   			}
	   	}
		
		$notes = array(
			"order_note" => $order_note,
			"payment_type" => $payment_title
		);

		//POS Ürünler
		foreach ($basket_products as $pro ) {
			$products[$pro['rowid']] = array(
				"product_id" => $pro['id'],
				"quantity" => $pro['qty'],
				"additional_products" => null,
				"stocks" => null,
				"text_notes" => $pro['details']

			);


			$additional_products = $pro["options"];

			if (is_array($additional_products) || is_object($additional_products))
			{
				foreach ($additional_products as $add_pro) {
					$products[$pro['rowid']]["additional_products"][$add_pro['id']] = $add_pro['id'];
				}
			}

			
			$stock_products = $pro["materials"];
			if (is_array($stock_products) || is_object($stock_products))
			{
				foreach ($stock_products as $stock_pro) {
					$products[$pro['rowid']]["stocks"][$stock_pro['id']] = $stock_pro['id'];
				}
			}
		}
		
		//POS İndirimler
		$discount_amount = 0;
		foreach ($discounts as $disc) {
			if($disc->active == 1 && $disc->deleted_time == null && ($user_orders_count == ($disc->which_discount - 1)))
			{
				$discount_amount += $disc->discount_rate;
				
			}
			else if($disc->active == 1 && $disc->deleted_time == null && $disc->which_discount == 0)
			{
				$discount_amount += $disc->discount_rate;
			}
		}

		//POS Ödeme Tipi
		$payments = array(
			"id" => $payment_type
		);



		$sale_Data = array(	
			"location_id" => $location_id,
			"customer_id" => $customer_id,
			"address" => $address_data,
			"products" => $products,
			"payments" => $payment_title,
			"order_date" => $order_date,
			"order_note" => $notes,
			"discount_amount" => $discount_amount
		);
		
		$pos_sale_id = $this->Posmaks->create_Sale($sale_Data);

			



		//Siparişin Oluşturulduğu Yer
		$orderData = array(
			"sale_id" 		=> $pos_sale_id,
			"location_id" 	=> $location_id,
			"user_id" 		=> $user_id,
			"payment_type" 	=> $payment_type,
			"address" 		=> $address,
			"order_note" 	=> $order_note,
			"order_date" 	=> $order_date,
			"total_price" 	=> $total_price,
			"total_discount_price" 	=> $total_discount_price,
			"status" 		=> "pending"

		);
		$add = $this->sale_model->add($orderData);


		//İndirimlerin Eklendiği Yer

		foreach ($discounts as $disc) {
			if($disc->active == 1 && $disc->deleted_time == null && ($user_orders_count == ($disc->which_discount - 1)))
			{
				$data = array(
					"order_id" => $pos_sale_id,
					"discount_id" => $disc->id,
					"discount_qty" => $disc->discount_rate
				);  

				$insert = $this->db->insert("{$this->database_name}order_discount",$data);
			}
			else if($disc->active == 1 && $disc->deleted_time == null && $disc->which_discount == 0)
			{
				$data_ = array(
					"order_id" => $pos_sale_id,
					"discount_id" => $disc->id,
					"discount_qty" => $disc->discount_rate
				);

				$insert = $this->db->insert("{$this->database_name}order_discount",$data_);
			}
		}


		//Ürünlerin Eklendiği Yer

		foreach ($basket_products as $pro) {
			$proData = array(
				"order_id" => $pos_sale_id,
				"product_row_id" => $pro["rowid"],
				"product_id" => $pro["id"],
				"qty" => $pro["qty"],
				"note" => $pro["details"]
			);

			$this->db->insert("{$this->database_name}order_products",$proData);

			if (is_array($pro["options"]) || is_object($pro["options"]))
			{
				foreach ($pro["options"] as $option) {
					$proOption = array(
						"order_id" => $pos_sale_id,
						"product_row_id" => $pro["rowid"],
						"product_id" => $pro["id"],	
						"option_product_id" => $option["id"],
						"qty" => $pro["qty"]

					);
					$this->db->insert("{$this->database_name}order_options",$proOption);
				}
			}
			if (is_array($pro["materials"]) || is_object($pro["materials"]))
			{
				foreach ($pro["materials"] as $material) {
					$proMaterial = array(
						"order_id" => $pos_sale_id,
						"product_row_id" => $pro["rowid"],
						"product_id" => $pro["id"],	
						"material_product_id" => $material["id"],
						"qty" => $pro["qty"]

					);
					$this->db->insert("{$this->database_name}order_materials",$proMaterial);

				}
			}
		}

		echo json_encode([
			'status' => 'success',
			'message' => 'Siparişiniz Şubemize İletildi.Teşekkür Ederiz',
		]);

		$this->cart->destroy();

		}
		catch (Exception $e) {

			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}

	}



}