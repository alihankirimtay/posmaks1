<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends My_Controller {


	public function __construct()
	{
		parent::__construct();
		
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/products_model");

		$this->load->helper(array('form', 'url'));
	}


	public function index()
	{		
		$restaurantPos = $this->Posmaks->getRestaurants([
			]);

		$restaurants = $this->restaurants_model->restaurants();
		
		if(count($restaurants) == 1)
		{
			foreach ($restaurants as $restaurant) {
				$restaurant_id = $restaurant->id;
			}
			$this->main($restaurant_id);
		}
		else
		{
			$this->load->view("frontend/locationPage",compact('restaurants','restaurantPos'));
		} 

	}

	public function main($restaurant_id = null)
	{
		$locations = $this->restaurants_model->restaurants();
		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		$products = $this->products_model->products($restaurant_id);
		$this->load->view("frontend/main",compact('locations','products','restaurant_id','restaurant_info'));

	}

}
