<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Panel {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/ilce_table_model");
		$this->load->model("backend/mahalle_table_model");
		$this->load->model("backend/restaurant_sehir_model");
		$this->load->model("backend/restaurant_ilce_model");
		$this->load->model("backend/restaurant_mahalle_model");
		$this->load->model("backend/products_model");
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/min_area_price_model");
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view("backend/panel");
	}

	

	public function get_ilce()
	{
		$sehir_key = $this->input->post('sehir_key');
		$ilceler = $this->ilce_table_model->ilce($sehir_key);
		if(count($ilceler)>0)
		{
			$ilce_select_box = '';
			foreach($ilceler as  $ilce)
			{
				$ilce_select_box .='<option value="'.$ilce->ilce_id.'" key="'.$ilce->ilce_key.'" >'.$ilce->ilce_title.'</option>';
			}

			echo json_encode($ilce_select_box);
		}
	}

	public function get_mahalle()
	{
		$ilce_key = $this->input->post('ilce_key');
		$mahalleler = $this->mahalle_table_model->mahalle($ilce_key);
		if(count($mahalleler)>0)
		{
			$mahalle_select_box = '<optgroup data-ilce="'.$ilce_key.'" label="'.$ilce_key.'">';
			foreach($mahalleler as  $mahalle)
			{
				$mahalle_select_box .='<option value="'.$mahalle->mahalle_id.'" key="'.$mahalle->mahalle_key.'" >'.$mahalle->mahalle_title.'</option>';
			}
			$mahalle_select_box .= '</optgroup>';
			echo json_encode($mahalle_select_box);
		}
	}
	public function get_products_group()
	{
		$products_group = $this->Posmaks->getProductsGroup([
		]);
		echo json_encode($products_group);

	}

	public function get_products()
	{

		$restaurant_id = $this->input->post('restaurant_id');
		$products = $this->Posmaks->getProducts([
			'location_id' => $restaurant_id
		]);
		echo json_encode($products);

	}

	

	public function addRestaurant()
	{
		$this->form_validation->set_rules('restaurant', 'restaurant', 'required');
		$this->form_validation->set_rules('sehir', 'sehir', 'required');
		$this->form_validation->set_rules('ilce', 'ilce', 'required');
		$this->form_validation->set_rules('mahalle', 'mahalle', 'required');

		$this->form_validation->set_rules('products', 'products', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$restaurant = $this->input->post("restaurant");
			$sehir 		= $this->input->post("sehir");
			$ilce 		= $this->input->post("ilce[]");
			$mahalle 	= $this->input->post("mahalle[]");
			$products 	= $this->input->post("products[]");





			$restaurantData = array(
				"id" 		=> $restaurant
			);
			$restaurant_add = $this->db->insert("{$this->database_name}restaurants",$restaurantData);

			$sehirData = array(
				"restaurant_id" => $restaurant,
				"sehir_id" 		=> $sehir
			);
			$sehir_add = $this->db->insert("{$this->database_name}restaurants_sehir",$sehirData);

			foreach($ilce as $ilc)
			{
				$ilceData = array(
					"ilce_id" 		=> $ilc,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}restaurants_ilce",$ilceData);
			}
			

			foreach($mahalle as $mah)
			{
				$mahalleData = array(
					"mahalle_id" 	=> $mah,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}restaurants_mahalle",$mahalleData);
			}
			

			foreach ($products as $product) {
				$productsData = array(
					"id" 			=> $product,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}products",$productsData);
			}
			
		

		}

	}


	public function editRestaurant()
	{

		$this->form_validation->set_rules('restaurant', 'restaurant', 'required');
		$this->form_validation->set_rules('sehir', 'sehir', 'required');
		$this->form_validation->set_rules('ilce', 'ilce', 'required');
		$this->form_validation->set_rules('mahalle', 'mahalle', 'required');

		$this->form_validation->set_rules('products', 'products', 'required');
		
		if($this->form_validation->run() == FALSE)
		{
			$restaurant = $this->input->post("restaurant");
			

			$restaurant_sehir 	= $this->input->post("restaurant_sehir");
			$restaurant_ilce 	= $this->input->post("restaurant_ilce");
			$restaurant_mahalle = $this->input->post("restaurant_mahalle");
			$openTime 			= $this->input->post("openTime");
			$closeTime 			= $this->input->post("closeTime");
			$phone 				= $this->input->post("phone");
			$mail 				= $this->input->post("mail");

			$location_sehir 	= $this->input->post("sehir");
			$location_ilce 		= $this->input->post("ilce[]");
			$location_mahalle 	= $this->input->post("mahalle[]");
			$products 			= $this->input->post("products[]");

			
			$area_mahalle_id 	= $this->input->post("area_mahalle_id[]");
			

		

			$restaurantData = array(
				"id" 			=> $restaurant,
				"sehir_id" 		=> $restaurant_sehir,
				"ilce_id" 		=> $restaurant_ilce,
				"mahalle_id" 	=> $restaurant_mahalle,
				"open_time" 	=> $openTime,
				"close_time" 	=> $closeTime,
				"phone" 		=> $phone,
				"email" 		=> $mail
			);
			$this->db->where('id',$restaurant)->update("{$this->database_name}restaurants",$restaurantData);
			

			$sehirData = array(
				"restaurant_id" => $restaurant,
				"sehir_id" 		=> $location_sehir
			);
			$this->db->delete("{$this->database_name}restaurants_sehir", array('restaurant_id' => $restaurant));
			$sehir_add = $this->db->insert("{$this->database_name}restaurants_sehir",$sehirData);

			$this->db->delete("{$this->database_name}restaurants_ilce", array('restaurant_id' => $restaurant));
			foreach($location_ilce as $ilc)
			{
				$ilceData = array(
					"ilce_id" 		=> $ilc,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}restaurants_ilce",$ilceData);
			}
			

			$this->db->delete("{$this->database_name}restaurants_mahalle", array('restaurant_id' => $restaurant));
			foreach($location_mahalle as $mah)
			{
				$mahalleData = array(
					"mahalle_id" 	=> $mah,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}restaurants_mahalle",$mahalleData);
			}
			
			$this->db->delete("{$this->database_name}min_area_price", array('restaurant_id' => $restaurant));
			foreach($area_mahalle_id as $area)
			{
				$area_ilce_id 		= $this->input->post("area_ilce_id[{$area}]");
				$area_price 		= $this->input->post("area_price[{$area}]");
				$areaData = array(
					"restaurant_id" => $restaurant,
					"ilce_id" => $area_ilce_id,
					"mahalle_id" => $area,
					"price" => $area_price
				);

				$this->db->insert("{$this->database_name}min_area_price",$areaData);
			}


			$this->db->delete("{$this->database_name}products", array('restaurant_id' => $restaurant));
			if(count($products) > 0){
				foreach ($products as $product) {
					$productsData = array(
						"id" 			=> $product,
						"restaurant_id" => $restaurant
					);
					$this->db->insert("{$this->database_name}products",$productsData);
				}
			}

		}

	}



	public function Panel()
	{
		$this->load->view("backend/panel");
	}

	public function locations()
	{
			$restaurantPos = $this->Posmaks->getRestaurants([
			]);

		$restaurants = $this->restaurants_model->restaurants();
	

		$this->load->view("backend/locations",compact('restaurants','restaurantPos'));
	}

	public function RestaurantStatus()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$status = $this->restaurants_model->status($restaurant_id);

		echo json_encode($status);
	}

	public function RestaurantStatusUpdate()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$status = $this->input->post("status");
	
		$update = $this->restaurants_model->statusUpdate($restaurant_id,$status);
		

	}

	public function editPage($restaurant_id = null)
	{
		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		$restaurant_address = $this->restaurants_model->restaurant_address($restaurant_id);
		$sehirler = $this->sehir_table_model->sehir();
		$area_address = $this->min_area_price_model->address($restaurant_id);
		$sehir = $this->restaurant_sehir_model->sehir($restaurant_id);
		$ilce = $this->restaurant_ilce_model->ilce($restaurant_id); 
		$mahalle = $this->restaurant_mahalle_model->mahalle($restaurant_id);
		$products = $this->products_model->products($restaurant_id);

		$this->load->view("backend/adminpanel_edit",compact('restaurant_info','restaurant_address','sehirler','area','area_address','sehir','ilce','mahalle','products','restaurant_id'));
		
	}



	public function delete()
	{
		$restaurant_id = $this->input->post('restaurant_id');

		
		$this->db->delete("{$this->database_name}restaurants_sehir", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants_ilce", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants_mahalle", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}products", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants", array('id' => $restaurant_id));
		$this->db->delete("{$this->database_name}min_area_price", array('restaurant_id' => $restaurant_id));
	}


	public function slideManagement()
	{
		$this->load->view("backend/slideManagement");
	}

	

}
