<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends Panel {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("backend/sehir_table_model");
		$this->load->model("backend/ilce_table_model");
		$this->load->model("backend/mahalle_table_model");
		$this->load->model("backend/restaurant_sehir_model");
		$this->load->model("backend/restaurant_ilce_model");
		$this->load->model("backend/restaurant_mahalle_model");
		$this->load->model("backend/products_model");
		$this->load->model("backend/restaurants_model");
		$this->load->model("backend/min_area_price_model");
		$this->load->model("backend/address_control_model");
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');


	}
    
    public function newLocationPage()
    {
        try {
			$restaurants = $this->Posmaks->getRestaurants([]);
			$sehirler = $this->sehir_table_model->sehir();
			$this->load->view("backend/adminpanel",compact('restaurants','sehirler'));
		} 
		catch (Exception $e) 
		{
			exit($e->getMessage());
		}
    }

    public function editPage($restaurant_id = null)
	{
		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		$restaurant_address = $this->restaurants_model->restaurant_address($restaurant_id);
		$sehirler = $this->sehir_table_model->sehir();
		$area_address = $this->min_area_price_model->address($restaurant_id);
		$sehir = $this->restaurant_sehir_model->sehir($restaurant_id);
		$ilce = $this->restaurant_ilce_model->ilce($restaurant_id); 
		$mahalle = $this->restaurant_mahalle_model->mahalle($restaurant_id);
		$products = $this->products_model->products($restaurant_id);

		$this->load->view("backend/adminpanel_edit",compact('restaurant_info','restaurant_address','sehirler','area','area_address','sehir','ilce','mahalle','products','restaurant_id'));
		
	}



	public function ClockTime()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$restaurant_info = $this->restaurants_model->restaurants_info($restaurant_id);
		echo json_encode($restaurant_info);
	}
	
	public function get_products_group()
	{
		$products_group = $this->Posmaks->getProductsGroup([
		]);
		echo json_encode($products_group);

	}

	public function get_products()
	{

		$restaurant_id = $this->input->post('restaurant_id');
		if($restaurant_id != null)
		{
			$products = $this->Posmaks->getProducts([
				'location_id' => $restaurant_id
			]);
			echo json_encode($products);

		}
		
	}


    
	
	public function locations()
	{
		
		$restaurantPos = $this->Posmaks->getRestaurants([]);

		$restaurants = $this->restaurants_model->restaurants();
	

		$this->load->view("backend/locations",compact('restaurants','restaurantPos'));
	}

	public function RestaurantStatus()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$status = $this->restaurants_model->status($restaurant_id);

		echo json_encode($status);
	}

	public function RestaurantStatusUpdate()
	{
		$restaurant_id = $this->input->post("restaurant_id");
		$status = $this->input->post("status");
	
		$update = $this->restaurants_model->statusUpdate($restaurant_id,$status);
		

	}
    
    

	public function addRestaurant()
	{
		try{

		$this->form_validation->set_rules('restaurant', 'restaurant', 'required');
		$this->form_validation->set_rules('sehir', 'sehir', 'required');
		
		
		$restaurant = $this->input->post("restaurant");
		$sehir 		= $this->input->post("sehir");
		$ilce 		= $this->input->post("ilce[]");
		$mahalle 	= $this->input->post("mahalle[]");
		$products 	= $this->input->post("products[]");

		$sehir_info = $this->address_control_model->address_control($sehir);
		$restaurants = $this->Posmaks->getRestaurants([]);



		$rest_control = 0;
		foreach ($restaurants as $restaurant_control) {
			if($restaurant == $restaurant_control["id"])
			{
				$rest_control = 1;
			}
		}
		if($rest_control == 0)
		{
			throw new Exception("Böyle bir restaurant yok");
		}



		if($sehir == null || $ilce == null || $mahalle == null)
		{
			throw new Exception("Gönderilecek Adresler Boş Olamaz");
		}


		$this->db->delete("{$this->database_name}restaurants", array('id' => $restaurant));
		$restaurantData = array(
			"id" 		=> $restaurant
		);
		$restaurant_add = $this->db->insert("{$this->database_name}restaurants",$restaurantData);

		
		if($sehir_info)
		{	
			$this->db->delete("{$this->database_name}restaurants_sehir", array('restaurant_id' => $restaurant));
			$sehirData = array(
				"restaurant_id" => $restaurant,
				"sehir_id" 		=> $sehir
			);
			$sehir_add = $this->db->insert("{$this->database_name}restaurants_sehir",$sehirData);
			
		}
		else
		{
			throw new Exception("Böyle Bir Şehir Yok");
			
		}
				

		$this->db->delete("{$this->database_name}restaurants_ilce", array('restaurant_id' => $restaurant));
		foreach($ilce as $ilc_)
		{
			$statu = 1;
			foreach ($sehir_info as $inf) {
				if($ilc_ == $inf->ilce_id && $statu == 1)
				{
					$ilceData = array(
						"ilce_id" 		=> $ilc_,
						"restaurant_id" => $restaurant
					);
					$this->db->insert("{$this->database_name}restaurants_ilce",$ilceData);
					$statu = 0;
				}
			}
		}


		$this->db->delete("{$this->database_name}restaurants_mahalle", array('restaurant_id' => $restaurant));
		foreach($mahalle as $mah_)
		{
			$statu_ = 1;
			foreach ($sehir_info as $inf) {
				if($mah_ == $inf->mahalle_id && $statu_ == 1)
				{
					$mahalleData = array(
						"mahalle_id" 	=> $mah_,
						"restaurant_id" => $restaurant
					);
					$this->db->insert("{$this->database_name}restaurants_mahalle",$mahalleData);
					$statu_ = 0;
				}
			}
		}
		

		$this->db->delete("{$this->database_name}products", array('restaurant_id' => $restaurant));
		if(count($products) > 0){
			foreach ($products as $product) {
				$productsData = array(
					"id" 			=> $product,
					"restaurant_id" => $restaurant
				);
				$this->db->insert("{$this->database_name}products",$productsData);
			}
		}

		echo json_encode([
			'status' => 'success',
			'message' => 'Kayıt başarılı',
		]);
		}
		catch (Exception $e) {
			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		}			

	}


	public function editRestaurant()
	{
		try {
			
		

			$this->form_validation->set_rules('restaurant', 'restaurant', 'required');
			$this->form_validation->set_rules('restaurant_sehir', 'restaurant_sehir', 'required');
			$this->form_validation->set_rules('restaurant_ilce', 'restaurant_ilce', 'required');
			$this->form_validation->set_rules('restaurant_mahalle', 'restaurant_mahalle', 'required');
			$this->form_validation->set_rules('acik_adres', 'acik_adres', 'required');
			$this->form_validation->set_rules('openTime', 'openTime', 'required');
			$this->form_validation->set_rules('closeTime', 'closeTime', 'required');
			$this->form_validation->set_rules('phone', 'phone', 'required');
			$this->form_validation->set_rules('mail', 'mail', 'required|valid_email');

			
			$restaurant = $this->input->post("restaurant");
			

			$restaurant_sehir 	= $this->input->post("restaurant_sehir");
			$restaurant_ilce 	= $this->input->post("restaurant_ilce");
			$restaurant_mahalle = $this->input->post("restaurant_mahalle");
			$acik_adres 		= $this->input->post("acik_adres");
			$openTime 			= $this->input->post("openTime");
			$closeTime 			= $this->input->post("closeTime");
			$phone 				= $this->input->post("phone");
			$mail 				= $this->input->post("mail");

			$lat 				= $this->input->post("lat");
			$lng 				= $this->input->post("lng");

			$location_sehir 	= $this->input->post("sehir");
			$location_ilce 		= $this->input->post("ilce[]");
			$location_mahalle 	= $this->input->post("mahalle[]");
			$products 			= $this->input->post("products[]");

			
			$area_mahalle_id 	= $this->input->post("area_mahalle_id[]");

			$sehir_info = $this->address_control_model->address_control($location_sehir);
			$restaurants = $this->Posmaks->getRestaurants([]);

			
			if(!$this->form_validation->run()) {
				throw new Exception(validation_errors());
			}
			
			


			$restaurantData = array(
				"id" 			=> $restaurant,
				"sehir_id" 		=> $restaurant_sehir,
				"ilce_id" 		=> $restaurant_ilce,
				"mahalle_id" 	=> $restaurant_mahalle,
				"acik_adres" 	=> $acik_adres,
				"open_time" 	=> $openTime,
				"close_time" 	=> $closeTime,
				"phone" 		=> $phone,
				"email" 		=> $mail,
				"map_x"			=> $lat,
				"map_y" 		=> $lng
			);
			$this->db->where('id',$restaurant)->update("{$this->database_name}restaurants",$restaurantData);



			$rest_control = 0;
			foreach ($restaurants as $restaurant_control) {
				if($restaurant == $restaurant_control["id"])
				{
					$rest_control = 1;
				}
			}
			if($rest_control == 0)
			{
				throw new Exception("Böyle bir restaurant yok");
			}



			if($location_sehir == null || $location_ilce == null || $location_mahalle == null)
			{
				throw new Exception("Gönderilecek Adresler Boş Olamaz");
			}

			
			if($sehir_info)
			{
				$sehirData = array(
				"restaurant_id" => $restaurant,
				"sehir_id" 		=> $location_sehir
				);
				$this->db->delete("{$this->database_name}restaurants_sehir", array('restaurant_id' => $restaurant));
				$sehir_add = $this->db->insert("{$this->database_name}restaurants_sehir",$sehirData);
				
			}
			else
			{
				throw new Exception("Böyle Bir Şehir Yok");
				
			}
					

			$this->db->delete("{$this->database_name}restaurants_ilce", array('restaurant_id' => $restaurant));
			foreach($location_ilce as $ilc_)
			{
				$statu = 1;
				foreach ($sehir_info as $inf) {
					if($ilc_ == $inf->ilce_id && $statu == 1)
					{
						$ilceData = array(
							"ilce_id" 		=> $ilc_,
							"restaurant_id" => $restaurant
						);
						$this->db->insert("{$this->database_name}restaurants_ilce",$ilceData);
						$statu = 0;
					}
				}
			}


			$this->db->delete("{$this->database_name}restaurants_mahalle", array('restaurant_id' => $restaurant));
			foreach($location_mahalle as $mah_)
			{
				$statu_ = 1;
				foreach ($sehir_info as $inf) {
					if($mah_ == $inf->mahalle_id && $statu_ == 1)
					{
						$mahalleData = array(
							"mahalle_id" 	=> $mah_,
							"restaurant_id" => $restaurant
						);
						$this->db->insert("{$this->database_name}restaurants_mahalle",$mahalleData);
						$statu_ = 0;
					}
				}
			}


			
			$this->db->delete("{$this->database_name}min_area_price", array('restaurant_id' => $restaurant));
			if(count($area_mahalle_id) > 0)
			{
				foreach($area_mahalle_id as $area)
				{
					$area_ilce_id 		= $this->input->post("area_ilce_id[{$area}]");
					$area_price 		= $this->input->post("area_price[{$area}]");
					$areaData = array(
						"restaurant_id" => $restaurant,
						"ilce_id" => $area_ilce_id,
						"mahalle_id" => $area,
						"price" => $area_price
					);

					$this->db->insert("{$this->database_name}min_area_price",$areaData);
				}
			}


			$this->db->delete("{$this->database_name}products", array('restaurant_id' => $restaurant));
			if(count($products) > 0){
				foreach ($products as $product) {
					$productsData = array(
						"id" 			=> $product,
						"restaurant_id" => $restaurant
					);
					$this->db->insert("{$this->database_name}products",$productsData);
				}
			}

			echo json_encode([
				'status' => 'success',
				'message' => 'Kayıt başarılı',
			]);

		} catch (Exception $e) {
			echo json_encode([
				'status' => 'error',
				'message' => $e->getMessage(),
			]);
		} 

		

	}



	public function delete()
	{
		$restaurant_id = $this->input->post('restaurant_id');

		
		$this->db->delete("{$this->database_name}restaurants_sehir", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants_ilce", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants_mahalle", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}products", array('restaurant_id' => $restaurant_id));
		$this->db->delete("{$this->database_name}restaurants", array('id' => $restaurant_id));
		$this->db->delete("{$this->database_name}min_area_price", array('restaurant_id' => $restaurant_id));
	}
	

}
